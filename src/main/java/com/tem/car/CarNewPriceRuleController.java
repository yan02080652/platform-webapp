package com.tem.car;

import com.alibaba.fastjson.JSON;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.car.api.service.ICarPriceRuleService;
import com.tem.car.dto.enums.CarType;
import com.tem.car.dto.enums.OriginType;
import com.tem.car.dto.enums.SupplierType;
import com.tem.car.dto.priceRule.CarPriceRuleDto;
import com.tem.car.dto.priceRule.CarPriceRuleGroupDto;
import com.tem.platform.action.BaseController;
import com.tem.pss.api.PriceRuleApplyService;
import com.tem.pss.dto.PriceRuleApplyDto;
import com.tem.pss.dto.PriceRuleCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 用车加价规则控制器
 * Created by heyuanjing on 18/8/27.
 */
@Controller
@RequestMapping("car/price")
public class CarNewPriceRuleController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(CarNewPriceRuleController.class);

    @Autowired
    private ICarPriceRuleService carPriceRuleService;
    @Autowired
    private PriceRuleApplyService priceRuleApplyService;

    /**
     * 价格策略组首页
     */
    @RequestMapping("group")
    public String groupIndex() {
        return "car/price/group.base";
    }

    /**
     * 策略组列表
     *
     * @param condition
     * @param pageIndex
     * @return
     */
    @RequestMapping("groupList")
    public String groupList(Model model, PriceRuleCondition condition, Integer pageIndex) {
        QueryDto<PriceRuleCondition> queryDto = new QueryDto<>(pageIndex, 10);
        queryDto.setCondition(condition);
        Page<CarPriceRuleGroupDto> pageList = carPriceRuleService.groupList(queryDto).getData();
        model.addAttribute("pageList", pageList);
        return "car/price/group_table.jsp";
    }

    /**
     * 新增或修改策略组
     *
     * @param id
     * @return
     */
    @RequestMapping("addGroup")
    public String addGroup(Model model ,Long id) {
        CarPriceRuleGroupDto priceRuleGroupDto = null;
        if (id == null) {
            priceRuleGroupDto = new CarPriceRuleGroupDto();
        } else {
            priceRuleGroupDto = carPriceRuleService.getGroup(id).getData();
        }
        model.addAttribute("priceRuleGroupDto", priceRuleGroupDto);
        return "car/price/group_detail.jsp";
    }

    /**
     * 删除策略组
     *
     * @param id
     * @return
     */
    @RequestMapping("delGroup")
    @ResponseBody
    public ResponseInfo delGroup(Long id) {
        return carPriceRuleService.delGroup(id).transfer();
    }

    /**
     * 保存策略组
     *
     * @param group
     * @return
     */
    @RequestMapping("saveGroup")
    @ResponseBody
    public ResponseInfo saveGroup(CarPriceRuleGroupDto group) {
        group.setCreateUser(getCurrentUser().getFullname());
        return carPriceRuleService.addOrUpdateGroup(group).transfer();
    }

    /**
     * 价格细则首页
     *
     * @param groupId
     * @return
     */
    @RequestMapping("rule")
    public String rule(Model model ,Long groupId) {
        CarPriceRuleGroupDto group = carPriceRuleService.getGroup(groupId).getData();
        model.addAttribute("group", group);
        model.addAttribute("hubs", OriginType.values());
        return "car/price/rule.base";
    }

    /**
     * 不同渠道商细则
     *
     * @return
     */
    @RequestMapping("ruleHub")
    public String ruleHub(Model model, OriginType channelCode) {
        model.addAttribute("channelCode", channelCode);
        model.addAttribute("supplierType", SupplierType.values());
        return "car/price/ruleHub.jsp";
    }

    /**
     * 细则列表
     *
     * @return
     */
    @RequestMapping("ruleList")
    public String ruleList(Model model, PriceRuleCondition condition, Integer pageIndex) {
        QueryDto<PriceRuleCondition> queryDto = new QueryDto<>(pageIndex, 10);
        queryDto.setCondition(condition);
        Page<CarPriceRuleDto> pageList = carPriceRuleService.ruleList(queryDto).getData();
        model.addAttribute("carTypes",CarType.values());
        model.addAttribute("supplierTypes",SupplierType.values());
        model.addAttribute("pageList", pageList);
        model.addAttribute("channelCode", condition.getChannelCode());
        return "car/price/rule_table.jsp";
    }

    /**
     * 新增或修改细则页面
     *
     * @param rule
     * @return
     */
    @RequestMapping("addRule")
    public String addRule(Model model, CarPriceRuleDto rule) {
        if (rule.getId() != null) {
            rule = carPriceRuleService.getRule(rule.getId()).getData();
            rule.setDistanceOp(rule.getDistance().substring(rule.getDistance().length() - 1));
            rule.setDistance(rule.getDistance().replace("+", "").replace("-", ""));
            rule.setPriceOp(rule.getEstPrice().substring(rule.getEstPrice().length() - 1));
            rule.setEstPrice(rule.getEstPrice().replace("+", "").replace("-", ""));
        }
        model.addAttribute("rule", rule);
        model.addAttribute("supplierType", SupplierType.values());
        model.addAttribute("carType", CarType.values());
        return "car/price/rule_detail.jsp";
    }

    /**
     * 保存细则
     *
     * @return
     */
    @RequestMapping("saveRule")
    @ResponseBody
    public ResponseInfo saveRule(CarPriceRuleDto rule) {
        rule.setDistance(rule.getDistance() + rule.getDistanceOp());
        String price = rule.getEstPrice() + rule.getPriceOp();
        rule.setEstPrice(price);
        rule.setSettlePrice(price);
        rule.setCreateUser(getCurrentUser().getFullname());
        return carPriceRuleService.addOrUpdateRule(rule).transfer();
    }

    /**
     * 删除细则
     *
     * @param id
     */
    @RequestMapping("delRule")
    @ResponseBody
    public ResponseInfo delRule(Long id) {
        return carPriceRuleService.delRule(id).transfer();
    }

    /**
     * 分配企业首页
     *
     * @return
     */
    @RequestMapping("apply")
    public String apply(Model model, Long groupId) {
        model.addAttribute("groupId",groupId);
        return "car/price/apply.base";
    }

    /**
     * 适用企业列表
     *
     * @return
     */
    @RequestMapping("applyList")
    public String applyList(Model model, PriceRuleCondition condition, Integer pageIndex) {
        QueryDto<PriceRuleCondition> queryDto = new QueryDto<>(pageIndex, 10);
        queryDto.setCondition(condition);
        Page<PriceRuleApplyDto> pageList = priceRuleApplyService.applyList(queryDto).getData();
        model.addAttribute("pageList", pageList);
        model.addAttribute("look", condition.getGroupId() != null);
        return "car/price/apply_table.jsp";
    }

    /**
     * 删除适用企业
     *
     * @param id
     * @return
     */
    @RequestMapping("delApply")
    @ResponseBody
    public ResponseInfo delApply(Long id) {
        return priceRuleApplyService.delApply(id);
    }

    /**
     * 修改分配
     *
     * @return
     */
    @RequestMapping("addApply")
    public String addApply(Model model, Long id, String groupName) {
        PriceRuleApplyDto apply = priceRuleApplyService.getApply(id).getData();
        apply.setGroupName(groupName);
        model.addAttribute("apply", apply);
        return "car/price/apply_detail.jsp";
    }

    /**
     * 保存企业分配
     *
     * @return
     */
    @RequestMapping("saveApply")
    @ResponseBody
    public ResponseInfo saveApply(PriceRuleApplyDto apply) {
        return priceRuleApplyService.addOrUpdateApply(apply);
    }

    /**
     * 企业分配价格策略组页面
     *
     * @param partnerId
     * @return
     */
    @RequestMapping("addApplyGroup")
    public String addApplyGroup(Model model, Long partnerId,String partnerName) {
        QueryDto<PriceRuleCondition> queryDto = new QueryDto<>(1,9999);
        PriceRuleCondition condition = new PriceRuleCondition();
        queryDto.setCondition(condition);
        Page<CarPriceRuleGroupDto> groupList = carPriceRuleService.groupList(queryDto).getData();
//        model.addAttribute("groupList",groupList.getList());

        condition.setPartnerId(partnerId);
        Page<PriceRuleApplyDto> applyList = priceRuleApplyService.applyList(queryDto).getData();
//        model.addAttribute("applyList",applyList.getList());

        List<String> app = new ArrayList<>();
        for(PriceRuleApplyDto apply : applyList.getList()){
            app.add(apply.getGroupId().toString());
        }


        List<PriceRuleApplyDto> groupApplyList = new ArrayList<>();
        for(CarPriceRuleGroupDto group : groupList.getList()){
            if(app.contains(group.getId().toString())){
                continue;
            }
            PriceRuleApplyDto form = new PriceRuleApplyDto();
            form.setGroupId(group.getId().intValue());
            form.setGroupName(group.getName());
            form.setPartnerId(partnerId);
            form.setPartnerName(partnerName);
//            for(PriceRuleApplyDto apply : applyList.getList()){
//                if(apply.getGroupId().intValue() == group.getId().intValue()){
//                    form.setApplyId(apply.getId());
//                    form.setEndTime(apply.getEndTime());
//                    form.setStartTime(apply.getStartTime());
//                    form.setPriority(apply.getPriority());
//                }
//            }
            groupApplyList.add(form);
        }
        model.addAttribute("groupApplyList",groupApplyList);
        model.addAttribute("partnerName",partnerName);
        return "car/price/apply_group_detail.jsp";
    }

    /**
     * 保存企业分配价格策略组
     * @param forms
     * @return
     */
    @RequestMapping("saveApplyGroup")
    @ResponseBody
    public ResponseInfo saveApplyGroup(String forms){
        ResponseInfo responseInfo = null;
        List<PriceRuleApplyDto> list = JSON.parseArray(forms,PriceRuleApplyDto.class);
        for(PriceRuleApplyDto apply : list){
            responseInfo = priceRuleApplyService.addOrUpdateApply(apply);
            if(!responseInfo.isSuccess()){
                return responseInfo;
            }
        }
        return responseInfo;
    }
}
