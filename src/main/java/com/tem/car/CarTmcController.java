package com.tem.car;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.tem.car.api.service.order.ICarOrderService;
import com.tem.car.dto.CarOrderDto;
import com.tem.car.dto.CarOrderOperationLog;
import com.tem.car.dto.query.CarOrderCondition;
import com.tem.car.dto.result.OrderDetailDto;
import com.tem.payment.api.BpAccTradeRecordService;
import com.tem.platform.action.BaseController;
import com.tem.pss.api.task.OrderTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by heyuanjing on 18/7/13.
 */
@Controller
@RequestMapping("car/tmc")
public class CarTmcController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(CarTmcController.class);

    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private ICarOrderService carOrderService;
    @Autowired
    private BpAccTradeRecordService bpAccTradeRecordService;

    /**
     * tmc 首页
     */
    @RequestMapping("index")
    public String index() {
        return "car/tmc/index.jsp";
    }

    /**
     * 订单列表
     *
     * @return
     */
    @RequestMapping("orderList")
    public String orderList() {
        return "car/tmc/orderList.jsp";
    }

    @RequestMapping(value = "orderTable", method = RequestMethod.POST)
    public String orderTable(CarOrderCondition condition, Integer pageIndex, Integer pageSize, Model model) {
        condition.setTmcId(super.getTmcId());
        QueryDto<CarOrderCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(condition);
        Page<CarOrderDto> pageList = carOrderService.queryListWithPage(queryDto).getData();
//        logger.info("pageList:" + JSONObject.toJSONString(pageList));

//        model.addAttribute("orderStatuslist", getOrderCountList(queryDto));
        model.addAttribute("pageList", pageList);
//        model.addAttribute("currentOrderStatus",currentOrderStatus);
//
//        //任务
//        List<OrderTaskDto> inOrWaitTaskList = orderTaskService.findTaskByStatusAndBizType(TaskStatus.WAIT_PROCESS, OrderBizType.CAR);
//        inOrWaitTaskList.addAll(orderTaskService.findTaskByStatusAndBizType(TaskStatus.PROCESSING, OrderBizType.HOTEL));
//        if (CollectionUtils.isNotEmpty(pageList.getList())) {
//            for (HotelOrderDto h: pageList.getList()) {
//                for (OrderTaskDto t: inOrWaitTaskList) {
//                    if (h.getId().equals(t.getOrderId())) {
//                        String operatorName = t.getOperatorName() == null ? "" : "/"+t.getOperatorName();
//                        h.setTaskInfo( t.getTaskStatus().getMessage() + "/" + t.getTaskType().getMessage() + operatorName);
//                        break;
//                    }
//                }
//            }
//        }

        return "car/tmc/orderTable.jsp";
    }

    /**
     * 明细
     * @param orderId
     * @return
     */
    @RequestMapping("getIncomeDetail")
    public String getIncomeDetail(String orderId, Model model){
        OrderDetailDto orderDto = carOrderService.orderDetail(orderId).getData();
        model.addAttribute("incomeDetail",bpAccTradeRecordService.listByOrderIdAndPlanId(orderDto.getOrder().getId(), Long.valueOf(orderDto.getOrder().getPaymentPlanNo())));
        return "car/tmc/incomeDetail.single";
    }

    /**
     * 日志
     * @param orderId
     * @return
     */
    @RequestMapping("getOrderLog")
    public String getOrderLog(String orderId, Model model){
        List<CarOrderOperationLog> logList = carOrderService.findOrderLogList(orderId).getData();
        model.addAttribute("logList", logList);
        return "car/tmc/log.single";
    }
}
