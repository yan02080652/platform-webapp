package com.tem.car.priceRule.form;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class PriceRuleGroupMainFrom <T> implements Serializable {
    private String name;
    private String createUser;
    private Date createDate;
    private String updateUser;
    private Date updateTime;
    private Map<String,PriceRuleGroupFrom<T>> map;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Map<String, PriceRuleGroupFrom<T>> getMap() {
        return map;
    }

    public void setMap(Map<String, PriceRuleGroupFrom<T>> map) {
        this.map = map;
    }
}
