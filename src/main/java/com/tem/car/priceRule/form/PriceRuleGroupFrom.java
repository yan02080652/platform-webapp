package com.tem.car.priceRule.form;

import com.iplatform.common.Page;

import java.io.Serializable;

public class PriceRuleGroupFrom <T>  implements Serializable {
    private String channelCode;
    private String channelName;
    private Page<T> page;

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }
    public String getChannelName() {
        return channelName;
    }
    public PriceRuleGroupFrom(){
        page=new Page<>();
    }
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public Page<T> getPage() {
        return page;
    }

    public void setPage(Page<T> page) {
        this.page = page;
    }


}
