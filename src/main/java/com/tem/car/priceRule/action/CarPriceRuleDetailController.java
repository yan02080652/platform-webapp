package com.tem.car.priceRule.action;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.car.ResponseDto;
import com.tem.car.api.service.ICarPriceRuleService;
import com.tem.car.api.service.hub.ICityService;
import com.tem.car.dto.CarCityDto;
import com.tem.car.dto.enums.CarType;
import com.tem.car.dto.enums.OriginType;
import com.tem.car.dto.enums.SupplierType;
import com.tem.car.dto.priceRule.CarPriceRuleDto;
import com.tem.car.dto.query.CarCityQuery;
import com.tem.car.dto.query.GetCityIdQuery;
import com.tem.car.priceRule.form.PriceRuleFrom;
import com.tem.car.priceRule.form.PriceRuleGroupFrom;
import com.tem.platform.action.BaseController;
import com.tem.platform.form.ResponseTable;
import com.tem.pss.dto.PriceRuleCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;


@Controller
@RequestMapping("/car/priceRule/detail")
public class CarPriceRuleDetailController extends BaseController {

    public static final String FAILED = "-1";
    public static final String SUCCESS = "1";

    @Autowired
    private ICarPriceRuleService carPriceRuleService;

    @Autowired
    private ICityService cityService;
    @RequestMapping(value = "")
    public String index(Model model) {
        return "car/priceRule/index.base";
    }

    /**
     * 获取策略细则信息
     * @param condition
     * @param hubsCode 渠道商
     * @param groupId  策略组id
     * @param supplierCode 供货商
     * @param status      策略状态是否启用
     * @param pageIndex
     * @return
     */
    @RequestMapping(value="list")
    public String list(PriceRuleCondition condition,String hubsCode,Long groupId,String supplierCode,String status, Integer pageIndex, Model model){
        PriceRuleGroupFrom<CarPriceRuleDto> priceRuleDto=new PriceRuleGroupFrom<>();
        priceRuleDto.setChannelCode(hubsCode);
        priceRuleDto.setChannelName(OriginType.valueOf(hubsCode).getName());

        QueryDto<PriceRuleCondition> queryDto = new QueryDto<>(pageIndex, 10);
        condition.setGroupId(groupId);
        condition.setChannelCode(hubsCode);
        condition.setSupplierCode(supplierCode);
        if (status != null && !"".equals(status)) {
            condition.setRuleStatus("1".equals(status) ? true : false);
        }
        queryDto.setCondition(condition);
        Page<CarPriceRuleDto> page = carPriceRuleService.ruleList(queryDto).getData();
        model.addAttribute("carTypes",CarType.values());
        model.addAttribute("supplierList",SupplierType.values());
        model.addAttribute("priceRuleDto", priceRuleDto);
        model.addAttribute("pageList", page);

        return "car/priceRule/priceRuleList.jsp";
    }

    /**
     * 新增/修改获取策略
     * @param model
     * @param id 策略id
     * @param channelCode 渠道code
     * @return
     */
    @RequestMapping(value="priceRuleDetail",method = RequestMethod.GET)
    public String getPriceRuleDetail(Model model,Integer id,String channelCode,@RequestParam(required = false)Integer iscopy){
        PriceRuleFrom priceRuleDto=new PriceRuleFrom();
        Map<Integer,String> carTypeMap=new HashMap<>();
        List<CarCityDto> cityList = new ArrayList<>();
        if(id!=null){
            ResponseInfo res=carPriceRuleService.getRule(Long.valueOf(id)).transfer();
            if(res.isSuccess()) {
                CarPriceRuleDto dto=res.getData();
                priceRuleDto=TransformUtils.transform(dto,PriceRuleFrom.class);
                if(!StringUtils.isBlank(priceRuleDto.getCityId())){
                    List<CarCityDto> cityDtoList = cityService.getCitysBySysCityIds(priceRuleDto.getCityId(),OriginType.goUp.toString()).getData();
                    if(CollectionUtils.isNotEmpty(cityDtoList)) {
                        for (CarCityDto carCity : cityDtoList) {
                            CarCityDto carCityDto = new CarCityDto();
                            carCityDto.setCityId(carCity.getSysCityId() + "");
                            carCityDto.setName(carCity.getSysCityName());
                            cityList.add(carCityDto);
                        }
                    }
                }

            }
        }

        priceRuleDto.setChannelName(OriginType.valueOf(channelCode).getName());
        priceRuleDto.setChannelCode(channelCode);
        for (CarType carType:CarType.values()){
            carTypeMap.put(carType.getCode(),carType.getMessage());
        }
        priceRuleDto.setCarTypes(carTypeMap);
        //拷贝
        if(iscopy!=null&&iscopy==1){
            priceRuleDto.setId(null);
            priceRuleDto.setPriority(priceRuleDto.getPriority()==null?null:priceRuleDto.getPriority()+1);
        }
        model.addAttribute("cityList", JSONObject.toJSON(cityList));
        model.addAttribute("supplierList",SupplierType.values());
        model.addAttribute("priceRuleDto",priceRuleDto);

        return "car/priceRule/priceRuleDetail.jsp";
    }
    /**
     * 城市分页查询
     * @return
     */
    @ResponseBody
    @RequestMapping("getCityData")
    public ResponseTable getCityData(Integer pageIndex, Integer pageSize, CarCityQuery condition){
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }
        QueryDto<CarCityQuery> queryDto = new QueryDto<>(pageIndex,pageSize);
        queryDto.setCondition(condition);
        ResponseDto<Page<CarCityDto>> responseDto=cityService.queryPageList(queryDto);
        System.out.println("responseDto :" +JSONObject.toJSON(responseDto));
        Page<CarCityDto> carCityDtoPage = responseDto.getData();
        if(CollectionUtils.isNotEmpty(carCityDtoPage.getList())){
            for (CarCityDto carCityDto:carCityDtoPage.getList()) {
                carCityDto.setName(carCityDto.getSysCityName());
                carCityDto.setCityId(carCityDto.getSysCityId()+"");
            }
        }
        return new ResponseTable(carCityDtoPage);
    }
    /**
     * 保存策略
     * @param
     * @return
     */
    @ResponseBody
    @RequestMapping(value="save")
    public Message saveRule(PriceRuleFrom priceRuleFrom ){
        CarPriceRuleDto priceRuleDto=TransformUtils.transform(priceRuleFrom,CarPriceRuleDto.class);
        if(priceRuleDto.getId()==null){
            priceRuleDto.setCreateDate(new Date());
            priceRuleDto.setCreateUser(super.getCurrentUser().getFullname());
        }else{
            priceRuleDto.setUpdateUser(super.getCurrentUser().getFullname());
            priceRuleDto.setUpdateTime(new Date());
        }
        ResponseInfo res=carPriceRuleService.addOrUpdateRule(priceRuleDto).transfer();
        if("0".equals(res.getCode())) {
            if(priceRuleDto.getId()==null) {
                return Message.success("添加成功");
            }else{
                return Message.success("修改成功");
            }
        }else{
            if(priceRuleDto.getId()==null) {
                return Message.error("添加失败");
            }else{
                return Message.error("修改失败");
            }
        }
    }

    /**
     * 设置策略状态
     * @param id
     * @param status
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "setStatus",method=RequestMethod.POST)
    public Message setPriceRuleStatus(Long id,Boolean status) {
        CarPriceRuleDto priceRuleDto=new CarPriceRuleDto();
        priceRuleDto.setId(id);
        priceRuleDto.setStatus(status);
        ResponseInfo res=carPriceRuleService.addOrUpdateRule(priceRuleDto).transfer();
        if("0".equals(res.getCode())) {
            return Message.success("修改成功");
        }else{
            return Message.error("修改失败");
        }

    }

    /**
     * 删除策略
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "deleted",method = RequestMethod.POST)
    public Message priceRuleDelete(Long id){
        ResponseInfo res=carPriceRuleService.delRule(id).transfer();
        if("0".equals(res.getCode())) {
            return Message.success("删除成功");
        }else{
            return Message.error("删除失败");
        }
    }


}
