package com.tem.car.priceRule.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.web.Message;
import com.tem.car.api.service.ICarPriceRuleService;
import com.tem.car.api.service.hub.ICityService;
import com.tem.car.dto.enums.OriginType;
import com.tem.car.dto.priceRule.CarPriceRuleGroupDto;
import com.tem.platform.action.BaseController;
import com.tem.pss.api.PriceRuleApplyService;
import com.tem.pss.dto.PriceRuleApplyDto;
import com.tem.pss.dto.PriceRuleCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;



@Controller
@RequestMapping("/car/priceRule")
public class CarPriceRuleController extends BaseController {

    public static final String FAILED = "-1";
    public static final String SUCCESS = "1";

    @Autowired
    private ICarPriceRuleService carPriceRuleService;
    @Autowired
    private PriceRuleApplyService priceRuleApplyService;

    @Autowired
    private ICityService cityService;

    @RequestMapping(value = "")
    public String index(Model model) {
        return "car/priceRule/group.base";
    }

    /**
     * 获取用车价格策略组列表
     *
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    public String list(PriceRuleCondition condition, Integer pageIndex, Model model) {
        QueryDto<PriceRuleCondition> queryDto = new QueryDto<>(pageIndex, 10);
        queryDto.setCondition(condition);
        Page<CarPriceRuleGroupDto> page = carPriceRuleService.groupList(queryDto).getData();
        model.addAttribute("pageList", page);
        return "car/priceRule/groupList.jsp";
    }

    //删除策略组
    @ResponseBody
    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public Message delete(String id) {
        ResponseInfo res = carPriceRuleService.delGroup(Long.valueOf(id)).transfer();
        if ("0".equals(res.getCode())) {
            return Message.success("删除成功");
        }
        return Message.error("删除失败，请刷新重试");
    }

    /**
     * 新增或修改获取策略组
     *
     * @param id
     * @return
     */
    @RequestMapping("addGroup")
    public String addGroup(Long id, Model model) {
        CarPriceRuleGroupDto priceRuleGroupDto = null;
        if (id == null) {
            priceRuleGroupDto = new CarPriceRuleGroupDto();
        } else {
            priceRuleGroupDto = carPriceRuleService.getGroup(id).getData();
        }
        model.addAttribute("priceRuleGroupDto", priceRuleGroupDto);
        return "car/priceRule/groupDetail.jsp";
    }

    ///新增修改策略组
    @ResponseBody
    @RequestMapping(value = "save")
    public ResponseInfo saveGroup(CarPriceRuleGroupDto groupDto) {
        boolean isAdd = false;
        ResponseInfo res = carPriceRuleService.getGroup(groupDto.getId()).transfer();
        if (res.getData() == null) {
            isAdd = true;
        }
        if (isAdd) {
            groupDto.setCreateUser(super.getCurrentUser().getFullname());
        } else {
            groupDto.setUpdateUser(super.getCurrentUser().getFullname());
        }
        res = carPriceRuleService.addOrUpdateGroup(groupDto).transfer();
        if (res.isSuccess()) {
            if (isAdd) {
                return new ResponseInfo(SUCCESS, "新增成功", "");
            }
            return new ResponseInfo(SUCCESS, "修改成功", "");
        } else {
            if (isAdd) {
                return new ResponseInfo(FAILED, "新增失败", "");
            }
            return new ResponseInfo(FAILED, "修改失败", "");
        }
    }

    /**
     * 获取策略组主信息
     * @param id  策略组id
     * @return
     */
    @RequestMapping(value = "priceRuleGroup")
    public String getPriceRuleGroup(@RequestParam(required = false) Long id, Model model) {
        CarPriceRuleGroupDto piceRuleGroupMain = carPriceRuleService.getGroup(id).getData();
        model.addAttribute("piceRuleGroupMain", piceRuleGroupMain);
        model.addAttribute("hubsCode", OriginType.values());
        return "car/priceRule/priceRule.base";
    }


    /**
     * 获取价格策略组适用企业列表表头
     *
     * @return
     */
    @RequestMapping(value="{id}/applyPartner/index")
    public String applyPartner(@PathVariable Long id, Model model) {
        model.addAttribute("priceRuleGroupDto", carPriceRuleService.getGroup(id).getData());
        return "car/priceRule/applyPartner.base";
    }


    /**
     * 获取价格策略组适用企业列表
     *
     * @return
     */
    @RequestMapping(value="{id}/applyPartner/list")
    public String getApplyPartnerList(PriceRuleCondition priceRuleCondition, @PathVariable Long id, @RequestParam(required = false) Long partnerId, Integer pageIndex, Model model) {
        priceRuleCondition.setGroupId(id);
        priceRuleCondition.setPartnerId(partnerId);
        QueryDto<PriceRuleCondition> query = new QueryDto<>(pageIndex, 10);
        query.setCondition(priceRuleCondition);
        Page<PriceRuleApplyDto> page = priceRuleApplyService.applyList(query).getData();
        model.addAttribute("pageList", page);
        return "car/priceRule/applyPartnerList.jsp";
    }
}
