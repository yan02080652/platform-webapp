package com.tem.car;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.tem.car.api.service.order.ICarOrderService;
import com.tem.car.dto.enums.CarOrderShowStatus;
import com.tem.car.dto.enums.OriginType;
import com.tem.car.dto.query.CmsOrderListQuery;
import com.tem.car.dto.result.CmsOrderList;
import com.tem.order.action.RefundOrderListController;
import com.tem.order.form.AllOrderQueryForm;
import com.tem.order.form.CarOrderSummaryForm;
import com.tem.order.util.ExportInfo;
import com.tem.platform.action.BaseController;
import com.tem.pss.enums.TravelTypeEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 用车
 * Created by heyuanjing on 18/1/4.
 */
@Controller
@RequestMapping("car")
public class CarController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(CarController.class);

    @Autowired
    private ICarOrderService carOrderService;
    
    @Autowired
    private RefundOrderListController refundOrderListController;

    /**
     * 订单列表
     */
    @RequestMapping(value = "order", method = RequestMethod.GET)
    public String orderList(Model model) {
        model.addAttribute("orderStatus", CarOrderShowStatus.values());
        model.addAttribute("origin", OriginType.values());
        return "order/car/carOrder.base";
    }

    @RequestMapping(value = "carOrderList", method = RequestMethod.POST)
    public String carOrderList(Model model, Integer pageIndex, Integer pageSize, AllOrderQueryForm condition) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()) {
            condition.setTmcId(super.getTmcId().toString());
        }

        QueryDto<CmsOrderListQuery> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(toCarCondition(condition));

        Page<CmsOrderList> pageList = carOrderService.getCmsPageList(queryDto).getData();
        model.addAttribute("pageList", pageList);

        model.addAttribute("currentUserId", super.getCurrentUser().getId());
        return "order/car/carOrderList.jsp";
    }

    private CmsOrderListQuery toCarCondition(AllOrderQueryForm condition) {
        CmsOrderListQuery query = new CmsOrderListQuery();
        query.setTmcId(condition.getTmcId());
        query.setCompPay(condition.isCompPay());
        query.setEndDate(condition.getEndDate());
        query.setIssueChannel(condition.getIssueChannel());
        query.setKeyword(condition.getKeyword());
        query.setOrderStatus(condition.getOrderStatus());
        query.setQueryBpId(condition.getQueryBpId());
        query.setStartDate(condition.getStartDate());
        query.setType(condition.getType());
        if(condition.getTravelType() !=null ){
        	query.setTravelType(condition.getTravelType());
        }
        return query;
    }
    
    @RequestMapping("exportCar")
    public void exportCar(AllOrderQueryForm condition){
        List<ExportInfo> infoList = new ArrayList<>();

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }
        
        QueryDto<CmsOrderListQuery> queryDto = new QueryDto<>(1, Integer.MAX_VALUE);
        queryDto.setCondition(toCarCondition(condition));
        
        Page<CmsOrderList> pageList = carOrderService.getCmsPageList(queryDto).getData();
        
        List<CarOrderSummaryForm> dateSet = TransformUtils.transformList(pageList.getList(),CarOrderSummaryForm.class);
      
        String[] carHeader = {"订单号","行程情况","预订人","下单时间","所属客户","客户结算价"
                ,"所属供应商","供应商结算价","状态"};

        ExportInfo<CarOrderSummaryForm> carExport = new ExportInfo<>();
        carExport.setDataset(dateSet);
        carExport.setHeaders(carHeader);
        carExport.setTitle("用车订单");
        infoList.add(carExport);

        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String fileName = "用车订单" + uuid +  ".xlsx";

        refundOrderListController.goToExport(infoList, fileName, getRequest(), getResponse());
    }
}
