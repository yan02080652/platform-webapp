package com.tem.car;

import com.tem.car.api.service.order.ICarOrderService;
import com.tem.car.dto.result.OrderDetailDto;
import com.tem.platform.action.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by heyuanjing on 18/7/16.
 */
@Controller
@RequestMapping("car/order")
public class CarOrderDetailController extends BaseController{

    @Autowired
    private ICarOrderService carOrderService;

    /**
     * 订单详情
     * @param orderId
     */
    @RequestMapping("detail/{orderId}")
    public String orderDetail(@PathVariable String orderId, Model model){
        OrderDetailDto order = carOrderService.orderDetail(orderId).getData();
        model.addAttribute("order",order);
        return "car/order/carDetail.base";
    }
}
