package com.tem.car;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.utils.LogUtils;
import com.iplatform.common.web.Message;
import com.tem.car.api.service.hub.ICityService;
import com.tem.car.dto.CarCityDto;
import com.tem.car.dto.query.CarCityQuery;
import com.tem.platform.action.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("/car/district")
public class CarDistrictController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(CarDistrictController.class);

    @Autowired
    private ICityService cityService;
    @RequestMapping("")
    public String index(Integer pageIndex,String name, Boolean isMatching, Model model){
        model.addAttribute("name",name);
        model.addAttribute("isMatching",isMatching);
        return "/car/district/index.base";
    }

    /**
     * 获取用车城市列表
     * @param pageIndex
     * @return
     */
    @RequestMapping(value = "list",method = RequestMethod.POST)
    public String getCityList(CarCityQuery condition,Integer pageIndex, Model model){

        QueryDto<CarCityQuery> queryDto=new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);
        queryDto.setCondition(condition);
        Page<CarCityDto> page=cityService.queryPageList(queryDto).getData();
        model.addAttribute("pageList", page);
        return "car/district/districtTable.jsp";
    }

    /**
     * 获取用车城市详情
     * @return
     */
    @RequestMapping(value="getDistrict")
    public String getCityById(Integer id,Boolean isMatching, Model model){
        CarCityDto carCityDto=cityService.getCityById(id).getData();
        model.addAttribute("district",carCityDto);
        model.addAttribute("isMatching",isMatching);
        return "car/district/districtModel.jsp";
    }
    /**
     * 修改用车城市
     * @param carCityDto
     * @return
     */
    @ResponseBody
    @RequestMapping("update")
    public Message update(CarCityDto carCityDto){
        if(carCityDto.getId() != null){
            carCityDto.setStatus(true);
        }
        Boolean res=cityService.updateCity(carCityDto).getData();
        return res?Message.success("修改成功"):Message.error("修改失败");
    }

    /**
     * 批量同步用车城市在途行政区划信息
     * @return
     */
    @ResponseBody
    @RequestMapping("batchUpdate")
    public boolean batchInsert() {
        try {
            Date start = new Date();
            cityService.syncCity();
            Date end = new Date();
            LogUtils.info(logger,"更新城市耗时{}",end.getTime()-start.getTime());
        } catch (Exception e) {
            LogUtils.error(logger, "同步城市数据异常,信息:{}", e);
        }
        return true;
    }




}
