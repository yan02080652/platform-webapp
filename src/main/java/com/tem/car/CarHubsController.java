package com.tem.car;

import com.iplatform.common.web.Message;
import com.tem.car.api.service.ICarHubsService;
import com.tem.car.dto.enums.SupplierType;
import com.tem.car.dto.query.CarHubsConnectQuery;
import com.tem.car.dto.result.CarHubsConnectDto;
import com.tem.car.dto.result.CarHubsDto;
import com.tem.supplier.api.SettleChannelService;
import com.tem.supplier.dto.SettleChannelDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 用车 hubs 设置
 * Created by heyuanjing on 18/1/12.
 */
@Controller
@RequestMapping("car/hubs")
public class CarHubsController extends BaseController{

    @Autowired
    private ICarHubsService carHubsService;
    @Autowired
    private PartnerService partnerService;
    @Autowired
    private IssueChannelService issueChannelService;
    @Autowired
    private SettleChannelService settleChannelService;

    @RequestMapping("")
    public String index(Model model){
        List<CarHubsDto> dtos = carHubsService.getAll().getData();
        model.addAttribute("hubsDtos",dtos);
        return "car/hubs/hubs.base";
    }

    /**
     * 详情
     * @param code
     * @return
     */
    @RequestMapping(value = "/hubsDetail", method = RequestMethod.GET)
    public String hubsDetail(String code, Model model) {
        CarHubsDto hubsDto = null;
        if(StringUtils.isNotEmpty(code)){
            hubsDto = carHubsService.getByCode(code).getData();
            model.addAttribute("editMode", "update");
        }else{
            hubsDto = new CarHubsDto();
            model.addAttribute("editMode", "add");
        }
        model.addAttribute("hubsDto", hubsDto);
        model.addAttribute("supplierType", SupplierType.values());
        return "car/hubs/hubsDetail.jsp";
    }

    /**
     * 加载右侧栏
     * @param code
     * @return
     */
    @RequestMapping(value = "loadRight", method = RequestMethod.GET)
    public String loadRight(String code, Model model) {
        CarHubsDto hubsDto = carHubsService.getByCode(code).getData();
        CarHubsConnectQuery query=new CarHubsConnectQuery();
        query.setHubsCode(code);
        List<CarHubsConnectDto> hubsConnectDtos=carHubsService.findConnect(query).getData();
        model.addAttribute("hubsDto", hubsDto);
        model.addAttribute("hubsConnectDtos",hubsConnectDtos);
        return "car/hubs/hubsRight.jsp";
    }

    /**
     * 保存 hub
     * @param carHubsDto
     * @param editMode
     */
    @ResponseBody
    @RequestMapping(value = "saveHub",method = RequestMethod.POST)
    public Message save(CarHubsDto carHubsDto, String editMode){
        ResponseDto<Void> result = null;
        if ("add".equals(editMode)) {
            result = carHubsService.addHub(carHubsDto);
        }else{
            result = carHubsService.updateHub(carHubsDto);
        }
        if(result.isSuccess()){
            return Message.success("修改成功");
        }else{
            return Message.success("修改失败:" + result.getMsg());
        }
    }

    /**
     * 新增或修改连接器
     * @return
     */
    @RequestMapping(value = "hubsConnectDetail", method = RequestMethod.GET)
    public String hubsConnectDetail(String hubsCode,Integer id, Model model) {
        //酒店渠道列表
        List<IssueChannelDto> issueChannelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(), IssueChannelType.CAR.getType(), LineType.ON_LINE.getType());

        CarHubsConnectDto carHubsConnectDto=null;
        if(id==null){
            carHubsConnectDto=new CarHubsConnectDto();
            carHubsConnectDto.setHubsCode(hubsCode);
        }else{
            carHubsConnectDto=carHubsService.getConnectById(id).getData();
            List<IssueChannelDto> issueChannelDtos=issueChannelService.findListByTmcIdAndChannelTypeAndLineType(Long.valueOf(carHubsConnectDto.getTmcId()),IssueChannelType.CAR.name(),LineType.ON_LINE.getType());
            if(CollectionUtils.isNotEmpty(issueChannelDtos)){
                for ( IssueChannelDto issueChannelDto:issueChannelDtos){
                    if(issueChannelDto.getCode().equals(carHubsConnectDto.getChannelCode())){
                        model.addAttribute("settleChannelName",settleChannelService.getById(issueChannelDto.getSettelChannelId()));
                        break;
                    }
                }
            }
            PartnerDto tmcPartner = partnerService.findById(Long.valueOf(carHubsConnectDto.getTmcId()));
            model.addAttribute("tmcPartner", tmcPartner);
        }
        model.addAttribute("hubsConnectDto", carHubsConnectDto);
        model.addAttribute("issueChannelList", issueChannelList);
        return "car/hubs/hubsConnectDetail.jsp";
    }

    @ResponseBody
    @RequestMapping(value="addOrUpdateHubsConnect",method = RequestMethod.POST)
    public Message addOrUpdateHubsConnect(@ModelAttribute("hubsConnectDto") @Validated CarHubsConnectDto hubsConnectDto,
                                         BindingResult br){
        if(br.hasErrors()){
            return Message.error("数据校验不通过。");
        }
        if(hubsConnectDto.getId()==null){
            if(carHubsService.addOrUpdateConnect(hubsConnectDto).getData()){
                return Message.success("添加成功。");
            }else{
                return Message.error("添加失败。");
            }
        }else{
            if(carHubsService.addOrUpdateConnect(hubsConnectDto).getData()){
                return Message.success("修改成功。");
            }else{
                return Message.error("修改失败。");
            }
        }
    }

    /**
     * 删除连接
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deleteHubsConnect")
    public Message deleteHubsConnect(@RequestParam Integer id) {
        if (carHubsService.deleteConnect(id).getData()) {
            return Message.success("删除成功");
        }
        return Message.error("删除失败");
    }

    /**
     * 获取该 tmc 可以选择的线上渠道
     * @param tmcId
     */
    @RequestMapping("getChannels")
    @ResponseBody
    public List<IssueChannelDto> getChannels(String tmcId){
        return issueChannelService.findListByTmcIdAndChannelTypeAndLineType(Long.valueOf(tmcId),IssueChannelType.CAR.name(),LineType.ON_LINE.getType());
    }

    /**
     * 获取收支渠道
     * @param settelChannelId
     */
    @RequestMapping("getSettleChannel")
    @ResponseBody
    public SettleChannelDto getSettleChannels(Long settelChannelId){
        return settleChannelService.getById(settelChannelId);
    }
}
