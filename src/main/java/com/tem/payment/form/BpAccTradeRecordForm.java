package com.tem.payment.form;

import java.io.Serializable;
import java.util.Date;

/**
 * 企业账户往来明
 * @author zhangtao
 *
 */
public class BpAccTradeRecordForm implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 往来流水号
	 */
    private Long id;

    /**
     * 账户Id
     */
    private Long accountId;

    /**
     * 业务类型
     */
    private String bizType;

    /**
     * 子类型
     */
    private String subType;

    /**
     * 关联单据。取值：C_支付计划号、O_订单号
     */
    private String billId;
    
    /**
     * 交易说明
     */
    private String tradeInstruction;

    /**
     * 交易金额
     */
    private Long amount;

    /**
     * 余额
     */
    private Long balance;

    /**
     * 交易时间
     */
    private Date tradeTime;

    /**
     * 记录人
     */
    private Long createUser;
    
    private String createUserName;

    /**
     * 记录时间
     */
    private Date createDate;

    private String memo;

    /**
     * 往来对账标记
     */
    private String flag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType == null ? null : bizType.trim();
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType == null ? null : subType.trim();
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId == null ? null : billId.trim();
    }
    
	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

	public Date getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(Date tradeTime) {
        this.tradeTime = tradeTime;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

	public String getTradeInstruction() {
		return tradeInstruction;
	}

	public void setTradeInstruction(String tradeInstruction) {
		this.tradeInstruction = tradeInstruction;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
    
}