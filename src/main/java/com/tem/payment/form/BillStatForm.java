package com.tem.payment.form;

import java.io.Serializable;

public class BillStatForm implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;

    /**
     * 账单id
     */
    private Long billId;

    /**
     * 枚举 OrderBizType
     */
    private String biz;
    
    private String bizName;

    /**
     * 金额
     */
    private Integer amount;

    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public String getBiz() {
        return biz;
    }

    public void setBiz(String biz) {
        this.biz = biz == null ? null : biz.trim();
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

	public String getBizName() {
		return bizName;
	}

	public void setBizName(String bizName) {
		this.bizName = bizName;
	}
    
}