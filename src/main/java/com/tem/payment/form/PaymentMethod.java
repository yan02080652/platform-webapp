package com.tem.payment.form;

import java.io.Serializable;

/**
 * Created by chenjieming on 2017/5/26.
 */
public class PaymentMethod implements Serializable {

    private String channel;

    private Integer count;

    private String amount;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public PaymentMethod(String channel, Integer count, String amount) {
        this.channel = channel;
        this.count = count;
        this.amount = amount;
    }
}
