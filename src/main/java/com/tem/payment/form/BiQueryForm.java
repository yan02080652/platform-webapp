package com.tem.payment.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 报表查询参数
 *
 * @author chenjieming
 *
 */
public class BiQueryForm implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 *  企业id
	 */
	private Long partnerId;
	/**
	 *  起始时间
	 */
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date beginDate;
	/**
	 *  截止时间
	 */
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date endDate;

	//分页
	private Integer pageIndex;

	private String partnerName;

	private String tmcId;
	//出行性质
	private String travelType;

	public BiQueryForm() {
	}

	public BiQueryForm(Long partnerId, Date beginDate, Date endDate) {
		this.partnerId = partnerId;
		this.beginDate = beginDate;
		this.endDate = endDate;
	}

	public String getTmcId() {
		return tmcId;
	}

	public void setTmcId(String tmcId) {
		this.tmcId = tmcId;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getTravelType() {
		return travelType;
	}

	public void setTravelType(String travelType) {
		this.travelType = travelType;
	}
	
}
