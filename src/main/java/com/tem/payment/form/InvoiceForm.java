package com.tem.payment.form;

import java.io.Serializable;

public class InvoiceForm implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 所属partnerId
	 */
	private Long partnerId;
	
	/**
	 * 受票方法人Id
	 */
	private Long invoiceDestCorpid;
	
	/**
	 * 物流状态
	 */
	private String	shippingStatus;
	
	/**
	 * 账单Id
	 */
	private Long billId;
	
	/**
	 * 受票方
	 */
	private String invoiceDest;
	
	private String liqStatus;
	
	//=====================
	
	/**
	 * 企业名称
	 */
	private String bpName;
	
	private Long tmcId;

	private String bpTmcName;

	public String getBpTmcName() {
		return bpTmcName;
	}

	public void setBpTmcName(String bpTmcName) {
		this.bpTmcName = bpTmcName;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public Long getTmcId() {
		return tmcId;
	}

	public void setTmcId(Long tmcId) {
		this.tmcId = tmcId;
	}

	public Long getInvoiceDestCorpid() {
		return invoiceDestCorpid;
	}

	public void setInvoiceDestCorpid(Long invoiceDestCorpid) {
		this.invoiceDestCorpid = invoiceDestCorpid;
	}

	public String getShippingStatus() {
		return shippingStatus;
	}

	public void setShippingStatus(String shippingStatus) {
		this.shippingStatus = shippingStatus;
	}

	public String getBpName() {
		return bpName;
	}

	public void setBpName(String bpName) {
		this.bpName = bpName;
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public String getInvoiceDest() {
		return invoiceDest;
	}

	public void setInvoiceDest(String invoiceDest) {
		this.invoiceDest = invoiceDest;
	}

	public String getLiqStatus() {
		return liqStatus;
	}

	public void setLiqStatus(String liqStatus) {
		this.liqStatus = liqStatus;
	}
	
	


}
