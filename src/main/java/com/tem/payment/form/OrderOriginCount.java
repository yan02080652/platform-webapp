package com.tem.payment.form;

/**
 * Created by chenjieming on 2017/5/26.
 */
public class OrderOriginCount {

    private String origin;

    private Integer count;

    private String rate;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public OrderOriginCount(String origin, Integer count, String rate) {
        this.origin = origin;
        this.count = count;
        this.rate = rate;
    }
}
