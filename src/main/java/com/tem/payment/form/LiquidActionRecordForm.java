package com.tem.payment.form;

import java.io.Serializable;
import java.util.List;

public class LiquidActionRecordForm implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 付款记录
	 */
	private Long paymentId;
	
	/**
	 * 账单
	 */
	private	List<BillForm> billFrom;

	public Long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	public List<BillForm> getBillFrom() {
		return billFrom;
	}

	public void setBillFrom(List<BillForm> billFrom) {
		this.billFrom = billFrom;
	}
	

}
