package com.tem.payment.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RefundTaskForm implements Serializable{
	
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
    private Long id;

    /**
     * 退款Id
     */
    private Long payRefundId;

    /**
     * 退款渠道 alipay-支付宝;yeepay-易宝;account-账户等
     */
    private String refundChannel;

    /**
     * 退款发起人
     */
    private Long startUser;
    
    /**
     * 退款发起人
     */
    private String startUserName;

    /**
     * 退款金额
     */
    private BigDecimal amount;

    /**
     * 订单号
     */
    private String orderId;

    /**
     * 退款原因，字典项： PAY_REFUND_REASON 10出票失败 20主动退款
     */
    private String refundReason;
    
    /**
     * 退款原因转译
     */
    private String refundReasonName;
    
    /**
     * 0-未处理，1-已处理
     */
    private Integer status;
    
    private String statusName;

    /**
     * 退款方式 online-在线退款，offline-线下退款
     */
    private String refundType;

    /**
     * 线下退款的时候退款渠道
     */
    private String paymentChannel;

    /**
     * 支付账号
     */
    private String paymentAccount;

    /**
     * 支付流水号
     */
    private String paymentTradeId;

    /**
     * 受款方名称
     */
    private String paymentDestName;

    /**
     * 受款方账号
     */
    private String paymentDestAccount;
    
    private Date createDate;

    private Date updateDate;

    private Long updateUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPayRefundId() {
        return payRefundId;
    }

    public void setPayRefundId(Long payRefundId) {
        this.payRefundId = payRefundId;
    }

    public String getRefundChannel() {
        return refundChannel;
    }

    public void setRefundChannel(String refundChannel) {
        this.refundChannel = refundChannel == null ? null : refundChannel.trim();
    }

    public Long getStartUser() {
        return startUser;
    }

    public void setStartUser(Long startUser) {
        this.startUser = startUser;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

	public String getRefundReasonName() {
		return refundReasonName;
	}

	public void setRefundReasonName(String refundReasonName) {
		this.refundReasonName = refundReasonName;
	}

	public String getStartUserName() {
		return startUserName;
	}

	public void setStartUserName(String startUserName) {
		this.startUserName = startUserName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRefundType() {
		return refundType;
	}

	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}

	public String getPaymentChannel() {
		return paymentChannel;
	}

	public void setPaymentChannel(String paymentChannel) {
		this.paymentChannel = paymentChannel;
	}

	public String getPaymentAccount() {
		return paymentAccount;
	}

	public void setPaymentAccount(String paymentAccount) {
		this.paymentAccount = paymentAccount;
	}

	public String getPaymentTradeId() {
		return paymentTradeId;
	}

	public void setPaymentTradeId(String paymentTradeId) {
		this.paymentTradeId = paymentTradeId;
	}

	public String getPaymentDestName() {
		return paymentDestName;
	}

	public void setPaymentDestName(String paymentDestName) {
		this.paymentDestName = paymentDestName;
	}

	public String getPaymentDestAccount() {
		return paymentDestAccount;
	}

	public void setPaymentDestAccount(String paymentDestAccount) {
		this.paymentDestAccount = paymentDestAccount;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(Long updateUser) {
		this.updateUser = updateUser;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
    
}