package com.tem.payment.form;

import java.io.Serializable;
import java.util.Date;

/**
 * 账户变更记录表
 * @author zhangtao
 *
 */

public class BpAccountChangeLogForm implements Serializable {
    
	private static final long serialVersionUID = 1L;

	private Long id;

    /**
     * 账户id
     */
    private Long accountId;

    /**
     * 修改类型
     */
    private String changeType;

    /**
     * 旧的授信额度
     */
    private String oldValue;

    /**
     * 新的授信额度
     */
    private String newValue;

    /**
     * 修改人
     */
    private Long updateUser;
    
    private String updateUserName;

    /**
     * 修改时间
     */
    private Date updateDate;

    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType == null ? null : changeType.trim();
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue == null ? null : oldValue.trim();
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue == null ? null : newValue.trim();
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUserName() {
		return updateUserName;
	}

	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}