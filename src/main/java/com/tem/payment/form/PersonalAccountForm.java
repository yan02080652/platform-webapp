package com.tem.payment.form;

import java.io.Serializable;

public class PersonalAccountForm implements Serializable {

    /**
     * 提现记录Id
     */
    private Long withdrawId;

    /**
     * 个人账户Id
     */
    private Long accountId;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 原始金额
     */
    private Long orginAmount;

    private Double withdrawAmount;

    /**
     * 交易金额
     */
    private Long amount;

    /**
     * 交易类型
     */
    private String tradeType;

    /**
     * 关联单据:C_支付计划号、O_订单号、T_流水号、null
     */
    private String billId;

    /**
     * 记录人
     */
    private Long tradeUserId;

    /**
     * 备注
     */
    private	String memo;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public Long getTradeUserId() {
        return tradeUserId;
    }

    public void setTradeUserId(Long tradeUserId) {
        this.tradeUserId = tradeUserId;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Long getOrginAmount() {
        return orginAmount;
    }

    public void setOrginAmount(Long orginAmount) {
        this.orginAmount = orginAmount;
    }

    public Long getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(Long withdrawId) {
        this.withdrawId = withdrawId;
    }

    public Double getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(Double withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }
}
