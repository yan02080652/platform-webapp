package com.tem.payment.form;

import java.io.Serializable;
import java.util.Date;

public class BillForm implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;

    /**
     * 所属BP
     */
    private Long bpId;
    
    private String bpName;

    private Long tmcId;

    /**
     * 账户id
     */
    private Long accountId;
    
    private String accountName;

    /**
     * 订单金额
     */
    private Integer orderAmount;

    /**
     * 账单净额
     */
    private Integer billAmount;

    /**
     * 调整金额
     */
    private Integer adjustAmount;

    /**
     * 调整说明
     */
    private String adjustDesc;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 创建人
     */
    private Long createUser;
    
    private String createUserName;

    /**
     * 逾期日期
     */
    private Date overdueDate;

    /**
     * 最后修改人
     */
    private Long updateUser;
    
    private String updateUserName;

    /**
     * 最后修改时间
     */
    private Date updateDate;

    /**
     * 结算期间备注
     */
    private String remark;
    
    /**
     * 结算期间开始时间
     */
    private Date liqDateBegin;
    
    /**
     * 结算期间结算时间
     */
    private Date liqDateEnd;

    /**
     * 确认状态 NOT_CONFIRM-待确认，CONFIRM-已确认
     */
    private String status;

    /**
     * 清账状态 OPEN-未清账完成，CLOSE-已清账
     */
    private String liquidationStatus;

    /**
     * 未清账金额
     */
    private Integer remainAmount;

    /**
     * 已开票金额
     */
    private Integer invoiceAmount;
    
    
    //========用户输入的清账金额
    private Double remainAmountInput;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBpId() {
        return bpId;
    }

    public void setBpId(Long bpId) {
        this.bpId = bpId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Integer getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Integer orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(Integer billAmount) {
        this.billAmount = billAmount;
    }

    public Integer getAdjustAmount() {
        return adjustAmount;
    }

    public void setAdjustAmount(Integer adjustAmount) {
        this.adjustAmount = adjustAmount;
    }

    public String getAdjustDesc() {
        return adjustDesc;
    }

    public void setAdjustDesc(String adjustDesc) {
        this.adjustDesc = adjustDesc == null ? null : adjustDesc.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Date getOverdueDate() {
        return overdueDate;
    }

    public void setOverdueDate(Date overdueDate) {
        this.overdueDate = overdueDate;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getLiquidationStatus() {
        return liquidationStatus;
    }

    public void setLiquidationStatus(String liquidationStatus) {
        this.liquidationStatus = liquidationStatus == null ? null : liquidationStatus.trim();
    }

    public Integer getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(Integer remainAmount) {
        this.remainAmount = remainAmount;
    }

    public Integer getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(Integer invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getBpName() {
		return bpName;
	}

	public void setBpName(String bpName) {
		this.bpName = bpName;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Double getRemainAmountInput() {
		return remainAmountInput;
	}

	public void setRemainAmountInput(Double remainAmountInput) {
		this.remainAmountInput = remainAmountInput;
	}

	public String getUpdateUserName() {
		return updateUserName;
	}

	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}

	public Date getLiqDateBegin() {
		return liqDateBegin;
	}

	public void setLiqDateBegin(Date liqDateBegin) {
		this.liqDateBegin = liqDateBegin;
	}

	public Date getLiqDateEnd() {
		return liqDateEnd;
	}

	public void setLiqDateEnd(Date liqDateEnd) {
		this.liqDateEnd = liqDateEnd;
	}

    public Long getTmcId() {
        return tmcId;
    }

    public void setTmcId(Long tmcId) {
        this.tmcId = tmcId;
    }
}