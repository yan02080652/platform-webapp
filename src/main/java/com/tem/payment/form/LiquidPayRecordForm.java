package com.tem.payment.form;

import java.io.Serializable;
import java.util.Date;

public class LiquidPayRecordForm implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;

    /**
     * 付款记录ID
     */
    private Long paymentId;
    
    /**
     * 付款金额
     */
    private Long payAmount;
    
    /**
     * 付款方式
     */
    private String payType;
    
    /**
     * 付款方式
     */
    private String payTypeName;
    
    /**
     * 付款日期
     */
    private Date paymentDate;

    /**
     * 账单id
     */
    private Long billId;

    /**
     * 勾兑金额
     */
    private Integer liquidationAmount;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 创建人
     */
    private Long createUser;
    
    private String createUserName;
    
    /**
     * 付款的付款凭证号
     */
    private String refRecord;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public Integer getLiquidationAmount() {
        return liquidationAmount;
    }

    public void setLiquidationAmount(Integer liquidationAmount) {
        this.liquidationAmount = liquidationAmount;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

	public Long getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(Long payAmount) {
		this.payAmount = payAmount;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getPayTypeName() {
		return payTypeName;
	}

	public void setPayTypeName(String payTypeName) {
		this.payTypeName = payTypeName;
	}

	public String getRefRecord() {
		return refRecord;
	}

	public void setRefRecord(String refRecord) {
		this.refRecord = refRecord;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
}