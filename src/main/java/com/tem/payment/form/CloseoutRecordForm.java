package com.tem.payment.form;

import java.io.Serializable;
import java.util.Date;

public class CloseoutRecordForm implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Long id;

    /**
     * 付款记录ID
     */
    private Long paymentId;

    /**
     * 账单id
     */
    private Long billId;

    /**
     * 勾兑金额
     */
    private Integer liquidationAmount;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 创建人
     */
    private Long createUser;
    
    
    //==============
	/**
	 * 账单备注
	 */
	private String billRemark;
	
	/**
	 * 操作人姓名
	 */
	private String createUserName;
	
	/**
	 * 账户名称
	 */
	private String accountName;
	
	 /**
     * 结算期间开始时间
     */
    private Date liqDateBegin;
    
    /**
     * 结算期间结算时间
     */
    private Date liqDateEnd;
	
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public Integer getLiquidationAmount() {
        return liquidationAmount;
    }

    public void setLiquidationAmount(Integer liquidationAmount) {
        this.liquidationAmount = liquidationAmount;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

	public String getBillRemark() {
		return billRemark;
	}

	public void setBillRemark(String billRemark) {
		this.billRemark = billRemark;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Date getLiqDateBegin() {
		return liqDateBegin;
	}

	public void setLiqDateBegin(Date liqDateBegin) {
		this.liqDateBegin = liqDateBegin;
	}

	public Date getLiqDateEnd() {
		return liqDateEnd;
	}

	public void setLiqDateEnd(Date liqDateEnd) {
		this.liqDateEnd = liqDateEnd;
	}

}