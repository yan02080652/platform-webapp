package com.tem.payment.form;

import java.math.BigDecimal;

/**
 * Created by chenjieming on 2017/5/26.
 */
public class OrderTypeCount {

    private String orderType;

    private BigDecimal count;

    private BigDecimal amount;

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public OrderTypeCount(String orderType, BigDecimal count, BigDecimal amount) {
        this.orderType = orderType;
        this.count = count;
        this.amount = amount;
    }
}
