package com.tem.payment.form;

import java.io.Serializable;
import java.util.Date;

/**
 * 企业账户
 */
public class BpAccountForm implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * 账户ID
	 */
    private Long id;

    /**
     * 所属BP
     */
    private Long bpId;
    
    private String bpName;
    
    /**
     * 账户名称
     */
    private String accountName;

    /**
     * 账户类型:C:信用卡账户 B:实体账户
     */
    private String accountType;
    
    private String accountTypeName;

    /**
     * 信用卡信息
     */
    private String creCard;

    /**
     * 是否开通授权 1:是，0:否
     */
    private Integer grantedCredit;

    /**
     * 授权额度
     */
    private Long creditAmount;
    
    /**
     * 账户余额
     */
    private Long balanceAmount;

    /**
     * 开户时间
     */
    private Date creteDate;

    /**
     * 最后修改时间
     */
    private Date lastModifyDate;
    
    /**
     * 状态 ACTIVE-可用的，DISABLED-禁用的
     */
    private String state;
    
    /**
     * 结算日
     */
    private Integer liquidationDay;
    
    /**
     * 账款逾期天数
     */
    private Integer overdueDays;
    
    /**
     * 滞纳金
     */
    private Integer lateFee;
    
    /**
     * 警戒线 金额不足多少
     */
    private Integer warnLine;
    
    /**
     * 警戒时发送的手机号
     */
    private String warnPhone;
    
    /**
     * 最后修改人
     */
    private Long lastModifyUser;
    
    private String lastModifyUserName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBpId() {
        return bpId;
    }

    public void setBpId(Long bpId) {
        this.bpId = bpId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType == null ? null : accountType.trim();
    }

    public String getCreCard() {
        return creCard;
    }

    public void setCreCard(String creCard) {
        this.creCard = creCard == null ? null : creCard.trim();
    }

    public Integer getGrantedCredit() {
        return grantedCredit;
    }

    public void setGrantedCredit(Integer grantedCredit) {
        this.grantedCredit = grantedCredit;
    }

	public Long getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(Long creditAmount) {
		this.creditAmount = creditAmount;
	}

	public Date getCreteDate() {
        return creteDate;
    }

    public void setCreteDate(Date creteDate) {
        this.creteDate = creteDate;
    }

    public Date getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(Date lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Integer getLiquidationDay() {
		return liquidationDay;
	}

	public void setLiquidationDay(Integer liquidationDay) {
		this.liquidationDay = liquidationDay;
	}

	public Integer getOverdueDays() {
		return overdueDays;
	}

	public void setOverdueDays(Integer overdueDays) {
		this.overdueDays = overdueDays;
	}

	public Integer getLateFee() {
		return lateFee;
	}

	public void setLateFee(Integer lateFee) {
		this.lateFee = lateFee;
	}

	public Long getLastModifyUser() {
		return lastModifyUser;
	}

	public void setLastModifyUser(Long lastModifyUser) {
		this.lastModifyUser = lastModifyUser;
	}

	public String getBpName() {
		return bpName;
	}

	public void setBpName(String bpName) {
		this.bpName = bpName;
	}

	public String getAccountTypeName() {
		return accountTypeName;
	}

	public void setAccountTypeName(String accountTypeName) {
		this.accountTypeName = accountTypeName;
	}

	public String getLastModifyUserName() {
		return lastModifyUserName;
	}

	public void setLastModifyUserName(String lastModifyUserName) {
		this.lastModifyUserName = lastModifyUserName;
	}

	public Long getBalanceAmount() {
		return balanceAmount;
	}

	public void setBalanceAmount(Long balanceAmount) {
		this.balanceAmount = balanceAmount;
	}

	public Integer getWarnLine() {
		return warnLine;
	}

	public void setWarnLine(Integer warnLine) {
		this.warnLine = warnLine;
	}

	public String getWarnPhone() {
		return warnPhone;
	}

	public void setWarnPhone(String warnPhone) {
		this.warnPhone = warnPhone;
	}
    
}