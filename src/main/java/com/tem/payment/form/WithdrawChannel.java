package com.tem.payment.form;

/**
 * 支付通道
 * @author wls
 *
 */
public enum WithdrawChannel {

	ALIPAY("支付宝账户"),

	WECHATPAY("微信账户"),

	BANK_TRANSFER("银行转账");

	private String value;

	private WithdrawChannel(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return value;
	}
}
