package com.tem.payment.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by chenjieming on 2017/5/26.
 */
public class PartnerOrderForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String partnerName;

    private BigDecimal totalAmount;

    //订单类型
    private List<OrderTypeCount> orderTypeCount;

    //下单渠道
    private List<OrderOriginCount> orderOriginCount;

    //支付方式
    private List<PaymentMethod> paymentMethod;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public List<OrderTypeCount> getOrderTypeCount() {
        return orderTypeCount;
    }

    public void setOrderTypeCount(List<OrderTypeCount> orderTypeCount) {
        this.orderTypeCount = orderTypeCount;
    }

    public List<OrderOriginCount> getOrderOriginCount() {
        return orderOriginCount;
    }

    public void setOrderOriginCount(List<OrderOriginCount> orderOriginCount) {
        this.orderOriginCount = orderOriginCount;
    }

    public List<PaymentMethod> getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(List<PaymentMethod> paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


}
