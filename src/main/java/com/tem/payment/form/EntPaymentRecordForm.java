package com.tem.payment.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 企业付款记录
 * @author jieming
 *
 */
public class EntPaymentRecordForm implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	/**
	 * 所属bpId
	 */
    private Long bpId;

    /**
     * 所属法人
     */
    private Long corpId;
    
    /**
     * 付款方
     */
    private String paymentName;

    /**
     * 付款方式
     */
    private String paymentChannel;

    /**
     * 付款金额
     */
    private Double amount;

    /**
     * 付款日期
     */
	@DateTimeFormat(pattern="yyyy-MM-dd")
    private Date paymentDate;

    /**
     * 参考凭证号
     */
    private String refRecord;

    /**
     * 企业账户流水
     */
    private String remark1;

    /**
     * 备注
     */
    private String remark2;

    /**
     * 备注
     */
    private String remark;

    /**
     * 记录人
     */
    private Long createUser;

    /**
     * 记录时间
     */
	@DateTimeFormat(pattern="yyyy-MM-dd")
    private Date createTime;

    /**
     * 清账状态
     * OPEN 未清账完成,
     * CLOSE 已清账
     */
    private String liquidationStatus;

    /**
     * 未清账金额
     */
    private Integer remainAmount;
    
    /**
     * BP姓名
     */
    private String bpName;
    
    /**
     * 法人姓名
     */
    private String corpName;
    
    /**
     * 记录人姓名
     */
    private String createUserName;
    
	/**
	 * 开始时间
	 */
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date formDate;
	
	/**
	 * 结束时间
	 */
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date toDate;
	
	/**
     * 状态 0未确认 1、已确认
     */
	private Integer status;

    private Long tmcId;

    private String bpTmcName;

    public Long getTmcId() {
        return tmcId;
    }

    public void setTmcId(Long tmcId) {
        this.tmcId = tmcId;
    }

    public String getBpTmcName() {
        return bpTmcName;
    }

    public void setBpTmcName(String bpTmcName) {
        this.bpTmcName = bpTmcName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBpId() {
        return bpId;
    }

    public void setBpId(Long bpId) {
        this.bpId = bpId;
    }

    public Long getCorpId() {
        return corpId;
    }

    public void setCorpId(Long corpId) {
        this.corpId = corpId;
    }

    public String getPaymentChannel() {
        return paymentChannel;
    }

    public void setPaymentChannel(String paymentChannel) {
        this.paymentChannel = paymentChannel == null ? null : paymentChannel.trim();
    }

    public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getRefRecord() {
        return refRecord;
    }

    public void setRefRecord(String refRecord) {
        this.refRecord = refRecord == null ? null : refRecord.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getLiquidationStatus() {
        return liquidationStatus;
    }

    public void setLiquidationStatus(String liquidationStatus) {
        this.liquidationStatus = liquidationStatus == null ? null : liquidationStatus.trim();
    }

    public Integer getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(Integer remainAmount) {
        this.remainAmount = remainAmount;
    }

	public String getBpName() {
		return bpName;
	}

	public void setBpName(String bpName) {
		this.bpName = bpName;
	}

	public String getCorpName() {
		return corpName;
	}

	public void setCorpName(String corpName) {
		this.corpName = corpName;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public Date getFormDate() {
		return formDate;
	}

	public void setFormDate(Date formDate) {
		this.formDate = formDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getPaymentName() {
		return paymentName;
	}

	public void setPaymentName(String paymentName) {
		this.paymentName = paymentName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }
}