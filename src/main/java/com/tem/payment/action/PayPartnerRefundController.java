package com.tem.payment.action;

import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.web.Message;
import com.iplatform.common.web.MessageTypeEnum;
import com.tem.flight.api.order.FlightOrderService;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.hotel.api.HotelOrderService;
import com.tem.hotel.dto.order.HotelOrderDto;
import com.tem.payment.api.PayPartnerRefundService;
import com.tem.payment.condition.PayPartnerRefundCondition;
import com.tem.payment.dto.PayPartnerRefundDto;
import com.tem.platform.api.PartnerService;
import com.tem.product.api.GeneralOrderService;
import com.tem.product.dto.general.GeneralOrderDto;
import com.tem.train.api.order.TrainOrderService;
import com.tem.train.dto.order.TrainOrderDto;
import com.tem.platform.action.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 退款给TMC
 * Created by pyy on 2017/9/7.
 */
@RequestMapping("payment/payRefund")
@Controller
public class PayPartnerRefundController extends BaseController {

    @Autowired
    private PayPartnerRefundService payPartnerRefundService;

    @Autowired
    private FlightOrderService flightOrderService;
    @Autowired
    private TrainOrderService trainOrderService;
    @Autowired
    private HotelOrderService hotelOrderService;
    @Autowired
    private GeneralOrderService generalOrderService;
    @Autowired
    private PartnerService partnerService;

    @RequestMapping
    public String index(Model model){
        String c_order = super.getStringValue("orderId");
        Long c_tmcId = super.getLongValue("tmcId");
        Integer page = super.getIntegerValue("page");
        if (page == null) {
            page = 1;
        }
        QueryDto<PayPartnerRefundCondition> queryDto = new QueryDto<>(page,12);
        PayPartnerRefundCondition con = new PayPartnerRefundCondition();
        con.setTmcId(c_tmcId);
        if (StringUtils.isNotEmpty(c_order)) {
            con.setOrderId(c_order.trim());
        }

        queryDto.setCondition(con);
        Page<PayPartnerRefundDto> pageList = payPartnerRefundService.query(queryDto);
        Map<Long, String> partnerNames = new HashMap<>();
        List<PayPartnerRefundDto> partnerRefundList = pageList.getList();
        if (partnerRefundList != null) {
            for (PayPartnerRefundDto pd:partnerRefundList){
                Long partnerId = pd.getPartnerId();
                Long tmcId = pd.getTmcId();
                if (partnerId != null && !partnerNames.containsKey(partnerId)) {
                    partnerNames.put(partnerId, this.getPartnerName(partnerId));
                }
                if(tmcId != null && !partnerNames.containsKey(tmcId)){
                    partnerNames.put(tmcId, this.getPartnerName(tmcId));
                }
            }
        }
        model.addAttribute("orderId", c_order);
        model.addAttribute("tmcId", c_tmcId);
        model.addAttribute("tmcName", this.getPartnerName(c_tmcId));
        model.addAttribute("pageList", pageList);
        model.addAttribute("partnerNames", partnerNames);
        return "payment/payRefund/list.base";
    }

    @RequestMapping("detail")
    public String detail(Model model){
        Long id = super.getLongValue("id");
        if(id != null){
            PayPartnerRefundDto payRefund = payPartnerRefundService.get(id);
            model.addAttribute("payRefund", payRefund);
        }
        return "payment/payRefund/detail.base";
    }

    @RequestMapping("save")
    @ResponseBody
    public Message save(PayPartnerRefundDto payRefund){
        payRefund.setCreateUserId(super.getCurrentUser().getId());
        payRefund.setCreateTime(new Date());
        int rs = payPartnerRefundService.add(payRefund);
        if(rs == 1){
            return new Message(MessageTypeEnum.success,"新增成功");
        }else{
            return new Message(MessageTypeEnum.error,"新增失败");
        }
    }

    @RequestMapping("getOrderInfo")
    @ResponseBody
    public Map<String,Object> getOrderInfo() {
        Map<String, Object> result = new HashMap<>();
        String orderId = super.getStringValue("orderId");
        if (StringUtils.isNotEmpty(orderId) && orderId.length() > 2) {
            String id = orderId.substring(0,2);
            //订单摘要
            String orderSummary = null;
            //订单总额
            BigDecimal orderAmount = new BigDecimal(0);
            //企业id
            Long partnerId = null;
            Long tmcId = null;
            if (id.equals(OrderBizType.FLIGHT.getCode()+"")) {
                FlightOrderDto flightOrderDto = flightOrderService.getFlightOrderById(orderId);
                if (flightOrderDto != null) {
                    orderSummary = flightOrderDto.getSummary();
                    orderAmount = flightOrderDto.getOrderFee().getTotalOrderPrice();
                    if(flightOrderDto.getcId() != null){
                        partnerId = Long.valueOf(flightOrderDto.getcId());
                    }
                    if (flightOrderDto.getTmcId() != null) {
                        tmcId = Long.valueOf(flightOrderDto.getTmcId());
                    }
                }
            } else if (id.equals(OrderBizType.TRAIN.getCode()+"")) {
                TrainOrderDto trainOrderDto = trainOrderService.getTrainOrderById(orderId);
                if (trainOrderDto != null) {
                    orderSummary = trainOrderDto.getSummary();
                    orderAmount = trainOrderDto.getTotalAmount();
                    if (trainOrderDto.getcId() != null) {
                        partnerId = Long.valueOf(trainOrderDto.getcId());
                    }
                    if (trainOrderDto.getTmcId() != null) {
                        tmcId = Long.valueOf(trainOrderDto.getTmcId());
                    }
                }
            } else if (id.equals(OrderBizType.HOTEL.getCode()+"")) {
                HotelOrderDto hotelOrderDto = hotelOrderService.getOrderById(orderId).getData();
                if (hotelOrderDto != null) {
                    orderSummary = hotelOrderDto.getSummary();
                    orderAmount = hotelOrderDto.getFinalTotalPrice();
                    if (hotelOrderDto.getCId() != null) {
                        partnerId = Long.valueOf(hotelOrderDto.getCId());
                    }
                    if (hotelOrderDto.getTmcId() != null) {
                        tmcId = Long.valueOf(hotelOrderDto.getTmcId());
                    }
                }
            } else if (id.equals(OrderBizType.GENERAL.getCode()+"")) {
                GeneralOrderDto generalOrderDto = generalOrderService.getById(orderId);
                if (generalOrderDto != null) {
                    orderSummary = generalOrderDto.getSummary();
                    orderAmount = generalOrderDto.getTotalOrderPrice();
                    if (generalOrderDto.getcId() != null) {
                        partnerId = generalOrderDto.getcId();
                    }
                    if (generalOrderDto.getTmcId() != null) {
                        tmcId = generalOrderDto.getTmcId();
                    }
                }
            }
            result.put("orderSummary", orderSummary);
            result.put("orderAmount", orderAmount);
            result.put("partnerId", partnerId);
            result.put("partnerName", this.getPartnerName(partnerId));
            result.put("tmcId", tmcId);
            result.put("tmcName",this.getPartnerName(tmcId));
        }
        return result;
    }

    private String getPartnerName(Long partnerId) {
        if (partnerId == null) {
            return null;
        }
        return partnerService.getNameById(partnerId);
    }

}
