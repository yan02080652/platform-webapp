package com.tem.payment.action;

import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.exception.BizException;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.web.Message;
import com.tem.car.ResponseDto;
import com.tem.car.api.service.order.ICarOrderService;
import com.tem.car.dto.CarOrderDto;
import com.tem.car.dto.result.OrderDetailDto;
import com.tem.flight.api.order.FlightOrderService;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.hotel.api.HotelOrderService;
import com.tem.hotel.dto.order.HotelOrderDto;
import com.tem.international.api.InternationalFlightOrderService;
import com.tem.international.dto.InternationalFlightOrderDto;
import com.tem.payment.api.*;
import com.tem.payment.dto.*;
import com.tem.payment.enums.EntPaymentChannel;
import com.tem.payment.enums.PayChannel;
import com.tem.payment.form.BillForm;
import com.tem.payment.form.BillStatForm;
import com.tem.payment.form.LiquidPayRecordForm;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.product.api.GeneralOrderService;
import com.tem.product.api.InsuranceOrderService;
import com.tem.product.dto.general.GeneralOrderDto;
import com.tem.product.dto.insurance.InsuranceOrderDto;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.dto.refund.RefundOrderDto;
import com.tem.pss.dto.refund.RefundStatus;
import com.tem.train.api.order.TrainOrderService;
import com.tem.train.dto.order.TrainOrderDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("bpAccount/settlement")
public class BillDetailController extends BaseController {

	@Autowired
	private UserService userService;

	@Autowired
	private AccountManageService accountManageService;

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private EntPaymentRecordService entPaymentRecordService;

	@Autowired
	private BillStatService billStatService;

	@Autowired
	private BillService billService;

	@Autowired
	private InvoiceService invoiceService;

	@Autowired
	private LiquidActionRecordService liquidActionRecordService;

	@Autowired
	private SubsidyService subsidyService;

	@Autowired
	private FlightOrderService flightOrderService;

	@Autowired
	private HotelOrderService hotelOrderService;

	@Autowired
	private TrainOrderService trainOrderService;

	@Autowired
	private GeneralOrderService generalOrderService;

	@Autowired
	private ICarOrderService carOrderService;

	@Autowired
	private InternationalFlightOrderService internationalFlightOrderService;

	@Autowired
	private BillOrderService billOrderService;

	@Autowired
	private InsuranceOrderService insuranceOrderService;

	@Autowired
	private RefundOrderService refundOrderService;

	/**
	 * 添加或修改查看账单明细
	 * 
	 * @param model
	 * @param id
	 *            账单id
	 * @return
	 */
	@RequestMapping(value = "billDetail")
	public String billDetail(Model model, @RequestParam(required = false) Long id) {

		BillForm billForm = new BillForm();
		// 已逾期天数
		int overDays = 0;
		// 未清账的金额
		Integer unLiquidAmount = 0;
		List<LiquidPayRecordForm> liquidPayRecordList = new ArrayList<LiquidPayRecordForm>();
		// 可用补贴
		Long canUseSubsidy = 0L;
		// 账单分类汇总
		List<BillStatForm> billStatList = new ArrayList<BillStatForm>();
		// 已开发票
		List<InvoiceDto> invoiceDtos = null;
		// 账单调整项
		List<BillAdjitemDto> billAdjitemDtos = null;

		// 说明查看账单明细 或者修改账单
		if (id != null) {
			BillDto billDto = billService.selectById(id);
			if (billDto != null) {
				if (!super.isSuperAdmin() && !billDto.getTmcId().equals(super.getTmcId())) {
					throw new BizException("403", "无访问权限");
				}

				billForm = TransformUtils.transform(billDto, BillForm.class);
				if (billForm.getBpId() != null) {
					PartnerDto partnerDto = partnerService.findById(billForm.getBpId());
					if (partnerDto != null) {
						billForm.setBpName(partnerDto.getName());
					}
				}
				if (billForm.getAccountId() != null) {
					BpAccountDto bpAccountDto = accountManageService.getById(billForm.getAccountId());
					if (bpAccountDto != null) {
						billForm.setAccountName(bpAccountDto.getAccountName());
					}
				}
				if (billForm.getUpdateUser() != null) {
					UserDto upUser = userService.getUser(billForm.getUpdateUser());
					if (upUser != null) {
						billForm.setUpdateUserName(upUser.getFullname());
					}
				}
			}
			// 得到该账单所开的发票
			invoiceDtos = invoiceService.getByBillId(id);

			billAdjitemDtos = billService.getBillAdjitemByBillId(id);

			// 得到逾期天数
			if (billDto.getOverdueDate() != null) {
				Calendar od = Calendar.getInstance();
				// 当前时间
				Long curMs = od.getTimeInMillis();
				od.setTime(billDto.getOverdueDate());
				// 逾期日
				Long odMs = od.getTimeInMillis();

				// 说明逾期了
				if (curMs > odMs) {
					overDays = DateUtils.getDaySpan(od.getTime(), billDto.getOverdueDate());
				}
			}

			// 查询该账单所有的清账记录
			List<LiquidActionRecordDto> liquidList = liquidActionRecordService.getByBillId(id);
			// 已清账金额
			Integer liquidAmount = 0;
			if (liquidList != null) {
				LiquidPayRecordForm liquidPayRecordForm = null;
				for (LiquidActionRecordDto lr : liquidList) {
					liquidPayRecordForm = TransformUtils.transform(lr, LiquidPayRecordForm.class);
					if (lr.getCreateUser() != null) {
						UserDto user = userService.getUser(lr.getCreateUser());
						if (user != null) {
							liquidPayRecordForm.setCreateUserName(user.getFullname());
						}
					}
					if (lr.getPaymentId() != null) {
						EntPaymentRecordDto epr = entPaymentRecordService.getById(lr.getPaymentId());
						if (epr != null) {
							liquidPayRecordForm.setPaymentDate(epr.getPaymentDate());
							liquidPayRecordForm.setPayAmount(epr.getAmount().longValue());
							liquidPayRecordForm.setPayType(epr.getPaymentChannel());
							liquidPayRecordForm
									.setPayTypeName(EntPaymentChannel.valueOf(epr.getPaymentChannel()).getValue());
							liquidPayRecordForm.setRefRecord(epr.getRefRecord());
						}
					}

					liquidPayRecordList.add(liquidPayRecordForm);
					liquidAmount += lr.getLiquidationAmount();
				}
			}
			unLiquidAmount = billForm.getBillAmount() - liquidAmount;

			// 得到账单分类统计
			List<BillStatDto> billList = billStatService.getByBillId(id);
			if (CollectionUtils.isNotEmpty(billList)) {
				BillStatForm billStatForm = null;
				for (BillStatDto bs : billList) {
					billStatForm = TransformUtils.transform(bs, BillStatForm.class);
					if (StringUtils.isNotEmpty(bs.getBiz())) {
						billStatForm.setBizName(OrderBizType.valueOf(bs.getBiz()).getMessage());
					}
					billStatList.add(billStatForm);
				}
			}

		} else {
			// 创建账单
			Long accountId = super.getLongValue("accountId");
			if (accountId == null) {
				return null;
			}
			BpAccountDto bpAccount = accountManageService.getById(accountId);
			if (bpAccount != null) {
				if (bpAccount.getBpId() != null) {
					billForm.setBpId(bpAccount.getBpId());
					PartnerDto partnerDto = partnerService.findById(bpAccount.getBpId());
					if (partnerDto != null) {
						billForm.setBpName(partnerDto.getName());
					}
				}
				billForm.setAccountId(accountId);
				billForm.setAccountName(bpAccount.getAccountName());

				if (bpAccount.getOverdueDays() != null) {
					Calendar c = Calendar.getInstance();
					c.add(Calendar.DAY_OF_MONTH, bpAccount.getOverdueDays());
					billForm.setOverdueDate(c.getTime());
				}
			}
			// 结算期间默认是上个月的一号和最后一天
			Calendar calendar1 = Calendar.getInstance();
			calendar1.add(Calendar.MONTH, -1);
			calendar1.set(Calendar.DAY_OF_MONTH, 1);
			billForm.setLiqDateBegin(calendar1.getTime());
			// 获取前一个月最后一天
			Calendar calendar2 = Calendar.getInstance();
			calendar2.set(Calendar.DAY_OF_MONTH, 0);
			billForm.setLiqDateEnd(calendar2.getTime());
			billForm.setCreateDate(new Date());
			billForm.setOrderAmount(0);
			billForm.setInvoiceAmount(0);
			billForm.setBillAmount(0);
		}

		// 得到可用补贴
		SubsidyDto subsidyDto = subsidyService.getByAccountId(billForm.getAccountId());
		if (subsidyDto != null && subsidyDto.getRemainAmount() != null) {
			canUseSubsidy = subsidyDto.getRemainAmount().longValue();
		}

		model.addAttribute("billInfo", billForm);
		model.addAttribute("billStatList", billStatList);
		model.addAttribute("canUseSubsidy", canUseSubsidy);
		model.addAttribute("liquidPayRecordList", liquidPayRecordList);
		model.addAttribute("unLiquidAmount", unLiquidAmount);
		model.addAttribute("overDays", overDays);
		model.addAttribute("invoiceDtos", invoiceDtos);
		model.addAttribute("billAdjitemDtos", billAdjitemDtos);
		model.addAttribute("tabIndex", super.getStringValue("tabIndex"));
		model.addAttribute("linkFrom", super.getStringValue("linkFrom"));
		return "payment/settlement/billDetail.base";
	}

	/**
	 * 得到该账单关联的订单 分页采用20条每页
	 * 
	 * @param model
	 * @param pageIndex
	 * @param billId
	 *            账单id
	 * @return
	 */
	@RequestMapping(value = "billOrderList")
	public String billOrderList(Model model, Integer pageIndex, Long billId) {
		pageIndex = pageIndex == null ? 1 : pageIndex;

		if (billId != null) {

			String bizType = super.getStringValue("bizType");
			String orderNo = super.getStringValue("orderNo");

			List<TotalOrderForm> totalOrderForms = billOrderService.getBillOrders(billId, true, bizType, null, null,
					orderNo);

			// 分页
			List<TotalOrderForm> resultList = new ArrayList<TotalOrderForm>();

			for (int i = (pageIndex - 1) * 10; (i < 10 * pageIndex && i < totalOrderForms.size()); i++) {
				resultList.add(totalOrderForms.get(i));
			}

			Page<TotalOrderForm> resultPage = new Page<TotalOrderForm>(pageIndex, 10, resultList,
					totalOrderForms.size());
			resultPage.setCurrentPage(pageIndex);

			model.addAttribute("pageList", resultPage);
			model.addAttribute("billId", billId);

			BillDto bill = billService.selectById(billId);

			model.addAttribute("liquidationStatus", bill.getLiquidationStatus());
			model.addAttribute("billStatus", bill.getStatus());
		}
		model.addAttribute("orderNo", super.getStringValue("orderNo"));
		model.addAttribute("bizType", super.getStringValue("bizType"));

		model.addAttribute("paymentMethodMap", getPaymentMethodMap());

		return "payment/settlement/billOrderList.jsp";
	}

	/**
	 * 得到所有需要结算的订单
	 * 
	 * @param model
	 * @param billId
	 * @return
	 */
	@RequestMapping(value = "needSettleOrderList")
	public String needSettleOrderList(Model model, @RequestParam(value = "billId", required = false) Long billId) {
		Map<String, String> dMap = getBillDefaultDate(billId);
		model.addAttribute("queryStartDate", dMap.get("sd"));
		model.addAttribute("queryEndDate", dMap.get("ed"));
		return "payment/settlement/needSettleOrderList.jsp";
	}

	/**
	 * 得到所有需要结算的订单 分页采用10条每页
	 * 
	 * @param model
	 * @param pageIndex
	 * @return
	 */
	@RequestMapping(value = "loadBillOrderTable")
	public String loadBillOrderTable(Model model, Integer pageIndex, Long billId) {
		pageIndex = pageIndex == null ? 1 : pageIndex;

		Date sd = null;
		Date ed = null;
		String queryStartDate = super.getStringValue("queryStartDate");
		String queryEndDate = super.getStringValue("queryEndDate");
		if (StringUtils.isNotEmpty(queryStartDate)) {
			sd = DateUtils.parse(queryStartDate, "yyyy-MM-dd");
			sd = DateUtils.getDayBegin(sd);
		}
		if (StringUtils.isNotEmpty(queryEndDate)) {
			ed = DateUtils.parse(queryEndDate, "yyyy-MM-dd");
			ed = DateUtils.getDayEnd(ed);
		}
		String bizType = super.getStringValue("bizType");
		String orderNo = super.getStringValue("orderNo");

		List<TotalOrderForm> totalOrderForms = billOrderService.getBillOrders(billId, false, bizType, sd, ed, orderNo);

		// 分页
		List<TotalOrderForm> resultList = new ArrayList<TotalOrderForm>();

		for (int i = (pageIndex - 1) * 10; (i < 10 * pageIndex && i < totalOrderForms.size()); i++) {
			resultList.add(totalOrderForms.get(i));
		}

		Page<TotalOrderForm> resultPage = new Page<TotalOrderForm>(pageIndex, 10, resultList, totalOrderForms.size());
		resultPage.setCurrentPage(pageIndex);

		model.addAttribute("pageList", resultPage);

		model.addAttribute("paymentMethodMap", getPaymentMethodMap());

		return "payment/settlement/needSettleOrderTable.jsp";
	}

	/**
	 * 批量添加订单
	 * 
	 * @param orderNos
	 *            订单编号 以逗号隔开
	 * @param billId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "addOrders")
	public Message addOrders(String orderNos, Long billId) {
		List<TotalOrderForm> totalOrderForms = new ArrayList<TotalOrderForm>();
		String queryStartDate = super.getStringValue("queryStartDate");
		String queryEndDate = super.getStringValue("queryEndDate");
		Date sd = null;
		Date ed = null;
		if (StringUtils.isNotEmpty(queryStartDate)) {
			sd = DateUtils.parse(queryStartDate, "yyyy-MM-dd");
			sd = DateUtils.getDayBegin(sd);
		}
		if (StringUtils.isNotEmpty(queryEndDate)) {
			ed = DateUtils.parse(queryEndDate, "yyyy-MM-dd");
			ed = DateUtils.getDayEnd(ed);
		}
		String[] orderNoList = orderNos.split(",");
		for (String orderNo : orderNoList) {
			OrderBizType orderType = OrderBizType.valueOfByOrderId(orderNo);
			OrderForm orderForm = new OrderForm();
			if (orderType.equals(OrderBizType.FLIGHT)) {
				FlightOrderDto flightOrderDto = flightOrderService.getFlightOrderById(orderNo);
				if (flightOrderDto != null) {// 说明是正向订单
					orderForm.setOrderDate(flightOrderDto.getCreateDate());
					orderForm.setOrderNo(flightOrderDto.getId());
					orderForm.setOrderType(OrderBizType.FLIGHT.name());
					orderForm.setOrderPrice(flightOrderDto.getBpTotalAmount() == null ? 0D
							: flightOrderDto.getBpTotalAmount().doubleValue());

					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm.setOrderForm(orderForm);
					totalOrderForm = getOrderFormByOrderNo(totalOrderForm, sd, ed);
					totalOrderForms.add(totalOrderForm);

				} else {// 说明是退单
					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm = getOrderFormByRefundOrderNo(orderNo, sd, ed);
					if (totalOrderForm != null) {
						totalOrderForms.add(totalOrderForm);
					}
				}
			} else if (orderType.equals(OrderBizType.INTERNATIONAL_FLIGHT)) {
				InternationalFlightOrderDto flightOrderDto = internationalFlightOrderService
						.findFlightOrderById(orderNo);
				if (flightOrderDto != null) {// 说明是正向订单
					orderForm.setOrderDate(flightOrderDto.getCreateTime());
					orderForm.setOrderNo(flightOrderDto.getId());
					orderForm.setOrderType(OrderBizType.INTERNATIONAL_FLIGHT.name());
					orderForm.setOrderPrice(flightOrderDto.getBpTotalAmount() == null ? 0D
							: flightOrderDto.getBpTotalAmount().doubleValue());

					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm.setOrderForm(orderForm);
					totalOrderForm = getOrderFormByOrderNo(totalOrderForm, sd, ed);
					totalOrderForms.add(totalOrderForm);

				} else {// 说明是退单
					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm = getOrderFormByRefundOrderNo(orderNo, sd, ed);
					if (totalOrderForm != null) {
						totalOrderForms.add(totalOrderForm);
					}
				}
			} else if (orderType.equals(OrderBizType.TRAIN)) {
				TrainOrderDto trainOrderDto = trainOrderService.getTrainOrderById(orderNo);
				if (trainOrderDto != null) {// 说明是正向订单
					orderForm.setOrderDate(trainOrderDto.getCreateDate());
					orderForm.setOrderNo(trainOrderDto.getId());
					orderForm.setOrderType(OrderBizType.TRAIN.name());
					orderForm.setOrderPrice(trainOrderDto.getBpTotalAmount() == null ? 0d
							: trainOrderDto.getBpTotalAmount().doubleValue());

					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm.setOrderForm(orderForm);
					totalOrderForm = getOrderFormByOrderNo(totalOrderForm, sd, ed);
					totalOrderForms.add(totalOrderForm);

				} else {// 说明是退单
					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm = getOrderFormByRefundOrderNo(orderNo, sd, ed);
					if (totalOrderForm != null) {
						totalOrderForms.add(totalOrderForm);
					}
				}

			} else if (orderType.equals(OrderBizType.HOTEL)) {
				HotelOrderDto hotelOrderDto = hotelOrderService.getOrderById(orderNo).getData();
				if (hotelOrderDto != null) {// 说明是正向订单
					orderForm.setOrderDate(hotelOrderDto.getCreateDate());
					orderForm.setOrderNo(hotelOrderDto.getId());
					orderForm.setOrderType(OrderBizType.HOTEL.name());
					orderForm.setOrderPrice(hotelOrderDto.getBpTotalAmount() == null ? 0d
							: hotelOrderDto.getBpTotalAmount().doubleValue());

					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm.setOrderForm(orderForm);
					totalOrderForm = getOrderFormByOrderNo(totalOrderForm, sd, ed);
					totalOrderForms.add(totalOrderForm);

				} else {// 说明是退单
					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm = getOrderFormByRefundOrderNo(orderNo, sd, ed);
					if (totalOrderForm != null) {
						totalOrderForms.add(totalOrderForm);
					}
				}
			} else if (orderType.equals(OrderBizType.GENERAL)) {
				GeneralOrderDto generalOrderDto = generalOrderService.getById(orderNo);
				if (generalOrderDto != null) {// 说明是正向订单
					orderForm.setOrderDate(generalOrderDto.getCreateDate());
					orderForm.setOrderNo(generalOrderDto.getId());
					orderForm.setOrderType(OrderBizType.GENERAL.name());
					orderForm.setOrderPrice(generalOrderDto.getBpTotalAmount() == null ? 0D
							: generalOrderDto.getBpTotalAmount().doubleValue());

					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm.setOrderForm(orderForm);
					totalOrderForm = getOrderFormByOrderNo(totalOrderForm, sd, ed);
					totalOrderForms.add(totalOrderForm);

				} else {// 说明是退单
					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm = getOrderFormByRefundOrderNo(orderNo, sd, ed);
					if (totalOrderForm != null) {
						totalOrderForms.add(totalOrderForm);
					}
				}
			} else if (orderType.equals(OrderBizType.CAR)) {
				ResponseDto<OrderDetailDto> responseDto = carOrderService.orderDetail(orderNo);

				CarOrderDto carOrderDto = responseDto.getData().getOrder();

				if (carOrderDto != null) {// 说明是正向订单
					orderForm.setOrderDate(carOrderDto.getCreateDate());
					orderForm.setOrderNo(carOrderDto.getId());
					orderForm.setOrderType(OrderBizType.CAR.name());
					orderForm.setOrderPrice(
							carOrderDto.getBpTotalAmount() == null ? 0D : carOrderDto.getBpTotalAmount().doubleValue());

					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm.setOrderForm(orderForm);
					totalOrderForm = getOrderFormByOrderNo(totalOrderForm, sd, ed);
					totalOrderForms.add(totalOrderForm);

				} else {// 说明是退单
					TotalOrderForm totalOrderForm = new TotalOrderForm();
					totalOrderForm = getOrderFormByRefundOrderNo(orderNo, sd, ed);
					if (totalOrderForm != null) {
						totalOrderForms.add(totalOrderForm);
					}
				}
			}
		}

		billService.addBillOrders(totalOrderForms, billId);
		return Message.success("处理成功");
	}

	/**
	 * 添加所有满足查询条件的订单
	 * 
	 * @param billId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "addAllOrder")
	public Message addAllOrder(Long billId) {

		String queryStartDate = super.getStringValue("queryStartDate");
		String queryEndDate = super.getStringValue("queryEndDate");

		Date sd = null;
		Date ed = null;
		if (StringUtils.isNotEmpty(queryStartDate)) {
			sd = DateUtils.parse(queryStartDate, "yyyy-MM-dd");
			sd = DateUtils.getDayBegin(sd);
		}
		if (StringUtils.isNotEmpty(queryEndDate)) {
			ed = DateUtils.parse(queryEndDate, "yyyy-MM-dd");
			ed = DateUtils.getDayEnd(ed);
		}
		String bizType = super.getStringValue("bizType");
		String orderNo = super.getStringValue("orderNo");

		List<TotalOrderForm> totalOrderForms = billOrderService.getBillOrders(billId, false, bizType, sd, ed, orderNo);

		if (CollectionUtils.isNotEmpty(totalOrderForms)) {
			billService.addBillOrders(totalOrderForms, billId);
		}
		return Message.success("处理成功");
	}

	/**
	 * 批量移除
	 * 
	 * @param orderNos
	 * @param billId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "batchRemoveOrder")
	public boolean batchRemoveOrder(String orderNos, Long billId) {

		boolean flag = true;
		String[] arrOrder = orderNos.split(",");
		for (String str : arrOrder) {

			billOrderService.removeBillOrder(str, billId);
		}
		return flag;
	}

	/**
	 * 从账单中移除订单
	 * 
	 * @param orderNo
	 * @param billId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "removeOrder")
	public Message removeOrder(String orderNo, Long billId) {

		billOrderService.removeBillOrder(orderNo, billId);

		return Message.success("处理成功");
	}

	/**
	 * 标记为争议订单
	 * 
	 * @param orderNo
	 * @param billId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "markControversial")
	public Message markControversial(String orderNo, Long billId) {

		billOrderService.markControversialOrder(orderNo, billId);

		return Message.success("处理成功");
	}

	private Map<String, String> getBillDefaultDate(Long billId) {
		Map<String, String> dMap = new HashMap<String, String>();
		Date sd = null;
		Date ed = null;
		BillDto billDto = billService.selectById(billId);
		if (billDto != null) {
			sd = billDto.getLiqDateBegin();
			// 开始时间往前推5天
			Calendar c = Calendar.getInstance();
			c.setTime(sd);
			c.add(Calendar.DAY_OF_MONTH, -5);
			sd = c.getTime();
			ed = billDto.getLiqDateEnd();
		}
		if (sd != null) {
			dMap.put("sd", DateUtils.format(sd));
		}
		if (ed != null) {
			dMap.put("ed", DateUtils.format(ed));
		}
		return dMap;
	}

	private static Map<String, String> getPaymentMethodMap() {
		Map<String, String> map = new HashMap<>();
		for (PayChannel p : PayChannel.values()) {
			map.put(p.name(), p.getValue());
		}
		return map;
	}

	/**
	 * 退款有多个可能传过来的只是一个
	 * 
	 * @param refundOrderNo
	 * @param sd
	 * @param ed
	 * @return
	 */
	private TotalOrderForm getOrderFormByRefundOrderNo(String refundOrderNo, Date sd, Date ed) {
		TotalOrderForm totalOrderForm = null;
		RefundOrderDto refundOrderDto = refundOrderService.getById(refundOrderNo);
		if (refundOrderDto != null) {
			totalOrderForm = new TotalOrderForm();

			List<RefundOrderForm> refundOrderForms = new ArrayList<RefundOrderForm>();
			List<RefundOrderDto> refundOrders = refundOrderService.findListByOrderId(refundOrderDto.getOrderId());
			RefundOrderForm refundOrderForm = null;

			for (RefundOrderDto refundOrder : refundOrders) {// 退单企业退款金额是负数
				if (RefundStatus.REFUNDED == refundOrder.getStatus()) {
					if (refundOrder.getApplyTime().getTime() >= sd.getTime()
							&& refundOrder.getApplyTime().getTime() <= ed.getTime()) {
						refundOrderForm = new RefundOrderForm();
						refundOrderForm.setOrderDate(refundOrder.getApplyTime());
						refundOrderForm.setOrderNo(refundOrder.getId());
						refundOrderForm.setPreOrderNo(refundOrder.getOrderId());
						refundOrderForm.setOrderPrice(refundOrder.getBpTotalAmount() == null ? 0D
								: refundOrder.getBpTotalAmount().doubleValue());
						refundOrderForms.add(refundOrderForm);
					}
				}
			}
			totalOrderForm.setRefundOrderForms(refundOrderForms);

			String orderId = refundOrderDto.getOrderId();
			List<InsuranceOrderDto> insuranceOrderDtos = insuranceOrderService.findInsuranceOrderByBizOrderId(orderId);
			List<RefundInsureOrderForm> refundInsureOrderForms = new ArrayList<RefundInsureOrderForm>();
			for (InsuranceOrderDto insuranceOrderDto : insuranceOrderDtos) {
				if (insuranceOrderDto.getBpTotalAmount() != null
						&& insuranceOrderDto.getBpTotalAmount().doubleValue() > 0) {
					List<RefundOrderDto> refundOrderDtos = refundOrderService
							.findListByOrderId(insuranceOrderDto.getId().toString());
					RefundInsureOrderForm refundInsureOrderForm = new RefundInsureOrderForm();
					for (RefundOrderDto refundOrderInsure : refundOrderDtos) {
						// 得到退款成功的那一个
						if (RefundStatus.REFUNDED == refundOrderInsure.getStatus()) {
							if (refundOrderInsure.getApplyTime().getTime() >= sd.getTime()
									&& refundOrderInsure.getApplyTime().getTime() <= ed.getTime()) {
								refundInsureOrderForm.setOrderDate(refundOrderInsure.getApplyTime());
								refundInsureOrderForm.setOrderNo(refundOrderInsure.getId());
								refundInsureOrderForm.setOrderPrice(refundOrderInsure.getBpTotalAmount() == null ? 0D
										: refundOrderInsure.getBpTotalAmount().doubleValue());
								refundInsureOrderForms.add(refundInsureOrderForm);
							}
						}
					}
				}
			}
			totalOrderForm.setRefundInsureOrderForms(refundInsureOrderForms);
		}
		return totalOrderForm;
	}

	/**
	 *
	 * @param totalOrderForm
	 * @param sd
	 * @param ed
	 * @return
	 */
	private TotalOrderForm getOrderFormByOrderNo(TotalOrderForm totalOrderForm, Date sd, Date ed) {
		// 得到这个订单所有对应的保险
		List<InsuranceOrderDto> insuranceOrderDtos = insuranceOrderService
				.findInsuranceOrderByBizOrderId(totalOrderForm.getOrderForm().getOrderNo());
		/**
		 * 所对应的公司支付的保险订单
		 */
		List<InsureOrderForm> insureOrderForms = new ArrayList<InsureOrderForm>();

		for (InsuranceOrderDto insuranceOrderDto : insuranceOrderDtos) {
			// 申请日期在这个时间范围内
			if (DateUtils.parse(insuranceOrderDto.getCreateDate(), "yyyy-MM-dd HH:mm:ss").getTime() >= sd.getTime()
					&& DateUtils.parse(insuranceOrderDto.getCreateDate(), "yyyy-MM-dd HH:mm:ss").getTime() <= ed
							.getTime()) {
				InsureOrderForm insureOrderForm = new InsureOrderForm();
				insureOrderForm.setOrderNo(insuranceOrderDto.getId().toString());
				insureOrderForm.setApplyUserName(totalOrderForm.getOrderForm().getApplyUserName());
				insureOrderForm.setOrderDate(DateUtils.parse(insuranceOrderDto.getCreateDate(), "yyyy-MM-dd HH:mm:ss"));
				insureOrderForm.setOrderPrice(insuranceOrderDto.getBpTotalAmount() == null ? 0D
						: insuranceOrderDto.getBpTotalAmount().doubleValue());
				insureOrderForm.setDesc(insuranceOrderDto.getSummary());
				insureOrderForms.add(insureOrderForm);
			}

		}
		totalOrderForm.setInsureOrderForms(insureOrderForms);

		/**
		 * 所对应的退单
		 */
		List<RefundOrderForm> refundOrderForms = new ArrayList<RefundOrderForm>();
		List<RefundOrderDto> refundOrderDtos = refundOrderService
				.findListByOrderId(totalOrderForm.getOrderForm().getOrderNo());
		// 找是不是存在这样的退单
		if (CollectionUtils.isNotEmpty(refundOrderDtos)) {
			RefundOrderForm refundOrderForm = null;
			for (RefundOrderDto refundOrderDto : refundOrderDtos) {
				if (RefundStatus.REFUNDED == refundOrderDto.getStatus()) {

					if (refundOrderDto.getApplyTime().getTime() >= sd.getTime()
							&& refundOrderDto.getApplyTime().getTime() <= ed.getTime()) {
						refundOrderForm = new RefundOrderForm();
						refundOrderForm.setOrderDate(refundOrderDto.getApplyTime());
						refundOrderForm.setApplyUserName(totalOrderForm.getOrderForm().getApplyUserName());
						refundOrderForm.setPreOrderNo(refundOrderDto.getOrderId());
						refundOrderForm.setOrderNo(refundOrderDto.getId());
						refundOrderForm.setOrderPrice(refundOrderDto.getBpTotalAmount() == null ? 0D
								: refundOrderDto.getBpTotalAmount().doubleValue());
						refundOrderForms.add(refundOrderForm);
					}
				}

			}
		}
		totalOrderForm.setRefundOrderForms(refundOrderForms);

		/**
		 * 所对应的退单公司支付的保险
		 */
		List<RefundInsureOrderForm> refundInsureOrderForms = new ArrayList<RefundInsureOrderForm>();
		// 找到退单保险
		if (CollectionUtils.isNotEmpty(insureOrderForms)) {
			for (InsureOrderForm insureOrderForm : insureOrderForms) {
				List<RefundOrderDto> refundOrderInsures = refundOrderService
						.findListByOrderId(insureOrderForm.getOrderNo());// 得到该保险的退单
				if (CollectionUtils.isNotEmpty(refundOrderInsures)) {
					for (RefundOrderDto refundOrderDto : refundOrderInsures) {
						if (RefundStatus.REFUNDED == refundOrderDto.getStatus()) {
							if (refundOrderDto.getApplyTime().getTime() >= sd.getTime()
									&& refundOrderDto.getApplyTime().getTime() <= ed.getTime()) {
								RefundInsureOrderForm refundInsureOrderForm = new RefundInsureOrderForm();
								refundInsureOrderForm.setOrderDate(refundOrderDto.getApplyTime());
								refundInsureOrderForm.setOrderNo(refundOrderDto.getId());
								refundInsureOrderForm.setOrderPrice(refundOrderDto.getBpTotalAmount() == null ? 0D
										: refundOrderDto.getBpTotalAmount().doubleValue());
								refundInsureOrderForms.add(refundInsureOrderForm);
							}
						}

					}
				}
			}
			totalOrderForm.setRefundInsureOrderForms(refundInsureOrderForms);
		}
		return totalOrderForm;

	}
}
