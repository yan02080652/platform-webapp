package com.tem.payment.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.payment.api.RefundService;
import com.tem.payment.api.RefundTaskService;
import com.tem.payment.condition.RefundTaskCondition;
import com.tem.payment.dto.RefundConfirmInfo;
import com.tem.payment.dto.RefundResult;
import com.tem.payment.dto.RefundTaskDto;
import com.tem.payment.form.RefundTaskForm;
import com.tem.payment.util.PaymentConstants;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.UserDto;
import com.tem.platform.security.authorize.ContextUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 退款维护	
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("payment/refund")
public class RefundTaskController extends BaseController {
	
	@Autowired
	private RefundTaskService refundTaskService;

	@Autowired
	private RefundService refundService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DictService dictService;
	
	@RequestMapping(value = "")
	public String notifyTmplPage(Model model,Integer pageIndex, RefundTaskCondition condition,
								 String bpTmcName,String bpName) {
		
		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<RefundTaskCondition> queryDto = new QueryDto<RefundTaskCondition>(pageIndex,Page.DEFAULT_PAGE_SIZE);

		if (!super.isSuperAdmin() ) {
			condition.setTmcId(super.getTmcId());
		}

		if(condition.getStatus() == null){//默认为未处理的
			condition.setStatus(0);
		}else if(condition.getStatus().intValue() == -1){//-1是全部
			condition.setStatus(null);
		}
		queryDto.setCondition(condition);
	   
		Page<RefundTaskDto> refundTaskPage = refundTaskService.queryListWithPage(queryDto);

		//前台赋值
		if(condition.getStatus() == null){
			condition.setStatus(-1);
		}

		//转译发起人
		List<RefundTaskDto> refundTaskList = refundTaskPage.getList();
		List<RefundTaskForm> resultList = new ArrayList<RefundTaskForm>();
		if(CollectionUtils.isNotEmpty(refundTaskList)){
			RefundTaskForm refundTaskForm = null;
			for(RefundTaskDto refundTaskDto : refundTaskList){
				refundTaskForm = TransformUtils.transform(refundTaskDto, RefundTaskForm.class);
				UserDto user = userService.getUser(refundTaskDto.getStartUser());
				if(user != null){
					refundTaskForm.setStartUserName(user.getFullname());
				}
				if(refundTaskDto.getStatus() == null || refundTaskDto.getStatus().intValue() == 0){
					refundTaskForm.setStatusName("未处理");
				}else if(refundTaskDto.getStatus().intValue() == 1){
					refundTaskForm.setStatusName("已处理");
				}else if(refundTaskDto.getStatus().intValue() == 2){
					refundTaskForm.setStatusName("待确认");
				}
				String refundReasonName = dictService.getCodeTxt(PaymentConstants.DIC_REFUND_REASON, refundTaskDto.getRefundReason());
				refundTaskForm.setRefundReasonName(refundReasonName);
				resultList.add(refundTaskForm);
			}
		}
		
		Page<RefundTaskForm> resultPage = new Page<RefundTaskForm>(refundTaskPage.getStart(), refundTaskPage.getSize(), resultList, refundTaskPage.getTotal());


		model.addAttribute("bpTmcName",bpTmcName);
		model.addAttribute("bpName",bpName);
		model.addAttribute("refundTask",condition);

		model.addAttribute("pageList", resultPage);

		return "payment/refundList.base";
	}
	
	@ResponseBody
	@RequestMapping(value = "/conformRefund")
	public Message conformRefund(Model model, @RequestParam(value="id") Long id,@RequestParam(value="type") Long type) {
		if (id == null) {
			return Message.error("不存在的退款");
		}
		RefundTaskDto refundTaskDto = refundTaskService.selectById(id);
		
		if(refundTaskDto != null){
			RefundTaskDto updateRefundTask = null;
			
			RefundConfirmInfo refundInfo = new RefundConfirmInfo();
			refundInfo.setRefundPlanId(refundTaskDto.getPayRefundId().toString());
			if(type == 0){//0线上退款  1线下已退款
				refundInfo.setOnlineRefund(true);
			}else {
				updateRefundTask = new RefundTaskDto();
				updateRefundTask.setId(id);
				updateRefundTask.setUpdateDate(new Date());
				updateRefundTask.setUpdateUser(ContextUtil.getCurrentUserId());
				updateRefundTask.setPaymentChannel(super.getStringValue("paymentChannel"));
				updateRefundTask.setPaymentAccount(super.getStringValue("paymentAccount"));
				updateRefundTask.setPaymentTradeId(super.getStringValue("paymentTradeId"));
				updateRefundTask.setPaymentDestName(super.getStringValue("paymentDestName"));
				updateRefundTask.setPaymentDestAccount(super.getStringValue("paymentDestAccount"));
				
				refundInfo.setChannel(updateRefundTask.getPaymentChannel());
				refundInfo.setRefId(updateRefundTask.getPaymentTradeId());

				refundInfo.setOnlineRefund(false);
			}
			RefundResult refundResult = refundService.refundConfirm(refundInfo);
			if("0".equals(refundResult.getState())){
				if(updateRefundTask != null) {
					refundTaskService.update(updateRefundTask);
				}
				return Message.success("退款成功！");
			}else{
				return Message.error("退款异常，请刷新页面重试！");
			}
		}else{
			return Message.error("不存在该笔退款，或该退款已处理，请刷新页面！");
		}
	}
	
}
