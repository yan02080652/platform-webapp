package com.tem.payment.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.web.Message;
import com.tem.payment.api.PersonalAccountManageService;
import com.tem.payment.condition.PAccTradeRecordCondition;
import com.tem.payment.condition.PersonalAccountCondition;
import com.tem.payment.condition.WithdrawCondition;
import com.tem.payment.dto.PAccTradeRecordDto;
import com.tem.payment.dto.PersonalAccountChangeInfo;
import com.tem.payment.dto.PersonalAccountDto;
import com.tem.payment.dto.WithdrawRecordDto;
import com.tem.payment.enums.TradeType;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.UserDto;
import com.tem.payment.form.PersonalAccountForm;
import com.tem.payment.form.WithdrawChannel;
import com.tem.platform.action.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by chenjieming on 2017/6/15.
 */
@Controller
@RequestMapping("personAccount")
public class PersonalPaymentController extends BaseController {

    @Autowired
    private PersonalAccountManageService personalAccountManageService;
    @Autowired
    private UserService userService;

    //个人账户首页
    @RequestMapping("index")
    public String index(Integer pageIndex,PersonalAccountCondition condition,String bpTmcName,String bpName, Model model){
        pageIndex = pageIndex == null ? 1 : pageIndex;
        QueryDto<PersonalAccountCondition> queryDto = new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);

        if(!super.isSuperAdmin()){
            condition.setTmcId(super.getTmcId());
        }
        queryDto.setCondition(condition);

        Page<PersonalAccountDto>  pageList = personalAccountManageService.queryListWithPage(queryDto);

        List<PersonalAccountDto> list2 = getDetailList(pageList.getList());
        pageList.setList(list2);

        model.addAttribute("pageList", pageList);
        model.addAttribute("bpTmcName",bpTmcName);
        model.addAttribute("bpName",bpName);
        model.addAttribute("condition",condition);
        return "payment/personAccount/index.base";
    }

    private List<PersonalAccountDto>  getDetailList(List<PersonalAccountDto> list){
        List<Long> userIds = new ArrayList<>();
        for(PersonalAccountDto dto : list){
            userIds.add(dto.getUserId());
        }
        List<UserDto> userDtoList = userService.getUsers(userIds);
        for (PersonalAccountDto dto : list){
            for (UserDto userDto : userDtoList){
                if(dto.getUserId().equals(userDto.getId())){
                    dto.setFullname(userDto.getFullname());
                    break;
                }
            }
        }
        return list;
    }

    //个人账户详情
    @RequestMapping("detail")
    public String getById(Long id, Model model){
        PersonalAccountDto personalAccountDto = personalAccountManageService.getById(id);
        String fullname = userService.getUser(personalAccountDto.getUserId()).getFullname();
        personalAccountDto.setFullname(fullname);

        model.addAttribute("pAccount", personalAccountDto);

        return "payment/personAccount/pAccountDetail.base";
    }

    /**
     * 个人账户流水
     * @param model
     * @param pageIndex
     * @return
     */
    @RequestMapping(value="accountFlow")
    public String accountFlow(Model model,Integer pageIndex, Long userId){
        PAccTradeRecordCondition condition = new PAccTradeRecordCondition();

        String startDate = super.getStringValue("startDate");
        String endDate = super.getStringValue("endDate");
        String keyword = super.getStringValue("keyword");
        String range = super.getStringValue("range");
        if(StringUtils.isEmpty(startDate)){
            //默认是一周
            Calendar curr = Calendar.getInstance();
            curr.set(Calendar.DAY_OF_MONTH,curr.get(Calendar.DAY_OF_MONTH)-7);
            startDate = DateUtils.format(curr.getTime(), "yyyy-MM-dd");
        }
        condition.setStartDate(DateUtils.parse(startDate, DateUtils.YYYY_MM_DD));
        if(StringUtils.isEmpty(endDate)){
            endDate = DateUtils.format(new Date(), "yyyy-MM-dd");
        }
        condition.setUserId(userId);
        condition.setEndDate(DateUtils.parse(endDate, DateUtils.YYYY_MM_DD));

        if(StringUtils.isEmpty(range)){//0是周 1是月 2是年
            range = "0";
        }

        if(StringUtils.isNotEmpty(keyword)){
            condition.setKeyWord(keyword);
        }

        pageIndex = pageIndex == null ? 1 : pageIndex;
        QueryDto<PAccTradeRecordCondition> queryDto = new QueryDto<PAccTradeRecordCondition>(pageIndex,20);
        queryDto.setCondition(condition);

        Page<PAccTradeRecordDto> pAccTradeRecordList = personalAccountManageService.queryListWithPagePAcc(queryDto);

        List<PAccTradeRecordDto> list = this.transToList(pAccTradeRecordList.getList());
        pAccTradeRecordList.setList(list);

        model.addAttribute("pageList", pAccTradeRecordList);
        model.addAttribute("userId",userId);
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("keyword", keyword);
        model.addAttribute("range", range);
        return "payment/personAccount/pAccTradeRecord.jsp";
    }

    //list转换
    private List<PAccTradeRecordDto> transToList(List<PAccTradeRecordDto> list){
        List<Long> userIds = new ArrayList<Long>();
        for(PAccTradeRecordDto dto : list){
            userIds.add(dto.getUserId());
        }
        List<UserDto> userDtoList = userService.getUsers(userIds);
        for (PAccTradeRecordDto dto : list){
            for (UserDto userDto : userDtoList){
                if(dto.getUserId().equals(userDto.getId())){
                    dto.setCreateUserName(userDto.getFullname());
                    break;
                }
            }
            if("ORDER".equals(dto.getBizType())){
                dto.setTradeInstruction("订单直接消费");
            }else{
                TradeType tradeType = TradeType.valueOf(dto.getBizType());
                dto.setTradeInstruction(tradeType.getValue());
            }
        }
        return list;
    }

    /**
     * 个人账户提现记录
     * @param model
     * @param pageIndex
     * @return
     */
    @RequestMapping(value="withdrawList")
    public String withdrawList(Model model,Integer pageIndex, Long userId) {
        WithdrawCondition condition = new WithdrawCondition();

        String startDate = super.getStringValue("startDate");
        String endDate = super.getStringValue("endDate");
        String keyword = super.getStringValue("keyword");
        String range = super.getStringValue("range");
        if (StringUtils.isEmpty(startDate)) {
            //默认是一周
            Calendar curr = Calendar.getInstance();
            curr.set(Calendar.DAY_OF_MONTH, curr.get(Calendar.DAY_OF_MONTH) - 7);
            startDate = DateUtils.format(curr.getTime(), "yyyy-MM-dd");
        }
        condition.setBeginDate(DateUtils.parse(startDate, DateUtils.YYYY_MM_DD));
        if (StringUtils.isEmpty(endDate)) {
            endDate = DateUtils.format(new Date(), "yyyy-MM-dd");
        }
        condition.setUserId(userId);
        condition.setEndDate(DateUtils.parse(endDate, DateUtils.YYYY_MM_DD));

        if (StringUtils.isEmpty(range)) {//0是周 1是月 2是年
            range = "0";
        }

        if (StringUtils.isNotEmpty(keyword)) {
            condition.setKeyword(keyword);
        }

        pageIndex = pageIndex == null ? 1 : pageIndex;
        QueryDto<WithdrawCondition> queryDto = new QueryDto<>(pageIndex, 20);
        queryDto.setCondition(condition);

        Page<WithdrawRecordDto> pageList = personalAccountManageService.queryListWithPageWithdraw(queryDto);

        List<WithdrawRecordDto> list1 = new ArrayList<>();
        for(WithdrawRecordDto withdrawRecordDto : pageList.getList()){
            withdrawRecordDto.setChannel(WithdrawChannel.valueOf(withdrawRecordDto.getChannel()).getValue());
            list1.add(withdrawRecordDto);
        }
        pageList.setList(list1);

        model.addAttribute("pageList", pageList);
        model.addAttribute("userId", userId);
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("keyword", keyword);
        model.addAttribute("range", range);
        return "payment/personAccount/withdrawRecord.jsp";
    }

    //提现申请主页
    @RequestMapping("withdraw/index")
    public String withdrawIndex(){

        return "payment/withdraw/index.base";
    }

    @RequestMapping("withdraw/list")
    public String withdrawList(String keyword, Integer pageIndex, Integer status, Model model){
        Long bpId = super.getLongValue("partnerId");
        Long tmcId = super.getLongValue("tmcId");

        pageIndex = pageIndex == null ? 1 : pageIndex;
        QueryDto<WithdrawCondition> queryDto = new QueryDto<>(pageIndex, 10);
        WithdrawCondition condition = new WithdrawCondition();
        if(status != null && status != -1){
            condition.setStatus(status + "");
        }
        condition.setKeyword(keyword);

        condition.setTmcId(tmcId);
        condition.setBpId(bpId);

        if(!super.isSuperAdmin()){
            condition.setTmcId(super.getTmcId());
        }

        queryDto.setCondition(condition);

        Page<WithdrawRecordDto> pageList = personalAccountManageService.queryListWithPageWithdraw(queryDto);

        List<WithdrawRecordDto> list1 = new ArrayList<>();
        for(WithdrawRecordDto withdrawRecordDto : pageList.getList()){
            withdrawRecordDto.setChannel(WithdrawChannel.valueOf(withdrawRecordDto.getChannel()).getValue());
            list1.add(withdrawRecordDto);
        }
        pageList.setList(list1);

        model.addAttribute("pageList", pageList);
        return "payment/withdraw/withdraw-list.jsp";
    }

    @RequestMapping("withdraw/getById")
    public String withdrawGetById(Long withdrawId, Long accountId, Model model){
        PersonalAccountDto personalAccountDto = personalAccountManageService.getById(accountId);
        model.addAttribute("personalAccountDto",personalAccountDto);

        WithdrawRecordDto withdrawRecord = personalAccountManageService.getWithdrawRecordById(withdrawId);
        withdrawRecord.setChannel(WithdrawChannel.valueOf(withdrawRecord.getChannel()).getValue());
        model.addAttribute("withdrawRecord",withdrawRecord);

        return "payment/withdraw/model.jsp";
    }

    //审核提现信息
    @RequestMapping("withdraw/review")
    @ResponseBody
    public Message reviewWithdraw(Long withdrawId, Integer status){
        WithdrawRecordDto withdrawRecordDto = new WithdrawRecordDto();
        withdrawRecordDto.setId(withdrawId);
        withdrawRecordDto.setStatus(status + "");
        withdrawRecordDto.setReviewTime(new Date());
        boolean flag = personalAccountManageService.saveWithdraw(withdrawRecordDto);

        String str = "";
        if(new Integer(1).equals(status)){
            str = "审核通过操作成功";
        }else{
            str = "审核拒绝操作成功";
        }
        if(flag){
            return Message.success(str);
        }
        return Message.error("操作失败");
    }

    @RequestMapping("withdraw/sure")
    @ResponseBody
    public Message sureWithdraw(PersonalAccountForm personAcc){

        personAcc.setTradeUserId(super.getCurrentUser().getId());
        personAcc.setTradeType(TradeType.WITHDRAW.name());
        String billId = personAcc.getBillId();
        personAcc.setBillId(String.format("T_%s", billId));

        PersonalAccountChangeInfo info = TransformUtils.transform(personAcc, PersonalAccountChangeInfo.class);

        if(personAcc.getOrginAmount() < personAcc.getAmount()){
            return Message.error("提现失败,提现金额不能大于账号余额");
        }

        info.setAmount(-info.getAmount());
        ResponseInfo responseInfo = personalAccountManageService.addBalance(info);
        if("1".equals(responseInfo.getCode())){
            return Message.error(responseInfo.getData().toString());
        }

        WithdrawRecordDto withdrawRecordDto = new WithdrawRecordDto();
        withdrawRecordDto.setId(personAcc.getWithdrawId());
        withdrawRecordDto.setStatus("3");
        withdrawRecordDto.setReviewTime(new Date());
        withdrawRecordDto.setTradeId(billId);
        withdrawRecordDto.setMemo(personAcc.getMemo());
        personalAccountManageService.saveWithdraw(withdrawRecordDto);

        return Message.success("提现成功");
    }

}
