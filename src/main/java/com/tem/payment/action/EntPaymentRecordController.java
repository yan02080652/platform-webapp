package com.tem.payment.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.iplatform.common.ResponseInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.payment.api.AccountManageService;
import com.tem.payment.api.BillService;
import com.tem.payment.api.EntPaymentRecordService;
import com.tem.payment.api.LiquidActionRecordService;
import com.tem.payment.condition.EntPaymentRecordCondition;
import com.tem.payment.dto.BillDto;
import com.tem.payment.dto.BpAccountDto;
import com.tem.payment.dto.EntPaymentRecordDto;
import com.tem.payment.dto.LiquidActionRecordDto;
import com.tem.payment.dto.LiquidateBillDto;
import com.tem.payment.form.BillForm;
import com.tem.payment.form.CloseoutRecordForm;
import com.tem.payment.form.EntPaymentRecordForm;
import com.tem.payment.form.LiquidActionRecordForm;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerBpService;
import com.tem.platform.api.PartnerCorpService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerCorpDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;

/**
 * 
 * @author jieming
 *
 */
@Controller
@RequestMapping("payment/entRecord")
public class EntPaymentRecordController extends BaseController {
	
	@Autowired
	private EntPaymentRecordService	entPaymentRecordService;
	
	@Autowired
	private PartnerService	partnerService;
	
	@Autowired
	private PartnerCorpService	partnerCorpService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private LiquidActionRecordService liquidActionRecordService;
	
	@Autowired
	private BillService billService;
	
	@Autowired
	private AccountManageService accountManageService;

	@Autowired
	private PartnerBpService partnerBpService;
	
	@RequestMapping(value ="")
	public String entRecord(Model model,Integer pageIndex,EntPaymentRecordForm recordForm) throws IOException{
		if(StringUtils.isEmpty(recordForm.getLiquidationStatus())){//空的话 为未寄送
			recordForm.setLiquidationStatus("OPEN");
		}else if("ALL".equals(recordForm.getLiquidationStatus())){
			recordForm.setLiquidationStatus(null);
		}
		if(recordForm.getStatus() != null && recordForm.getStatus().intValue() == -99){
			recordForm.setStatus(null);
		}
		EntPaymentRecordCondition condition = TransformUtils.transform(recordForm, EntPaymentRecordCondition.class);
		Long bpId = super.getLongValue("bpId");
		condition.setBpId(bpId);

		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<EntPaymentRecordCondition> queryDto = new QueryDto<EntPaymentRecordCondition>(pageIndex,Page.DEFAULT_PAGE_SIZE);

		if(!super.isSuperAdmin()){
			condition.setTmcId(super.getTmcId());
		}

		queryDto.setCondition(condition);
		Page<EntPaymentRecordDto> pageList = entPaymentRecordService.queryListWithPage(queryDto);
		
		if(pageList.getList()!= null && pageList.getList().size() > 0){
			
			List<EntPaymentRecordDto> entPaymentList = pageList.getList();
			List<EntPaymentRecordDto> entPaymentList1 = new ArrayList<EntPaymentRecordDto>();
			
			Set<Long> userIds = new HashSet<Long>();
			Set<Long> partnerIds = new HashSet<Long>();
			for(EntPaymentRecordDto entPayment:entPaymentList){
				userIds.add(entPayment.getCreateUser());
				partnerIds.add(entPayment.getBpId());
			}
			List<UserDto> userList1 = userService.getUsers(new ArrayList<Long>(userIds));
			List<PartnerDto> partnerList1 = partnerService.findByBpIds(new ArrayList<Long>(partnerIds));
			
			for(EntPaymentRecordDto entPayment:entPaymentList){
				for(UserDto user : userList1){
					if(user.getId().equals(entPayment.getCreateUser())){
						entPayment.setCreateUserName(user.getFullname());
						break;
					}
				}
				for(PartnerDto partnerDto : partnerList1){
					if(partnerDto.getId().equals(entPayment.getBpId())){
						entPayment.setBpName(partnerDto.getName());
					}
				}
				entPaymentList1.add(entPayment);
			}
			pageList.setList(entPaymentList1);
		}
		model.addAttribute("recordForm", recordForm);
		model.addAttribute("pageList", pageList);
		return "payment/entPaymentRecord/enPayRecord.base";
	}
	
	/**
	 * 某一付款的详细信息
	 * @param model
	 * @param recordId
	 * @return
	 */
	@RequestMapping(value="detail")
	public String getDetail(Model model , Long recordId){
		EntPaymentRecordDto	record = entPaymentRecordService.getById(recordId);
		if(record != null){
			String bpName =	partnerService.findById(record.getBpId()).getName();
			record.setBpName(bpName);
			
			Long liquidAmount = record.getRemainAmount().longValue();
			
			if(liquidAmount == 0){
				model.addAttribute("liquidAmountTxt", "已清算" );
			}else{
				Double liquidAmount1 = liquidAmount/100D;
				model.addAttribute("liquidAmountTxt", "剩余可用于清账金额：" + liquidAmount1 + "元");	
			}
			
			List<CloseoutRecordForm> liquiAction = getLiquiAction(record.getId());
			model.addAttribute("recordList1", liquiAction);
			
		}
		model.addAttribute("record", record);
		return "payment/entPaymentRecord/enPayRecord-model.base";
	}
	
	/**
	 * 保存付款信息
	 * @param model
	 * @param form
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value="save")
	public Message save(Model model , EntPaymentRecordForm form) throws IOException{
		EntPaymentRecordDto dto = TransformUtils.transform(form, EntPaymentRecordDto.class);

		String remark1 = form.getRemark1() == null ? "" : form.getRemark1();
		String remark2 = form.getRemark2() == null ? "" : form.getRemark2();

		String remark = remark1 + "$$" + remark2;
		dto.setRemark(remark);

		if(dto.getCorpId() != null){
			PartnerCorpDto byId = partnerCorpService.getById(dto.getCorpId());
			if(byId == null){
				dto.setCorpId(null);
				return Message.error("企业法人不存在");
			}else{
				if(byId.getPaymentAccount() == null){
					return Message.error("来款的法人未绑定账户");
				}
			}
		}
		dto.setStatus(0);
		
		if(form.getAmount() != null){
			dto.setAmount((int)(form.getAmount() * 100));
			dto.setRemainAmount((int)(form.getAmount() * 100));
		}
		
		dto.setCreateUser(super.getCurrentUser().getId());

		if(form.getBpId() != null){
			Long tmcId = partnerBpService.findTmcIdByPId(form.getBpId());
			dto.setTmcId(tmcId);
		}

		if(dto.getAmount() > 0) {//正数，表示来示
			dto.setLiquidationStatus("OPEN");
		} else if(dto.getAmount() == 0) {//不能为0
			return Message.error("金额不能为0");
		} else {//负数通常用于调账，不需要清账
			dto.setLiquidationStatus("CLOSE");
			dto.setRemainAmount(0);
		}

		Long recordId= entPaymentRecordService.save(dto);
		return	Message.success(recordId.toString());
	}
	
	/**
	 * 得到某一公司的账单列表
	 * @param model
	 * @param bpId 企业id
	 * @param paymentId 付款id
	 * @return
	 */
	@RequestMapping("getBillList")
	public String getBillList(Model model,Long bpId,Long paymentId){
		EntPaymentRecordDto	record = entPaymentRecordService.getById(paymentId);
		int remainAmount = record.getRemainAmount();
		
		Long accountId = null;
		if(record.getCorpId() != null){
			PartnerCorpDto partnerCorpDto = partnerCorpService.getById(record.getCorpId());
			accountId = partnerCorpDto.getPaymentAccount();
		}
		List<BillForm> billFroms = getBillList(bpId, accountId,remainAmount);

		model.addAttribute("remainAmount", remainAmount/100D);
		model.addAttribute("paymentId", paymentId);
		model.addAttribute("bills", billFroms);
		
		if(accountId != null){
			BpAccountDto account = accountManageService.getById(accountId);
			if(account != null){
				model.addAttribute("account", account);
			}
			
		}
		return "payment/entPaymentRecord/billListModel.jsp";
	}
	
	/**
	 * 根据条件得到待清账的账单列表
	 * @param model
	 * @param bpId 公司id
	 * @param accountId 账户id
	 * @param paymentId 付款id
	 * @return
	 */
	@RequestMapping("findBillList")
	public String findBillList(Model model ,Long bpId,Long accountId,Long paymentId){
		EntPaymentRecordDto	record = entPaymentRecordService.getById(paymentId);
		int remainAmount = record.getRemainAmount();
		
		List<BillForm> billFroms = getBillList(bpId, accountId ,remainAmount);

		model.addAttribute("remainAmount", remainAmount/100D);
		model.addAttribute("bills", billFroms);
		return "payment/entPaymentRecord/bill-model.jsp";
	}

	/**
	 * 给某一付款，寻找账单清账  如果此付款是某一账户下的则先消该账户  如果不是则分配到公司下 默认按久的分配  
	 * 现在修改为只能请某一账户下的账单
	 * @param bpId
	 * @param accountId
	 * @param remainAmount
	 * @return
	 */
	public List<BillForm> getBillList(Long bpId, Long accountId, int remainAmount){
		List<BillDto> billDtos = new ArrayList<BillDto>();
		if(accountId == null){
			/*List<BpAccountDto> bpAccountDtos = accountManageService.getAllByBpId(bpId);//得到公司所有账户下的账单
			if(CollectionUtils.isNotEmpty(bpAccountDtos)){
				for(BpAccountDto bpAccountDto : bpAccountDtos){
					List<BillDto> billList = billService.getUnLiquidationByAccountId(bpAccountDto.getId());
					billDtos.addAll(billList);
				}
			}*/
		}else{
			List<BillDto> billList = billService.getUnLiquidationByAccountId(accountId);
			billDtos.addAll(billList);
		}
		
		//账单按时间升序 越久的在上面
		Collections.sort(billDtos, new Comparator<BillDto>() {
			@Override
			public int compare(BillDto o1, BillDto o2) {
				return (int) (o1.getCreateDate().getTime()-o2.getCreateDate().getTime());
			}
		});
		
		List<BillForm> billFroms = new ArrayList<BillForm>();
		if(CollectionUtils.isNotEmpty(billDtos)){
			BillForm billForm = new BillForm();
			for(BillDto billDto : billDtos){
				if("CONFIRM".equals(billDto.getStatus())){
					billForm = TransformUtils.transform(billDto, BillForm.class);
					if(billDto.getCreateUser() != null){
						UserDto user = userService.getUser(billDto.getCreateUser());
						if(user != null){
							billForm.setCreateUserName(user.getFullname());
						}
					}
					if(billDto.getAccountId() != null){
						BpAccountDto bpAccountDto = accountManageService.getById(billDto.getAccountId());
						if(bpAccountDto != null){
							billForm.setAccountName(bpAccountDto.getAccountName());
						}
					}
					if(remainAmount < 0){
						billForm.setRemainAmountInput(0.0);
					}else{
						remainAmount -= billDto.getRemainAmount();
						if(remainAmount > 0){
							billForm.setRemainAmountInput(billDto.getRemainAmount()/100.0);
						}else{
							billForm.setRemainAmountInput((billDto.getRemainAmount() + remainAmount)/100.0);
						}
					}
					if(billForm.getRemainAmount().intValue() > 0) {
						billFroms.add(billForm);
					}
				}
			}
		}
	  return  billFroms;
	}
	
	
	//生成清账户记录
	@RequestMapping("saveLiquidActionRecord")
	public void saveLiquidActionRecord(LiquidActionRecordForm la) throws IOException{
		Long paymentId = la.getPaymentId();
		EntPaymentRecordDto epr = entPaymentRecordService.getById(paymentId);
		if(epr.getTmcId().longValue() != super.getTmcId().longValue()) {
			throw new RuntimeException("非法操作");
		}
		
		List<LiquidateBillDto> liqBills = new ArrayList<LiquidateBillDto>();
		if(CollectionUtils.isNotEmpty(la.getBillFrom())){
			for(BillForm billForm: la.getBillFrom()){
				LiquidateBillDto liqBill = new LiquidateBillDto();
				liqBill.setBillId(billForm.getId());
				liqBill.setLiqAmount((long)(billForm.getRemainAmountInput() * 100));
				
				liqBills.add(liqBill);
			}
		}
		
		entPaymentRecordService.liquidateBill(paymentId, liqBills, super.getCurrentUser().getId());
		
		getResponse().sendRedirect(getRequest().getContextPath() + "/payment/entRecord/detail?recordId="+paymentId+"");
	}
	
	//获取清账记录列表
	public List<CloseoutRecordForm> getLiquiAction(Long paymentId){
		List<LiquidActionRecordDto> recordList = liquidActionRecordService.getByPaymentId(paymentId);
		
		List<CloseoutRecordForm> recordList1 = new ArrayList<CloseoutRecordForm>();
		if(recordList != null && recordList.size() > 0){
			Set<Long> userIds = new HashSet<Long>();
			for(LiquidActionRecordDto recordDto : recordList){
				userIds.add(recordDto.getCreateUser());
			}
			List<UserDto> userList1 = userService.getUsers(new ArrayList<Long>(userIds));
			
			CloseoutRecordForm closeoutRecordForm = null;;
			for (LiquidActionRecordDto liquidActionRecordDto : recordList) {
				closeoutRecordForm = TransformUtils.transform(liquidActionRecordDto, CloseoutRecordForm.class);
				BillDto billDto = billService.selectById(liquidActionRecordDto.getBillId());
				if(billDto != null){
					String billRemark = billDto.getRemark();
					closeoutRecordForm.setBillRemark(billRemark);
					closeoutRecordForm.setLiqDateBegin(billDto.getLiqDateBegin());
					closeoutRecordForm.setLiqDateEnd(billDto.getLiqDateEnd());
					
					BpAccountDto bpAccountDto = accountManageService.getById(billDto.getAccountId());
					if(bpAccountDto != null){
						closeoutRecordForm.setAccountName(bpAccountDto.getAccountName());
					}
				}
				for(UserDto user : userList1){
					if(user.getId().equals(liquidActionRecordDto.getCreateUser())){
						closeoutRecordForm.setCreateUserName(user.getFullname());
						break;
					}
				}
				recordList1.add(closeoutRecordForm);
			}
		}
		return recordList1;
	}
	
	/**
	 * 确认付款
	 * @param model
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="confirmEntRecord")
	public	Message confirmEntRecord(Model model,@RequestParam("id")Long id){
		ResponseInfo res = entPaymentRecordService.confirmEntRecord(id, super.getCurrentUser().getId());
		if (res.isSuccess()) {
			return Message.success("处理成功");
		}
		return Message.error(res.getMsg());
	}
}
