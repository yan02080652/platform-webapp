package com.tem.payment.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.DateUtils;
import com.tem.payment.api.AccountManageService;
import com.tem.payment.api.BillService;
import com.tem.payment.api.InvoiceService;
import com.tem.payment.condition.InvoiceCondition;
import com.tem.payment.dto.BillDto;
import com.tem.payment.dto.BpAccountDto;
import com.tem.payment.dto.InvoiceDto;
import com.tem.payment.enums.ShippingStatus;
import com.tem.platform.api.DictService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.payment.Constants;
import com.tem.payment.form.InvoiceForm;
import com.tem.platform.action.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author jieming
 *
 */
@Controller
@RequestMapping("payment/invoice")
public class InvoiceController extends BaseController {
	
	@Autowired
	private PartnerService	partnerService;
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private BillService billService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccountManageService accountManageService;
	
	@RequestMapping(value ="")
	public String entRecord(Model model,Integer pageIndex,InvoiceForm invoiceForm) {
		if(StringUtils.isEmpty(invoiceForm.getShippingStatus())){//空的话 为未寄送
			invoiceForm.setShippingStatus("NOTSHIP");
		}else if("ALL".equals(invoiceForm.getShippingStatus())){
			invoiceForm.setShippingStatus(null);
		}
		if(StringUtils.isEmpty(invoiceForm.getLiqStatus())){//空的话 为未结清
			invoiceForm.setLiqStatus("OPEN");
		}else if("ALL".equals(invoiceForm.getLiqStatus())){
			invoiceForm.setLiqStatus(null);
		}
		InvoiceCondition invoiceCon = TransformUtils.transform(invoiceForm, InvoiceCondition.class);

		if(!super.isSuperAdmin()){
			invoiceCon.setTmcId(super.getTmcId());
		}
		invoiceCon.setBpId(invoiceForm.getPartnerId());

		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<InvoiceCondition> queryDto = new QueryDto<InvoiceCondition>(pageIndex,Page.DEFAULT_PAGE_SIZE);
		queryDto.setCondition(invoiceCon);
		Page<InvoiceDto> pageList = invoiceService.queryListWithPage(queryDto);
		
		if(pageList.getList() != null && pageList.getList().size() > 0){
			List<DictCodeDto> codes = dictService.getCodes(Constants.PAY_INVOICE_TYPE);
			Set<Long> partnerIds = new HashSet<Long>();
			for(InvoiceDto invoice : pageList.getList()){
				partnerIds.add(invoice.getBpId());
			}
			List<PartnerDto> partnerDtos = partnerService.findByBpIds(new ArrayList<Long>(partnerIds));
			
			List<InvoiceDto> invoiceList =new ArrayList<InvoiceDto>();
			
			for(InvoiceDto  invoiceDto : pageList.getList()){
				for (DictCodeDto dic : codes) {
					if(dic.getItemCode().equals(invoiceDto.getInvoiceType())){
						invoiceDto.setInvoiceTypeName(dic.getItemTxt());
						break;
					}
				}
				for(PartnerDto partnerDto : partnerDtos){
					if(partnerDto.getId().equals(invoiceDto.getBpId())){
						invoiceDto.setBpName(partnerDto.getName());
						break;
					}
				}
				ShippingStatus shippingStatus = ShippingStatus.valueOf(invoiceDto.getShippingStatus());
				invoiceDto.setShippingStatusName(shippingStatus.getMsg());
				invoiceList.add(invoiceDto);
			}
			pageList.setList(invoiceList);
		}
		
		model.addAttribute("invoiceForm", invoiceForm);
		model.addAttribute("pageList", pageList);
		return "payment/invoice/invoice.base";
	}
	
	@RequestMapping(value = "detail")
	public String detail(Model model,Long invoiceId){
		InvoiceDto invoice = invoiceService.getById(invoiceId);
		if(invoice != null){
			String bpName = partnerService.findById(invoice.getBpId()).getName();
			invoice.setBpName(bpName);
			BillDto BillDto = billService.selectById(invoice.getBillId());
			BpAccountDto bpAccountDto = accountManageService.getById(BillDto.getAccountId());
			if(bpAccountDto != null){
				model.addAttribute("accountName", bpAccountDto.getAccountName());
			}
			String billRemark = billService.selectById(invoice.getBillId()).getRemark();
			ShippingStatus shippingStatus = ShippingStatus.valueOf(invoice.getShippingStatus());
			String invoiceTypeName = dictService.getCodeTxt(Constants.PAY_INVOICE_TYPE, invoice.getInvoiceType());
			
			if(invoice.getCreateUser() != null){
				UserDto userDto = userService.getUser(invoice.getCreateUser());
				if(userDto != null){
					model.addAttribute("openUserName", userDto.getFullname());
				}
			}
			if("未寄送".equals(shippingStatus.getMsg())){
				invoice.setShippingStatusName("确认寄送");
			}else{
				invoice.setShippingStatusName(shippingStatus.getMsg());
			}
			invoice.setBillRemark(billRemark);
			invoice.setInvoiceTypeName(invoiceTypeName);
		}
		
		model.addAttribute("invoice", invoice);
		return "payment/invoice/invoice-model.base";
	}
	
	@RequestMapping(value = "update")
	@ResponseBody
	public String updateShippingStatus(Long invoiceId,String shippingInfo,String shippingDate){
		
		InvoiceDto inDto = new InvoiceDto();
		inDto.setId(invoiceId);
		inDto.setShippingStatus(ShippingStatus.SHIPED.name());
		inDto.setShippingInfo(shippingInfo);
		inDto.setShippingDate(DateUtils.parse(shippingDate));
		invoiceService.update(inDto);
		
		return "success";
	}
	
}
