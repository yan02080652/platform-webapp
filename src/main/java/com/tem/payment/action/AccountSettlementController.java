package com.tem.payment.action;

import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.web.Message;
import com.tem.flight.api.order.FlightOrderService;
import com.tem.flight.dto.order.FlightOrderCondition;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.hotel.api.HotelOrderService;
import com.tem.hotel.dto.order.HotelOrderCondition;
import com.tem.hotel.dto.order.HotelOrderDto;
import com.tem.hotel.dto.order.enums.OrderShowStatus;
import com.tem.hotel.dto.order.enums.PaymentStatus;
import com.tem.international.api.InternationalFlightOrderService;
import com.tem.international.order.InternationCondition;
import com.tem.international.dto.InternationalFlightOrderDto;
import com.tem.payment.Constants;
import com.tem.payment.api.*;
import com.tem.payment.condition.BpAccTradeRecordCondition;
import com.tem.payment.dto.*;
import com.tem.payment.enums.PayChannel;
import com.tem.payment.form.BillForm;
import com.tem.payment.form.BpAccTradeRecordForm;
import com.tem.payment.util.ExportExcel;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.*;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.PartnerCorpDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.product.api.GeneralOrderService;
import com.tem.product.api.InsuranceOrderService;
import com.tem.product.condition.GeneralOrderCondition;
import com.tem.product.dto.general.GeneralOrderDto;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.dto.refund.RefundOrderCondition;
import com.tem.pss.dto.refund.RefundOrderDto;
import com.tem.pss.dto.refund.RefundStatus;
import com.tem.train.api.order.TrainOrderService;
import com.tem.train.dto.order.TrainOrderCondition;
import com.tem.train.dto.order.TrainOrderDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 企业结算	
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("bpAccount/settlement")
public class AccountSettlementController extends BaseController {
	@Autowired
	private UserService userService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private AccountManageService accountManageService;
	
	@Autowired
	private	PartnerCorpService partnerCorpService;
		
	@Autowired
	private	PartnerService partnerService;
	
	@Autowired
	private	BillService billService;
	
	@Autowired
	private	InvoiceService invoiceService;
	
	@Autowired
	private	LiquidActionRecordService liquidActionRecordService;
	
	@Autowired
	private	BpAccTradeRecordService bpAccTradeRecordService;

	@Autowired
	private PartnerBpService partnerBpService;

	@Autowired
	private BillOrderService billOrderService;
	
	/**
	 * 账户详情 包括订单信息和流水信息
	 * @param model
	 * @param id 账户id
	 * @return
	 */
	@RequestMapping(value="accountDetail")
	public String accountDetail(Model model,@RequestParam(required=false) Long id){
		BpAccountDto bpAccount = accountManageService.getById(id);
		if(bpAccount.getCreditAmount() == null){
			bpAccount.setCreditAmount(0L);
		}
		if(bpAccount.getBalanceAmount() == null){
			bpAccount.setBalanceAmount(0L);
		}
		model.addAttribute("bpAccount", bpAccount);
		model.addAttribute("nowDate", new Date());//当前统计时间
		
		return "payment/settlement/settlementDetail.base";
	}
	
	/**
	 * 得到账单信息
	 * @param model
	 * @param id 账户id
	 * @return
	 */
	@RequestMapping(value="billList")
	public String billList(Model model,@RequestParam(required=false) Long id){
		List<BillDto> billDtos = billService.getUnLiquidationByAccountId(id);//得到所有未清账的账单
		List<BillForm> billFroms = new ArrayList<BillForm>();
		if(CollectionUtils.isNotEmpty(billDtos)){
			BillForm billForm = null;
			for(BillDto billDto : billDtos){
				billForm = TransformUtils.transform(billDto, BillForm.class);
				if(billDto.getCreateUser() != null){
					UserDto user = userService.getUser(billDto.getCreateUser());
					if(user != null){
						billForm.setCreateUserName(user.getFullname());
					}
				}
				billFroms.add(billForm);
			}
		}
		model.addAttribute("bills", billFroms);
		model.addAttribute("accountId", id);
		
		return "payment/settlement/billList.jsp";
	}

	@ResponseBody
	@RequestMapping(value = "deleteAdjitem")
	public boolean deleteAdjitemAmount(Long id){
		return 	billService.deleteBillAdjitemById(id);
	}
	
	@ResponseBody
	@RequestMapping(value="getSettleAmount")
	public Double getSettleAmount(@RequestParam("id")Long id){

		return billOrderService.getSettleAmount(id);
	}
	
	/**
	 * 账户流水
	 * @param model
	 * @param id 账户id
	 * @param pageIndex
	 * @return
	 */
	@RequestMapping(value="accountFlow")
	public String accountFlow(Model model,@RequestParam(required=false) Long id,@RequestParam(value="pageIndex",required=false)Integer pageIndex,@RequestParam(value="accountId",required=false)Long accountId){
		BpAccTradeRecordCondition bpAccTradeRecordCondition = new BpAccTradeRecordCondition();
		
		String startDate = super.getStringValue("startDate");
		String endDate = super.getStringValue("endDate");
		String keyword = super.getStringValue("keyword");
		String range = super.getStringValue("range");
		if(StringUtils.isEmpty(startDate)){
			//默认是一周
			Calendar curr = Calendar.getInstance();
			curr.set(Calendar.DAY_OF_MONTH,curr.get(Calendar.DAY_OF_MONTH)-7);
			startDate = DateUtils.format(curr.getTime(), "yyyy-MM-dd");
		}
		Date st = DateUtils.parse(startDate + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
		st = DateUtils.getDayBegin(st);
		bpAccTradeRecordCondition.setStartQueryDate(st);
		if(StringUtils.isEmpty(endDate)){
			endDate = DateUtils.format(new Date(), "yyyy-MM-dd");
		}

		Date ed = DateUtils.parse(endDate + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
		ed = DateUtils.getDayEnd(ed);
		bpAccTradeRecordCondition.setEndQueryDate(ed);
		
		if(StringUtils.isEmpty(range)){//0是周 1是月 2是年
			range = "0";
		}
		
		if(StringUtils.isNotEmpty(keyword)){
			bpAccTradeRecordCondition.setKeyWord(keyword);
			bpAccTradeRecordCondition.setOrderNo(keyword);
		}
		bpAccTradeRecordCondition.setAccountId(id);
		
		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<BpAccTradeRecordCondition> queryDto = new QueryDto<BpAccTradeRecordCondition>(pageIndex,20);
		queryDto.setCondition(bpAccTradeRecordCondition);
		
		Page<BpAccTradeRecordDto> bpAccTradeRecordList = bpAccTradeRecordService.queryListWithPage(queryDto);
		
		List<BpAccTradeRecordDto> list = bpAccTradeRecordList.getList();
		List<BpAccTradeRecordForm> resultList = transToBpAccTradeRecordForm(list);
		
		
		Page<BpAccTradeRecordForm> resultListPage = new Page<BpAccTradeRecordForm>(bpAccTradeRecordList.getStart(), bpAccTradeRecordList.getSize(), resultList, bpAccTradeRecordList.getTotal());
		
		model.addAttribute("pageList", resultListPage);
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		model.addAttribute("keyword", keyword);
		model.addAttribute("range", range);
		model.addAttribute("id", id);
		return "payment/settlement/bpAccTradeRecord.jsp";
	}
	
	private List<BpAccTradeRecordForm> transToBpAccTradeRecordForm(List<BpAccTradeRecordDto> list){
		List<BpAccTradeRecordForm> resultList = new ArrayList<BpAccTradeRecordForm>();
		if(CollectionUtils.isNotEmpty(list)){
			BpAccTradeRecordForm bpAccTradeRecordForm = null;
			for(BpAccTradeRecordDto bpAccTradeRecordDto : list){
				bpAccTradeRecordForm = TransformUtils.transform(bpAccTradeRecordDto, BpAccTradeRecordForm.class);
				if(bpAccTradeRecordDto.getCreateUser() != null){
					UserDto user = userService.getUser(bpAccTradeRecordDto.getCreateUser());
					if(user != null){
						bpAccTradeRecordForm.setCreateUserName(user.getFullname());
					}
				}
				
				if(bpAccTradeRecordDto.getTradeType() != null){
					bpAccTradeRecordForm.setTradeInstruction(bpAccTradeRecordDto.getTradeType().getValue());
				}
				resultList.add(bpAccTradeRecordForm);
			}
		}
		return resultList;
	}


	@RequestMapping(value = "exportBill")
	public void exportBill(Long id){

		try {
			String fileName = "账单导出.xls";

			//获得请求文件名
			getRequest().setCharacterEncoding("UTF-8");
			BufferedInputStream bis = null;
			BufferedOutputStream bos = null;

			//获取项目根目录
			String ctxPath = getRequest().getSession().getServletContext().getRealPath("");

			//获取下载文件露肩
			String downLoadPath = ctxPath+"/resource/excel/"+ fileName;

			BillDetailDto billDetail = billService.getBillDetail(id);

			this.getExportList(billDetail, downLoadPath);

			//获取文件的长度
			long fileLength = new File(downLoadPath).length();

			//设置文件输出类型
			getResponse().setContentType("application/octet-stream");
			getResponse().setHeader("Content-disposition", "attachment; filename="
					+ new String(fileName.getBytes("utf-8"), "ISO8859-1"));
			//设置输出长度
			getResponse().setHeader("Content-Length", String.valueOf(fileLength));
			//获取输入流
			bis = new BufferedInputStream(new FileInputStream(downLoadPath));
			//输出流
			bos = new BufferedOutputStream(getResponse().getOutputStream());
			byte[] buff = new byte[2048];
			int bytesRead;
			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
			}
			//关闭流
			bis.close();
			bos.close();
		} catch (Exception e) {

			e.printStackTrace();

		}

	}

	//获取导出列表的数据
	private boolean getExportList(BillDetailDto billDetail, String filePath){
		ExportExcel ex = new ExportExcel();
		try
		{
			OutputStream out = new FileOutputStream(filePath);
			ex.exportExcel(billDetail, out, "yyyy-MM-dd HH:mm");
			out.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 创建或修改账单
	 * @param model
	 * @param id
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="sendBill")
	public	Message	sendBill(Model model, @RequestParam("billId")Long id){
		if(id != null){
			billService.sendBill(id, super.getCurrentUser().getId());
		}
		
		return Message.success("操作成功");
	}
	
	/**
	 * 创建或修改账单
	 * @param model
	 * @param billDto
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="saveOrUpdateBill")
	public	Message	saveOrUpdateBill(Model model,BillDto billDto,String saveMode){
		if(billDto.getId() != null){//修改
			List<LiquidActionRecordDto> liquidList = liquidActionRecordService.getByBillId(billDto.getId()); 
			Integer liquidAmount = 0;//已清账金额
			if(liquidList != null){
				for(LiquidActionRecordDto lr : liquidList){
					liquidAmount += lr.getLiquidationAmount();
				}
			}
			billDto.setRemainAmount(billDto.getBillAmount() - liquidAmount);
			
			billDto.setUpdateUser(super.getCurrentUser().getId());
			billDto.setUpdateDate(new Date());
			billService.update(billDto);
		}else{//新增
			billDto.setBillAmount(0);
			billDto.setInvoiceAmount(0);
			billDto.setOrderAmount(0);
			billDto.setAdjustAmount(0);
			billDto.setStatus("DRAFT");
			billDto.setRemainAmount(billDto.getBillAmount());
			billDto.setCreateDate(new Date());
			billDto.setCreateUser(super.getCurrentUser().getId());
			billDto.setLiquidationStatus("OPEN");
			billDto.setUpdateUser(super.getCurrentUser().getId());
			billDto.setUpdateDate(new Date());

			if(billDto.getBpId() != null){
				Long tmcId = partnerBpService.findTmcIdByPId(billDto.getBpId());
				billDto.setTmcId(tmcId);
			}

			billDto = billService.save(billDto);
		}
		
		return Message.success(billDto.getId().toString());
	}
	
	@ResponseBody
	@RequestMapping(value="saveOrUpdateBillAdjitem")
	public	Message	saveOrUpdateBillAdjitem(Model model,BillAdjitemDto billAdjitemDto){
		billService.saveBillAdjitem(billAdjitemDto);
		return Message.success("添加成功！");
	}
	
	
	@ResponseBody
	@RequestMapping(value="getRefundOrderType")
	public	Message	getRefundOrderType(Model model,String orderNo){
		String msg = "";
		OrderBizType orderBizType = OrderBizType.valueOfByOrderId(orderNo);
		if(orderBizType.name().equals(OrderBizType.TRAIN.name())){
			msg = "TRAIN";
		}else{
			msg = orderBizType.name();
		}
		
		return Message.success(msg);
	}
	
	/**
	 * 开票
	 * @param model
	 * @param billId
	 * @return
	 */
	@RequestMapping(value="openInvoice")
	public String openInvoice(Model model,@RequestParam(value="billId",required=false)Long billId){
		BillDto billDto = billService.selectById(billId);
		BillForm billForm = null;
		if(billDto != null){
			billForm = TransformUtils.transform(billDto, BillForm.class);
			if(billForm.getBpId() != null){
				PartnerDto partnerDto = partnerService.findById(billForm.getBpId());
				if(partnerDto != null){
					billForm.setBpName(partnerDto.getName());
				}
			}
			if(billForm.getAccountId() != null){
				BpAccountDto bpAccountDto = accountManageService.getById(billForm.getAccountId());
				if(bpAccountDto != null){
					billForm.setAccountName(bpAccountDto.getAccountName());
				}
			}
			if(billForm.getInvoiceAmount() == null){
				billForm.setInvoiceAmount(0);
			}
			if(billForm.getBillAmount() == null){
				billForm.setBillAmount(0);
			}
		}
		
		List<PartnerCorpDto> corpAccounts = partnerCorpService.getByPaymentAccount(billForm.getAccountId().toString());//得到使用改账户的所有法人
		if(corpAccounts != null){
			model.addAttribute("corpAccount", corpAccounts.get(0).getId());
			model.addAttribute("corpAccountName", corpAccounts.get(0).getInvoiceTitle());
		}
		
		List<DictCodeDto> invoiceTypes = dictService.getCodes(Constants.PAY_INVOICE_TYPE);
		model.addAttribute("billForm", billForm);
		model.addAttribute("invoiceTypes", invoiceTypes);
		model.addAttribute("curUserName", super.getCurrentUser().getFullname());
		return "payment/settlement/openInvoice.jsp";
	}
	
	
	@ResponseBody
	@RequestMapping(value="getAllCorps")
	public List<PartnerCorpDto> getAllCorps(Model model,@RequestParam(value="billId",required=false)Long billId){
		BillDto billDto = billService.selectById(billId);
		List<PartnerCorpDto> corps = partnerCorpService.getByPartnerId(billDto.getBpId());//得到该公司的所有法人
		return corps;
	}
	
	@ResponseBody
	@RequestMapping(value="saveInvoice")
	public	Message	saveInvoice(InvoiceDto invoiceDto){
		//检查受票方是手选的还是手输入的
		boolean isSelect = false;
		List<PartnerCorpDto> corps = partnerCorpService.getByPartnerId(invoiceDto.getBpId());
		if(CollectionUtils.isNotEmpty(corps)){
			for(PartnerCorpDto pc : corps){
				if(invoiceDto.getInvoiceDest().equals(pc.getInvoiceTitle())){
					invoiceDto.setInvoiceDestCorpid(pc.getId());
					isSelect = true;
					break;
				}
			}
		}
		if(!isSelect){
			invoiceDto.setInvoiceDestCorpid(null);
		}
		//检查如果此账单已经清账 则开票直接已清账 否则为未清账
		BillDto bill = billService.selectById(invoiceDto.getBillId());
		if(bill != null){
			if(bill.getLiquidationStatus() == null || "OPEN".equals(bill.getLiquidationStatus())){
				invoiceDto.setLiqStatus("OPEN");
			}else{
				invoiceDto.setLiqStatus("CLOSE");
			}
		}else{
			return Message.error("此账单不存在！");
		}
		
		invoiceDto.setCreateDate(new Date());
		invoiceDto.setCreateUser(super.getCurrentUser().getId());

		if(invoiceDto.getShippingDate().compareTo(DateUtils.parse("1970-01-01")) == 0){
			invoiceDto.setShippingDate(null);
		}

		invoiceService.save(invoiceDto);
		return Message.success("保存成功");
	}

	
}
