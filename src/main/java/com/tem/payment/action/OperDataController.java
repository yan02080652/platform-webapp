package com.tem.payment.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tem.train.api.bi.TrainBiService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.car.api.service.bi.ICarBiService;
import com.tem.flight.api.bi.FlightBiService;
import com.tem.hotel.api.bi.HotelBiService;
import com.tem.international.api.bi.InternationalFlightBiService;
import com.tem.international.dto.bi.BiQueryDto;
import com.tem.payment.api.AccountManageService;
import com.tem.payment.api.BillService;
import com.tem.payment.condition.BillCondition;
import com.tem.payment.dto.BillDto;
import com.tem.payment.dto.BpAccountDto;
import com.tem.payment.form.BiQueryForm;
import com.tem.payment.form.BillForm;
import com.tem.payment.form.OrderOriginCount;
import com.tem.payment.form.OrderTypeCount;
import com.tem.payment.form.PartnerOrderForm;
import com.tem.payment.form.PaymentMethod;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.product.api.GeneralBiService;
import com.tem.product.api.InsuranceBiService;
import com.tem.pss.api.bi.RefundBiService;
import com.tem.pss.dto.bi.PartnerOrderAmountDto;
import com.tem.train.api.order.TrainOrderService;

@Controller
@RequestMapping("payment/operData")
public class OperDataController extends BaseController {
	private static final Logger logger = LoggerFactory.getLogger(OperDataController.class);
	@Autowired
	private FlightBiService flightBiService;
	@Autowired
	private HotelBiService hotelBiService;
	@Autowired
	private TrainBiService trainBiService;
	@Autowired
	private TrainOrderService trainOrderService;
	@Autowired
	private GeneralBiService generalBiService;
	@Autowired
	private InsuranceBiService insuranceBiService;
	@Autowired
	private ICarBiService carBiService;
	@Autowired
	private RefundBiService refundBiService;
	@Autowired
	private BillService billService;
	@Autowired
	private PartnerService partnerService;
	@Autowired
	private UserService userService;
	@Autowired
	private AccountManageService accountManageService;
	@Autowired
	private InternationalFlightBiService internationalFlightBiService;

	@RequestMapping("index")
	public String index(){
		return "payment/operData/partnerOrder.base";
	}

	@RequestMapping("orderList")
	public String orderList(BiQueryForm biQueryForm, Model model){

		if(biQueryForm.getEndDate() != null){

			biQueryForm.setEndDate(DateUtils.getDayEnd(biQueryForm.getEndDate()));
		}
		if(StringUtils.isEmpty(biQueryForm.getTmcId())){
			biQueryForm.setTmcId(null);
		}

		if (!super.isSuperAdmin()) {
			biQueryForm.setTmcId(String.valueOf(super.getTmcId()));
		}

		if(StringUtils.isEmpty(biQueryForm.getTravelType())){
			biQueryForm.setTravelType(null);
		}
		
		LogUtils.debug(logger, "综合订单查询,查询条件{}",JSONObject.toJSONString(biQueryForm));
		List<PartnerOrderAmountDto> order1 = flightBiService.findOrderAmountByPartner
				(TransformUtils.transform(biQueryForm, com.tem.flight.dto.bi.BiQueryDto.class));
		List<PartnerOrderAmountDto> orderInternational = internationalFlightBiService.findOrderAmountByPartner
				(TransformUtils.transform(biQueryForm, BiQueryDto.class));

		List<PartnerOrderAmountDto> order2 = hotelBiService.findOrderAmountByPartner
				(TransformUtils.transform(biQueryForm, com.tem.hotel.dto.bi.HotelBiQueryDto.class)).getData();
		List<PartnerOrderAmountDto> order3 = trainOrderService.findOrderAmountByPartner
				(TransformUtils.transform(biQueryForm, com.tem.train.dto.bi.BiQueryDto.class));
		List<PartnerOrderAmountDto> order4 = generalBiService.findOrderAmountByPartner
				(TransformUtils.transform(biQueryForm, com.tem.product.dto.general.BiQueryDto.class));
		List<PartnerOrderAmountDto> order5 = insuranceBiService.findOrderAmountByPartner
				(TransformUtils.transform(biQueryForm, com.tem.product.dto.general.BiQueryDto.class));
		
		List<PartnerOrderAmountDto> carOrder = carBiService.findOrderAmountByPartner
		(TransformUtils.transform(biQueryForm, com.tem.car.dto.bi.CarBiQueryDto.class)).getData();
		LogUtils.debug(logger, "综合订单查询,用车订单返回数据{}",JSONObject.toJSONString(carOrder));
		//退单金额   根据业务线区分，根据企业区分 
		List<PartnerOrderAmountDto> refundList = refundBiService.findOrderAmountByPartner
				(TransformUtils.transform(biQueryForm, com.tem.pss.dto.bi.BiQueryDto.class));
		
		//设置企业名称
		for(PartnerOrderAmountDto dto : refundList){
			if(dto.getPartnerId()!=null && !"".equals(dto.getPartnerId())){
				dto.setPartnerName(partnerService.findById(Long.valueOf(dto.getPartnerId())).getName());
			}
		}
		
		Map<String,PartnerOrderAmountDto> orderMap = new HashMap<String, PartnerOrderAmountDto>();
		for(PartnerOrderAmountDto dto : refundList){
			if(orderMap.containsKey(dto.getPartnerId())){
				PartnerOrderAmountDto po =  orderMap.get(dto.getPartnerId());
				po.setCount(po.getCount()+dto.getCount());
				po.setRefundAmount(dto.getRefundAmount()==null?po.getRefundAmount():po.getRefundAmount().add(dto.getRefundAmount()));
				orderMap.put(dto.getPartnerId(), po);
			}else {
				PartnerOrderAmountDto po =  new PartnerOrderAmountDto();
				po.setPartnerId(dto.getPartnerId());
				po.setPartnerName(dto.getPartnerName());
				po.setCount(dto.getCount());
				po.setRefundAmount(dto.getRefundAmount()==null?BigDecimal.ZERO:dto.getRefundAmount());
				orderMap.put(dto.getPartnerId(), po);
			}
		}
		//退单金额   不根据业务线区分，根据企业区分 
		List<PartnerOrderAmountDto> order6 = new ArrayList<PartnerOrderAmountDto>(orderMap.values());
		
		List<PartnerOrderAmountDto> order7 = this.getMergeMapList(order1, order2, order3, order4, order5, carOrder,order6,orderInternational);
		List<PartnerOrderAmountDto> order8 = this.getMergeList(order7, order1, order2, order3, order4, order5,carOrder, order6,orderInternational);
		
		//订单数据汇总查询
		PartnerOrderAmountDto partnerOrderAmount = new PartnerOrderAmountDto();
		if(CollectionUtils.isNotEmpty(order8)){
			for(PartnerOrderAmountDto p : order8){
				if(partnerOrderAmount.getFlightAmount()!=null){
					partnerOrderAmount.setTotalAmount(partnerOrderAmount.getTotalAmount().add(p.getTotalAmount()==null?BigDecimal.ZERO:p.getTotalAmount()));
					partnerOrderAmount.setFlightAmount(partnerOrderAmount.getFlightAmount().add(p.getFlightAmount()==null?BigDecimal.ZERO:p.getFlightAmount()));
					partnerOrderAmount.setInternationalFlightAmout(partnerOrderAmount.getInternationalFlightAmout().add(p.getInternationalFlightAmout()==null?BigDecimal.ZERO:p.getInternationalFlightAmout()));
					partnerOrderAmount.setHotelAmount(partnerOrderAmount.getHotelAmount().add(p.getHotelAmount()==null?BigDecimal.ZERO:p.getHotelAmount()));
					partnerOrderAmount.setTrainAmount(partnerOrderAmount.getTrainAmount().add(p.getTrainAmount()==null?BigDecimal.ZERO:p.getTrainAmount()));
					partnerOrderAmount.setInsuranceAmount(partnerOrderAmount.getInsuranceAmount().add(p.getInsuranceAmount()==null?BigDecimal.ZERO:p.getInsuranceAmount()));
					partnerOrderAmount.setGeneralAmount(partnerOrderAmount.getGeneralAmount().add(p.getGeneralAmount()==null?BigDecimal.ZERO:p.getGeneralAmount()));
					partnerOrderAmount.setCarAmount(partnerOrderAmount.getCarAmount().add(p.getCarAmount()==null?BigDecimal.ZERO:p.getCarAmount()));
					partnerOrderAmount.setRefundAmount(partnerOrderAmount.getRefundAmount().add(p.getRefundAmount()==null?BigDecimal.ZERO:p.getRefundAmount()));
				}else{
					partnerOrderAmount.setTotalAmount(p.getTotalAmount()==null?BigDecimal.ZERO:p.getTotalAmount());
					partnerOrderAmount.setFlightAmount(p.getFlightAmount()==null?BigDecimal.ZERO:p.getFlightAmount());
					partnerOrderAmount.setInternationalFlightAmout(p.getInternationalFlightAmout()==null?BigDecimal.ZERO:p.getInternationalFlightAmout());
					partnerOrderAmount.setHotelAmount(p.getHotelAmount()==null?BigDecimal.ZERO:p.getHotelAmount());
					partnerOrderAmount.setTrainAmount(p.getTrainAmount()==null?BigDecimal.ZERO:p.getTrainAmount());
					partnerOrderAmount.setInsuranceAmount(p.getInsuranceAmount()==null?BigDecimal.ZERO:p.getInsuranceAmount());
					partnerOrderAmount.setGeneralAmount(p.getGeneralAmount()==null?BigDecimal.ZERO:p.getGeneralAmount());
					partnerOrderAmount.setCarAmount(p.getCarAmount()==null?BigDecimal.ZERO:p.getCarAmount());
					partnerOrderAmount.setRefundAmount(p.getRefundAmount()==null?BigDecimal.ZERO:p.getRefundAmount());
				}
			}
		}
		//汇总各业务线 退单金额
		if(CollectionUtils.isNotEmpty(refundList)){
			for(PartnerOrderAmountDto refundOrder : refundList){
				BigDecimal amount = (refundOrder.getRefundAmount()==null)?BigDecimal.ZERO:refundOrder.getRefundAmount();
				if("10".equals(refundOrder.getBizType())){
					partnerOrderAmount.setRefundFlightAmount(partnerOrderAmount.getRefundFlightAmount()==null?amount:partnerOrderAmount.getRefundFlightAmount().add(amount));
				}else if("17".equals(refundOrder.getBizType())){
					partnerOrderAmount.setRefundInternationalFlightAmout(partnerOrderAmount.getRefundInternationalFlightAmout()==null?amount:partnerOrderAmount.getRefundInternationalFlightAmout().add(amount));
				}else if("11".equals(refundOrder.getBizType())){
					partnerOrderAmount.setRefundHotelAmount(partnerOrderAmount.getRefundHotelAmount()==null?amount:partnerOrderAmount.getRefundHotelAmount().add(amount));
				}else if("12".equals(refundOrder.getBizType())){
					partnerOrderAmount.setRefundTrainAmount(partnerOrderAmount.getRefundTrainAmount()==null?amount:partnerOrderAmount.getRefundTrainAmount().add(amount));
				}else if("13".equals(refundOrder.getBizType())){
					partnerOrderAmount.setRefundInsuranceAmount(partnerOrderAmount.getRefundInsuranceAmount()==null?amount:partnerOrderAmount.getRefundInsuranceAmount().add(amount));
				}else{ //通用订单等等 都 归类到“其他”类型中
					partnerOrderAmount.setRefundGeneralAmount(partnerOrderAmount.getRefundGeneralAmount()==null?amount:partnerOrderAmount.getRefundGeneralAmount().add(amount));
				}
			}
		}
		
		
		
		List<PartnerOrderAmountDto> orderList1 = new ArrayList<>();

		Integer pageIndex = biQueryForm.getPageIndex() == null ? 1 : biQueryForm.getPageIndex();
		Integer	pageSize = 20;

		int start = (pageIndex - 1) * pageSize;

		for(int i= start ; i < start + pageSize ; i++){
			if(i + 1 > order8.size()){
				break;
			}
			orderList1.add(order8.get(i));
		}

		int totalCount = order8.size();

		//退单各业务线金额  区分企业
		for(PartnerOrderAmountDto order : orderList1){
			for(PartnerOrderAmountDto refundOrder : refundList){
				if(order.getPartnerId().equals(refundOrder.getPartnerId())){
					BigDecimal refundAmount =  (refundOrder.getRefundAmount()==null)?BigDecimal.ZERO:refundOrder.getRefundAmount();
					if("10".equals(refundOrder.getBizType())){
						order.setRefundFlightAmount(refundAmount);
					}else if("17".equals(refundOrder.getBizType())){
						order.setRefundInternationalFlightAmout(refundAmount);
					}else if("11".equals(refundOrder.getBizType())){
						order.setRefundHotelAmount(refundAmount);
					}else if("12".equals(refundOrder.getBizType())){
						order.setRefundTrainAmount(refundAmount);
					}else if("13".equals(refundOrder.getBizType())){
						order.setRefundInsuranceAmount(refundAmount);
					}else{ //通用订单等等 都 归类到“其他”类型中
						order.setRefundGeneralAmount(order.getRefundGeneralAmount()==null?refundAmount:order.getRefundGeneralAmount().add(refundAmount));
					}
				}
				
			}
		}
		Page<PartnerOrderAmountDto> pageList = new Page<>(start,pageSize,orderList1, totalCount);

		model.addAttribute("pageList", pageList);
		model.addAttribute("partnerOrderAmount", partnerOrderAmount);
		
		return "payment/operData/partnerList.jsp";
	}

	//多个list合并成一个list
	private List<PartnerOrderAmountDto> getMergeList(List<PartnerOrderAmountDto> list,List<PartnerOrderAmountDto>... lists){
		for (List<PartnerOrderAmountDto> poaList: lists) {
			for (PartnerOrderAmountDto poA: poaList) {//输入
				for (PartnerOrderAmountDto poA1 : list ) {//输出

					if(poA1.getPartnerId().equals(poA.getPartnerId())){

						if(poA.getFlightAmount() != null){
							poA1.setFlightAmount(poA.getFlightAmount());
						}else if(poA.getHotelAmount() != null){
							poA1.setHotelAmount(poA.getHotelAmount());
						}else if(poA.getTrainAmount() != null){
							poA1.setTrainAmount(poA.getTrainAmount());
						}else if(poA.getInsuranceAmount() != null){
							poA1.setInsuranceAmount(poA.getInsuranceAmount());
						}else if(poA.getGeneralAmount() != null){
							poA1.setGeneralAmount(poA.getGeneralAmount());
						}else if(poA.getRefundAmount() != null){
							poA1.setRefundAmount(poA.getRefundAmount());
						}else if(poA.getInternationalFlightAmout() != null){
							poA1.setInternationalFlightAmout(poA.getInternationalFlightAmout());
						}else if(poA.getCarAmount() != null){
							poA1.setCarAmount(poA.getCarAmount());
						}

					}
				}
			}
		}

		for (PartnerOrderAmountDto poa2 : list){
			BigDecimal totalAmount = BigDecimalUtils.add(2, poa2.getFlightAmount(),poa2.getInternationalFlightAmout(),poa2.getHotelAmount(),
					poa2.getTrainAmount(),poa2.getGeneralAmount(),poa2.getInsuranceAmount(),poa2.getRefundAmount(),poa2.getCarAmount());
			poa2.setTotalAmount(totalAmount);
		}
		return list;
	}

	//把多个list首先合并成一个map再合并成一个list
	private List<PartnerOrderAmountDto>  getMergeMapList(List<PartnerOrderAmountDto>... lists){
		Map<String,String> map = new HashMap<String, String>();

		for(List<PartnerOrderAmountDto> list: lists){
			for (PartnerOrderAmountDto poa : list) {
				if(poa.getPartnerId() != null && poa.getPartnerName() != null){
					map.put(poa.getPartnerId(),poa.getPartnerName());
				}
			}
		}

		List<PartnerOrderAmountDto> poaList = new ArrayList<PartnerOrderAmountDto>();
		for (String string: map.keySet()) {
			poaList.add(new PartnerOrderAmountDto(string, map.get(string)));
		}
		return poaList;
	}


	//企业订单查询
	@RequestMapping("detail")
	public String partnerOrder(BiQueryForm biQueryForm, Model model){

		if(biQueryForm.getEndDate() != null){

			biQueryForm.setEndDate(DateUtils.getDayEnd(biQueryForm.getEndDate()));
		}

		if(StringUtils.isEmpty(biQueryForm.getTravelType())){
			biQueryForm.setTravelType(null);
		}
		
		biQueryForm.setTmcId(null);

		Map<String, BigDecimal> flight = flightBiService.findOrderLiqAmount
				(TransformUtils.transform(biQueryForm, com.tem.flight.dto.bi.BiQueryDto.class));

		Map<String, BigDecimal> internationalFlight = internationalFlightBiService.findOrderLiqAmount
				(TransformUtils.transform(biQueryForm, BiQueryDto.class));

		Map<String, BigDecimal> hotel =hotelBiService.findOrderLiqAmount
				(TransformUtils.transform(biQueryForm, com.tem.hotel.dto.bi.HotelBiQueryDto.class)).getData();

		Map<String, BigDecimal> train = trainBiService.findOrderLiqAmount
				(TransformUtils.transform(biQueryForm, com.tem.train.dto.bi.BiQueryDto.class));

		Map<String, BigDecimal> general = generalBiService.findOrderLiqAmount
				(TransformUtils.transform(biQueryForm, com.tem.product.dto.general.BiQueryDto.class));

		Map<String, BigDecimal> refund = refundBiService.findOrderLiqAmount
				(TransformUtils.transform(biQueryForm, com.tem.pss.dto.bi.BiQueryDto.class));

		Map<String, BigDecimal> insurance = insuranceBiService.findOrderLiqAmount
				(TransformUtils.transform(biQueryForm, com.tem.product.dto.general.BiQueryDto.class));
		
		Map<String, BigDecimal> car = carBiService.findOrderLiqAmount
		(TransformUtils.transform(biQueryForm, com.tem.car.dto.bi.CarBiQueryDto.class)).getData();
		
		BigDecimal total =BigDecimalUtils.add(2, flight.get("total"),internationalFlight.get("total"), hotel.get("total"),
				train.get("total"), general.get("total"), refund.get("total"), insurance.get("total"), car.get("total"));

		PartnerOrderForm partnerOrderForm = new PartnerOrderForm();
		partnerOrderForm.setTotalAmount(total);

		List<OrderTypeCount> orderTypeCount = new ArrayList<>();
		orderTypeCount.add(new OrderTypeCount("国内机票", flight.get("orderCount"), flight.get("total")));
		orderTypeCount.add(new OrderTypeCount("国际机票", internationalFlight.get("orderCount"), internationalFlight.get("total")));
		orderTypeCount.add(new OrderTypeCount("酒店订单", hotel.get("orderCount"), hotel.get("total")));
		orderTypeCount.add(new OrderTypeCount("火车订单", train.get("orderCount"), train.get("total")));
		orderTypeCount.add(new OrderTypeCount("保险订单", insurance.get("orderCount"), insurance.get("total")));
		orderTypeCount.add(new OrderTypeCount("用车订单", car.get("orderCount"), car.get("total")));
		orderTypeCount.add(new OrderTypeCount("其他订单", general.get("orderCount"), general.get("total")));
		orderTypeCount.add(new OrderTypeCount("退&emsp;&emsp;单", refund.get("orderCount"), refund.get("total")));

		partnerOrderForm.setOrderTypeCount(orderTypeCount);
		partnerOrderForm.setOrderOriginCount(this.getOrderOriginCount(flight,internationalFlight,hotel,train,insurance,general,car,refund));
		partnerOrderForm.setPaymentMethod(this.getPaymentMethod(flight,internationalFlight,hotel,train,insurance,general,car,refund));

		model.addAttribute(partnerOrderForm);
		return "payment/operData/partnerOrderDetail.jsp";
	}

	//下单渠道统计
	private List<OrderOriginCount> getOrderOriginCount(Map<String, BigDecimal>... maps){
		List<OrderOriginCount> orderOriginCount = new ArrayList<>();

		BigDecimal pc_count = null, weixin_count = null, tel_count1 = null,tel_count2 = null, ios_count = null, android_count = null, h5_count = null, xiaochengxu_count = null;
		for (Map<String, BigDecimal> map: maps){
			pc_count = BigDecimalUtils.add(pc_count, map.get("WWW"));
			tel_count1 = BigDecimalUtils.add(tel_count1, map.get("CC_WWW"));
			tel_count2 = BigDecimalUtils.add(tel_count2, map.get("CC_MANUAL"));
			android_count = BigDecimalUtils.add(android_count, map.get("ANDROID"));
			ios_count = BigDecimalUtils.add(ios_count, map.get("IOS"));
			weixin_count = BigDecimalUtils.add(weixin_count, map.get("WEIXIN"));
			h5_count = BigDecimalUtils.add(h5_count, map.get("H5"));
			xiaochengxu_count = BigDecimalUtils.add(xiaochengxu_count, map.get("WEIXIN_APP"));

		}

		BigDecimal total_count = BigDecimalUtils.add(pc_count,weixin_count,tel_count1, tel_count2, ios_count, android_count, h5_count, xiaochengxu_count);
		if(total_count.compareTo(BigDecimal.ZERO) == 0){
			total_count = new BigDecimal(1);
		}
		BigDecimal pc_rate = BigDecimalUtils.divide(pc_count,total_count);
		BigDecimal tel_rate1 = BigDecimalUtils.divide(tel_count1,total_count);
		BigDecimal tel_rate2 = BigDecimalUtils.divide(tel_count2,total_count);
		BigDecimal android_rate = BigDecimalUtils.divide(android_count,total_count);
		BigDecimal ios_rate = BigDecimalUtils.divide(ios_count,total_count);
		BigDecimal weixin_rate = BigDecimalUtils.divide(weixin_count,total_count);
		BigDecimal h5_rate = BigDecimalUtils.divide(h5_count,total_count);
		BigDecimal xiaochengxu_rate = BigDecimalUtils.divide(xiaochengxu_count,total_count);

		orderOriginCount.add(new OrderOriginCount("pc端下单&nbsp;&nbsp;",pc_count.intValue(), pc_rate + ""));
		orderOriginCount.add(new OrderOriginCount("客服代订下单&nbsp;",tel_count1.intValue(), tel_rate1 + ""));
		orderOriginCount.add(new OrderOriginCount("客服手工录单",tel_count2.intValue(), tel_rate2 + ""));
		orderOriginCount.add(new OrderOriginCount("安卓下单&nbsp;&nbsp;&nbsp;",android_count.intValue(), android_rate + ""));
		orderOriginCount.add(new OrderOriginCount("IOS下单&nbsp;&nbsp;&nbsp;",ios_count.intValue(), ios_rate + ""));
		orderOriginCount.add(new OrderOriginCount("微信公众号下单",weixin_count.intValue(), weixin_rate + ""));
		orderOriginCount.add(new OrderOriginCount("H5下单&nbsp;&nbsp;&nbsp;",h5_count.intValue(), h5_rate + ""));
		orderOriginCount.add(new OrderOriginCount("微信小程序下单",xiaochengxu_count.intValue(), xiaochengxu_rate + ""));
		return orderOriginCount;
	}

	//支付方式统计
	private List<PaymentMethod> getPaymentMethod(Map<String, BigDecimal>... maps){
		List<PaymentMethod> paymentMethod = new ArrayList<>();

		BigDecimal p_count = null,bp_count = null,p_amount = null,bp_amount = null;
		for (Map<String, BigDecimal> map: maps) {
			p_count = BigDecimalUtils.add(0,map.get("p_count"),p_count);
			bp_count = BigDecimalUtils.add(0,map.get("bp_count"),bp_count);
			p_amount = BigDecimalUtils.add(map.get("p_amount"),p_amount);
			bp_amount = BigDecimalUtils.add(map.get("bp_amount"),bp_amount);
		}
		paymentMethod.add(new PaymentMethod("个人支付",p_count.intValue(),p_amount + ""));
		paymentMethod.add(new PaymentMethod("企业支付",bp_count.intValue(),bp_amount + ""));
		return paymentMethod;
	}

	/**
	 * 账单的查询页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value="bill")
	public String bill(Model model){
		return "payment/operData/bill/index.base";
	}


	/**
	 * 得到查询的账单
	 */
	@RequestMapping(value="billList")
	public String billList(Integer pageIndex,Long partnerId,String billGap,
						   String billStatus,String beginDate,String endDate,Long tmcId, Model model){

		BillCondition billCondition = new BillCondition();
		billCondition.setBpId(partnerId);
		billCondition.setTmcId(tmcId);
		if(StringUtils.isNotEmpty(billStatus)){
			billCondition.setBillStatus(billStatus);
		}
		if(StringUtils.isNotEmpty(beginDate)){
			billCondition.setBeginDate(DateUtils.parse(beginDate));
		}
		if(StringUtils.isNotEmpty(endDate)){
			billCondition.setEndDate(DateUtils.parse(endDate));
		}
		if (StringUtils.isNotEmpty(billGap)){
			billGap = billGap + "-01";
			Date begin = DateUtils.parse(billGap);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(begin);
			calendar.add(Calendar.MONTH, 1);
			Date end = calendar.getTime();
			billCondition.setLiqDateBegin(begin);
			billCondition.setLiqDateEnd(end);
		}
		if(!super.isSuperAdmin()){
			billCondition.setTmcId(super.getTmcId());
		}

		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<BillCondition> queryDto = new QueryDto<BillCondition>(pageIndex,20);
		queryDto.setCondition(billCondition);


		Page<BillDto> billListPage = billService.queryListWithAccount(queryDto);

		List<BillForm> resultList = dtoToForm(billListPage.getList());
		Page<BillForm> resultPage = new Page<BillForm>(billListPage.getStart(), billListPage.getSize(), resultList, billListPage.getTotal());

		model.addAttribute("pageList", resultPage);
		return "payment/operData/bill/billList.jsp";
	}

	/**
	 * 账单的查询页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value="remain")
	public String remain(Model model){
		return "payment/operData/remain/index.base";
	}

	@RequestMapping(value="remainList")
	public String findPartnerRemain(Integer pageIndex,Long partnerId ,Long tmcId, Model model){
		BillCondition billCondition =new BillCondition();
		billCondition.setBeginDate(DateUtils.parse("1970-01-01"));
		billCondition.setEndDate(new Date());
		billCondition.setBillStatus("OPEN");
		billCondition.setConfirmStatus("CONFIRM");
		billCondition.setBpId(partnerId);
		billCondition.setTmcId(tmcId);

		if(!super.isSuperAdmin()){
			billCondition.setTmcId(super.getTmcId());
		}

		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<BillCondition> queryDto = new QueryDto<BillCondition>(pageIndex,20);
		queryDto.setCondition(billCondition);
		Page<BillDto> billListPage = billService.queryListWithRemain(queryDto);

		List<BillForm> resultList = dtoToForm(billListPage.getList());
		Page<BillForm> resultPage = new Page<BillForm>(billListPage.getStart(), billListPage.getSize(), resultList, billListPage.getTotal());

		model.addAttribute("pageList", resultPage);

		return "payment/operData/remain/billList.jsp";
	}

	private List<BillForm> dtoToForm(List<BillDto> billList){
		List<BillForm> resultList = new ArrayList<BillForm>();
		if(CollectionUtils.isNotEmpty(billList)){
			BillForm billForm = null;
			for(BillDto billDto : billList){
				if(billDto == null){
					continue;
				}
				billForm = TransformUtils.transform(billDto, BillForm.class);
				if(billDto.getBpId() != null){
					PartnerDto partnerDto = partnerService.findById(billDto.getBpId());
					if(partnerDto != null){
						billForm.setBpName(partnerDto.getName());
					}
				}
				if(billDto.getAccountId() != null){
					BpAccountDto bpAccountDto = accountManageService.getById(billDto.getAccountId());
					if(bpAccountDto != null){
						billForm.setAccountName(bpAccountDto.getAccountName());
					}
				}
				resultList.add(billForm);
			}
		}
		return resultList;
	}

}
