package com.tem.payment.action;

import com.iplatform.common.Page;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.web.Message;
import com.tem.payment.api.AccountManageService;
import com.tem.payment.api.BpAccountChangeLogService;
import com.tem.payment.dto.BpAccountChangeLogDto;
import com.tem.payment.dto.BpAccountDto;
import com.tem.payment.enums.BpAccountChangeType;
import com.tem.platform.api.PartnerBpService;
import com.tem.platform.api.PartnerCorpService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerCorpDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.platform.security.authorize.PermissionUtil;
import com.tem.ConstantAuth;
import com.tem.payment.form.BpAccountChangeLogForm;
import com.tem.payment.form.BpAccountForm;
import com.tem.platform.action.BaseController;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.*;

/**
 * 账户维护	
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("bpAccount/manage")
public class BpAccountManageController extends BaseController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AccountManageService accountManageService;
	
	@Autowired
	private	PartnerCorpService partnerCorpService;
	
	@Autowired
	private	PartnerService partnerService;
	
	@Autowired
	private	BpAccountChangeLogService bpAccountChangeLogService;

	@Autowired
	private PartnerBpService partnerBpService;

	/**
	 * 1:all tmc:type=tmc
	 * 2:all partner
	 * 管理企业账户
	 * @return
	 */
	@RequestMapping
	public String manage(Model model) {
		this.searchAccount(model);
		boolean m = PermissionUtil.checkTrans(ConstantAuth.IPE_COMP_ACCOUNT_MAG, "M") == 0;
        boolean l = PermissionUtil.checkTrans(ConstantAuth.IPE_COMP_ACCOUNT_MAG, "L") == 0;
        model.addAttribute("m", m);
        model.addAttribute("l", l);
		return "payment/bpAccountList.base";
	}

	/**
	 * 1:tmc管理的企业（默认）search
	 * 2:本企业：search?type=me
	 * 查询企业账户:1.tmc管理的企业（默认）；2.本企业
	 * @return
	 */
	@RequestMapping("search")
	public String search(Model model) {
		this.searchAccount(model);
		return "payment/balance/bpAccountList.base";
	}

	private void searchAccount(Model model){
		Integer pageIndex = super.getIntegerValue("pageIndex");
		if (pageIndex == null) {
			pageIndex = 1;
		}
		Map<String, Object> partMap = new HashMap<>();
		partMap.put("pageSize",10);
		partMap.put("start",(pageIndex-1)*10);

		Long partnerId = super.getLongValue("c_partnerId");
		if(partnerId != null){
			partMap.put("partnerId",partnerId);
			PartnerDto partner = partnerService.findById(partnerId);
			if(partner != null){
				model.addAttribute("bpName", partner.getName());
			}
			model.addAttribute("c_partnerId", partnerId);
		}

		//查询类型：运营平台：all tmc；客服平台：all partner；本企业账户查询
		String searchType = super.getStringValue("type");
		boolean onlyme = "me".equals(searchType);
		boolean onlytmc = "tmc".equals(searchType);
		boolean admin = isSuperAdmin();
		String title = null;

		List<PartnerDto> partnerList = new ArrayList<>();
		int count = 0;
		Long myPartnerId = super.getCurrentUser().getPartnerId();

		if(onlyme){
			title = "当前企业";
			PartnerDto partnerDto = partnerService.findById(myPartnerId);
			partnerList.add(partnerDto);
			count = 1;
		}else if(onlytmc){
			title = "TMC";
			if (admin) {
				partMap.put("type",1);
				partnerList = partnerService.getPartnerList(partMap);
				count = partnerService.getPartnerCount(partMap);
			}
		}else{//默认查询tmc下属企业
			title = "企业";
			partMap.put("type",0);
			partMap.put("tmcId",myPartnerId);
			partnerList = partnerService.getPartnerList(partMap);
			count = partnerService.getPartnerCount(partMap);
		}

		//企业分组 企业名称转译 账户类型转译 最后修改人转译
		List<Map<String,Object>> bpGroupList = new ArrayList<>();
		for (int i = 0; i < partnerList.size(); i++) {
			PartnerDto partnerDto = partnerList.get(i);
			Map<String, Object> bpMap = new HashMap<>();
			bpMap.put("bpId", partnerDto.getId());
			bpMap.put("bpName", partnerDto.getName());

			List<BpAccountDto> partnerAccounts = accountManageService.getAllByBpId(partnerDto.getId());
			List<BpAccountForm> resultList = new ArrayList<>();
			if (!partnerAccounts.isEmpty()) {
				for (int j = 0; j < partnerAccounts.size(); j++) {
					BpAccountDto bpAccountDto = partnerAccounts.get(j);
					BpAccountForm bpAccountForm = TransformUtils.transform(bpAccountDto, BpAccountForm.class);
					if(StringUtils.isNotEmpty(bpAccountDto.getAccountType())){
						if("B".equals(bpAccountDto.getAccountType())){
							bpAccountForm.setAccountTypeName("预存账户");
						}else if("C".equals(bpAccountDto.getAccountType())){
							bpAccountForm.setAccountTypeName("授信账户");
						}
					}
					if(bpAccountForm.getLastModifyUser() !=null ){
						UserDto user = userService.getUser(bpAccountForm.getLastModifyUser());
						if(user !=null ){
							bpAccountForm.setLastModifyUserName(user.getFullname());
						}
					}
					resultList.add(bpAccountForm);
				}
			}else{
				resultList.add(new BpAccountForm());
			}
			bpMap.put("bpList", resultList);
			bpMap.put("canChange", admin || !partnerDto.getId().equals(myPartnerId));
			bpGroupList.add(bpMap);
		}

		Page<PartnerDto> bpAccountPage = new Page<>(partnerList,pageIndex,10,count);
		model.addAttribute("bpGroupList", bpGroupList);
		model.addAttribute("pageList", bpAccountPage);
		model.addAttribute("title", title);
	}
	
	@ResponseBody
	@RequestMapping(value="cancelBpAccount")
	public	Message cancelBpAccount(Model model,@RequestParam("id")Long id){
		BpAccountDto bpAccount = accountManageService.getById(id);
		bpAccount.setState("DISABLED");
		accountManageService.update(bpAccount);
		return	Message.success("处理成功");
	}
	
	@ResponseBody
	@RequestMapping(value="reOpenBpAccount")
	public	Message reOpenBpAccount(Model model,@RequestParam("id")Long id){
		BpAccountDto bpAccount = accountManageService.getById(id);
		bpAccount.setState("ACTIVE");
		accountManageService.update(bpAccount);
		return	Message.success("处理成功");
	}
	
	@RequestMapping(value="detail")
	public String detail(Model model,@RequestParam(required=false) Long id){
		BpAccountDto bpAccount = null;
		if(id == null){
			bpAccount = new BpAccountDto();
			model.addAttribute("typeTitle", "新增账户");
		}else{
			bpAccount = accountManageService.getById(id);
			if(bpAccount.getBpId() != null){
				PartnerDto partner = partnerService.findById(bpAccount.getBpId());
				if(partner != null){
					model.addAttribute("bpName", partner.getName());
				}
				List<PartnerCorpDto> partnerCorps = partnerCorpService.getByPaymentAccount(id.toString());
				model.addAttribute("partnerCorps", partnerCorps);
			}
			model.addAttribute("typeTitle", "修改账户");
			
			//得到 变更记录
			List<BpAccountChangeLogDto> changeLogDtos = bpAccountChangeLogService.selectByAccountId(id);
			List<BpAccountChangeLogForm> changeLogs = new ArrayList<BpAccountChangeLogForm>();
			if(CollectionUtils.isNotEmpty(changeLogDtos)){
				BpAccountChangeLogForm changelogForm = null;
				for(BpAccountChangeLogDto bacl : changeLogDtos){
					changelogForm = TransformUtils.transform(bacl, BpAccountChangeLogForm.class);
					if(changelogForm.getUpdateUser() != null){
						UserDto user = userService.getUser(changelogForm.getUpdateUser());
						if(user != null){
							changelogForm.setUpdateUserName(user.getFullname());
						}
					}
					changeLogs.add(changelogForm);
				}
			}
			model.addAttribute("changeLogs", changeLogs);

			model.addAttribute("grantedCredit", BigDecimalUtils.divide(new BigDecimal(bpAccount.getCreditAmount()), new BigDecimal(100)));
		}
		model.addAttribute("bpAccount", bpAccount);

		return "payment/bpAccountDetail.jsp";
	}
	
	@RequestMapping(value="getCorpList")
	public String getCorpList(Model model,@RequestParam(required=false) Long partnerId,@RequestParam(required=false) String ids){		
		
		List<PartnerCorpDto> hasSetList = new ArrayList<PartnerCorpDto>();				
		List<PartnerCorpDto> resultList = new ArrayList<PartnerCorpDto>();
		List<PartnerCorpDto> partnerCorps = partnerCorpService.getByPartnerId(partnerId);
		if(CollectionUtils.isNotEmpty(partnerCorps)){
			for(PartnerCorpDto pc : partnerCorps){//只有没有设置账户的才要
				if(ids!=null&&ids.contains(String.valueOf(pc.getId()))){
					hasSetList.add(pc);
				}else{
					resultList.add(pc);
				}
			}
		}
		model.addAttribute("partnerCorps", resultList);
		model.addAttribute("hasSetList", hasSetList);
		return "payment/corpListModel.jsp";
	}
	
	@RequestMapping(value="rechargeAmount")
	public String rechargeAmount(Model model,@RequestParam(required=false) String amountType){
		
		String titleModel = "";
		if("credit".equals(amountType)){
			titleModel = "修改授信额度";
		}else{
			titleModel = "账户充值";
		}
		model.addAttribute("amountType", amountType);
		model.addAttribute("titleModel", titleModel);
		return "payment/amountModel.jsp";
	}
	@ResponseBody
	@RequestMapping(value="saveReChangeAmount")
	public	Message	saveReChangeAmount(Model model,String amountType,Long bpAccountId,Long reChangeAmount,String remark ){
		if("credit".equals(amountType)){
			BpAccountDto bpAccountDto = accountManageService.getById(bpAccountId);
			bpAccountDto.setCreditAmount(reChangeAmount);
			
			BpAccountDto bpAccount1 = accountManageService.getById(bpAccountDto.getId());
			accountManageService.update(bpAccountDto);
			
			Long creditAmount = bpAccount1.getCreditAmount();
			//金额不同则记录
			boolean needRecod = false;
			if(creditAmount == null){
				if(bpAccountDto.getCreditAmount() != null){
					needRecod = true;
				}
			}else{
				if(bpAccountDto.getCreditAmount() == null || creditAmount.longValue() != bpAccountDto.getCreditAmount().longValue()){
					needRecod = true;
				}
			}
			if(needRecod){
				BpAccountChangeLogDto changeLog = new BpAccountChangeLogDto();
				changeLog.setAccountId(bpAccountDto.getId());
				changeLog.setChangeType(BpAccountChangeType.CREDIT.name());
				changeLog.setNewValue(bpAccountDto.getCreditAmount() == null?"":bpAccountDto.getCreditAmount().toString());
				changeLog.setOldValue(creditAmount == null?"":creditAmount.toString());
				changeLog.setUpdateUser(super.getCurrentUser().getId());
				changeLog.setUpdateDate(new Date());
				changeLog.setRemark(remark);
				bpAccountChangeLogService.save(changeLog);
			}
		}
		
		return Message.success("处理成功");
	}
	
	@ResponseBody
	@RequestMapping(value="saveOrUpdate")
	public	Message	saveOrUpdate(Model model,BpAccountDto bpAccountDto){
		Long accountId = null;

		//新增
		if(bpAccountDto.getId() == null){
			bpAccountDto.setCreditAmount(0L);
			bpAccountDto.setBalanceAmount(0L);
			bpAccountDto.setCreteDate(new Date());
			bpAccountDto.setLastModifyUser(super.getCurrentUser().getId());
			bpAccountDto.setLastModifyDate(new Date());
			bpAccountDto.setState("ACTIVE");

			Long tmcId = partnerBpService.findTmcIdByPId(bpAccountDto.getBpId());
			bpAccountDto.setTmcId(tmcId);

			BpAccountDto ba = accountManageService.insert(bpAccountDto);
			accountId = ba.getId();
			
		}else{

			//修改
			bpAccountDto.setCreditAmount(null);
			bpAccountDto.setBalanceAmount(null);
			bpAccountDto.setLastModifyDate(new Date());
			bpAccountDto.setLastModifyUser(super.getCurrentUser().getId());
			accountId = bpAccountDto.getId();
			accountManageService.update(bpAccountDto);
		}

		//删除原先这个账户所有的关联法人
		partnerCorpService.batchUpdateByPaymentAccount(accountId);
		
		//添加关联法人
		String corpSelect = super.getStringValue("corpSelect");
		if(StringUtils.isNotEmpty(corpSelect)){
			String[] corpSelects = corpSelect.split(",");
			PartnerCorpDto partnerCorpDto = null;
			for(String cs : corpSelects){
				partnerCorpDto = new PartnerCorpDto();
				partnerCorpDto.setId(Long.parseLong(cs));
				partnerCorpDto.setPaymentAccount(accountId);
				partnerCorpService.save(partnerCorpDto);
			}
		}
		return Message.success("处理成功");
	}
	
	
	
	@ResponseBody
	@RequestMapping(value = "checkAccountName")
	public Message checkAccountName(Model model,@RequestParam("accountName") String accountName,@RequestParam("id") Long id) {
		if(StringUtils.isNotEmpty(super.getStringValue("partnerId"))){
			List<BpAccountDto> bpAccountDtos = accountManageService.getAllByBpId(super.getLongValue("partnerId"));//得到这个企业所有的账户
			if(id == null){
				if(CollectionUtils.isNotEmpty(bpAccountDtos)){
					for(BpAccountDto BpAccountDto : bpAccountDtos){
						if(BpAccountDto.getAccountName().equals(accountName)){
							return Message.error("no");
						}
					}
				}
				return Message.success("yes");
			}else{//修改的时候
				if(CollectionUtils.isNotEmpty(bpAccountDtos)){
					for(BpAccountDto BpAccountDto : bpAccountDtos){
						if(BpAccountDto.getAccountName().equals(accountName) && !BpAccountDto.getId().toString().endsWith(id.toString())){
							return Message.error("no");
						}
					}
				}
				return Message.success("yes");
			}
		}
		return Message.success("yes");
	}
}
