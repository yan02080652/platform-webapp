package com.tem.payment.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.web.Message;
import com.tem.payment.api.AccountManageService;
import com.tem.payment.api.BillService;
import com.tem.payment.condition.BillCondition;
import com.tem.payment.dto.BillDto;
import com.tem.payment.dto.BpAccountDto;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.payment.form.BillForm;
import com.tem.platform.action.BaseController;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 账单信息	
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("bill/manage")
public class BillManageController extends BaseController {
	
	@Autowired
	private UserService userService;

	@Autowired
	private AccountManageService accountManageService;

	@Autowired
	private	PartnerService partnerService;
	
	@Autowired
	private	BillService billService;
	
	/**
	 * 账单的查询页面
	 * @param model
	 * @return
	 */
	@RequestMapping(value="")
	public String index(Model model){
		String billStatus = super.getStringValue("billStatus");
		String confirmStatus = super.getStringValue("confirmStatus");
		String beginDate = super.getStringValue("beginDate");
		String endDate = super.getStringValue("endDate");
		String accountId = super.getStringValue("accountId");
		if(StringUtils.isEmpty(billStatus)){//默认为未清账的账单
			billStatus = "OPEN";
		}
		if(StringUtils.isNotEmpty(confirmStatus)){
			model.addAttribute("confirmStatus", confirmStatus);
		}
		if(StringUtils.isNotEmpty(accountId)){
			BpAccountDto partnerDto = accountManageService.getById(Long.parseLong(accountId));

			if(partnerDto != null){
				model.addAttribute("accountName", partnerDto.getAccountName());
				model.addAttribute("accountId",accountId);
			}
		}
		
		model.addAttribute("billStatus", billStatus);
		model.addAttribute("beginDate", beginDate);
		model.addAttribute("endDate", endDate);
		return "payment/bill/index.base";
	}
	
	/**
	 * 得到查询的账单
	 * @param model
	 * @param pageIndex
	 * @return
	 */
	@RequestMapping(value="billList")
	public String billList(Model model,@RequestParam(value="pageIndex",required=false)Integer pageIndex
			,@RequestParam(value="partnerId",required=false)Long partnerId
			,@RequestParam(value="accountId",required=false)Long accountId
			,@RequestParam(value="billStatus",required=false)String billStatus
			,@RequestParam(value="confirmStatus",required=false)String confirmStatus
			,@RequestParam(value="invoiceStatus",required=false)String invoiceStatus
			,@RequestParam(value="beginDate",required=false)String beginDate
			,@RequestParam(value="endDate",required=false)String endDate
			,@RequestParam(value="tmcId",required=false)Long tmcId){
		
		BillCondition billCondition = new BillCondition();
		billCondition.setBpId(partnerId);
		if(StringUtils.isNotEmpty(billStatus)){
			billCondition.setBillStatus(billStatus);
		}
		if(StringUtils.isNotEmpty(confirmStatus)){
			billCondition.setConfirmStatus(confirmStatus);
		}
		if(StringUtils.isNotEmpty(invoiceStatus)){
			billCondition.setInvoiceStatus(invoiceStatus);
		}
		if(StringUtils.isNotEmpty(beginDate)){
			billCondition.setBeginDate(DateUtils.parse(beginDate));
		}
		if(StringUtils.isNotEmpty(endDate)){
			billCondition.setEndDate(DateUtils.parse(endDate));
		}
		billCondition.setAccountId(accountId);

		//限制tmcId
		if (!isSuperAdmin()) {
			billCondition.setTmcId(super.getTmcId());
		}

		billCondition.setTmcId(tmcId);

		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<BillCondition> queryDto = new QueryDto<BillCondition>(pageIndex,20);
		queryDto.setCondition(billCondition);
		
		Page<BillDto> billListPage = billService.queryListWithPage(queryDto);
		
		//转译
		List<BillDto> billList = billListPage.getList();
		List<BillForm> resultList = new ArrayList<BillForm>();
		if(CollectionUtils.isNotEmpty(billList)){
			BillForm billForm = null;
			for(BillDto billDto : billList){
				billForm = TransformUtils.transform(billDto, BillForm.class);
				if(billDto.getCreateUser() != null){
					UserDto user = userService.getUser(billDto.getCreateUser());
					if(user != null){
						billForm.setCreateUserName(user.getFullname());
					}
				}
				if(billDto.getBpId() != null){
					PartnerDto partnerDto = partnerService.findById(billDto.getBpId());
					if(partnerDto != null){
						billForm.setBpName(partnerDto.getName());
					}
				}
				if(billDto.getAccountId() != null){
					BpAccountDto bpAccountDto = accountManageService.getById(billDto.getAccountId());
					if(bpAccountDto != null){
						billForm.setAccountName(bpAccountDto.getAccountName());
					}
				}
				resultList.add(billForm);
			}
		}
		Page<BillForm> resultPage = new Page<BillForm>(billListPage.getStart(), billListPage.getSize(), resultList, billListPage.getTotal());
		
		model.addAttribute("pageList", resultPage);
		return "payment/bill/billList.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value="confirmBill")
	public	Message confirmBill(Model model,@RequestParam("id")Long id){
		if(id != null){
			billService.confirmBill(id, super.getCurrentUser().getId());
		}
		return	Message.success("处理成功");
	}

	@ResponseBody
	@RequestMapping(value="cancelBill")
	public ResponseInfo cancelBill(Long id){

		return	billService.cancelBill(id, super.getCurrentUser().getId());
	}

}
