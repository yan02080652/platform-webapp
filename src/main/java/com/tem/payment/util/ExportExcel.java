package com.tem.payment.util;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iplatform.common.utils.LogUtils;
import com.tem.payment.dto.BillAmountItemDto;
import com.tem.payment.dto.BillDetailDto;
import com.tem.payment.dto.BillExportInfo;
import com.tem.payment.form.ExportInfo;

/**
 * excel 导出
 */
public class ExportExcel {
	private Logger logger = LoggerFactory.getLogger(ExportExcel.class);

    private final static String MATH_NUMBER_VALID = "^((-[1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$";

    private final static String MATH_NUMBER_VALID1 = "^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$";

    /**
     * 账单导出
     *
     * @param billDetailDto
     * @param out
     * @param pattern
     * @param <T>
     */
    public <T> void exportExcel(BillDetailDto billDetailDto, OutputStream out, String pattern) {
        HSSFWorkbook workbook = new HSSFWorkbook();

        HSSFSheet sheet0 = workbook.createSheet("账单概览");

        createSheet(workbook, sheet0, billDetailDto);

        List<BillExportInfo> exportInfos = billDetailDto.getExportInfos();

        if (CollectionUtils.isNotEmpty(exportInfos)) {

            for (BillExportInfo exportInfo : exportInfos) {

                String title = exportInfo.getTitle();
                Collection<T> dataset = (Collection<T>) exportInfo.getDataset();
                String[] headers = exportInfo.getHeaders();

                HSSFSheet sheet = workbook.createSheet(title);
                createSheet(headers, dataset, pattern, workbook, sheet, false);
            }
        }

        try {
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 订单导出
     *
     * @param exportInfoList
     * @param out
     * @param pattern
     * @param <T>
     */
    public <T> void exportExcel(List<ExportInfo> exportInfoList, OutputStream out, String pattern) {
        HSSFWorkbook workbook = new HSSFWorkbook();

        for (ExportInfo exportInfo : exportInfoList) {

            String title = exportInfo.getTitle();
            Collection<T> dataset = (Collection<T>) exportInfo.getDataset();
            String[] headers = exportInfo.getHeaders();

            HSSFSheet sheet = workbook.createSheet(title);
            createSheet(headers, dataset, pattern, workbook, sheet, true);
        }

        try {
            workbook.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings({"deprecation", "rawtypes", "unchecked"})
    private <T> void createSheet(HSSFWorkbook workbook, HSSFSheet sheet, BillDetailDto bill) {

        CellRangeAddress region = new CellRangeAddress(2, 2, 1, 5);
        sheet.addMergedRegion(region);

        CellRangeAddress region1 = new CellRangeAddress(3, 3, 0, 5);
        sheet.addMergedRegion(region1);

        CellRangeAddress region2 = new CellRangeAddress(4, 4, 1, 5);
        sheet.addMergedRegion(region2);

        //生成账单的标题样式
        HSSFCellStyle style = workbook.createCellStyle();

        setBorders(style, HSSFColor.BLACK.index, HSSFColor.BLACK.index, HSSFColor.BLACK.index, HSSFColor.BLACK.index, HSSFCellStyle.BORDER_MEDIUM, HSSFCellStyle.BORDER_MEDIUM, HSSFCellStyle.BORDER_MEDIUM, HSSFCellStyle.BORDER_MEDIUM);

        style.setFillBackgroundColor(HSSFColor.WHITE.index);
        style.setWrapText(true);
        style.setAlignment(HSSFCellStyle.ALIGN_LEFT);
        style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        HSSFFont fontStyle = workbook.createFont();
        fontStyle.setFontName("黑体");
        fontStyle.setFontHeightInPoints((short) 15);
        fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        // 把字体应用到当前的样式
        style.setFont(fontStyle);

        // 生成账单的数据样式
        HSSFCellStyle style1 = workbook.createCellStyle();
        // 设置这些样式

        style1.setFillBackgroundColor(HSSFColor.WHITE.index);
        style1.setWrapText(true);
        style1.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style1.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

        setBorders(style1, HSSFColor.BLACK.index, HSSFColor.BLACK.index, HSSFColor.BLACK.index, HSSFColor.BLACK.index, HSSFCellStyle.BORDER_MEDIUM, HSSFCellStyle.BORDER_MEDIUM, HSSFCellStyle.BORDER_MEDIUM, HSSFCellStyle.BORDER_MEDIUM);

        // 生成字体
        HSSFFont fontStyle1 = workbook.createFont();
        fontStyle1.setFontName("宋体");
        fontStyle1.setFontHeightInPoints((short) 15);
        // 把字体应用到当前的样式
        style1.setFont(fontStyle1);

        //设置表格默认列宽度为25个字节
        sheet.setDefaultColumnWidth(24);
        sheet.setDefaultRowHeightInPoints(30);

        sheet.setColumnWidth(0, 27 * 256);
        sheet.setColumnWidth(2, 27 * 256);
        sheet.setColumnWidth(4, 27 * 256);

        sheet.setColumnWidth(1, 25 * 256);
        sheet.setColumnWidth(3, 25 * 256);
        sheet.setColumnWidth(5, 25 * 256);
        sheet.setDisplayGridlines(false);

        // 产生表格标题行
        HSSFRow row = sheet.createRow(0);

        HSSFCell cell00 = row.createCell(0);
        cell00.setCellValue("企业名称");
        cell00.setCellStyle(style);

        HSSFCell cell01 = row.createCell(1);
        cell01.setCellValue(bill.getBpName());
        cell01.setCellStyle(style1);

        HSSFCell cell02 = row.createCell(2);
        cell02.setCellValue("账户名称");
        cell02.setCellStyle(style);

        HSSFCell cell03 = row.createCell(3);
        cell03.setCellValue(bill.getBpAccountName());
        cell03.setCellStyle(style1);

        HSSFCell cell04 = row.createCell(4);
        cell04.setCellValue("账单生成日期");
        cell04.setCellStyle(style);

        HSSFCell cell05 = row.createCell(5);
        cell05.setCellValue(bill.getCreateDate());
        cell05.setCellStyle(style1);

        HSSFRow row1 = sheet.createRow(1);

        HSSFCell cell10 = row1.createCell(0);
        cell10.setCellValue("账单金额");
        cell10.setCellStyle(style);

        HSSFCell cell11 = row1.createCell(1);
        cell11.setCellValue(bill.getBillAmount());
        cell11.setCellStyle(style1);

        HSSFCell cell12 = row1.createCell(2);
        cell12.setCellValue("结算期间");
        cell12.setCellStyle(style);

        HSSFCell cell13 = row1.createCell(3);
        cell13.setCellValue(bill.getLiqDate());
        cell13.setCellStyle(style1);

        HSSFCell cell14 = row1.createCell(4);
        cell14.setCellValue("逾期日");
        cell14.setCellStyle(style);

        HSSFCell cell15 = row1.createCell(5);
        cell15.setCellValue(bill.getOverdueDate());
        cell15.setCellStyle(style1);

        HSSFRow row2 = sheet.createRow(2);

        HSSFCell cell20 = row2.createCell(0);
        cell20.setCellValue("备注");
        cell20.setCellStyle(style);

        HSSFCell cell21 = row2.createCell(1);
        cell21.setCellValue(bill.getRemark());
        cell21.setCellStyle(style1);

        HSSFCell cell22 = row2.createCell(2);
        cell22.setCellStyle(style1);

        HSSFCell cell23 = row2.createCell(3);
        cell23.setCellStyle(style1);

        HSSFCell cell24 = row2.createCell(4);
        cell24.setCellStyle(style1);

        HSSFCell cell25 = row2.createCell(5);
        cell25.setCellStyle(style1);

        //合并单元格
        HSSFRow row4 = sheet.createRow(4);
        HSSFCell cell40 = row4.createCell(0);
        cell40.setCellValue("账单构成(单位:元)");
        cell40.setCellStyle(style);

        HSSFCell cell41 = row4.createCell(1);
        cell41.setCellValue("  ");
        cell41.setCellStyle(style1);

        HSSFCell cell42 = row4.createCell(2);
        cell42.setCellStyle(style1);

        HSSFCell cell43 = row4.createCell(3);
        cell43.setCellStyle(style1);

        HSSFCell cell44 = row4.createCell(4);
        cell44.setCellStyle(style1);

        HSSFCell cell45 = row4.createCell(5);
        cell45.setCellStyle(style1);

        List<BillAmountItemDto> itemList = bill.getBillAmountItemList();

        if (CollectionUtils.isNotEmpty(itemList)) {
            int size = itemList.size() + 5;

            BillAmountItemDto itemDto = null;
            for (int i = 5; i < size; i++) {

                CellRangeAddress region3 = new CellRangeAddress(i, i, 2, 5);
                sheet.addMergedRegion(region3);

                itemDto = itemList.get(i - 5);

                HSSFRow row5 = sheet.createRow(i);

                HSSFCell cell50 = row5.createCell(0);

                if ((itemDto.getItemName() != null && itemDto.getItemName().startsWith("调整项")) || "订单结算".equals(itemDto.getItemName())) {

                    cell50.setCellValue(itemDto.getItemName());
                } else {
                    cell50.setCellValue("    " + itemDto.getItemName());
                }

                cell50.setCellStyle(style);

                HSSFCell cell51 = row5.createCell(1);
                cell51.setCellValue(itemDto.getAmount());
                cell51.setCellStyle(style1);

                HSSFCell cell52 = row5.createCell(2);
                cell52.setCellValue(itemDto.getRemark());
                cell52.setCellStyle(style1);

                HSSFCell cell53 = row5.createCell(3);
                cell53.setCellStyle(style1);

                HSSFCell cell54 = row5.createCell(4);
                cell54.setCellStyle(style1);

                HSSFCell cell55 = row5.createCell(5);
                cell55.setCellStyle(style1);

            }
        }

    }

    //添加颜色
    private static void setBorders(HSSFCellStyle style, short lc, short bc, short rc, short tc, short bl, short bb, short br, short bt) {
        style.setBottomBorderColor(lc);
        style.setLeftBorderColor(bc);
        style.setRightBorderColor(rc);
        style.setTopBorderColor(tc);
        style.setBorderLeft(bl);
        style.setBorderBottom(bb);
        style.setBorderRight(br);
        style.setBorderTop(bt);
    }

    @SuppressWarnings({"deprecation", "rawtypes", "unchecked"})
    private <T> void createSheet(String[] headers,
                                 Collection<T> dataset, String pattern, HSSFWorkbook workbook, HSSFSheet sheet, boolean isFlag) {

        // 设置表格默认列宽度为15个字节
        sheet.setDefaultColumnWidth((short) 15);

        if (isFlag) {
            sheet.setColumnWidth(0, 256 * 35 + 184);
            sheet.setColumnWidth(1, 256 * 45 + 184);
        }

        sheet.setDefaultRowHeightInPoints(20);

        // 生成一个样式
        HSSFCellStyle style = workbook.createCellStyle();
        // 设置这些样式
        style.setFillForegroundColor(HSSFColor.WHITE.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        // 生成一个字体
        HSSFFont fontStyle3 = workbook.createFont();
        fontStyle3.setFontName("黑体");
        fontStyle3.setFontHeightInPoints((short) 12);
        fontStyle3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        // 把字体应用到当前的样式
        style.setFont(fontStyle3);
        // 生成并设置另一个样式
        HSSFCellStyle style2 = workbook.createCellStyle();
        style2.setFillForegroundColor(HSSFColor.WHITE.index);
        style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        style2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style2.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style2.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        // 生成另一个字体
        HSSFFont font2 = workbook.createFont();
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        // 把字体应用到当前的样式
        style2.setFont(font2);

        // 产生表格标题行
        HSSFRow row = sheet.createRow(0);
        for (short i = 0; i < headers.length; i++) {
            HSSFCell cell = row.createCell(i);
            cell.setCellStyle(style);
            HSSFRichTextString text = new HSSFRichTextString(headers[i]);
            cell.setCellValue(text);
        }
        
        String[] notNumberFiledArr = new String[]{"orderId", "orderNo", "id", "passengersCount", "passengerCount", "stayDay", "ticketNos", "traveller", "desc", "originOrderId", "travelPlanNo", "oldOrderId"};
        Set<String> notNumberFields = new HashSet<>(Arrays.asList(notNumberFiledArr));

        String curOrderId = "";
        // 遍历集合数据，产生数据行
        Iterator<T> it = dataset.iterator();
        int index = 0;
        while (it.hasNext()) {
            index++;
            row = sheet.createRow(index);
            T t = (T) it.next();
            // 利用反射，根据javabean属性的先后顺序，动态调用getXxx()方法得到属性值
            Field[] fields = t.getClass().getDeclaredFields();
            for (short i = 0; i < fields.length; i++) {
                HSSFCell cell = row.createCell(i);
                cell.setCellStyle(style2);
                Field field = fields[i];
                String fieldName = field.getName();

                String getMethodName = "get"
                        + fieldName.substring(0, 1).toUpperCase()
                        + fieldName.substring(1);
                try {
                    Class tCls = t.getClass();
                    Method getMethod = tCls.getMethod(getMethodName, new Class[]{});
                    Object value = getMethod.invoke(t, new Object[]{});

                    // 判断值的类型后进行强制类型转换
                    String textValue = null;
                    if (value instanceof Date) {
                        Date date = (Date) value;
                        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
                        textValue = sdf.format(date);
                    } else {
                        // 其它数据类型都当作字符串简单处理
                        if (value != null) {
                            textValue = value.toString();
                        } else {
                            textValue = "";
                            
                            if(!"id".equals(fieldName) && 
                            		!"oldOrderId".equals(fieldName) && 
                            		!"orderOriginType".equals(fieldName)) {
                            	LogUtils.info(logger, "账单导出时字段值为空, field:{}, orderId:{}", fieldName, curOrderId);
                            }
                        }
                    }

                    if ("id".equals(fieldName)) {
                        textValue = (index) + "";
                    }
                    
                    if ("orderId".equals(fieldName)) {
                    	curOrderId = textValue;
                    }

                    // 利用正则表达式判断textValue是否全部由数字组成
                    Pattern p = Pattern.compile(MATH_NUMBER_VALID);
                    Matcher matcher = p.matcher(textValue);
                    
                    Pattern p1 = Pattern.compile(MATH_NUMBER_VALID1);
                    Matcher matcher1 = p1.matcher(textValue);

                    boolean matcher2 = matcher.matches() || matcher1.matches();
                    boolean isContains = notNumberFields.contains(fieldName);

                    if (matcher2 && !isContains) {
                        // 是数字当作double处理
                        cell.setCellValue(Double.parseDouble(textValue));
                    } else {
                        cell.setCellValue(textValue);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    // 清理资源
                }
            }
        }

    }

}
