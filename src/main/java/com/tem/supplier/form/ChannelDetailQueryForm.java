package com.tem.supplier.form;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.iplatform.common.utils.DateUtils;
import com.tem.supplier.condition.ChannelDetailCondition;

/**
 * 
 * @author marvel
 *
 */
public class ChannelDetailQueryForm {

	/**
     * 入账起始日期
     */
    private String beginDate;

    /**
     * 入账截止日期
     */
    private String endDate;
    
    /**
     * 收支通道
     */
    private String settleChannel;
    
    /**
     * 出票渠道
     */
    private String issueChannel;
    
    /**
     * 交易类型
     */
    private String tradeType;
    
    /**
     * 关键词
     */
    private String keyword;
    /**
     * tmc
     */
    private Long tmcId;
    
    public ChannelDetailCondition toChannelDetailCondition() {
        ChannelDetailCondition condition = new ChannelDetailCondition();
        
    	if(StringUtils.isNotEmpty(endDate)){
    		Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
            condition.setEndDate(end);
    	}
    	if(StringUtils.isNotEmpty(beginDate)){
            Date begin = DateUtils.parse(beginDate, DateUtils.YYYY_MM_DD);
            condition.setBeginDate(begin);
    	}
        condition.setTmcId(tmcId);
        if(StringUtils.isNotBlank(settleChannel) && !"0".equals(settleChannel)){
        	condition.setChannelId(Long.valueOf(settleChannel));
        }
        if(StringUtils.isNotBlank(issueChannel) && !"0".equals(issueChannel)){
        	condition.setSupplyId(Long.valueOf(issueChannel));
        }
        if(StringUtils.isNotBlank(tradeType) && !"0".equals(tradeType)){
        	condition.setTradeType(tradeType);
        }
        if(StringUtils.isNotBlank(keyword)){
            condition.setKeyword(keyword);
        }
        
        return condition;
	}

    
	public String getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getSettleChannel() {
		return settleChannel;
	}

	public void setSettleChannel(String settleChannel) {
		this.settleChannel = settleChannel;
	}

	public String getIssueChannel() {
		return issueChannel;
	}

	public void setIssueChannel(String issueChannel) {
		this.issueChannel = issueChannel;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public Long getTmcId() {
		return tmcId;
	}

	public void setTmcId(Long tmcId) {
		this.tmcId = tmcId;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

}
