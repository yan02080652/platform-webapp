package com.tem.supplier.form;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author chf
 *
 */
public class FlightOrderContextForm {

	  /**
	   * 业务类型,对应excel列 “业务类型”
	   */
	  private String businessType;
	  
	  /**
	   * 交易订单号,对应excel列 “交易订单号”
	   */
	  private String orderId;
	  
	  /**
	   * 出票时间，对应excel列 “交易日期”
	   */
	  private Date issueTicketTime;
	  
	  /**
	   * 订单金额，对应excel列 “订单金额”
	   */
	  private BigDecimal orderAmount;
	  
	  /**
	   * 交易金额，对应excel列 “交易金额”
	   */
	  private BigDecimal transactionAmount;
	  
	  /**
	   * 支付机构，对应excel列 “交易金额”
	   */
	  private String paymentAgent;
	  
	  /**
	   * 交易类型，对应excel列 “交易类型”
	   */
	  private String transactionType;
	  
	  /**
	   * 支付方式，对应excel列 “支付方式”
	   */
	  private String paymentMethod;
	  
	  /**
	   * 支付状态，对应excel列 “支付状态”
	   */
	  private String paymentStatus;
	  
	  
	  
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	public Date getIssueTicketTime() {
		return issueTicketTime;
	}
	public void setIssueTicketTime(Date issueTicketTime) {
		this.issueTicketTime = issueTicketTime;
	}
	public BigDecimal getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(BigDecimal orderAmount) {
		this.orderAmount = orderAmount;
	}
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getPaymentAgent() {
		return paymentAgent;
	}
	public void setPaymentAgent(String paymentAgent) {
		this.paymentAgent = paymentAgent;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	  
	  
	
}
