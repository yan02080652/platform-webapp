package com.tem.supplier.form;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * chenghaofan
 */
public class SettleChannelDetailShowForm {

	/**
	 * 主键
	 */
	public Long id;

	/**
     * 入账日期   、交易时间
     */
	public Date tradeTime;
    /**
     * 所属通道
     */
	public String channelName;
    /**
     * 交易类型,order-订单交易;adjust-调整;settle-销账
     */
	public String tradeType;
    /**
     * 收支方向
     */
	public String direction;
    /**
     * 交易金额，放大100倍存储, 收入为“+”，支出为“-”
     */
	public BigDecimal amount;
    
    /**
     * 供应商名称
     */
	public String supplyName;
    /**
     * 供应商订单号/票号等
     */
	public String supplyRefRecord;
    
    /**
     * tem订单号/账单号
     */
	public String temRefRecord;
    /**
     * 收支通道流水号
     */
	public String channelRefRecord;
    /**
     * 描述
     */
	public String descr;
   
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getTradeTime() {
		return tradeTime;
	}
	public void setTradeTime(Date tradeTime) {
		this.tradeTime = tradeTime;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	public String getSupplyName() {
		return supplyName;
	}
	public void setSupplyName(String supplyName) {
		this.supplyName = supplyName;
	}
	public String getSupplyRefRecord() {
		return supplyRefRecord;
	}
	public void setSupplyRefRecord(String supplyRefRecord) {
		this.supplyRefRecord = supplyRefRecord;
	}
	public String getTemRefRecord() {
		return temRefRecord;
	}
	public void setTemRefRecord(String temRefRecord) {
		this.temRefRecord = temRefRecord;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getChannelRefRecord() {
		return channelRefRecord;
	}
	public void setChannelRefRecord(String channelRefRecord) {
		this.channelRefRecord = channelRefRecord;
	}
	
}
