package com.tem.supplier.form;

import java.math.BigDecimal;

/**
 * 
 * @author marvel
 *
 */
public class ChannelDetailDataForm {

	/**
     * 收入
     */
    private BigDecimal incomeMoney;
    /**
     * 支出
     */
    private BigDecimal disburseMoney;
    /**
     * 合计
     */
    private BigDecimal totalMoney;
    
	public BigDecimal getIncomeMoney() {
		return incomeMoney;
	}
	public void setIncomeMoney(BigDecimal incomeMoney) {
		this.incomeMoney = incomeMoney;
	}
	public BigDecimal getDisburseMoney() {
		return disburseMoney;
	}
	public void setDisburseMoney(BigDecimal disburseMoney) {
		this.disburseMoney = disburseMoney;
	}
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}


}
