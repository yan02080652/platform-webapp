package com.tem.supplier.form;

import java.util.Date;

/**
 * 
 * llf
 */
public class SettleChannelShowForm {
	
	/**
	 * 主键
	 */
	private Long id;

	/**
	 * 类型
	 */
	public String type;

    /**
     * 名称
     */
	public String name;
    /**
     * 描述
     */
	public String descr;
    /**
     * 最后修改人
     */
	public String update_user;
    /**
     * 最后修改时间
     */
	public Date update_date;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
    
   
	
	
}
