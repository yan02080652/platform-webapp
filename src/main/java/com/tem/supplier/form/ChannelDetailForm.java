package com.tem.supplier.form;

import java.util.Date;

/**
 * 
 * @author marvel
 *
 */
public class ChannelDetailForm {

	/**
     * 入账日期
     */
    private String tradeTime;

    
    /**
     * 收支通道
     */
    private String settleChannelId;
    
    /**
     * 出票渠道
     */
    private String issueChannelId;
    
    /**
     * 金额
     */
    private String amount;
    /**
     * 摘要描述
     */
    private String descr;
    
    /**
     * 供应单号
     */
    private String supplyRefRecord;
    /**
     * 交易类型
     */
    private String tradeType;
    /**
     * tmc
     */
    private Long tmcId;
    /**
     * 创建人
     */
    private Long createUser;
    
    /**
    * 创建时间
    */
   private Date createDate;
    
	public String getTradeTime() {
		return tradeTime;
	}
	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}
	public String getSettleChannelId() {
		return settleChannelId;
	}
	public void setSettleChannelId(String settleChannelId) {
		this.settleChannelId = settleChannelId;
	}
	public String getIssueChannelId() {
		return issueChannelId;
	}
	public void setIssueChannelId(String issueChannelId) {
		this.issueChannelId = issueChannelId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getTradeType() {
		return tradeType;
	}
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	public Long getTmcId() {
		return tmcId;
	}
	public void setTmcId(Long tmcId) {
		this.tmcId = tmcId;
	}
	public Long getCreateUser() {
		return createUser;
	}
	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getSupplyRefRecord() {
		return supplyRefRecord;
	}
	public void setSupplyRefRecord(String supplyRefRecord) {
		this.supplyRefRecord = supplyRefRecord;
	}
    
}
