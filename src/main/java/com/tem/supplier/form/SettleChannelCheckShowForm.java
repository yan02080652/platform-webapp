package com.tem.supplier.form;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author marvel
 *
 */
public class SettleChannelCheckShowForm {

	/**
	 * 收支通道名称、入账日期、审核日期
	 */
	private String title;
	
	private boolean titleFlag = false;
	
	
	/**
     * 所属通道id
     */
	private Long channelId;
	
	/**
	 * "",小计、合计
	 */
	private String titleType;
	/**
	 *核对状态
	 */
	private String statusName;
	
	/**
     * 收入金额
     */
    private BigDecimal incomeMoney = new BigDecimal(0);
    /**
     * 收入笔数
     */
    private int  incomeCount;
    /**
     * 支出金额
     */
    private BigDecimal disburseMoney = new BigDecimal(0);
    /**
     * 支出笔数
     */
    private int disburseCount;
    /**
     * 合计金额
     */
    private BigDecimal totalMoney = new BigDecimal(0);
    /**
     * 合计笔数
     */
    private int totalCount;
    /**
     * 期末余额
     */
    private BigDecimal endBalanceMoney;
    /**
     * 最后修改人
     */
    private String updateUser;
	
    /**
     * 最后修改时间
     */
    private Date updateDate;

    /**
     * 列表审核入账日期 起始日期
     */
    private String checkedBeginDate;
    
    /**
     * 列表审核入账日期 截止日期
     */
    private String checkedEndDate;
    
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleType() {
		return titleType;
	}

	public void setTitleType(String titleType) {
		this.titleType = titleType;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public BigDecimal getIncomeMoney() {
		return incomeMoney;
	}

	public void setIncomeMoney(BigDecimal incomeMoney) {
		this.incomeMoney = incomeMoney;
	}

	public int getIncomeCount() {
		return incomeCount;
	}

	public void setIncomeCount(int incomeCount) {
		this.incomeCount = incomeCount;
	}

	public BigDecimal getDisburseMoney() {
		return disburseMoney;
	}

	public void setDisburseMoney(BigDecimal disburseMoney) {
		this.disburseMoney = disburseMoney;
	}

	public int getDisburseCount() {
		return disburseCount;
	}

	public void setDisburseCount(int disburseCount) {
		this.disburseCount = disburseCount;
	}

	public BigDecimal getTotalMoney() {
		return totalMoney;
	}

	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public BigDecimal getEndBalanceMoney() {
		return endBalanceMoney;
	}

	public void setEndBalanceMoney(BigDecimal endBalanceMoney) {
		this.endBalanceMoney = endBalanceMoney;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getChannelId() {
		return channelId;
	}

	public void setChannelId(Long channelId) {
		this.channelId = channelId;
	}

	public void addDisburseMoney(BigDecimal money) {
		this.disburseMoney = this.disburseMoney.add(money);
	}
	public void addIncomeMoney(BigDecimal money) {
		this.incomeMoney = this.incomeMoney.add(money);
	}
	public void addTotalMoney(BigDecimal money) {
		this.totalMoney = this.totalMoney.add(money);
	}
	public void addDisburseCount(int count) {
		this.disburseCount+=count;
	}
	public void addIncomeCount(int count) {
		this.incomeCount+=count;
	}
	public void addTotalCount(int count) {
		this.totalCount+=count;
	}

	public boolean isTitleFlag() {
		return titleFlag;
	}

	public void setTitleFlag(boolean titleFlag) {
		this.titleFlag = titleFlag;
	}

	public String getCheckedBeginDate() {
		return checkedBeginDate;
	}

	public void setCheckedBeginDate(String checkedBeginDate) {
		this.checkedBeginDate = checkedBeginDate;
	}

	public String getCheckedEndDate() {
		return checkedEndDate;
	}

	public void setCheckedEndDate(String checkedEndDate) {
		this.checkedEndDate = checkedEndDate;
	}
   

}
