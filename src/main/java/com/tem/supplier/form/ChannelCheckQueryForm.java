package com.tem.supplier.form;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.iplatform.common.utils.DateUtils;
import com.tem.supplier.condition.ChannelCheckCondition;
import com.tem.supplier.condition.ChannelDetailCondition;
import com.tem.supplier.condition.ChannelDetailQuery;

/**
 * 
 * @author marvel
 *
 */
public class ChannelCheckQueryForm {

	/**
     * 入账起始日期
     */
    private String beginDate;

    /**
     * 入账截止日期
     */
    private String endDate;
    
    /**
     * 收支通道
     */
    private String settleChannel;
    
    /**
     * 收支通道类型
     */
    private String type;
    
    /**
     * 审核
     */
    private String status;
    
    
    /**
     * tmc
     */
    private Long tmcId;


	public String getBeginDate() {
		return beginDate;
	}


	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getSettleChannel() {
		return settleChannel;
	}


	public void setSettleChannel(String settleChannel) {
		this.settleChannel = settleChannel;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Long getTmcId() {
		return tmcId;
	}


	public void setTmcId(Long tmcId) {
		this.tmcId = tmcId;
	}


	public ChannelDetailQuery toChannelDetailQuery() {
		ChannelDetailQuery query =new ChannelDetailQuery();
		
		query.setTmcId(tmcId);
		
		if(StringUtils.isNotEmpty(endDate)){
    		Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
    		query.setEndDate(end);
    	}
    	if(StringUtils.isNotEmpty(beginDate)){
            Date begin = DateUtils.parse(beginDate, DateUtils.YYYY_MM_DD);
            query.setBeginDate(begin);
    	}
    	if(StringUtils.isNotBlank(settleChannel) && !"0".equals(settleChannel)){
    		query.setChannelId(Long.valueOf(settleChannel));
        }
    	if(StringUtils.isNotBlank(type) && !"0".equals(type)){
    		query.setType(type);
        }
    	
    	
		return query;
	}
    
    public ChannelCheckCondition toChannelCheckCondition() {
    	ChannelCheckCondition condition = new ChannelCheckCondition();
        
    	if(StringUtils.isNotEmpty(endDate)){
    		Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
            condition.setEndDate(end);
    	}
    	if(StringUtils.isNotEmpty(beginDate)){
            Date begin = DateUtils.parse(beginDate, DateUtils.YYYY_MM_DD);
            condition.setBeginDate(begin);
    	}
        condition.setTmcId(tmcId);
        
        if(StringUtils.isNotBlank(settleChannel) && !"0".equals(settleChannel)){
        	condition.setChannelId(Long.valueOf(settleChannel));
        }
        /*if(StringUtils.isNotEmpty(status)){
            condition.setStatus(Integer.parseInt(status));
        }*/
        
        return condition;
	}

    
	

}
