package com.tem.supplier.form;

import java.math.BigDecimal;

/**
 * 
 * @author marvel
 *
 */
public class ChannelCheckDataForm {

	/**
     * 收入金额
     */
    private BigDecimal incomeMoney = new BigDecimal(0);
    /**
     * 收入笔数
     */
    private int  incomeCount;
    /**
     * 支出金额
     */
    private BigDecimal disburseMoney = new BigDecimal(0);
    /**
     * 支出笔数
     */
    private int disburseCount;
    /**
     * 合计金额
     */
    private BigDecimal totalMoney = new BigDecimal(0);
    /**
     * 合计笔数
     */
    private int totalCount;
    /**
     * 期末余额
     */
    private BigDecimal endBalanceMoney;
    
    /**
     * 勾选的集合
     */
    private String checkItems;
    /**
     * 1表示核对成功，0表示校验不通过
     */
    private String msgCode;
    
    private String msg;
    
	public BigDecimal getIncomeMoney() {
		return incomeMoney;
	}
	public void setIncomeMoney(BigDecimal incomeMoney) {
		this.incomeMoney = incomeMoney;
	}
	public BigDecimal getDisburseMoney() {
		return disburseMoney;
	}
	public void setDisburseMoney(BigDecimal disburseMoney) {
		this.disburseMoney = disburseMoney;
	}
	public BigDecimal getTotalMoney() {
		return totalMoney;
	}
	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}
	public int getIncomeCount() {
		return incomeCount;
	}
	public void setIncomeCount(int incomeCount) {
		this.incomeCount = incomeCount;
	}
	public int getDisburseCount() {
		return disburseCount;
	}
	public void setDisburseCount(int disburseCount) {
		this.disburseCount = disburseCount;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public BigDecimal getEndBalanceMoney() {
		return endBalanceMoney;
	}
	public void setEndBalanceMoney(BigDecimal endBalanceMoney) {
		this.endBalanceMoney = endBalanceMoney;
	}
	public String getCheckItems() {
		return checkItems;
	}
	public void setCheckItems(String checkItems) {
		this.checkItems = checkItems;
	}
	
	public void addDisburseMoney(BigDecimal money) {
		this.disburseMoney = this.disburseMoney.add(money);
	}
	public void addIncomeMoney(BigDecimal money) {
		this.incomeMoney = this.incomeMoney.add(money);
	}
	public void addTotalMoney(BigDecimal money) {
		this.totalMoney = this.totalMoney.add(money);
	}
	public void addDisburseCount(int count) {
		this.disburseCount+=count;
	}
	public void addIncomeCount(int count) {
		this.incomeCount+=count;
	}
	public void addTotalCount(int count) {
		this.totalCount+=count;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
