package com.tem.supplier.form;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author marvel
 *
 */
public class SettleChannelCheckLogShowForm {

	/**
	 * 主键
	 */
	private Long id;

	/**
	 * 所属tmc
	 */
    private Long tmcId;

    /**
     * 所属通道id
     */
    private String channelName;

    /**
     * 校对的业务时间(记录当天的12点)
     */
    private Date checkedDate;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 操作人
     */
    private String createUserName;

    /**
     * 状态，0-正常核销;1-取消核销
     */
    private String statusName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTmcId() {
		return tmcId;
	}

	public void setTmcId(Long tmcId) {
		this.tmcId = tmcId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public Date getCheckedDate() {
		return checkedDate;
	}

	public void setCheckedDate(Date checkedDate) {
		this.checkedDate = checkedDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
    
	
}
