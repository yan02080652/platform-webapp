package com.tem.supplier.form;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.iplatform.common.utils.DateUtils;
import com.tem.supplier.condition.ChannelCheckLogCondition;

/**
 * 
 * @author marvel
 *
 */
public class ChannelCheckLogQueryForm {

	/**
     * 校对的业务时间   起始日期
     */
    private String beginDate;

    /**
     * 校对的业务时间  截止日期
     */
    private String endDate;
    
    /**
     * 收支通道
     */
    private String settleChannel;
    
    
    /**
     * tmc
     */
    private Long tmcId;


	public String getBeginDate() {
		return beginDate;
	}


	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getSettleChannel() {
		return settleChannel;
	}


	public void setSettleChannel(String settleChannel) {
		this.settleChannel = settleChannel;
	}


	public Long getTmcId() {
		return tmcId;
	}


	public void setTmcId(Long tmcId) {
		this.tmcId = tmcId;
	}


	public ChannelCheckLogCondition toChannelCheckLogCondition() {
		ChannelCheckLogCondition query =new ChannelCheckLogCondition();
		
		query.setTmcId(tmcId);
		
		if(StringUtils.isNotEmpty(endDate)){
    		Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
    		query.setEndDate(end);
    	}
    	if(StringUtils.isNotEmpty(beginDate)){
            Date begin = DateUtils.parse(beginDate, DateUtils.YYYY_MM_DD);
            query.setBeginDate(begin);
    	}
    	if(StringUtils.isNotBlank(settleChannel) && !"0".equals(settleChannel)){
    		query.setChannelId(Long.valueOf(settleChannel));
        }
		return query;
	}
    
    
	

}
