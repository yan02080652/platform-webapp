package com.tem.supplier.util;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 时间工具
 * 
 * @author chf
 *
 */
public class DateTool {

	/**
	 * 将长时间格式字符串转换为时间 yyyy-MM-dd HH:mm:ss
	 * 
	 * @param strDate
	 * @return
	 */
	public static Date strToDateLong(String strDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ParsePosition pos = new ParsePosition(0);
		Date strtodate = formatter.parse(strDate, pos);
		return strtodate;
	}

	/**
	 * 计算两个时间的时间差（单位：h）
	 * 
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	public static long dateInterval(Date beginDate, Date endDate) {
		long beginTime = beginDate.getTime();
		long endTime = endDate.getTime();
		long betweenHours = (long) ((endTime - beginTime) / (1000 * 60 * 60));
		return betweenHours;
	}

	/**
	 * 获取两个日期之间的日期(包含begin,end,以及end+1天)
	 * 
	 * @param start
	 *            开始日期
	 * @param end
	 *            结束日期
	 * @return 日期集合
	 */
	public static List<Date> getBetweenDates(Date begin, Date end) {

		List<Date> result = new ArrayList<Date>();
		Calendar tempStart = Calendar.getInstance();
		tempStart.setTime(begin);
		while (begin.getTime() <= end.getTime()) {
			result.add(tempStart.getTime());
			tempStart.add(Calendar.DAY_OF_YEAR, 1);
			begin = tempStart.getTime();
		}
		// 添加一天
		result.add(tempStart.getTime());
		tempStart.add(Calendar.DAY_OF_YEAR, 1);
		begin = tempStart.getTime();

		return result;
	}

	/**
	 * 指定日期字符串的前一天
	 * @param specifiedDay
	 * @return
	 */
	public static String lastDayStr(String specifiedDay) {
		Calendar c = Calendar.getInstance();
		Date date = null;
		try {
			date = new SimpleDateFormat("yy-MM-dd").parse(specifiedDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day - 1);

		String dayBefore = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
		return dayBefore;
	}

	/**
	 * 指定日期字符串的后一天
	 * @param specifiedDay
	 * @return
	 */
	public static String nextDayStr(String specifiedDay) {
		Calendar c = Calendar.getInstance();
		Date date = null;
		try {
			date = new SimpleDateFormat("yy-MM-dd").parse(specifiedDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day + 1);

		String dayAfter = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
		return dayAfter;
	}

	public static void main(String[] args) {
		List<Date> result = getBetweenDates(strToDateLong("2018-09-10 00:00:00"), strToDateLong("2018-09-10 23:59:59"));
		for (Date date : result) {
			System.out.println(date.toString());
		}
	}
}
