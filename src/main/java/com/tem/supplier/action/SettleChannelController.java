package com.tem.supplier.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.supplier.api.SettleChannelService;
import com.tem.supplier.dto.SettleChannelDto;
import com.tem.supplier.enums.ChannelTradeType;
import com.tem.flight.Constants;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.UserService;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.supplier.form.SettleChannelShowForm;


/**
 * 收支通道
 * @author llf 
 *
 */
@Controller
@RequestMapping("supplier/channel")
public class SettleChannelController extends BaseController {
	
	@Autowired
	private SettleChannelService settleChannelService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private IssueChannelService issueChannelService;

	@RequestMapping(value = "")
	public String channel(Model model,String state){
        
		return "supplier/settleChannelIndex.base";
	}
	
	@RequestMapping(value = "/list")
	public String channelList(Model model,Integer pageIndex, Integer pageSize,String channelName){
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) { 
			pageSize = 10;
		}
		
		QueryDto<Object> queryDto = new QueryDto<>(pageIndex, pageSize);
		//当前登录tmc
		queryDto.setField3(super.getTmcId());
		//通道名称
		if(channelName != null && !"".equals(channelName)){
			 queryDto.setField1(channelName);
		}	
		//通道状态
		queryDto.setField4(0);
		
		Page<SettleChannelDto> pageList = settleChannelService.queryListWithPage(queryDto);
		
		List<SettleChannelDto> list = pageList.getList();
		List<SettleChannelShowForm> resultList = this.getSettleChannelShowList(list);
		List<SettleChannelShowForm> resultList1 = new ArrayList<>();
		
		int  num = pageList.getStart()+10;
		if(resultList.size()<pageList.getStart()+10){
			num = resultList.size();
		}
		
		for(int i=pageList.getStart(); i<num; i++){
			resultList1.add(resultList.get(i));
		}
				
		Page<SettleChannelShowForm> resultListPage = new Page<SettleChannelShowForm>(pageList.getStart(), pageList.getSize(), resultList1, pageList.getTotal());

        model.addAttribute("pageList", resultListPage);
        
		return "supplier/settleChannelList.jsp";
	}
	
	//收支通道展示转换
    private List<SettleChannelShowForm> getSettleChannelShowList(List<SettleChannelDto> list){  	  	
		List<SettleChannelShowForm> resultList = new ArrayList<SettleChannelShowForm>();
		if(CollectionUtils.isNotEmpty(list)){
			for(SettleChannelDto settleChannelDto : list){
				SettleChannelShowForm channelShowForm = new SettleChannelShowForm();
				if(settleChannelDto.getId()!=null){					
					channelShowForm.setId(settleChannelDto.getId());
				}
				if(settleChannelDto.getType()!=null){
					if(settleChannelDto.getType().equals("CREDIT")){						
						channelShowForm.setType("欠款");
					}else if(settleChannelDto.getType().equals("DEPOSIT")){
						channelShowForm.setType("预存");
					}else if(settleChannelDto.getType().equals("CASH")){
						channelShowForm.setType("现金");
					}
						
				}
				if(settleChannelDto.getName()!=null){
					channelShowForm.setName(settleChannelDto.getName());
				}
				if(settleChannelDto.getDescr()!=null){
					channelShowForm.setDescr(settleChannelDto.getDescr());
				}
				if(settleChannelDto.getUpdateUser()!=null){
					String userName = userService.getFullName(settleChannelDto.getUpdateUser());
					if(userName!=null){
						channelShowForm.setUpdate_user(userName);
					}
				}
				if(settleChannelDto.getUpdateDate()!=null){
					channelShowForm.setUpdate_date(settleChannelDto.getUpdateDate());
				}
					
				resultList.add(channelShowForm);
			}
		}
		return resultList;
	}	
    
    @RequestMapping("/detail")
    public String detail(Model model, Long id) {
    	SettleChannelDto settleChannelDto = null;
        if (id != null) {
        	settleChannelDto = settleChannelService.getById(id);
        }
        
        model.addAttribute("settleChannelDto", settleChannelDto);
        model.addAttribute("channelType", getTypeMap());
        return "supplier/settleChannelDetails.jsp";
    }
    
    private Map<String,String> getTypeMap(){
        Map<String,String> map = new LinkedHashMap<>();
        ChannelTradeType[] values = ChannelTradeType.values();
        for (ChannelTradeType i : values) {
            map.put(i.name(),i.getValue());
        }
        return map;
    }
    
    @ResponseBody
	@RequestMapping("/save")
	public ResponseInfo save(SettleChannelDto settleChannelDto){
    	
    	//判断是新增还是修改
    	if(settleChannelDto.getId()==null){
    		//新增
    		settleChannelDto.setTmcId(super.getTmcId());
    		settleChannelDto.setCreateDate(new Date());
    		settleChannelDto.setCreateUser(super.getCurrentUser().getId());
    		settleChannelDto.setUpdateDate(new Date());
    		settleChannelDto.setUpdateUser(super.getCurrentUser().getId());
    		settleChannelDto.setStatus(0);
    		SettleChannelDto channelDto = settleChannelService.save(settleChannelDto);
    		if(channelDto.getId()!=null){
    			return new ResponseInfo("0","保存成功");
    		}else{
    			return new ResponseInfo("1", "保存失败");
    		}
    		
    	}else{
    		//修改
    		//更新时间
    		settleChannelDto.setUpdateDate(new Date());
    		//更新人
    		settleChannelDto.setUpdateUser(super.getCurrentUser().getId());
    		boolean update = settleChannelService.update(settleChannelDto);
    		if(update){
    			return new ResponseInfo("0","保存成功");
    		}else{
    			return new ResponseInfo("1", "保存失败");
    		}
    	}
	}
    
    @RequestMapping("/delete/{id}")
    @ResponseBody
    public ResponseInfo delete(Model model, @PathVariable("id") Long id) {
    	//是否被供应商关联
    	List<IssueChannelDto> list= issueChannelService.findIssuesBySettleId(id);
    	if(list.size()!=0){
    		String name = list.get(0).getName();
    		return new ResponseInfo("2", "该收支通道与【"+name+"】存在关联，无法停用");
    	}else{
    		
    		SettleChannelDto settleChannelDto = new SettleChannelDto();
    		settleChannelDto.setId(id);
    		settleChannelDto.setStatus(1);
    		boolean remove = settleChannelService.update(settleChannelDto);
    		return remove ? new ResponseInfo("0", "success") : new ResponseInfo("1", "failed");
    	}       
    }
}
