package com.tem.supplier.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.DateUtils;
import com.tem.supplier.api.SettleChannelCheckLogService;
import com.tem.supplier.api.SettleChannelCheckService;
import com.tem.supplier.api.SettleChannelDetailService;
import com.tem.supplier.api.SettleChannelService;
import com.tem.supplier.condition.ChannelCheckCondition;
import com.tem.supplier.condition.ChannelCheckLogCondition;
import com.tem.supplier.condition.ChannelDetailOnCheckQuery;
import com.tem.supplier.condition.ChannelDetailQuery;
import com.tem.supplier.dto.SettleChannelCheckDto;
import com.tem.supplier.dto.SettleChannelCheckLogDto;
import com.tem.supplier.dto.SettleChannelDetailDto;
import com.tem.supplier.dto.SettleChannelDto;
import com.tem.supplier.enums.ChannelTradeType;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.UserService;
import com.tem.pss.api.IssueChannelService;
import com.tem.supplier.form.ChannelCheckDataForm;
import com.tem.supplier.form.ChannelCheckLogQueryForm;
import com.tem.supplier.form.ChannelCheckQueryForm;
import com.tem.supplier.form.ChannelDetailQueryForm;
import com.tem.supplier.form.SettleChannelCheckLogShowForm;
import com.tem.supplier.form.SettleChannelCheckShowForm;
import com.tem.supplier.form.SettleChannelDetailShowForm;
import com.tem.supplier.util.DateTool;

/**
 * 收支明细核销记录
 * @author chenghaofan 
 *
 */
@Controller
@RequestMapping("supplier/channelCheck")
public class SettleChannelCheckController extends BaseController {

	@Autowired
	private SettleChannelDetailService settleChannelDetailService;
	
	@Autowired
	private SettleChannelCheckService settleChannelCheckService;
	
	@Autowired
	private SettleChannelCheckLogService settleChannelCheckLogService;
	
	@Autowired
	private SettleChannelService settleChannelService;
	
	@Autowired
    private IssueChannelService issueChannelService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SettleChannelDetailController settleChannelDetailController;
	
	 
	private final Logger logger = LoggerFactory.getLogger(SettleChannelCheckController.class);

	@RequestMapping("/check")
	public String channelDetail(Model model,String state){
		
		//收支通道类型
		ChannelTradeType[] types = ChannelTradeType.values();
        model.addAttribute("types",types);

        
        //收支通道
        List<SettleChannelDto> settleChannels= settleChannelService.findSettleChannelByTmcId(super.getTmcId(),0);
        model.addAttribute("settleChannels",settleChannels);
        
		return "supplier/settleChannelCheck.base";
	}
	@RequestMapping("/checklList")
	public String channelChecklList(Model model,ChannelCheckQueryForm condition){
		
		/**
		 * 1、根据检索条件获取收支明细记录
		 * 	1.1	按天分割汇总对象
		 * 	1.2  按收支通道id 分割汇总对象
		 * 2、根据检索日期条件 获取收支明细审核记录
		 * 3、拼接展示对象
		 */
		condition.setTmcId(super.getTmcId());
		//根据检索条件获取收支明细记录
		List<SettleChannelDetailDto>  detailDtoList = settleChannelDetailService.querySettleChannelDetail(condition.toChannelDetailQuery());
		//根据 收支通道 分割 收支明细记录
		Map<Long,List<SettleChannelDetailDto>> channelDetailMap = new HashMap<Long,List<SettleChannelDetailDto>>();
		
		for(SettleChannelDetailDto dto : detailDtoList){
			if (channelDetailMap.containsKey(dto.getChannelId())) {
				channelDetailMap.get(dto.getChannelId()).add(dto);
			} else {
				List<SettleChannelDetailDto> list = new ArrayList<SettleChannelDetailDto>();
				list.add(dto);
				channelDetailMap.put(dto.getChannelId(),list);
			}
		}
		
		//收支通道
        List<SettleChannelDto> settleChannels= settleChannelService.findSettleChannelByTmcId(super.getTmcId(),null);
        Map<Long,String> settleChannelMap = new HashMap<Long,String>();
        for(SettleChannelDto dto: settleChannels){
        	settleChannelMap.put(dto.getId(), dto.getName());
        }
        
		//channelDetailMap添加不含收支记录的通道对象
		for(SettleChannelDto dto: settleChannels){
			if (!channelDetailMap.containsKey(dto.getId())) {
				channelDetailMap.put(dto.getId(), new ArrayList<SettleChannelDetailDto>());
			}
		}
		
		//根据检索日期条件 获取收支明细审核记录
		List<SettleChannelCheckDto>  checkDtoList = settleChannelCheckService.querySettleChannelCheck(condition.toChannelCheckCondition());
		//根据 收支通道 分割 收支明细审核记录
		Map<Long,List<SettleChannelCheckDto>> channelCheckMap = new HashMap<Long,List<SettleChannelCheckDto>>();
		for(SettleChannelCheckDto dto : checkDtoList){
			if (channelCheckMap.containsKey(dto.getChannelId())) {
				channelCheckMap.get(dto.getChannelId()).add(dto);
			} else {
				List<SettleChannelCheckDto> list = new ArrayList<SettleChannelCheckDto>();
				list.add(dto);
				channelCheckMap.put(dto.getChannelId(),list);
			}
		}
		
		
		if(condition.getBeginDate().compareTo(condition.getEndDate())<=0){
			//获取选择的日期集合
	        List<Date> dateResult = DateTool.getBetweenDates(DateTool.strToDateLong(condition.getBeginDate()+" 00:00:00"),DateTool.strToDateLong(condition.getEndDate()+" 23:59:59"));
			
			//根据条件筛选数据
			if(!"0".equals(condition.getSettleChannel()) || !"0".equals(condition.getType())){
				
				SettleChannelDto dto = new SettleChannelDto();
				if(!"0".equals(condition.getSettleChannel())){
					dto.setId(Long.valueOf(condition.getSettleChannel()));
				}
				if(!"0".equals(condition.getType())){
					dto.setType(condition.getType());
				}
				dto.setTmcId(super.getTmcId());
				List<SettleChannelDto>  settleChannelList = settleChannelService.findSettleChannelList(dto);
				
				Map<Long,List<SettleChannelDetailDto>> channelDetailSearchMap = new HashMap<Long,List<SettleChannelDetailDto>>();
				if(settleChannelList.size() == 0){
					model.addAttribute("pageList",new ArrayList<SettleChannelCheckShowForm>());
				}else{
					for(SettleChannelDto settleChannel: settleChannelList){
						channelDetailSearchMap.put(settleChannel.getId(), channelDetailMap.get(settleChannel.getId()));
					}
					
					List<SettleChannelCheckShowForm> checkFromList = getSettleChannelCheckShowForm(channelDetailSearchMap,channelCheckMap,settleChannelMap,dateResult,condition.getStatus());
					model.addAttribute("pageList",checkFromList);
				}
				
				
			}else{ //无收支通道筛选条件
				List<SettleChannelCheckShowForm> checkFromList = getSettleChannelCheckShowForm(channelDetailMap,channelCheckMap,settleChannelMap,dateResult,condition.getStatus());
				model.addAttribute("pageList",checkFromList);
			}
			
		}else{
			model.addAttribute("pageList",new ArrayList<SettleChannelCheckShowForm>());
		}
		return "supplier/settleChannelCheckList.jsp";
	}
	
	@RequestMapping("/toCheckData")
	public String toCheckData(Model model,ChannelCheckDataForm channelCheckDataForm){
		
		Map<String,List<String>> itemsMap= transCheckItems(channelCheckDataForm.getCheckItems());
		
		
		ChannelCheckDataForm checkDataForm = new  ChannelCheckDataForm();
		
		checkDataForm.setCheckItems(channelCheckDataForm.getCheckItems());
		
		ChannelDetailQuery condition = new ChannelDetailQuery();
		condition.setTmcId(super.getTmcId());
		
		for (Map.Entry<String,List<String>> entry : itemsMap.entrySet()) {
			List<String> dateStr =  entry.getValue();
			
			condition.setChannelId(Long.parseLong(entry.getKey()));
			for(String day:dateStr){
				condition.setBeginDate(DateTool.strToDateLong(day+" 00:00:00"));
				condition.setEndDate(DateTool.strToDateLong(day+" 23:59:59"));
				//根据检索条件获取收支明细记录
				List<SettleChannelDetailDto>  detailDtoList = settleChannelDetailService.querySettleChannelDetail(condition);
				if(detailDtoList != null){
					for(SettleChannelDetailDto dto:detailDtoList){
						if(dto.getAmount()>=0){
							//收入
							checkDataForm.addIncomeMoney(new BigDecimal(dto.getAmount()).divide(new BigDecimal(100)));
							checkDataForm.addIncomeCount(1);
						}else{
							//支出
							checkDataForm.addDisburseMoney(new BigDecimal(dto.getAmount()).divide(new BigDecimal(100)));
							checkDataForm.addDisburseCount(1);
						}
						//合计
						checkDataForm.addTotalMoney(new BigDecimal(dto.getAmount()).divide(new BigDecimal(100)));
						checkDataForm.addTotalCount(1);
					}
				}
				
			}
			
		}
		
		model.addAttribute("checkDataForm", checkDataForm);
		
		return "supplier/settleChannelCheckData.jsp";
	}
	/**
	 * 核对数据
	 * @param model
	 * @param channelCheckDataForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/checkData")
	public ResponseInfo checkData(Model model,ChannelCheckDataForm channelCheckDataForm){
		
		Map<String,List<String>> itemsMap= transCheckItems(channelCheckDataForm.getCheckItems());
		
		//返回对象
		ChannelCheckDataForm checkDataForm = new  ChannelCheckDataForm();
		//检查审核条件
		for (Map.Entry<String,List<String>> entry : itemsMap.entrySet()) {
			List<String> dateList =  entry.getValue();
			//实现排序方法  
	       Collections.sort(dateList, new Comparator() {  
	            @Override  
	            public int compare(Object o1, Object o2) {  
	                String str1=(String) o1;  
	                String str2=(String) o2;  
	                if (str1.compareToIgnoreCase(str2)<0){    
	                    return -1;    
	                }    
	                return 1;    
	            }  
	        });
	       //所选通道，日期的前一天是否为已审核
	       /*ChannelCheckCondition queryDto= new ChannelCheckCondition();
	       queryDto.setEndDate(DateTool.strToDateLong(dateList.get(0)+" 00:00:00"));
	       queryDto.setTmcId(super.getTmcId());
	       queryDto.setStatus(0);
		   List<SettleChannelCheckDto> list = settleChannelCheckService.querySettleChannelCheck(queryDto);*/
		   //根据检索条件  获取   未审核和取消的收支明细记录
	       ChannelDetailOnCheckQuery checkQueryDto = new ChannelDetailOnCheckQuery();
	       checkQueryDto.setTmcId(super.getTmcId());
	       checkQueryDto.setChannelId(Long.parseLong(entry.getKey()));
	       //所选日期的前一天
	       checkQueryDto.setEndDate(DateTool.strToDateLong(DateTool.lastDayStr(dateList.get(0))+" 23:59:59"));
		   
		   List<SettleChannelDetailDto>  detailDtoList = settleChannelDetailService.queryDetailByCheck(checkQueryDto);
		   if(detailDtoList == null || detailDtoList.size()==0){
			   //判断时间是否为连续的
			   for(int i=0 ;i<dateList.size()-1;i++){
		    	   if(!DateTool.nextDayStr(dateList.get(i)).equals(dateList.get(i+1))){
					   return new ResponseInfo("-1", "所选的收支通道日期为非连续的，无法进行核对!");
		    	   }
		       }
		   }else{
			   
			   return new ResponseInfo("-1", "所选日期之前存在未核对的记录，无法完成核对!");
		   }
		}
		//条件符合要求，进行完成审核操作
		
		for (Map.Entry<String,List<String>> entry : itemsMap.entrySet()) {
			List<String> dateList =  entry.getValue();
			for(String day:dateList){
				// saveOrUpdate  未审核插入已核记录，取消状态更新成已核,已核状态，不改变
				SettleChannelCheckDto record = new SettleChannelCheckDto();
				record.setTmcId(super.getTmcId());
				record.setChannelId(Long.parseLong(entry.getKey()));
				record.setCheckedDate(DateTool.strToDateLong(day+" 12:00:00"));
				record.setCreateDate(new Date());
				record.setCreateUser(super.getCurrentUser().getId());
				record.setUpdateDate(new Date());
				record.setUpdateUser(super.getCurrentUser().getId());
				record.setStatus(0);
				settleChannelCheckService.saveOrUpdate(record,0);
				// 插入审核日志
				SettleChannelCheckLogDto recordLog = new SettleChannelCheckLogDto();
				recordLog.setTmcId(super.getTmcId());
				recordLog.setChannelId(Long.parseLong(entry.getKey()));
				recordLog.setCheckedDate(DateTool.strToDateLong(day+" 12:00:00"));
				recordLog.setCreateDate(new Date());
				recordLog.setCreateUser(super.getCurrentUser().getId());
				recordLog.setStatus(0);
				settleChannelCheckLogService.save(recordLog);
			}
			
		}
		return new ResponseInfo("1", "核对完成!");
	}
	
	
	/**
	 * 取消数据审核记录
	 * @param model
	 * @param channelCheckDataForm
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/cancelCheckData")
	public ResponseInfo cancelCheckData(Model model,ChannelCheckDataForm channelCheckDataForm){
		
		Map<String,List<String>> itemsMap= transCheckItems(channelCheckDataForm.getCheckItems());
		
		//返回对象
		ChannelCheckDataForm checkDataForm = new  ChannelCheckDataForm();
		//检查审核条件
		for (Map.Entry<String,List<String>> entry : itemsMap.entrySet()) {
			List<String> dateList =  entry.getValue();
			//实现排序方法  
			Collections.sort(dateList, new Comparator() {  
				@Override  
				public int compare(Object o1, Object o2) {  
					String str1=(String) o1;  
					String str2=(String) o2;  
					if (str1.compareToIgnoreCase(str2)<0){    
						return -1;    
					}    
					return 1;    
				}  
			});
			//所选通道，日期的前一天是否为已审核
			ChannelCheckCondition queryDto= new ChannelCheckCondition();
			//所选日期的后一天
			queryDto.setEndDate(DateTool.strToDateLong(DateTool.nextDayStr(dateList.get(dateList.size()-1))+" 00:00:00"));
			queryDto.setBeginDate(DateTool.strToDateLong(DateTool.nextDayStr(dateList.get(dateList.size()-1))+" 23:59:59"));
			queryDto.setChannelId(Long.parseLong(entry.getKey()));
		    queryDto.setTmcId(super.getTmcId());
		    queryDto.setStatus(0);
		    List<SettleChannelCheckDto> list = settleChannelCheckService.querySettleChannelCheck(queryDto);
		    if(list != null && list.size() > 0){ //存在已核对的记录
		    	return new ResponseInfo("-1", "所选日期之后存在已核对记录，无法取消核对!");
		    }else{
		    	//判断时间是否为连续的
				for(int i=0 ;i<dateList.size()-1;i++){
					if(!DateTool.nextDayStr(dateList.get(i)).equals(dateList.get(i+1))){
						return new ResponseInfo("-1", "所选的收支通道日期为非连续的，无法取消核对!");
					}
				}
		    }
		}
		//条件符合要求，进行取消审核操作
		for (Map.Entry<String,List<String>> entry : itemsMap.entrySet()) {
			List<String> dateList =  entry.getValue();
			for(String day:dateList){
				// 未审核插入取消记录，已审核记录更新为取消状态
				SettleChannelCheckDto record = new SettleChannelCheckDto();
				record.setTmcId(super.getTmcId());
				record.setChannelId(Long.parseLong(entry.getKey()));
				record.setCheckedDate(DateTool.strToDateLong(day+" 12:00:00"));
				record.setCreateDate(new Date());
				record.setCreateUser(super.getCurrentUser().getId());
				record.setUpdateDate(new Date());
				record.setUpdateUser(super.getCurrentUser().getId());
				record.setStatus(1);
				settleChannelCheckService.saveOrUpdate(record,1);
				// 插入审核日志
				SettleChannelCheckLogDto recordLog = new SettleChannelCheckLogDto();
				recordLog.setTmcId(super.getTmcId());
				recordLog.setChannelId(Long.parseLong(entry.getKey()));
				recordLog.setCheckedDate(DateTool.strToDateLong(day+" 12:00:00"));
				recordLog.setCreateDate(new Date());
				recordLog.setCreateUser(super.getCurrentUser().getId());
				recordLog.setStatus(1);
				settleChannelCheckLogService.save(recordLog);
			}
			
		}
		
		return new ResponseInfo("1", "取消完成!");
	}
	
	private Map<String, List<String>> transCheckItems(String checkItems) {
		Map<String,List<String>> itemsMap = new HashMap<>();
		String[] itemList = checkItems.split(",");
		for(String item : itemList){
			String[] it = item.split("\\|");
			if(it.length==2){
				if (itemsMap.containsKey(it[0])) {
					itemsMap.get(it[0]).add(it[1]);
				} else {
					List<String> list = new ArrayList<String>();
					list.add(it[1]);
					itemsMap.put(it[0],list);
				}
			}
		}
		return itemsMap;
	}
	/**
	 * 
	 * @param channelDetailMap
	 * @param channelCheckMap
	 * @param settleChannelMap
	 * @param dateResult
	 * @param status 审核记录条件检索
	 * @return
	 */
	private List<SettleChannelCheckShowForm> getSettleChannelCheckShowForm(
			Map<Long, List<SettleChannelDetailDto>> channelDetailMap,
			Map<Long, List<SettleChannelCheckDto>> channelCheckMap, Map<Long, String> settleChannelMap, List<Date> dateResult, String status) {
		
		List<SettleChannelCheckShowForm> list = new ArrayList<SettleChannelCheckShowForm>();
		
		
		SettleChannelCheckShowForm totalForm = new SettleChannelCheckShowForm();
		totalForm.setTitleType("合计");
		totalForm.setTitleFlag(true);
		for (Map.Entry<Long,List<SettleChannelDetailDto>> entry : channelDetailMap.entrySet()) {
			List<SettleChannelDetailDto> detailDtos =  entry.getValue();
			List<SettleChannelCheckDto> checkDtos = channelCheckMap.get(entry.getKey());
			
			SettleChannelCheckShowForm totalInner = new SettleChannelCheckShowForm();
			totalInner.setTitleFlag(true);
			totalInner.setTitle(settleChannelMap.get(entry.getKey()));
			totalInner.setChannelId(entry.getKey());
			totalInner.setTitleType("小计");
			//设置时间
			totalInner.setCheckedBeginDate(DateUtils.format(dateResult.get(0), DateUtils.YYYY_MM_DD));
			totalInner.setCheckedEndDate(DateUtils.format(dateResult.get(dateResult.size()-1), DateUtils.YYYY_MM_DD));
			
			boolean isCheck = true;
			
			List<SettleChannelCheckShowForm> innerList = new ArrayList<SettleChannelCheckShowForm>();
			
			for(int i=0 ; i < dateResult.size()-1 ; i++){
				SettleChannelCheckShowForm inner = new SettleChannelCheckShowForm();
				inner.setTitleFlag(false);
				//日期字符串
				inner.setTitle(DateUtils.format(dateResult.get(i), DateUtils.YYYY_MM_DD));
				inner.setCheckedBeginDate(DateUtils.format(dateResult.get(i), DateUtils.YYYY_MM_DD));
				inner.setCheckedEndDate(DateUtils.format(dateResult.get(i), DateUtils.YYYY_MM_DD));
				//所属通道id
				inner.setChannelId(entry.getKey());
				
				//收支明细记录
				for(SettleChannelDetailDto dto : detailDtos){
					if( dateResult.get(i).getTime()<=dto.getTradeTime().getTime()&& dto.getTradeTime().getTime()<dateResult.get(i+1).getTime()){
						if(dto.getAmount()>=0){
							//收入
							inner.addIncomeMoney(new BigDecimal(dto.getAmount()).divide(new BigDecimal(100)));
							inner.addIncomeCount(1);
						}else{
							//支出
							inner.addDisburseMoney(new BigDecimal(dto.getAmount()).divide(new BigDecimal(100)));
							inner.addDisburseCount(1);
						}
						//合计
						inner.addTotalMoney(new BigDecimal(dto.getAmount()).divide(new BigDecimal(100)));
						inner.addTotalCount(1);
						
					}
				}
				//收支明细审核记录
				if(checkDtos != null){
					for(SettleChannelCheckDto checkDto : checkDtos){
						if( dateResult.get(i).getTime()<=checkDto.getCheckedDate().getTime() && checkDto.getCheckedDate().getTime()<dateResult.get(i+1).getTime()){
							if(checkDto.getStatus().equals(0)){
								inner.setStatusName("已核");
							}else if(checkDto.getStatus().equals(1)){
								isCheck = false;
								//已取消，默认显示“未核”
								//inner.setStatusName("取消");
							}
							inner.setUpdateDate(checkDto.getUpdateDate());
							inner.setUpdateUser(userService.getFullName(checkDto.getCreateUser()));
							break;
						}
					}
				}
				
				//未找到审核记录
				if(StringUtils.isEmpty(inner.getStatusName())){
					isCheck = false;
					inner.setStatusName("未核");
				}
				
				if("".equals(status)){//检索全部记录
					//通道收入
					totalInner.addIncomeMoney(inner.getIncomeMoney());
					totalInner.addIncomeCount(inner.getIncomeCount());
					//通道支出
					totalInner.addDisburseMoney(inner.getDisburseMoney());
					totalInner.addDisburseCount(inner.getDisburseCount());
					//通道合计
					totalInner.addTotalMoney(inner.getTotalMoney());
					totalInner.addTotalCount(inner.getTotalCount());
					
					innerList.add(inner);
				}else if(status.equals("-1") && "未核".equals(inner.getStatusName())){//检索未核记录
					//通道收入
					totalInner.addIncomeMoney(inner.getIncomeMoney());
					totalInner.addIncomeCount(inner.getIncomeCount());
					//通道支出
					totalInner.addDisburseMoney(inner.getDisburseMoney());
					totalInner.addDisburseCount(inner.getDisburseCount());
					//通道合计
					totalInner.addTotalMoney(inner.getTotalMoney());
					totalInner.addTotalCount(inner.getTotalCount());
					
					innerList.add(inner);
				}else if(status.equals("0") && "已核".equals(inner.getStatusName())){//检索已核记录
					//通道收入
					totalInner.addIncomeMoney(inner.getIncomeMoney());
					totalInner.addIncomeCount(inner.getIncomeCount());
					//通道支出
					totalInner.addDisburseMoney(inner.getDisburseMoney());
					totalInner.addDisburseCount(inner.getDisburseCount());
					//通道合计
					totalInner.addTotalMoney(inner.getTotalMoney());
					totalInner.addTotalCount(inner.getTotalCount());
					
					innerList.add(inner);
				}
				
			}
			
			if(isCheck){
				//该通道的收支记录全部已核
				totalInner.setStatusName("已核");
			}
			
			
			list.add(totalInner);
			
			//合计收入
			totalForm.addIncomeMoney(totalInner.getIncomeMoney());
			totalForm.addIncomeCount(totalInner.getIncomeCount());
			//合计支出
			totalForm.addDisburseMoney(totalInner.getDisburseMoney());
			totalForm.addDisburseCount(totalInner.getDisburseCount());
			//合计合计
			totalForm.addTotalMoney(totalInner.getTotalMoney());
			totalForm.addTotalCount(totalInner.getTotalCount());
			
			list.addAll(innerList);
			
        }
		
		list.add(totalForm);
		return list;
	}
	
	/**
	 * 审核明细记录
	 * @param model
	 * @return
	 */
	@RequestMapping(value="getChannelDetail")
	public String getChannelCheck(Model model){
		
		String channelId = super.getStringValue("channelId");
		String checkedBeginDate = super.getStringValue("checkedBeginDate");
		String checkedEndDate = super.getStringValue("checkedEndDate");
		
		
		ChannelCheckLogQueryForm query = new ChannelCheckLogQueryForm();
		query.setBeginDate(checkedBeginDate);
		query.setEndDate(checkedEndDate);
		query.setSettleChannel(channelId);
		
        model.addAttribute("query",query);
        
		return "supplier/settleChannelDetailPage.base";
	}
	
	/**
	 * 审核明细记录
	 * @param model
	 * @return
	 */
	@RequestMapping(value="getChannelDetailPage")
	public String getChannelCheckPage(Model model,Integer pageIndex, Integer pageSize,ChannelDetailQueryForm condition){
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = 10;
		}
	
		//当前TMC下收支通道的收支明细
		condition.setTmcId(super.getTmcId());
		
		Page<SettleChannelDetailShowForm> pageList = settleChannelDetailController.getChannelDetailList(pageIndex,pageSize, condition);
		
		int totalCount = pageList.getTotal();
		
		Page<SettleChannelDetailShowForm> pageFormList = new Page<>(pageList.getStart(),pageList.getSize(),
				pageList.getList(), totalCount);
		
		model.addAttribute("pageList",pageFormList);
		
		return "supplier/settleChannelDetailPageList.jsp";
	}
	
	
	
	
	/**
	 * 日志明细记录
	 * @param model
	 * @return
	 */
	@RequestMapping(value="getChannelCheckLog")
	public String getChannelCheckLog(Model model){
		String channelId = super.getStringValue("channelId");
		String checkedBeginDate = super.getStringValue("checkedBeginDate");
		String checkedEndDate = super.getStringValue("checkedEndDate");
		
		
		ChannelCheckLogQueryForm query = new ChannelCheckLogQueryForm();
		query.setBeginDate(checkedBeginDate);
		query.setEndDate(checkedEndDate);
		query.setSettleChannel(channelId);
		
        model.addAttribute("query",query);
        
		return "supplier/settleChannelCheckLogList.base";
	}
	/**
	 * 日志明细记录
	 * @param model
	 * @return
	 */
	@RequestMapping(value="getChannelCheckLogPage")
	public String getChannelCheckLogPage(Model model,Integer pageIndex, Integer pageSize,ChannelCheckLogQueryForm condition){
		
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = 10;
		}
		
		condition.setTmcId(super.getTmcId());
		
		Page<SettleChannelCheckLogShowForm> pageList = this.getChannelCheckLogList(pageIndex,pageSize, condition);
		int totalCount = pageList.getTotal();
		
		Page<SettleChannelCheckLogShowForm> pageFormList = new Page<>(pageList.getStart(),pageList.getSize(),
				pageList.getList(), totalCount);
		
		model.addAttribute("pageList",pageFormList);
		
		return "supplier/settleChannelCheckLogPageList.jsp";
	}
	
	
	public Page<SettleChannelCheckLogShowForm> getChannelCheckLogList(Integer pageIndex, Integer pageSize, ChannelCheckLogQueryForm condition) {
		//查询对象
        QueryDto<ChannelCheckLogCondition> queryDto = new QueryDto<>(pageIndex, pageSize);

        queryDto.setCondition(condition.toChannelCheckLogCondition());

        //收支通道
        List<SettleChannelDto> settleChannels= settleChannelService.findSettleChannelByTmcId(super.getTmcId(),null);
        Map<Long,String> settleChannelMap = new HashMap<Long,String>();
        for(SettleChannelDto dto:settleChannels){
        	settleChannelMap.put(dto.getId(), dto.getName());
        }
        //获取页面数据
        Page<SettleChannelCheckLogDto> pageList = settleChannelCheckLogService.queryListWithPage(queryDto);
        List<SettleChannelCheckLogShowForm> settleChannelDetailList = transSettleChannelDetailForm(pageList.getList(),settleChannelMap);

        return new Page<SettleChannelCheckLogShowForm>(pageList.getStart(),pageList.getSize(),settleChannelDetailList,pageList.getTotal());
        
	}
	
	public List<SettleChannelCheckLogShowForm> transSettleChannelDetailForm(List<SettleChannelCheckLogDto> list,
			Map<Long, String> settleChannelMap) {
		
		List<SettleChannelCheckLogShowForm> channelCheckLogFormSet = new ArrayList<SettleChannelCheckLogShowForm>();

		for(SettleChannelCheckLogDto dto : list){
			SettleChannelCheckLogShowForm settleChannelCheckLogShowForm= new SettleChannelCheckLogShowForm();
			settleChannelCheckLogShowForm.setId(dto.getId());
			settleChannelCheckLogShowForm.setCreateUserName(userService.getFullName(dto.getCreateUser()));
			settleChannelCheckLogShowForm.setCheckedDate(dto.getCheckedDate());
			settleChannelCheckLogShowForm.setCreateDate(dto.getCreateDate());
			settleChannelCheckLogShowForm.setChannelName(settleChannelMap.get(dto.getChannelId()));
			if(dto.getStatus()==0){
				settleChannelCheckLogShowForm.setStatusName("完成核对");
			}else{
				settleChannelCheckLogShowForm.setStatusName("取消核对");
			}
			channelCheckLogFormSet.add(settleChannelCheckLogShowForm);
		}
		return channelCheckLogFormSet;
	}
}
