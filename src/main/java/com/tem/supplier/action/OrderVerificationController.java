package com.tem.supplier.action;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.tem.flight.api.order.FlightOrderService;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.platform.action.BaseController;
import com.tem.supplier.form.FlightOrderContextForm;
import com.tem.supplier.util.DateTool;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
/**
 *
 * @author chf
 * @date 2018/4/23
 */
@Controller
@RequestMapping("supplier")
public class OrderVerificationController extends BaseController {

    @Autowired
    private FlightOrderService flightOrderService;
   
    @RequestMapping("/flightOrderVerify")
    public String flightOrderVerification(Model model){
    	
        return "supplier/flightOrderVerify.base";
    }
   
    @RequestMapping(value="/upload",method=RequestMethod.POST,produces="text/html;charset=UTF-8")
    @ResponseBody
    public String upload(HttpServletRequest request,
           @RequestParam("file") MultipartFile file) throws Exception {
    	
       if(!file.isEmpty()) {
    	   String fileName = file.getOriginalFilename();
    	   String str = getExtensionName(fileName);
    	   //判断文件后缀名
    	   if("xls".equals(str)){ /*jxl仅支持.xls文件的解析与操作*/    /*|| str.equals("xlsx")*/
    		 //上传文件路径
               String path = request.getSession().getServletContext().getRealPath("/resource/verify/");
               //上传文件名
               
               File filepath = new File(path,fileName);
               //判断路径是否存在，如果不存在就创建一个
               if (!filepath.getParentFile().exists()) { 
                   filepath.getParentFile().mkdirs();
               }else{
            	   //判断文件是否存在，已存在就删除
                   File virtualFile = new File(path + File.separator + fileName);
                   if(!virtualFile.exists()){
                	   virtualFile.delete();
                   }  
               }
               //传入最新文件
               file.transferTo(new File(path + File.separator + fileName));
            
               return fileName;
    	   }
       } 
       
       return null;
    }
    
    @RequestMapping(value="/download",produces="text/html;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<byte[]> download(HttpServletRequest request,
            @RequestParam("filename") String filename,
            Model model)throws Exception {
       //下载文件路径
       String path = request.getSession().getServletContext().getRealPath("/resource/verify/");
       File file = new File(path + File.separator + filename);
       
       //审核FILE内容
       File verifiedFile = doFlightOrderVerify(file);
       
       HttpHeaders headers = new HttpHeaders();  
       String downloadFielName = new String(filename.getBytes("UTF-8"),"iso-8859-1");
       //通知浏览器以attachment（下载方式）
       headers.setContentDispositionFormData("attachment", downloadFielName); 
       //application/octet-stream ： 二进制流数据（最常见的文件下载）。
       headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
       ResponseEntity<byte[]> responseEntity = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(verifiedFile),    
               headers, HttpStatus.CREATED);
       
       
       //下载文件完成后，删除文件
       if (file.exists()) {  
    	   file.delete();
        }
       return responseEntity;  
    }
    
    /**
     * 审核文件，校验不夜城订单内容
     * @param file
     * @return
     */
    public  File doFlightOrderVerify(File file) {
    	
    	try {
            // 创建输入流，读取Excel
            InputStream is = new FileInputStream(file.getAbsolutePath());
            // jxl提供的Workbook类
            Workbook   wb = Workbook.getWorkbook(is);
            
           //打开一个文件的副本，并且指定数据写回到原文件
            WritableWorkbook wbook = Workbook.createWorkbook(file,wb);
            
            // Excel的页签数量
            int sheet_size = wbook.getNumberOfSheets();
            for (int index = 0; index < sheet_size; index++) {
                // 获取提取的excel,每个页签创建一个Sheet对象
            	WritableSheet  sheet =  wbook.getSheet(index);
            	//初始本工作页，行数
            	int rowIndex = sheet.getRows();
            	//初始本工作页，列数
            	int columnIndex = sheet.getColumns();
            	
            	
            	//在excel文件列尾，添加两列数据
            	WritableCell cellLastOne = new Label(sheet.getColumns(),0,"核对标记");
            	WritableCell cellLastTwo = new Label(sheet.getColumns()+1,0,"核对结果");
               //将单元格添加到sheet中,sheet.getColumns() 已经+2
               sheet.addCell(cellLastOne);
               sheet.addCell(cellLastTwo);
            	
                // sheet.getRows()返回该页的总行数
                for (int i = 1; i < rowIndex; i++) {
                	sheet.getRows();
                	//RowView RowView = sheet.getRowView(i);
                	FlightOrderContextForm flightOrderContext = new FlightOrderContextForm();
                	
                    // sheet.getColumns()返回该页的总列数
                    for (int j = 0; j < columnIndex; j++) {
                    	
                        String cellinfo = sheet.getCell(j, i).getContents().trim();

                        switch (j) {
                            case 0 :
                            	//业务类型
                            	flightOrderContext.setBusinessType(cellinfo);
                                break;
                            case 1 :
                                //交易订单号
                            	flightOrderContext.setOrderId(cellinfo);
                                break;
                            case 2 :
                                //交易日期
                            	flightOrderContext.setIssueTicketTime("".equals(cellinfo) ? null:DateTool.strToDateLong(cellinfo));
                                break;
                            case 3 :
                                //订单金额
                            	flightOrderContext.setOrderAmount("".equals(cellinfo.replace(",", "")) ? null:new BigDecimal(cellinfo.replace(",","")));
                                break;
                            case 4 :
                            	//交易金额
                            	flightOrderContext.setTransactionAmount("".equals(cellinfo.replace(",", "")) ? null:new BigDecimal(cellinfo.replace(",","")));
                                break;
                            case 5 :
                            	//支付机构
                            	flightOrderContext.setPaymentAgent(cellinfo);
                                break;
                            case 6 :
                            	//交易类型
                            	flightOrderContext.setTransactionType(cellinfo);
                                break;
                            case 7 :
                            	//支付方式
                            	flightOrderContext.setPaymentMethod(cellinfo);
                                break;
                            case 8 :
                            	//支付状态
                            	flightOrderContext.setPaymentStatus(cellinfo);
                                break;
                            default:
                                break;
                        }
                    }
                    /**
                     * 根据不夜城的导出订单excel文件，检查系统中相应的订单数据是否存在且正确。
						逻辑如下：
						1、以不夜城的订单号作为我们系统的供应商订单号条件查询机票订单，如若查询不到订单则标红此条数据。
						2、检查不夜城的订单时间与我们系统订单的出票时间是否一致，时间允许偏差2小时。
						3、检查不夜城的订单金额与我们的订单供应商应付金额是否一致。
						4、检查不夜城订单的支付状态与我们系统的支付状态是否一致。
						凡是不一致的数据，把excel当条数据标红色，并备注不一致原因。
                     */
                    
                    //根据订单号检索订单信息
                    FlightOrderDto flightOrderDto = flightOrderService.getFlightOrderById(flightOrderContext.getOrderId());
                    
                    //审核结果集合
                    ArrayList<String> resultList = new ArrayList<String>();
                    if(flightOrderDto==null){
                    	resultList.add("该订单系统中不存在；");
                    }else{
                    	//出票时间 时间允许偏差2小时
                    	if(flightOrderDto.getIssueTicketTime() != null && flightOrderContext.getIssueTicketTime() != null){
                    		long d = DateTool.dateInterval(flightOrderContext.getIssueTicketTime(), flightOrderDto.getIssueTicketTime());
                    		if(d<-2 || d>2){
                    			resultList.add("不夜城的出票时间与系统中订单的出票时间不一致；");
                    		}
                    	}else{
                    		if(!(flightOrderDto.getIssueTicketTime() == null && flightOrderContext.getIssueTicketTime() == null)){
                    			resultList.add("不夜城的订单时间与系统中订单的出票时间不一致；");
                    		}
                    	}
                    	//订单金额校验
                    	if(flightOrderDto.getAmount() != null && flightOrderContext.getTransactionAmount() != null){
                    		
                        	if(flightOrderDto.getAmount().compareTo(flightOrderContext.getTransactionAmount())!=0){
                        		resultList.add("不夜城的订单金额与系统的订单供应商应付金额不一致；");
                        	}
                    	}else{
                    		if(!(flightOrderDto.getAmount() == null && flightOrderContext.getTransactionAmount() == null)){
                    			resultList.add("不夜城的订单金额与系统的订单供应商应付金额不一致；");
                    		}
                    	}
                    	
                    	//支付状态校验    系统中存储的是枚举编码
                    	if(!stringMatch(flightOrderDto.getPaymentStatus().getMessage(),flightOrderContext.getPaymentStatus())){
                    		resultList.add("不夜城订单的支付状态与系统的订单支付状态不一致；");
                    	}
                    	
                    }
                    if(resultList.size()>0){
                    	//添加 核对标记
                    	WritableCell markCell = new Label(columnIndex,i,"");
                    	 //创建一个单元格样式
                        WritableCellFormat writableCellFormat = new WritableCellFormat();
                        //设置样式的背景色为红色
                        writableCellFormat.setBackground(Colour.RED);
                        //设置单元格的样式
                        markCell.setCellFormat(writableCellFormat);
                        //将单元格添加到sheet中
                        sheet.addCell(markCell);
                        //添加 核对结果
                    	WritableCell resultCell = new Label(columnIndex+1,i,resultListToContext(resultList));
                        //添加 核对结果
                        sheet.addCell(resultCell);
                    }
                    
                }
                //写入修改
                wbook.write();
                wbook.close();
                wb.close();
                is.close();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
		}finally{
			
		}
		return file;
	}
    /**
     * 订单问题拼接
     * @param resultList
     * @return
     */
    private String resultListToContext(List<String> resultList) {
    	String result="";
		for(int i=0;i<resultList.size();i++){
			int index = i+1;
			result = result + index+". "+resultList.get(i);
		}
		return result;
	}
    /**
     * Java文件操作 获取文件扩展名
     * @param filename
     * @return
     */
    public static String getExtensionName(String filename) { 
        if ((filename != null) && (filename.length() > 0)) { 
            int dot = filename.lastIndexOf('.'); 
            if ((dot >-1) && (dot < (filename.length() - 1))) { 
                return filename.substring(dot + 1); 
            } 
        } 
        return filename; 
    } 
    /**
     * 字符串含义匹配 
     * @param str1  不夜城订单
     * @param str2   系统
     * @return
     */
    public boolean stringMatch(String str1,String str2){
    	if(("成功".equals(str1) || "已支付".equals(str1)) &&
                "已支付".equals(str2)){
    		return true;
    	}
    	
    	if(("未支付".equals(str1)) &&
    			("未支付".equals(str2) || "待支付审批".equals(str2))){
    		return true;
    	}
    	
    	if(("失败".equals(str1)) &&
    			("支付审批驳回".equals(str2) || "失败".equals(str2))){
    		return true;
    	}
    	
    	return false;
    }
    
	/**
     * 核对订单的错误地方
     * @param model
     * @return
     */
    public Map<String, String> getVerifyResultMap(Model model) {
    	Map<String, String> map = new HashMap<>();
        // TODO 初始化员工违规类型,假数据 ,已注释
		map.put("001", "该订单系统中不存在；");
		map.put("002", "不夜城的出票时间与系统中订单的出票时间不一致；");
		map.put("003", "系统中该订单无出票时间；");
		map.put("004", "不夜城的订单金额与系统的订单供应商应付金额不一致；");
		map.put("005", "不夜城订单的支付状态与系统的订单支付状态不一致；");
        return map;
    }
    /*
     * 机票支付枚举对象
     * package com.tem.flight.dto.order.enums;
		public enum PaymentStatus
		{
		  NOT_PAYMENT("未支付"),  WAIT_AUTHORIZE("待支付审批"),  AUTHORIZE_RETURN("支付审批驳回"),  PAYMENT_SUCCESS("已支付");
		  private String message;
		  public String getMessage()
		  {
		    return this.message;
		  }
		  private PaymentStatus(String message)
		  {
		    this.message = message;
		  }
		}*/
}
