package com.tem.supplier.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.DateUtils;
import com.tem.supplier.api.SettleChannelCheckService;
import com.tem.supplier.api.SettleChannelDetailService;
import com.tem.supplier.api.SettleChannelService;
import com.tem.supplier.condition.ChannelCheckCondition;
import com.tem.supplier.condition.ChannelDetailCondition;
import com.tem.supplier.dto.SettleChannelCheckDto;
import com.tem.supplier.dto.SettleChannelDetailDto;
import com.tem.supplier.dto.SettleChannelDto;
import com.tem.supplier.enums.ChannelDetailTradeType;
import com.tem.order.action.RefundOrderListController;
import com.tem.order.service.PartnerOrderListService;
import com.tem.order.util.ExportInfo;
import com.tem.platform.action.BaseController;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.supplier.form.ChannelDetailDataForm;
import com.tem.supplier.form.ChannelDetailForm;
import com.tem.supplier.form.ChannelDetailQueryForm;
import com.tem.supplier.form.SettleChannelDetailShowForm;
import com.tem.supplier.util.DateTool;

/**
 * 收支通道收支明细
 * @author chenghaofan 
 *
 */
@Controller
@RequestMapping("supplier/channelDetail")
public class SettleChannelDetailController extends BaseController {

	@Autowired
	private SettleChannelDetailService settleChannelDetailService;
	
	@Autowired
	private SettleChannelService settleChannelService;
	
	@Autowired
	private SettleChannelCheckService settleChannelCheckService;
	
	@Autowired
    private IssueChannelService issueChannelService;
	
	@Autowired
    private PartnerOrderListService orderListService;
	
	@Autowired
	private RefundOrderListController refundOrderListController;
	 
	private final Logger logger = LoggerFactory.getLogger(SettleChannelDetailController.class);

	@RequestMapping("/detail")
	public String channelDetail(Model model,String state){
		
		//交易类型
		ChannelDetailTradeType[] tradeTypes = ChannelDetailTradeType.values();
        model.addAttribute("tradeTypes",tradeTypes);

        //供应商
        List<IssueChannelDto> issueChannels = issueChannelService.findListByTmcId(super.getTmcId());
        model.addAttribute("issueChannels",issueChannels);
        
        //收支通道
        List<SettleChannelDto> settleChannels= settleChannelService.findSettleChannelByTmcId(super.getTmcId(),0);
        model.addAttribute("settleChannels",settleChannels);
        
		return "supplier/settleChannelDetail.base";
	}
	
	@RequestMapping("/toAddDetail")
	public String toAddDetail(Model model){
		
		//供应商
		List<IssueChannelDto> issueChannels = issueChannelService.findListByTmcId(super.getTmcId());
		model.addAttribute("issueChannels",issueChannels);
		
		//收支通道
		List<SettleChannelDto> settleChannels= settleChannelService.findSettleChannelByTmcId(super.getTmcId(),0);
		model.addAttribute("settleChannels",settleChannels);
		
		return "supplier/addSettleChannelDetail.jsp";
	}
	
	/**
	 * 新增调整记录，先确认该通道是否已审核
	 * @param model
	 * @return
	 */
	@RequestMapping("/addDetailCheck")
	@ResponseBody
	public ResponseInfo addDetailCheck(Model model){
		String checkedDate = super.getStringValue("tradeTime");
		String channelId = super.getStringValue("settleChannelId");
		if(StringUtils.isEmpty(checkedDate)){
			return new ResponseInfo("-1", "请选择入账日期！");
		}
		if(StringUtils.isEmpty(channelId)){
			return new ResponseInfo("-1", "请选择收支通道！");
		}
		ChannelCheckCondition queryDto= new ChannelCheckCondition();
		
		queryDto.setEndDate(DateTool.strToDateLong(DateTool.nextDayStr(checkedDate+" 00:00:00")));
		queryDto.setBeginDate(DateTool.strToDateLong(DateTool.nextDayStr(checkedDate+" 23:59:59")));
		queryDto.setChannelId(Long.parseLong(channelId));
	    queryDto.setTmcId(super.getTmcId());
	    queryDto.setStatus(0);
	    List<SettleChannelCheckDto> list = settleChannelCheckService.querySettleChannelCheck(queryDto);
		
	    if(list != null && list.size() > 0){ //存在已核对的记录
	    	return new ResponseInfo("-1", checkedDate+ChannelDetailTradeType.valueOf(channelId).getValue()+"已核对，无法新增记录！");
	    }else{
	    	return new ResponseInfo("1", "可以添加调整");
	    }
		
	}
	
	@ResponseBody
	@RequestMapping("/save")
	public ResponseInfo save(ChannelDetailForm channelDetailForm){

		//当前tmc
		channelDetailForm.setTmcId(super.getTmcId());
		//新增，默认交易类型：调整
		channelDetailForm.setTradeType(ChannelDetailTradeType.ADJUST.name());
		//当前用户
		channelDetailForm.setCreateUser(super.getCurrentUser().getId());
		channelDetailForm.setCreateDate(new Date());
		
		SettleChannelDetailDto record = transSettleChannelDetailDto(channelDetailForm);
		SettleChannelDetailDto isDto = settleChannelDetailService.save(record);

		if(isDto.getId() != null){
			return new ResponseInfo("0","保存成功");
		}else{
			return new ResponseInfo("1", "保存失败");
		}
	}
	/**
	 * 
	 * @param form
	 * @return
	 */
	private SettleChannelDetailDto transSettleChannelDetailDto(ChannelDetailForm form) {
		SettleChannelDetailDto dto = new SettleChannelDetailDto();
		dto.setChannelId(Long.valueOf(form.getSettleChannelId()));
		dto.setSupplyId(Long.valueOf(form.getIssueChannelId()));
		dto.setDescr(form.getDescr());
		dto.setAmount((int) (Double.valueOf(form.getAmount())*100));
		dto.setTradeTime(DateUtils.parse(form.getTradeTime()+" 00:00:00", DateUtils.YYYY_MM_DD_HH_MM_SS));
		dto.setSupplyRefRecord(form.getSupplyRefRecord());
		dto.setTmcId(form.getTmcId());
		dto.setTradeType(form.getTradeType());
		dto.setCreateUser(form.getCreateUser());
		dto.setCreateDate(form.getCreateDate());
		return dto;
	}
	@RequestMapping("/detailList")
	public String channelDetailList(Model model,Integer pageIndex, Integer pageSize,ChannelDetailQueryForm condition){
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = 100;
		}

		//当前TMC下收支通道的收支明细
		condition.setTmcId(super.getTmcId());
		
		
		Page<SettleChannelDetailShowForm> pageList = this.getChannelDetailList(pageIndex,pageSize, condition);
		
        int totalCount = pageList.getTotal();

        Page<SettleChannelDetailShowForm> pageFormList = new Page<>(pageList.getStart(),pageList.getSize(),
        		pageList.getList(), totalCount);

        model.addAttribute("pageList",pageFormList);

        model.addAttribute("currentUserId", super.getCurrentUser().getId());
        
		return "supplier/settleChannelDetailList.jsp";
	}
	
	@RequestMapping("/detailData")
	public String channelDetailData(Model model,ChannelDetailQueryForm condition){
		
		//当前TMC下收支通道的收支明细
		condition.setTmcId(super.getTmcId());
		
		ChannelDetailDataForm dataFrom = this.getChannelDetailData(condition);
		
		model.addAttribute("dataFrom", dataFrom);
		
		return "supplier/settleChannelDetailData.jsp";
	}
	
	
	//导出
	@RequestMapping("/exportSettleChannelDetail")
	public void exportSettleChannelDetail(Model model,ChannelDetailQueryForm condition){
		List<ExportInfo> infoList = new ArrayList<>();

        //所属tmc的权限，
		condition.setTmcId(super.getTmcId());
		
		//获取检索数据
		List<SettleChannelDetailShowForm> settleChannelDetailSet = this.getChannelDetailList(1,Integer.MAX_VALUE, condition).getList();
        
        String[] dataHeader = {"序号", "入账时间","所属收支通道","交易类型","收支方向","交易金额","供应商名称"
                ,"供应商订单号/票号","TEM订单号/账单号","收支通道流水号","描述"};

        ExportInfo<SettleChannelDetailShowForm> settleChannelDetailExport = new ExportInfo<>();
        settleChannelDetailExport.setDataset(settleChannelDetailSet);
        settleChannelDetailExport.setHeaders(dataHeader);
        settleChannelDetailExport.setTitle("收支明细");
        infoList.add(settleChannelDetailExport);

        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String fileName = "收支明细" + uuid + ".xlsx";

        refundOrderListController.goToExport(infoList,fileName,getRequest(),getResponse());
	}

	/**
     * 收支明细
     * @param pageIndex
     * @param pageSize
     * @param condition
     * @return
     */
    public Page<SettleChannelDetailShowForm> getChannelDetailList(Integer pageIndex, Integer pageSize, ChannelDetailQueryForm condition){
        //查询对象
        QueryDto<ChannelDetailCondition> queryDto = new QueryDto<>(pageIndex, pageSize);

        queryDto.setCondition(condition.toChannelDetailCondition());

        //供应商
        List<IssueChannelDto> issueChannels = issueChannelService.findListByTmcId(super.getTmcId());
        Map<Long,String> issueChannelMap = new HashMap<Long,String>();
        for(IssueChannelDto dto:issueChannels){
        	issueChannelMap.put(Long.valueOf(dto.getId()), dto.getName());
        }
        //收支通道
        List<SettleChannelDto> settleChannels= settleChannelService.findSettleChannelByTmcId(super.getTmcId(),null);
        Map<Long,String> settleChannelMap = new HashMap<Long,String>();
        for(SettleChannelDto dto:settleChannels){
        	settleChannelMap.put(dto.getId(), dto.getName());
        }
        //获取页面数据
        Page<SettleChannelDetailDto> pageList = settleChannelDetailService.queryListWithPage(queryDto);
        List<SettleChannelDetailShowForm> settleChannelDetailList = transSettleChannelDetailForm(pageList.getList(),issueChannelMap,settleChannelMap);

        return new Page<SettleChannelDetailShowForm>(pageList.getStart(),pageList.getSize(),settleChannelDetailList,pageList.getTotal());
    }

    /**
     * 收支金额汇总
     * @param condition
     * @return
     */
    public ChannelDetailDataForm getChannelDetailData(ChannelDetailQueryForm condition){
    	//查询对象
        QueryDto<ChannelDetailCondition> queryDto = new QueryDto<>();
        queryDto.setStart(0);
        queryDto.setSize(Integer.MAX_VALUE);
        
        queryDto.setCondition(condition.toChannelDetailCondition());
    	//获取统计数据,查询符合条件的所有记录
        Page<SettleChannelDetailDto> dataList = settleChannelDetailService.queryListWithPage(queryDto);
        
    	 ChannelDetailDataForm dataFrom= new ChannelDetailDataForm();
         int incomeMoney = 0,disburseMoney = 0;
        
         for(SettleChannelDetailDto dto:dataList.getList()){
         	if(dto.getAmount() < 0){
         		disburseMoney += dto.getAmount();
         	}else{
         		incomeMoney += dto.getAmount();
         	}
         }
         dataFrom.setIncomeMoney(new BigDecimal(incomeMoney).divide(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP));
         dataFrom.setDisburseMoney(new BigDecimal(disburseMoney).divide(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP));
         dataFrom.setTotalMoney(new BigDecimal(incomeMoney+disburseMoney).divide(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP));
         return dataFrom;
    }
    /**
     * 
     * @param settleChannelDetailList
     * @param settleChannelMap 
     * @param issueChannelMap 
     * @return
     */
    public List<SettleChannelDetailShowForm> transSettleChannelDetailForm(List<SettleChannelDetailDto> settleChannelDetailList, 
			Map<Long, String> issueChannelMap, Map<Long, String> settleChannelMap){
		
		List<SettleChannelDetailShowForm> channelDetailFormSet = new ArrayList<SettleChannelDetailShowForm>();

		for(SettleChannelDetailDto dto : settleChannelDetailList){
			SettleChannelDetailShowForm settleChannelDetailForm= new SettleChannelDetailShowForm();
			
			settleChannelDetailForm.setId(dto.getId());
			settleChannelDetailForm.setTradeTime(dto.getTradeTime());
			
			settleChannelDetailForm.setChannelName(settleChannelMap.get(dto.getChannelId()));
			settleChannelDetailForm.setTradeType(ChannelDetailTradeType.valueOf(dto.getTradeType()).getValue());
			settleChannelDetailForm.setSupplyName(issueChannelMap.get(dto.getSupplyId()));
			settleChannelDetailForm.setSupplyRefRecord(dto.getSupplyRefRecord());
			settleChannelDetailForm.setTemRefRecord(dto.getTemRefRecord());
			settleChannelDetailForm.setChannelRefRecord(dto.getChannelRefRecord());
			settleChannelDetailForm.setDescr(dto.getDescr());
			if(dto.getAmount() != null){
				if(dto.getAmount() < 0){
					settleChannelDetailForm.setDirection("支出");
				}else{
					settleChannelDetailForm.setDirection("收入");
				}
				settleChannelDetailForm.setAmount(new BigDecimal(dto.getAmount()).divide(new BigDecimal(100)).setScale(2,BigDecimal.ROUND_HALF_UP));
				
			}
			channelDetailFormSet.add(settleChannelDetailForm);
		}
		return channelDetailFormSet;
	}

	

}
