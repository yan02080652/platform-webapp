package com.tem.general.action;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Config;
import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.tem.general.form.GeneralOrderForm;
import com.tem.general.form.RefundForm;
import com.tem.general.utils.GeneralConstants;
import com.tem.imgserver.client.ImgClient;
import com.tem.imgserver.client.UploadResult;
import com.tem.notify.api.SmsService;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.OrgCostcenterService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.OrgCostcenterDto;
import com.tem.platform.security.authorize.User;
import com.tem.product.api.GeneralOrderService;
import com.tem.product.api.GeneralRefundService;
import com.tem.product.api.GeneralSettleChannelService;
import com.tem.product.condition.GeneralOrderCondition;
import com.tem.product.dto.general.GeneralOrderDto;
import com.tem.product.dto.general.GeneralRefundDto;
import com.tem.product.dto.general.OrderCountDto;
import com.tem.product.dto.general.enums.OrderShowStatus;
import com.tem.product.dto.general.enums.OrderStatus;
import com.tem.product.dto.insurance.OrderOriginType;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.api.task.OrderTaskService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.refund.RefundInfoDto;
import com.tem.pss.dto.refund.RefundOrderDto;
import com.tem.pss.dto.refund.RefundStatus;
import com.tem.pss.dto.task.OrderTaskCondition;
import com.tem.pss.dto.task.OrderTaskDto;
import com.tem.pss.dto.task.TaskParamDto;
import com.tem.pss.dto.task.TaskStatus;
import com.tem.pss.dto.task.TaskType;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import com.tem.pss.enums.TravelTypeEnum;
import com.tem.sso.api.SSOService;
import com.tem.tmc.TaskCommonService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 通用订单 tmc 任务台
 * @author chenjieming
 *
 */
@Controller
@RequestMapping("general/tmc")
public class GeneralTmcController extends BaseController {

	@Autowired
	private OrderTaskService orderTaskService;
	@Autowired
	private GeneralOrderService generalOrderService;
	@Autowired
	private SmsService smsService;
	@Autowired
	private DictService dictService;
	@Autowired
	private OrgCostcenterService orgCostcenterService;
	@Autowired
	private GeneralRefundService generalRefundService;
	@Autowired
	private RefundOrderService	refundOrderService;
	@Autowired
	private TaskCommonService taskCommonService;
	@Autowired
	private IssueChannelService issueChannelService;
	@Autowired
	private SSOService ssoService;
	@Autowired
	private GeneralSettleChannelService generalSettleChannelService;
	
	/**
	 * 首页
	 * @return
	 */
	@RequestMapping("index")
	public String index() {
		return "general/tmc/index.jsp";
	}
	
	/**
	 * 我的任务 tab
	 * @return
	 */
	@RequestMapping(value = "getMyTaskTable")
	public String getMyTaskTable(OrderTaskCondition condition,Model model,Integer pageIndex){
		if(pageIndex==null){
			pageIndex=1;
		}
		if(StringUtils.isEmpty(condition.getTaskStatus())){
			condition.setTaskStatus("PROCESSING");
		}

		model.addAttribute("active",condition.getTaskStatus());

		QueryDto<OrderTaskCondition> queryDto = new QueryDto<>(pageIndex,10);
		if(super.getTmcId() != null){
			condition.setTmcId(super.getTmcId().toString());
		}

		condition.setOperatorId(super.getCurrentUser().getId().intValue());
		condition.setOrderBizType(OrderBizType.GENERAL.name());
		queryDto.setCondition(condition);
		
		Page<OrderTaskDto> pageList = generalOrderService.orderTaskPageListQuery(queryDto);
		
//		//处理中任务数量（包含各状态的数量）
		condition.setTaskStatus(TaskStatus.PROCESSING.name());
		Map<String, Integer> typeMap = orderTaskService.getTaskCountMap(condition);
//		
		//已处理 
		condition.setTaskStatus(TaskStatus.ALREADY_PROCESSED.name());
		Map<String, Integer> alreadyMap = orderTaskService.getTaskCountMap(condition);
		
//		//待处理任务数量（包含各状态的数量）
		condition.setTaskStatus(TaskStatus.WAIT_PROCESS.name());
		condition.setOperatorId(null);
		Map<String, Integer> waitTaskTypeMap = orderTaskService.getTaskCountMap(condition);
		
		model.addAttribute("pageList", pageList);
		model.addAttribute("typeMap", typeMap);
		model.addAttribute("alreadyMap", alreadyMap);
		model.addAttribute("waitTaskTypeMap", waitTaskTypeMap);
		model.addAttribute("originMap",OrderOriginType.getMap());
		taskCommonService.getTaskGroupCount(model,super.getTmcId().toString(),super.getCurrentUser().getId().intValue());
		
		return "general/tmc/myTask.jsp";
	}
	
	//领取任务
	@ResponseBody
	@RequestMapping("/receiveTask")
	public boolean receiveTask(boolean type1,boolean type2){
		User user = super.getCurrentUser();
		TaskParamDto dto = new TaskParamDto();
		dto.setUserId(user.getId().intValue());
		dto.setUserName(user.getFullname());

		if(super.getTmcId() != null){
			dto.setTmcId(super.getTmcId().toString());
		}

		List<String> list = new ArrayList<String>();
		if(type1){
			list.add(TaskType.WAIT_OFFER.toString());
		}
		if(type2){
			list.add(TaskType.WAIT_REFUND_G.toString());
		}
		dto.setTaskType(list);
		dto.setOrderBizType(OrderBizType.GENERAL.name());
		
		boolean rs = orderTaskService.receiveTask(dto);
		
		return rs;
	}
	
	//任务交接
	@ResponseBody
	@RequestMapping("/transfer")
	public boolean transfer(Integer taskId,Integer operatorId,Integer userId,String userName){
		TaskParamDto dto = new TaskParamDto();
		dto.setTaskId(taskId);
		dto.setOperatorId(operatorId == null ? getCurrentUser().getId().intValue() : operatorId);
		dto.setUserId(userId);
		dto.setUserName(userName);
		dto.setTaskStatus(TaskStatus.PROCESSING);
		dto.setRemork(super.getCurrentUser().getFullname()+"("+operatorId+")将任务交接给"+userName+"("+userId+"),任务id:"+taskId);
		dto.setOrderBizType(OrderBizType.GENERAL.name());

		if(super.getTmcId() != null){
			dto.setTmcId(super.getTmcId().toString());
		}

		boolean rs = orderTaskService.saveTaskAndLog(dto);
		
		return rs;
	}
	
	/**
	 * 后台报价详细页
	 * @param model
	 * @param orderId
	 * @param taskId
	 * @return
	 */
	@RequestMapping("/getOrderDetail")
	public String getOrderDetail(Model model,String orderId,Integer taskId,String taskType){
		
		GeneralOrderDto generalOrderDto = generalOrderService.getById(orderId);
		
		//供应商信息
		List<IssueChannelDto> issueChannelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType
				(super.getTmcId(), IssueChannelType.GENERAL.getType(), LineType.OFF_LINE.getType());
		model.addAttribute("issueChannelList", issueChannelList);

		//服务类型
		List<DictCodeDto> serviceType = dictService.getCodes(GeneralConstants.GENERAL_SERVICE_TYPE);
		List<DictCodeDto> serviceTypeList = new ArrayList<DictCodeDto>();
		for(DictCodeDto serType : serviceType){
			if(serType.getPid() != null){
				serviceTypeList.add(serType);
			}
		}
		model.addAttribute("dictCodes", serviceTypeList);
		
		Long partnerId = generalOrderDto.getcId();//预订人企业
		Long userId = generalOrderDto.getUserId();
		
		//差旅类型
		List<DictCodeDto> travelTypes = dictService.getPartnerCodes(partnerId, GeneralConstants.TM_TRAVEL_TYPE);
		model.addAttribute("travelTypes", travelTypes);
		
		//当前预订人的成本中心
		List<OrgCostcenterDto> costCenter = orgCostcenterService.findByUserId(partnerId, userId);
		model.addAttribute("costCenter", costCenter);
		
		if(generalOrderDto.getCostUnitCode() != null){
			OrgCostcenterDto byOrgIdAndCode = orgCostcenterService.getById(Long.valueOf(generalOrderDto.getCostUnitCode()));
			if(byOrgIdAndCode != null){
				model.addAttribute("costCenterName", byOrgIdAndCode.getName());
			}
		}
		
		model.addAttribute("taskId", taskId);
		model.addAttribute("generalOrderDto", generalOrderDto);
		model.addAttribute("taskType", taskType);
		
		return "general/tmc/order-model.jsp";
	}
	
	/**
	 * 通用订单报价
	 */
	@RequestMapping("/offer")
	@ResponseBody
	public boolean repair(GeneralOrderForm orderForm, HttpServletRequest request1, String taskId , MultipartFile file){
		
		GeneralOrderDto generalOrderDto = TransformUtils.transform(orderForm, GeneralOrderDto.class);
		
		if(file != null){
			UploadResult  uploadRs = uploadImg(file);
			
			generalOrderDto.setFileName(uploadRs.getMsg());
			generalOrderDto.setFileUrls(uploadRs.getUrlPath());
		}
		
		BigDecimal totalOrderPrice = BigDecimalUtils.add(generalOrderDto.getServicePrice() , generalOrderDto.getTotalSalePrice());
		BigDecimal trSpreads = BigDecimalUtils.subtract(generalOrderDto.getTotalSalePrice() , generalOrderDto.getTotalSettlePrice());
		
		generalOrderDto.setTotalOrderPrice(totalOrderPrice);
		generalOrderDto.setTrSpreads(trSpreads);
		
		if(StringUtils.isNotEmpty(orderForm.getOrgIdAndcostIdAndCorpId())){
			String[] split = orderForm.getOrgIdAndcostIdAndCorpId().split(",");
			if(split.length == 3){
				generalOrderDto.setOrgId(split[0]);
				generalOrderDto.setCostUnitCode(split[1]);
				generalOrderDto.setCorporationId(split[2]);
			}
		}
		if(StringUtils.isNotBlank(orderForm.getProviderCode())){
			String[] split = orderForm.getProviderCode().split(",");
			if(split.length == 2){
				generalOrderDto.setProviderCode(split[0]);
				generalOrderDto.setProviderName(split[1]);
			}
		}

		generalOrderDto.setOrderShowStatus(OrderShowStatus.WAIT_PAYMENT);
		generalOrderDto.setOrderStatus(OrderStatus.WAIT_PAYMENT);
		generalOrderDto.setOfferDate(new Date());
		generalOrderService.save(generalOrderDto);
		
		//生成一个已报价任务
		OrderTaskDto task = new OrderTaskDto();
		task.setId(Integer.valueOf(taskId));
		task.setCompleteDate(Calendar.getInstance().getTime());
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		task.setTaskType(TaskType.WAIT_OFFER);
		task.setResult("通用订单报价已经成功");
		task.setUpdateDate(Calendar.getInstance().getTime());
		task.setOperatorId(getCurrentUser().getId().intValue());
		task.setOperatorName(getCurrentUser().getFullname());
		if(super.getTmcId() != null){
			task.setTmcId(super.getTmcId().toString());
		}
		orderTaskService.update(task);
		
		return true;
	}
	
	public UploadResult uploadImg(@RequestParam("file") MultipartFile file){
		String fileName = file.getOriginalFilename();
		
		String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		String fileType = FilenameUtils.getExtension(fileName).toLowerCase();
		
		String fileUrl ="general/" + uuid +"." + fileType;
		
		UploadResult result = ImgClient.uploadImg(file, "general" , uuid +"." + fileType);
		
		result.setUrlPath(fileUrl);
		result.setMsg(fileName);
		
		return result;
	}
	
	@RequestMapping(value = "deleteFile",method= RequestMethod.POST)
	@ResponseBody
	public void deleteFile(String id){

		generalOrderService.updateFile(id);
	}
	
	/**
	 * 催款任务
	 * @param orderId
	 * @param taskId
	 * @return
	 */
	@RequestMapping(value = "urgePay",method= RequestMethod.POST)
	@ResponseBody
	public boolean urgeDone(String orderId,String taskId){
		OrderTaskDto task = new OrderTaskDto();
		task.setId(Integer.valueOf(taskId));
		task.setTaskType(TaskType.WAIT_URGE_PAY);
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		task.setResult("客服已发短信催款");
		task.setUpdateDate(Calendar.getInstance().getTime());
		task.setOperatorId(getCurrentUser().getId().intValue());
		task.setOperatorName(getCurrentUser().getFullname());
		if(super.getTmcId() != null){
			task.setTmcId(super.getTmcId().toString());
		}
		orderTaskService.update(task);
		
		GeneralOrderDto	generalOrder = generalOrderService.getById(orderId);
		if(generalOrder != null){
			smsService.sendSms(generalOrder.getUserMobile(), "111", new String[]{},generalOrder.getcId());
		}
		return true;
	}
	
	/**
	 * 得到拒绝退款信息
	 * @param model
	 * @param orderId
	 * @param taskId
	 * @return
	 */
	@RequestMapping("/getRefuseRefundInfo")
	public String getRefuseRefundInfo(Model model,String orderId,Integer taskId ,String refundOrderId){
		
		model.addAttribute("taskId", taskId);
		model.addAttribute("orderId", orderId);
		model.addAttribute("refundOrderId", refundOrderId);
		
		return "general/tmc/refuseRefund.jsp";
	}
	
	/**
	 * 拒绝退款
	 * @param refundForm
	 * @return
	 */
	@RequestMapping(value = "refuseRefund",method= RequestMethod.POST)
	@ResponseBody
	public boolean refuseRefund(RefundForm refundForm){
		
		OrderTaskDto task = new OrderTaskDto();
		task.setId(Integer.valueOf(refundForm.getTaskId()));
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		task.setResult("拒绝退款,退款任务取消");
		task.setUpdateDate(Calendar.getInstance().getTime());
		if(super.getTmcId() != null){
			task.setTmcId(super.getTmcId().toString());
		}
		orderTaskService.update(task);
		
		RefundOrderDto refundOrderDto = new	RefundOrderDto();
		refundOrderDto.setId(refundForm.getRefundOrderId());
		refundOrderDto.setStatus(RefundStatus.REFUSED_REFUND);
		refundOrderDto.setAuditEndTime(new Date());
		refundOrderDto.setRefuseReason(refundForm.getRefuseReason());
		refundOrderService.update(refundOrderDto);
		
		return true;
	}
	
	
	/**
	 * 得到退款信息
	 * @param model
	 * @param orderId
	 * @param taskId
	 * @return
	 */
	@RequestMapping("/getRefundInfo")
	public String getRefundInfo(Model model,String orderId,Integer taskId ,String refundOrderId){
		
		GeneralOrderDto generalOrder = generalOrderService.getById(orderId);
		
		RefundOrderDto refundOrder = refundOrderService.getById(refundOrderId);
		
		model.addAttribute("taskId", taskId);
		model.addAttribute("orderId", orderId);
		model.addAttribute("refundOrderId", refundOrderId);
		model.addAttribute("generalOrderDto", generalOrder);
		model.addAttribute("refundOrder", refundOrder);
		
		return "general/tmc/refund.jsp";
	}	
	
	/**
	 * 取消退款前 生成退款单
	 * @param orderId
	 */
	@RequestMapping(value = "createRefund")
	@ResponseBody
	public String createRefundAbleAmount(String orderId){
		
		List<RefundOrderDto> findListByOrderId = refundOrderService.findListByOrderId(orderId);
		if(findListByOrderId != null && findListByOrderId.size() > 0 ){
			for(RefundOrderDto	refund: findListByOrderId){
				if(refund.getStatus().equals(RefundStatus.REFUNDING)){
					return refund.getId();
				}
			}
		}
		
		GeneralOrderDto generalOrder = generalOrderService.getById(orderId);
		BigDecimal saleOrderPrice = generalOrder.getTotalSalePrice();//销售价
		Integer personCount = generalOrder.getTravellerCount();//出行人数

		//生成退款订单
		RefundOrderDto refundOrderDto = new RefundOrderDto();
		refundOrderDto.setOrderId(orderId);
		refundOrderDto.setStatus(RefundStatus.REFUNDING);
		refundOrderDto.setReason("通用订单取消退款");
		refundOrderDto.setApplyTime(Calendar.getInstance().getTime());
		refundOrderDto.setAuditStartTime(Calendar.getInstance().getTime());
		refundOrderDto.setIsSystemRefund(false);
		refundOrderDto.setUserId(generalOrder.getUserId() + "");

		refundOrderDto.setRefundableAmount(saleOrderPrice);// 可退金额
		refundOrderDto.setRefundTotalAmount(saleOrderPrice);
		refundOrderDto.setRefundCount(personCount);// 份数

		RefundInfoDto infoDto = new RefundInfoDto();
		infoDto.setAim(generalOrder.getDestination());
		infoDto.setFromTime(generalOrder.getTravellerDate());
		infoDto.setRefundName(generalOrder.getUserName());

		refundOrderDto.setRefundInfoJson(JSONObject.toJSONString(infoDto));
		refundOrderDto.setPartnerId(generalOrder.getcId());

		refundOrderDto.setOrderSummary(generalOrder.getSummary());
		refundOrderDto.setTravelDate(generalOrder.getTravelBeginDate());
		refundOrderDto.setOrderCreateDate(generalOrder.getCreateDate());
		refundOrderDto.setIssueChannelType(generalOrder.getProviderCode());

		if(super.getTmcId() != null){
			refundOrderDto.setTmcId(super.getTmcId().toString());
		}
		refundOrderDto.setTravelType(TravelTypeEnum.BUSINESS);
		RefundOrderDto refundOrder = refundOrderService.applyRefund(refundOrderDto);
		
		return refundOrder.getId();
	}
	
	
	/**
	 * 退款
	 * @return
	 */
	@RequestMapping(value = "doRefund",method= RequestMethod.POST)
	@ResponseBody
	public ResponseInfo doRefund(RefundForm refundForm) {
		
		GeneralRefundDto refundDto = TransformUtils.transform(refundForm, GeneralRefundDto.class);
		refundDto.setRefundReasonCode("20");
		ResponseInfo refund = generalRefundService.refund(refundDto);
		
		return refund;
	}
	
	/**
	 * 任务交付
	 * @param taskId
	 * @return
	 */
	@RequestMapping(value = "confirmDone")
	@ResponseBody
	public boolean confirmDone(GeneralOrderForm orderForm, String taskId , MultipartFile file){
		GeneralOrderDto generalOrderDto = new GeneralOrderDto();
		
		if(file != null){
			UploadResult uploadRs = uploadImg(file);
			
			generalOrderDto.setFileName(uploadRs.getMsg());
			generalOrderDto.setFileUrls(uploadRs.getUrlPath());
		}
		
		generalOrderDto.setId(orderForm.getId());
		generalOrderDto.setOrderShowStatus(OrderShowStatus.ORDER_SUCCESS);
		generalOrderDto.setServiceDescr(orderForm.getServiceDescr());
		generalOrderDto.setRefundFlag(orderForm.getRefundFlag());
		generalOrderDto.setFinishDate(new Date());
		generalOrderService.save(generalOrderDto);
		
		OrderTaskDto task = new OrderTaskDto();
		task.setId(Integer.valueOf(taskId));
		task.setCompleteDate(Calendar.getInstance().getTime());
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		task.setTaskType(TaskType.WAIT_DELIVER);
		task.setResult("任务交付成功");
		task.setUpdateDate(Calendar.getInstance().getTime());
		task.setOperatorId(getCurrentUser().getId().intValue());
		task.setOperatorName(getCurrentUser().getFullname());
		orderTaskService.update(task);
        GeneralOrderDto generalOrder = generalOrderService.getById(orderForm.getId());
        //交付完成记录支出
		generalSettleChannelService.save(generalOrder,new BigDecimal(orderForm.getTotalSettlePrice()),-1);
		return true;
	}
	
	/**
	 * 取消订单
	 * @param orderId
	 * @param taskId
	 * @return
	 */
	@RequestMapping(value = "cancelOrder",method = RequestMethod.POST)
	@ResponseBody
	public boolean cancelOrder(String orderId , String taskId){

		GeneralOrderDto updateOrder = new GeneralOrderDto();
		updateOrder.setId(orderId);
		updateOrder.setOrderShowStatus(OrderShowStatus.CANCELED);
		generalOrderService.save(updateOrder);
		
		OrderTaskDto task = new OrderTaskDto();
		task.setId(Integer.valueOf(taskId));
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		task.setResult("订单取消,任务取消");
		task.setUpdateDate(Calendar.getInstance().getTime());
		orderTaskService.update(task);

		return true;
	}
	
	//全部任务
	@RequestMapping("/allTask")
	public String allTask(OrderTaskCondition condition, Model model, Integer pageIndex){
		if(pageIndex==null){
			pageIndex=1;
		}
		
		model.addAttribute("active",condition.getTaskStatus());
		
		QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex,10);
		condition.setOrderBizType(OrderBizType.GENERAL.name());
		if(super.getTmcId() != null){
			condition.setTmcId(super.getTmcId().toString());
		}
		queryDto.setCondition(condition);
		Page<OrderTaskDto> pageList = generalOrderService.orderTaskPageListQuery(queryDto);
		
//		//处理中任务数量（包含各状态的数量）
		condition.setTaskStatus(TaskStatus.PROCESSING.name());
		Map<String, Integer> typeMap = orderTaskService.getTaskCountMap(condition);
//		
		//已处理 
		condition.setTaskStatus(TaskStatus.ALREADY_PROCESSED.name());
		Map<String, Integer> alreadyMap = orderTaskService.getTaskCountMap(condition);
		
		//待处理任务数量（包含各状态的数量）
		condition.setTaskStatus(TaskStatus.WAIT_PROCESS.name());
		Map<String, Integer> waitTaskTypeMap = orderTaskService.getTaskCountMap(condition);
		
		model.addAttribute("pageList", pageList);
		model.addAttribute("typeMap", typeMap);
		model.addAttribute("alreadyMap", alreadyMap);
		model.addAttribute("waitTaskTypeMap", waitTaskTypeMap);
		model.addAttribute("originMap",OrderOriginType.getMap());
		taskCommonService.getTaskGroupCount(model,super.getTmcId().toString(),super.getCurrentUser().getId().intValue());

		return "general/tmc/allTask.jsp";
	}
	
	//订单查询页面
	@RequestMapping("/orderList")
	public String orderList(Model model){
		model.addAttribute("originMap",OrderOriginType.getMap());
		return "general/tmc/orderList.jsp";
	}

	@RequestMapping(value = "orderTable",method = RequestMethod.POST)
	public String orderTable(GeneralOrderCondition condition,Integer pageIndex,Integer pageSize,Model model){

		List<OrderCountDto> OrderStatuslist = getOrderCountList();
		if(pageIndex == null){
			pageIndex = 1;
		}

		condition.setTmcId(super.getTmcId());

		Page<GeneralOrderDto> pageList = getPageList(pageIndex,pageSize,condition);
		model.addAttribute("OrderStatuslist", OrderStatuslist);
		model.addAttribute("pageList", pageList);
		model.addAttribute("originMap",OrderOriginType.getMap());
		if(condition.getOrderStatus() != null && condition.getOrderStatus().length != 0){
			model.addAttribute("currentOrderStatus",condition.getOrderStatus()!=null?condition.getOrderStatus()[0]:null);
		}
		return "general/tmc/orderTable.jsp";
	}

	/**
	 * 查询各个状态的订单数量
	 * @return
	 */
	private List<OrderCountDto> getOrderCountList() {
		List<OrderCountDto> orderCountList = generalOrderService.findOrderCountList(super.getTmcId());
		
		List<OrderCountDto> list = new ArrayList<OrderCountDto>();
		OrderShowStatus[] values = OrderShowStatus.values();
		
		for(OrderShowStatus orderShowStatus : values){
			OrderCountDto orderCountDto = new OrderCountDto(orderShowStatus,0);
			for(OrderCountDto countDto : orderCountList){
				if(orderShowStatus.equals(countDto.getOrderShowStatus())){
					orderCountDto.setCount(countDto.getCount());
					break;
				}
			}
			
			list.add(orderCountDto);
		}
		return list;
	}

	private Page<GeneralOrderDto> getPageList(Integer pageIndex,Integer pageSize,GeneralOrderCondition condition){
		if(pageIndex==null){
			pageIndex = 1;
		}
		if(pageSize==null){
			pageSize = 10;
		}
		QueryDto<GeneralOrderCondition> queryDto = new QueryDto<GeneralOrderCondition>(pageIndex,pageSize);
		queryDto.setCondition(condition);
		Page<GeneralOrderDto> page = generalOrderService.queryListWithPage(queryDto);
		return page;
	}
	
	@RequestMapping("serviceType")
	@ResponseBody
	public List<DictCodeDto> getServiceType(){
		//通用订单   提交需求的类型
		List<DictCodeDto> serviceType = dictService.getCodes(GeneralConstants.GENERAL_SERVICE_TYPE);
		List<DictCodeDto> serviceTypeList = new ArrayList<DictCodeDto>();
		for(DictCodeDto serType : serviceType){
			if(serType.getPid() != null){
				serviceTypeList.add(serType);
			}
		}
		return serviceTypeList;
	}
	
	/**
	 * 修改任务超时时间
	 * @param taskId 任务id
	 * @param delay 延时时间：分钟
	 */
	@RequestMapping("/updateMaxProcessDate")
	public void updateMaxProcessDate(Integer taskId,Integer delay){
		OrderTaskDto dto = new OrderTaskDto();
		dto.setId(taskId);
		dto.setMaxProcessDate(DateUtils.addMinute(new Date(), delay));
		dto.setTaskStatus(TaskStatus.PROCESSING);
		orderTaskService.update(dto);
	}

	@ResponseBody
	@RequestMapping("/getToDesk")
	public Map<String, Object> getToDesk(Long userId ,String orderNo){
		JSONObject json = new JSONObject();
		//客服人员id
		json.put("data_serviceId", super.getCurrentUser().getId());

		String crc = ssoService.getLoginAuthToken(userId, json);

		Map<String, Object> loginMap = new HashMap<String, Object>();

		loginMap.put("loginUrl", Config.getString("loginUrl"));
		loginMap.put("alp", crc);
		loginMap.put("service", Config.getString("paymentUrl") + orderNo);

		String[] logoutString = ssoService.getLogoutUrls();

		loginMap.put("logoutUrl", logoutString);

		return loginMap;
	}

}
