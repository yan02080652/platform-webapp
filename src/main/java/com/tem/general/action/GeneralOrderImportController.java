package com.tem.general.action;

import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.ExcelUtil;
import com.iplatform.common.utils.LogUtils;
import com.iplatform.common.utils.ResourceUtils;
import com.tem.platform.api.DictService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.product.api.GeneralOrderService;
import com.tem.product.dto.general.GeneralOrderDto;
import com.tem.general.utils.ExcelHeaderUtil;
import com.tem.general.utils.FileDownloadUtil;
import com.tem.general.utils.GeneralConstants;
import com.tem.platform.action.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 通用订单导入
 * Created by chenjieming on 2017/8/18.
 */
@Controller
@RequestMapping("general/import")
public class GeneralOrderImportController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(GeneralOrderImportController.class);

    @Autowired
    private GeneralOrderService generalOrderService;

    @Autowired
    private DictService dictService;

    @RequestMapping()
    public String index(){
        return "general/import/index.base";
    }

    //下载模板
    @RequestMapping("download")
    public void download(Long partnerId) throws Exception {

        String filename = "通用订单导入模板.xls";
        String path = getRequest().getSession().getServletContext().getRealPath("") + "/resource/excel/";

        String[] handers = {"预订人","服务品类","差旅类型","出行人数","行程时间","行程地点","需求描述","销售价","服务费","服务内容","供应商名称","供应商订单号","应付供应商","订单创建时间"}; //列标题

        //供应商信息
        List<DictCodeDto> suppliers = dictService.getCodes(GeneralConstants.OTAH_HOTEL_SUPPLIER);
        List<String> supplierList = new ArrayList<>();
        for(DictCodeDto dict: suppliers){
            supplierList.add(dict.getItemTxt() + "-" + dict.getItemCode());
        }

        //服务类型
        List<DictCodeDto> serviceType = dictService.getCodes(GeneralConstants.GENERAL_SERVICE_TYPE);
        List<String> serviceTypeList = new ArrayList<>();
        for(DictCodeDto serType : serviceType){
            if(serType.getPid() != null){
                serviceTypeList.add(serType.getItemTxt() + "-" + serType.getItemCode());
            }
        }

        //差旅类型
        List<DictCodeDto> travelTypes = dictService.getPartnerCodes(partnerId, GeneralConstants.TM_TRAVEL_TYPE);
        List<String> travelTypeList = new ArrayList<>();
        for (DictCodeDto dict : travelTypes){
            travelTypeList.add(dict.getItemTxt() + "-" + dict.getItemCode());
        }

        //下拉框数据
        List<String[]> downData = new ArrayList<>();

        downData.add(supplierList.toArray(new String[supplierList.size()]));
        downData.add(serviceTypeList.toArray(new String[serviceTypeList.size()]));
        downData.add(travelTypeList.toArray(new String[travelTypeList.size()]));

        String [] downRows = {"10","1","2"}; //下拉的列序号数组(序号从0开始)

        ExcelHeaderUtil.createExcelTemplate(path + filename,handers,downData,downRows);

        FileDownloadUtil.downLoadFile(filename,"xlsx",path, getResponse());
    }

    //上传模板
    @ResponseBody
    @RequestMapping("upload")
    public ResponseInfo upload(Long pid, Long userId, MultipartFile excelFile) {
        List<GeneralOrderDto> generalList;
        try {
            InputStream is = excelFile.getInputStream();
            Map<String, String> keyValue = ResourceUtils.getResource("generalExcel").getMap();
            generalList = ExcelUtil.readExcel(is, keyValue, GeneralOrderDto.class);

        } catch (Exception e) {
            LogUtils.error(logger, "excel模板转换异常，异常信息={}", e.getMessage());
            return new ResponseInfo("1", e.getMessage(), null);
        }

        boolean flag = generalOrderService.batchImportOrder(generalList, pid, userId);
        if (flag) {
            return new ResponseInfo("0", "数据导入成功", null);
        } else {
            return new ResponseInfo("1", "数据导入失败", null);
        }
    }

}
