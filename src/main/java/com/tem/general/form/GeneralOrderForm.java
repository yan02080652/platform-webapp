
package com.tem.general.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 通用订单
 * @author chenjieming
 *
 */
public class GeneralOrderForm implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 订单号
	 */
	private String id;
	
	/**
	 * 服务品类
	 */
    private String serviceType;
    
    /**
     * 服务品类名称
     */
    private String serviceName;

    /**
     * 行程
     */
    private String destination;

    /**
     * 需求描述
     */
    private String descr;

    /**
     * 出行人数
     */
    private Integer travellerCount;
    
    /**
     * 出行时间
     */
    private String travellerDate;

	/**
	 * 供应商订单号
	 */
	private String providerOrderNo;
	
	  /**
     * 供应商编码
     */
    private String providerCode;

    /**
     * 供应商名称
     */
    private String providerName;

    /**
     * 服务内容
     */
    private String serviceDescr;

    /**
     * 总服务费
     */
    private Double servicePrice;

    /**
     * 客户票面销售总价
     */
    private Double totalSalePrice;
    
    /**
     * 客户应付总价
     * 
     * 客户最终结算价 = 客户销售价 + 服务费
     */
    private Double totalOrderPrice;
    
    /**
     * 供应商结付总价
     */
    private Double totalSettlePrice;
    
    /**
     * TR价差         
     * 
     * TR价差 = 客户销售价 - 供应商结算价，与服务费无关
     */
    private	Double trSpreads;

    /**
     * 成本单元所属组织,成本中心,法人,用逗号分隔
     */
    private String orgIdAndcostIdAndCorpId;
    
    /**
     * 差旅类型
     */
    private String travelCode;
    
    /**
     * 是否 允许退款 
     */
    private Integer refundFlag;
    
    /**
     * 附件
     */
    private String	fileUrls;
    
    /**
     * 附件名称
     */
    private String fileName;

	/**
	 * 实际出行开始时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date travelBeginDate;

	/**
	 * 实际出行结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date travelEndDate;
    

	public String getProviderOrderNo() {
		return providerOrderNo;
	}

	public void setProviderOrderNo(String providerOrderNo) {
		this.providerOrderNo = providerOrderNo;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getServiceDescr() {
		return serviceDescr;
	}

	public void setServiceDescr(String serviceDescr) {
		this.serviceDescr = serviceDescr;
	}


	public Double getTotalOrderPrice() {
		return totalOrderPrice;
	}

	public void setTotalOrderPrice(Double totalOrderPrice) {
		this.totalOrderPrice = totalOrderPrice;
	}

	public Double getTotalSettlePrice() {
		return totalSettlePrice;
	}

	public void setTotalSettlePrice(Double totalSettlePrice) {
		this.totalSettlePrice = totalSettlePrice;
	}

	public String getOrgIdAndcostIdAndCorpId() {
		return orgIdAndcostIdAndCorpId;
	}

	public void setOrgIdAndcostIdAndCorpId(String orgIdAndcostIdAndCorpId) {
		this.orgIdAndcostIdAndCorpId = orgIdAndcostIdAndCorpId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Integer getTravellerCount() {
		return travellerCount;
	}

	public void setTravellerCount(Integer travellerCount) {
		this.travellerCount = travellerCount;
	}

	public String getTravelCode() {
		return travelCode;
	}

	public void setTravelCode(String travelCode) {
		this.travelCode = travelCode;
	}

	public Integer getRefundFlag() {
		return refundFlag;
	}

	public void setRefundFlag(Integer refundFlag) {
		this.refundFlag = refundFlag;
	}

	public String getTravellerDate() {
		return travellerDate;
	}

	public void setTravellerDate(String travellerDate) {
		this.travellerDate = travellerDate;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Double getServicePrice() {
		return servicePrice;
	}

	public void setServicePrice(Double servicePrice) {
		this.servicePrice = servicePrice;
	}

	public Double getTotalSalePrice() {
		return totalSalePrice;
	}

	public void setTotalSalePrice(Double totalSalePrice) {
		this.totalSalePrice = totalSalePrice;
	}

	public Double getTrSpreads() {
		return trSpreads;
	}

	public void setTrSpreads(Double trSpreads) {
		this.trSpreads = trSpreads;
	}

	public String getFileUrls() {
		return fileUrls;
	}

	public void setFileUrls(String fileUrls) {
		this.fileUrls = fileUrls;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Date getTravelBeginDate() {
		return travelBeginDate;
	}

	public void setTravelBeginDate(Date travelBeginDate) {
		this.travelBeginDate = travelBeginDate;
	}

	public Date getTravelEndDate() {
		return travelEndDate;
	}

	public void setTravelEndDate(Date travelEndDate) {
		this.travelEndDate = travelEndDate;
	}
}