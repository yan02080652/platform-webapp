
package com.tem.general.form;

import java.io.Serializable;

/**
 * 退款form
 * @author chenjieming
 *
 */
public class RefundForm implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * 任务Id
	 */
	private String taskId;
	
	/**
	 * 原订单Id
	 */
	private String orderId;
	
	/**
	 * 退单Id
	 */
	private String refundOrderId;
	
	/**
	 * 供应商应退金额
	 */
	private String providerFee;
	
	/**
	 * 客户应退金额
	 */
	private String customerFee;
	
	/**
	 * 拒绝退款原因
	 */
	private String refuseReason;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getRefundOrderId() {
		return refundOrderId;
	}

	public void setRefundOrderId(String refundOrderId) {
		this.refundOrderId = refundOrderId;
	}

	public String getProviderFee() {
		return providerFee;
	}

	public void setProviderFee(String providerFee) {
		this.providerFee = providerFee;
	}

	public String getCustomerFee() {
		return customerFee;
	}

	public void setCustomerFee(String customerFee) {
		this.customerFee = customerFee;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getRefuseReason() {
		return refuseReason;
	}

	public void setRefuseReason(String refuseReason) {
		this.refuseReason = refuseReason;
	}
	
	

}