package com.tem.general.utils;

public class GeneralConstants {
	
	/**
	 * 酒店供应商
	 */
	public static final String OTAH_HOTEL_SUPPLIER = "OTAH_HOTEL_SUPPLIER";
	
	/**
	 * 服务品类
	 */
	public static final String GENERAL_SERVICE_TYPE = "GENERAL_SERVICE_TYPE";
	
	/**
	 * 差旅类型
	 */
	public static final String TM_TRAVEL_TYPE = "TM_TRAVEL_TYPE";

}
