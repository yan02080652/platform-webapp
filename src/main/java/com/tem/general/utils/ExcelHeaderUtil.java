package com.tem.general.utils;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by chenjieming on 2017/8/22.
 */
public class ExcelHeaderUtil {

    /**
     * @Title: createExcelTemplate
     * @Description: 生成Excel导入模板
     * @param @param filePath  Excel文件路径
     * @param @param handers   Excel列标题(数组)
     * @param @param downData  下拉框数据(数组)
     * @param @param downRows  下拉列的序号(数组,序号从0开始)
     * @return void
     * @throws
     */
    public static void createExcelTemplate(String filePath, String[] handers,
                                           List<String[]> downData, String[] downRows){

        HSSFWorkbook wb = new HSSFWorkbook();//创建工作薄

        HSSFSheet sheet1 = wb.createSheet("通用订单导入模板");

        //设置日期格式
        CellStyle cellStyle = wb.createCellStyle();
        CreationHelper creationHelper = wb.getCreationHelper();
        cellStyle.setDataFormat(
                creationHelper.createDataFormat().getFormat("yyyy-MM-dd  hh:mm:ss")
        );
        //设置double格式
        HSSFCellStyle doubleStyle = wb.createCellStyle();
        doubleStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
        //设置int格式
        HSSFCellStyle intStyle = wb.createCellStyle();
        intStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0"));

        for (int i = 1; i < 2; i++) {
            HSSFRow row = sheet1.createRow(i);//第一个sheet的第一行为标题

            HSSFCell cell2 = row.createCell(3);
            cell2.setCellStyle(intStyle);

            HSSFCell cell7 = row.createCell(7);
            cell7.setCellStyle(doubleStyle);

            HSSFCell cell8 = row.createCell(8);
            cell8.setCellStyle(doubleStyle);

            HSSFCell cell12 = row.createCell(12);
            cell12.setCellStyle(doubleStyle);

            HSSFCell cell13 = row.createCell(13); //获取第一行的每个单元格
            cell13.setCellStyle(cellStyle);
        }

        //生成sheet1内容
        HSSFRow row1 = sheet1.createRow(0);//第一个sheet的第一行为标题
        row1.setHeight((short)500);
        for(int i=0;i<handers.length;i++){
            HSSFCell cell = row1.createCell(i); //获取第一行的每个单元格
            sheet1.setColumnWidth(i, 3500); //设置每列的列宽
            cell.setCellValue(handers[i]); //往单元格里写数据
        }

        //设置下拉框数据
        for(int r=0;r<downRows.length;r++){
            String[] dlData = downData.get(r);//获取下拉对象
            int rownum = Integer.parseInt(downRows[r]);

            if(dlData.length == 0){
                continue;
            }

            if(dlData.length<255){ //255以内的下拉
                //255以内的下拉,参数分别是：作用的sheet、下拉内容数组、起始行、终止行、起始列、终止列
                sheet1.addValidationData(setDataValidation(sheet1, dlData, 1, 50000, rownum ,rownum)); //超过255个报错
            }
        }

        try {

            File f = new File(filePath); //写文件

            FileOutputStream out = new FileOutputStream(f);
            out.flush();
            wb.write(out);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @Title: setDataValidation
     * @Description: 下拉列表元素不多的情况(255以内的下拉)
     * @param @param sheet
     * @param @param textList
     * @param @param firstRow
     * @param @param endRow
     * @param @param firstCol
     * @param @param endCol
     * @param @return
     * @return DataValidation
     * @throws
     */
    private static DataValidation setDataValidation(Sheet sheet, String[] textList, int firstRow, int endRow, int firstCol, int endCol) {

        DataValidationHelper helper = sheet.getDataValidationHelper();
        //加载下拉列表内容
        DataValidationConstraint constraint = helper.createExplicitListConstraint(textList);
        //DVConstraint constraint = new DVConstraint();
        constraint.setExplicitListValues(textList);

        //设置数据有效性加载在哪个单元格上。四个参数分别是：起始行、终止行、起始列、终止列
        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);

        //数据有效性对象
        DataValidation data_validation = helper.createValidation(constraint, regions);

        return data_validation;
    }
}
