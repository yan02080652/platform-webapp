package com.tem.internationalFlight.DataHandler;

import cn.afterturn.easypoi.handler.impl.ExcelDataHandlerDefaultImpl;
import com.tem.international.enums.PassengerQualificationEnum;
import com.tem.internationalFlight.form.PolicyForm;
import com.tem.pss.dto.IssueChannelDto;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * @author yuanjianxin
 * @date 2018/10/8 12:08
 * 导入导出时对数据进行的处理
 */
public class PolicyDataHandler extends ExcelDataHandlerDefaultImpl<PolicyForm> {

    private List<IssueChannelDto> issueChannelList;

    public PolicyDataHandler() {
    }

    public PolicyDataHandler(List<IssueChannelDto> issueChannelList) {
        this.issueChannelList = issueChannelList;
    }

    @Override
    public Object exportHandler(PolicyForm obj, String name, Object value) {
        switch (name) {
            case "供应商":
                for (IssueChannelDto issueChannelDto : issueChannelList) {
                    if (issueChannelDto.getId().equals(value)) {
                        return issueChannelDto.getName();
                    }
                }
                break;
            case "乘客类型":
                return transformPassengerTypeExport((String) value);
            case "旅客资质":
                return transformQualificationExport((String) value);
            default:
                break;
        }
        return null;
    }

    @Override
    public Object importHandler(PolicyForm obj, String name, Object value) {
        switch (name) {
            case "供应商":
                if (StringUtils.isBlank((String) value)) {
                    break;
                }
                for (IssueChannelDto issueChannelDto : issueChannelList) {
                    if (issueChannelDto.getName().equals(value)) {
                        return issueChannelDto.getId();
                    }
                }
                //excel有值，但不匹配
             return -1;
            case "乘客类型":
                return transformPassengerTypeImport((String) value);
            case "旅客资质":
                return transformQualificationImport((String) value);
            default:
                break;
        }
        return null;
    }


    enum passengerType {
        ADU("成人"), CHD("成人陪伴儿童"), INF("无座婴儿");
        private String msg;

        passengerType() {
        }

        passengerType(String msg) {
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }
    }

    private String transformPassengerTypeExport(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        for (passengerType p : passengerType.values()) {
            if (value.contains(p.name())) {
                str.append(p.getMsg()).append("/");
            }
        }
        return str.substring(0, str.length() - 1);
    }

    private String transformQualificationExport(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        for (PassengerQualificationEnum qualificationEnum : PassengerQualificationEnum.values()) {
            if (value.contains(qualificationEnum.name())) {
                str.append(qualificationEnum.getMsg()).append("/");
            }
        }
        return str.substring(0, str.length() - 1);
    }

    private String transformPassengerTypeImport(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        for (passengerType p : passengerType.values()) {
            if (value.contains(p.getMsg())) {
                str.append(p.name()).append("/");
            }
        }
        return str.toString();
    }

    private String transformQualificationImport(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        for (PassengerQualificationEnum qualificationEnum : PassengerQualificationEnum.values()) {
            if (value.contains(qualificationEnum.getMsg())) {
                str.append(qualificationEnum.name()).append("/");
            }
        }
        return str.toString();
    }
}
