package com.tem.internationalFlight;

/**
 * @author yuanjianxin
 * @date 2018/9/30 15:36
 */
public class ErrorMsg {
    private int seq;
    private String msg;

    public ErrorMsg() {
    }

    public ErrorMsg(int seq, String msg) {
        this.seq = seq;
        this.msg = msg;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
