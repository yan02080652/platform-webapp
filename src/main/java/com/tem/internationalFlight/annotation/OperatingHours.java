package com.tem.internationalFlight.annotation;

import org.apache.commons.lang.ArrayUtils;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.text.SimpleDateFormat;
import java.util.Date;

@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = OperatingHours.Validator.class)
public @interface OperatingHours {

    String message() default "格式不正确";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


    class Validator implements ConstraintValidator<OperatingHours, Object> {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String[] weekDay = {"SUN","MON","TUE","WED","THU","FRI","SAT"};

        @Override
        public void initialize(OperatingHours timeLimit) {
        }

        @Override
        public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {

            if (value == null) {
                return Boolean.TRUE;
            }

            /**
             * 09:00-18:00表示每一天的早上9点到下午6点
             * 09:00MON-18:00FRI表示周一到周五的每天早上9点到下午6点
             * 00:00-23:59表示每一天的24小时
             */
            try{
                String timeStr = (String) value;
                String[] arr = timeStr.split("/");
                if (arr.length > 3){
                    return false;
                }

                for (String time : arr) {
                    String[] split = time.split("-");
                    Date start = sdf.parse(split[0].substring(0, 5));
                    Date end = sdf.parse(split[1].substring(0, 5));
                    if (!start.before(end)) {
                       return false;
                    }

                    if (split[0].length() > 5) {
                        if (!ArrayUtils.contains(weekDay,split[0].substring(5,8).toUpperCase())
                                || !ArrayUtils.contains(weekDay,split[1].substring(5,8).toUpperCase()) ) {
                            return false;
                        }
                    }
                }
            }catch (Exception e){
                return false;
            }
            return true;
        }

    }
}
