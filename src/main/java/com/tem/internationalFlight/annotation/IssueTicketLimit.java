package com.tem.internationalFlight.annotation;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = IssueTicketLimit.Validator.class)
public @interface IssueTicketLimit {

    String message() default "格式不正确";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


    class Validator implements ConstraintValidator<IssueTicketLimit, Object> {

        @Override
        public void initialize(IssueTicketLimit timeLimit) {
        }

        @Override
        public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {

            if (value == null) {
                return Boolean.TRUE;
            }

            /**
             * 0-365,7,3至少于预订后7天内出票或起飞前3天前出票
             */
            try {
                String timeStr = (String) value;
                String[] arr = timeStr.split("/");
                for (String time : arr) {
                    String[] limit = time.split(",");
                    String min = limit[0].split("-")[0];
                    String max = limit[0].split("-")[1];
                    if (Integer.parseInt(min) > Integer.parseInt(max)) {
                        return false;
                    }

                    if (Integer.parseInt(min) != 0 && Integer.parseInt(limit[2]) > Integer.parseInt(min)) {
                        return false;
                    }
                }
            } catch (Exception e) {
                return false;
            }
            return true;
        }
    }
}
