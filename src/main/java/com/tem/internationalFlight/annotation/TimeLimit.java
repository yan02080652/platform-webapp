package com.tem.internationalFlight.annotation;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.text.SimpleDateFormat;
import java.util.Date;

@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TimeLimit.Validator.class)
public @interface TimeLimit {

    String message() default "格式不正确";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};


    class Validator implements ConstraintValidator<TimeLimit, Object> {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


        @Override
        public void initialize(TimeLimit timeLimit) {
        }

        @Override
        public boolean isValid(Object value, ConstraintValidatorContext constraintValidatorContext) {

            if (value == null) {
                return Boolean.TRUE;
            }

            //2014-11-01>2015-06-01  2014-11-01>2015-06-01/2014-11-01>2015-06-01
            try{
                String str = (String) value;
                String[] split = str.split("/");
                for (String data : split) {
                    String[] timeArr = data.split(">");
                    Date start = sdf.parse(timeArr[0]);
                    Date end = sdf.parse(timeArr[1]);
                    if (!start.before(end)) {
                        constraintValidatorContext.disableDefaultConstraintViolation();
                        constraintValidatorContext.buildConstraintViolationWithTemplate("开始时间必须小余截止时间")
                                .addConstraintViolation();
                        return false;
                    }
                }
            }catch (Exception e){
                return false;
            }
            return true;
        }

    }
}
