package com.tem.internationalFlight.action;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.cache.utils.RedisCacheUtils;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.flight.Constants;
import com.tem.international.SegmentUtils;
import com.tem.international.api.InternationalFlightOrderService;
import com.tem.international.dto.InternationalFlightOrderDto;
import com.tem.international.dto.InternationalOrderFlightDetailDto;
import com.tem.international.dto.InternationalOrderPassengerDto;
import com.tem.international.enums.InternationalPassengerStatus;
import com.tem.international.enums.OrderShowStatus;
import com.tem.international.enums.OrderStatus;
import com.tem.international.enums.TicketStatus;
import com.tem.international.enums.TripType;
import com.tem.international.interFlight.InternationalOfferDto;
import com.tem.international.order.InternationCondition;
import com.tem.international.order.OfferForm;
import com.tem.international.order.OrderCountDto;
import com.tem.internationalFlight.form.CreatePicForm;
import com.tem.internationalFlight.form.InterTicketFrom;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.api.task.OrderTaskService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.refund.RefundOrderDto;
import com.tem.pss.dto.task.OrderTaskCondition;
import com.tem.pss.dto.task.OrderTaskDto;
import com.tem.pss.dto.task.TaskParamDto;
import com.tem.pss.dto.task.TaskStatus;
import com.tem.pss.dto.task.TaskType;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import com.tem.pss.enums.TicketTypeEnum;
import com.tem.tmc.TaskCommonService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping(value = "/internationalflight/tmc")
@Controller
public class InternationalTmcController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(InternationalTmcController.class);

    private static final String INTERNATIONAL_CACHE_KEY = "INTERNATIONAL_%s_%s";

    @Autowired
    private InternationalFlightOrderService internationalFlightOrderService;
    @Autowired
    private DictService dictService;
    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private IssueChannelService issueChannelService;
    @Autowired
    private TaskCommonService taskCommonService;
    @Autowired
    private RefundOrderService refundOrderService;
    @Autowired
    private PartnerService partnerService;


    @RequestMapping("/index")
    public String index(Model model) {
        return "flight/tmc/international/index.jsp";
    }

    @RequestMapping("/orderList")
    public String orderList(Model model) {
        QueryDto<InternationCondition> queryDto = new QueryDto<InternationCondition>();
        InternationCondition flightOrderCondition = new InternationCondition();
        flightOrderCondition.setTmcId(super.getTmcId());
        queryDto.setCondition(flightOrderCondition);
        List<OrderCountDto> OrderStatuslist = getOrderCountList(queryDto);

        Page<InternationalFlightOrderDto> pageList = getPageList(null, null, flightOrderCondition);


        Map<String, String> cabinClassMap = getCabinClassMap();
        //出票渠道map
        Map<String, String> issueChannelMap = getIssueChannelMap();

        model.addAttribute("OrderStatuslist", OrderStatuslist);
        model.addAttribute("pageList", pageList);
        model.addAttribute("cabinClassMap", cabinClassMap);
        model.addAttribute("issueChannelMap", issueChannelMap);

        return "flight/tmc/international/internationalOrderList.jsp";
    }

    //分页数据
    private Page<InternationalFlightOrderDto> getPageList(Integer pageIndex, Integer pageSize, InternationCondition condition) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        QueryDto<InternationCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(condition);
        Page<InternationalFlightOrderDto> page = internationalFlightOrderService.findOrderList(queryDto);
        return page;
    }

    //查询各个状态的订单数量
    private List<OrderCountDto> getOrderCountList(QueryDto<InternationCondition> queryDto) {
        List<OrderCountDto> orderCountList = internationalFlightOrderService.findOrderCountList(queryDto);

        List<OrderCountDto> list = new ArrayList<>();
        //TODO 目前只有待支付和已出票

        for (OrderShowStatus orderShowStatus : OrderShowStatus.values()) {
            OrderCountDto orderCountDto = new OrderCountDto(orderShowStatus, 0);
            for (OrderCountDto countDto : orderCountList) {
                if (orderShowStatus.equals(countDto.getOrderShowStatus())) {
                    orderCountDto.setCount(countDto.getCount());
                    break;
                }
            }

            list.add(orderCountDto);
        }
        return list;
    }

    //订单查询异步加载的数据
    @RequestMapping(value = "/getOrderTable")
    public String getOrderTable(Model model, Integer pageIndex, Integer pageSize, Long startDate1,
                                Long endDate1, InternationCondition condition) {

        String currentOrderStatus = condition.getOrderStatus() != null ? condition.getOrderStatus()[0] : null;

        if (startDate1 != null) {
            Date start = new Date(startDate1);
            condition.setStartDate(DateUtils.getDayBegin(start));
        }
        if (endDate1 != null) {
            Date end = new Date(endDate1);
            condition.setEndDate(DateUtils.getDayEnd(end));
        }
        if (StringUtils.isBlank(condition.getIssueChannel())) {
            condition.setIssueChannel(null);
        }

        condition.setTmcId(super.getTmcId());

        Page<InternationalFlightOrderDto> pageList = getPageList(pageIndex, pageSize, condition);

        Map<String, String> cabinClassMap = getCabinClassMap();
        //出票渠道map
        //Map<String, String> issueChannelMap = getIssueChannelMap();


        QueryDto<InternationCondition> queryDto = new QueryDto<>();
        condition.setOrderStatus(null);
        queryDto.setCondition(condition);
        List<OrderCountDto> OrderStatuslist = getOrderCountList(queryDto);

        model.addAttribute("pageList", pageList);
        model.addAttribute("cabinClassMap", cabinClassMap);
        model.addAttribute("OrderStatuslist", OrderStatuslist);
//        model.addAttribute("issueChannelMap", issueChannelMap);
        model.addAttribute("currentOrderStatus", currentOrderStatus);

        return "flight/tmc/international/internationalOrderTable.jsp";
    }

    //舱位等级Map
    private Map<String, String> getCabinClassMap() {
        List<DictCodeDto> codes = dictService.getCodes(Constants.OTAF_CABIN_GRADE);
        Map<String, String> cabinClassMap = new HashMap<String, String>();
        for (DictCodeDto codeDto : codes) {
            cabinClassMap.put(codeDto.getItemCode(), codeDto.getItemTxt());
        }
        return cabinClassMap;
    }

    //我的任务
    @RequestMapping("/getMyTaskTable")
    public String getMyTaskTable(Model model, Integer pageIndex, String taskStatus, String taskType) {

        if (pageIndex == null) {
            pageIndex = 1;
        }

        if (taskStatus == null) {
            taskStatus = TaskStatus.PROCESSING.toString();
        }

        QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex, 10);
        OrderTaskCondition condition = new OrderTaskCondition();

        condition.setTaskStatus(taskStatus);
        condition.setTaskType(taskType);
        condition.setOperatorId(super.getCurrentUser().getId().intValue());
        condition.setOrderBizType(OrderBizType.INTERNATIONAL_FLIGHT.name());
        condition.setTmcId(super.getTmcId().toString());
        queryDto.setCondition(condition);


        Page<OrderTaskDto> pageList = internationalFlightOrderService.orderTaskPageListQuery(queryDto);

        this.getStatusAndTypeCount(model, super.getCurrentUser().getId().intValue());

        //舱位map
        Map<String, String> cabinClassMap = getCabinClassMap();
        Map<String, String> issueChannelMap = getIssueChannelMap();

        taskCommonService.getTaskGroupCount(model, super.getTmcId().toString(), super.getCurrentUser().getId().intValue());

        model.addAttribute("pageList", pageList);
        model.addAttribute("cabinClassMap", cabinClassMap);
        model.addAttribute("taskStatus", taskStatus);
        model.addAttribute("issueChannelMap", issueChannelMap);

        return "flight/tmc/international/myTask.jsp";
    }

    //全部任务
    @RequestMapping("/allTask")
    public String allTask(Model model, Integer pageIndex, String taskStatus, String taskType, String keyword) {
        if (pageIndex == null) {
            pageIndex = 1;
        }

        if (taskStatus == null) {
            taskStatus = TaskStatus.WAIT_PROCESS.toString();
        }

        QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex, 10);
        OrderTaskCondition condition = new OrderTaskCondition();


        condition.setTaskStatus(taskStatus);
        condition.setTaskType(taskType);
        condition.setOperatorId(null);
        condition.setTmcId(super.getTmcId().toString());
        condition.setOrderBizType(OrderBizType.INTERNATIONAL_FLIGHT.name());
        if (StringUtils.isNotBlank(keyword)) {
            condition.setKeyword(keyword);
        }
        queryDto.setCondition(condition);

        Page<OrderTaskDto> pageList = internationalFlightOrderService.orderTaskPageListQuery(queryDto);

        this.getStatusAndTypeCount(model, null);

        //舱位map
        Map<String, String> cabinClassMap = getCabinClassMap();
        //出票渠道map
        Map<String, String> issueChannelMap = getIssueChannelMap();

        taskCommonService.getTaskGroupCount(model, super.getTmcId().toString(), super.getCurrentUser().getId().intValue());

        model.addAttribute("pageList", pageList);
        model.addAttribute("cabinClassMap", cabinClassMap);
        model.addAttribute("taskStatus", taskStatus);
        model.addAttribute("issueChannelMap", issueChannelMap);

        return "flight/tmc/international/allTask.jsp";
    }

    @ResponseBody
    @RequestMapping("/transfer")
    public boolean transfer(Integer taskId, Integer operatorId, Integer userId, String userName) {
        logger.debug("tmc任务交接，入参:" + taskId + "--" + operatorId + "--" + userId + "--" + userName);
        if (operatorId == null) {
            operatorId = super.getCurrentUser().getId().intValue();
        }
        TaskParamDto dto = new TaskParamDto();
        dto.setTaskId(taskId);
        dto.setOperatorId(operatorId);
        dto.setUserId(userId);
        dto.setUserName(userName);
        dto.setRemork(super.getCurrentUser().getFullname() + "(" + operatorId + ")将任务交接给" + userName + "(" + userId + "),任务id:" + taskId);
        dto.setOrderBizType(OrderBizType.FLIGHT.name());
        dto.setTaskStatus(TaskStatus.PROCESSING);

        boolean rs = orderTaskService.saveTaskAndLog(dto);

        logger.debug("tmc任务交接返回结果=={}", rs);

        return rs;
    }

    /**
     * 后台报价详细页
     *
     * @param model
     * @param orderId
     * @param taskId
     * @return
     */
    @RequestMapping("/fillOrder")
    public String fillOrder(Model model, String orderId, Integer taskId, String operatorId) {

        InternationalFlightOrderDto orderDto = internationalFlightOrderService.findFlightOrderById(orderId);

        //供应商信息
        List<IssueChannelDto> issueChannelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType
                (super.getTmcId(), IssueChannelType.INTERNATIONAL_FLIGHT.getType(), LineType.OFF_LINE.getType());
        model.addAttribute("issueChannelList", issueChannelList);

        model.addAttribute("taskId", taskId);
        model.addAttribute("orderDto", orderDto);

        return "flight/tmc/international/order-model.jsp";
    }

    /**
     * 国际机票订单报价
     */
    @RequestMapping("/offer")
    @ResponseBody
    public boolean repair(OfferForm form, String taskId) {
        LogUtils.debug(logger, "国际机票订单报价，入参={}", JSONObject.toJSONString(form));
        try {
            form.setServiceId(super.getCurrentUser().getId());

            internationalFlightOrderService.offerOrder(form);

            //生成一个已报价任务
            OrderTaskDto task = new OrderTaskDto();
            task.setId(Integer.valueOf(taskId));
            task.setCompleteDate(Calendar.getInstance().getTime());
            task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
            task.setTaskType(TaskType.WAIT_OFFER);
            task.setResult("国际机票报价已经成功");
            task.setUpdateDate(Calendar.getInstance().getTime());
            task.setOperatorId(getCurrentUser().getId().intValue());
            task.setOperatorName(getCurrentUser().getFullname());
            if (super.getTmcId() != null) {
                task.setTmcId(super.getTmcId().toString());
            }
            orderTaskService.update(task);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 判断操作人是否为任务领取人
     *
     * @param operatorId 操作人id
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkOperator")
    public boolean checkOperator(String operatorId) {
        Long userId = super.getCurrentUser().getId();
        if (userId.toString().equals(operatorId)) {
            return true;
        }
        return false;
    }

    //获取订单数据（回填票号和线下出票时弹出model是需要的数据）
    @RequestMapping("/getTicketData")
    public String getTicketData(Model model, String orderId, Integer taskId, String flag) {
        InternationalFlightOrderDto order = internationalFlightOrderService.findFlightOrderById(orderId);

        List<IssueChannelDto> channelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
                IssueChannelType.INTERNATIONAL_FLIGHT.getType(), LineType.OFF_LINE.getType());

        model.addAttribute("channelList", channelList);
        model.addAttribute("passengerList", order.getPassengers());
        model.addAttribute("taskId", taskId);
        model.addAttribute("orderId", orderId);
        model.addAttribute("flag", flag);

        return "flight/tmc/international/ticket.jsp";
    }

    //保存（线下出票和回填票号）
    @ResponseBody
    @RequestMapping("/saveTicket")
    public boolean saveTicket(@RequestBody InterTicketFrom formObj) {
        try {
            InternationalFlightOrderDto dto = internationalFlightOrderService.findFlightOrderById(formObj.getOrderId());
            if (OrderShowStatus.ISSUED.equals(dto.getOrderShowStatus())) {
                return false;
            }
            List<IssueChannelDto> channelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
                    IssueChannelType.INTERNATIONAL_FLIGHT.getType(), LineType.OFF_LINE.getType());
            for (IssueChannelDto channelDto : channelList) {
                if (channelDto.getCode().equals(formObj.getIssueChannel())) {
                    dto.setSupplierName(channelDto.getName());
                }
            }

            dto.setIssueChannel(formObj.getIssueChannel());
            dto.setPnrCode(formObj.getPnr());
            dto.setExternalOrderId(formObj.getExternalOrderId());
            dto.setExternalOrderStatus("ISSUED");

            //供应商结算价
            dto.setTotalSettlePrice(formObj.getTotalPrice());
            //重新计算tr价差
//            dto.settr(BigDecimalUtils.subtract(dto2.getTotalSalePrice(), dto2.getTotalSettlePrice()));

            dto.setOrderShowStatus(OrderShowStatus.ISSUED);
            dto.setOrderStatus(OrderStatus.ISSUED);

            OrderTaskDto orderTaskDto = new OrderTaskDto();
            orderTaskDto.setId(formObj.getTaskId());
            orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
            orderTaskDto.setCompleteDate(new Date());
            orderTaskDto.setOperatorId(super.getCurrentUser().getId().intValue());
            orderTaskDto.setOperatorName(super.getCurrentUser().getFullname());
            orderTaskDto.setTmcId(String.valueOf(super.getTmcId()));
            orderTaskDto.setResult("线下出票");
            orderTaskDto.setTicketType(TicketTypeEnum.OFFLINE);


            for (InternationalOrderPassengerDto newPassenger : formObj.getPassengerDtos()) {
                for (InternationalOrderPassengerDto oldPassenger : dto.getPassengers()) {
                    if (newPassenger.getId().equals(oldPassenger.getId())) {
                        oldPassenger.setTicketStatus(TicketStatus.UNUSED);
                        oldPassenger.setStatus(InternationalPassengerStatus.ISSUED);
                        oldPassenger.setTicketNo(newPassenger.getTicketNo());
                    }
                }
            }
            return internationalFlightOrderService.offLineTicket(dto, orderTaskDto, formObj.isCancelSupplierOrder());
        } catch (Exception e) {
            LogUtils.error(logger, "TMC线下出票或回填票号异常，异常信息={}", e);
            return false;
        }
    }

    @RequestMapping(value = "/modifyTicket", produces = "application/json; charset=utf-8")
    @ResponseBody
    public ResponseInfo modifyTicket(@RequestBody InterTicketFrom formObj) {
        try {
            internationalFlightOrderService.modifyTicket(formObj.getOrderId(), formObj.getPassengerDtos(), formObj.getIssueChannel(),
                    formObj.getPnr(), formObj.getExternalOrderId(), formObj.getTotalPrice(),
                    super.getCurrentUser().getFullname());
        } catch (Exception e) {
            LogUtils.error(logger, "修改出票信息异常，异常信息={}", e);
            return new ResponseInfo("-1", "系统异常，修改失败！");
        }
        return new ResponseInfo("1", "success");
    }

    //拒单退款
    @ResponseBody
    @RequestMapping("/rejectOrder")
    public boolean rejectOrder(String orderId, Integer taskId, String userId, String remark) {
        LogUtils.debug(logger, "进入拒单退款,orderId={},taskId={},userId={},remark={}", orderId, taskId, userId, remark);
        try {
            OrderTaskDto orderTaskDto = new OrderTaskDto();
            orderTaskDto.setId(taskId);
            orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
            orderTaskDto.setCompleteDate(new Date());
            orderTaskDto.setResult("拒单退款");
            orderTaskDto.setOperatorId(super.getCurrentUser().getId().intValue());
            orderTaskDto.setOperatorName(super.getCurrentUser().getFullname());

            boolean rs = internationalFlightOrderService.rejectOrder(orderId, userId, remark, orderTaskDto);

            LogUtils.debug(logger, "接口返回结果={}", rs);

            return rs;
        } catch (Exception e) {
            LogUtils.error(logger, "拒单退款异常，msg={}", e);
            return false;
        }
    }

    //取消订单
    @ResponseBody
    @RequestMapping("/cancelOrder")
    public boolean cancelOrder(String orderId, String taskId) {
        //调用取消订单接口

        try {
            internationalFlightOrderService.cancelOfferOrder(orderId, taskId, super.getCurrentUser().getId());
        } catch (Exception e) {
            LogUtils.error(logger, "取消待报价订单发生错误={}", e);
            return false;
        }
        return true;
    }

    //获取订单详情和退票信息
    @RequestMapping("/getOrderDetail")
    public String getOrderDetail(Model model, String refundOrderId, String taskId) {

        //退票单
        RefundOrderDto refundOrderDto = refundOrderService.getById(refundOrderId);
        //原订单
        refundOrderDto.setOrderDto(internationalFlightOrderService.findFlightOrderById(refundOrderDto.getOrderId()));
        double serverCharge = 0;//serverChargeService.getServerCharge(refundOrderDto.getPartnerId(), ServerChargeSceneEnum.IN_FLIGHT_REFUND.getValue());

        //出票渠道map
        Map<String, String> issueChannelMap = getIssueChannelMap();

        Map<String, String> cabinClassMap = getCabinClassMap();
        model.addAttribute("cabin", cabinClassMap);
        model.addAttribute(refundOrderDto);
        model.addAttribute("taskId", taskId);
        model.addAttribute("issueChannelMap", issueChannelMap);
        model.addAttribute("serverCharge", serverCharge);

        return "flight/tmc/international/orderDetail.single";
    }

    //审核通过
    @ResponseBody
    @RequestMapping("/refundAduitPass")
    public boolean refundAduitPass(RefundOrderDto refundOrderDto, Integer taskId) {
        try {
            refundOrderDto.setOperatorName(super.getCurrentUser().getFullname());
            refundOrderDto.setOperatorId(super.getCurrentUser().getId());
            refundOrderDto.setPassed(true);
            refundOrderDto.setAuditEndTime(new Date());

            InternationalFlightOrderDto orderDto = internationalFlightOrderService.findFlightOrderById(refundOrderDto.getOrderId());

            //可退金额（单）= 销售价+机建费+燃油+升舱+改签
            BigDecimal refundableAmount = BigDecimalUtils.add(orderDto.getSingleTicketPrice(), orderDto.getSingleTaxFee());
            refundOrderDto.setRefundableAmount(refundableAmount);

            //扣除金额 （单）= 退票费（单）+ 调整费（单）+ TEM服务费（单）
            BigDecimal decimal = BigDecimalUtils.add(refundOrderDto.getRefundFee(), refundOrderDto.getAdjustFee(), refundOrderDto.getRefundServiceFee());

            //应退金额（单） = 可退金额（单） - 扣除金额 （单）
            refundOrderDto.setActualRefundAmount(BigDecimalUtils.subtract(refundableAmount, decimal));

            //应退总额
            BigDecimal totalAmount = BigDecimalUtils.multiply(refundOrderDto.getActualRefundAmount(), refundOrderDto.getRefundCount());
            refundOrderDto.setRefundTotalAmount(BigDecimalUtils.multiply(totalAmount, -1));

            //供应商退票费（总） = 供应商退票费(单)*退单人数
            BigDecimal totalRefundFee = BigDecimalUtils.multiply(refundOrderDto.getRefundFee(), refundOrderDto.getRefundCount());

            //供应商应退额   = -1*（原订单.供应商结算价/原订单.乘机人个数*退单人数 - 供应商退票费(单)*退单人数）
            BigDecimal supperRefundAmount = BigDecimalUtils.subtract(BigDecimalUtils.multiply(BigDecimalUtils.divide(orderDto.getTotalSettlePrice(),
                    BigDecimal.valueOf(orderDto.getPassengers().size())), refundOrderDto.getRefundCount()), totalRefundFee);
            refundOrderDto.setSupperRefundAmount(BigDecimalUtils.multiply(supperRefundAmount, -1));

            //总调整费
            BigDecimal totalAdjustFee = BigDecimalUtils.multiply(refundOrderDto.getAdjustFee(), refundOrderDto.getRefundCount());

            // 客户票面应退额 = = -1*（正向客户票面销售总价/原订单.乘机人个数*退单人数 - 供应商退票费(总)-调整费(总))
            BigDecimal customerRefundAmount = BigDecimalUtils.subtract(BigDecimalUtils.subtract(
                    BigDecimalUtils.multiply(BigDecimalUtils.divide(orderDto.getTotalSalePrice(),
                            BigDecimal.valueOf(orderDto.getPassengers().size())),
                            refundOrderDto.getRefundCount()), totalRefundFee), totalAdjustFee);
            refundOrderDto.setCustomerRefundAmount(BigDecimalUtils.multiply(customerRefundAmount, -1));

            //服务费
            refundOrderDto.setServicePrice(BigDecimalUtils.multiply(refundOrderDto.getRefundServiceFee(), refundOrderDto.getRefundCount()));

            //tr价差
            refundOrderDto.setTrSpreads(BigDecimalUtils.subtract(refundOrderDto.getCustomerRefundAmount(), refundOrderDto.getSupperRefundAmount()));

            boolean rs = internationalFlightOrderService.refundorderAduit(refundOrderDto, taskId);
            if (rs) {
                try {
                    //修改退票人记录中的分摊金额
                    BigDecimal amount1 = BigDecimalUtils.multiply(refundOrderDto.getActualRefundAmount(), -1);
                    refundOrderService.updateRefundPassengrByRefundOrderId(refundOrderDto.getId(), amount1);
                } catch (Exception e) {
                    LogUtils.error(logger, "修改退票人记录中的分摊金额时发生异常，退票单id=={}，异常信息=={}", refundOrderDto.getId(), e);
                }
            }
            return rs;
        } catch (Exception e) {
            LogUtils.error(logger, "TMC客服审核发送异常，异常信息={}", e);
            return false;
        }
    }

    //审核不通过
    @ResponseBody
    @RequestMapping("/refundAduitNoPass")
    public boolean refundAduitNoPass(RefundOrderDto refundOrderDto, Integer taskId) {
        refundOrderDto.setOperatorName(super.getCurrentUser().getFullname());
        refundOrderDto.setOperatorId(super.getCurrentUser().getId());
        refundOrderDto.setPassed(false);
        refundOrderDto.setAuditEndTime(new Date());
        boolean rs = internationalFlightOrderService.refundorderAduit(refundOrderDto, taskId);
        return rs;
    }

    @RequestMapping("/orderDetail")
    public String orderDetail(Model model, String orderId) {
        InternationalFlightOrderDto orderDto = internationalFlightOrderService.findFlightOrderById(orderId);
        List<IssueChannelDto> channelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
                IssueChannelType.INTERNATIONAL_FLIGHT.getType(), LineType.OFF_LINE.getType());

        model.addAttribute("order", orderDto);
        model.addAttribute("channelList", channelList);

        return "flight/tmc/international/modifyTicket.jsp";
    }

    /**
     * 获取各状态下各个类型的任务数量
     *
     * @param model
     */
    private void getStatusAndTypeCount(Model model, Integer userId) {

        OrderTaskCondition condition = new OrderTaskCondition();
        condition.setOrderBizType(OrderBizType.INTERNATIONAL_FLIGHT.name());
        condition.setOperatorId(userId);
        condition.setTmcId(super.getTmcId().toString());

        condition.setTaskStatus(TaskStatus.PROCESSING.toString());
        //处理中任务数量（包含各状态的数量）
        Map<String, Integer> processingMap = orderTaskService.getTaskCountMap(condition);

        condition.setTaskStatus(TaskStatus.ALREADY_PROCESSED.toString());
        //已处理任务数量（包含各状态的数量）
        Map<String, Integer> alreadyMap = orderTaskService.getTaskCountMap(condition);

        //还未领取的任务(包含各状态的数量)
        condition.setTaskStatus(TaskStatus.WAIT_PROCESS.toString());
        condition.setOperatorId(null);
        Map<String, Integer> waitTaskTypeMap = orderTaskService.getTaskCountMap(condition);

        model.addAttribute("processingMap", processingMap);
        model.addAttribute("alreadyMap", alreadyMap);
        model.addAttribute("waitTaskTypeMap", waitTaskTypeMap);
    }

    //出票渠道map
    private Map<String, String> getIssueChannelMap() {
        Map<String, String> issueChannelMap = new HashMap<>();
        List<IssueChannelDto> list = issueChannelService.findListByTmcIdAndChannelType(super.getTmcId(), IssueChannelType.INTERNATIONAL_FLIGHT.getType());
        for (IssueChannelDto channelDto : list) {
            issueChannelMap.put(channelDto.getCode(), channelDto.getName());
        }
        return issueChannelMap;
    }


    /**
     * 国际机票报价
     *
     * @return
     */
    @RequestMapping(value = "/orderOffer")
    public String internationalFlightOffer(Model model) {
        PartnerDto partnerDto = partnerService.findById(super.getCurrentPartnerId());

        model.addAttribute("partner", partnerDto);
        return "flight/tmc/international/orderOffer.base";
    }

    @RequestMapping(value = "createTemplate")
    @ResponseBody
    public ResponseInfo createTemplate(@RequestBody CreatePicForm form) {
        ResponseInfo res = new ResponseInfo();
        try {

            LogUtils.debug(logger, "国际机票创建模板,入参={}", form);

            InternationalOfferDto offerDto = form.toOfferDto();

            String key = String.format(INTERNATIONAL_CACHE_KEY, super.getTmcId(), System.currentTimeMillis());

            RedisCacheUtils.put(key, offerDto, 10 * 60);
            res.setMsg(key);
            res.setCode("0");
        } catch (Exception e) {
            res.setCode("1");
        }
        return res;
    }


    @RequestMapping(value = "/parsePnr")
    @ResponseBody
    public Map<String, Object> parsePnr(String pnr) {

        Map<String, Object> map = null;
        try {

            map = new HashMap<>();
            ResponseInfo responseInfo = internationalFlightOrderService.pnrParse(String.valueOf(super.getCurrentPartnerId()), String.valueOf(super.getTmcId()), pnr);
            if ("1".equals(responseInfo.getCode())) {

                InternationalFlightOrderDto data = (InternationalFlightOrderDto) responseInfo.getData();

                List<InternationalOrderFlightDetailDto> details = data.getDetails();
                for (int i = 0; i < details.size(); i++) {
                    InternationalOrderFlightDetailDto detailDto = details.get(i);
                    if (i != details.size() - 1) {
                        detailDto.setTurningPoint(SegmentUtils.transferPoint(detailDto, details.get(i + 1)));
                        detailDto.setBaggageDirect(SegmentUtils.directBaggage(detailDto, details.get(i + 1)));
                        detailDto.setTransitVisa(SegmentUtils.needTransitVisa(detailDto, details.get(i + 1)));
                    } else {
                        detailDto.setTurningPoint(false);
                        detailDto.setBaggageDirect(false);
                        detailDto.setTransitVisa(false);
                    }

                }

                if (data.getDetails().size() == 1) {
                    data.setTripType(TripType.OW);
                } else {
                    InternationalOrderFlightDetailDto first = data.getDetails().get(0);
                    InternationalOrderFlightDetailDto last = data.getDetails().get(data.getDetails().size() - 1);

                    boolean b = SegmentUtils.isInternational(first.getFromVisaDto(), last.getToVisaDto(), first.getFromAirportDto().getCountryId(), last.getToAirportDto().getCountryId());
                    if (b) {
                        data.setTripType(TripType.OW);
                    } else {
                        data.setTripType(TripType.RT);
                    }
                }

                map.put("result", 1);
                map.put("data", data);
            } else if ("-2".equals(responseInfo.getCode())) {
                map.put("result", -2);
                map.put("msg", "企业未找到有效可用的黑屏连接，请联系管理员");
            } else if ("-3".equals(responseInfo.getCode())) {
                map.put("result", -2);
                map.put("msg", responseInfo.getMsg());
            } else {
                map.put("result", -1);
            }
        } catch (Exception e) {
            LogUtils.debug(logger, "解析pnr，发生异常={}", e);
            map.put("result", -1);
        }

        return map;
    }


}
