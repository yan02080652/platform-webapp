package com.tem.internationalFlight.action;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.international.api.policy.InternationalPolicyService;
import com.tem.international.dto.policy.InternationalPolicyDto;
import com.tem.international.dto.policy.PolicyCondition;
import com.tem.international.enums.PolicyStatusEnum;
import com.tem.internationalFlight.DataHandler.PolicyDataHandler;
import com.tem.internationalFlight.ErrorMsg;
import com.tem.internationalFlight.form.PolicyForm;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.platform.form.ResponseTable;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.enums.IssueChannelType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yuanjianxin
 * @date 2018/9/26 10:18
 */
@RequestMapping("/flight/international/policy")
@Controller
public class PolicyController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(PolicyController.class);

    public static final String FAILED = "0";
    public static final String SUCCESS = "1";

    @Autowired
    private InternationalPolicyService internationalPolicyService;
    @Autowired
    private PartnerService partnerService;
    @Autowired
    private IssueChannelService issueChannelService;
    @Autowired
    private UserService userService;
    @Autowired
    private Validator validator;

    @RequestMapping(value = "")
    public String index(Model model) {
        PartnerDto partner = partnerService.findById(super.getTmcId());
        model.addAttribute("partner", partner);
        return "flight/international/policy/index.base";
    }

    /**
     * 分页查询
     */
    @ResponseBody
    @RequestMapping(value = "/list")
    public ResponseTable list(Integer pageIndex, Integer pageSize, PolicyCondition condition) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<PolicyCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        condition.setTmcId(super.getTmcId());
        queryDto.setCondition(condition);
        Page<InternationalPolicyDto> page = internationalPolicyService.queryListWithPage(queryDto);
        return new ResponseTable(page);
    }

    /**
     * 批量删除
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public ResponseInfo delete(String id) {
        String[] split = id.split(",");
        Integer[] ids = new Integer[split.length];
        for (int i = 0; i < split.length; i++) {
            ids[i] = Integer.parseInt(split[i]);
        }
        try {
            internationalPolicyService.batchDelete(Arrays.asList(ids));
            return new ResponseInfo(SUCCESS, "");
        } catch (Exception e) {
            return new ResponseInfo(FAILED, "");
        }
    }

    /**
     * 批量删除
     *
     * @param id
     * @param status
     * @return
     */
    @ResponseBody
    @RequestMapping("/batchUpdate")
    public ResponseInfo batchUpdateStatus(String id, String status) {
        if (StringUtils.isBlank(id)){
            return new ResponseInfo(SUCCESS, "");
        }
        String[] split = id.split(",");
        Integer[] ids = new Integer[split.length];
        for (int i = 0; i < split.length; i++) {
            ids[i] = Integer.parseInt(split[i]);
        }
        try {
            internationalPolicyService.batchUpdateStatus(Arrays.asList(ids), status, super.getCurrentUser().getId());
            return new ResponseInfo(SUCCESS, "");
        } catch (Exception e) {
            return new ResponseInfo(FAILED, "");
        }
    }

    /**
     * 用于新增和修改时查询对象
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping("/edit")
    public String policyDetail(Model model, Integer id) {
        List<IssueChannelDto> channelList = issueChannelService.findListByTmcIdAndChannelType(super.getTmcId(),
                IssueChannelType.INTERNATIONAL_FLIGHT.name());
        model.addAttribute("channelList", channelList);
        model.addAttribute("id", id);
        return "flight/international/policy/edit.base";
    }

    @RequestMapping("/detail")
    @ResponseBody
    public ResponseInfo policy(Integer id) {
        if (id != null) {
            InternationalPolicyDto policyDto = internationalPolicyService.getById(id);
            UserDto createUser = userService.getUser(policyDto.getCreateUser());
            UserDto lastUpdateUser = userService.getUser(policyDto.getLastUpdateUser());
            Map<String, Object> map = new HashMap<>(3);
            map.put("policy", policyDto);
            map.put("createUser", createUser);
            map.put("lastUpdateUser", lastUpdateUser);
            return new ResponseInfo(SUCCESS, map);
        }
        return new ResponseInfo(FAILED, "id不能为空");
    }

    @ResponseBody
    @RequestMapping(value = "/save")
    public ResponseInfo save(@Validated PolicyForm form, BindingResult bindingResult) {
        try {
            if (bindingResult.hasErrors()) {
                Map<String, String> map = new HashMap<>(4);
                List<FieldError> fieldErrors = bindingResult.getFieldErrors();
                map.put("field", fieldErrors.get(0).getField());
                String fieldName = PolicyForm.mapping().get(fieldErrors.get(0).getField());
                map.put("msg", fieldName + fieldErrors.get(0).getDefaultMessage());
                return new ResponseInfo(FAILED, map);
            }
            InternationalPolicyDto policy = TransformUtils.transform(form, InternationalPolicyDto.class);

            if (policy.getId() == null) {
                policy.setCreateUser(super.getCurrentUser().getId());
                policy.setLastUpdateUser(super.getCurrentUser().getId());
                policy.setStatus(PolicyStatusEnum.EFFECTIVE);
            } else {
                InternationalPolicyDto oldPolicy = internationalPolicyService.getById(policy.getId());
                if (PolicyStatusEnum.ABNORMAL.equals(oldPolicy.getStatus())) {
                    policy.setStatus(PolicyStatusEnum.HANG);
                }else{
                    policy.setStatus(oldPolicy.getStatus());
                }
                policy.setLastUpdateUser(super.getCurrentUser().getId());
                policy.setCreateUser(oldPolicy.getCreateUser());
            }

            policy.setTmcId(super.getTmcId());
            internationalPolicyService.save(policy);
        } catch (Exception e) {
            return new ResponseInfo(FAILED, "");
        }
        return new ResponseInfo(SUCCESS, "");
    }

    @RequestMapping("/uploadPage")
    public String importPage() {
        return "flight/international/policy/uploadPage.base";
    }

    @RequestMapping("/export")
    public void export(HttpServletRequest request, HttpServletResponse response, PolicyCondition condition) {
        try {
            QueryDto<PolicyCondition> queryDto = new QueryDto<>(1, Integer.MAX_VALUE);
            condition.setTmcId(super.getTmcId());
            queryDto.setCondition(condition);
            Page<InternationalPolicyDto> page = internationalPolicyService.queryListWithPage(queryDto);
            List<PolicyForm> data = TransformUtils.transformList(page.getList(), PolicyForm.class);

            List<IssueChannelDto> channelList = issueChannelService.findListByTmcIdAndChannelType(super.getTmcId(),
                    IssueChannelType.INTERNATIONAL_FLIGHT.name());

            String fileName = "国际机票政策.xls";
            request.setCharacterEncoding("UTF-8");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename="
                    + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
            ExportParams exportParams = new ExportParams();
            //供应商名称处理类
            PolicyDataHandler dataHandler = new PolicyDataHandler(channelList);
            dataHandler.setNeedHandlerFields(new String[] { "供应商","乘客类型","旅客资质" });
            exportParams.setDataHandler(dataHandler);
            Workbook workbook = ExcelExportUtil.exportExcel(exportParams, PolicyForm.class, data);
            OutputStream fos = new BufferedOutputStream(response.getOutputStream());
            workbook.write(fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            LogUtils.error(logger, "政策导出异常,e=" + e);
        }
    }

    @RequestMapping("/excelTemplateExport")
    public void excelTemplateExport(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fileName = "国际机票政策模板.xls";
            String path = request.getSession().getServletContext().getRealPath("") + "/resource/excel/" + fileName;
            File file = new File(path);
            // 取得文件名
            String filename = file.getName();
            // 以流的形式下载文件
            InputStream fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @RequestMapping("upload")
    @ResponseBody
    public ResponseInfo upload(HttpServletRequest request, MultipartFile excelFile) {
        if (excelFile == null) {
            return new ResponseInfo(FAILED, "文件不能为空！", null);
        }
        List<IssueChannelDto> channelList = issueChannelService.findListByTmcIdAndChannelType(super.getTmcId(),
                IssueChannelType.INTERNATIONAL_FLIGHT.name());
        List<PolicyForm> excelData = null;
        try {
            ImportParams params = new ImportParams();
            //供应商名称处理类
            PolicyDataHandler dataHandler = new PolicyDataHandler(channelList);
            dataHandler.setNeedHandlerFields(new String[] { "供应商","乘客类型","旅客资质"});
            params.setDataHandler(dataHandler);
            excelData = ExcelImportUtil.importExcel(excelFile.getInputStream(), PolicyForm.class, params);
        } catch (Exception e) {
            LogUtils.error(logger, "excel模板转换异常，异常信息=" + e);
            return new ResponseInfo(FAILED, e.getMessage(), null);
        }

        try {
            List<InternationalPolicyDto> policyDtos = new ArrayList<>();
            List<ErrorMsg> failedList = new ArrayList<>();
            for (int i = 0; i < excelData.size(); i++) {
                PolicyForm form = excelData.get(i);
                List<String> msg = new ArrayList<>();
                Map<String, String> map = new HashMap<>(16);
                MapBindingResult errors = new MapBindingResult(map, "");
                validator.validate(form, errors);
                if (errors.hasErrors()) {
                    for (FieldError fieldError : errors.getFieldErrors()) {
                        msg.add(PolicyForm.mapping().get(fieldError.getField()) + fieldError.getDefaultMessage());
                    }
                }
                if (StringUtils.isNotBlank(form.getFileNum()) && StringUtils.isBlank(form.getCombinationFileNum())) {
                    msg.add("文件编号存在时，可组文件编号不能为空");
                }
                if (form.getTransferPrivate() !=null && form.getTransferPrivate() && form.getRefundChangePercentage() == null) {
                    msg.add("当'是否转私有'为是时,'退改费用'必填");
                }

                if (form.getSupplierId() != null && form.getSupplierId().equals(-1)) {
                    msg.add("供应商不匹配");
                }

                if (CollectionUtils.isNotEmpty(msg)) {
                    failedList.add(new ErrorMsg(i + 2, msg.toString()));
                    form.setStatus(PolicyStatusEnum.ABNORMAL.name());
                } else {
                    form.setStatus(PolicyStatusEnum.EFFECTIVE.name());
                }

                form.setCreateUser(super.getCurrentUser().getId());
                form.setTmcId(super.getTmcId());
                form.setLastUpdateUser(super.getCurrentUser().getId());

                InternationalPolicyDto policyDto = TransformUtils.transform(form, InternationalPolicyDto.class);
                policyDtos.add(policyDto);
            }
            internationalPolicyService.batchAdd(policyDtos);
            return new ResponseInfo(SUCCESS, failedList);
        } catch (Exception e) {
            return new ResponseInfo(FAILED, "导入失败", "");
        }
    }
}
