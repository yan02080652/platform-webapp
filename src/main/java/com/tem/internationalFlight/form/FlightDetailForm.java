package com.tem.internationalFlight.form;

import com.iplatform.common.utils.DateUtils;
import com.tem.flight.dto.aircraft.AircraftDto;
import com.tem.international.interFlight.InternationalFlightSegmentDto;
import com.tem.international.interFlight.InternationalFlightStopInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FlightDetailForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * digital: $(this).find("input[name='digital']").val(),
     * flightNo: $(this).find("input[name='flightNo_" + (i + 1) + "']").val(),
     * share: $(this).find("input[name='share_" + (i + 1) + "']").is(":checked"),
     * realFlightNo: $(this).find("input[name='realFlightNo_" + (i + 1) + "']").val(),
     * cabinCode: $(this).find("input[name='cabinCode_" + (i + 1) + "']").val(),
     * fromDate: $(this).find("input[name='fromDate_" + (i + 1) + "']").val(),
     * toDate: $(this).find("input[name='toDate_" + (i + 1) + "']").val(),
     * fromAirportCode: $(this).find("input[name='fromAirportCode_" + (i + 1) + "']").val(),
     * fromTerminal: $(this).find("input[name='fromTerminal_" + (i + 1) + "']").val(),
     * toAirportCode: $(this).find("input[name='toAirportCode_" + (i + 1) + "']").val(),
     * toTerminal: $(this).find("input[name='toTerminal_" + (i + 1) + "']").val(),
     * fromTime: $(this).find("input[name='fromTime_" + (i + 1) + "']").val(),
     * toTime: $(this).find("input[name='toTime_" + (i + 1) + "']").val(),
     * aircraftType: $(this).find("input[name='aircraftType_" + (i + 1) + "']").val(),
     * stopOver: $(this).find("input[name='stopOver_" + (i + 1) + "']").is(":checked"),
     * stopCityName: $(this).find("input[name='stopCityName_" + (i + 1) + "']").val(),
     * stopTime: $(this).find("input[name='stopTime_" + (i + 1) + "']").val(),
     * <p>
     * baggageType: $(this).find("input[name='baggageType_" + (i + 1) + "']:checked").val(),
     * pieceWeight: $(this).find("select[name='pieceWeight_" + (i + 1) + "'] option:selected").val(),
     * piece: $(this).find("select[name='piece_" + (i + 1) + "'] option:selected").val(),
     * weight: $(this).find("select[name='weight_" + (i + 1) + "'] option:selected").val(),
     * refundRule: $("input[name='refundRule']").val(),
     * changeRule: $("input[name='changeRule']").val(),
     * specialChangeRule: $("input[name='specialChangeRule']").val(),
     * specialRefundRule: $("input[name='specialRefundRule']").val(),
     * turningPoint: $(this).find("select[name='turningPoint_" + (i + 1) + "'] option:selected").val() == 1 ? true : false,
     * baggageDirect: $(this).find("select[name='baggageDirect_" + (i + 1) + "'] option:selected").val() == 1 ? true : false,
     * transitVisa: $(this).find("select[name='transitVisa_" + (i + 1) + "'] option:selected").val() == 1 ? true : false,
     */

    private Integer digital;
    private String flightNo;
    private Boolean share;
    private String realFlightNo;
    private String cabinCode;
    private String fromDate;
    private String toDate;
    private String fromAirportCode;
    private String fromTerminal;
    private String toAirportCode;
    private String toTerminal;
    private String fromTime;
    private String toTime;
    private String aircraftType;
    private Boolean stopOver;
    private String stopCityName;
    private String stopTime;

    //行李类型 1 无 2 件数 3 重量
    private Integer baggageType;

    //type==2
    private Integer pieceWeight;
    private Integer piece;

    //type==3
    private Integer weight;

    //退票规则
    private String refundRule;
    //改签规则
    private String changeRule;
    //特殊重点规则
    private String specialChangeRule;

    private String specialRefundRule;
    //转机点
    private boolean turningPoint;
    //行李直达
    private boolean baggageDirect;
    //过境签
    private boolean transitVisa;

    public InternationalFlightSegmentDto toSegment() {
        InternationalFlightSegmentDto segmentDto = new InternationalFlightSegmentDto();
        segmentDto.setSegmentNo(digital);
        segmentDto.setFlightNo(flightNo);
        segmentDto.setShareFlightNo(share);
        segmentDto.setRealFlightNo(realFlightNo);
        segmentDto.setCabinCode(cabinCode);
        segmentDto.setFromAirport(fromAirportCode);
        segmentDto.setToAirport(toAirportCode);
        segmentDto.setFromTerminal(fromTerminal);
        segmentDto.setToTerminal(toTerminal);
        segmentDto.setFromDate(DateUtils.parse(fromDate + fromTime, "yyyyMMddHHmm"));
        segmentDto.setToDate(DateUtils.parse(toDate + toTime, "yyyyMMddHHmm"));
        AircraftDto aircraftDto = new AircraftDto();
        aircraftDto.setName(aircraftType);
        segmentDto.setAircraftDto(aircraftDto);

        if(stopOver != null && stopOver) {
            List<InternationalFlightStopInfo> stopInfos = new ArrayList<>();
            InternationalFlightStopInfo stopInfo = new InternationalFlightStopInfo();
            stopInfo.setStopTime(stopTime);
            stopInfo.setStopCity(stopCityName);
            segmentDto.setStopInfos(stopInfos);
        }

        segmentDto.setTurningPoint(turningPoint);
        segmentDto.setBaggageDirect(baggageDirect);
        segmentDto.setTransitVisa(transitVisa);

        return segmentDto;
    }

    public Integer getDigital() {
        return digital;
    }

    public void setDigital(Integer digital) {
        this.digital = digital;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public Boolean getShare() {
        return share;
    }

    public void setShare(Boolean share) {
        this.share = share;
    }

    public String getRealFlightNo() {
        return realFlightNo;
    }

    public void setRealFlightNo(String realFlightNo) {
        this.realFlightNo = realFlightNo;
    }

    public String getCabinCode() {
        return cabinCode;
    }

    public void setCabinCode(String cabinCode) {
        this.cabinCode = cabinCode;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getFromAirportCode() {
        return fromAirportCode;
    }

    public void setFromAirportCode(String fromAirportCode) {
        this.fromAirportCode = fromAirportCode;
    }

    public String getFromTerminal() {
        return fromTerminal;
    }

    public void setFromTerminal(String fromTerminal) {
        this.fromTerminal = fromTerminal;
    }

    public String getToAirportCode() {
        return toAirportCode;
    }

    public void setToAirportCode(String toAirportCode) {
        this.toAirportCode = toAirportCode;
    }

    public String getToTerminal() {
        return toTerminal;
    }

    public void setToTerminal(String toTerminal) {
        this.toTerminal = toTerminal;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getAircraftType() {
        return aircraftType;
    }

    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    public Boolean getStopOver() {
        return stopOver;
    }

    public void setStopOver(Boolean stopOver) {
        this.stopOver = stopOver;
    }

    public String getStopCityName() {
        return stopCityName;
    }

    public void setStopCityName(String stopCityName) {
        this.stopCityName = stopCityName;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public Integer getBaggageType() {
        return baggageType;
    }

    public void setBaggageType(Integer baggageType) {
        this.baggageType = baggageType;
    }

    public Integer getPieceWeight() {
        return pieceWeight;
    }

    public void setPieceWeight(Integer pieceWeight) {
        this.pieceWeight = pieceWeight;
    }

    public Integer getPiece() {
        return piece;
    }

    public void setPiece(Integer piece) {
        this.piece = piece;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getRefundRule() {
        return refundRule;
    }

    public void setRefundRule(String refundRule) {
        this.refundRule = refundRule;
    }

    public String getChangeRule() {
        return changeRule;
    }

    public void setChangeRule(String changeRule) {
        this.changeRule = changeRule;
    }

    public String getSpecialChangeRule() {
        return specialChangeRule;
    }

    public void setSpecialChangeRule(String specialChangeRule) {
        this.specialChangeRule = specialChangeRule;
    }

    public String getSpecialRefundRule() {
        return specialRefundRule;
    }

    public void setSpecialRefundRule(String specialRefundRule) {
        this.specialRefundRule = specialRefundRule;
    }

    public boolean isTurningPoint() {
        return turningPoint;
    }

    public void setTurningPoint(boolean turningPoint) {
        this.turningPoint = turningPoint;
    }

    public boolean isBaggageDirect() {
        return baggageDirect;
    }

    public void setBaggageDirect(boolean baggageDirect) {
        this.baggageDirect = baggageDirect;
    }

    public boolean isTransitVisa() {
        return transitVisa;
    }

    public void setTransitVisa(boolean transitVisa) {
        this.transitVisa = transitVisa;
    }
}
