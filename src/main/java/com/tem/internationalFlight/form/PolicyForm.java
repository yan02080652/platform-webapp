package com.tem.internationalFlight.form;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.tem.international.enums.CommissionEnum;
import com.tem.international.enums.FreightOriginEnum;
import com.tem.international.enums.FreightTypeEnum;
import com.tem.international.enums.SceneEnum;
import com.tem.international.enums.TripType;
import com.tem.international.enums.VoucherEnum;
import com.tem.internationalFlight.annotation.EnumValue;
import com.tem.internationalFlight.annotation.IssueTicketLimit;
import com.tem.internationalFlight.annotation.OperatingHours;
import com.tem.internationalFlight.annotation.TimeLimit;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yuanjianxin
 * @date 2018/9/28 9:51
 */
public class PolicyForm{

    private Integer id;

    /**
     * 外部编号
     */
    @Size(max = 50,message = "最长50个字符")
    @Excel(name = "外部编号",orderNum="1")
    private String externalNum;

    /**
     * 文件编号
     */
    @Size(max = 50,message = "最长50个字符")
    @Excel(name = "文件编号",orderNum="2")
    private String fileNum;

    /**
     * 可组文件编号
     */
    @Size(max = 50,message = "最长50个字符")
    @Excel(name = "可组文件编号",orderNum="3")
    private String combinationFileNum;

    /**
     * 行程类型
     */
    @EnumValue(enumClass=TripType.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "行程类型",orderNum="4",replace = {"单程_OW","往返_RT","单程/往返_OWRT"})
    private String airRangeType;

    /**
     * 开票航司
     */
    @NotBlank(message = "必填")
    @Pattern(regexp = "^[0-9a-zA-Z]{2}$",message = "格式不正确")
    @Excel(name = "开票航司",orderNum="5")
    private String billingCarrier;

    /**
     * 出发地
     */
    @Size(max = 500,message = "最长500字符")
    @Pattern(regexp = "^([a-zA-Z0-9]{2,4})(/[a-zA-Z0-9]{2,4})*$",message = "格式不正确")
    @Excel(name = "出发地",orderNum="6")
    private String departure;

    /**
     * 到达地
     */
    @Size(max = 500,message = "最长500字符")
    @Pattern(regexp = "^([a-zA-Z0-9]{2,4})(/[a-zA-Z0-9]{2,4})*$",message = "格式不正确")
    @Excel(name = "到达地",orderNum="7")
    private String destination;

    /**
     * 出发地除外
     */
    @Size(max = 500,message = "最长500字符")
    @Pattern(regexp = "^([a-zA-Z0-9]{2,4})(/[a-zA-Z0-9]{2,4})*$",message = "格式不正确")
    @Excel(name = "出发地除外",orderNum="8")
    private String excludeDeparture;

    /**
     * 到达地除外
     */
    @Size(max = 500,message = "最长500字符")
    @Pattern(regexp = "^([a-zA-Z0-9]{2,4})(/[a-zA-Z0-9]{2,4})*$",message = "格式不正确")
    @Excel(name = "到达地除外",orderNum="9")
    private String excludeDestination;

    /**
     * 适用舱位
     */
    @Size(max = 100,message = "最长100字符")
    @Pattern(regexp = "^([a-zA-Z0-9]{1,2})(/[a-zA-Z0-9]{1,2})*$",message = "格式不正确")
    @Excel(name = "适用舱位",orderNum="10")
    private String cabinCodes;

    /**
     * 是否适用联运
     */
    @NotNull(message = "必填")
    @Excel(name = "是否适用联运",orderNum="11",replace = {"是_true","否_false"})
    private Boolean intermodal;

    /**
     * 联运航司编码
     */
    @Size(max = 100,message = "最长100字符")
    @Pattern(regexp = "^([0-9a-zA-Z]{2})(/[0-9a-zA-Z]{2}){0,19}$",message = "格式不正确")
    @Excel(name = "联运航司编码",orderNum="12")
    private String intermodalCarrierCodes;

    /**
     * 除外联运航司编码
     */
    @Size(max = 100,message = "最长100字符")
    @Pattern(regexp = "^([0-9a-zA-Z]{2})(/[0-9a-zA-Z]{2}){0,19}$",message = "格式不正确")
    @Excel(name = "除外联运航司编码",orderNum="13")
    private String excludeIntermodalCarrierCodes;

    /**
     * 是否适用中转 0不适用,1适用,-1不限
     */
    @NotBlank(message = "必填")
    @EnumValue(enumClass=SceneEnum.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "是否适用中转",orderNum="14",replace = {"是_1","否_0","不限_-1"})
    private String transfer;

    /**
     * 指定转机点
     */
    @Size(max = 500,message = "最长500个字符")
    @Pattern(regexp = "^([a-zA-Z]{3})(/[a-zA-Z0-9]{3})*$",message = "格式不正确")
    @Excel(name = "指定转机点",orderNum="15")
    private String transitAirport;

    /**
     * 是否适用代码共享航班 0不适用,1适用,-1不限
     */
    @NotBlank(message = "必填")
    @EnumValue(enumClass=SceneEnum.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "是否适用代码共享航班",orderNum="16",replace = {"是_1","否_0","不限_-1"})
    private String shareAirline;

    /**
     * 是否允许缺口 0不适用,1适用,-1不限
     */
    @NotBlank(message = "必填")
    @EnumValue(enumClass=SceneEnum.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "是否允许缺口",orderNum="17",replace = {"是_1","否_0","不限_-1"})
    private String gap;

    /**
     * 运价类型
     */
    @NotNull(message = "必填")
    @EnumValue(enumClass=FreightTypeEnum.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "运价类型",orderNum="18")
    private String freightType;

    /**
     * 报销凭证
     */
    @NotNull(message = "必填")
    @EnumValue(enumClass=VoucherEnum.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "报销凭证",orderNum="19",replace = {"旅游发票_TOURISTINVOICE","行程单_ITINERARY"})
    private String reimbursementVoucher;

    /**
     * 去程旅行日期
     */
    @Size(max = 500,message = "最长500字符")
    @Pattern(regexp = "^[0-9]{4}-[0-9]{2}-[0-9]{2}>[0-9]{4}-[0-9]{2}-[0-9]{2}(/[0-9]{4}-[0-9]{2}-[0-9]{2}>[0-9]{4}-[0-9]{2}-[0-9]{2})*$",message = "格式不正确")
    @TimeLimit
    @Excel(name = "去程旅行日期",orderNum="20")
    private String departureDate;

    /**
     * 回程旅行日期
     */
    @Size(max = 500,message = "最长500字符")
    @Pattern(regexp = "^[0-9]{4}-[0-9]{2}-[0-9]{2}>[0-9]{4}-[0-9]{2}-[0-9]{2}(/[0-9]{4}-[0-9]{2}-[0-9]{2}>[0-9]{4}-[0-9]{2}-[0-9]{2})*$",message = "格式不正确")
    @TimeLimit
    @Excel(name = "回程旅行日期",orderNum="21")
    private String destinationDate;

    /**
     * 销售日期
     */
    @NotBlank(message = "必填")
    @Pattern(regexp = "^[0-9]{4}-[0-9]{2}-[0-9]{2}>[0-9]{4}-[0-9]{2}-[0-9]{2}$",message = "格式不正确")
    @TimeLimit
    @Excel(name = "销售日期",orderNum="22")
    private String saleDate;


    /**
     * 返点
     */
    @NotNull(message = "必填")
    @Digits(fraction = 2,integer = 2,message = "格式不正确")
    @Range(min = 0,max = 99,message = "超出有效值区间")
    @Excel(name = "返点",orderNum="23")
    private Double rebate;

    /**
     * 留钱
     */
    @NotNull(message = "必填")
    @Excel(name = "留钱",orderNum="24")
    private Integer keepMoney;

    /**
     * 备注
     */
    @Size(max = 200,message = "最长200字符")
    @Excel(name = "备注",orderNum="25")
    private String remark;

    /**
     * 工作时间
     */
    @Size(max = 100,message = "最长100字符")
    @OperatingHours
    @Excel(name = "工作时间",orderNum="26")
    private String operatingHours;

    /**
     * 适用乘客国籍
     */
    @Size(max = 100,message = "最长100字符")
    @Pattern(regexp = "^([a-zA-Z]{2})(/[a-zA-Z]{2})*$",message = "格式不正确")
    @Excel(name = "适用乘客国籍",orderNum="27")
    private String passengerNationality;

    /**
     * 除外适用乘客国籍
     */
    @Size(max = 100,message = "最长100字符")
    @Pattern(regexp = "^([a-zA-Z]{2})(/[a-zA-Z]{2})*$",message = "格式不正确")
    @Excel(name = "除外适用乘客国籍",orderNum="28")
    private String excludePassengerNationality;

    /**
     * 佣金计算方式
     */
    @NotNull(message = "必填")
    @EnumValue(enumClass=CommissionEnum.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "佣金计算方式",orderNum="29",replace = {"各取各_EACH","取严_STRICT"})
    private String commissionCalculationMethod;

    /**
     * 出票时限
     */
    @Size(max = 30,message = "最长30字符")
    @Pattern(regexp = "^([0-9]{1,3}-[0-9]{1,3}(,[0-9]{1,3}){2})(/[0-9]{1,3}-[0-9]{1,3}(,[0-9]{1,3}){2})*$",message = "格式不正确")
    @IssueTicketLimit
    @Excel(name = "出票时限",orderNum="30")
    private String timeLimitTicketing;

    /**
     * 出票城市
     */
    @Size(max = 3,message = "最长3字符")
    @Pattern(regexp = "^[a-zA-Z]{3}$",message = "格式不正确")
    @Excel(name = "出票城市",orderNum="31")
    private String ticketingCity;

    /**
     * 是否公布转私有
     */
    @NotNull(message = "必填")
    @Excel(name = "是否公布转私有",orderNum="32",replace = {"是_true","否_false"})
    private Boolean transferPrivate;

    /**
     * 退改费用百分比
     */
    @Range(max = 60,message = "超出有效值区间")
    @Excel(name = "退改费用百分比",orderNum="33")
    private Integer refundChangePercentage;

    /**
     * 是否适用小团体 0不适用,1适用,-1不限
     */
    @EnumValue(enumClass=SceneEnum.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "是否适用小团体",orderNum="34",replace = {"是_1","否_0","不限_-1"})
    private String smallGroup;

    /**
     * 是否适用文件公布运价 0不适用,1适用,-1不限
     */
    @EnumValue(enumClass=SceneEnum.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "是否适用文件公布运价",orderNum="35",replace = {"是_1","否_0","不限_-1"})
    private String publicFreight;

    /**
     * 可使用运价来源 0公布运价,1私有运价,-1不限
     */
    @EnumValue(enumClass=FreightOriginEnum.class, enumMethod="isValidName",message = "格式不正确")
    @Excel(name = "可使用运价来源",orderNum="34",replace = {"私有运价_1","公布运价_0","不限_-1"})
    private String freightOrigin;

    /**
     * 供应商编码
     */
    @Excel(name = "供应商",orderNum="35")
    private Integer supplierId;

    /**
     * 乘客类型
     */
    @Excel(name = "乘客类型",orderNum="36")
    private String passengerType;

    /**
     * 旅客资质
     */
    @Excel(name = "旅客资质",orderNum="37")
    private String passengerQualification;

    /**
     * 状态
     */
    private String status;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 所属tmc
     */
    private Long tmcId;

    /**
     * 最后修改时间
     */
    private Date lastUpdateTime;

    /**
     * 最后修改人
     */
    private Long lastUpdateUser;

    public static Map<String,String> mapping(){
        Map<String, String> map = new HashMap<>(40);
        map.put("externalNum", "外部编号");
        map.put("fileNum", "文件编号");
        map.put("combinationFileNum", "可组文件编号");
        map.put("airRangeType", "行程类型");
        map.put("billingCarrier", "开票航司");
        map.put("departure", "出发地");
        map.put("destination", "到达地");
        map.put("excludeDeparture", "出发地除外");
        map.put("excludeDestination", "到达地除外");
        map.put("cabinCodes", "适用舱位");
        map.put("intermodal", "是否可联运");
        map.put("intermodalCarrierCodes", "联运航司编码");
        map.put("excludeIntermodalCarrierCodes", "除外联运航司编码");
        map.put("transfer", "是否适用中转");
        map.put("transitAirport", "指定转机点");
        map.put("shareAirline", "是否适用代码共享航班");
        map.put("gap", "是否允许缺口");
        map.put("freightType", "运价类型");
        map.put("reimbursementVoucher", "报销凭证");
        map.put("departureDate", "去程旅行日期");
        map.put("destinationDate", "回程旅行日期");
        map.put("saleDate", "销售日期");
        map.put("passengerType", "乘客类型");
        map.put("passengerQualification", "旅客资质");
        map.put("rebate", "返点");
        map.put("keepMoney", "留钱");
        map.put("remark", "备注");
        map.put("operatingHours", "工作时间");
        map.put("passengerNationality", "适用乘客国籍");
        map.put("excludePassengerNationality", "除外适用乘客国籍");
        map.put("commissionCalculationMethod", "佣金计算方式");
        map.put("timeLimitTicketing", "出票时限");
        map.put("ticketingCity", "出票城市");
        map.put("transferPrivate", "是否公转私");
        map.put("refundChangePercentage", "退改费用百分比");
        map.put("smallGroup", "是否适用小团体");
        map.put("publicFreight", "是否适用文件公布运价");
        map.put("freightOrigin", "可使用运价来源");
        map.put("supplierCode", "供应商");
        return map;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExternalNum() {
        return externalNum;
    }

    public void setExternalNum(String externalNum) {
        this.externalNum = externalNum;
    }

    public String getFileNum() {
        return fileNum;
    }

    public void setFileNum(String fileNum) {
        this.fileNum = fileNum;
    }

    public String getCombinationFileNum() {
        return combinationFileNum;
    }

    public void setCombinationFileNum(String combinationFileNum) {
        this.combinationFileNum = combinationFileNum;
    }

    public String getAirRangeType() {
        return airRangeType;
    }

    public void setAirRangeType(String airRangeType) {
        this.airRangeType = airRangeType;
    }

    public String getBillingCarrier() {
        return billingCarrier;
    }

    public void setBillingCarrier(String billingCarrier) {
        this.billingCarrier = billingCarrier;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getExcludeDeparture() {
        return excludeDeparture;
    }

    public void setExcludeDeparture(String excludeDeparture) {
        this.excludeDeparture = excludeDeparture;
    }

    public String getExcludeDestination() {
        return excludeDestination;
    }

    public void setExcludeDestination(String excludeDestination) {
        this.excludeDestination = excludeDestination;
    }

    public String getCabinCodes() {
        return cabinCodes;
    }

    public void setCabinCodes(String cabinCodes) {
        this.cabinCodes = cabinCodes;
    }

    public String getIntermodalCarrierCodes() {
        return intermodalCarrierCodes;
    }

    public void setIntermodalCarrierCodes(String intermodalCarrierCodes) {
        this.intermodalCarrierCodes = intermodalCarrierCodes;
    }

    public String getExcludeIntermodalCarrierCodes() {
        return excludeIntermodalCarrierCodes;
    }

    public void setExcludeIntermodalCarrierCodes(String excludeIntermodalCarrierCodes) {
        this.excludeIntermodalCarrierCodes = excludeIntermodalCarrierCodes;
    }

    public String getTransfer() {
        return transfer;
    }

    public void setTransfer(String transfer) {
        this.transfer = transfer;
    }

    public String getTransitAirport() {
        return transitAirport;
    }

    public void setTransitAirport(String transitAirport) {
        this.transitAirport = transitAirport;
    }

    public String getShareAirline() {
        return shareAirline;
    }

    public void setShareAirline(String shareAirline) {
        this.shareAirline = shareAirline;
    }

    public String getGap() {
        return gap;
    }

    public void setGap(String gap) {
        this.gap = gap;
    }

    public String getFreightType() {
        return freightType;
    }

    public void setFreightType(String freightType) {
        this.freightType = freightType;
    }

    public String getReimbursementVoucher() {
        return reimbursementVoucher;
    }

    public void setReimbursementVoucher(String reimbursementVoucher) {
        this.reimbursementVoucher = reimbursementVoucher;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDestinationDate() {
        return destinationDate;
    }

    public void setDestinationDate(String destinationDate) {
        this.destinationDate = destinationDate;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }

    public String getPassengerQualification() {
        return passengerQualification;
    }

    public void setPassengerQualification(String passengerQualification) {
        this.passengerQualification = passengerQualification;
    }

    public Double getRebate() {
        return rebate;
    }

    public void setRebate(Double rebate) {
        this.rebate = rebate;
    }

    public Integer getKeepMoney() {
        return keepMoney;
    }

    public void setKeepMoney(Integer keepMoney) {
        this.keepMoney = keepMoney;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOperatingHours() {
        return operatingHours;
    }

    public void setOperatingHours(String operatingHours) {
        this.operatingHours = operatingHours;
    }

    public String getPassengerNationality() {
        return passengerNationality;
    }

    public void setPassengerNationality(String passengerNationality) {
        this.passengerNationality = passengerNationality;
    }

    public String getExcludePassengerNationality() {
        return excludePassengerNationality;
    }

    public void setExcludePassengerNationality(String excludePassengerNationality) {
        this.excludePassengerNationality = excludePassengerNationality;
    }

    public String getCommissionCalculationMethod() {
        return commissionCalculationMethod;
    }

    public void setCommissionCalculationMethod(String commissionCalculationMethod) {
        this.commissionCalculationMethod = commissionCalculationMethod;
    }

    public String getTimeLimitTicketing() {
        return timeLimitTicketing;
    }

    public void setTimeLimitTicketing(String timeLimitTicketing) {
        this.timeLimitTicketing = timeLimitTicketing;
    }

    public String getTicketingCity() {
        return ticketingCity;
    }

    public void setTicketingCity(String ticketingCity) {
        this.ticketingCity = ticketingCity;
    }

    public Integer getRefundChangePercentage() {
        return refundChangePercentage;
    }

    public void setRefundChangePercentage(Integer refundChangePercentage) {
        this.refundChangePercentage = refundChangePercentage;
    }

    public String getSmallGroup() {
        return smallGroup;
    }

    public void setSmallGroup(String smallGroup) {
        this.smallGroup = smallGroup;
    }

    public String getPublicFreight() {
        return publicFreight;
    }

    public void setPublicFreight(String publicFreight) {
        this.publicFreight = publicFreight;
    }

    public String getFreightOrigin() {
        return freightOrigin;
    }

    public void setFreightOrigin(String freightOrigin) {
        this.freightOrigin = freightOrigin;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getTmcId() {
        return tmcId;
    }

    public void setTmcId(Long tmcId) {
        this.tmcId = tmcId;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Long getLastUpdateUser() {
        return lastUpdateUser;
    }

    public void setLastUpdateUser(Long lastUpdateUser) {
        this.lastUpdateUser = lastUpdateUser;
    }

    public Boolean getIntermodal() {
        return intermodal;
    }

    public void setIntermodal(Boolean intermodal) {
        this.intermodal = intermodal;
    }

    public Boolean getTransferPrivate() {
        return transferPrivate;
    }

    public void setTransferPrivate(Boolean transferPrivate) {
        this.transferPrivate = transferPrivate;
    }

}
