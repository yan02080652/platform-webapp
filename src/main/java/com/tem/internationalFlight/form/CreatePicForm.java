package com.tem.internationalFlight.form;

import com.tem.international.interFlight.InternationalOfferDto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class CreatePicForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * title : $("#partnerName").val(),
     *                     singleTicketPrice: $("input[name='singleTicketPrice']").val(),
     *                     singleTaxFee: $("input[name='singleTaxFee']").val(),
     *                     singleSettelPrice: $("input[name='singleSettelPrice']").val(),
     *                     singleSettelTaxFee: $("input[name='singleSettelTaxFee']").val(),
     *                     singleExtraFee: $("input[name='singleExtraFee']").val(),
     *
     *                     tripType: $("input[name='tripType']:checked").val(),
     *                     detailDtos: details,
     */

    private String title;

    private BigDecimal singleTicketPrice;
    private BigDecimal singleTaxFee;
    private BigDecimal singleExtraFee;

    //结算价暂时无用
    private BigDecimal singleSettelPrice;
    private BigDecimal singleSettelTaxFee;
    private String tripType;

    private List<FlightDetailForm> detailDtos;

    public InternationalOfferDto toOfferDto(){
        InternationalOfferDto dto = new InternationalOfferDto();

        dto.setTitle(title);
        dto.setSingleExtraFee(singleExtraFee);
        dto.setSingleTaxFee(singleTaxFee);
        dto.setSingleTicketPrice(singleTicketPrice);

        for (FlightDetailForm detailDto : detailDtos) {
            dto.getSegmentDtos().add(detailDto.toSegment());
        }
        dto.setRefundRule(detailDtos.get(0).getRefundRule());
        dto.setChangeRule(detailDtos.get(0).getChangeRule());
        dto.setSpecialChangeRule(detailDtos.get(0).getSpecialChangeRule());
        dto.setSpecialRefundRule(detailDtos.get(0).getSpecialRefundRule());
        return dto;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getSingleTicketPrice() {
        return singleTicketPrice;
    }

    public void setSingleTicketPrice(BigDecimal singleTicketPrice) {
        this.singleTicketPrice = singleTicketPrice;
    }

    public BigDecimal getSingleTaxFee() {
        return singleTaxFee;
    }

    public void setSingleTaxFee(BigDecimal singleTaxFee) {
        this.singleTaxFee = singleTaxFee;
    }

    public BigDecimal getSingleExtraFee() {
        return singleExtraFee;
    }

    public void setSingleExtraFee(BigDecimal singleExtraFee) {
        this.singleExtraFee = singleExtraFee;
    }

    public BigDecimal getSingleSettelPrice() {
        return singleSettelPrice;
    }

    public void setSingleSettelPrice(BigDecimal singleSettelPrice) {
        this.singleSettelPrice = singleSettelPrice;
    }

    public BigDecimal getSingleSettelTaxFee() {
        return singleSettelTaxFee;
    }

    public void setSingleSettelTaxFee(BigDecimal singleSettelTaxFee) {
        this.singleSettelTaxFee = singleSettelTaxFee;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public List<FlightDetailForm> getDetailDtos() {
        return detailDtos;
    }

    public void setDetailDtos(List<FlightDetailForm> detailDtos) {
        this.detailDtos = detailDtos;
    }
}
