package com.tem.train;

import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.action.form.RefundAduitForm;
import com.tem.action.form.TicketFrom;
import com.tem.payment.api.BpAccTradeRecordService;
import com.tem.payment.api.PaymentPlanItemService;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.OrgService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.security.authorize.User;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.api.task.OrderTaskService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.refund.RefundOrderDto;
import com.tem.pss.dto.refund.RefundPassengerDto;
import com.tem.pss.dto.task.OrderTaskCondition;
import com.tem.pss.dto.task.OrderTaskDto;
import com.tem.pss.dto.task.TaskParamDto;
import com.tem.pss.dto.task.TaskStatus;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import com.tem.train.api.payment.TrainOrderPaymentService;
import com.tem.train.api.order.TrainOrderService;
import com.tem.train.dto.order.*;
import com.tem.train.dto.order.enums.OrderShowStatus;
import com.tem.train.dto.order.enums.OrderStatus;
import com.tem.train.dto.order.enums.ProcessTagType;
import com.tem.train.dto.order.enums.TicketStatus;
import com.tem.train.form.ChangePassengerForm;
import com.tem.train.form.OffLineChangeForm;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.*;

@Controller
@RequestMapping("train/tmc")
public class TrainTmcController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(TrainTmcController.class);

	@Autowired
	private DictService dictService;
	@Autowired
	private OrderTaskService orderTaskService;
	@Autowired
	private UserService userService;
	@Autowired
	private OrgService orgService;
	@Autowired
	private PaymentPlanItemService paymentPlanItemService;
	@Autowired
	private RefundOrderService refundOrderService;
	@Autowired
	private TrainOrderService trainOrderService;
	@Autowired
	private TrainOrderPaymentService trainOrderPaymentService;
	@Autowired
	private PartnerService partnerService;
	@Autowired
	private BpAccTradeRecordService bpAccTradeRecordService;
	@Autowired
	private IssueChannelService issueChannelService;

	@RequestMapping("index")
	public String index(Model model){
		QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(1,10);
		OrderTaskCondition condition = new OrderTaskCondition();
		condition.setTaskStatus(TaskStatus.PROCESSING.toString());
		condition.setOrderBizType(OrderBizType.TRAIN.name());
		condition.setOperatorId(super.getCurrentUser().getId().intValue());
		condition.setTmcId(super.getTmcId().toString());
		queryDto.setCondition(condition);

		Page<OrderTaskDto> pageList = trainOrderService.orderTaskPageListQuery(queryDto);
		if (CollectionUtils.isNotEmpty(pageList.getList())) {
			TrainOrderDto order = null;
			PartnerDto partner = null;
			for (OrderTaskDto task: pageList.getList()) {
				order = (TrainOrderDto) task.getOrderDto();
				if (order == null || StringUtils.isBlank(order.getcName())) {
					continue;
				}
				partner = partnerService.findById(super.getPartnerId());
				if (partner != null) {
					order.setcName(partner.getName());
				}
			}
		}

		this.getStatusAndTypeCount(super.getCurrentUser().getId().intValue(), model);
		model.addAttribute("pageList", pageList);
		model.addAttribute("issueChannelMap", getIssueChannelType());
		return "train/tmc/index.jsp";
	}

	/**
	 * 获取各状态下各个类型的任务数量
	 * @param
	 */
	private void getStatusAndTypeCount(Integer userId, Model model){
		OrderTaskCondition condition = new OrderTaskCondition();
		condition.setOrderBizType(OrderBizType.TRAIN.name());
		condition.setOperatorId(userId);
		condition.setTmcId(super.getTmcId().toString());

		condition.setTaskStatus(TaskStatus.PROCESSING.toString());
		// 处理中任务数量（包含各状态的数量）
		Map<String, Integer> processingMap = orderTaskService.getTaskCountMap(condition);

		condition.setTaskStatus(TaskStatus.ALREADY_PROCESSED.toString());
		// 已处理任务数量（包含各状态的数量）
		Map<String, Integer> alreadyMap = orderTaskService.getTaskCountMap(condition);

		// 还未领取的任务(包含各状态的数量)
		condition.setTaskStatus(TaskStatus.WAIT_PROCESS.toString());
		condition.setOperatorId(null);
		Map<String, Integer> waitTaskTypeMap = orderTaskService.getTaskCountMap(condition);

		model.addAttribute("processingMap",processingMap);
		model.addAttribute("alreadyMap",alreadyMap);
		model.addAttribute("waitTaskTypeMap",waitTaskTypeMap);
	}


	//全部任务
	@RequestMapping("/allTask")
	public String allTask(Integer pageIndex,String taskStatus,String taskType,String keyword, Model model){
		if(pageIndex==null){
			pageIndex=1;
		}
		if(taskStatus==null){
			taskStatus = TaskStatus.WAIT_PROCESS.toString();
		}

		QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex,10);
		OrderTaskCondition condition = new OrderTaskCondition();
		condition.setTaskStatus(taskStatus);
		condition.setTaskType(taskType);
		condition.setOperatorId(null);
		condition.setOrderBizType(OrderBizType.TRAIN.name());
		condition.setTmcId(super.getTmcId().toString());
		if(StringUtils.isNotBlank(keyword)){
			condition.setKeyword(keyword);
		}
		queryDto.setCondition(condition);

		Page<OrderTaskDto> pageList = trainOrderService.orderTaskPageListQuery(queryDto);

		this.getStatusAndTypeCount(null, model);
		model.addAttribute("pageList", pageList);
		model.addAttribute("taskStatus", taskStatus);
		model.addAttribute("issueChannelMap", getIssueChannelType());
		return "train/tmc/allTask.jsp";
	}

	//订单查询异步加载的数据
	@RequestMapping("/getOrderTable")
	public String getOrderTable(Integer pageIndex, Integer pageSize, Long startDate1,
								Long endDate1, TrainOrderCondition condition, Model model){
		if(startDate1 != null){
			condition.setStartDate(DateUtils.getDayBegin(new Date(startDate1)));
		}
		if(endDate1 != null){
			condition.setEndDate(DateUtils.getDayEnd(new Date(endDate1)));
		}
		Page<TrainOrderDto> pageList = getPageList(pageIndex,pageSize,condition);

		QueryDto<TrainOrderCondition> queryDto = new QueryDto<>();
		condition.setOrderShowStatus(null);
		queryDto.setCondition(condition);
		List<OrderCountDto> orderStatuslist = getOrderCountList(queryDto);

		model.addAttribute("pageList", pageList);
		model.addAttribute("orderStatuslist", orderStatuslist);
		model.addAttribute("issueChannelMap", getIssueChannelType());
		model.addAttribute("currentOrderStatus", condition.getOrderShowStatus()!=null?condition.getOrderShowStatus()[0]:null);

		return "train/tmc/orderTable.jsp";
	}

	/**
	 * 查询各个状态的订单数量
	 * @param queryDto
	 * @return
	 */
	private List<OrderCountDto> getOrderCountList(QueryDto<TrainOrderCondition> queryDto) {
		List<OrderCountDto> orderCountList = trainOrderService.findOrderCountList(queryDto);

		List<OrderCountDto> list = new ArrayList<OrderCountDto>();
		OrderShowStatus[] values = OrderShowStatus.values();

		for(OrderShowStatus orderShowStatus : values){
			OrderCountDto orderCountDto = new OrderCountDto(orderShowStatus,0);
			for(OrderCountDto countDto : orderCountList){
				if(orderShowStatus.equals(countDto.getOrderShowStatus())){
					orderCountDto.setCount(countDto.getCount());
					break;
				}
			}

			list.add(orderCountDto);
		}
		return list;
	}

	//我的任务
	@RequestMapping("/getMyTaskTable")
	public String getMyTaskTable(Integer pageIndex,String taskStatus,String taskType, Model model){
		if(pageIndex==null){
			pageIndex=1;
		}
		if(taskStatus==null){
			taskStatus = TaskStatus.PROCESSING.toString();
		}
		QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex,10);

		OrderTaskCondition condition = new OrderTaskCondition();
		condition.setTaskStatus(taskStatus);
		condition.setTaskType(taskType);
		condition.setOperatorId(super.getCurrentUser().getId().intValue());
		condition.setOrderBizType(OrderBizType.TRAIN.name());
		condition.setTmcId(super.getTmcId().toString());
		queryDto.setCondition(condition);

		Page<OrderTaskDto> pageList = trainOrderService.orderTaskPageListQuery(queryDto);

		this.getStatusAndTypeCount(super.getCurrentUser().getId().intValue(), model);

		model.addAttribute("pageList", pageList);
		model.addAttribute("taskStatus", taskStatus);
		model.addAttribute("issueChannelMap", getIssueChannelType());
		return "train/tmc/myTask.jsp";
	}

	//任务交接
	@ResponseBody
	@RequestMapping("/transfer")
	public boolean transfer(Integer taskId, Integer operatorId, Integer userId, String userName){
		LogUtils.info(logger, "tmc任务交接，入参:taskId={},operatorId={},userId={},userName={}", taskId, operatorId, userId, userName);
		if(operatorId == null){
			operatorId = super.getCurrentUser().getId().intValue();
		}
		TaskParamDto dto = new TaskParamDto();
		dto.setTaskId(taskId);
		dto.setOperatorId(operatorId);
		dto.setUserId(userId);
		dto.setUserName(userName);
		dto.setRemork(super.getCurrentUser().getFullname()+"("+operatorId+")将任务交接给"+userName+"("+userId+"),任务id:"+taskId);
		dto.setOrderBizType(OrderBizType.TRAIN.name());
		dto.setTaskStatus(TaskStatus.PROCESSING);

		boolean rs = orderTaskService.saveTaskAndLog(dto);
		LogUtils.info(logger, "tmc任务交接返回结果{}", rs);
		return rs;
	}

	//获取订单日志
	@RequestMapping("/getOrderLog")
	public String getOrderLog(String orderId, Model model){
		List<OrderOperationLogDto> logList = trainOrderService.findOrderLogList(orderId);
		model.addAttribute("logList", logList);
		return "train/tmc/log.single";
	}

	@RequestMapping("/getTicketData")
	public String getTicketData(String orderId, Integer taskId, String flag, Boolean isRevise, Model model){
		TrainOrderDto trainOrderDto = trainOrderService.getTrainOrderById(orderId);
		if (StringUtils.isBlank(trainOrderDto.getcName())) {
			trainOrderDto.setcName(partnerService.findById(super.getPartnerId()).getName());
		}
		model.addAttribute("trainOrderDto", trainOrderDto);
		model.addAttribute("issueChannelMap", getIssueChannelType());
		model.addAttribute("taskId", taskId);
		model.addAttribute("orderId", orderId);
		model.addAttribute("flag", flag);
		model.addAttribute("isRevise",isRevise);
		return "train/tmc/ticket.jsp";
	}


	//向19e重新支付
	@ResponseBody
	@RequestMapping("/payAgainForSupplier")
	public ResponseInfo payAgainForSupplier(String orderId,Integer taskId){
		TaskParamDto dto = new TaskParamDto();
		dto.setOrderId(orderId);
		dto.setTaskId(taskId);
		dto.setRemork("向供应商支付");
		dto.setOperatorName(super.getCurrentUser().getFullname());
		dto.setOperatorId(Integer.valueOf(super.getCurrentUser().getId()+""));
		return trainOrderPaymentService.payAgainForSupplier(dto);
	}


	//加载收入明细
	@RequestMapping("/getIncomeDetail")
	public String getIncomeDetail(String orderId, Model model) {
		TrainOrderDto orderDto = trainOrderService.getTrainOrderById(orderId);
		model.addAttribute("incomeDetail",
				bpAccTradeRecordService.listByOrderIdAndPlanId(orderDto.getId(), Long.valueOf(orderDto.getPaymentPlanNo())));
		return "train/tmc/incomeDetail.single";
	}


	/**
	 * 修改任务超时时间
	 * @param taskId 任务id
	 * @param delay 延时时间：分钟
	 */
	@RequestMapping("/updateMaxProcessDate")
	public void updateMaxProcessDate(Integer taskId,Integer delay){
		OrderTaskDto dto = new OrderTaskDto();
		dto.setId(taskId);
		dto.setMaxProcessDate(DateUtils.addMinute(new Date(), delay));
		dto.setTaskStatus(TaskStatus.PROCESSING);
		orderTaskService.update(dto);
	}


	/**
	 * 判断操作人是否为任务领取人
	 * @param operatorId 操作人id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/checkOperator")
	public boolean checkOperator(String operatorId){
		Long userId = super.getCurrentUser().getId();
		if(userId.toString().equals(operatorId)){
			return true;
		}
		return false;
	}


	//订单查询页面
	@RequestMapping("/orderList")
	public String orderList(Model model){
		QueryDto<TrainOrderCondition> queryDto = new QueryDto<TrainOrderCondition>();
		TrainOrderCondition condition = new TrainOrderCondition();
		condition.setTmcId(super.getTmcId());
		queryDto.setCondition(condition);
		List<OrderCountDto> orderStatuslist = getOrderCountList(queryDto);

		Page<TrainOrderDto> pageList  = getPageList(null,null,new TrainOrderCondition());

		model.addAttribute("orderStatuslist", orderStatuslist);
		model.addAttribute("pageList", pageList);
		model.addAttribute("issueChannelMap", getIssueChannelType());
		return "train/tmc/orderList.jsp";
	}

	private Map<String, Map<String, String>> getIssueChannelType() {
		Map<String, Map<String, String>> map = new HashMap<>(16);

		List<IssueChannelDto> onLineList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
				IssueChannelType.TRAIN.getType(), LineType.ON_LINE.getType());
		if (!CollectionUtils.isEmpty(onLineList)) {
			Map<String, String> onLineMap = new HashMap<>(16);
			for (IssueChannelDto channel: onLineList) {
				onLineMap.put(channel.getCode(), channel.getName());
			}
			map.put("0", onLineMap);
		}

		List<IssueChannelDto> offLineList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
				IssueChannelType.TRAIN.getType(), LineType.OFF_LINE.getType());
		if (!CollectionUtils.isEmpty(offLineList)) {
			Map<String, String> offLineMap = new HashMap<>(16);
			for (IssueChannelDto channel: offLineList) {
				offLineMap.put(channel.getCode(), channel.getName());
			}
			map.put("1", offLineMap);
		}

		return map;
	}

	/**
	 * 分页数据
	 * @param pageIndex
	 * @param pageSize
	 * @param condition
	 * @return
	 */
	private Page<TrainOrderDto> getPageList(Integer pageIndex,Integer pageSize,TrainOrderCondition condition){
		if(pageIndex==null){
			pageIndex = 1;
		}
		if(pageSize==null){
			pageSize = 10;
		}
		condition.setTmcId(super.getTmcId());
		QueryDto<TrainOrderCondition> queryDto = new QueryDto<TrainOrderCondition>(pageIndex,pageSize);
		queryDto.setCondition(condition);
		Page<TrainOrderDto> page = trainOrderService.findOrderList(queryDto);
		if (CollectionUtils.isNotEmpty(page.getList())) {
			for (TrainOrderDto order: page.getList()) {
				if (StringUtils.isBlank(order.getcName())) {
					// 设置企业名称
					order.setcName(partnerService.findById(Long.valueOf(order.getcId())).getName());
				}

				//任务
				List<OrderTaskDto> inOrWaitTaskList = orderTaskService.findTaskByStatusAndBizType(TaskStatus.WAIT_PROCESS, OrderBizType.TRAIN);
				inOrWaitTaskList.addAll(orderTaskService.findTaskByStatusAndBizType(TaskStatus.PROCESSING, OrderBizType.TRAIN));
				for (OrderTaskDto t: inOrWaitTaskList) {
					if (order.getId().equals(t.getOrderId())) {
						String operatorName = t.getOperatorName() == null ? "" : "/"+t.getOperatorName();
						order.setTaskInfo( t.getTaskStatus().getMessage() + "/" + t.getTaskType().getMessage() + operatorName);
						break;
					}
				}
			}
		}

		return page;
	}


	//拒单退款
	@ResponseBody
	@RequestMapping("/rejectOrder")
	public ResponseInfo rejectOrder(String orderId, Integer taskId, String userId, String remark){
		//修改信息
		TrainOrderDto dto = trainOrderService.getTrainOrderById(orderId);
		dto.setUserId(userId);
		dto.setRemark(remark);
		dto.setOrderStatus(OrderStatus.ISSUE_FAIL_CANCEL);
		dto.setProcessTagType(ProcessTagType.NULL);

		OrderTaskDto taskDto = new OrderTaskDto();
		taskDto.setId(taskId);
		taskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		taskDto.setCompleteDate(new Date());
		taskDto.setResult("拒单退款");
		taskDto.setOperatorId(super.getCurrentUser().getId().intValue());
		taskDto.setOperatorName(super.getCurrentUser().getFullname());


		return trainOrderService.rejectOrder(dto, taskDto);
	}

	//保存（线下出票和回填票号）
	@ResponseBody
	@RequestMapping("/saveTicket")
	public ResponseInfo saveTicket(@RequestBody TicketFrom formObj){
		LogUtils.info(logger, "线下出票/修改订单,入参:{}", formObj);
		TrainOrderDto dto = trainOrderService.getTrainOrderById(formObj.getOrderId());

		dto.setIssuedDate(new Date());
		dto.setIssueChannelType(formObj.getIssueChannel());
		dto.setLineType(LineType.OFF_LINE.getType());
		dto.setOutOrderId(formObj.getOutOrderId());
		dto.setOutOrderStatus("ISSUED");
		dto.setTicketNo(formObj.getTicketNo());
		for (TrainPassengerDto passenger: dto.getPassengerList()) {
			for (TrainPassengerDto p:formObj.getPassengerDtos()) {
				if (passenger.getId().equals(p.getId())) {
					passenger.setTrainBox(p.getTrainBox());
					passenger.setSeatNo(p.getSeatNo());
					passenger.setIsNoSeat(p.getIsNoSeat());
					passenger.setTicketStatus(TicketStatus.ISSUED);
					break;
				}
			}
		}
		//供应商结算价
		dto.setTotalSettlePrice(formObj.getTotalPiace());
		//重新计算tr价差
		dto.setTrSpreads(BigDecimalUtils.subtract(dto.getTotalSalePrice(), dto.getTotalSettlePrice()));
		if (!formObj.getIsRevise()) {
			dto.setOrderStatus(OrderStatus.ISSUED);
			dto.setOrderShowStatus(OrderShowStatus.ISSUED);
			dto.setProcessTagType(ProcessTagType.NULL);
		}

		OrderTaskDto orderTaskDto = new OrderTaskDto();
		orderTaskDto.setId(formObj.getTaskId());
		orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		orderTaskDto.setCompleteDate(new Date());
		orderTaskDto.setOperatorId(super.getCurrentUser().getId().intValue());
		orderTaskDto.setOperatorName(super.getCurrentUser().getFullname());
		orderTaskDto.setResult("线下出票");

		return trainOrderService.offLineTicket(dto, orderTaskDto, formObj.getIsRevise());
	}

	//获取订单详情和退票信息
	@RequestMapping("/getOrderDetail")
	public String getOrderDetail(String refundOrderId,String taskId, Model model){

		RefundOrderDto refundOrderDto = trainOrderService.getRefundOrderById(refundOrderId);

		double serverCharge = 0;

		TrainOrderDto order = ((TrainOrderDto)refundOrderDto.getOrderDto());
		if (StringUtils.isBlank(order.getIssueChannelType())) {
			order.setIssueChannelType(order.getHubsCode());
		}

		model.addAttribute(refundOrderDto);
		model.addAttribute("taskId",taskId);
		model.addAttribute("serverCharge",serverCharge);

		return "train/tmc/orderDetail.single";
	}

	//审核通过
	@ResponseBody
	@RequestMapping("/refundAduitPass")
	public ResponseInfo refundAduitPass(@RequestBody RefundAduitForm from){
		RefundOrderDto refundForm = from.getRefundOrderDto();

		RefundOrderDto refundorder = refundOrderService.getById(refundForm.getId());
		refundorder.setSupperRefundAmount(refundForm.getSupperRefundAmount());
		refundorder.setServicePrice(refundForm.getServicePrice());
		refundorder.setRefundTotalAmount(refundForm.getRefundTotalAmount());

		TrainOrderDto order = trainOrderService.getTrainOrderById(refundorder.getOrderId());
		List<TrainPassengerDto> passengerList = order.getPassengerList();
		//refundForm.getPassengersList()中存储的是id=passengerId
		for (RefundPassengerDto formP : refundForm.getPassengersList()) {
			for (TrainPassengerDto op : passengerList) {
				if (formP.getId().equals(op.getId())) {
					op.setAmount(formP.getAmount());
					break;
				}
			}
		}

		for (RefundPassengerDto rp : refundorder.getPassengersList()) {
			for (TrainPassengerDto op : passengerList) {
				if (rp.getUserid().equals(Integer.valueOf(op.getUserId().toString()))
						&& rp.getTravellerType().equals(op.getTravellerType())) {
					rp.setAmount(op.getAmount());
					break;
				}
			}

		}

		refundorder.setOperatorName(super.getCurrentUser().getFullname());
		refundorder.setOperatorId(super.getCurrentUser().getId());
		refundorder.setPassed(true);
		refundorder.setAuditEndTime(new Date());

		// 供应商应退额   = -1*（原订单.供应商结算价 - 供应商退票费）
		refundorder.setSupperRefundAmount(BigDecimalUtils.multiply(refundorder.getSupperRefundAmount(), -1));
		// 客户票面应退额 = = -1*（正向客户票面销售价 - 供应商退票费-调整费）
		refundorder.setCustomerRefundAmount(refundorder.getSupperRefundAmount());
		//供应商退票费（总） = 供应商退票费(单)*退单人数
		refundorder.setRefundServiceFee(BigDecimalUtils.divide(refundorder.getServicePrice(),new BigDecimal(refundorder.getRefundCount())));
		//tr价差
		refundorder.setTrSpreads(BigDecimalUtils.subtract(refundorder.getCustomerRefundAmount(), refundorder.getSupperRefundAmount()));

		return trainOrderService.refundorderAduit(refundorder,from.getTaskId());
	}

	//审核不通过
	@ResponseBody
	@RequestMapping("/refundAduitNoPass")
	public ResponseInfo refundAduitNoPass(RefundOrderDto refundOrderDto,Integer taskId){
		refundOrderDto.setOperatorName(super.getCurrentUser().getFullname());
		refundOrderDto.setOperatorId(super.getCurrentUser().getId());
		refundOrderDto.setPassed(false);
		refundOrderDto.setAuditEndTime(new Date());
		return trainOrderService.refundorderAduit(refundOrderDto,taskId);
	}


	@RequestMapping("/getChangeData")
	public String getChangeData(String orderId, Integer taskId, Model model){

		TrainOrderDto order = trainOrderService.getTrainOrderById(orderId);
		if (StringUtils.isBlank(order.getcName())) {
			order.setcName(partnerService.findById(super.getPartnerId()).getName());
		}
		model.addAttribute("order", order);
		model.addAttribute("taskId", taskId);
		model.addAttribute("oldOrder", trainOrderService.getTrainOrderById(order.getOldOrderId()));
		model.addAttribute("issueChannelMap", getIssueChannelType());
		return "train/tmc/change.jsp";
	}

	@ResponseBody
	@RequestMapping("/offLineChange")
	public ResponseInfo offLineChange(OffLineChangeForm form) {

		LogUtils.info(logger, "客服线下改签,入参:{}", form);
		TrainOrderDto order = trainOrderService.getTrainOrderById(form.getOrderId());
		OrderTaskDto task = orderTaskService.findByTaskId(form.getTaskId());
		if (form.getSuccess()) {
			if (form.getBalance().compareTo(new BigDecimal(0)) <= 0) {
				order.setTotalRefundBalance(form.getBalance());
			} else {
				order.setTotalRefundBalance(new BigDecimal(0));
			}

			order.setOutOrderId(form.getOutOrderId());
			order.setIssueChannelType(form.getIssueChannelCode());
			order.setTicketNo(form.getTicketNo());


			order.setTotalSettlePrice(order.getTotalRefundBalance());

			List<TrainPassengerDto> passengerList = order.getPassengerList();

			//更新成本分摊金额
			BigDecimal amount = null;
			if (order.getTotalAmount().compareTo(new BigDecimal(0)) < 0) {
				amount = BigDecimalUtils.divide(order.getTotalRefundBalance(), new BigDecimal(passengerList.size()));
			} else {
				amount = BigDecimalUtils.divide(order.getTotalAmount(), new BigDecimal(passengerList.size()));
			}
			for (ChangePassengerForm formP: form.getPassengerList()) {
				for (TrainPassengerDto p: passengerList) {
					if (p.getId().equals(formP.getId())) {
						p.setIsNoSeat(formP.getNoSeat() == null ? false : true);
						p.setTrainBox(formP.getTrainBox());
						p.setSeatNo(formP.getSeatNo());
						p.setAmount(amount);
						break;
					}
				}
			}
			task.setResult("客服线下改签成功");
		} else {
			task.setResult("客服线下改签,拒单退款");
			order.setRemark(form.getRefundReason());
		}
		User currentUser = super.getCurrentUser();
		task.setOperatorId(Integer.valueOf(currentUser.getId()+""));
		task.setOperatorName(currentUser.getFullname());
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		task.setCompleteDate(new Date());

		return trainOrderService.offLineChange(order, form.getSuccess(),task);
	}

	@ResponseBody
	@RequestMapping("/tmcPayAgain")
	public ResponseInfo tmcPayAgain(String orderId, Integer taskId){
		LogUtils.info(logger, "tmc再次代扣,入参:orderId={},taskId={}", orderId, taskId);

		ResponseInfo rtn = new ResponseInfo();
		TrainOrderDto order = trainOrderService.getTrainOrderById(orderId);
		OrderTaskDto task = orderTaskService.findByTaskId(taskId);
		if (order == null || task == null) {
			rtn.setCode(TbpRtnCodeConst.ERROR);
			rtn.setMsg("参数错误");
			return rtn;
		}
		task.setCompleteDate(new Date());
		User currentUser = super.getCurrentUser();
		task.setOperatorId(Integer.valueOf(currentUser.getId().toString()));
		task.setOperatorName(currentUser.getFullname());
		task.setResult("tmc再次代扣成功");
		return trainOrderService.tmcPayAgain(order, task);
	}

	@ResponseBody
	@RequestMapping("/finishTask")
	public ResponseInfo finishTask(Integer taskId){
		LogUtils.info(logger, "完成任务,入参:taskId={}", taskId);

		ResponseInfo rtn = new ResponseInfo();
		OrderTaskDto task = orderTaskService.findByTaskId(taskId);
		if (taskId == null) {
			rtn.setCode(TbpRtnCodeConst.ERROR);
			rtn.setMsg("参数错误");
			return rtn;
		}
		task.setCompleteDate(new Date());
		User currentUser = super.getCurrentUser();
		task.setOperatorId(Integer.valueOf(currentUser.getId().toString()));
		task.setOperatorName(currentUser.getFullname());
		task.setResult("任务结束");
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		return trainOrderService.finishTask(task);
	}


}
