
package com.tem.train;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.utils.LogUtils;
import com.iplatform.common.web.Message;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DistrictService;
import com.tem.platform.api.dto.DistrictDto;
import com.tem.platform.form.ResponseTable;
import com.tem.train.api.station.TrainStationService;
import com.tem.train.dto.station.CityLinkEnum;
import com.tem.train.dto.station.TrainStationCondition;
import com.tem.train.dto.station.TrainStationDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("train/station")
public class TrainStationController extends BaseController {

	@Autowired
    private TrainStationService trainStationService;
	@Autowired
	private DistrictService districtService;

	private static final Logger logger = LoggerFactory.getLogger(TrainStationController.class);


	@RequestMapping("")
	public String index(Model model){
		model.addAttribute("cityLinkEnum", CityLinkEnum.values());
		return "train/station/index.base";

	}

	@ResponseBody
	@RequestMapping("getCitysList")
	public List<DistrictDto> getCitysList(){
		List<DistrictDto> cityList = districtService.findAll(3);
		List<DistrictDto> districtCountyList = districtService.findAll(4);
		if (CollectionUtils.isEmpty(cityList)) {
			cityList = new ArrayList<>();
		}
		if (CollectionUtils.isEmpty(districtCountyList)) {
			districtCountyList = new ArrayList<>();
		}
		for (DistrictDto districtCounty: districtCountyList) {
			cityList.add(districtCounty);
		}

		return  cityList;
	}


	@ResponseBody
	@RequestMapping("/batchInsert")
	public boolean batchInsert(@RequestBody  List<TrainStationDto> list) {
		try {
			return trainStationService.saveOrUpdate(list);
		} catch (Exception e) {
			LogUtils.error(logger, "同步车站数据异常,信息:{}", e);
		}
		return false;
	}

	// 分页查询
	@RequestMapping(value = "/list")
	public String list(Integer pageIndex, String name, String belongCity, CityLinkEnum cityLinkEnum, Model model) {

		if (pageIndex == null) {
			pageIndex = 1;
		}

		QueryDto<TrainStationCondition> queryDto = new QueryDto<>(pageIndex,  Page.DEFAULT_PAGE_SIZE);
		queryDto.setCondition(new TrainStationCondition(name, belongCity, cityLinkEnum));

		Page<TrainStationDto> pageList = trainStationService.stationPageListQuery(queryDto);

		model.addAttribute("pageList", pageList);
		return "train/station/stationTable.jsp";
	}

	// 用于新增和修改时查询对象
	@RequestMapping(value = "/getStation")
	public String getCarrier(long id, Model model) {
		TrainStationDto station = trainStationService.findById(id);
		model.addAttribute("station", station);
		JSONObject jo = new JSONObject();
		if (StringUtils.isNotBlank(station.getCityId())) {
			jo.put("id", station.getCityId());
			jo.put("nameCn", station.getCityName());
			model.addAttribute("city", jo.toJSONString());
		}

		return "train/station/stationModel.jsp";
	}

	// 修改时对象
	@ResponseBody
	@RequestMapping(value = "/updateStation")
	public Message updateStation(TrainStationDto station) {
		if (trainStationService.update(station)) {
			return Message.success("修改车站成功");
		}
		return Message.error("修改车站失败");
	}

	/**
	 * 城市分页查询
	 * @return
	 */
	@ResponseBody
	@RequestMapping("districtData")
	public ResponseTable districtData(){
		Integer pageIndex = super.getIntegerValue("pageIndex");
		Integer pageSize = super.getIntegerValue("pageSize");
		if(pageIndex == null){
			pageIndex = 1;
		}
		if(pageSize == null){
			pageSize = 10;
		}
		QueryDto<Object> queryDto = new QueryDto<>(pageIndex,pageSize);
		queryDto.setCondition("city");
		String name = super.getStringValue("name");
		if (StringUtils.isNotBlank(name)) {
			queryDto.setField2(name);
		}

		Page<DistrictDto> districtDtoPage = districtService.queryListWithPage(queryDto);
		return new ResponseTable(districtDtoPage);
	}


	
}

