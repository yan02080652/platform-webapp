package com.tem.train;

import com.iplatform.common.ResponseInfo;
import com.iplatform.common.web.Message;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import com.tem.train.api.hubs.TrainHubsService;
import com.tem.train.dto.HubsConnectDto;
import com.tem.train.dto.hubs.TrainHubsDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("train/hubs")
public class TrainHubsController extends BaseController {

    @Autowired
    private TrainHubsService hubsService;

    @Autowired
    private IssueChannelService issueChannelService;

    @Autowired
    private PartnerService partnerService;

    @RequestMapping("")
    public String index(Model model) {
        ResponseInfo hubsDtos = hubsService.getAllHubsList();
        model.addAttribute("hubsDtos", hubsDtos.getData());
        return "train/hubs/hubs.base";
    }

    /**
     * 进入连接器修改或新增页面
     *
     * @param model Model
     * @return view
     */
    @RequestMapping(value = "/hubsDetail", method = RequestMethod.GET)
    public String hubsDetail(String hubsCode, Model model) {
        TrainHubsDto trainHubsDto = new TrainHubsDto();
        if (StringUtils.isNotEmpty(hubsCode)) {
            trainHubsDto = hubsService.findHubsByCode(hubsCode);
        }
        model.addAttribute(trainHubsDto);
        return "train/hubs/hubsDetail.jsp";
    }

    /**
     * 加载连接器所支持连接
     *
     * @param model Model
     * @param code  连接器编码
     * @return view
     */
    @RequestMapping(value = "/loadRight", method = RequestMethod.GET)
    public String loadRight(Model model, @RequestParam("code") String code) {
        ResponseInfo hubsConnectDtos = hubsService.findHubsConnectListByHubsCode(code, null);
        model.addAttribute("hubsCode", code);
        model.addAttribute("hubsConnectDtos", hubsConnectDtos.getData());
        model.addAttribute("hubsDto", hubsService.findHubsByCode(code));
        return "train/hubs/hubsRight.jsp";
    }

    /**
     * 保存连接器
     *
     * @param hubsDto 连接器数据
     * @param br      数据验证
     * @return Message
     */
    @ResponseBody
    @RequestMapping(value = "/saveOrupdateHubs", method = RequestMethod.POST)
    public Message saveOrupdateHubs(@ModelAttribute("trainHubsDto") @Validated TrainHubsDto hubsDto,
                                    BindingResult br, String editMode) {
        if (br.hasErrors()) {
            return Message.error("数据校验不通过。");
        }
        if (!Boolean.valueOf(editMode)) {
            ResponseInfo responseInfo = hubsService.saveHubs(hubsDto);
            if (TbpRtnCodeConst.SUCCESS.equals(responseInfo.getCode())) {
                return Message.success("新增成功。");
            } else {
                return Message.error(responseInfo.getMsg());
            }
        } else {
            ResponseInfo responseInfo = hubsService.updateHubs(hubsDto);
            if (TbpRtnCodeConst.SUCCESS.equals(responseInfo.getCode())) {
                return Message.success("修改成功。");
            } else {
                return Message.error(responseInfo.getMsg());
            }
        }
    }

    /**
     * 进入连接器所属连接修改或新增页面
     *
     * @param model Model
     * @return view
     */
    @RequestMapping(value = "/hubsConnectDetail", method = RequestMethod.GET)
    public String hubsConnectDetail(Model model) {
        HttpServletRequest request = getRequest();
        HubsConnectDto hubsConnectDto = null;
        String id = request.getParameter("id");
        if (id != null) {
            String tmcId = request.getParameter("tmcId");
            hubsConnectDto = hubsService.findHubsConnectById(id);
            PartnerDto tmcPartner = partnerService.findById(Long.valueOf(tmcId));
            model.addAttribute("tmcPartner", tmcPartner);
        } else {
            //新增的时候回传进来
            String hubsCode = request.getParameter("hubsCode");
            hubsConnectDto = new HubsConnectDto();
            hubsConnectDto.setHubsCode(hubsCode);
        }
        model.addAttribute("hubsConnectDto", hubsConnectDto);
        return "train/hubs/hubsConnectDetail.jsp";
    }

    /**
     * 根据tmcId获得出票渠道
     *
     * @return hubsConnectDtos
     */
    @RequestMapping("getIssueChannels.json")
    @ResponseBody
    public List<IssueChannelDto> getIssueChannels(Long tmcId) {
        return issueChannelService.findListByTmcIdAndChannelTypeAndLineType(
                tmcId,
                IssueChannelType.TRAIN.getType(),
                LineType.ON_LINE.getType()
        );
    }

    /**
     * 保存连接器的连接
     *
     * @param hubsConnectDto 连接器的连接的数据
     * @param br             数据效验
     * @return Message
     */
    @ResponseBody
    @RequestMapping(value = "/saveOrupdateHubsConnect", method = RequestMethod.POST)
    public Message saveOrupdateHubsConnect(@ModelAttribute("hubsConnectDto") @Validated HubsConnectDto hubsConnectDto,
                                           BindingResult br) {
        if (br.hasErrors()) {
            return Message.error("数据校验不通过。");
        }
        ResponseInfo responseInfo = null;
        try{
            responseInfo = hubsService.saveUpdateHubsConnect(hubsConnectDto);
        } catch (Exception e) {
            return Message.error(String.format("连接器名字:%s,存在相同!", hubsConnectDto.getHusConnectName()));
        }

        if (hubsConnectDto.getId() == null) {
            if (TbpRtnCodeConst.SUCCESS.equals(responseInfo.getCode())) {
                return Message.success("新增成功。");
            } else {
                return Message.error(responseInfo.getMsg());
            }
        } else {
            if (TbpRtnCodeConst.SUCCESS.equals(responseInfo.getCode())) {
                return Message.success("修改成功。");
            } else {
                return Message.error(responseInfo.getMsg());
            }
        }
    }

    @ResponseBody
    @RequestMapping(value = "/getHubByCode")
    public TrainHubsDto  getHubByCode(String hubsCode) {
        return hubsService.findHubsByCode(hubsCode);
    }
}
