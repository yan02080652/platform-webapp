package com.tem.train.form;

/**
 * @author: wangzheqi
 * @description:
 * @date: 创建于: 2018-04-11 16:48
 * @modified By:
 */
public class ChangePassengerForm {

    // 乘客id
    private Integer id;
    // 是否无座
    private Boolean noSeat;
    // 座位号
    private String seatNo;
    // 车厢号
    private String trainBox;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getNoSeat() {
        return noSeat;
    }

    public void setNoSeat(Boolean noSeat) {
        this.noSeat = noSeat;
    }

    public String getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(String seatNo) {
        this.seatNo = seatNo;
    }

    public String getTrainBox() {
        return trainBox;
    }

    public void setTrainBox(String trainBox) {
        this.trainBox = trainBox;
    }
}
