package com.tem.train.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author: wangzheqi
 * @description: 线下改签
 * @date: 创建于: 2018-04-11 16:48
 * @modified By:
 */
public class OffLineChangeForm implements Serializable {

    private static final long serialVersionUID = -1733141908204324803L;

    private Integer taskId;

    private String orderId;

    private String outOrderId;

    private String issueChannelCode;

    private String ticketNo;

    private Boolean success;

    private List<ChangePassengerForm> passengerList;

    private BigDecimal balance;

    private String refundReason;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOutOrderId() {
        return outOrderId;
    }

    public void setOutOrderId(String outOrderId) {
        this.outOrderId = outOrderId;
    }

    public String getIssueChannelCode() {
        return issueChannelCode;
    }

    public void setIssueChannelCode(String issueChannelCode) {
        this.issueChannelCode = issueChannelCode;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public List<ChangePassengerForm> getPassengerList() {
        return passengerList;
    }

    public void setPassengerList(List<ChangePassengerForm> passengerList) {
        this.passengerList = passengerList;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getRefundReason() {
        return refundReason;
    }

    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }
}
