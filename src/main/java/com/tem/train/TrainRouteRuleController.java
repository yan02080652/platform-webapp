package com.tem.train;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerBpService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.UserDto;
import com.tem.train.api.hubs.TrainHubsService;
import com.tem.train.api.routerule.TrainRouteRuleService;
import com.tem.train.dto.HubsConnectDto;
import com.tem.train.dto.RouteRuleDto;
import com.tem.train.dto.RouteRuleItemDto;
import com.tem.train.dto.RuleGrade;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("train/routeRule")
public class TrainRouteRuleController extends BaseController {

    private static final String FAILED = "-1";
    private static final String SUCCESS = "0";

    @Autowired
    private TrainRouteRuleService routeRuleService;

    @Autowired
    private TrainHubsService hubsService;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private PartnerBpService bpService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "")
    public String index(Model model) {
        model.addAttribute("ruleGrade", RuleGrade.values());
        return "train/routeRule/index.base";
    }

    /**
     * 路由规则分页查询
     *
     * @param model     Model
     * @param pageIndex 当前页
     * @param pageSize  页数据行数
     * @return view
     */
    @RequestMapping(value = "/list")
    public String list(Integer pageIndex, String pId, String routeRuleGrade, Model model) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        QueryDto<RouteRuleDto> queryDto = new QueryDto<>(pageIndex,  Page.DEFAULT_PAGE_SIZE);
        RouteRuleDto routeRuleDto = new RouteRuleDto();
        if (StringUtils.isNotBlank(routeRuleGrade)) {
            routeRuleDto.setRuleGrade(RuleGrade.valueOf(routeRuleGrade));
        }
        if (StringUtils.isNotBlank(pId)) {
            routeRuleDto.setScope(pId);
        }
        queryDto.setCondition(routeRuleDto);
        Page<RouteRuleDto> pageList = routeRuleService.queryRouteRuleListWithPage(queryDto).getData();
        model.addAttribute("pageList", pageList);
        return "train/routeRule/routeRuleTable.jsp";
    }

    /**
     * 删除路由规则
     *
     * @param id 路由规则id
     * @return msg
     */
    @ResponseBody
    @RequestMapping(value = "/delete")
    public ResponseInfo delete(String id) {
        String[] ids = id.split(",");
        ResponseInfo responseInfo = routeRuleService.batchDeleteRouteRule(ids);
        if (TbpRtnCodeConst.SUCCESS.equals(responseInfo.getCode())) {
            return new ResponseInfo(SUCCESS, "删除成功!");
        } else {
            return new ResponseInfo(FAILED, "删除失败!");
        }
    }

    /**
     * 进入新增或修改页面
     *
     * @param model Model
     * @param id    路由规则id
     * @return view
     */
    @RequestMapping(value = "/getRouteRule")
    public String getRouteRule(String id, Model model) {
        RouteRuleDto routeRuleDto = new RouteRuleDto();
        if (StringUtils.isNotBlank(id)) {
            routeRuleDto = routeRuleService.findRouteRuleById(Long.valueOf(id));
            model.addAttribute("tmcPartner", partnerService.getNameById(Long.valueOf(routeRuleDto.getScope())));
            UserDto user = null;
            if (StringUtils.isNotBlank(routeRuleDto.getCreater())) {
                user = userService.getUser(Long.valueOf(routeRuleDto.getCreater()));
                routeRuleDto.setCreaterName(user == null ? null : user.getFullname());
            }
            if (StringUtils.isNotBlank(routeRuleDto.getLastModifier())) {
                user = userService.getUser(Long.valueOf(routeRuleDto.getLastModifier()));
                routeRuleDto.setLastModifierName(user == null ? null : user.getFullname());
            }
        }
        model.addAttribute("routeRuleDto", routeRuleDto);
        //查询连接器
        model.addAttribute("hubs", hubsService.getAllHubsList().getData());
        model.addAttribute("ruleGrade", RuleGrade.values());
        return "train/routeRule/routeRuleModels.base";
    }

    /**
     * 根据hubsCode获取连接
     *
     * @return hubsConnectDtos
     */
    @RequestMapping("getConnects.json")
    @ResponseBody
    public List<HubsConnectDto> getConnects(String hubsCode, String scope, String ruleGrade) {
        if ("PARTNER".equals(ruleGrade)) {
            //如果获得的是企业id
            return hubsService.findHubsConnectListByHubsCode(hubsCode, bpService.findTmcIdByPId(Long.valueOf(scope)).toString()).getData();
        }
        return hubsService.findHubsConnectListByHubsCode(hubsCode, scope).getData();
    }

    /**
     * 保存路由规则
     *
     * @param routeRuleDto 路由规则数据
     * @return msg
     */
    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ResponseInfo save(RouteRuleDto routeRuleDto) {
        //最后修改人
        routeRuleDto.setLastModifier(getCurrentUser().getId().toString());
        //最后修改时间
        routeRuleDto.setLastUpdateDate(new Date());
        if (routeRuleDto.getId() == null) {
            //创建人
            routeRuleDto.setCreater(getCurrentUser().getId().toString());
            //创建时间
            routeRuleDto.setCreateDate(new Date());
        }
        ResponseInfo rtn = new ResponseInfo();
        try {
            rtn = routeRuleService.saveUpdateRouteRule(routeRuleDto);
        } catch (Exception e) {
            rtn.setCode(TbpRtnCodeConst.ERROR);
            rtn.setMsg(String.format("%s路由规则,不能重复添加!", routeRuleDto.getRuleGrade().getMsg()));
        }
        return rtn;
    }

    /**
     * 更改启用状态
     *
     * @param id 路由规则id
     * @return ResponseInfo
     */
    @ResponseBody
    @RequestMapping(value = "/updateDisable")
    public ResponseInfo updateDisable(String id) {
        // 根据路由规则id查询
        RouteRuleDto routeRuleDto = routeRuleService.findRouteRuleById(Long.valueOf(id));
        // 如果是启用则改为禁用，如果是禁用则改为启用
        if (routeRuleDto.getIsDisable()) {
            routeRuleDto.setIsDisable(false);
        } else {
            routeRuleDto.setIsDisable(true);
        }
        // 执行保存方法
        return save(routeRuleDto);
    }

    @RequestMapping("/getRouteRuleItem")
    public String getRouteRuleItem(Long id, Long routeRuleId, Model model) {
        //查询配置项
        RouteRuleItemDto itemDto = routeRuleService.findByRuleItemId(id);
        //根据配置id查询该tmc下的可用连接账号
        RouteRuleDto routeRuleDto = routeRuleService.findRouteRuleById(routeRuleId);
        if (routeRuleDto != null) {
            //tmc级别
            Long tmcId = 0L;
            if (RuleGrade.TMC == routeRuleDto.getRuleGrade()) {
                tmcId = Long.valueOf(routeRuleDto.getScope());
            } else if (RuleGrade.PARTNER == routeRuleDto.getRuleGrade()){
                //根据pid查询tmcId
                tmcId = bpService.findTmcIdByPId(Long.valueOf(routeRuleDto.getScope()));
            }
            List<HubsConnectDto> hubsConnectList = hubsService.findAvailableConnectByTmcId(tmcId);
            //根据hubsCode分组
            Map<String, List<HubsConnectDto>> connectMap = new HashMap<>(16);
            for (HubsConnectDto dto : hubsConnectList) {
                if (connectMap.containsKey(dto.getHubsCode())) {
                    connectMap.get(dto.getHubsCode()).add(dto);
                } else {
                    List<HubsConnectDto> list = new ArrayList<>();
                    list.add(dto);
                    connectMap.put(dto.getHubsCode(), list);
                }
            }
            model.addAttribute("connectMap", connectMap);
        }

        model.addAttribute("itemDto", itemDto);
        model.addAttribute("routeRuleId", routeRuleId);
        return "train/routeRule/item.single";
    }

    @ResponseBody
    @RequestMapping("/saveRouteRuleItem")
    public ResponseInfo saveRouteRuleItem(RouteRuleItemDto itemDto) {
        ResponseInfo rtn = new ResponseInfo();
        rtn.setCode(TbpRtnCodeConst.SUCCESS);
        rtn.setMsg("成功");
       try {
           routeRuleService.saveOrUpdateRouteRuleItem(itemDto);
       } catch (Exception e) {
           rtn.setCode(TbpRtnCodeConst.ERROR);
           rtn.setMsg("优先级不能相同!");
       }
       return rtn;
    }

    @ResponseBody
    @RequestMapping("/deleteRouteRuleItem")
    public boolean deleteRouteRuleItem(Long id) {
        return routeRuleService.deleteRouteRuleItem(id);
    }

}