package com.tem.pss.form;

import java.io.Serializable;

/**
 * 企业服务费模版
 * Created by pyy on 2017/6/5.
 */
public class RelPartnerForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long partnerId;

    private String partnerName;

    private Integer partnerStatus;

    private Integer tmplId;

    private String tmplName;

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public Integer getPartnerStatus() {
        return partnerStatus;
    }

    public void setPartnerStatus(Integer partnerStatus) {
        this.partnerStatus = partnerStatus;
    }

    public Integer getTmplId() {
        return tmplId;
    }

    public void setTmplId(Integer tmplId) {
        this.tmplId = tmplId;
    }

    public String getTmplName() {
        return tmplName;
    }

    public void setTmplName(String tmplName) {
        this.tmplName = tmplName;
    }
}
