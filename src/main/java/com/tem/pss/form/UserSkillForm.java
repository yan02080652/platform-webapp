package com.tem.pss.form;

import com.tem.platform.api.dto.UserDto;
import com.tem.platform.api.dto.UserRoleDto;
import com.tem.pss.dto.serverPartner.ServerSkillDto;

import java.io.Serializable;

/**
 * 客服角色 工作条线配置
 * Created by pyy on 2017/10/24.
 */
public class UserSkillForm implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long orgId;

    private String orgName;

    private Long userId;

    private String fullName;

    /**
     * 工作条线配置
     */
    private ServerSkillDto serverSkill;

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public ServerSkillDto getServerSkill() {
        return serverSkill;
    }

    public void setServerSkill(ServerSkillDto serverSkill) {
        this.serverSkill = serverSkill;
    }


}
