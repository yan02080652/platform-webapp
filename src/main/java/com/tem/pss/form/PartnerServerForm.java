package com.tem.pss.form;

import java.io.Serializable;

/**
 * 企业客服分配
 * Created by pyy on 2017/10/25.
 */
public class PartnerServerForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long partnerId;

    private String partnerName;

    private String userIds;

    private String userNames;

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public String getUserIds() {
        return userIds;
    }

    public void setUserIds(String userIds) {
        this.userIds = userIds;
    }

    public String getUserNames() {
        return userNames;
    }

    public void setUserNames(String userNames) {
        this.userNames = userNames;
    }
}
