package com.tem.pss.action;

import com.google.gson.reflect.TypeToken;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.utils.JsonUtils;
import com.iplatform.common.web.Message;
import com.tem.platform.api.*;
import com.tem.platform.api.condition.UserCondition;
import com.tem.platform.api.condition.UserRoleCondition;
import com.tem.platform.api.dto.*;
import com.tem.platform.form.ResponseTable;
import com.tem.pss.api.ServerPartnerService;
import com.tem.pss.dto.serverPartner.ServerPartnerDto;
import com.tem.pss.dto.serverPartner.ServerSkillDto;
import com.tem.ConstantCommon;
import com.tem.platform.action.BaseController;
import com.tem.pss.form.PartnerServerForm;
import com.tem.pss.form.UserSkillForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * 企业客服分配
 * Created by pyy on 2017/10/23.
 */
@RequestMapping("pss/serverPartner")
@Controller
public class ServerPartnerController extends BaseController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private OrgService orgService;

    @Autowired
    private ServerPartnerService serverPartnerService;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private UserService userService;

    @RequestMapping
    public String index(){
        /*RoleDto roleDto = roleService.getByCode(super.getTmcId(), ConstantCommon.IP_ROLE_CS);
        if (roleDto == null) {
            model.addAttribute("no_cs_role", true);
        }*/
        return "pss/serverPartner/index.base";
    }

    @RequestMapping(value = "skill")
    public String index(Model model) {
        Integer pageIndex = super.getIntegerValue("pageIndex");
        if (pageIndex == null) {
            pageIndex = 1;
        }
        Long tmcId = super.getTmcId();
        /*RoleDto roleDto = roleService.getByCode(tmcId, ConstantCommon.IP_ROLE_CS);
        if (roleDto == null) {
            return "pss/serverPartner/tip.base";
        }
        String searchName = super.getStringValue("searchName");
        QueryDto queryDto = new QueryDto(pageIndex,15 );
        UserRoleCondition userRoleCondition = new UserRoleCondition();
        userRoleCondition.setPartnerId(tmcId);
        userRoleCondition.setRoleId(roleDto.getId());
        userRoleCondition.setGroupBy("USER_ID");
        userRoleCondition.setSearchField(searchName);
        queryDto.setCondition(userRoleCondition);
        queryDto.setSidx("ORG_ID");
        Page<UserRoleDto> page = userRoleService.queryListWithPage(queryDto);

        List<Long> userIds = new ArrayList<>();
        //增加单位信息
        List<UserRoleDto> userRoleDtos = page.getList();
        Map<Long, OrgDto> orgNameMap = new HashMap<>();
        for(UserRoleDto userRoleDto:userRoleDtos){
            Long orgId = userRoleDto.getOrgId();
            if (orgNameMap.containsKey(orgId)) {
                userRoleDto.setOrg(orgNameMap.get(orgId));
            } else {
                OrgDto orgDto = orgService.getById(orgId);
                userRoleDto.setOrg(orgDto);
                orgNameMap.put(orgId, orgDto);
            }
            userIds.add(userRoleDto.getUserId());
        }*/
        UserCondition userCondition = new UserCondition();
        userCondition.setPartnerId(tmcId);
        String searchName = super.getStringValue("searchName");
        userCondition.setFullname(searchName);
        QueryDto queryDto = new QueryDto(pageIndex, 15);
        queryDto.setCondition(userCondition);
        queryDto.setSidx("ORG_ID");
        Page userPage = userService.queryListWithPage(queryDto);

        Map<Long, String> orgNameMap = new HashMap<>();
        List<UserDto> userDtos = userPage.getList();
        List<UserSkillForm> userRoleForms = new ArrayList<>();
        Map<Long, UserSkillForm> userSkillMap = new HashMap<>();
        //转到新的对象中
        for (int i = 0; i < userDtos.size(); i++) {
            UserDto userDto = userDtos.get(i);
            UserSkillForm userSkillForm = new UserSkillForm();
            userSkillForm.setUserId(userDto.getId());
            userSkillForm.setFullName(userDto.getFullname());
            Long orgId = userDto.getOrgId();
            userSkillForm.setOrgId(orgId);
            if (orgId != null) {
                String orgName = null;
                if (orgNameMap.containsKey(orgId)) {
                    orgName = orgNameMap.get(orgId);
                }else{
                    OrgDto org = orgService.getById(orgId);
                    if (org != null) {
                        orgName = org.getName();
                    }
                    orgNameMap.put(orgId, orgName);
                }
                userSkillForm.setOrgName(orgName);
            }
            userRoleForms.add(userSkillForm);
            userSkillMap.put(userDto.getId(),userSkillForm);
        }

        //增加附加属性
        this.setSkill(userSkillMap);
        userPage.setList(userRoleForms);

        //Page<UserSkillForm> pageForm = new Page(userRoleForms,userPage.getCurrentPage(),userPage.getSize(),userPage.getTotal());

        model.addAttribute("pageList", userPage);
        model.addAttribute("searchName", searchName);
        //是否需要国际航班、国际酒店
        model.addAttribute("out", super.getStringValue("out"));
        return "pss/serverPartner/serverSkill.single";
    }

    /**
     * 查询本企业客服
     * @return
     */
    @RequestMapping("searchCs")
    @ResponseBody
    public Map<String,Object> searchCs(){
        Integer pageIndex = super.getIntegerValue("pageIndex");
        Integer pageSize = super.getIntegerValue("pageSize");
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }

        Long tmcId = super.getTmcId();
        UserCondition userCondition = new UserCondition();
        userCondition.setPartnerId(tmcId);
        String searchName = super.getStringValue("searchName");
        userCondition.setFullname(searchName);
        QueryDto queryDto = new QueryDto(pageIndex, pageSize);
        queryDto.setCondition(userCondition);
        queryDto.setSidx("ORG_ID");
        Page userPage = userService.queryListWithPage(queryDto);

        List<UserDto> userDtos = userPage.getList();
        List<UserSkillForm> userRoleForms = new ArrayList<>();
        Map<Long, UserSkillForm> userSkillMap = new HashMap<>();
        //转到新的对象中
        for (int i = 0; i < userDtos.size(); i++) {
            UserDto userDto = userDtos.get(i);
            UserSkillForm userSkillForm = new UserSkillForm();
            userSkillForm.setUserId(userDto.getId());
            userSkillForm.setFullName(userDto.getFullname());
            userSkillForm.setOrgId(userDto.getOrgId());
            userRoleForms.add(userSkillForm);
            userSkillMap.put(userDto.getId(),userSkillForm);
        }
        this.setSkill(userSkillMap);
        userPage.setList(userRoleForms);
        Map<String, Object> result = new HashMap<>();
        result.put("pagelist",userPage);
        return result;
    }

    private void setSkill(Map<Long,UserSkillForm> userSkillMap){
        List<Long> userIds = new ArrayList<>(userSkillMap.keySet());
        List<ServerSkillDto> skills = serverPartnerService.findSkill(userIds);
        for (ServerSkillDto skill : skills) {
            UserSkillForm userSkill = userSkillMap.get(skill.getUserId());
            if (userSkill != null) {
                userSkill.setServerSkill(skill);
            }
        }
    }

    /**
     * 查询已选列表数据、查询本企业客服（共用一个方法，防止使用）
     * @return
     */
    @RequestMapping("spData")
    @ResponseBody
    public ResponseTable serverPartner(){
        String moreInfo = super.getStringValue("moreInfo");
        if (StringUtils.isNotEmpty(moreInfo)) {
            //加载已选列表
            return this.getByUserIds();
        }else{
            //查询分页数据
            return this.pageTableInfo();
        }
    }

    private ResponseTable getByUserIds(){
        String keys = super.getStringValue("keys");
        ResponseTable responseTable = new ResponseTable();
        if (StringUtils.isNotEmpty(keys)) {
            String[] keysArr = keys.split(",");
            List<Long> keyList = new ArrayList<>();
            for (int i = 0; i < keysArr.length; i++) {
                keyList.add(Long.valueOf(keysArr[i]));
            }
            List<UserDto> users = userService.getUsers(keyList);
            List<ServerSkillDto> skills = serverPartnerService.findSkill(keyList);
            Map<Long, ServerSkillDto> serverSkillDtoMap = new HashMap<>();
            for (ServerSkillDto ss : skills) {
                serverSkillDtoMap.put(ss.getUserId(), ss);
            }
            List<ServerSkillDto> serverSkillDtos = new ArrayList<>();
            for (UserDto user : users) {
                ServerSkillDto ss = serverSkillDtoMap.get(user.getId());
                if (ss == null) {
                    ss = new ServerSkillDto();
                    ss.setUserId(user.getId());
                }
                ss.setFullname(user.getFullname());
                serverSkillDtos.add(ss);
            }
            responseTable.setData(serverSkillDtos);
        }
        return responseTable;
    }

    private ResponseTable pageTableInfo(){
        Integer pageIndex = super.getIntegerValue("pageIndex");
        Integer pageSize = super.getIntegerValue("pageSize");
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }

        Long tmcId = super.getTmcId();
        RoleDto roleDto = roleService.getByCode(tmcId, ConstantCommon.IP_ROLE_CS);
        Map<String, Object> result = new HashMap<>();
        if (roleDto != null) {
            String searchName = super.getStringValue("searchName");
            QueryDto queryDto = new QueryDto(pageIndex,pageSize );
            UserRoleCondition userRoleCondition = new UserRoleCondition();
            userRoleCondition.setPartnerId(tmcId);
            userRoleCondition.setRoleId(roleDto.getId());
            userRoleCondition.setGroupBy("USER_ID");
            userRoleCondition.setSearchField(searchName);
            queryDto.setCondition(userRoleCondition);
            queryDto.setSidx("ORG_ID");
            Page<UserRoleDto> page = userRoleService.queryListWithPage(queryDto);

            List<Long> userIds = new ArrayList<>();
            Map<Long, String> userNameMap = new HashMap<>();
            List<UserRoleDto> userRoleDtos = page.getList();
            for(UserRoleDto userRoleDto:userRoleDtos){
                Long userId = userRoleDto.getUserId();
                userIds.add(userId);
                UserDto user = userRoleDto.getUser();
                if (user != null) {
                    userNameMap.put(userId, user.getFullname());
                }
            }

            //最终返回结果集
            List<ServerSkillDto> serverSkillDtos = new ArrayList<>();
            //增加附加属性
            if (userIds.size() > 0) {
                Map<Long, ServerSkillDto> skillMap = new HashMap<>(16);
                List<ServerSkillDto> skills = serverPartnerService.findSkill(userIds);
                for (ServerSkillDto skill : skills) {
                    skillMap.put(skill.getUserId(), skill);
                }
                for (Long userId:userIds) {
                    ServerSkillDto ss = skillMap.get(userId);
                    if (ss == null) {
                        ss = new ServerSkillDto();
                        ss.setUserId(userId);
                    }
                    ss.setFullname(userNameMap.get(userId));
                    serverSkillDtos.add(ss);
                }
            }
            Page<ServerSkillDto> pageForm = new Page(serverSkillDtos,page.getCurrentPage(),page.getSize(),page.getTotal());
            return new ResponseTable(pageForm);
        }
        return new ResponseTable(null,0,"找不到客服人员");
    }

    /**
     * 保存客服工作技能
     * @param skills
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "saveSkill")
    public Message saveSkill(String skills){
        List<ServerSkillDto> serverSkillDtos = JsonUtils.getGson().fromJson(skills, new TypeToken<List<ServerSkillDto>>(){}.getType());
        Long partnerId = super.getTmcId();
        for (ServerSkillDto ss : serverSkillDtos) {
            ss.setPartnerId(partnerId);
        }
        serverPartnerService.saveSkill(serverSkillDtos);
        return Message.success("ok");
    }

    @RequestMapping(value = "serverPartner")
    public String show(Model model) {
        //List<Long> ids = serverPartnerService.findCsServerPartner(808L, null);
        Integer pageIndex = super.getIntegerValue("pageIndex");
        if (pageIndex == null) {
            pageIndex = 1;
        }
        int pageSize = 15;
        String searchName = super.getStringValue("searchName");
        Map<String, Object> param = new HashMap<>(16);
        Long tmcId = super.getTmcId();
        param.put("tmcId", tmcId);
        param.put("start", (pageIndex-1)*pageSize);
        param.put("pageSize", pageSize);
        param.put("name", searchName);
        List<PartnerDto> partnerList = partnerService.getPartnerList(param);
        int count = partnerService.getPartnerCount(param);

        List<PartnerServerForm> partnerServerForms = new ArrayList<>(partnerList.size());
        List<Long> partnerIds = new ArrayList<>(partnerList.size());
        for (PartnerDto partnerDto:partnerList) {
            PartnerServerForm ps = new PartnerServerForm();
            ps.setPartnerId(partnerDto.getId());
            ps.setPartnerName(partnerDto.getName());
            partnerIds.add(partnerDto.getId());
            partnerServerForms.add(ps);
        }
        List<ServerPartnerDto> serverPartners = serverPartnerService.findServerPartner(partnerIds);
        Map<Long, List<Long>> csMap = new HashMap<>(16);
        Set<Long> csUserIds = new HashSet<>();
        for (ServerPartnerDto serverPartner:serverPartners){
            Long partnerId = serverPartner.getPartnerId();
            List<Long> userIds = csMap.get(partnerId);
            if (userIds == null) {
                userIds = new ArrayList<>();
                csMap.put(partnerId,userIds);
            }
            userIds.add(serverPartner.getUserId());
            csUserIds.add(serverPartner.getUserId());
        }

        Map<Long, String> userNameMap = new HashMap<>(16);
        for (PartnerServerForm ps : partnerServerForms) {
            List<Long> userIds = csMap.get(ps.getPartnerId());
            ps.setUserIds(toStringIds(userIds));
            String userNames =  this.getUserNames(userNameMap, userIds);
            //ps.setUserNames(userNames);
        }

        //每个客服所擅长的技能
        if (csUserIds.size() > 0) {
            List<ServerSkillDto> skills = serverPartnerService.findSkill(new ArrayList<>(csUserIds));
            model.addAttribute("skills", JsonUtils.getGson().toJson(skills));
        }else{
            model.addAttribute("skills", "[]");
        }

        Page<PartnerServerForm> pageList = new Page(partnerServerForms,pageIndex,pageSize,count);
        model.addAttribute("userNameMap", JsonUtils.getGson().toJson(userNameMap));

        model.addAttribute("pageList", pageList);
        model.addAttribute("searchName", searchName);
        return "pss/serverPartner/serverPartner.single";
    }

    public String toStringIds(List<Long> userIds){
        String ids = "";
        if (userIds != null) {
            for (int i = 0; i < userIds.size(); i++) {
                ids += userIds.get(i)+",";
            }
        }
        return ids;
    };

    /**
     * 缓存用户名称
     * @param userNameMap
     * @param userIds
     */
    private String getUserNames(Map<Long,String> userNameMap,List<Long> userIds){
        if (userIds == null) {
            return null;
        }
        String rs = null;
        for (int i = 0; i < userIds.size(); i++) {
            String userName = null;
            Long userId = userIds.get(i);
            if (userNameMap.containsKey(userId)) {
                userName = userNameMap.get(userId);
            }else{
                UserDto userDto = userService.getUserBaseInfo(userId);
                if (userDto != null) {
                    userName = userDto.getFullname();
                    userNameMap.put(userId, userName);
                }else{
                    userNameMap.put(userId, null);
                }
            }

            if (rs == null) {
                rs = userName;
            }else{
                rs += ","+userName;
            }
        }
        return rs;
    }

    @ResponseBody
    @RequestMapping(value = "skillUsers")
    public List<UserDto> getSkillUser(Model model) {
        String type = super.getStringValue("type");
        Long partnerId = super.getTmcId();
        List<Long> userIds = serverPartnerService.fingSkillUsers(partnerId, type);
        return userService.getUsers(userIds);
    }

    @ResponseBody
    @RequestMapping(value="addCsPartner")
    public Message addCsPartner(ServerPartnerDto serverPartnerDto){
        serverPartnerDto.setTmcId(super.getTmcId());
        serverPartnerDto.setUpdateTime(new Date());
        List<ServerPartnerDto> serverPartnerDtos = new ArrayList<>();
        serverPartnerDtos.add(serverPartnerDto);
        int rs = serverPartnerService.addServerPartners(serverPartnerDtos);
        return Message.success("ok");
    }

    @ResponseBody
    @RequestMapping(value="removeCsPartner")
    public Message removeCsPartner(ServerPartnerDto serverPartnerDto){
        serverPartnerDto.setTmcId(super.getTmcId());
        serverPartnerDto.setUpdateTime(new Date());
        int rs = serverPartnerService.removeServerPartners(serverPartnerDto);
        return Message.success("ok");
    }

}
