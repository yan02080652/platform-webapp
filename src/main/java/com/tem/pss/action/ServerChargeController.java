package com.tem.pss.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.reflect.TypeToken;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.utils.JsonUtils;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.pss.api.ServerChargeService;
import com.tem.pss.dto.serverCharge.RelPartnerDto;
import com.tem.pss.dto.serverCharge.RelPartnerQueryDto;
import com.tem.pss.dto.serverCharge.ServerChargeDetailsDto;
import com.tem.pss.dto.serverCharge.ServerChargeDto;
import com.tem.pss.dto.serverCharge.ServerChargeTmplQueryDto;
import com.tem.pss.form.RelPartnerForm;

/**
 * 服务费设置
 * Created by pyy on 2017/5/31.
 */
@RequestMapping("pss/serverCharge")
@Controller
public class ServerChargeController extends BaseController {

    @Autowired
    private ServerChargeService serverChargeService;

    @Autowired
    private UserService userService;

    @Autowired
    private PartnerService partnerService;

    /**
     * 服务费模版设置首页
     * @param model
     * @return
     */
    @RequestMapping(value = "")
    public String index(Model model) {
        //火车票服务费
        /*ServerChargeDetailDto serverChargeDetailDto = serverChargeService.getServerCharge(1424L, ServerChargeSceneEnum.IN_TRAIN.getValue());
        System.out.println(serverChargeDetailDto.getBack());*/

        String name = super.getStringValue("name");
        ServerChargeTmplQueryDto serverChargeTmplQueryDto = new ServerChargeTmplQueryDto();
        serverChargeTmplQueryDto.setTmplName(name);
        serverChargeTmplQueryDto.setPartnerName(name);
        List<ServerChargeDto> serverChargeDtos = serverChargeService.query(serverChargeTmplQueryDto);

        Map<Long, String> userNameMap = new HashMap<>();
        for (int i = 0; i < serverChargeDtos.size(); i++) {
            ServerChargeDto serverChargeDto = serverChargeDtos.get(i);
            String creUserName = this.getUserName(serverChargeDto.getCreateUser(), userNameMap);
            serverChargeDto.setCreateUserName(creUserName);

            String updateUserName = this.getUserName(serverChargeDto.getUpdateUser(), userNameMap);
            serverChargeDto.setUpdateUserName(updateUserName);
        }
        model.addAttribute("list", serverChargeDtos);
        model.addAttribute("name", name);
        return "pss/serverCharge/list.base";
    }

    @RequestMapping("searchServerChargeTmpl")
    @ResponseBody
    public Map<String,Object> searcTmpl(){
        String name = super.getStringValue("name");
        ServerChargeTmplQueryDto serverChargeTmplQueryDto = new ServerChargeTmplQueryDto();
        serverChargeTmplQueryDto.setTmplName(name);
        serverChargeTmplQueryDto.setPartnerName(name);
        List<ServerChargeDto> serverChargeDtos = serverChargeService.query(serverChargeTmplQueryDto);
        Map<String, Object> result = new HashMap<>();
        result.put("list", serverChargeDtos);
        return result;
    }

    /**
     * 获取用户名称
     * @param userId
     * @param userNameMap
     * @return
     */
    private String getUserName(Long userId, Map<Long, String> userNameMap) {
        if (userId == null) {
            return null;
        }
        if (userId == -1) {
            return "系统管理员";
        }
        if (userNameMap.containsKey(userId)) {
            return userNameMap.get(userId);
        }
        UserDto user = userService.getUser(userId);
        if (user != null) {
            userNameMap.put(userId, user.getFullname());
            return user.getFullname();
        }
        return null;
    }

    /**
     * 服务费模版关联的企业数量
     * @param tmplId
     * @return
     */
    @RequestMapping("countRelPartner")
    @ResponseBody
    public Map<String, Object> countRel(@RequestParam("tmplId")Integer tmplId){
        Integer count = serverChargeService.countRelPartner(tmplId);
        Map<String, Object> result = new HashMap<>();
        result.put("count", count);
        return  result;
    }

    /**
     * 删除一个服务费模版
     * @param tmplId
     * @return
     */
    @RequestMapping("delete")
    @ResponseBody
    public Map<String, Object> delete(@RequestParam("tmplId") Integer tmplId) {
        int rs = serverChargeService.deleteTmpl(tmplId);
        Map<String, Object> result = new HashMap<>();
        result.put("code", rs);
        return result;
    }

    /**
     * 跳转至明细
     * @param model
     * @return
     */
    @RequestMapping("detail")
    public String detail(Model model){
        Integer tmplId = super.getIntegerValue("tmplId");
        if (tmplId != null) {
            ServerChargeDetailsDto serverChargeDetail = serverChargeService.getServerChangeDetail(tmplId);

            //jsp无法使用detail.typeMap.10,下面包装一下：detail.typeMap.a10
           /* Map<String, Double> detailTypeMap = serverChargeDetail.getTypeMap();
            if(detailTypeMap != null){
                Map<String, Double> typeMap = new HashMap<>();
                for (String code : detailTypeMap.keySet()) {
                    typeMap.put("a"+code,detailTypeMap.get(code));
                }
                model.addAttribute("typeMap", typeMap);
            }*/

            model.addAttribute("typeMap", serverChargeDetail.getTypeMap());

            String copy = super.getStringValue("copy");
            if ("Y".equals(copy)) {
                serverChargeDetail.setTmplId(null);
                serverChargeDetail.setName(null);
            }
            model.addAttribute("detail", serverChargeDetail);
            String detailTypeMapStr = JsonUtils.getGson().toJson(serverChargeDetail.getTypeMap());
            model.addAttribute("detailTypeMap", detailTypeMapStr);
        }
        return "pss/serverCharge/detail.base";
    }

    /**
     * 保存
     * @return
     */
    @RequestMapping("save")
    @ResponseBody
    public Map<String, Object> save() {
        String json = super.getStringValue("data");
        ServerChargeDetailsDto serverChargeDetailDto = JsonUtils.getGson().fromJson(json, ServerChargeDetailsDto.class);
        Map<String, Object> map = new HashMap<>();
        serverChargeDetailDto.setCurUserId(super.getCurrentUser().getId());
        int rs = serverChargeService.saveOrUpdate(serverChargeDetailDto);
        map.put("code",rs>0?"Y":"N");
        return map;
    }

    /**
     * 检查模版名称
     * @return
     */
    @RequestMapping("checkTmplName")
    @ResponseBody
    public Map<String,Object> checkTmplName(){
        String tmplName = super.getStringValue("tmplName");
        String code = "N";
        if (StringUtils.isNotEmpty(tmplName)) {
            Integer tmplId = super.getIntegerValue("tmplId");
            Integer count = serverChargeService.countName(tmplName,tmplId);
            code = count > 0?"N":"Y";
        }
        Map<String, Object> rs = new HashMap<>();
        rs.put("code", code);
        return rs;
    }

    /**
     * 模版关联的企业
     * @param model
     * @return
     */
    @RequestMapping("relPartner")
    public String toRelPartner(Model model){
        Integer tmplId = super.getIntegerValue("tmplId");
        if (tmplId == null) {
            //return "forward:";
            return "pss/serverCharge/relPartner.base";
        }
        Integer partnerStatus = super.getIntegerValue("partnerStatus");
        String partnerName = super.getStringValue("partnerName");
        Integer pageIndex = super.getIntegerValue("pageIndex");
        Integer pageSize = super.getIntegerValue("pageSize");
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = 15;
        }
        if(partnerStatus != null && -1 == partnerStatus){
            partnerStatus = null;
        }
        RelPartnerQueryDto relPartnerQueryDto = new RelPartnerQueryDto();
        relPartnerQueryDto.setTmplId(tmplId);
        relPartnerQueryDto.setPartnerStatus(partnerStatus);
        relPartnerQueryDto.setPartnerName(partnerName);
        QueryDto<RelPartnerQueryDto> queryDto = new QueryDto<>(pageIndex,pageSize);
        queryDto.setCondition(relPartnerQueryDto);
        queryDto.setSidx("rel_time");
        queryDto.setSord("desc");
        Page<RelPartnerDto> pageList = serverChargeService.queryRelPartner(queryDto);
        ServerChargeDto serverChargeDto = serverChargeService.getServerChargeTmpl(tmplId);
        String tmplName = serverChargeDto == null ? null : serverChargeDto.getName();

        Map<Long, String> userNameMap = new HashMap<>();
        List<RelPartnerDto> list = pageList.getList();
        for (int i = 0; i < list.size(); i++) {
            RelPartnerDto partnerDto = list.get(i);
            String userName = this.getUserName(partnerDto.getRelUser(), userNameMap);
            partnerDto.setRelUserName(userName);
        }

        model.addAttribute("pageList", pageList);
        model.addAttribute("tmplName", tmplName);

        model.addAttribute("tmplId", tmplId);
        model.addAttribute("partnerStatus", partnerStatus);
        model.addAttribute("partnerName", partnerName);
        return "pss/serverCharge/relPartner.base";
    }

    /**
     * 分页查询企业
     * @return
     */
    @RequestMapping("searchRelPartner")
    @ResponseBody
    public Map<String,Object> searchRelPartner(){
        Integer pageIndex = super.getIntegerValue("pageIndex");
        Integer pageSize = super.getIntegerValue("pageSize");
        if(pageIndex == null){
            pageIndex = 0;
        } else {
            pageIndex--;
        }
        if(pageSize == null){
            pageSize = 10;
        }

        Map<String,Object> params=new HashMap<>();
        params.put("start", pageIndex * pageSize);
        params.put("pageSize", pageSize);

        String partnerName = super.getStringValue("name");
        if(StringUtils.isNotEmpty(partnerName)){
            String firstSpell = partnerService.getFirstSpell(partnerName);
            params.put("name", partnerName);
            params.put("code", firstSpell);

        }

        List<PartnerDto> partnerList = partnerService.getPartnerList(params);
        Integer count = partnerService.getPartnerCount(params);

        List<RelPartnerForm> relPartnerForms = new ArrayList<>();
        for (int i = 0; i < partnerList.size(); i++) {
            PartnerDto partnerDto = partnerList.get(i);
            RelPartnerForm relPartnerForm = new RelPartnerForm();
            relPartnerForm.setPartnerId(partnerDto.getId());
            relPartnerForm.setPartnerName(partnerDto.getName());
            relPartnerForm.setPartnerStatus(partnerDto.getStatus());

            ServerChargeDto serverChargeDto = serverChargeService.getServerChargeByPartner(partnerDto.getId());
            if (serverChargeDto != null) {
                relPartnerForm.setTmplId(serverChargeDto.getId());
                relPartnerForm.setTmplName(serverChargeDto.getName());
            }
            relPartnerForms.add(relPartnerForm);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("pagelist",new Page<>(pageIndex * pageSize,pageSize,relPartnerForms,count));
        return result;
    }

    /**
     * 将一批企业移入到另一个服务费模版里
     * @return
     */
    @RequestMapping("moveIn")
    @ResponseBody
    public Map<String,Object> moveIn(String relPartnerFormJson){
        Map<String, Object> result = new HashMap<>();
        int count = 0;
        if (StringUtils.isNotEmpty(relPartnerFormJson)) {
            List<RelPartnerForm> relPartnerForms = JsonUtils.getGson().fromJson(relPartnerFormJson, new TypeToken<List<RelPartnerForm>>(){}.getType());
            List<RelPartnerDto> relPartnerDtos = new ArrayList<>();
            for (int i = 0; i < relPartnerForms.size(); i++) {
                RelPartnerForm form = relPartnerForms.get(i);
                RelPartnerDto relPartnerDto = new RelPartnerDto();
                relPartnerDto.setPartnerId(form.getPartnerId());
                relPartnerDto.setPartnerName(form.getPartnerName());
                relPartnerDto.setPartnerStatus(form.getPartnerStatus());

                relPartnerDto.setTmplId(form.getTmplId());
                relPartnerDto.setRelUser(super.getCurrentUser().getId());
                relPartnerDto.setRelTime(new Date());
                relPartnerDtos.add(relPartnerDto);
            }
            serverChargeService.addRelPartner(relPartnerDtos);
            count = relPartnerDtos.size();
        }
        result.put("count", count);
        return result;
    }

}
