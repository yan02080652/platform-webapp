package com.tem.pss.partner.priceRule.action;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.car.api.service.ICarPriceRuleService;
import com.tem.car.dto.priceRule.CarPriceRuleGroupDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.pss.api.PriceRuleApplyService;
import com.tem.pss.dto.PriceRuleApplyDto;
import com.tem.pss.dto.PriceRuleCondition;
import com.tem.pss.partner.priceRule.form.PriceRuleApplyFrom;
import com.tem.pss.partner.priceRule.form.PriceRuleApplyGroupFrom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("partner/priceRule")
public class ApplyPartnerPriceRuleController extends BaseController {

    @Autowired
    private ICarPriceRuleService carPriceRuleService;
    @Autowired
    private PriceRuleApplyService priceRuleApplyService;
    @Autowired
    private PartnerService partnerService;
    @RequestMapping(value = "")
    public String index(Model model) {
        return "pss/partner/priceRule/apply.base";
    }

    @RequestMapping(value = "applyList")
    public String getApplyList(Model model, PriceRuleCondition condition, Integer pageIndex){
        QueryDto<PriceRuleCondition> queryDto = new QueryDto<>(pageIndex, 10);
        queryDto.setCondition(condition);
        Page<PriceRuleApplyDto> pageList = priceRuleApplyService.applyList(queryDto).getData();
        for(int i=0;i<pageList.getList().size();i++){
            PriceRuleApplyDto dto=pageList.getList().get(i);
            CarPriceRuleGroupDto priceRuleGroupDto = carPriceRuleService.getGroup(Long.valueOf(dto.getGroupId())).getData();
            if(priceRuleGroupDto!=null){
                dto.setGroupName(priceRuleGroupDto.getName());
            }
        }
        System.out.println("pageList:" + JSONObject.toJSON(pageList));
        model.addAttribute("pageList", pageList);
        return "pss/partner/priceRule/applyList.jsp";
    }
    //获取详情
    @RequestMapping(value="apply")
    public String getApply(Long id, Model model){
        PriceRuleApplyDto dto=priceRuleApplyService.getApply(id).getData();
        PriceRuleApplyFrom applyFrom=TransformUtils.transform(dto,PriceRuleApplyFrom.class);
        CarPriceRuleGroupDto priceRuleGroupDto = carPriceRuleService.getGroup(Long.valueOf(dto.getGroupId())).getData();
        applyFrom.setGroupName(priceRuleGroupDto.getName());
        model.addAttribute("applyDto",applyFrom);
        return "pss/partner/priceRule/applyDetail.jsp";
    }
    //删除
    @ResponseBody
    @RequestMapping(value="deleteApply",method = RequestMethod.POST)
    public Message deleteApply(Long id){
        ResponseInfo res=priceRuleApplyService.delApply(id);
        if("0".equals(res.getCode())){
            return  Message.success("删除成功");
        }
        return Message.error("删除失败，请刷新重试");
    }
    //保存
    @ResponseBody
    @RequestMapping(value="saveApply",method = RequestMethod.POST)
    public Message saveApply(PriceRuleApplyDto dto){
        ResponseInfo responseInfo = priceRuleApplyService.addOrUpdateApply(dto);
        if(responseInfo.isSuccess()){
            return  Message.success("修改成功");
        }
        return Message.error("修改失败: " + responseInfo.getMsg());
    }

    /**
     * 获取企业的价格策略组
     * @param priceRuleCondition
     * @param id 企业id
     * @return
     */
    @RequestMapping(value="getPartnerPriceRuleGroup")
    public String getPartnerPriceRuleGroup(Model model, PriceRuleCondition priceRuleCondition,Long id,Integer pageIndex){
        QueryDto<PriceRuleCondition> queryDto=new QueryDto<>(pageIndex, 10);
        PartnerDto partner = partnerService.findById(id);
        model.addAttribute("partner",partner);
        priceRuleCondition.setPartnerId(id);
        queryDto.setCondition(priceRuleCondition);
        Page<CarPriceRuleGroupDto>  groupDtoPage=carPriceRuleService.groupList(queryDto).getData();

        Page<PriceRuleApplyDto> applyList = priceRuleApplyService.applyList(queryDto).getData();

        List<String> applyGroup = new ArrayList<>();
        for(PriceRuleApplyDto apply : applyList.getList()){
            applyGroup.add(apply.getGroupId().toString());
        }

        List<CarPriceRuleGroupDto> newGroup = new ArrayList<>();
        int count = groupDtoPage.getTotal();
        for(CarPriceRuleGroupDto group : groupDtoPage.getList()){
            if(applyGroup.contains(group.getId().toString())){
                count--;
                continue;
            }
            newGroup.add(group);
        }

        groupDtoPage = new Page<>(pageIndex,10,newGroup,count);
        model.addAttribute("pageList", groupDtoPage);
        return "pss/partner/priceRule/applyPartnerPriceRuleGroup.jsp";
    }

    /**
     * 保存企业价格策略组信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value="saveApplyPartnerPriceRuleGroup")
    public Message saveApplyPartnerPriceRuleGroup(String formList){
        System.out.println("formList: "+formList);
        List<PriceRuleApplyGroupFrom> list = JSON.parseArray(formList,PriceRuleApplyGroupFrom.class);
        ResponseInfo res = null;
        for(PriceRuleApplyGroupFrom apply:list) {
            PriceRuleApplyDto priceRuleApplyDto=TransformUtils.transform(apply,PriceRuleApplyDto.class);
            priceRuleApplyDto.setPartnerName(partnerService.getNameById(priceRuleApplyDto.getPartnerId()));
            priceRuleApplyDto.setOtaType(OrderBizType.CAR);
            res=priceRuleApplyService.addOrUpdateApply(priceRuleApplyDto);
            if (!res.isSuccess()){
                return Message.error(String.format("策略组id %s 保存失败: %s",apply.getGroupId(),res.getMsg()));
            }
        }
        return Message.success("保存成功");
    }

}
