package com.tem.product.form;

import com.tem.product.dto.ProductCategoryDto;
import com.tem.product.dto.ProductDto;

import java.util.List;

public class ProductCategoryFrom  extends ProductCategoryDto{

	private static final long serialVersionUID = 1L;
	
	private List<ProductDto> productDto;


	public List<ProductDto> getProductDto() {
		return productDto;
	}

	public void setProductDto(List<ProductDto> productDto) {
		this.productDto = productDto;
	}
    
}