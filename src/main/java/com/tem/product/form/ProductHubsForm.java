package com.tem.product.form;

import com.tem.product.dto.ProductHubsDto;

public class ProductHubsForm extends ProductHubsDto{
	
	private static final long serialVersionUID = 1L;
	
	private String idAndCodeAndPCode;

	public String getIdAndCodeAndPCode() {
		return idAndCodeAndPCode;
	}

	public void setIdAndCodeAndPCode(String idAndCodeAndPCode) {
		this.idAndCodeAndPCode = idAndCodeAndPCode;
	}
	
}
