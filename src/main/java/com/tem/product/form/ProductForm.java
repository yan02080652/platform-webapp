package com.tem.product.form;

import java.io.Serializable;

/**
 * @author chenjieming
 *
 */
public class ProductForm implements Serializable{
	

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String name;

	private String providerCode;

	private String providerProductCode;

	private boolean flag;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getProviderProductCode() {
		return providerProductCode;
	}

	public void setProviderProductCode(String providerProductCode) {
		this.providerProductCode = providerProductCode;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
}
