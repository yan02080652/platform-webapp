package com.tem.product.form;

import java.io.Serializable;

/**
 * 产品场景
 * @author chenjieming
 *
 */
public class Scence implements Serializable{
	

	private static final long serialVersionUID = 1L;

	/**
	 * 场景名称
	 */
	private String senceName;
	
	/**
	 * 场景编码
	 */
	private String senceCode;
	
	/**
	 * 商品场景Id
	 */
	private Integer scenceId;
	
	/**
	 * 是否保存
	 */
	private String isChecked;


	public Integer getScenceId() {
		return scenceId;
	}

	public void setScenceId(Integer scenceId) {
		this.scenceId = scenceId;
	}

	public String getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(String isChecked) {
		this.isChecked = isChecked;
	}

	public String getSenceName() {
		return senceName;
	}

	public void setSenceName(String senceName) {
		this.senceName = senceName;
	}

	public String getSenceCode() {
		return senceCode;
	}

	public void setSenceCode(String senceCode) {
		this.senceCode = senceCode;
	}
	

}
