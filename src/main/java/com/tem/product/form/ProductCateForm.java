package com.tem.product.form;

import com.tem.product.dto.ProductCategoryDto;

import java.util.List;

public class ProductCateForm extends ProductCategoryDto{
	
	private static final long serialVersionUID = 1L;
	
	private List<Scence> scence;

	public List<Scence> getScence() {
		return scence;
	}

	public void setScence(List<Scence> scence) {
		this.scence = scence;
	}
	
}
