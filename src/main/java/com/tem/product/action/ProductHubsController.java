package com.tem.product.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.product.api.ProductHubsService;
import com.tem.product.api.ProductService;
import com.tem.product.dto.ProductCondition;
import com.tem.product.dto.ProductDto;
import com.tem.product.dto.ProductHubsDto;
import com.tem.platform.action.BaseController;
import com.tem.product.form.ProductForm;
import com.tem.product.form.ProductHubsForm;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@RequestMapping("product/hubs")
@Controller
public class ProductHubsController extends BaseController {

	@Autowired
	private ProductHubsService productHubsService;
	
	@Autowired
	private ProductService productService;

	@Autowired
	private IssueChannelService issueChannelService;
	
	@RequestMapping(value="")
	public String index(Model model){
		
		List<ProductHubsDto> findProductHubs = productHubsService.findAllHubs();
		
		model.addAttribute("productHubs", findProductHubs);
		return "product/hubs/hubs.base";
	}
	
	@RequestMapping(value="detail")
	public String detail(Model model, String hubsCode){

		QueryDto<ProductCondition> condition = new QueryDto<ProductCondition>(1, 100);
		condition.setCondition(new ProductCondition());
		Page<ProductDto> pageList = productService.queryListWithPage(condition);

		List<ProductForm> productForms = TransformUtils.transformList(pageList.getList(), ProductForm.class);

		if(hubsCode != null){
			List<ProductHubsDto> findProductHubs = productHubsService.findHubsByCode(hubsCode);
			
			model.addAttribute("hubsDto", findProductHubs.get(0));

			for(ProductHubsDto dto : findProductHubs){
				for (ProductForm form : productForms){
					if(dto.getProductId().equals(form.getId() + "")){
						form.setFlag(true);
						break;
					}
				}
			}
		}
		List<IssueChannelDto> issueChannels = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(), IssueChannelType.INSURANCE.getType(), LineType.ON_LINE.getType());

		model.addAttribute("issueChannels",issueChannels);
		model.addAttribute("productList", productForms);
		
		return "product/hubs/hubsDetail.jsp";
	}
	
	@RequestMapping(value="right")
	public String right(Model model, String hubsCode){
		
		List<ProductHubsDto> findProductHubs = productHubsService.findHubsByCode(hubsCode);
		
		
		for (ProductHubsDto productHubsDto : findProductHubs) {
			
			Integer productId = Integer.valueOf(productHubsDto.getProductId());
			
			ProductDto productDto = productService.getById(productId);
			
			productHubsDto.setProductName(productDto.getName());
			productHubsDto.setProviderName(productDto.getProvider().getName());
			
		}
		
		model.addAttribute("productHubs", findProductHubs);
		
		return "product/hubs/hubsRight.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value="save")
	public Message save(Model model,ProductHubsForm product){
		
		String	productCode = product.getIdAndCodeAndPCode();
		
		
		String[] split = productCode.split(",");
		Integer strLength = split.length;
		Integer strIndex = strLength/3;
		
		if("0".equals(product.getFlag() + ""))
		{
			productHubsService.delete(product.getHubsCode());
		}
		
		for(int i=0 ; i< strIndex ; i++)
		{
			ProductHubsDto hubs= new ProductHubsDto();
			hubs = TransformUtils.transform(product, ProductHubsDto.class);
			hubs.setProductId(split[i * 3]);
			hubs.setProductCode(split[i * 3 + 1]);
			hubs.setProviderCode(split[i * 3 + 2]);
			hubs.setFlag(1);
			productHubsService.save(hubs);
		}
		
		return Message.success("保存成功");
	}
	
	@ResponseBody
	@RequestMapping(value="delete")
	public Message delete(Model model,String hubsCode){
		
		productHubsService.delete(hubsCode);
		
		return Message.success("删除成功");
	}
	
	@ResponseBody
	@RequestMapping(value="checkCode")
	public boolean checkCode(Model model,String hubsCode, Integer flag){
		if(flag == 0){
			return true;
		}
		
		List<ProductHubsDto> findProductHubs = productHubsService.findHubsByCode(hubsCode);
		
		if(findProductHubs != null && findProductHubs.size() > 0){
			
			return false;
		}

		return true;
	}
	
}