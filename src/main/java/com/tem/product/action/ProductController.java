package com.tem.product.action;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.common.utils.StringUtils;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.product.api.ProductCategoryService;
import com.tem.product.api.ProductService;
import com.tem.product.dto.*;
import com.tem.platform.action.BaseController;
import com.tem.product.form.ProductCategoryFrom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 商品维护
 * @author chenjieming
 */
@RequestMapping("product")
@Controller
public class ProductController extends BaseController {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductCategoryService productCategoryService;

	private final static String AVAILABLE = "available";

	private final static String COMPANY_PAY = "companyPay";

	private final static String FORCED_BUY = "forcedBuy";

	private final static String DEFAULT_CHECKED = "defaultChecked";
	
	@RequestMapping()
	public String index(){

		return "product/manage/index.base";
	}

	//加载列表
	@RequestMapping(value="list")
	public String list(ProductCondition condition,Integer pageIndex, Model model){

		QueryDto<ProductCondition> queryDto=new QueryDto<>(pageIndex,Page.DEFAULT_PAGE_SIZE);
		queryDto.setCondition(condition);

		Page<ProductDto> pageList=productService.queryListWithPage(queryDto);
		model.addAttribute("pageList", pageList);
		
		return "product/manage/list.jsp";
	}
	
	@RequestMapping(value="detail",method=RequestMethod.POST)
	public String detail(Model model, Integer productId, String providerCode, String categoryId){
		
		ProductDto product=productService.getById(productId);
		model.addAttribute("product", product);

		if(StringUtils.isEmpty(providerCode) && product != null){
			providerCode = product.getProviderCode();
		}

		if(product != null){
			categoryId = product.getCategoryId();
		}
		model.addAttribute("categoryId", categoryId);
		
		List<ProviderDto> providerList = productService.findAllProvider();
		if(CollectionUtils.isNotEmpty(providerList)){
			for (ProviderDto providerDto : providerList) {
				if(providerDto.getCode() != null && providerDto.getCode().equals(providerCode)){
					model.addAttribute("providerDto", providerDto);
					break;
				}
			}
		}
		
		return "product/manage/model.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value="save",method = RequestMethod.POST)
	public Message save(ProductDto product){
		
		productService.save(product);
		
		return Message.success("保存成功");
	}
	
	@ResponseBody
	@RequestMapping(value="delete/{id}",method=RequestMethod.POST)
	public Message delete(@PathVariable(value="id") Integer id){
		if(productService.delete(id)){
			return Message.success("删除成功");
		}
		return Message.error("删除失败");
	}
	
	@RequestMapping(value="config")
	public String config(Model model){

		model.addAttribute("superAdmin",super.isSuperAdmin());
		return "product/config/index.base";
	}
	
	@ResponseBody
	@RequestMapping(value="config/list")
	public List<ProductCategoryFrom> configList(Long partnerId1){
		
		List<ProductCategoryDto> productCategoryList= productCategoryService.findList();
		
		List<ProductCategoryFrom> pCFList = TransformUtils.transformList(productCategoryList, ProductCategoryFrom.class);
		
		for (ProductCategoryFrom productCategoryFrom : pCFList) {
			
			Integer catagoryId = productCategoryFrom.getId();
			
			QueryDto<ProductCondition> queryDto=new QueryDto<ProductCondition>(1,Integer.MAX_VALUE);
			
			ProductCondition condition=new ProductCondition();
			condition.setCategoryId(catagoryId + "");
			
			queryDto.setCondition(condition);
			
			Page<ProductDto> pageList= productService.queryListWithPage(queryDto);
			
			List<ProductDto> productList = pageList.getList();

			for(ProductDto productDto : productList){
				
				Integer productId = productDto.getId();

				ProductConfigDto config = productService.getByProductIdAndPId(Long.valueOf(productId), partnerId1);

				productDto.setProductConfig(config);
			}
			
			productCategoryFrom.setProductDto(productList);
			
		}
		return pCFList;
	}
	
	/**
	 * 根据productId和
	 * @param productId
	 * @param ischecked
	 * @param type
	 */
	@ResponseBody
	@RequestMapping(value="config/check")
	public void productConfig(Long productId, boolean ischecked, String type, Long partnerId1){

		ProductConfigDto config =  new	ProductConfigDto();
		config.setPartnerId(partnerId1);
		config.setProductId(productId);
		if(AVAILABLE.equals(type)) {

			config.setAvailable(ischecked ? 1:0);
		} else if(COMPANY_PAY.equals(type)) {

			config.setCompanyPay(ischecked ? 1:0);
		} else if(FORCED_BUY.equals(type)) {

			config.setForcedBuy(ischecked ? 1:0);
		}else if(DEFAULT_CHECKED.equals(type)){

			config.setDefaultChecked(ischecked ? 1 : 0);
		}

		productService.saveConfig(config);
	}
	
}