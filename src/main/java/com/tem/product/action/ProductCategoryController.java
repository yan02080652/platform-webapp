package com.tem.product.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.product.ProductConstants;
import com.tem.product.api.ProductCategoryService;
import com.tem.product.api.ProductService;
import com.tem.product.api.ProviderService;
import com.tem.product.dto.ProductCategoryDto;
import com.tem.product.dto.ProductSceneDto;
import com.tem.product.dto.ProviderDto;
import com.tem.product.form.ProductCateForm;
import com.tem.product.form.Scence;

/**
 * 产品分类
 */
@RequestMapping("product/category")
@Controller
public class ProductCategoryController extends BaseController {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductCategoryService productCategoryService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private ProviderService providerService;
	
	@ResponseBody
	@RequestMapping(value="list",method=RequestMethod.GET)
	public List<ProductCategoryDto> list(){

		List<ProductCategoryDto> productCategoryList=productCategoryService.findList();
		return productCategoryList;
	}
	
	@RequestMapping(value="detail")
	public String detail(Model model,Integer Id){
		List<ProductSceneDto> productList = new ArrayList<>();
		if(Id!=null){
			ProductCategoryDto productCategory=productCategoryService.getById(Id);
			model.addAttribute("category", productCategory);
			productList = productCategory.getProductScene();
		}
		
		List<ProviderDto> providerList = productService.findAllProvider();
		model.addAttribute("providerList", providerList);
		
		List<DictCodeDto> codeList = dictService.getCodes(ProductConstants.PRODUCT_SCENE);
		
		List<Scence> scenceList = new ArrayList<>();
		for(DictCodeDto code : codeList){
			Scence scence =	new Scence();
			scence.setSenceName(code.getItemTxt());
			scence.setSenceCode(code.getItemCode());
			
			if(!CollectionUtils.isEmpty(productList)){
				for(ProductSceneDto proSt : productList){
					if(code.getItemCode() != null && code.getItemCode().equals(proSt.getSuitScene())){
						scence.setScenceId(proSt.getId());
					}
				}
			}
			scenceList.add(scence);
		}
		model.addAttribute("scenceList", scenceList);
		
		return "product/manage/cateDetail.jsp";
	}
	
	@RequestMapping(value="add")
	public String add(Model model,@RequestParam Integer pid){
		ProductCategoryDto productCategory=new ProductCategoryDto();
		productCategory.setParentId(pid);
		model.addAttribute("category", productCategory);
		
		//List<ProviderDto> providerList = productService.findAllProvider();
		List<ProviderDto> providerList = providerService.findProvidersByTmcId(String.valueOf(super.getTmcId()));
		model.addAttribute("providerList", providerList);
		
		List<DictCodeDto> codeList = dictService.getCodes(ProductConstants.PRODUCT_SCENE);
		
		List<Scence> scenceList = new ArrayList<Scence>();
		for(DictCodeDto code : codeList){
			Scence scence =	new Scence();
			scence.setSenceName(code.getItemTxt());
			scence.setSenceCode(code.getItemCode());
			scenceList.add(scence);
		}
		model.addAttribute("scenceList", scenceList);
		
		return "product/manage/cateDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value="save")
	public Message save(Model model,ProductCateForm product){
		
		List<ProductSceneDto> scenceList = new ArrayList<ProductSceneDto>();
		
		if(CollectionUtils.isNotEmpty(product.getScence())){
			for(Scence scence : product.getScence()){
				ProductSceneDto productSceneDto = 	new	ProductSceneDto();
				if(scence != null && "on".equals(scence.getIsChecked())){
					productSceneDto.setSuitScene(scence.getSenceCode());
					scenceList.add(productSceneDto);
				}
			}
		}
		ProductCategoryDto productDto = TransformUtils.transform(product, ProductCategoryDto.class);
		productDto.setProductScene(scenceList);
		
		productCategoryService.save(productDto);

		return Message.success("保存成功");	
	}
	
	@ResponseBody
	@RequestMapping(value="delete")
	public Message delete(Model model,@RequestParam Integer Id){

		if(productCategoryService.delete(Id)){
			productService.deleteByCateId(Id);
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败");
		}
	}
}