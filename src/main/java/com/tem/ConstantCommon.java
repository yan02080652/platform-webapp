package com.tem;

/**
 * 全局变量
 * Created by pyy on 2017/10/23.
 */
public class ConstantCommon {

    /**
     * 角色编码：客服人员
     */
    public static final String IP_ROLE_CS = "CS";

    public static final String SUCCESS = "success";
    public static final String NO_CHANGE = "nochange";
    public static final String FAIL = "fail";
}
