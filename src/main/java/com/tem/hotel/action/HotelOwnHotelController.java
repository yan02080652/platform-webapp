package com.tem.hotel.action;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.transform.TransformUtils;
import com.tem.hotel.api.HotelDistrictService;
import com.tem.hotel.api.hotel.HotelBaseService;
import com.tem.hotel.api.hotel.own.OwnHotelService;
import com.tem.hotel.dto.hotel.HotelBaseDto;
import com.tem.hotel.dto.hotel.HotelDistrictDto;
import com.tem.hotel.dto.hotel.OwnHotelDto;
import com.tem.hotel.dto.hotel.enums.DistrictType;
import com.tem.hotel.dto.hotel.enums.OriginType;
import com.tem.hotel.dto.hotel.own.OwnHotelCondition;
import com.tem.hotel.dto.hotel.own.OwnSuspectDto;
import com.tem.hotel.dto.hotel.own.SuspectCondition;
import com.tem.hotel.query.HotelConditionQuery;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.dto.DistrictDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Created by heyuanjing on 18/12/10.
 */
@Controller
@RequestMapping("hotel/ownData")
public class HotelOwnHotelController extends BaseController{

    private Logger logger = LoggerFactory.getLogger(HotelOwnHotelController.class);

    @Autowired
    private OwnHotelService ownHotelService;
    @Autowired
    private HotelBaseService hotelBaseService;
    @Autowired
    private HotelDistrictService hotelDistrictService;
    /**
     * 聚合酒店首页
     * @return
     */
    @RequestMapping("own-hotel")
    public String ownHotel(Model model){
        model.addAttribute("ownHotelCondition",new OwnHotelCondition());
        model.addAttribute("originTypes", OriginType.values());
        model.addAttribute("districtTypes", DistrictType.values());
        return "hotel/ownData/hotel/index.jsp";
    }

    @RequestMapping("hotelList")
    public String hotelList(Model model,OwnHotelCondition condition,Integer pageIndex){
        if(pageIndex == null){
            pageIndex = 1;
        }
        QueryDto<OwnHotelCondition> queryDto = new QueryDto<>(pageIndex,10);
        queryDto.setCondition(condition);
        queryDto.setSidx("own_id");
        queryDto.setSord("asc");
        Page<HotelBaseDto> pageList = ownHotelService.queryOriginHotels(queryDto).getData();
        model.addAttribute("pageList",pageList);
        model.addAttribute("condition",condition);
        return "hotel/ownData/hotel/list.jsp";
    }

    @RequestMapping("hotelSuspectList")
    public String suspectList(Model model,String originId, Integer type){
        model.addAttribute("originId",originId);
        model.addAttribute("type",type);
        return "hotel/ownData/hotel/hotelSuspectList.jsp";
    }

    @RequestMapping("hotelOwnList")
    public String ownHotelList(Model model,Integer id){
        model.addAttribute("id",id);
        return "hotel/ownData/hotel/hotelOwnList.jsp";
    }

    @RequestMapping("hotelSuspectTable")
    public String hotelSuspectTable(Model model,SuspectCondition condition,Integer pageIndex,Integer pageSize){

        if(pageIndex == null){
            pageIndex = 1;
        }
        HotelBaseDto hotelBaseDto = hotelBaseService.findHotelById(condition.getOriginId()).getData();
        HotelDistrictDto districtDto = hotelDistrictService.queryByCode(hotelBaseDto.getCityId(),hotelBaseDto.getOrigin()).getData();
        OwnSuspectDto ownSuspectDto = OwnSuspectDto.toDto(hotelBaseDto);
        ownSuspectDto.setOwnCityName(districtDto == null ? "" : districtDto.getDistrictName());
        QueryDto<SuspectCondition> queryDto = new QueryDto(pageIndex,9);
        queryDto.setCondition(condition);
        Page<OwnSuspectDto> pageList=ownHotelService.suspectList(queryDto).getData();
        if(pageList.getList() == null){
            pageList.setList(new ArrayList<OwnSuspectDto>());
        }
        pageList.getList().add(0,ownSuspectDto);
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/hotel/hotelSuspectTable.jsp";
    }

    @RequestMapping("hotelOwnTable")
    public String ownHotelTable(Model model,OwnHotelCondition condition,Integer id,Integer pageIndex){
        if(pageIndex == null){
            pageIndex = 1;
        }
        HotelBaseDto hotelBaseDto = hotelBaseService.findHotelById(String.valueOf(id)).getData();
        if(hotelBaseDto.getOwnId() == 0){
            model.addAttribute("pageList",new Page<HotelBaseDto>());
            return "hotel/ownData/hotel/hotelOwnTable.jsp";
        }
        HotelDistrictDto districtDto = hotelDistrictService.queryByCode(hotelBaseDto.getCityId(),hotelBaseDto.getOrigin()).getData();
        hotelBaseDto.setCityName(districtDto == null ? "" :districtDto.getNameCn());
        QueryDto<OwnHotelCondition> queryDto = new QueryDto<>(pageIndex,9);
        condition.setOwnId(hotelBaseDto.getOwnId());
        condition.setNotId(String.valueOf(id));
        queryDto.setSidx("id");
        queryDto.setSord("asc");
        queryDto.setCondition(condition);
        Page<HotelBaseDto> pageList = ownHotelService.queryOriginHotels(queryDto).getData();
        if(pageList.getList() == null){
            pageList.setList(new ArrayList<HotelBaseDto>());
        }
        pageList.getList().add(0,hotelBaseDto);
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/hotel/hotelOwnTable.jsp";
    }
    /**
     * 脱离聚合
     * @param id
     * @return
     */
    @RequestMapping(value = "awayHotel",method = RequestMethod.POST)
    @ResponseBody
    public ResponseInfo awayHotel(Integer id){
         return ownHotelService.awayHotel(id);
    }

    /**
     * 新增聚合
     * @param id
     * @return
     */
    @RequestMapping("addOwnHotel")
    public String addOwnHotel(Model model,Integer id){
        model.addAttribute("id",id);
        HotelBaseDto hotelBaseDto = hotelBaseService.findHotelById(String.valueOf(id)).getData();
        HotelDistrictDto districtDto = hotelDistrictService.queryByCode(hotelBaseDto.getCityId(),hotelBaseDto.getOrigin()).getData();
        model.addAttribute("hotel",hotelBaseDto);
        if(districtDto != null) {
            model.addAttribute("ownCityId", districtDto.getDistrictId());
            model.addAttribute("cityName", districtDto.getDistrictName());
            List<DistrictDto> list = new ArrayList<DistrictDto>();
            list.add(districtDto.toDistrictDto());
            model.addAttribute("city", JSONObject.toJSON(list));
        }else{
            model.addAttribute("ownCityId", "");
            model.addAttribute("cityName", "");
            model.addAttribute("city", null);
        }
        return "hotel/ownData/hotel/addOwnHotel.jsp";
    }

    /**
     * 根据酒店民查询聚合酒店
     * @param hotelName
     * @return
     */
    @RequestMapping("loadOwnHotelData")
    public String loadOwnHotelData(Model model,String hotelName,String ownCityIds,Integer origin_hotel_id,Integer pageIndex, Integer pageSize){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        HotelBaseDto hotelBaseDto = hotelBaseService.findHotelById(String.valueOf(origin_hotel_id)).getData();
        OwnHotelDto ownHotelDto = TransformUtils.transform(hotelBaseDto,OwnHotelDto.class);
        HotelDistrictDto districtDto = hotelDistrictService.queryByCode(hotelBaseDto.getCityId(),hotelBaseDto.getOrigin()).getData();
        ownHotelDto.setId(0l);
        ownHotelDto.setCityName(districtDto == null ? "" :districtDto.getNameCn());
        QueryDto<HotelConditionQuery> queryDto = new QueryDto<>(pageIndex,9);
        HotelConditionQuery condition = new HotelConditionQuery();
        condition.setHotelName(hotelName);
        condition.setOwnCityIds(ownCityIds);
        queryDto.setCondition(condition);
        Page<OwnHotelDto> pageList = ownHotelService.queryOwnHotels(queryDto).getData();
        if(pageList.getList() == null){
            pageList.setList(new ArrayList<OwnHotelDto>());
        }
        pageList.getList().add(0,ownHotelDto);
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/hotel/ownHotelTable.jsp";
    }

    /**
     * 关联聚合酒店
     * @return
     */
    @RequestMapping("relateHotel")
    @ResponseBody
    public ResponseInfo relateHotel(Long ownId, Integer hotelId){
        return ownHotelService.relateHotel(ownId,hotelId);
    }

    /**
     * 设置为聚合酒店
     * @param hotelId
     * @return
     */
    @RequestMapping("addHotelToOwn")
    @ResponseBody
    public ResponseInfo addHotelToOwn(Integer hotelId){
         return ownHotelService.addHotelToOwn(hotelId);
    }
}
