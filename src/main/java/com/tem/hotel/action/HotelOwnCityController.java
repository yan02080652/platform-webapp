package com.tem.hotel.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.hotel.ResponseDto;
import com.tem.hotel.api.hotel.own.OwnHotelCityService;
import com.tem.hotel.api.hotel.own.OwnHotelService;
import com.tem.hotel.dto.hotel.HotelDistrictDto;
import com.tem.hotel.dto.hotel.enums.OriginType;
import com.tem.hotel.dto.hotel.own.OwnHotelCityCondition;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.dto.DistrictDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by heyuanjing on 18/12/10.
 */
@Controller
@RequestMapping("hotel/ownData")
public class HotelOwnCityController extends BaseController{

    @Autowired
    private OwnHotelService ownHotelService;

    @Autowired
    private OwnHotelCityService ownHotelCityService;

    @RequestMapping("own-city")
    public String ownCity(Model model){
        model.addAttribute("ownHotelCityCondition", new  OwnHotelCityCondition());
        model.addAttribute("originTypes", OriginType.values());
        return "hotel/ownData/city/index.jsp";
    }

    @RequestMapping("cityList")
    public String getCityList(Model model,OwnHotelCityCondition condition,Integer pageIndex,Integer pageSize){
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<OwnHotelCityCondition> query =new QueryDto<>(pageIndex,pageSize);
        query.setCondition(condition);
        query.setSidx("d.district_id");
        query.setSord("asc");
        Page<HotelDistrictDto> pageList=ownHotelCityService.queryOriginCityList(query).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/city/list.jsp";
    }

    /**
     * 脱离聚合
     * @param id
     * @return
     */
    @RequestMapping("awayCity")
    @ResponseBody
    public ResponseInfo awayCity(Integer id){
        return ownHotelService.awayCity(id);
    }

    /**
     * 新增聚合
     * @param id
     * @return
     */
    @RequestMapping("addOwnHotelCity")
    public String addOwnHotelCity(Model model,Integer id,String name,String originName){
        model.addAttribute("id",id);
        model.addAttribute("name",name);
        model.addAttribute("originName",originName);
        return "hotel/ownData/city/addOwnHotelCity.jsp";
    }

    @RequestMapping("loadOwnCityData")
    public String loadOwnCityData(Model model,String cityName,Integer pageIndex, Integer pageSize){
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        Page<DistrictDto> pageList =ownHotelCityService.findByNameWithLimit(cityName,pageIndex, pageSize).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/city/ownHotelCityTable.jsp";
    }

    /**
     * 关联城市
     * @param ownId
     * @param cityId
     * @return
     */
    @RequestMapping("relateCity")
    @ResponseBody
    public ResponseInfo relateCity(Integer ownId,Integer cityId){
         ownHotelCityService.relateCity(ownId,cityId);
        return ResponseDto.success(null);
    }
}
