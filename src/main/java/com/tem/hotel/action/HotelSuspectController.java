package com.tem.hotel.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.hotel.ResponseDto;
import com.tem.hotel.api.hotel.own.OwnHotelService;
import com.tem.hotel.dto.hotel.own.OwnSuspectDto;
import com.tem.hotel.dto.hotel.own.SuspectCondition;
import com.tem.platform.action.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by heyuanjing on 18/12/11.
 */
@Controller
@RequestMapping("hotel/ownData")
public class HotelSuspectController extends BaseController{

    @Autowired
    private OwnHotelService ownHotelService;

    @RequestMapping("own-suspect")
    public String ownSucpect(Model model){
        model.addAttribute("suspectCondition",new SuspectCondition());
        return "hotel/ownData/suspect/index.jsp";
    }

    @RequestMapping("suspectList")
    public String suspectList(Model model,SuspectCondition condition,Integer pageIndex,Integer pageSize){
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<SuspectCondition> queryDto = new QueryDto(pageIndex,pageSize);
        queryDto.setCondition(condition);
        Page<OwnSuspectDto> pageList=ownHotelService.suspectList(queryDto).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/suspect/list.jsp";
    }

    /**
     * 添加疑似聚合
     * @param id
     * @return
     */
    @RequestMapping("addOwn")
    @ResponseBody
    public ResponseInfo addOwn(Long id){
        return ownHotelService.addSuspectToOwn(id);
    }

    /**
     * 解除疑似聚合
     * @param id
     * @return
     */
    @RequestMapping("removeSuspect")
    @ResponseBody
    public ResponseInfo removeSuspect(Long id){
        return ownHotelService.removeSuspect(id);
    }
}
