package com.tem.hotel.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.hotel.api.hotel.own.OwnHotelAmenityService;
import com.tem.hotel.api.hotel.own.OwnHotelService;
import com.tem.hotel.dto.hotel.HotelAmenityDto;
import com.tem.hotel.dto.hotel.enums.OriginType;
import com.tem.hotel.dto.hotel.own.OwnHotelAmenityCondition;
import com.tem.hotel.dto.hotel.own.OwnHotelAmenityDto;
import com.tem.platform.action.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by heyuanjing on 18/12/10.
 */
@Controller
@RequestMapping("hotel/ownData")
public class HotelOwnFacController extends BaseController{

    @Autowired
    private OwnHotelService ownHotelService;
    @Autowired
    private OwnHotelAmenityService ownHotelAmenityService;
    @RequestMapping("own-fac")
    public String ownFac(Model model){
        OwnHotelAmenityCondition condition = new OwnHotelAmenityCondition();
        condition.setAmenityType(1);
        model.addAttribute("ownHotelFacilitiesCondition",condition);
        model.addAttribute("originTypes", OriginType.values());
        return "hotel/ownData/fac/index.jsp";
    }

    @RequestMapping("own-roomfac")
    public String ownRoomFac(Model model){
        OwnHotelAmenityCondition condition = new OwnHotelAmenityCondition();
        condition.setAmenityType(2);
        model.addAttribute("ownHotelFacilitiesCondition", condition);
        model.addAttribute("originTypes", OriginType.values());
        return "hotel/ownData/fac/index.jsp";
    }

    @RequestMapping("facList")
    public String facList(Model model, OwnHotelAmenityCondition condition , Integer pageIndex, Integer pageSize){
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<OwnHotelAmenityCondition> query = new QueryDto<>(pageIndex,pageSize);
        query.setCondition(condition);
        query.setSidx("own_id");
        query.setSord("asc");
        Page<HotelAmenityDto> pageList=ownHotelAmenityService.queryOriginAmenityList(query).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/fac/list.jsp";
    }

    /**
     * 新增聚合设施
     * @param id
     * @return
     */
    @RequestMapping("addOwnHotelFac")
    public String addOwnHotelFac(Model model,Integer id,String name,Integer amenityType,String originName){
        model.addAttribute("id",id);
        model.addAttribute("name",name);
        model.addAttribute("amenityType",amenityType);
        model.addAttribute("originName",originName);
        return "hotel/ownData/fac/addOwnHotelFac.jsp";

    }

    /**
     * 关联聚合设施
     * @param ownId
     * @param id
     * @return
     */
    @RequestMapping("relateHotelFac")
    @ResponseBody
    public ResponseInfo relateHotelFac(Long ownId,Integer id){
        return ownHotelAmenityService.relateAmenity(ownId,id);
    }

    /**
     * 根据设施名称获取聚合设施
     * @param facName
     * @return
     */
    @RequestMapping("loadOwnHotelFacData")
    public String loadOwnHotelFacData(Model model,String facName,Integer amenityType,Integer pageIndex, Integer pageSize){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<HotelAmenityDto> queryDto = new QueryDto<>(pageIndex,pageSize);
        HotelAmenityDto condition = new HotelAmenityDto();
        condition.setAmenityType(amenityType);
        condition.setName(facName);
        queryDto.setSidx("id");
        queryDto.setSord("asc");
        queryDto.setCondition(condition);
        Page<OwnHotelAmenityDto> pageList = ownHotelAmenityService.queryAmenityList(queryDto).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/fac/ownHotelFacTable.jsp";
    }

    /**
     * 设置为聚合设施
     * @param id
     * @return
     */
    @RequestMapping("addHotelFacToOwn")
    @ResponseBody
    public ResponseInfo addHotelFacToOwn(Integer id){
        return ownHotelAmenityService.addAmenityToOwn(id);
    }

    /**
     * 脱离聚合
     * @param id
     * @return
     */
    @RequestMapping("awayFac")
    @ResponseBody
    public ResponseInfo awayFac(Integer id){
        return ownHotelService.awayFac(id);
    }
}
