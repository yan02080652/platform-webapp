package com.tem.hotel.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.hotel.api.hotel.HotelBrandService;
import com.tem.hotel.api.hotel.own.OwnHotelBrandService;
import com.tem.hotel.dto.hotel.HotelBrandDto;
import com.tem.hotel.dto.hotel.own.OwnHotelBrandCondition;
import com.tem.hotel.dto.hotel.own.OwnHotelBrandDto;
import com.tem.platform.action.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 酒店品牌维护
 * Created by heyuanjing on 18/10/24.
 */
@Controller
@RequestMapping("hotel/brand")
public class HotelBrandController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(HotelBrandController.class);

    @Autowired
    private OwnHotelBrandService ownHotelBrandService;

    @RequestMapping("index")
    public String index() {
        return "hotel/brand/index.base";
    }

    @RequestMapping("list")
    public String list(OwnHotelBrandCondition condition,int pageIndex, Model model){
        QueryDto<OwnHotelBrandCondition> query=new QueryDto<>(pageIndex,10);
        query.setCondition(condition);
        Page<OwnHotelBrandDto> pageList =ownHotelBrandService.findByNameWithLimit(query).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/brand/table.jsp";
    }

    /**
     * 热门切换
     * @return
     */
    @RequestMapping("changeHot")
    @ResponseBody
    public ResponseInfo changeHot(OwnHotelBrandDto brand){
        return ownHotelBrandService.updateBrand(brand);
    }
}
