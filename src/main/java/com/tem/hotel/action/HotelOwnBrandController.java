package com.tem.hotel.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.hotel.api.hotel.own.OwnHotelBrandService;
import com.tem.hotel.api.hotel.own.OwnHotelService;
import com.tem.hotel.dto.hotel.HotelBrandDto;
import com.tem.hotel.dto.hotel.enums.OriginType;
import com.tem.hotel.dto.hotel.own.OwnHotelBrandCondition;
import com.tem.hotel.dto.hotel.own.OwnHotelBrandDto;
import com.tem.platform.action.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by heyuanjing on 18/12/10.
 */
@Controller
@RequestMapping("hotel/ownData")
public class HotelOwnBrandController extends BaseController{

    @Autowired
    private OwnHotelService ownHotelService;
    @Autowired
    private OwnHotelBrandService ownHotelBrandService;

    /**
     * @return
     */
    @RequestMapping("own-brand")
    public String ownBrand(Model model){
        model.addAttribute("ownHotelBrandCondition",new OwnHotelBrandCondition());
        model.addAttribute("originTypes", OriginType.values());
        return "hotel/ownData/brand/index.jsp";
    }

    /**
     * @param model
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @RequestMapping("brandList")
    public String getBrandList(Model model,OwnHotelBrandCondition condition, Integer pageIndex, Integer pageSize){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<OwnHotelBrandCondition> query=new QueryDto<>(pageIndex,pageSize);
        query.setCondition(condition);
        query.setSidx("own_id");
        query.setSord("asc");
        Page<HotelBrandDto> pageList =ownHotelBrandService.queryOriginBrandList(query).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/brand/list.jsp";
    }

    /**
     * 脱离聚合
     * @param id
     * @return
     */
    @RequestMapping(value="awayBrand",method= RequestMethod.POST)
    @ResponseBody
    public ResponseInfo awayBrand(String id,String originType){
        return ownHotelService.awayBrand(id,originType);
    }

    /**
     * 新增聚合
     * @param id
     * @param originType
     * @return
     */
    @RequestMapping("addOwnHotelBrand")
    public String addOwnHotelBrand(Model model,String id,String originType,String name){
        model.addAttribute("id",id);
        model.addAttribute("originType",originType);
        model.addAttribute("name",name);
        return "hotel/ownData/brand/addOwnHotelBrand.jsp";
    }

    /**
     * 根据品牌名称查询聚合品牌
     * @return
     */
    @RequestMapping("loadOwnHotelBrandData")
    public String loadOwnHotelBrandData(Model model,OwnHotelBrandCondition condition, Integer pageIndex, Integer pageSize){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<OwnHotelBrandCondition> query = new QueryDto<>(pageIndex, pageSize);
        query.setCondition(condition);
        query.setSidx("id");
        query.setSord("asc");
        Page<OwnHotelBrandDto> pageList = ownHotelBrandService.findByNameWithLimit(query).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/brand/ownHotelBrandTable.jsp";
    }

    /**
     * 关联聚合品牌
     * @return
     */
    @RequestMapping("relateBrand")
    @ResponseBody
    public ResponseInfo relateBrand(Long ownId, String brandId,String originType){
        return ownHotelBrandService.relateBrand(ownId,brandId,originType);
    }

    /**
     * 设置为聚合品牌
     * @param brandId
     * @param originType
     * @return
     */
    @RequestMapping("addBrandToOwn")
    @ResponseBody
    public ResponseInfo addBrandToOwn(String brandId,String originType){
        return ownHotelBrandService.addBrandToOwn( brandId, originType);
    }
}
