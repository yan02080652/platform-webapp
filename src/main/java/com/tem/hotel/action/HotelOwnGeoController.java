package com.tem.hotel.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.hotel.api.hotel.GeoService;
import com.tem.hotel.api.hotel.own.OwnHotelGeoService;
import com.tem.hotel.api.hotel.own.OwnHotelService;
import com.tem.hotel.dto.hotel.HotelGeoDto;
import com.tem.hotel.dto.hotel.enums.OriginType;
import com.tem.hotel.dto.hotel.own.OwnHotelGeoCondition;
import com.tem.hotel.dto.hotel.own.OwnHotelGeoDto;
import com.tem.platform.action.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by heyuanjing on 18/12/10.
 */
@Controller
@RequestMapping("hotel/ownData")
public class HotelOwnGeoController extends BaseController{

    @Autowired
    private OwnHotelService ownHotelService;
    @Autowired
    private OwnHotelGeoService ownHotelGeoService;
    @RequestMapping("own-geo")
    public String ownGeo(Model model){
        model.addAttribute("ownHotelGeoCondition",new OwnHotelGeoCondition());
        model.addAttribute("originTypes", OriginType.values());
        return "hotel/ownData/geo/index.jsp";
    }

    @RequestMapping("geoList")
    public String geoList(Model model, OwnHotelGeoCondition condition, Integer pageIndex, Integer pageSize ){
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<OwnHotelGeoCondition> query= new QueryDto<>(pageIndex,pageSize);
        query.setCondition(condition);
        query.setSidx("own_id");
        query.setSord("asc");
        Page<HotelGeoDto> pageList = ownHotelGeoService.queryOriginGeoList(query).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/geo/list.jsp";
    }

    /**
     * 新增聚合商圈
     * @param id
     * @return
     */
    @RequestMapping("addOwnHotelGeo")
    public String addOwnHotelGeo(Model model,Integer id,String geoName,String originName){
        model.addAttribute("id",id);
        model.addAttribute("name",geoName);
        model.addAttribute("originName",originName);
        Integer ownCityId = ownHotelGeoService.getOwnCityIdByGeo(id).getData();
        model.addAttribute("ownCityId",ownCityId);
        return "hotel/ownData/geo/addOwnHotelGeo.jsp";
    }

    /**
     * 关联聚合商圈
     * @return
     */
    @RequestMapping("relateHotelGeo")
    @ResponseBody
    public ResponseInfo relateHotelGeo(Long ownId,Integer geoId){
        return ownHotelGeoService.relateGeo(ownId,geoId);
    }

    /**
     * 根据商圈名查询聚合商圈
     * @return
     */
    @RequestMapping("loadOwnHotelGeoData")
    public String loadOwnHotelGeoData(Model model,OwnHotelGeoDto condition,Integer pageIndex, Integer pageSize ){
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<OwnHotelGeoDto> query = new QueryDto<>(pageIndex,pageSize);
        query.setCondition(condition);
        query.setSidx("g.id");
        query.setSord("asc");
        Page<OwnHotelGeoDto> pageList=ownHotelGeoService.queryOwnHotelGeoList(query).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/ownData/geo/ownHotelGeoTable.jsp";
    }

    /**
     * 设置为聚合商圈
     * @param geoId
     * @return
     */
    @RequestMapping("addHotelGeoToOwn")
    @ResponseBody
    public ResponseInfo addHotelGeoToOwn(Integer geoId){
        return ownHotelGeoService.addGeoToOwn(geoId);
    }

    /**
     * 脱离聚合
     * @param id
     * @return
     */
    @RequestMapping(value = "awayGeo",method = RequestMethod.POST)
    @ResponseBody
    public ResponseInfo awayGeo(Integer id){
        return ownHotelService.awayGeo(id);
    }

}
