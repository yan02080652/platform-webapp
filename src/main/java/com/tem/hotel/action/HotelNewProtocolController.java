package com.tem.hotel.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.hotel.api.hotel.HotelAggregationService;
import com.tem.hotel.api.protocol.HotelNewProtocolService;
import com.tem.hotel.dto.hotel.protocol.*;
import com.tem.hotel.dto.hotel.room.HotelRoomDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerBpService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 协议酒店控制器
 * Created by heyuanjing on 18/6/26.
 */
@Controller
@RequestMapping("hotel/newProtocol")
public class HotelNewProtocolController extends BaseController {

    public Logger logger = LoggerFactory.getLogger(HotelNewProtocolController.class);

    @Autowired
    private HotelAggregationService hotelAggregationService;
    @Autowired
    private HotelNewProtocolService hotelNewProtocolService;
    @Autowired
    private PartnerBpService partnerBpService;

    /**
     * 首页
     */
    @RequestMapping("index")
    public String index() {
        return "hotel/new_protocol/index.base";
    }

    /**
     * 协议酒店列表
     * @return
     */
    @RequestMapping("list")
    public String list(AggregationQuery condition,Integer pageIndex, Model model){
        QueryDto<AggregationQuery> query = new QueryDto(pageIndex,10);
        condition.setcIds(partnerBpService.findByTmcId(getTmcId()));
        query.setCondition(condition);
        Page<AggregationResult> pageList = hotelAggregationService.getAggregatonList(query).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/new_protocol/protocol_table.jsp";
    }

    /**
     * 价格计划首页
     * @param query
     * @return
     */
    @RequestMapping("ratePlanIndex")
    public String ratePlanIndex(AggregationQuery query,String cName, Model model){
        model.addAttribute("condition",query);
        model.addAttribute("cName",cName);
        return "hotel/new_protocol/ratePlanIndex.base";
    }

    /**
     * 价格计划列表
     * @param condition
     * @param pageIndex
     * @return
     */
    @RequestMapping("ratePlanList")
    public String ratePlanList(HotelProtocolRatePlanQuery condition, Integer pageIndex, Model model){
        QueryDto<HotelProtocolRatePlanQuery> query = new QueryDto(pageIndex,10);
        query.setCondition(condition);
        Page<HotelProtocolRatePlanResult> pageList = hotelNewProtocolService.getRatePlanList(query).getData();
        model.addAttribute("pageList",pageList);
        if(pageList !=null && pageList.getTotal()>0) {
	        model.addAttribute("showLabelTemp",pageList.getList().get(0).getShowLabel());
	        model.addAttribute("isFlash","1");
        }else {
        	model.addAttribute("isFlash","0");	
        }
        return "hotel/new_protocol/ratePlanTable.jsp";
    }

    /**
     * 价格标签编辑页面
     * @param condition
     * @param pageIndex
     * @return
     */
    @RequestMapping("editShowLabel")
    public String editShowLabel(HotelProtocolRatePlanQuery condition, Model model){
     
        List<HotelProtocolRatePlanResult> pageList = hotelNewProtocolService.getAllRatePlan(condition).getData();
        if(pageList !=null && pageList.size()>0) {
        model.addAttribute("showLabel",pageList.get(0).getShowLabel());
        }
        return "hotel/new_protocol/editShowLabel.jsp";
    }
    
    
    
    /**
     * 价格计划列表
     * @param condition
     * @param pageIndex
     * @return
     */
    @RequestMapping("saveShowLabel")
    @ResponseBody
    public ResponseInfo saveShowLabel(HotelProtocolRatePlanQuery condition){
    	
        return hotelNewProtocolService.saveShowLabelOfRatePlanList(condition);
    }
    
    
    /**
     * 新增或编辑价格计划
     * @param condition
     * @return
     */
    @RequestMapping("editRatePlan")
    public String editRatePlan(HotelProtocolRatePlanResult condition, Model model){
        List<HotelRoomDto> rooms = hotelNewProtocolService.getRoomInfoByHotelOwnId(condition.getHotelId()).getData();
        Map<String,HotelRoomDto> map = new HashMap<>();
        List<HotelRoomDto> roomList =new ArrayList<>();
        if (CollectionUtils.isNotEmpty(rooms)) {
            for (HotelRoomDto room : rooms) {
                if (!map.containsKey(room.getName())) {
                    map.put(room.getName(), room);
                    roomList.add(room);
                    continue;
                }
            }
        }
        model.addAttribute("rooms",roomList);
        if (condition.getId() == null) {
            model.addAttribute("ratePlan",condition);
        } else {
            HotelProtocolRatePlanResult ratePLan = hotelNewProtocolService.getRatePlanById(condition.getId()).getData();
            model.addAttribute("ratePlan",ratePLan);
        }
        return "hotel/new_protocol/ratePlan.jsp";
    }

    /**
     * 保存价格计划
     * @param ratePlan
     */
    @RequestMapping("saveRatePlan")
    @ResponseBody
    public ResponseInfo saveRatePlan(HotelProtocolRatePlanResult ratePlan){
        if(StringUtils.isNotBlank(ratePlan.getCancelRule()) && ratePlan.getCancelRule().startsWith("其他规则")){
            ratePlan.setCancelRule(ratePlan.getCancelRule().split(",")[1]);
        }
        if(StringUtils.isNotBlank(ratePlan.getCancelRule()) && ratePlan.getCancelRule().startsWith("免费取消")){
            ratePlan.setCancelRule("免费取消");
        }
        if(StringUtils.isNotBlank(ratePlan.getCancelRule()) && ratePlan.getCancelRule().startsWith("不可取消")){
            ratePlan.setCancelRule("不可取消");
        }
        return hotelNewProtocolService.addOrUpdateRatePlan(ratePlan);
    }

    /**
     * 删除价格计划
     * @param id
     * @return
     */
    @RequestMapping("delRatePlan")
    @ResponseBody
    public ResponseInfo delRatePlan(Integer id){
         return hotelNewProtocolService.delRatePlan(id);
    }

    /**
     * 新增协议酒店
     * @return
     */
    @RequestMapping("addProtocol")
    public String addProtocol(){
        return "hotel/new_protocol/addProtocol.jsp";
    }

    /**
     * 酒店搜索
     * @param
     * @return
     */
    @RequestMapping("searchHotel")
    public String searchHotel(AggregationQuery condition,Integer pageIndex, Model model){
        QueryDto<AggregationQuery> query = new QueryDto<>(pageIndex,10);
        query.setCondition(condition);
        Page<HotelSimpleInfo> pageList = hotelNewProtocolService.searchHotel(query).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/new_protocol/addProtocol_table.jsp";
    }

}
