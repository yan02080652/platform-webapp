package com.tem.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.RedirectUrlBuilder;
import org.springframework.security.web.util.UrlUtils;

public class HttpsAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {
	public HttpsAuthenticationEntryPoint() {
		this("/login");
    }

	HttpsAuthenticationEntryPoint(String loginFormUrl) {
        super(loginFormUrl);
    }
	
	protected String buildRedirectUrlToLoginPage(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException authException) {

        String loginForm = determineUrlToUseForThisRequest(request, response, authException);

        if (UrlUtils.isAbsoluteUrl(loginForm)) {
            return loginForm;
        }

        int serverPort = super.getPortResolver().getServerPort(request);
        String scheme = request.getScheme();
        String xscheme = request.getHeader("X-Scheme");
        if(xscheme != null) {
        	scheme = xscheme;
        	if("https".equalsIgnoreCase(xscheme)) {
        		serverPort = 443;
        	}
        }

        RedirectUrlBuilder urlBuilder = new RedirectUrlBuilder();

        urlBuilder.setScheme(scheme);
        urlBuilder.setServerName(request.getServerName());
        urlBuilder.setPort(serverPort);
        urlBuilder.setContextPath(request.getContextPath());
        urlBuilder.setPathInfo(loginForm);

        return urlBuilder.getUrl();
    }
}
