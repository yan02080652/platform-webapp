package com.tem.filter.util;

import java.util.Collection;

public class IpUtil {
	/*
	 * 验证IP是否属于某个IP段
	 * 
	 * ipSection IP段（以'-'分隔）
	 * 
	 * ip 所验证的IP号码
	 */
	private static boolean ipExistsInRange(String ip, String ipSection) {
		ipSection = ipSection.trim();
		ip = ip.trim();
		int idx = ipSection.indexOf('-');
		String beginIP = ipSection.substring(0, idx);
		String endIP = ipSection.substring(idx + 1);
		return getIp2long(beginIP) <= getIp2long(ip) && getIp2long(ip) <= getIp2long(endIP);
	}
	
	/**
	 * 判断ip是否在ips指定的IP范围内
	 * @param ip 需要判断的ip地址
	 * @param ips ip地址范围，可是一个具体的IP，也可以是以'-'分隔的一个IP段
	 * @return
	 */
	public static boolean isExists(String ip, String ips) {
		if(ip == null || ips == null) {
			return false;
		}
		
		if(ips.indexOf("-") != -1) {
			return ipExistsInRange(ip, ips);
		}
		
		return ip.equals(ips);
	}
	
	/**
	 * 判断ip是否在ips指定的IP范围内
	 * @param ip 需要判断的ip地址
	 * @param ips ip地址范围列表，每个元素可是一个具体的IP，也可以是以'-'分隔的一个IP段
	 * @return
	 */
	public static boolean isExists(String ip, Collection<String> ips) {
		if(ip == null || ips == null || ips.size() == 0) {
			return false;
		}
		
		for(String ipRange : ips) {
			boolean rs = false;
			if(ipRange.indexOf("-") != -1) {
				rs = ipExistsInRange(ip, ipRange);
			} else {
				rs = ip.equals(ipRange);
			}
			
			if(rs) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * 把IP转换成整数
	 * @param ip
	 * @return
	 */
	public static long getIp2long(String ip) {
		ip = ip.trim();
		String[] ips = ip.split("\\.");
		long ip2long = 0L;
		for (int i = 0; i < 4; ++i) {
			ip2long = ip2long << 8 | Integer.parseInt(ips[i]);
		}
		return ip2long;
	}

	/**
	 * 把IP转换成整数
	 * @param ip
	 * @return
	 */
//	private static long getIp2long2(String ip) {
//		ip = ip.trim();
//		String[] ips = ip.split("\\.");
//		long ip1 = Integer.parseInt(ips[0]);
//		long ip2 = Integer.parseInt(ips[1]);
//		long ip3 = Integer.parseInt(ips[2]);
//		long ip4 = Integer.parseInt(ips[3]);
//		long ip2long = 1L * ip1 * 256 * 256 * 256 + ip2 * 256 * 256 + ip3 * 256 + ip4;
//		return ip2long;
//	}
	
	public static void main(String[] args) {
		// 10.10.10.116 是否属于固定格式的IP段10.10.1.00-10.10.255.255
		String ip = "192.168.1.100";
		String ipSection = "192.168.1.20-192.168.10.100";
		boolean exists = isExists(ip, ipSection);
		System.out.println(exists);
		System.out.println(getIp2long(ip));
		System.out.println(isExists(ip, ip));
		System.out.println(isExists(ip, "192.168.1.101"));
		
		System.out.println(isExists("101.81.23.1", "101.81.23.1-101.81.23.254"));
		//119.32.0.254-119.32.254.254,101.81.23.1-101.81.23.254
	}
}
