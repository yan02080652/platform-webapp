package com.tem.filter;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.SpringContextUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.platform.api.WhiteListService;
import com.tem.platform.api.condition.WhiteListCondition;
import com.tem.platform.api.dto.WhiteListDto;
import com.tem.filter.util.IpUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * IP地址访问限制filter
 * 
 */
public class WhiteListFilter implements Filter {
	private static final Logger logger = LoggerFactory.getLogger("com.tem.whitelist");
//	private static final String CACHE_KEY = "PT_WHITE_LIST";
	private List<WhiteListDto> list = null;
	private long lastLoadTime = System.currentTimeMillis();
	
	public void doFilter(ServletRequest req, ServletResponse res,FilterChain chain) throws IOException, ServletException {
		HttpServletResponse resp = (HttpServletResponse)res;
		HttpServletRequest requ = (HttpServletRequest)req;
		
		String addr = getRemoteAddr(requ);
		
		long curTime = System.currentTimeMillis();
		if((curTime - lastLoadTime) > 1 * 60 * 1000 ||  list == null) {//1分钟从数据库里加载一次
			List<WhiteListDto> list = new ArrayList<WhiteListDto>();
			
			WhiteListCondition whiteListCondition = new WhiteListCondition();
			QueryDto<WhiteListCondition> queryDto = new QueryDto<WhiteListCondition>(1,9999);
			queryDto.setCondition(whiteListCondition);
			
			WhiteListService whiteListService = SpringContextUtils.getBean(WhiteListService.class);
			if(whiteListService != null) {
				Page<WhiteListDto> whiteListDtoList = whiteListService.queryListWithPage(queryDto);
				list = whiteListDtoList.getList();
//				if(list.size() > 0) {
//					RedisCacheUtils.put(CACHE_KEY, list);
//				}
			}
			
			this.list = list;
		}
		
		boolean valid = false;
		if(addr.split(":").length == 8){
			valid = true;
		}else{
			if(CollectionUtils.isNotEmpty(list)){
				for(WhiteListDto whiteList : list){
					String ips = whiteList.getIp();
					
					Long cur = System.currentTimeMillis();
					Long effect = whiteList.getEffectTime().getTime();
					
					if(StringUtils.isNotEmpty(ips) && cur.longValue() <= effect.longValue()){
						if(IpUtil.isExists(addr, ips)){
							valid = true;
							break;
						}
					}
					
					
				}
			}
		}
		if(!valid) {
			if(logger.isDebugEnabled()) {
				LogUtils.debug(logger,"非白名单ip试图访问系统, id:{}, ipList: {}", addr, list);
			} else {
				LogUtils.warn(logger,"非白名单ip试图访问系统, id:{}", addr);
			}
			
			resp.sendError(403);
			return;
		}
		
		chain.doFilter(req, res);
	}
	
	private String getRemoteAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Real-IP");
		if (ip != null && ip.length() > 0 && !"unknown".equalsIgnoreCase(ip)) {
			return ip;
		}
		
		ip = request.getHeader("x-forwarded-for");
		if (ip != null && ip.length() > 0 && !"unknown".equalsIgnoreCase(ip)) {
			int index = ip.indexOf(',');
			if (index != -1) {
				return ip.substring(0, index);
			} else {
				return ip;
			}
		}
		ip = request.getHeader("Proxy-Client-IP");
		if (ip != null && ip.length() > 0 && !"unknown".equalsIgnoreCase(ip)) {
			int index = ip.indexOf(',');
			if (index != -1) {
				return ip.substring(0, index);
			} else {
				return ip;
			}
		}
		
		return request.getRemoteAddr();
	}

	public void init(FilterConfig filterConfig) {
		
	}

	public void destroy() {
	}
}
