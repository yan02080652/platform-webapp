package com.tem.filter;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Config;
import com.iplatform.common.utils.AESCoder;
import com.iplatform.common.utils.DigestUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.platform.Constants;
import com.tem.platform.util.LoginRedisManger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author chenjieming
 * @date 2017/9/12
 */
public class LoginFilter implements Filter {

    private final static Logger logger = LoggerFactory.getLogger(LoginFilter.class);

    private LoginRedisManger loginRedisManger;

    public final static String LOGIN_VERIFY_KEY = "#@#$!$!@$!@$!";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;

        String uri=req.getRequestURI();
        
        if("/security_login".equals(uri)){

        	boolean isNeedVerify = isNeedVerify(req);

        	if(isNeedVerify){

                Map<String,Object> map = new HashMap<String,Object>();
                map.put("message","需要校验手机号码或者邮箱");
                map.put("count",-100);
                writeToClient(map,res);
        	    return;
            }

            String code = req.getParameter("code");
        	if(code != null){
        		LogUtils.debug(logger, "后台登录校验验证码，缺少cookie，校验验证码。");
        		
	            String username = req.getParameter("username");
	
	            boolean flag = loginRedisManger.isValidateCode(username, code);
	
	            if(!flag){

	            	LogUtils.debug(logger, "后台登录校验验证码，验证码校验不成功。");

                    Map<String,Object> map = new HashMap<String,Object>();
                    map.put("message","验证码错误");
                    map.put("count",0);
                    writeToClient(map,res);
                    return;
	            }
        	}
        	
        	chain.doFilter(req, res);
        }else {
            chain.doFilter(req, res);
        }
    }

    /**
     * 存在登陆密码的cookies,且合法
     * @param req
     * @return
     */
    public static boolean isNeedVerify(HttpServletRequest req){

        String flag = Config.getString("identify.code.flag");

        if("false".equals(flag)){
            return false;
        }


        String code = req.getParameter("code");
        if(code != null){
            return false;
        }

        String cookiesStr = null;

        String username = req.getParameter("username");

        String cookieKey = Constants.LOGIN_CODE_CHECKED_COOKIES + "_" + DigestUtils.md5(username);

        if(req != null){
            Cookie[] cookies = req.getCookies();
            if(cookies!=null){
                for(Cookie cookie : cookies){
                    if(cookie != null && cookie.getName().equalsIgnoreCase(cookieKey)){
                        cookiesStr = cookie.getValue();
                    }
                }
            }
        }

        try {
            String string = AESCoder.decryptHexString2String(cookiesStr, LOGIN_VERIFY_KEY);

            if(username.equals(string)){
                return false;
            }

        }catch (Exception e){
            LogUtils.debug(logger, "用户是否需要登陆，解密错误:{}",e);
        }

        return true;
    }

    @Override
    public void destroy() {

    }

    public LoginRedisManger getLoginRedisManger() {
        return loginRedisManger;
    }

    public void setLoginRedisManger(LoginRedisManger loginRedisManger) {
        this.loginRedisManger = loginRedisManger;
    }

    public void writeToClient(Object info , HttpServletResponse response) {
        try {
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/json;charset=utf-8");
            String jsonInfo = JSONObject.toJSONString(info);
            PrintWriter writer = response.getWriter();
            writer.print(jsonInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
