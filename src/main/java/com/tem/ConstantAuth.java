package com.tem;

/**
 * 作业授权变量存储
 * Created by pyy on 2017/9/19.
 */
public class ConstantAuth {

    /**
     *  企业账户设置
     *  M:管理;L:清算;A:管理所有
     */
    public static final String IPE_COMP_ACCOUNT_MAG = "IPE_COMP_ACCOUNT_MAG";


}
