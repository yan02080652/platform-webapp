package com.tem.flight;

import org.apache.commons.lang.StringUtils;

import java.util.Date;

public class  Constants {
	/**
	 * 舱位类型
	 */
	public static final String OTAF_CABIN_TYPE = "OTAF_CABIN_TYPE";
	/**
	 * 舱位等级
	 */
	public static final String OTAF_CABIN_GRADE = "OTAF_CABIN_GRADE";
	/**
	 * 路由规则级别
	 */
	public static final String OTA_ROUTE_RULE_GRADE = "OTA_ROUTE_RULE_GRADE";
	/**
	 * 占座策略
	 */
	public static final String OTA_HOLD_SEAT_WAY = "OTA_HOLD_SEAT_WAY";
	/**
	 * 出票策略
	 */
	public static final String OTA_ISSUE_WAY = "OTA_ISSUE_WAY";
	/**
	 * 路由规则场景
	 */
	public static final String OTA_RULE_SCENE = "OTA_RULE_SCENE";
	/**
	 * 相同航班/舱位信息筛选方式
	 */
	public static final String OTA_FILTER_METHOD = "OTA_FILTER_METHOD";
	/**
	 * 验价策略
	 */
	public static final String OTA_CHECK_PRICE_WAY = "OTA_CHECK_PRICE_WAY";
	
	/**
	 * 出票类型：线下出票
	 */
	public static final String TICKET_LINE = "1";
	/**
	 * 出票类型：回填票号
	 */
	public static final String BACKFILL_TICKET = "2";
	/**
	 * 出票类型：改签
	 */
	public static final String CHANGE_SUCCESS = "3";
	
	/**
	 * 国内机票保险常量标识
	 */
	public static final String FLIHGT_DOMESTIC = "AIRT_D";
	/**
	 * 国际机票保险常量标识
	 */
	public static final String FLIGHT_INTERNATIONAL = "AIRT_I";

	public static final String SUCCESS = "success";
	public static final String FAILED = "failed";
	public static final String EXIST = "exist";

	public static final String TM_TRAVEL_TYPE = "TM_TRAVEL_TYPE";//差旅类型字典

}
