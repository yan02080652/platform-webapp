package com.tem.flight.action;

import com.iplatform.common.web.Message;
import com.tem.flight.api.HubsConnectService;
import com.tem.flight.api.HubsService;
import com.tem.flight.dto.enums.ProductTypeEnum;
import com.tem.flight.dto.hubs.HubsConnectDto;
import com.tem.flight.dto.hubs.HubsDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("flight/hubs")
public class HubsController extends BaseController {

    @Autowired
    private HubsService hubsService;

    @Autowired
    private HubsConnectService hubsConnectService;

    @Autowired
    private IssueChannelService issueChannelService;
    @Autowired
    private PartnerService partnerService;

    @RequestMapping("")
    public String index(Model model, Boolean needDisabled) {
        //默认是的
        if (needDisabled == null) {
            needDisabled = true;
        }
        List<HubsDto> hubsDtos = hubsService.findAll(needDisabled);

        model.addAttribute("hubsDtos", hubsDtos);
        return "flight/hubs/hubs.base";
    }

    @RequestMapping(value = "/hubsDetail", method = RequestMethod.GET)
    public String hubsDetail(Model model) {
        String code = (String) super.getRequest().getParameter("code");
        HubsDto hubsDto = null;
        if (StringUtils.isNotEmpty(code)) {
            hubsDto = hubsService.getByCode(code);
            model.addAttribute("editMode", "update");
        } else {
            hubsDto = new HubsDto();
            model.addAttribute("editMode", "add");
        }

        model.addAttribute("hubsDto", hubsDto);
        return "flight/hubs/hubsDetail.jsp";
    }

    @RequestMapping(value = "/loadRight", method = RequestMethod.GET)
    public String loadRight(Model model, @RequestParam("code") String code) {
        HubsDto hubsDto = hubsService.getByCode(code);

        model.addAttribute("hubsDto", hubsDto);

        List<HubsConnectDto> hubsConnectDtos = hubsConnectService.findAllByHubsCode(code, true);
        model.addAttribute("hubsConnectDtos", hubsConnectDtos);
        return "flight/hubs/hubsRight.jsp";
    }

    @ResponseBody
    @RequestMapping(value = "/saveOrupdateHubs", method = RequestMethod.POST)
    public Message saveOrupdateHubs(@ModelAttribute("hubsDto") @Validated HubsDto hubsDto,
                                    Model model, BindingResult br) {
        if (hubsDto.getIsCanQuery() != null && !hubsDto.getIsCanQuery()) {
            //不支持查询时 showMethod 置为0
            hubsDto.setShowMethod(0);
        }
        if (br.hasErrors()) {
            return Message.error("数据校验不通过。");
        }
        String editMode = (String) super.getRequest().getParameter("editMode");
        if ("add".equals(editMode)) {
            if (hubsService.save(hubsDto)) {
                return Message.success("新增成功。");
            } else {
                return Message.error("新增失败。");
            }
        } else {
            //如果改为禁用则连接均为禁用
            if (hubsDto.getIsDisable()) {
                hubsConnectService.setDisableByHubsCode(hubsDto.getCode());
            }
            if (hubsService.save(hubsDto)) {
                return Message.success("修改成功。");
            } else {
                return Message.error("修改失败。");
            }
        }
    }

    @ResponseBody
    @RequestMapping(value = "/deleteHubs")
    public Message deleteHubs(Model model, @RequestParam("code") String code) {
        if (StringUtils.isEmpty(code)) {
            return Message.error("没有要删除的数据");
        }
        //删除判断子项
        List<HubsConnectDto> hubsConnectDtos = hubsConnectService.findAllByHubsCode(code, true);
        if (hubsConnectDtos != null && hubsConnectDtos.size() > 0) {
            hubsConnectService.deleteByHubsCode(code);
        }
        if (hubsService.deleteByCode(code)) {
            return Message.success("删除成功");
        }
        return Message.error("删除失败");
    }

    @ResponseBody
    @RequestMapping(value = "/checkHubsCode")
    public Message checkHubsCode(Model model, @RequestParam("code") String code, @RequestParam("editMode") String editMode) {
        if ("update".equals(editMode)) {//修改下 code不能修改
            return Message.success("yes");
        } else {
            HubsDto hubsDto = hubsService.getByCode(code);
            if (hubsDto == null) {
                return Message.success("yes");
            } else {
                return Message.error("no");
            }
        }
    }

    @RequestMapping(value = "/hubsConnectDetail", method = RequestMethod.GET)
    public String hubsConnectDetail(Model model) {
        //机票渠道列表

        String hubsCode = "";
        String regionType = null;
        HubsConnectDto hubsConnectDto = null;
        if (super.getRequest().getParameter("id") != null) {
            hubsConnectDto = hubsConnectService.getById(Integer.parseInt(super.getRequest().getParameter("id")));
            hubsCode = hubsConnectDto.getHubsCode();

            PartnerDto tmcPartner = partnerService.findById(hubsConnectDto.getTmcId().longValue());
            if("0".equals(String.valueOf(hubsConnectDto.getRegionType()))) {
                List<IssueChannelDto> issueChannelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(hubsConnectDto.getTmcId().longValue(), IssueChannelType.FLIGHT.getType(), LineType.ON_LINE.getType());
                model.addAttribute("issueChannelList", issueChannelList);
            } else {
                List<IssueChannelDto> interIssueChannelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(hubsConnectDto.getTmcId().longValue(), IssueChannelType.INTERNATIONAL_FLIGHT.getType(), LineType.ON_LINE.getType());
                model.addAttribute("issueChannelList", interIssueChannelList);
            }

            model.addAttribute("tmcPartner", tmcPartner);
        } else {
            hubsCode = (String) super.getRequest().getParameter("hubsCode");//新增的时候回传进来
            regionType = (String) super.getRequest().getParameter("regionType");//新增的时候回传进来

            hubsConnectDto = new HubsConnectDto();
            hubsConnectDto.setHubsCode(hubsCode);
            if(StringUtils.isNotBlank(regionType)) {
                hubsConnectDto.setRegionType(Integer.parseInt(regionType));
            }

        }
        model.addAttribute("hubsConnectDto", hubsConnectDto);
        HubsDto hubsDto = hubsService.getByCode(hubsCode); //此处的作用是告诉前台那些接口是可以选择的
        model.addAttribute("hubsDto", hubsDto);

        model.addAttribute("productTypeEnumList", ProductTypeEnum.productTypeList());
        return "flight/hubs/hubsConnectDetail.jsp";
    }

    @ResponseBody
    @RequestMapping(value = "/saveOrupdateHubsConnect", method = RequestMethod.POST)
    public Message saveOrupdateHubsConnect(@ModelAttribute("hubsConnectDto") @Validated HubsConnectDto hubsConnectDto,
                                           Model model, BindingResult br) {
        if (br.hasErrors()) {
            return Message.error("数据校验不通过。");
        }
        if (hubsConnectDto.getId() == null) {
            if (hubsConnectService.save(hubsConnectDto)) {
                return Message.success("新增成功。");
            } else {
                return Message.error("新增失败。");
            }
        } else {
            if (hubsConnectService.save(hubsConnectDto)) {
                return Message.success("修改成功。");
            } else {
                return Message.error("修改失败。");
            }
        }
    }

    @ResponseBody
    @RequestMapping(value = "/deleteHubsConnect")
    public Message deleteHubsConnect(Model model, @RequestParam("id") Integer id) {
        if (id == null) {
            return Message.error("没有要删除的数据");
        }
        if (hubsConnectService.deleteById(id)) {
            return Message.success("删除成功");
        }
        return Message.error("删除失败");
    }

    @ResponseBody
    @RequestMapping(value = "/getHubByCode")
    public HubsDto getHubByCode(String code) {
        return hubsService.getByCode(code);
    }


    @RequestMapping(value = "/getOnlineChannel")
    @ResponseBody
    public List<IssueChannelDto> getOnlineChannel(String tmcId,Integer regionType){
        if("0".equals(String.valueOf(regionType))) {
            List<IssueChannelDto> issueChannelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(Long.parseLong(tmcId), IssueChannelType.FLIGHT.getType(), LineType.ON_LINE.getType());
            return issueChannelList;
        } else {
            List<IssueChannelDto> issueChannelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(Long.parseLong(tmcId), IssueChannelType.INTERNATIONAL_FLIGHT.getType(), LineType.ON_LINE.getType());
            return issueChannelList;
        }

    }
}
