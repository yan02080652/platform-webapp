package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.tem.flight.Constants;
import com.tem.flight.api.CabinService;
import com.tem.flight.api.RefundChangeRuleService;
import com.tem.flight.dto.cabin.CabinDto;
import com.tem.flight.dto.freight.FreightCondition;
import com.tem.flight.dto.freight.RefundChangeRuleDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.dto.DictCodeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author yuanjianxin
 * @date 2017/8/7 9:34
 */
@Controller
@RequestMapping("/flight/refundChangeRule")
public class RefundChangeRuleController extends BaseController {

    @Autowired
    private RefundChangeRuleService refundChangeRuleService;
    @Autowired
    private DictService dictService;
    @Autowired
    private CabinService cabinService;

    @RequestMapping(value = "")
    public String index(Model model) {
        return "flight/refundChangeRule/index.base";
    }

    // 分页查询
    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex, Integer pageSize, FreightCondition condition) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        //查询对象
        QueryDto<FreightCondition> queryDto = new QueryDto<FreightCondition>(pageIndex, pageSize);
        queryDto.setCondition(condition);
        Page<RefundChangeRuleDto> pageList = refundChangeRuleService.queryListWithPage(queryDto);

        model.addAttribute("pageList", pageList);
        return "flight/refundChangeRule/list.jsp";
    }

    @RequestMapping("/detail")
    public String detail(Model model, Integer id) {
        List<DictCodeDto> cabinGrade = dictService.getCodes(Constants.OTAF_CABIN_GRADE);

        RefundChangeRuleDto refundChangeRuleDto = null;
        if (id != null) {
            refundChangeRuleDto = refundChangeRuleService.getById(id);
        }
        model.addAttribute("refundChangeRuleDto", refundChangeRuleDto);
        model.addAttribute("cabinGrade", cabinGrade);
        return "flight/refundChangeRule/detail.base";
    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public String delete(Model model, @PathVariable("id") Integer id) {
        boolean rs = refundChangeRuleService.deleteById(id);
        return rs ? Constants.SUCCESS : Constants.FAILED;
    }

    @RequestMapping("/save")
    @ResponseBody
    public String save(Model model, RefundChangeRuleDto refundChangeRuleDto) {
        String[] cabinCodes = super.getRequest().getParameterValues("cabinCode");
        if (cabinCodes.length > 0) {
            StringBuilder st = new StringBuilder();
            for (String cabinCode : cabinCodes) {
                st.append(cabinCode).append("/");
            }
            refundChangeRuleDto.setCabinCodes(st.toString());
        }

        if (refundChangeRuleService.isExistRule(refundChangeRuleDto)) {
            return Constants.EXIST;
        }

        boolean rs = refundChangeRuleService.save(refundChangeRuleDto);
        return rs ? Constants.SUCCESS : Constants.FAILED;
    }

    @RequestMapping("/getCabinList")
    @ResponseBody
    public List<CabinDto> getCabinList(String carrierCode, String grade) {
        List<CabinDto> list = cabinService.findCabinsByCarrierCode(carrierCode);
        for (int i = 0; i < list.size(); i++) {
            if (!grade.equals(list.get(i).getGrade())) {
                list.remove(i--);
            }
        }
        return list;
    }
}
