package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.flight.ExcelUtil;
import com.tem.flight.ExportWhitelistUserExcel;
import com.tem.flight.api.agreement.AgreementPolicyService;
import com.tem.flight.api.agreement.WhitelistService;
import com.tem.flight.dto.agreement.AgreementPolicyDto;
import com.tem.flight.dto.agreement.AgreementWhitelistDto;
import com.tem.flight.dto.agreement.WhitelistUserCondition;
import com.tem.flight.dto.agreement.WhitelistUserDto;
import com.tem.flight.form.WhitelistUserForm;
import com.tem.order.util.ExportExcel;
import com.tem.order.util.ExportInfo;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.OrgService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.condition.UserCondition;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

@Controller
@RequestMapping(value = "/flight/whitelist")
public class WhitelistController extends BaseController {

    public final Logger logger = LoggerFactory.getLogger(WhitelistController.class);

    public static final String SUCCESS = "0";
    public static final String FAILED = "-1";
    public static final String CONIFRM = "1";


    @Autowired
    private WhitelistService whitelistService;

    @Autowired
    private AgreementPolicyService agreementPolicyService;

    @Autowired
    private UserService userService;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private OrgService orgService;


   @RequestMapping(value = "")
    public String index(){

        return "flight/agreement/whitelist/index.base";
    }

    @RequestMapping(value = "list")
    public String list(Model model,Integer pageIndex, Integer pageSize,String agreementPolicyCode,String carrierCode){

        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        QueryDto<Object> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setField1(agreementPolicyCode);
        queryDto.setField2(carrierCode);

        Page<AgreementWhitelistDto> pageList = whitelistService.queryPageList(queryDto);

        for (AgreementWhitelistDto whitelistDto : pageList.getList()) {
            whitelistDto.setUserCount(whitelistService.findUsersByWiltelistId(whitelistDto.getId()).size());
        }

        model.addAttribute("pageList",pageList);

        return "flight/agreement/whitelist/list.jsp";
    }

    @RequestMapping(value = "/delete")
    @ResponseBody
    public ResponseInfo delete(String id){

        AgreementWhitelistDto whitelistDto = whitelistService.findById(Integer.parseInt(id));
        List<AgreementPolicyDto> policyDtos = agreementPolicyService.findOriginPolicyByCode(whitelistDto.getCustomerAgreementCode());
        for (AgreementPolicyDto policyDto : policyDtos) {
            if(policyDto != null && policyDto.getWhiteListControl()){
                return new ResponseInfo(CONIFRM, "该白名单正在被使用中，如需删除，请先关闭关联原始协议的白名单控制!");
            }
            if(policyDto != null) {
                policyDto.setWhiteListId(null);
                agreementPolicyService.save(policyDto);
            }
        }

        boolean result = whitelistService.deleteById(id);
        if (result) {
            return new ResponseInfo(SUCCESS, "删除成功!");
        } else {
            return new ResponseInfo(FAILED, "删除失败!");
        }

    }

    @RequestMapping(value = "/detail")
    public String detail(Model model,String id){
        AgreementWhitelistDto whitelistDto = whitelistService.findById(Integer.parseInt(id));
        if(whitelistDto == null) {
            return "redirect:/flight/whitelist";
        }
        if(whitelistDto.getCreator() != null) {
            whitelistDto.setCreateName(userService.getFullName(whitelistDto.getCreator().longValue()));
        }
        if(whitelistDto.getUpdator() != null) {
            whitelistDto.setUpdateName(userService.getFullName(whitelistDto.getUpdator().longValue()));
        }
        List<Integer> list = agreementPolicyService.findPartnerIdsByAgreementCode(whitelistDto.getCustomerAgreementCode());
        model.addAttribute("parentIds",StringUtils.join(list,","));
        model.addAttribute("whitelistDto",whitelistDto);
        return "flight/agreement/whitelist/detail.base";
    }

    @RequestMapping(value = "/userList")
    public String userList(Model model,Integer pageIndex, Integer pageSize,WhitelistUserCondition con){

        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = 100;
        }
        if(con.getStatus() != null && con.getStatus() == 1) {
            con.setEffective("");
        }

        QueryDto<WhitelistUserCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(con);
        Page<WhitelistUserDto> pageList = whitelistService.queryUserPageList(queryDto);
        for (WhitelistUserDto userDto : pageList.getList()) {
            if(userDto.getPartnerId() != null) {
                PartnerDto partnerDto = partnerService.findById(userDto.getPartnerId().longValue());
                if(partnerDto != null) {
                    userDto.setPartnerName(partnerDto.getName());
                }
            }
        }


        model.addAttribute("pageList",pageList);
        if("1".equals(con.getStatus()+"")){
            return "flight/agreement/whitelist/userList2.jsp";
        } else {
            return "flight/agreement/whitelist/userList1.jsp";
        }

    }

    @RequestMapping(value = "/deleteUsers")
    @ResponseBody
    public ResponseInfo batchDelete(String ids){
        String[] split = ids.split(",");
        boolean boo = whitelistService.batchDeleteUser(Arrays.asList(split));
        if (boo) {
            return new ResponseInfo(SUCCESS, "删除成功!");
        } else {
            return new ResponseInfo(FAILED, "删除失败!");
        }
    }

    @RequestMapping(value = "/batchUpdateUsers")
    @ResponseBody
    public ResponseInfo batchUpdateUsers(String whitelistId,String date,String type){

        boolean boo = whitelistService.batchUpdateUserDate(whitelistId,type,date);
        if (boo) {
            return new ResponseInfo(SUCCESS, "修改成功!");
        } else {
            return new ResponseInfo(FAILED, "修改失败!");
        }
    }





    @RequestMapping(value = "/addChooseUser")
    @ResponseBody
    public ResponseInfo addChooseUser(String partnerIds,String orgIds,String userIds,String whitelistId){

        List<String> pids = new ArrayList<>();
        List<String> oids = new ArrayList<>();
        List<String> uids = new ArrayList<>();
        if(StringUtils.isNotBlank(partnerIds)) {
            pids = Arrays.asList(partnerIds.split(","));
        }
        if(StringUtils.isNotBlank(orgIds)) {
            oids = Arrays.asList(orgIds.split(","));
        }
        if(StringUtils.isNotBlank(userIds)) {
            uids = Arrays.asList(userIds.split(","));
        }
        Set<Long> allUserIds = new HashSet<>();
        for (String pid : pids) {
            List<UserDto> allUsers = userService.findUserListWithOrg(Long.parseLong(pid), "");
            for (UserDto allUser : allUsers) {
                allUserIds.add(allUser.getId());
            }

        }
        for (String oid : oids) {
            QueryDto<UserCondition> queryDto = new QueryDto<>();
            UserCondition condition = new UserCondition();
            condition.setOrgId(Long.parseLong(oid));
            queryDto.setCondition(condition);
            List<UserDto> orgUsers = userService.queryListWithPage(queryDto).getList();
            for (UserDto orgUser : orgUsers) {
                allUserIds.add(orgUser.getId());
            }
        }
        for (String uid : uids) {
            allUserIds.add(Long.parseLong(uid));
        }
        if(CollectionUtils.isEmpty(allUserIds)) {
            return new ResponseInfo(FAILED, "选择的部门无员工!");
        }
        boolean boo = whitelistService.addChooseUser(allUserIds,whitelistId);
        if(boo) {
            return new ResponseInfo(SUCCESS, "添加成功!");
        }else {
            return new ResponseInfo(FAILED, "添加失败!");
        }
    }



















    @RequestMapping(value = "/importUser")
    @ResponseBody
    public ResponseInfo importUser(Model model,@RequestParam("file") MultipartFile file,String whitelistId){
        try {
            List<WhitelistUserDto> users = new ArrayList<WhitelistUserDto>();
            users = getUsersFromExcel(file.getInputStream(), users,whitelistId);
            boolean result = whitelistService.batchInsertUsers(users);
            if (result) {
                return new ResponseInfo(SUCCESS, "导入成功!");
            } else {
                return new ResponseInfo(FAILED, "导入失败!");
            }
        }catch (Exception e) {
            return new ResponseInfo(FAILED, "导入失败!");
        }

    }


    private List<WhitelistUserDto> getUsersFromExcel(InputStream fis, List<WhitelistUserDto> users,String whitelistId) throws InvalidFormatException, IOException {
        if (users == null) {
            users = new ArrayList<WhitelistUserDto>();
        }

        Workbook wb = WorkbookFactory.create(fis);

        int sheetNumber = wb.getNumberOfSheets();
        //只读取第一个表
        sheetNumber = 1;

        for (int numSheet = 0; numSheet < sheetNumber; numSheet++) {
            Sheet sheet = wb.getSheetAt(numSheet);
            if (sheet == null) {
                continue;
            }
            // 循环行Row, 从第四行开始
            for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
                Row row = sheet.getRow(rowNum);
                if (row == null) {
                    continue;
                }

                String fullname = ExcelUtil.getStringValueFromCell(row.getCell(0));
                String nameEn = ExcelUtil.getStringValueFromCell(row.getCell(1));
                String idNo = ExcelUtil.getStringValueFromCell(row.getCell(2));
                String certificateno1 = ExcelUtil.getStringValueFromCell(row.getCell(3));
                String certificateno2 = ExcelUtil.getStringValueFromCell(row.getCell(4));

                if(StringUtils.isBlank(fullname) && StringUtils.isBlank(nameEn)) {
                    continue;
                }
                //新建一个user对象
                WhitelistUserDto userDto = new WhitelistUserDto();
                userDto.setFullName(fullname);
                userDto.setNameEn(nameEn);
                userDto.setIdNo(ExcelUtil.parseString(idNo));
                userDto.setCertificateno1(ExcelUtil.parseString(certificateno1));
                userDto.setCertificateno2(ExcelUtil.parseString(certificateno2));
                userDto.setUserType(2);//导入的非员工
                userDto.setStatus(1);
                userDto.setWhitelistId(Integer.parseInt(whitelistId));
                users.add(userDto);

            }
        }

        return users;
    }



    @RequestMapping("/downLoadTemplate")
    public void excelTemplateExport(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fileName = "非员工导入模板.xlsx";
            String path = request.getSession().getServletContext().getRealPath("") + "/resource/excel/" + fileName;
            File file = new File(path);
            // 取得文件名
            String filename = file.getName();
            // 以流的形式下载文件
            InputStream fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    @RequestMapping(value="exportUser")
    public void exportUser(WhitelistUserCondition condition){

        List<ExportInfo> infoList = new ArrayList<>();


        QueryDto<WhitelistUserCondition> queryDto = new QueryDto<>(1, Integer.MAX_VALUE);
        queryDto.setCondition(condition);
        Page<WhitelistUserDto> pageList = whitelistService.queryUserPageList(queryDto);

        List<WhitelistUserForm> list = transOrdersFormList(pageList.getList());

        String[] header = {"序号", "类型","中文名","英文姓/英文名","身份证","其他证件1","其他证件2","生效日期","失效日期"};

        ExportInfo<WhitelistUserForm> userExport = new ExportInfo<>();
        userExport.setDataset(list);
        userExport.setHeaders(header);
        userExport.setTitle("白名单用户");
        infoList.add(userExport);

        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String fileName = "白名单用户" + uuid + ".xlsx";

        HttpServletRequest request = getRequest();
        HttpServletResponse response = getResponse();

        try
        {
            long begin = System.currentTimeMillis();

            String ctxPath = request.getSession().getServletContext().getRealPath("");
            String downLoadPath = ctxPath+"/resource/excel/"+ fileName;

            ExportWhitelistUserExcel ex = new ExportWhitelistUserExcel();
            OutputStream out = new FileOutputStream(downLoadPath);

            long begin1 = System.currentTimeMillis();

            ex.exportExcel(infoList, out, "yyyy-MM-dd");

            long end1 = System.currentTimeMillis();
            LogUtils.debug(logger,"生成excel,耗时：{}",end1 - begin1);

            out.close();

            //获得请求文件名
            request.setCharacterEncoding("UTF-8");
            BufferedInputStream bis = null;
            BufferedOutputStream bos = null;

            //获取文件的长度
            long fileLength = new File(downLoadPath).length();

            //设置文件输出类型
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename="
                    + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
            //设置输出长度
            response.setHeader("Content-Length", String.valueOf(fileLength));
            //获取输入流
            bis = new BufferedInputStream(new FileInputStream(downLoadPath));
            //输出流
            bos = new BufferedOutputStream(response.getOutputStream());
            byte[] buff = new byte[2048];
            int bytesRead;
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
            //关闭流
            bis.close();
            bos.close();

            long end = System.currentTimeMillis();
            LogUtils.debug(logger,"文件的操作,耗时：{}",end - begin);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private List<WhitelistUserForm> transOrdersFormList(List<WhitelistUserDto> list) {

        List<WhitelistUserForm> userlist = new ArrayList<>();
        int num = 0;
        for (WhitelistUserDto whitelistUserDto : list) {
            num++;
            WhitelistUserForm form = new WhitelistUserForm();
            form.setId(num);
            form.setUserType(whitelistUserDto.getUserType() == 1?"员工":"非员工");
            form.setFullname(whitelistUserDto.getFullName());
            form.setNameEn(whitelistUserDto.getNameEn());
            form.setIdNo(whitelistUserDto.getIdNo());
            form.setCertificateno1(whitelistUserDto.getCertificateno1());
            form.setCertificateno2(whitelistUserDto.getCertificateno2());
            form.setEffectiveDate(whitelistUserDto.getEffectiveDate() == null ?"": DateUtils.format(whitelistUserDto.getEffectiveDate(),DateUtils.YYYY_MM_DD));
            form.setInvalidDate(whitelistUserDto.getInvalidDate() == null ?"": DateUtils.format(whitelistUserDto.getInvalidDate(),DateUtils.YYYY_MM_DD));
            userlist.add(form);
        }
        return  userlist;
    }


}
