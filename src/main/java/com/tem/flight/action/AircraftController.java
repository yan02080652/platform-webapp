package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.web.Message;
import com.tem.flight.api.AircraftService;
import com.tem.flight.dto.aircraft.AircraftDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.dto.DictCodeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("flight/aircraft")
public class AircraftController extends BaseController {
	@Autowired
	private AircraftService aircraftService;
	
	@Autowired
	private DictService dictService;
	
	@RequestMapping(value = "")
	public String index(Model model) {
		model.addAttribute("typeList",getAircraftType());
		return "flight/aircraft/index.base";
	}
	//分页查询
	@RequestMapping(value = "/list")
	public String list(Model model,Integer pageIndex, @RequestParam(value="pageSize",required=false)Integer pageSize,Integer type ,String name){
		if(pageIndex==null){
			   pageIndex = 1;
		   }
		   if(pageSize==null){
			   pageSize = Page.DEFAULT_PAGE_SIZE; 
		   }

		   
		QueryDto<Object> queryDto=new QueryDto<Object>(pageIndex,pageSize);
		
		queryDto.setField1(name);
		queryDto.setField4(type);
		
		Page<AircraftDto> list = aircraftService.queryListWithPage(queryDto);
		model.addAttribute("pageList", list);
	
		return "flight/aircraft/aircraftList.jsp";
	}
	
	//批量删除
	@ResponseBody
	@RequestMapping("/delete")
	public Message delete(String id) {
		String[] split = id.split(",");
		Integer[] ids= new Integer[split.length];
		for(int i=0;i<split.length;i++){
			ids[i] = Integer.parseInt(split[i]);
		}
		boolean rs = aircraftService.batchDelete(ids);
		if (rs) {
			return Message.success("删除成功");
		} else {
			return Message.error("删除失败，请刷新重试");
		}
	}
	
	@ResponseBody
	@RequestMapping("/getAircraftType")
	public List<DictCodeDto> getAircraftType(){
			List<DictCodeDto> list = dictService.getCodes("OTAF_AIRCRAFT");
			return list;
	}

	//用于新增和修改对象
	@RequestMapping(value = "/getAircraft")
	public String getAircraft(Model model, Integer id) {
		AircraftDto aircraftDto = null;
		if (id != null && !"".equals(id)) {
			aircraftDto = aircraftService.getById(id);
		}
		model.addAttribute("aircraftDto", aircraftDto);
		model.addAttribute("typeList",getAircraftType());
		return "flight/aircraft/aircraftModel.jsp";
	}
	
	// 保存对象
		@ResponseBody
		@RequestMapping(value = "/save")
		public Message addOrUpdate(Model model, AircraftDto aircraftDto) {
			if(aircraftDto.getId() == null){// 新增
				boolean rs = aircraftService.save(aircraftDto);
				if (rs) {
					return Message.success("添加成功");
				}
				return Message.error("添加失败,请刷新重试");
			} else {// 修改
				boolean rs = aircraftService.save(aircraftDto);
				if (rs) {
					return Message.success("修改成功");
				}
				return Message.error("修改失败,请刷新重试");
			}
			
			
		}
}