package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.tem.flight.api.CarrierService;
import com.tem.flight.api.SegmentService;
import com.tem.flight.dto.carrier.CarrierDto;
import com.tem.flight.dto.segment.SegmentCondition;
import com.tem.flight.dto.segment.SegmentDto;
import com.tem.platform.action.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yuanjianxin
 * @date 2017/7/13 16:07
 */
@Controller
@RequestMapping("/flight/segment")
public class SegmentController extends BaseController {

    @Autowired
    private SegmentService segmentService;

    @Autowired
    private CarrierService carrierService;

    @RequestMapping(value = "")
    public String index(Model model,String carrierCode) {
        model.addAttribute("carrierCode", carrierCode);
        return "flight/segment/index.base";
    }

    // 分页查询
    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex,String carrierCode,String fromKeyword,String toKeyword) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        //查询对象
        QueryDto<SegmentCondition> queryDto = new QueryDto<SegmentCondition>(pageIndex, Page.DEFAULT_PAGE_SIZE);
        SegmentCondition condition = new SegmentCondition(carrierCode,fromKeyword,toKeyword);
        queryDto.setCondition(condition);
        Page<SegmentDto> pageList = segmentService.queryListWithPage(queryDto);
        model.addAttribute("pageList", pageList);
        model.addAttribute("carrierMap", getCarrierMap());
        return "flight/segment/list.jsp";
    }

    public Map<String,String> getCarrierMap(){
        Map<String, String> map = new HashMap<>();
        List<CarrierDto> list = carrierService.findAll();
        for (CarrierDto carrierDto : list) {
            map.put(carrierDto.getCode(), carrierDto.getNameShort());
        }

        return map;
    }

}
