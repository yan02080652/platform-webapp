package com.tem.flight.action;

import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.ExcelUtil;
import com.iplatform.common.utils.LogUtils;
import com.iplatform.common.utils.ResourceUtils;
import com.tem.flight.api.AirportService;
import com.tem.flight.api.CabinService;
import com.tem.flight.api.CarrierService;
import com.tem.flight.dto.enums.CityAirportType;
import com.tem.flight.dto.enums.RegionType;
import com.tem.pss.api.IssueChannelService;
import com.tem.flight.api.order.FlightOrderImportService;
import com.tem.flight.dto.airport.AirportDto;
import com.tem.flight.dto.cabin.CabinCondition;
import com.tem.flight.dto.cabin.CabinDto;
import com.tem.flight.dto.carrier.CarrierDto;
import com.tem.flight.dto.enums.OrderTypeEnum;
import com.tem.flight.dto.flight.AirRangeType;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.flight.dto.order.ContactsDto;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.flight.dto.order.FlightOrderPriceDto;
import com.tem.flight.dto.order.OrderEndorseRefundRuleDto;
import com.tem.flight.dto.order.OrderFlightDetailDto;
import com.tem.flight.dto.order.PassengerDto;
import com.tem.flight.dto.order.enums.CredentialType;
import com.tem.flight.dto.order.enums.GenderType;
import com.tem.flight.dto.order.enums.OrderOriginType;
import com.tem.flight.dto.order.enums.OrderShowStatus;
import com.tem.flight.dto.order.enums.OrderStatus;
import com.tem.flight.dto.order.enums.PassengerStatus;
import com.tem.flight.dto.order.enums.PassengerType;
import com.tem.flight.dto.order.enums.PaymentStatus;
import com.tem.flight.form.FlightBaseDataForm;
import com.tem.payment.enums.PayChannel;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.DistrictService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserContactService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.DistrictDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserContactDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.platform.api.dto.UserPayInfo;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author yuanjianxin
 * @date 2017/6/29 10:49
 */
@Controller
@RequestMapping("/flight/data")
public class FlightDataImportController extends BaseController {

    private static final Logger log = LoggerFactory.getLogger(FlightDataImportController.class);

    //成功
    public static final String SUCCESS = "0";
    //失败
    public static final String ERROR = "1";
    //部分成功
    public static final String PARTIAL_SUCCESS = "2";
    //差旅类型字典
    public static final String TM_TRAVEL_TYPE = "TM_TRAVEL_TYPE";

    //默认预订人
    private UserDto defaultUser;
    //企业
    private PartnerDto partner;
    //出行类别列表
    private List<DictCodeDto> codes;

    Map<String, CarrierDto> carrierDtoMap = new HashMap<>();
    Map<String, AirportDto> airportDtoMap = new HashMap<>();
    Map<String, CabinDto> cabinDtoMap = new HashMap<>();
    Map<String, DistrictDto> districtDtoMap = new HashMap<>();

    @Autowired
    private UserContactService userContactService;
    @Autowired
    private UserService userService;
    @Autowired
    private PartnerService partnerService;
    @Autowired
    private CarrierService carrierService;
    @Autowired
    private CabinService cabinService;
    @Autowired
    private DistrictService districtService;
    @Autowired
    private AirportService airportService;
    @Autowired
    private FlightOrderImportService flightOrderImportService;
    @Autowired
    private DictService dictService;
    @Autowired
    private IssueChannelService issueChannelService;


    @RequestMapping("/index")
    public String index(Model model) {
        //出票渠道
        List<IssueChannelDto> list = issueChannelService.findList();
        model.addAttribute("issueChannelList",list);
        return "/flight/import/index.base";
    }

    @RequestMapping("/excelTemplateExport")
    public void excelTemplateExport(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fileName = "机票导入模板.xlsx";
            String path = request.getSession().getServletContext().getRealPath("") + "/resource/excel/" + fileName;
            File file = new File(path);
            // 取得文件名
            String filename = file.getName();
            // 以流的形式下载文件
            InputStream fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            // 设置response的Header
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=" + new String(fileName.getBytes("utf-8"), "ISO8859-1"));
            response.addHeader("Content-Length", "" + file.length());
            OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
            toClient.write(buffer);
            toClient.flush();
            toClient.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @ResponseBody
    @RequestMapping("/import")
    public ResponseInfo importData(HttpServletRequest request, Long pid, Long userId, MultipartFile excelFile,String issueChannel) {

        long start = System.currentTimeMillis();
        if (excelFile == null) {
            return new ResponseInfo(ERROR, "文件不能为空！", null);
        }

        //默认预订人
        defaultUser = userService.getUser(userId);
        if (defaultUser == null) {
            LogUtils.error(log, "员工查询异常，id=" + userId + ",返回结果=" + defaultUser + "");
            return new ResponseInfo(ERROR, "默认预订人不存在！", null);
        }
        //企业
        partner = partnerService.findById(pid);

        codes = dictService.getPartnerCodes(pid, TM_TRAVEL_TYPE);

        List<FlightBaseDataForm> beans = null;
        List<FlightBaseDataForm> failedList = new ArrayList<>();
        try {
            InputStream is = excelFile.getInputStream();
            Map<String, String> keyValue = ResourceUtils.getResource("excelMapping").getMap();
            beans = ExcelUtil.readExcel(is, keyValue, FlightBaseDataForm.class);
        } catch (Exception e) {
            LogUtils.error(log, "excel模板转换异常，异常信息={}", e.getMessage());
            return new ResponseInfo(ERROR, e.getMessage(), null);
        }
        //航司编码集合
        List<String> carrierCodeList = new ArrayList<String>();
        //机场编码集合
        List<String> airportCodeList = new ArrayList<String>();
        //舱位-航司编码集合
        List<CabinCondition> cabinCodeList = new ArrayList<CabinCondition>();

        for (FlightBaseDataForm bean : beans) {
            if (!carrierCodeList.contains(bean.getCarrierCode())) {
                carrierCodeList.add(bean.getCarrierCode());
            }
            if (!airportCodeList.contains(bean.getFromAirportCode())) {
                airportCodeList.add(bean.getFromAirportCode());
            }
            if (!airportCodeList.contains(bean.getToAirportCode())) {
                airportCodeList.add(bean.getToAirportCode());
            }
            CabinCondition cabinCondition = new CabinCondition(bean.getCabinCode(), bean.getCarrierCode());
            if (!cabinCodeList.contains(cabinCondition)) {
                cabinCodeList.add(cabinCondition);
            }

            UserPayInfo userPayInfo = userService.getUserPayInfo(pid, bean.getUserName());
            if (userPayInfo != null) {
                bean.setUserId(userPayInfo.getId());
                bean.setUserType("员工");
                bean.setEmail(userPayInfo.getEmail());
                bean.setMobile(userPayInfo.getMobile());

            } else {
                //非员工使用默认预订人的成本归属等信息
                userPayInfo = userService.getUserPayInfo(pid, defaultUser.getFullname());
                bean.setUserType("非员工");
            }

            if (userPayInfo == null) {
                return new ResponseInfo(ERROR, "默认预订人基本信息查询失败！", null);
            }

            if (userPayInfo.getOrgId() == null || userPayInfo.getCorpId() == null || userPayInfo.getCostCenterId() == null) {
                return new ResponseInfo(ERROR, userPayInfo.getFullname() + "的 费用归属 | 费用归属法人 | 组织 有空值，请检查！", null);
            }

            bean.setOrgId(userPayInfo.getOrgId().toString());
            bean.setCorporationId(userPayInfo.getCorpId().toString());
            bean.setCostUnitCode(userPayInfo.getCostCenterId().toString());
            bean.setEmpLevelCode(userPayInfo.getRank());
            bean.setIssueChannel(issueChannel);
        }

        if (!CollectionUtils.isEmpty(airportCodeList)) {
            districtDtoMap = districtService.findMapByCodes(airportCodeList);
        }
        if (!CollectionUtils.isEmpty(carrierCodeList)) {
            carrierDtoMap = carrierService.findMapByCodes(carrierCodeList);
        }
        if (!CollectionUtils.isEmpty(airportCodeList)) {
            airportDtoMap = airportService.findMapByCodes(airportCodeList, CityAirportType.AIRPORT.getType(), RegionType.DOMESTIC.getType());
        }
        if (!CollectionUtils.isEmpty(cabinCodeList)) {
            cabinDtoMap = cabinService.findMapByCabinConditions(cabinCodeList);
        }

        Map<String, List<FlightBaseDataForm>> dataMap = dataGrouping(beans);

        for (Map.Entry<String, List<FlightBaseDataForm>> entry : dataMap.entrySet()) {
            try {
                FlightOrderDto flightOrderDto = getFlightOrderDto(entry.getValue());
                flightOrderImportService.importOrder(flightOrderDto);
            } catch (Exception e) {
                LogUtils.error(log, "订单信息插入异常，异常信息={}", e.getMessage());
                failedList.addAll(entry.getValue());
            }
        }

        long end = System.currentTimeMillis();
        LogUtils.debug(log, "总耗时={}", (end - start));

        if (CollectionUtils.isEmpty(failedList)) {
            return new ResponseInfo(SUCCESS, "数据导入成功！", null);
        } else {
            return new ResponseInfo(PARTIAL_SUCCESS, "数据部分导入成功，请检查失败部分数据合法性和完整性后再次导入！", failedList);
        }
    }

    /**
     * 根据分组后的数据构建订单对象
     *
     * @param dataMap
     * @return
     */
    private List<FlightOrderDto> getOrderList(Map<String, List<FlightBaseDataForm>> dataMap) {
        List<FlightOrderDto> list = new ArrayList<>();

        for (Map.Entry<String, List<FlightBaseDataForm>> entry : dataMap.entrySet()) {
            FlightOrderDto orderDto = new FlightOrderDto();

            list.add(orderDto);
        }

        return list;
    }

    private FlightOrderDto getFlightOrderDto(List<FlightBaseDataForm> list) throws Exception {
        FlightOrderDto flightOrderDto = new FlightOrderDto();
        FlightBaseDataForm dataForm = list.get(0);
        UserDto user = null;
        for (FlightBaseDataForm form : list) {
            if ("员工".equals(form.getUserType())) {
                user = userService.getUser(form.getUserId());
                if (user == null) {
                    LogUtils.error(log, "员工查询异常，id=" + form.getUserId() + ",返回结果=" + user + "");
                    throw new RuntimeException("默认预订人不存在!");
                }
                break;
            }
        }
        //当全是非员工时取默认预订人
        if (user == null) {
            user = defaultUser;
        }

        // 机票订单航程信息列表
        List<OrderFlightDetailDto> flightDetailDtos = new ArrayList<>();
        OrderFlightDetailDto orderFlightDetailDto = getOrderFlightDetailDto(dataForm);
        flightDetailDtos.add(orderFlightDetailDto);
        flightOrderDto.setOrderFlights(flightDetailDtos);

        List<PassengerDto> passengerDtoList = new ArrayList<>();
        for (FlightBaseDataForm form : list) {
            PassengerDto dto = getPassengerDto(form);
            passengerDtoList.add(dto);
        }

        //乘机人列表
        flightOrderDto.setPassengers(passengerDtoList);
        //联系人信息
        ContactsDto contactorDto = new ContactsDto();
        contactorDto.setName(user.getFullname());
        contactorDto.setEmail(user.getEmail());
        contactorDto.setMobile(user.getMobile());
        flightOrderDto.setContactor(contactorDto);

        //订单来源
        flightOrderDto.setOrderOriginType(OrderOriginType.WWW);
        //预订人id
        flightOrderDto.setUserId(user.getId().toString());
        //预订人名称
        flightOrderDto.setUserName(user.getFullname());
        //预订人手机号
        flightOrderDto.setUserMobile(user.getMobile());
        //企业id
        flightOrderDto.setcId(partner.getId().toString());
        //企业名称
        flightOrderDto.setcName(partner.getName());
        //支付方式
        flightOrderDto.setPaymentMethod(PayChannel.BP_ACCOUNT.name());
        //创建订单时间
        flightOrderDto.setCreateDate(dataForm.getCreateDate());
        //支付时间
        flightOrderDto.setPaymentDate(getPayDateOrIssueDate(flightOrderDto.getCreateDate()));
        //出票时间
        flightOrderDto.setIssueTicketTime(getPayDateOrIssueDate(flightOrderDto.getPaymentDate()));
        //最后处理时间 = 出票时间
        flightOrderDto.setLastUpdateDate(flightOrderDto.getIssueTicketTime());

        //航程类型
        flightOrderDto.setAirRangeType(AirRangeType.OW);
        //创建订单者
        flightOrderDto.setCreator(user.getId().toString());

        //支付状态
        flightOrderDto.setPaymentStatus(PaymentStatus.PAYMENT_SUCCESS);
        //订单状态
        flightOrderDto.setOrderStatus(OrderStatus.ISSUED);
        //订单显示状态
        flightOrderDto.setOrderShowStatus(OrderShowStatus.ISSUED);
        //订单类型
        flightOrderDto.setOrderType(OrderTypeEnum.ROUTINE);
        //出票渠道
        flightOrderDto.setIssueChannel(dataForm.getIssueChannel());
        //pnr
        flightOrderDto.setPnrCode(dataForm.getPnr());
        //是否需要支付审批
        flightOrderDto.setNeedAuthorize(false);
        //供应商退废票服务时间
        flightOrderDto.setRefundServiceTime("09:00-23:30");
        //机票订单费用信息对象
        FlightOrderPriceDto priceDto = new FlightOrderPriceDto();
        //销售价
        priceDto.setSalePrice(dataForm.getFacePrice());
        //结算价
        priceDto.setSettlePrice(dataForm.getFacePrice());
        // 票面价
        priceDto.setFacePrice(dataForm.getFacePrice());
        //机场建设税
        priceDto.setDepartureTax(dataForm.getDepartureTax());
        //燃油费
        priceDto.setFuelTax(dataForm.getFuelTax());

        BigDecimal unitPrice = BigDecimalUtils.add(priceDto.getSalePrice(), priceDto.getFuelTax(), priceDto.getDepartureTax());
        BigDecimal totalPrice = unitPrice.multiply(new BigDecimal(list.size()));

        //订单总价
        priceDto.setTotalOrderPrice(totalPrice);
        //结算总价
        priceDto.setTotalSettlePrice(totalPrice);
        //客户结算总价
        priceDto.setTotalSalePrice(totalPrice);
        //tr价差
        priceDto.setTrSpreads(BigDecimal.ZERO);
        //价格
        flightOrderDto.setOrderFee(priceDto);

        //退改规则
        List<OrderEndorseRefundRuleDto> endorseRefundRules = new ArrayList<OrderEndorseRefundRuleDto>();
        OrderEndorseRefundRuleDto ruleDto = new OrderEndorseRefundRuleDto();
        //乘客类型
        ruleDto.setPassengerType(PassengerType.ADU);
        //退票规则
        ruleDto.setRefundRule("航班规定离站时间4小时（含）之前收取各舱位对应票价20%退票费；航班规定离站时间4小时（不含）之后收取各舱位对应票价40%退票费");
        //改期规则
        ruleDto.setChangeRule("航班规定离站时间4小时（含）之前收取10%变更费；航班规定离站时间4小时（不含）后变更收取20%变更费");
        //签转规则
        ruleDto.setEndorseRule("不允许签转");
        //废票规则
        ruleDto.setCancelRemark("所有客票出票后不得作废。");
        endorseRefundRules.add(ruleDto);
        //机票订单退改签规则信息
        flightOrderDto.setEndorseRefundRules(endorseRefundRules);

        return flightOrderDto;
    }

    /**
     * 构建航程信息
     *
     * @param form
     * @return
     */
    private OrderFlightDetailDto getOrderFlightDetailDto(FlightBaseDataForm form) throws Exception {
        OrderFlightDetailDto detailDto = new OrderFlightDetailDto();
        detailDto.setFlightNo(form.getFlightNo());

        CarrierDto carrierDto = carrierDtoMap.get(form.getCarrierCode());
        if (carrierDto != null) {
            detailDto.setCarrierName(carrierDto.getNameShort());
        }

        detailDto.setCabinCode(form.getCabinCode());
        CabinDto cabinDto = cabinDtoMap.get(form.getCabinCode() + form.getCarrierCode());
        if (cabinDto != null) {
            detailDto.setCabinClass(cabinDto.getGrade());
        } else {
            detailDto.setCabinClass("F");
        }
        detailDto.setFromAirport(form.getFromAirportCode());
        AirportDto formAirport = airportDtoMap.get(form.getFromAirportCode());
        if (formAirport != null) {
            detailDto.setFromAirportName(formAirport.getNameCn());
        }
        detailDto.setToAirport(form.getToAirportCode());
        AirportDto toAirport = airportDtoMap.get(form.getToAirportCode());
        if (toAirport != null) {
            detailDto.setToAirportName(toAirport.getNameCn());
        }

        String duration = getDistanceTime(form.getToDate(), form.getFromDate());

        detailDto.setFromDate(form.getFromDate());
        detailDto.setToDate(form.getToDate());
        detailDto.setFlightDuration(duration);

        //出发城市
        DistrictDto fromCity = districtDtoMap.get(form.getFromAirportCode());
        //到达城市
        DistrictDto toCity = districtDtoMap.get(form.getToAirportCode());
        //如果没有，则查询机场城市关系表
        if (fromCity == null) {
            fromCity = airportService.getAirportWithinCity(form.getFromAirportCode().toUpperCase());
        }
        if (toCity == null) {
            toCity = airportService.getAirportWithinCity(form.getToAirportCode().toUpperCase());
        }

        detailDto.setFromCity(fromCity.getCode());
        detailDto.setFromCityName(fromCity.getNameCn());
        detailDto.setToCity(toCity.getCode());
        detailDto.setToCityName(toCity.getNameCn());
        detailDto.setAircraftType(!"无".equals(form.getAircraftCode()) ? form.getAircraftCode() : null);
        detailDto.setFacePrice(form.getFacePrice().intValue());
        detailDto.setDepartureTax(form.getDepartureTax().intValue());
        detailDto.setFuelTax(form.getFuelTax().intValue());

        return detailDto;
    }

    /**
     * 构建乘机人信息
     *
     * @param form
     * @return
     */
    private PassengerDto getPassengerDto(FlightBaseDataForm form) throws Exception {
        PassengerDto dto = new PassengerDto();
        dto.setUserId(form.getUserId());
        dto.setName(form.getUserName());

        dto.setPassengerType(PassengerType.ADU);
        switch (form.getCertType()) {
            case "身份证":
                dto.setCredentialType(CredentialType.IDENTITY);
                break;
            case "护照":
                dto.setCredentialType(CredentialType.PASSPORT);
                break;
            case "因公护照":
                dto.setCredentialType(CredentialType.SERVICE_PASSPORT);
                break;
            case "港澳通行证":
                dto.setCredentialType(CredentialType.GANGAO);
                break;
            case "台湾通行证":
                dto.setCredentialType(CredentialType.TAIWAN);
                break;
            case "军队证件":
                dto.setCredentialType(CredentialType.JUNDUI);
                break;
            case "海员证":
                dto.setCredentialType(CredentialType.HAIYUAN);
                break;
            case "台胞证":
                dto.setCredentialType(CredentialType.TAIBAO);
                break;
            case "回乡证":
                dto.setCredentialType(CredentialType.HUIXIANG);
                break;
        }
        dto.setCredentialNo(form.getCertNo());

        UserContactDto userContactDto = null;
        if ("员工".equals(form.getUserType())) {
            dto.setTravellerType("3");
            userContactDto = userContactService.getUserContactByType(form.getUserId(), UserContactDto.TravellerType.EMPLOYEE);
        } else {
            dto.setTravellerType("1");
            userContactDto = userContactService.getUserContactByType(form.getUserId(), UserContactDto.TravellerType.GENERATE_TRAVELLER);
        }

        if (userContactDto == null) {
            //构建一个随机性别和生日
            userContactDto = new UserContactDto();
            userContactDto.setGender(rangeInt(0, 1) + "");
            userContactDto.setBirthday(getRandomDate());
        }

        //如果证件是身份证，则计算性别和生日，其他证件从查询到的对象中取
        if (dto.getCredentialType().equals(CredentialType.IDENTITY)) {
            String sexNum = dto.getCredentialNo().substring(16, 17);
            if (Integer.valueOf(sexNum) % 2 == 0) {
                dto.setGender(GenderType.F);
            } else {
                dto.setGender(GenderType.M);
            }
            String birDate = dto.getCredentialNo().substring(6, 14);
            dto.setBirthday(DateUtils.parse(birDate, "yyyyMMdd"));
        } else {
            dto.setGender("0".equals(userContactDto.getGender()) ? GenderType.M : GenderType.F);
            dto.setBirthday(userContactDto.getBirthday());
        }

        dto.setEmail(userContactDto.getEmail());
        dto.setMobile(userContactDto.getMobile());
        dto.setTicketNo(form.getTicketNo());
        dto.setCostUnitCode(form.getCostUnitCode());
        dto.setOrgId(form.getOrgId());
        dto.setCorporationId(form.getCorporationId());
        dto.setEmpLevelCode(form.getEmpLevelCode());
        if (CollectionUtils.isNotEmpty(codes)) {
            dto.setTravelCode(codes.get(rangeInt(0, codes.size() - 1)).getItemCode());
        }
        dto.setAmount(form.getTotalPrice());
        dto.setStatus(PassengerStatus.ISSUED);
        dto.setIsPassEmpLevel(true);

        return dto;
    }

    /**
     * 根据出发日期，航班号，舱位号将数据分组，一组的数据作为一个订单
     *
     * @return
     */
    private Map<String, List<FlightBaseDataForm>> dataGrouping(List<FlightBaseDataForm> beanList) {
        Map<String, List<FlightBaseDataForm>> map = new HashMap<>();

        for (FlightBaseDataForm form : beanList) {
            if (map.containsKey(getKey(form))) {
                map.get(getKey(form)).add(form);
            } else {
                List<FlightBaseDataForm> list = new ArrayList<>();
                list.add(form);
                map.put(getKey(form), list);
            }
        }

        return map;
    }

    /**
     * 获取分组的key
     *
     * @param form
     * @return
     */
    private String getKey(FlightBaseDataForm form) {
        return form.getFromDate() + form.getFlightNo() + form.getCabinCode();
    }

    /**
     * 计算2个时间的差
     *
     * @param one
     * @param two
     * @return
     */
    private String getDistanceTime(Date one, Date two) {
        long diff = Math.abs(one.getTime() - two.getTime());
        long day = diff / (24 * 60 * 60 * 1000);
        long hour = (diff / (60 * 60 * 1000) - day * 24);
        long min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
        return supplementZero(hour) + ":" + supplementZero(min);
    }

    private String supplementZero(long num) {
        if (num < 10) {
            return "0" + num;
        } else {
            return "" + num;
        }
    }

    /**
     * 订单创建时间 = 起飞时间-[1,7]随机天-[0,4]随机小时-[0,20]随机分钟
     *
     * @param date
     * @return
     */
    private Date getCreateOrderDate(Date date) {
        return DateUtils.addMinute(DateUtils.addHour(DateUtils.adddate(date, -rangeInt(1, 7)), -rangeInt(0, 4)), -rangeInt(0, 20));
    }

    /**
     * 支付时间 = 订单创建时间 + [1,5]随机分钟 + [0,10]随机秒钟
     * 出票时间 = 支付时间 + [1,5]随机分钟 + [1,10]随机秒钟
     *
     * @param date
     * @return
     */
    private Date getPayDateOrIssueDate(Date date) {
        return DateUtils.addSecond(DateUtils.addMinute(date, rangeInt(1, 5)), rangeInt(1, 10));
    }

    //获取范围内的随机数
    private int rangeInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    /**
     * 获取一个随机日期(成年人生日)
     *
     * @return
     */
    private Date getRandomDate() {
        Calendar calendar = Calendar.getInstance();
        int year = rangeInt(1977, 1990);
        int month = rangeInt(0, 12);
        int day = rangeInt(0, 28);
        calendar.set(year, month, day);
        return calendar.getTime();
    }


}
