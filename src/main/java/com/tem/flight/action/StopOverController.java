package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.DateUtils;
import com.tem.flight.api.FlightStopOverService;
import com.tem.flight.dto.flight.FlightStopOverDto;
import com.tem.platform.action.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author yuanjianxin
 * @date 2018/8/22 11:42
 */
@Controller
@RequestMapping("flight/stopover")
public class StopOverController extends BaseController {

    @Autowired
    private FlightStopOverService flightStopOverService;

    @RequestMapping(value = "")
    public String index(Model model) {
        return "flight/stopover/index.base";
    }

    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex, String flightNo) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        QueryDto<Object> queryDto = new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);
        if (StringUtils.isNotBlank(flightNo)) {
            queryDto.setField1(flightNo);
        }
        Page<FlightStopOverDto> page = flightStopOverService.queryListWithPage(queryDto);
        model.addAttribute("pageList", page);
        return "flight/stopover/list.jsp";
    }

    @RequestMapping("/detail")
    public String detail(Model model, Integer id) {
        if (id != null) {
            FlightStopOverDto dto = flightStopOverService.getById(id);
            model.addAttribute("stopOver", dto);
        }
        return "flight/stopover/detail.single";
    }

    @ResponseBody
    @RequestMapping("/delete/{id}")
    public ResponseInfo delete(@PathVariable("id") Integer id) {
        try {
            flightStopOverService.deleteById(id);
            return new ResponseInfo("1", "删除成功!", null);
        } catch (Exception e) {
            return new ResponseInfo("-1", "删除失败!", null);
        }
    }

    @ResponseBody
    @RequestMapping("/save")
    public ResponseInfo save(FlightStopOverDto dto, String from, String to) {
        try {
            dto.setFromTime(DateUtils.parse(from, "HH:mm"));
            dto.setToTime(DateUtils.parse(to, "HH:mm"));
            flightStopOverService.save(dto);
            return new ResponseInfo("1", "保存成功", null);
        } catch (NumberFormatException e) {
            return new ResponseInfo("-1", "保存失败，请刷新重试", null);
        }
    }

}
