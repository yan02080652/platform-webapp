package com.tem.flight.action;

import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.tem.flight.Constants;
import com.tem.flight.api.AirportService;
import com.tem.flight.api.IssueRuleService;
import com.tem.flight.dto.airport.FlightCityAirportDto;
import com.tem.flight.dto.enums.RegionType;
import com.tem.flight.dto.issueRule.IssueRuleDto;
import com.tem.flight.form.HotFlightCity;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.DistrictService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.UserDto;

/**
 * @author yuanjianxin
 * @date 2017/11/3 10:33
 */
@Controller
@RequestMapping("/flight/issueRule")
public class IssueRuleController extends BaseController {

    @Autowired
    private IssueRuleService issueRuleService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private DistrictService districtService;
    
    @Autowired
    private PartnerService partnerService;

	@Autowired
	private DictService dictService;

	@Autowired
	private AirportService airportService;

    @RequestMapping(value = "")
    public String index(Model model) {
		model.addAttribute("admin",super.isSuperAdmin());
        return "flight/issueRule/index.base";
    }

    // 分页查询
    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex, Integer pageSize, Long partnerId) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        //查询对象
        QueryDto<Object> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setField3(partnerId);

        //不是超级管理员只能看到自己的配置
        boolean superAdmin = super.isSuperAdmin();
        if (!superAdmin) {
            queryDto.setField1(super.getTmcId().toString());
        }

        Page<IssueRuleDto> pageList = issueRuleService.queryListWithPage(queryDto);
		//舱位map
		Map<String, String> cabinClassMap = getCabinClassMap();



        model.addAttribute("pageList", pageList);
        model.addAttribute("cabinClassMap", cabinClassMap);
        return "flight/issueRule/list.jsp";
    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public String delete(Model model, @PathVariable("id") Integer id) {
        boolean rs = issueRuleService.deleteById(id);
        return rs ? Constants.SUCCESS : Constants.FAILED;
    }
    
    
    @RequestMapping(value="/detail")
    public String detail(String id,String type, Model model){
    	
    	IssueRuleDto dto = null;
    	if(StringUtils.isNotBlank(id)) {
    		dto = issueRuleService.getById(Integer.parseInt(id));
    		UserDto creatorUser=userService.getUser(dto.getCreator());
    		if(creatorUser!=null) {
        		dto.setCreateName(creatorUser.getFullname());	
    		}
    		if(dto.getUpdater() != null) {
    			UserDto updaterUser=userService.getUser(dto.getUpdater());
        		if(updaterUser!=null) {
            		dto.setUpdateName(updaterUser.getFullname());	
        		}
        	}
    	}
    	if("0".equals(type) && dto != null) {
    		//克隆 去id 创建人 修改人
    		dto.setId(null);
    		dto.setCreator(null);
    		dto.setCreateDate(null);
    		dto.setUpdateDate(null);
    		dto.setUpdater(null);
    		dto.setCreateName(null);
    		dto.setUpdateName(null);
    	}

    	model.addAttribute("admin",super.isSuperAdmin());
    	model.addAttribute("issueRuleDto", dto);
    	model.addAttribute("type", StringUtils.isBlank(type) ? "1" : type);
    	return "flight/issueRule/detail.base";
    }
    
    
    @RequestMapping(value="/save")
    @ResponseBody
    public boolean save(IssueRuleDto dto){
    	
    	if(dto.getId() == null) {
    		dto.setCreateDate(new Date());
    		dto.setCreator(super.getCurrentUser().getId());
    	} else {
    		dto.setUpdateDate(new Date());
    		dto.setUpdater(super.getCurrentUser().getId());
    	}
    	
    	dto.setAllAirline(dto.getAllAirline() == null ? false : dto.getAllAirline());
    	dto.setRoundTrip(dto.getRoundTrip() == null ? false : dto.getRoundTrip());
    	dto.setAllCarrier(dto.getAllCarrier() == null ? false : dto.getAllCarrier());
    	dto.setAllCabin(dto.getAllCabin() == null ? false : dto.getAllCabin());
    	dto.setAllTime(dto.getAllTime() == null ? false : dto.getAllTime());
    	dto.setWeekend(dto.getWeekend() == null ? false : dto.getWeekend());
    	
    	dto.setPartnerId(dto.getPartnerId() == null ? super.getTmcId() : dto.getPartnerId());
		dto.setTmcId(dto.getTmcId() == null ? super.getTmcId() : dto.getTmcId());

		dto.setPartnerName(dto.getTmcId().equals(dto.getPartnerId()) ? partnerService.findById(dto.getTmcId()).getName() : dto.getPartnerName());
    	
    	boolean rs = issueRuleService.save(dto);
    	
    	return rs;
    }
    
    @ResponseBody
	@RequestMapping("/cityData")
	public String findCity(){
		//机场城市
		Map<String, List<FlightCityAirportDto>> cityMap = airportService.findCityMap(RegionType.DOMESTIC.getType());
		//热门机场
		List<FlightCityAirportDto> city = airportService.findCityByCode(HotFlightCity.getAllHotCityCode());

		Map<String,Object> dataMap = new HashMap<String, Object>();

		for(Map.Entry<String, List<FlightCityAirportDto>> entry : cityMap.entrySet()){
			dataMap.put(entry.getKey(),entry.getValue());
		}
		dataMap.put("热门",city);

		Map<String, Object> resultMap = sortMapByKey(dataMap);

		String jsonString = JSON.toJSONString(resultMap);

		return jsonString;
	}
    
    public static Map<String, Object> sortMapByKey(Map<String, Object> map) {
		if (map == null || map.isEmpty()) {
			return null;
		}

		Map<String, Object> sortMap = new TreeMap<String, Object>(new Comparator<String>() {
			@Override
			public int compare(String str1, String str2) {
				return str1.compareTo(str2);
			}
		});

		sortMap.putAll(map);

		return sortMap;
	}


	private Map<String, String> getCabinClassMap() {
		List<DictCodeDto> codes = dictService.getCodes(Constants.OTAF_CABIN_GRADE);
		Map<String, String> cabinClassMap = new HashMap<String, String>();
		for (DictCodeDto codeDto : codes) {
			cabinClassMap.put(codeDto.getItemCode(), codeDto.getItemTxt());
		}
		return cabinClassMap;
	}
}
