package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.web.Message;
import com.tem.flight.Constants;
import com.tem.flight.api.CabinService;
import com.tem.flight.api.CarrierService;
import com.tem.flight.dto.cabin.CabinCondition;
import com.tem.flight.dto.cabin.CabinDto;
import com.tem.flight.dto.carrier.CarrierDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.dto.DictCodeDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("flight/cabin")
public class CabinController extends BaseController {

	@Autowired
	private CabinService  cabinService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private CarrierService carrierService;
	
	@RequestMapping(value = "")
	public String index(Model model,String carrierCode) {
		CarrierDto carrierDto = carrierService.findByCode(carrierCode);
		model.addAttribute("dicMap",getDicMap());
		model.addAttribute("carrierDto",carrierDto);
		return "flight/cabin/index.jsp";
	}
	
	// 分页查询
	@RequestMapping(value = "/list")
	public String list(Model model, Integer pageIndex, Integer pageSize,String cabinCode,Integer cabinType ,String cabinGrade,String carrierCode) {
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}
		//查询对象
		QueryDto<CabinCondition> queryDto = new QueryDto<CabinCondition>(pageIndex, pageSize);
		//自定义条件封装对象
		CabinCondition cabinCondition = new CabinCondition();
		
		cabinCondition.setCarrierCode(carrierCode);
		cabinCondition.setCabinType(cabinType);
		if (StringUtils.isNotBlank(cabinGrade)){
			cabinCondition.setCabinGrade(cabinGrade);
		}
		if (cabinCode != null && !"".equals(cabinCode)) {
			cabinCondition.setCabinCode(cabinCode);
		}
		
		queryDto.setCondition(cabinCondition);
		
		Page<CabinDto> pageList = cabinService.queryListWithPage(queryDto);
		
		model.addAttribute("pageList", pageList);
		return "flight/cabin/cabinTable.jsp";
	}
		
		// 批量删除
		@ResponseBody
		@RequestMapping("/delete")
		public Message delete(String id) {
			String[] split = id.split(",");
			Integer[] ids = new Integer[split.length];
			for (int i = 0; i < split.length; i++) {
				ids[i] = Integer.parseInt(split[i]);
			}
			boolean rs = cabinService.batchDelete(ids);
			if (rs) {
				return Message.success("删除成功");
			} else {
				return Message.error("删除失败，请刷新重试");
			}
		}
		
		// 用于新增和修改时查询对象
		@RequestMapping(value = "/getCabin")
		public String getCabin(Model model, Integer id,String carrierCode) {
			CabinDto cabinDto = null;
			if (id != null && !"".equals(id)) {
				cabinDto = cabinService.findById(id);
			}
			model.addAttribute("cabinDto", cabinDto);
			model.addAttribute("carrierCode", carrierCode);
			model.addAttribute("dicMap",getDicMap());
			return "flight/cabin/cabinModel.jsp";
		}
		
		// 保存对象
		@ResponseBody
		@RequestMapping(value = "/save")
		public Message addOrUpdate(Model model, CabinDto cabinDto) {
			boolean rs = cabinService.save(cabinDto);
			if (rs) {
				return Message.success("保存成功");
			}else{
				return Message.error("保存失败,请刷新重试");
			}
		}
		
	// 查询舱位类型和舱位等级
	@ResponseBody
	@RequestMapping("/getDicMap")
	public Map<String, List<DictCodeDto>> getDicMap() {
		Map<String, List<DictCodeDto>> map = new HashMap<String, List<DictCodeDto>>();
		List<DictCodeDto> cabinType = dictService.getCodes(Constants.OTAF_CABIN_TYPE);
		List<DictCodeDto> cabinGrade = dictService.getCodes(Constants.OTAF_CABIN_GRADE);
		map.put("cabinType", cabinType);
		map.put("cabinGrade", cabinGrade);
		return map;
	}

	// 查询所有航司
	@ResponseBody
	@RequestMapping("/getCarrierList")
	public List<CarrierDto> getCarrierList() {
		List<CarrierDto> list = carrierService.findAll();
		return list;
	}
	
	//验证舱位编码唯一（在同一个航司下唯一）
	@ResponseBody
	@RequestMapping("/checkCode")
	public boolean checkCode(String code,String carrierCode,Integer id){
		CabinDto cabinDto = cabinService.findByCabinCodeAndCarrierCode(code, carrierCode);
		if(cabinDto==null){
			return true;
		}else if(cabinDto.getId().equals(id)){
			return true;
		}
		return false;
	}
	
}
