package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.web.Message;
import com.tem.flight.api.AirportService;
import com.tem.flight.api.CityAirportService;
import com.tem.flight.dto.airport.AirportDto;
import com.tem.flight.dto.airport.CityAirportDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DistrictService;
import com.tem.platform.api.dto.DistrictDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("flight/cityAirport")
public class CityAirPortController extends BaseController {
	
	@Autowired
	private CityAirportService cityAirportService;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private AirportService airportService;
	
	@RequestMapping(value = "")
	public String index(Model model) {
		List<DistrictDto> cityList = districtService.findCity();
		model.addAttribute("cityList",cityList);
		return "flight/cityAirport/index.base";
	}
	
	// 分页查询
	@RequestMapping(value = "/list")
	public String list(Model model, Integer pageIndex, Integer pageSize,String cityCode,String airportCode) {
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}
		
		QueryDto<Object> queryDto = new QueryDto<Object>(pageIndex,pageSize);

		if (cityCode != null && !"".equals(cityCode)) {
			queryDto.setField1(cityCode);
		}
		if (airportCode != null && !"".equals(airportCode)) {
			queryDto.setField2(airportCode);
		}
		
		Page<CityAirportDto> pageList = cityAirportService.queryListWithPage(queryDto);
		
		model.addAttribute("pageList", pageList);
		return "flight/cityAirport/cityAirportTable.jsp";
	}
	
	// 批量删除
	@ResponseBody
	@RequestMapping("/delete")
	public Message delete(String id) {
		String[] split = id.split(",");
		Integer[] ids = new Integer[split.length];
		for (int i = 0; i < split.length; i++) {
			ids[i] = Integer.parseInt(split[i]);
		}
		boolean rs = cityAirportService.batchDelete(ids);
		if (rs) {
			return Message.success("删除成功");
		} else {
			return Message.error("删除失败，请刷新重试");
		}
	}
	
	// 用于新增和修改时查询对象
	@RequestMapping(value = "/getCityAirport")
	public String getCityAirport(Model model, Integer id) {
		CityAirportDto cityAirportDto = null;
		if (id != null && !"".equals(id)) {
			cityAirportDto = cityAirportService.findById(id);
		}
		model.addAttribute("cityAirportDto", cityAirportDto);
		model.addAttribute("dataMap", getCitysAndAirport());
		return "flight/cityAirport/cityAirportModel.jsp";
	}
	
	//保存对象
	@ResponseBody
	@RequestMapping(value="/save")	
	public Message addOrUpdate(Model model,CityAirportDto cityAirportDto){
		if (isExist(cityAirportDto.getCityCode(),cityAirportDto.getAirportCode(),cityAirportDto.getId())) {
			return Message.error("该对应关系已经存在");
		}
		boolean rs = false;
		if (cityAirportDto.getId() == null) {// 新增时设置城市Id
			DistrictDto districtDto = districtService.findByCode(cityAirportDto.getCityCode());
			cityAirportDto.setCityId(districtDto.getId());
		}
		//保存
		rs = cityAirportService.save(cityAirportDto);
		
		if (rs) {
			return Message.success("添加成功");
		} else {
			return Message.error("添加失败,请刷新重试");
		}
	}
	
	// 根据城市编码和机场编码验证对象是否存在(true:存在   false:不存在)
	public boolean isExist(String cityCode,String airportCode,Integer id) {
		CityAirportDto cityAirportDto = cityAirportService.findByCityCodeAndAirportCode(cityCode, airportCode);
		if(cityAirportDto==null){
			return false;
		}else if(cityAirportDto.getId().equals(id)){
			return false;
		}else{
			return true;
		}
	}

	// 查询所有城市和直辖市和机场列表
	@ResponseBody
	@RequestMapping("/getCitysAndAirport")
	public Map<String,List<?>> getCitysAndAirport() {
		Map<String,List<?>> dataMap = new HashMap<String, List<?>>();
		 List<DistrictDto> cityList = districtService.findCity();
		 List<AirportDto> airportList = airportService.findAll();
		 dataMap.put("cityList",cityList);
		 dataMap.put("airportList", airportList);
		 return dataMap;
	}
}
