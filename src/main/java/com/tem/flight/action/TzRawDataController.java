package com.tem.flight.action;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.tem.flight.dto.enums.HubsCodeEnum;
import com.tem.platform.action.BaseController;
import com.tem.pss.api.ProviderLogService;
import com.tem.pss.dto.provider.FlightMethodEnum;
import com.tem.pss.dto.provider.ProviderLogCondition;
import com.tem.pss.dto.provider.ProviderLogDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("flight/tzRawData")
public class TzRawDataController extends BaseController {

    @Autowired
    private ProviderLogService providerLogService;

    @RequestMapping(value = "")
    public String index(Model model) {
        model.addAttribute("bizList", OrderBizType.values());
        return "flight/tzRawData/index.base";
    }

    @ResponseBody
    @RequestMapping("/paramsList")
    public Map<String, List<Object>> methodList(OrderBizType bizType) {
        Map<String, List<Object>> map = new HashMap<>(2);
        List<Object> hubsList = new ArrayList<>();
        List<Object> methodList = new ArrayList<>();
        switch (bizType) {
            case FLIGHT:
                for (FlightMethodEnum flightMethodEnum : FlightMethodEnum.values()) {
                    JSONObject json = new JSONObject();
                    json.put("name", flightMethodEnum.name());
                    json.put("msg", flightMethodEnum.getMsg());
                    methodList.add(json);
                }
                for (HubsCodeEnum hubsCodeEnum : HubsCodeEnum.values()) {
                    hubsList.add(hubsCodeEnum.name());
                }
                break;
            default:
                break;
        }
        map.put("hubsList", hubsList);
        map.put("methodList", methodList);
        return map;
    }

    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex, String hubsCode, String methodName, String bizType,
                       @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date startTime,
                       @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date endTime) {
        if (pageIndex == null) {
            pageIndex = 1;
        }

        QueryDto<ProviderLogCondition> queryDto = new QueryDto<ProviderLogCondition>(pageIndex, Page.DEFAULT_PAGE_SIZE);
        ProviderLogCondition condition = new ProviderLogCondition();
        if (StringUtils.isNotBlank(hubsCode)) {
            condition.setHubsCode(hubsCode);
        }
        if (StringUtils.isNotBlank(bizType)) {
            condition.setBizType(bizType);
        }
        if (StringUtils.isNotBlank(methodName)) {
            condition.setMethodName(methodName);
        }
        condition.setStartTime(startTime);
        condition.setEndTime(endTime);
        queryDto.setCondition(condition);
        Page<ProviderLogDto> page = providerLogService.queryListWithPage(queryDto);
        model.addAttribute("pageList", page);
        return "flight/tzRawData/list.jsp";
    }


    @RequestMapping("/detail/{id}")
    public String detail(Model model, @PathVariable("id") Integer id) {
        ProviderLogDto log = providerLogService.findById(id);
        int length = log.getResponseXml().getBytes().length;
        //500K
        int maxLength = 500 * 1024;
        boolean show = false;
        if (length > maxLength) {
            log.setResponseXml(null);
            show = true;
        }
        log.setId(id);
        model.addAttribute("log", log);
        model.addAttribute("showDownload", show);
        return "flight/tzRawData/detail.jsp";
    }

    @RequestMapping("/responseXml/download/{id}")
    public ResponseEntity<byte[]> download(@PathVariable("id") Integer id) {
        try {
            ProviderLogDto logDto = providerLogService.findById(id);
            byte[] body = logDto.getResponseXml().getBytes();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "attchement;filename=" + id + ".xml");
            HttpStatus statusCode = HttpStatus.OK;
            ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>(body, headers, statusCode);
            return entity;
        } catch (Exception e) {
            return null;
        }
    }

}