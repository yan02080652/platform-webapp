package com.tem.flight.action;

import com.iplatform.common.Config;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.web.Message;
import com.tem.flight.ExcelUtil;
import com.tem.flight.api.CarrierService;
import com.tem.flight.dto.agreement.WhitelistUserDto;
import com.tem.flight.dto.carrier.CarrierDto;
import com.tem.flight.dto.carrier.CarrierType;
import com.tem.imgserver.client.ImgClient;
import com.tem.platform.action.BaseController;
import com.tem.platform.form.ResponseTable;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("flight/carrier")
public class CarrierController extends BaseController {

	@Autowired
	private CarrierService carrierService;

	@RequestMapping(value = "")
	public String index(Model model) {
		return "flight/carrier/index.base";
	}
	
	//查看航司-航段列表
	@RequestMapping(value = "/existingAirlineTable")
	public String existingAirlineTable(Model model,String carrierCode,Integer type) {
		CarrierDto carrierDto = carrierService.findByCode(carrierCode);
		model.addAttribute("type",type);
		model.addAttribute("carrierCode",carrierCode);
		model.addAttribute("carrierName",carrierDto.getNameCn());
		return "flight/carrier-airline/index.jsp";
	}

	// 分页查询
	@RequestMapping(value = "/list")
	public String list(Model model, Integer pageIndex, Integer pageSize,String keyword) {
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}
		
		QueryDto<Object> queryDto = new QueryDto<Object>(pageIndex,pageSize);
		
		if (keyword != null && !"".equals(keyword)) {
			queryDto.setField1(keyword);
		}
		
		Page<CarrierDto> pageList = carrierService.queryListWithPage(queryDto);
		
		model.addAttribute("pageList", pageList);
		return "flight/carrier/carrierTable.jsp";
	}

	// 批量删除
	@ResponseBody
	@RequestMapping("/delete")
	public Message delete(String codes) {
		String[] split = codes.split(",");
		boolean rs = carrierService.batchDelete(split);
		if (rs) {
			return Message.success("删除成功");
		} else {
			return Message.error("删除失败，请刷新重试");
		}
	}

	// 用于新增和修改时查询对象
	@RequestMapping(value = "/getCarrier")
	public String getCarrier(Model model, String code) {
		CarrierDto carrierDto = null;
		if (code != null && !"".equals(code)) {
			carrierDto = carrierService.findByCode(code);
		}
		model.addAttribute("carrierDto", carrierDto);
		return "flight/carrier/carrierModel.jsp";
	}
	
	//保存对象
	@ResponseBody
	@RequestMapping(value="/save")	
	public Message addOrUpdate(Model model,CarrierDto carrierDto,MultipartFile carrier_logo){
		
		try {
			// 图片上传
			if (carrier_logo != null) {
				String fileName = carrier_logo.getOriginalFilename();
				if (StringUtils.isNotEmpty(fileName)) {
					// 获取后缀名
					String img_suffix = fileName.substring(fileName.lastIndexOf("."));
					// 生成一个新的文件名
					String newName = "";
					if(StringUtils.isNotEmpty(carrierDto.getCode())){
						newName = carrierDto.getCode() + img_suffix;
					}else{
						newName = UUID.randomUUID() + img_suffix;
					}
					// 图片存放的位置
					String path = Config.getString("logoroot");
					
					ImgClient.uploadImg(carrier_logo, path,newName);
					
					carrierDto.setLogo(path + "/" + newName);
				}
			}
		} catch (Exception e) {
			return Message.error("文件上传出错，请刷新重试");
		}
		
		boolean rs = carrierService.save(carrierDto);
		if (rs) {
			return Message.success("保存成功");
		} else {

			return Message.error("保存失败,请刷新重试");
		}
	}
	
	//验证编码唯一
	@ResponseBody
	@RequestMapping("/checkCode")
	public boolean checkCode(String code) {
		if (code == null || "".equals(code)) {
			return true;
		}
		CarrierDto carrierDto = carrierService.findByCode(code);
		if (carrierDto == null) {
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 航司分页查询:航司选择器使用
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/carrierData")
	public ResponseTable carrierData(){
		Integer pageIndex = super.getIntegerValue("pageIndex");
		Integer pageSize = super.getIntegerValue("pageSize");
		if(pageIndex == null){
			pageIndex = 1;
		}
		if(pageSize == null){
			pageSize = 10;
		}
		QueryDto<Object> queryDto = new QueryDto<>(pageIndex,pageSize);
		String keyword = super.getStringValue("keyword");
		if (StringUtils.isNotBlank(keyword)) {
			queryDto.setField1(keyword);
		}
		Page<CarrierDto> page = carrierService.queryListWithPage(queryDto);
		return new ResponseTable(page);
	}

	@RequestMapping(value = "/importCarrier")
	@ResponseBody
	public ResponseInfo importUser(Model model, @RequestParam("file") MultipartFile file){
		try {
			List<CarrierDto> carrierDtos = new ArrayList<>();
			carrierDtos = getCarrierFromExcel(file.getInputStream(), carrierDtos);
			carrierService.batchInsert(carrierDtos);
			return new ResponseInfo("0", "导入成功!");
		}catch (Exception e) {
			return new ResponseInfo("1", "导入失败!");
		}

	}


	private List<CarrierDto> getCarrierFromExcel(InputStream fis, List<CarrierDto> carrierDtos) throws InvalidFormatException, IOException {
		if (carrierDtos == null) {
			carrierDtos = new ArrayList<>();
		}

		Workbook wb = WorkbookFactory.create(fis);

		int sheetNumber = wb.getNumberOfSheets();
		//只读取第一个表
		sheetNumber = 1;

		for (int numSheet = 0; numSheet < sheetNumber; numSheet++) {
			Sheet sheet = wb.getSheetAt(numSheet);
			if (sheet == null) {
				continue;
			}
			// 循环行Row, 从第四行开始
			for (int rowNum = 1; rowNum <= sheet.getLastRowNum(); rowNum++) {
				Row row = sheet.getRow(rowNum);
				if (row == null) {
					continue;
				}

				String code = ExcelUtil.getStringValueFromCell(row.getCell(0));
				String nameCn = ExcelUtil.getStringValueFromCell(row.getCell(1));
				String nameEn = ExcelUtil.getStringValueFromCell(row.getCell(2));


				if(StringUtils.isBlank(code)) {
					continue;
				}
				//新建一个user对象
				CarrierDto carrierDto = new CarrierDto();
				carrierDto.setCode(code);
				carrierDto.setNameCn(nameCn);
				carrierDto.setNameShort(nameCn);
				carrierDto.setNameEn(nameEn);
				carrierDto.setType(CarrierType.INTERNATIONAL);
				carrierDtos.add(carrierDto);

			}
		}

		return carrierDtos;
	}
}
