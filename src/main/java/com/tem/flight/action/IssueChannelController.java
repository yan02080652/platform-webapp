package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.tem.flight.Constants;
import com.tem.flight.api.HubsConnectService;
import com.tem.flight.dto.hubs.HubsConnectDto;
import com.tem.hotel.api.hotel.HotelHubConnectorService;
import com.tem.hotel.query.HotelHubsConnectQuery;
import com.tem.hotel.result.HotelHubsConnectResult;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.product.api.ProductHubsService;
import com.tem.product.dto.ProductHubsDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import com.tem.supplier.api.SettleChannelService;
import com.tem.supplier.dto.SettleChannelDto;

import com.tem.train.api.hubs.TrainHubsService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("flight/issueChannel")
public class IssueChannelController extends BaseController {

    @Autowired
    private IssueChannelService issueChannelService;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private SettleChannelService settleChannelService;

    @Autowired
    private HubsConnectService hubsConnectService;

    @Autowired
    private ProductHubsService productHubsService;

    @Autowired
    private TrainHubsService trainHubsService;

    @Autowired
    private HotelHubConnectorService hotelHubConnectorService;



    @RequestMapping(value = "")
    public String index(Model model) {

        model.addAttribute("admin",super.isSuperAdmin());
        model.addAttribute("typeMap",getTypeMap());
        model.addAttribute("lineTypes", LineType.getLineTypeMap());
        return "flight/issueChannel/index.base";
    }

    // 分页查询
    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex, Integer pageSize, String tmcId,String channelType,Integer lineType) {

        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        QueryDto<Object> queryDto = new QueryDto<>(pageIndex, pageSize);

        if (tmcId != null && !"".equals(tmcId)) {
            queryDto.setField1(tmcId);
        }

        boolean admin = super.isSuperAdmin();
        queryDto.setField4(lineType);

        if(!admin) {
            //非超級管理員只能查看自己的出票規則
            queryDto.setField3(super.getTmcId());
            //非管理员只能查看线下的出票规则
            queryDto.setField4(1);
        }

        if(channelType != null && !"".equals(channelType)) {
            queryDto.setField2(channelType);
        }

        Page<IssueChannelDto> pageList = issueChannelService.queryListWithPage(queryDto);

        Map<String, Map<String,List<IssueChannelDto>>> map = new LinkedHashMap<>();
        Map<String,Integer> countMap = new HashMap<>();
        List<IssueChannelDto> list = pageList.getList();
        if (CollectionUtils.isNotEmpty(list)) {
            for (IssueChannelDto issueChannelDto : list) {
                PartnerDto partnerDto = partnerService.findById(Long.valueOf(issueChannelDto.getTmcId()));
                if (partnerDto != null) {

                    if(map.containsKey(partnerDto.getName())) {
                        Map<String, List<IssueChannelDto>> sonMap = map.get(partnerDto.getName());
                        if(sonMap.containsKey(IssueChannelType.valueOfType(issueChannelDto.getChannelType()))) {
                            map.get(partnerDto.getName()).get(IssueChannelType.valueOfType(issueChannelDto.getChannelType())).add(issueChannelDto);
                        } else {
                            Map<String,List<IssueChannelDto>> map1 = new LinkedHashMap<>();
                            List<IssueChannelDto> dtoList = new ArrayList<>();
                            dtoList.add(issueChannelDto);
                            map.get(partnerDto.getName()).put(IssueChannelType.valueOfType(issueChannelDto.getChannelType()),dtoList);
                        }
                        Integer integer = countMap.get(partnerDto.getName());
                        countMap.put(partnerDto.getName(),integer+1);
                    } else {
                        Map<String,List<IssueChannelDto>> sonMap = new LinkedHashMap<>();
                        if(sonMap.containsKey(IssueChannelType.valueOfType(issueChannelDto.getChannelType()))) {
                            sonMap.get(IssueChannelType.valueOfType(issueChannelDto.getChannelType())).add(issueChannelDto);
                        } else {
                            List<IssueChannelDto> dtoList = new ArrayList<>();
                            dtoList.add(issueChannelDto);
                            sonMap.put(IssueChannelType.valueOfType(issueChannelDto.getChannelType()),dtoList);
                            map.put(partnerDto.getName(),sonMap);
                        }
                        countMap.put(partnerDto.getName(),1);
                    }

                }
            }
        }

        model.addAttribute("countMap",countMap);
        model.addAttribute("pageList", pageList);
        model.addAttribute("dataMap", map);
        model.addAttribute("admin",admin);
        model.addAttribute("lineTypes",LineType.getLineTypeMap());
        return "flight/issueChannel/list.jsp";
    }


    @RequestMapping("/detail")
    public String detail(Model model, Integer id) {
        IssueChannelDto issueChannelDto = null;
        if (id != null) {
            issueChannelDto = issueChannelService.getById(id);
            if (issueChannelDto != null) {
                PartnerDto partnerDto = partnerService.findById(Long.valueOf(issueChannelDto.getTmcId()));
                model.addAttribute("partnerDto", partnerDto);
            }
        }
        List<SettleChannelDto> settleChannels = settleChannelService.findSettleChannelByTmcId(super.getTmcId(), 0);

        model.addAttribute("settleChannels",settleChannels);
        model.addAttribute("partner",partnerService.findById(super.getTmcId()));
        model.addAttribute("admin",super.isSuperAdmin());
        model.addAttribute("typeMap",getTypeMap());
        model.addAttribute("issueChannelDto", issueChannelDto);
        model.addAttribute("lineTypes", LineType.getLineTypeMap());
        return "flight/issueChannel/detail.jsp";
    }

    @RequestMapping("/save")
    @ResponseBody
    public String save(Model model, IssueChannelDto issueChannelDto) {
        if(!super.isSuperAdmin()) {
            //非管理员添加的只能时线下出票的渠道
            issueChannelDto.setLineType(1);
        }
        //添加 检查code唯一性

        IssueChannelDto dto = issueChannelService.checkCode(issueChannelDto);

        if(dto != null) {
            if(issueChannelDto.getId() == null) {
                return Constants.EXIST;
            } else {
                if(!dto.getId().equals(issueChannelDto.getId())) {
                    return Constants.EXIST;
                }
            }
        }
        if(issueChannelDto.getId() != null) {
            issueChannelDto.setUpdator(super.getCurrentUser().getId());
        }
        if(StringUtils.isBlank(issueChannelDto.getTmcId())) {
            issueChannelDto.setTmcId(String.valueOf(super.getTmcId()));
        }

        boolean rs = issueChannelService.save(issueChannelDto);
        return rs ? Constants.SUCCESS : Constants.FAILED;
    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public String delete(Model model, @PathVariable("id") Integer id) {
        //TODO 需检查其是否被连接关联，如果是，则提示用户"该供应商与【连接名称】存在关联，无法删除"
        IssueChannelDto channelDto = issueChannelService.getById(id);

        if(LineType.ON_LINE.getType().equals(channelDto.getLineType())) {
            if (IssueChannelType.FLIGHT.name().equals(channelDto.getChannelType())) {
                List<HubsConnectDto> connects = hubsConnectService.findConnectsByChannelCode(channelDto.getCode(), 0);
                if(CollectionUtils.isNotEmpty(connects)) {
                    return Constants.EXIST;
                }
            } else if(IssueChannelType.INTERNATIONAL_FLIGHT.name().equals(channelDto.getChannelType())){
                List<HubsConnectDto> connects = hubsConnectService.findConnectsByChannelCode(channelDto.getCode(), 1);
                if(CollectionUtils.isNotEmpty(connects)) {
                    return Constants.EXIST;
                }
            } else if(IssueChannelType.INSURANCE.name().equals(channelDto.getChannelType())) {
                List<ProductHubsDto> hubs = productHubsService.findHubsByIssueChannelId(channelDto.getId());
                if(CollectionUtils.isNotEmpty(hubs)) {
                    return Constants.EXIST;
                }
            } else if(IssueChannelType.HOTEL.name().equals(channelDto.getChannelType())) {
                HotelHubsConnectQuery query=new HotelHubsConnectQuery();
                query.setChannelCode(channelDto.getCode());
                List<HotelHubsConnectResult> hubs=hotelHubConnectorService.findConnect(query).getData();
                if(CollectionUtils.isNotEmpty(hubs)) {
                    return Constants.EXIST;
                }

            } else if(IssueChannelType.TRAIN.name().equals(channelDto.getChannelType())){
                List<com.tem.train.dto.HubsConnectDto> TrainConnects = trainHubsService.findHubsConnecctByChannelCode(channelDto.getCode());
                if(CollectionUtils.isNotEmpty(TrainConnects)){
                    return Constants.EXIST;
                }
            }
        }


        boolean rs = issueChannelService.delete(id);
        return rs ? Constants.SUCCESS : Constants.FAILED;
    }

    private Map<String,String> getTypeMap(){
        Map<String,String> map = new LinkedHashMap<>();
        IssueChannelType[] values = IssueChannelType.values();
        for (IssueChannelType i : values) {
            map.put(i.getType(),i.getMsg());
        }
        return map;
    }

}
