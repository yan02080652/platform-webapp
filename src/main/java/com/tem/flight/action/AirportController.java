package com.tem.flight.action;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.web.Message;
import com.tem.flight.api.AirportService;
import com.tem.flight.api.CityAirportService;
import com.tem.flight.dto.airport.AirportDto;
import com.tem.flight.dto.airport.CityAirportDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DistrictService;
import com.tem.platform.api.dto.DistrictDto;
import com.tem.platform.form.ResponseTable;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("flight/airport")
public class AirportController extends BaseController {
	@Autowired
	private AirportService airportService;
	
	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private CityAirportService cityAirportService;
	

	@RequestMapping(value = "")
	public String index(Model model) {
		model.addAttribute("admin",super.isSuperAdmin());
		return "flight/airport/index.base";
	}

	// 分页查询
	@RequestMapping(value = "/list")
	public String list(Model model, Integer pageIndex, Integer pageSize,String keyword,String type,String airportType) {
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}

		//查询对象
		QueryDto<Object> queryDto=new QueryDto<>(pageIndex,pageSize);
		//查询条件
		queryDto.setField1(keyword);

		queryDto.setField2(StringUtils.isNotBlank(type) ? type : null);
		queryDto.setField3(StringUtils.isNotBlank(airportType) ? Long.valueOf(airportType) : null);

		Page<AirportDto> pageList = airportService.queryListWithPage(queryDto);

		model.addAttribute("pageList", pageList);
		return "flight/airport/airportTable.jsp";
	}

	// 批量删除
	@ResponseBody
	@RequestMapping("/delete")
	public Message delete(String ids) {
		String[] split = ids.split(",");
		boolean rs = airportService.batchDelete(split);
		if (rs) {
			return Message.success("删除成功");
		} else {
			return Message.error("删除失败，请刷新重试");
		}
	}
	
	// 用于新增和修改时查询对象
	@RequestMapping(value = "/getAirport")
	public String getAirport(Model model, String id) {
		AirportDto airportDto = null;
		if (id != null && !"".equals(id)) {
			airportDto = airportService.findById(id);
			JSONObject jo = new JSONObject();
			jo.put("id", airportDto.getCityId());
			jo.put("nameCn", airportDto.getCityName());
			model.addAttribute("city", jo.toJSONString());
		}

		model.addAttribute("airportDto", airportDto);
		return "flight/airport/airportModel.jsp";
	}
	
	//保存对象
	@ResponseBody
	@RequestMapping(value="/save")	
	public Message addOrUpdate(Model model,AirportDto airportDto){
		boolean rs = airportService.save(airportDto);
		if (rs) {
			return Message.success("保存成功");
		} else {
			return Message.error("保存失败,请刷新重试");
		}
	}
	
	// 验证编码唯一
	@ResponseBody
	@RequestMapping("/checkCode")
	public boolean checkCode(String code,String type) {
		if (code == null || "".equals(code)) {
			return true;
		}
		AirportDto airportDto = airportService.findByCodeAndType(code,type);
		if (airportDto == null) {
			return true;
		} else {
			return false;
		}
	}
	
	// 查询所有城市和直辖市
	@ResponseBody
	@RequestMapping("/getCitys")
	public List<DistrictDto> getCitys() {
		return districtService.findCity();
	}
	
	//查询机场附近的城市列表
	@RequestMapping("/getNearCityList")
	public String getNearCityList(Model model,String airportCode){
		//城市-机场关系
		List<CityAirportDto> cityAirportList = cityAirportService.findByAirportCode(airportCode);
		
		//查询城市列表
		List<DistrictDto> cityList  = new ArrayList<DistrictDto>();
		
		for(CityAirportDto cityAirportDto : cityAirportList ){
			DistrictDto city = districtService.findByCode(cityAirportDto.getCityCode());
			if(city!=null){
				cityList.add(city);
			}
		}
		
		model.addAttribute("cityList",cityList);
		
		return "flight/airport/nearCity.jsp";
	}


	/**
	 * 城市分页查询
	 * @return
	 */
	@ResponseBody
	@RequestMapping("districtData")
	public ResponseTable districtData(){
		Integer pageIndex = super.getIntegerValue("pageIndex");
		Integer pageSize = super.getIntegerValue("pageSize");
		if(pageIndex == null){
			pageIndex = 1;
		}
		if(pageSize == null){
			pageSize = 10;
		}
		QueryDto<Object> queryDto = new QueryDto<>(pageIndex,pageSize);
		queryDto.setCondition("flight");
		String name = super.getStringValue("name");
		if (StringUtils.isNotBlank(name)) {
			queryDto.setField2(name);
		}

		Page<DistrictDto> districtDtoPage = districtService.queryListWithPage(queryDto);
		return new ResponseTable(districtDtoPage);
	}

	@RequestMapping(value = "batchUpdateAirportCityData")
	@ResponseBody
	public boolean batchUpdateAirportCityData(){
		//异步跟新 直接返回
		airportService.batchUpdateAirportCityData();
		return true;
	}

}
