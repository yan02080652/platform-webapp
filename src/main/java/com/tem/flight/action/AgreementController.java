package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.flight.api.CabinService;
import com.tem.flight.api.HubsConnectService;
import com.tem.flight.api.agreement.AgreementPolicyService;
import com.tem.flight.api.agreement.WhitelistService;
import com.tem.flight.dto.agreement.*;
import com.tem.flight.dto.cabin.CabinDto;
import com.tem.flight.dto.enums.FilterTypeEnum;
import com.tem.flight.dto.enums.PolicyType;
import com.tem.flight.dto.enums.ProductTypeEnum;
import com.tem.flight.dto.enums.RegionType;
import com.tem.flight.dto.hubs.HubsConnectDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author yuanjianxin
 */
@Controller
@RequestMapping("flight/agreement")
public class AgreementController extends BaseController {
    public static final String FAILED = "-1";
    public static final String SUCCESS = "0";
    public static final String CONFIRM = "1";

    @Autowired
    private AgreementPolicyService agreementPolicyService;
    @Autowired
    private UserService userService;
    @Autowired
    private PartnerService partnerService;
    @Autowired
    private CabinService cabinService;
    @Autowired
    private HubsConnectService hubsConnectService;
    @Autowired
    private WhitelistService whitelistService;

    @RequestMapping(value = "")
    public String index(Model model) {
        model.addAttribute("policyTypeMap", PolicyType.getMaps());
        return "flight/agreement/index.base";
    }

    /**
     * 分页查询
     */
    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex, Integer pageSize,
                       AgreementPolicySearchForm form) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        QueryDto<Object> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(form);

        Page<AgreementPolicyDto> pageList = agreementPolicyService.queryListWithPage(queryDto);

        //将每个规则的使用范围的企业名称查询出来
        List<AgreementPolicyDto> list = pageList.getList();
        for (AgreementPolicyDto dto : list) {
            if("0".equals(String.valueOf(dto.getType()))) {
                PartnerDto partnerDto = partnerService.findById(dto.getPartnerScope().longValue());
                if (partnerDto != null) {
                    dto.setPartnerName(partnerDto.getName());
                }
            } else if("1".equals(String.valueOf(dto.getType()))){
                List<AgreementShareScopeDto> scopeDtos = agreementPolicyService.findShareScopeDtos(String.valueOf(dto.getId()));
                if(CollectionUtils.isEmpty(scopeDtos)) {
                    dto.setPartnerName("");
                } else {
                    PartnerDto partnerDto = partnerService.findById(scopeDtos.get(0).getPartnerId().longValue());
                    if (partnerDto != null) {
                        dto.setPartnerName(partnerDto.getName());
                    }
                    if(scopeDtos.size() == 1) {
                        dto.setPartnerName(partnerDto.getName());
                    }else {
                        dto.setPartnerName(partnerDto.getName()+"...等"+scopeDtos.size()+"家企业");
                    }
                }
            }
        }
        model.addAttribute("pageList", pageList);
        model.addAttribute("policyTypeMap", PolicyType.getMaps());
        return "flight/agreement/list.jsp";
    }

    /**
     * 枚举类型map映射
     *
     * @return
     */
    @ModelAttribute("agreementTypeEnumMap")
    private Map<String, String> getAgreementTypeEnumMap() {
        Map<String, String> map = new HashMap<>(16);
        for (ProductTypeEnum agreementTypeEnum : ProductTypeEnum.agreementTypeList()) {
            map.put(agreementTypeEnum.toString(), agreementTypeEnum.getMsg());
        }
        return map;
    }


    /**
     * 删除
     *
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public ResponseInfo delete(Integer id) {

        AgreementPolicyDto policyDto = agreementPolicyService.getById(id);

        List<AgreementPolicyDto> list = agreementPolicyService.findPolicyListByOriginId(id);
        if(CollectionUtils.isNotEmpty(list)) {
            return new ResponseInfo(CONFIRM, "存在共享协议，无法删除!");
        }

        if(policyDto.getWhiteListControl() != null &&policyDto.getWhiteListControl()) {
            AgreementWhitelistDto whitelist = agreementPolicyService.findWhitelistByPolicyCode(policyDto.getCustomerAgreementCode());
            if(whitelist != null) {
                return new ResponseInfo(CONFIRM, "存在白名单关联，无法删除!");
            }
        }

        boolean result = agreementPolicyService.deleteById(id);
        if (result) {
            return new ResponseInfo(SUCCESS, "删除成功!");
        } else {
            return new ResponseInfo(FAILED, "删除失败!");
        }
    }

    /**
     * 用于新增和修改时查询对象
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/detail")
    public String getAgreementPolicy(Model model, Integer id,String operateType) {
        AgreementPolicyDto agreementPolicyDto = null;
        if (id != null && !"".equals(id)) {
            agreementPolicyDto = agreementPolicyService.getById(id);
            if(agreementPolicyDto.getPartnerScope() != null) {
                PartnerDto partnerDto = partnerService.findById(agreementPolicyDto.getPartnerScope().longValue());
                if (partnerDto != null) {
                    agreementPolicyDto.setPartnerName(partnerDto.getName());
                }

            }
            if ("1".equals(agreementPolicyDto.getType()) && agreementPolicyDto.getOriginPolicyId() != null) {
                agreementPolicyDto.setOriginPolicyName(agreementPolicyService.getById(agreementPolicyDto.getOriginPolicyId()).getName());
            }
            //创建人
            UserDto creator = userService.getUser(Long.valueOf(agreementPolicyDto.getCreator()));
            //最后修改人
            UserDto lastModifier = userService.getUser(Long.valueOf(agreementPolicyDto.getLastMender()));
            model.addAttribute("creator", creator);
            model.addAttribute("lastModifier", lastModifier);

        }
        if("3".equals(operateType)) {//复制
            //复制 如果页面带出原始协议值  如果为空说明为原始协议 则带入自己
            if(agreementPolicyDto.getOriginPolicyId() == null) {
                agreementPolicyDto.setOriginPolicyId(agreementPolicyDto.getId());
            }
            agreementPolicyDto.setId(null);
            agreementPolicyDto.setCreator(null);
            agreementPolicyDto.setCreateDate(null);
            agreementPolicyDto.setLastMender(null);
            agreementPolicyDto.setLastUpdateDate(null);
        }
        model.addAttribute("agreementPolicyDto", agreementPolicyDto);
        model.addAttribute("filterTypeEnums", FilterTypeEnum.values());

        List<HubsConnectDto> hubsConnectList = hubsConnectService.findListByTmcId(super.getTmcId(), Integer.valueOf(RegionType.DOMESTIC.getType()));
        //根据hubsCode分组
        Map<String, List<HubsConnectDto>> connectMap = new HashMap<>(16);
        for (HubsConnectDto dto : hubsConnectList) {
            if (connectMap.containsKey(dto.getHubsCode())) {
                connectMap.get(dto.getHubsCode()).add(dto);
            } else {
                List<HubsConnectDto> list = new ArrayList<>();
                list.add(dto);
                connectMap.put(dto.getHubsCode(), list);
            }
        }
        model.addAttribute("operateType",operateType);
        model.addAttribute("policyTypeMap", PolicyType.getMaps());
        model.addAttribute("connectMap", connectMap);

        return "flight/agreement/detail.base";
    }


    @RequestMapping(value = "/checkWhitelist")
    @ResponseBody
    public ResponseInfo checkWhitelist(String code){

        AgreementWhitelistDto whitelistDto = agreementPolicyService.findWhitelistByPolicyCode(code);
        if (whitelistDto != null) {
            return new ResponseInfo(SUCCESS, "存在白名单");
        } else {
            return new ResponseInfo(FAILED, "未找到与该协议关联的白名单，是否自动创建？");
        }
    }

    @RequestMapping(value = "/createWhitelist")
    @ResponseBody
    public ResponseInfo createWhitelist(String code){
        /*try {
            List<AgreementPolicyDto> policys = agreementPolicyService.findOriginPolicyByCode(code);
        }catch (Exception e) {
            return new ResponseInfo(FAILED, "创建失败");
        }*/

        AgreementWhitelistDto dto = new AgreementWhitelistDto();
        dto.setCustomerAgreementCode(code);
        dto.setCreator(super.getCurrentUser().getId().intValue());
        dto.setMaxOverbookNum(0);
        dto.setRestOverbookNum(0);
        dto.setAutoRest(false);


        boolean b = whitelistService.addWhitelist(dto);
        AgreementWhitelistDto whitelistByPolicyCode = agreementPolicyService.findWhitelistByPolicyCode(code);
        if (b) {
            return new ResponseInfo(SUCCESS, whitelistByPolicyCode);
        } else {
            return new ResponseInfo(FAILED, "创建失败");
        }
    }


    /**
     * 保存
     *
     * @param agreementPolicyDto
     * @return
     */
    @ResponseBody
    @RequestMapping("/save")
    public ResponseInfo save(AgreementPolicyDto agreementPolicyDto) {
        try {
            //添加操作判断逻辑

            /*// 1原始协议大客户编码不能重复
            if (agreementPolicyDto.getType() != null && "0".equals(agreementPolicyDto.getType())) {
                AgreementPolicyDto originPolicyByCode = agreementPolicyService.findOriginPolicyByCode(agreementPolicyDto.getCustomerAgreementCode());

                if (originPolicyByCode != null) {
                    if(!"2".equals(agreementPolicyDto.getOperateType())) {
                        return new ResponseInfo(FAILED, "保存失败，已存在相同的原始协议大客户编码");
                    }
                    if("2".equals(agreementPolicyDto.getOperateType()) && !agreementPolicyDto.getId().equals(originPolicyByCode.getId())) {
                        return new ResponseInfo(FAILED, "保存失败，已存在相同的原始协议大客户编码");
                    }
                }
            }*/
            //TODO



            if (agreementPolicyDto.getCheckPrice() == null) {
                agreementPolicyDto.setCheckPrice(false);
            }
            if (agreementPolicyDto.getPayAfterSeat() == null) {
                agreementPolicyDto.setPayAfterSeat(false);
            }
            if (agreementPolicyDto.getAutoIssue() == null) {
                agreementPolicyDto.setAutoIssue(false);
            }
            if(agreementPolicyDto.getWhiteListControl() == null) {
                agreementPolicyDto.setWhiteListControl(false);
            }

            if("3".equals(agreementPolicyDto.getOperateType())) {
                List<AgreementCabinDto> cabins = agreementPolicyService.getAgreementCabinsByPolicyId(agreementPolicyDto.getOriginPolicyId());
                agreementPolicyDto.setAgreementCabinDtos(cabins);

            }

            //最后修改人
            agreementPolicyDto.setLastMender(Integer.parseInt(super.getCurrentUser().getId().toString()));
            if (agreementPolicyDto.getId() == null) {
                //创建人
                agreementPolicyDto.setCreator(Integer.parseInt(super.getCurrentUser().getId().toString()));
                if("1".equals(agreementPolicyDto.getOperateType())) {
                    agreementPolicyDto.setType("0");
                }
            }
            if(agreementPolicyDto.getType() != null && "0".equals(agreementPolicyDto.getType())) {
                //原始协议无 原始协议id
                agreementPolicyDto.setOriginPolicyId(null);
                if("2".equals(agreementPolicyDto.getOperateType())) {
                    //修改原始协议 添加修改人信息 服务层 修改白名单相关数据
                    agreementPolicyDto.setUpdator(super.getCurrentUser().getId());
                }
            } else if(agreementPolicyDto.getType() != null && "1".equals(agreementPolicyDto.getType())){
                //共享协议为空字段
                agreementPolicyDto.setPartnerScope(null);
                agreementPolicyDto.setEffectiveSaleDay(null);
                agreementPolicyDto.setShowName(null);
            }

            AgreementPolicyDto resultDto = agreementPolicyService.save(agreementPolicyDto);
            return new ResponseInfo(SUCCESS, "保存成功", resultDto);
        } catch (NumberFormatException e) {
            return new ResponseInfo(FAILED, "保存失败，请刷新重试");
        }
    }

    @RequestMapping("/agreementCabinDetail")
    public String getAgreementCabin(Model model, Integer id, Integer policyId) {
        String selectedCabin = "";
        //查询适用舱位
        if (id != null) {
            AgreementCabinDto agreementCabinDto = agreementPolicyService.getAgreementCabinById(id);
            selectedCabin = agreementCabinDto.getCabinCodes();
            model.addAttribute("agreementCabinDto", agreementCabinDto);
        }
        AgreementPolicyDto agreementPolicyDto = agreementPolicyService.getById(policyId);
        String carrierCode = agreementPolicyDto.getCarrierCodes().split("/")[0];
        //过滤掉其他配置已经选用的舱位,修改的时候还要留下自己选用的
        List<CabinDto> list = cabinService.findCabinsByCarrierCode(carrierCode);
        /*for (AgreementCabinDto agreementCabinDto : agreementPolicyDto.getAgreementCabinDtos()) {
            for (int i = 0; i < list.size(); i++) {
                if (!selectedCabin.contains(list.get(i).getCode() + "/")
                        && agreementCabinDto.getCabinCodes().contains(list.get(i).getCode() + "/")) {
                    list.remove(i--);
                }
            }
        }*/
        model.addAttribute("usableCabinList", list);
        model.addAttribute("policyId", policyId);
        return "flight/agreement/cabin.single";
    }

    @ResponseBody
    @RequestMapping("/saveAgreementCabin")
    public ResponseInfo saveRouteRuleItem(AgreementCabinDto agreementCabinDto) {
        String[] codes = super.getRequest().getParameterValues("cabinCodes");
        String join = StringUtils.join(codes, "/");
        agreementCabinDto.setCabinCodes(join + "/");
        boolean result = agreementPolicyService.saveAgreementCabin(agreementCabinDto);
        if (result) {
            return new ResponseInfo(SUCCESS, "保存成功");
        } else {
            return new ResponseInfo(FAILED, "保存失败，请刷新重试");
        }
    }

    @ResponseBody
    @RequestMapping("/deleteAgreementCabin")
    public ResponseInfo deleteRouteRuleItem(Integer id) {
        boolean result = agreementPolicyService.deleteAgreementCabin(id);
        if (result) {
            return new ResponseInfo(SUCCESS, "删除成功");
        } else {
            return new ResponseInfo(FAILED, "删除失败，请刷新重试");
        }
    }


    //------------------------共享协议的   企业相关----------------------------//

    @RequestMapping(value = "/shareScope")
    public String shareScopeList(Integer id, Model model){

        AgreementPolicyDto policyDto = agreementPolicyService.getById(id);
        if(policyDto.getPartnerScope() != null) {
            PartnerDto partnerDto = partnerService.findById(policyDto.getPartnerScope().longValue());
            if (partnerDto != null) {
                policyDto.setPartnerName(partnerDto.getName());
            }
        }

        model.addAttribute("policyDto",policyDto);
        return "flight/agreement/shareScope.base";
    }

    @RequestMapping(value = "/shareScopelist")
    public String shareScopelist(Model model, Integer pageIndex, Integer pageSize,String partnerId,String policyId){

        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        QueryDto<Object> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setField1(policyId);
        queryDto.setField2(partnerId);
        Page<AgreementShareScopeDto> page = agreementPolicyService.queryShareScopeListWithPage(queryDto);

        for (AgreementShareScopeDto scopeDto : page.getList()) {
            PartnerDto partnerDto = partnerService.findById(scopeDto.getPartnerId().longValue());
            if (partnerDto != null) {
                scopeDto.setPartnerName(partnerDto.getName());
            }
        }

        model.addAttribute("pageList", page);
        return "flight/agreement/shareScopeList.jsp";
    }

    @ResponseBody
    @RequestMapping("/deleteScope")
    public ResponseInfo deleteScope(Integer id) {

        // 20190415 共享协议使用企业可以随便删除。。。。

       /* AgreementShareScopeDto scope = agreementPolicyService.findScopeById(id);

        AgreementPolicyDto policyDto = agreementPolicyService.getById(scope.getAgreementPolicyId());

        if(policyDto.getWhiteListControl() != null && policyDto.getWhiteListControl()) {
            AgreementWhitelistDto whitelist = agreementPolicyService.findWhitelistByPolicyCode(policyDto.getCustomerAgreementCode());
            if(whitelist != null) {
                return new ResponseInfo(CONFIRM, "存在白名单关联，无法删除!");
            }
        }*/

        boolean result = agreementPolicyService.deleteScopeById(id);
        if (result) {
            return new ResponseInfo(SUCCESS, "删除成功!");
        } else {
            return new ResponseInfo(FAILED, "删除失败!");
        }
    }

    @RequestMapping(value = "/scopeDetail")
    public String getAgreementScopeDetail(Model model, String id,String policyId) {

        AgreementShareScopeDto scope = null;
        if(StringUtils.isNotBlank(id)) {
            scope = agreementPolicyService.findScopeById(Integer.parseInt(id));
            scope.setPartnerName(partnerService.findById(scope.getPartnerId().longValue()).getName());
        }

        model.addAttribute("policyId",policyId);
        model.addAttribute("scope",scope);
        return "flight/agreement/scopeDetail.jsp";
    }

    @ResponseBody
    @RequestMapping("/saveScope")
    public ResponseInfo saveScope(AgreementShareScopeDto dto) {

        if(dto.getId() == null) {
            dto.setCreator(super.getCurrentUser().getId().intValue());
            //导入增加重复判断
            QueryDto<Object> queryDto = new QueryDto<>(1, Integer.MAX_VALUE);
            queryDto.setField1(dto.getAgreementPolicyId()+"");
            queryDto.setField2(dto.getPartnerId()+"");
            Page<AgreementShareScopeDto> page = agreementPolicyService.queryShareScopeListWithPage(queryDto);
            if(CollectionUtils.isNotEmpty(page.getList())) {
                return new ResponseInfo(FAILED, "添加失败，该企业已经共享此该大客户协议",null);
            }
        } else {
            dto.setUpdator(super.getCurrentUser().getId().intValue());
        }

        boolean result = agreementPolicyService.saveScope(dto);
        if (result) {
            return new ResponseInfo(SUCCESS, "保存成功");
        } else {
            return new ResponseInfo(FAILED, "保存失败，请刷新重试",null);
        }
    }

    @ResponseBody
    @RequestMapping(value = "findWhitelistByCode")
    public AgreementWhitelistDto findWhitelistByPolicyCode(String code){
        AgreementWhitelistDto dto = agreementPolicyService.findWhitelistByPolicyCode(code);
        return dto;
    }

}
