package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.flight.Constants;
import com.tem.flight.api.HubsConnectService;
import com.tem.flight.api.RouteRuleService;
import com.tem.flight.dto.hubs.HubsConnectDto;
import com.tem.flight.dto.routeRule.RouteRuleDto;
import com.tem.flight.dto.routeRule.RouteRuleItemDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.PartnerBpService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("flight/routeRule")
public class RouteRuleController extends BaseController {
    public static final String FAILED = "-1";
    public static final String SUCCESS = "0";

    @Autowired
    private RouteRuleService routeRuleService;

    @Autowired
    private DictService dictService;

    @Autowired
    private UserService userService;

    @Autowired
    private HubsConnectService hubsConnectService;

    @Autowired
    private PartnerService partnerService;
    @Autowired
    private PartnerBpService partnerBpService;

    @RequestMapping(value = "")
    public String index(Model model) {
        return "flight/routeRule/index.base";
    }

    // 分页查询
    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex, Integer pageSize, Integer ruleGrade, Long pid,Integer regionType) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        QueryDto<Object> queryDto = new QueryDto<Object>(pageIndex, pageSize);

        if (ruleGrade != null && !"".equals(ruleGrade)) {
            queryDto.setField1(ruleGrade.toString());
        }
        if (pid != null && !"".equals(pid)) {
            queryDto.setField2(pid.toString());
        }
        if (regionType != null) {
            queryDto.setField4(regionType);
        }

        Page<RouteRuleDto> pageList = routeRuleService.queryListWithPage(queryDto);

        //将每个规则的使用范围的企业名称查询出来
        List<RouteRuleDto> list = pageList.getList();
        for (RouteRuleDto dto : list) {
            Integer scope = dto.getScope();
            if (scope != null) {
                PartnerDto partnerDto = partnerService.findById(scope.longValue());
                if (partnerDto != null) {
                    dto.setScopeName(partnerDto.getName());
                }
            }
        }
        model.addAttribute("pageList", pageList);
        return "flight/routeRule/routeRuleTable.jsp";
    }

    @ModelAttribute("gradeMap")
    public Map<Integer, String> getGradeMap() {
        //ruleGrade  规则级别
        List<DictCodeDto> gradeList = dictService.getCodes(Constants.OTA_ROUTE_RULE_GRADE);
        Map<Integer, String> map = new HashMap<>(16);
        for (DictCodeDto dictCodeDto : gradeList) {
            //过滤掉平台级别，以后只有tmc/企业，暂时页面过滤，后续再删除相关逻辑和数据
            if ("1".equals(dictCodeDto.getItemCode())) {
                continue;
            }
            map.put(Integer.valueOf(dictCodeDto.getItemCode()), dictCodeDto.getItemTxt());
        }
        return map;
    }


    // 批量删除
    @ResponseBody
    @RequestMapping("/delete")
    public ResponseInfo delete(String id) {
        String[] split = id.split(",");
        Integer[] ids = new Integer[split.length];
        for (int i = 0; i < split.length; i++) {
            ids[i] = Integer.parseInt(split[i]);
        }
        boolean rs = routeRuleService.batchDelete(ids);
        if (rs) {
            return new ResponseInfo(SUCCESS, "删除成功!");
        } else {
            return new ResponseInfo(FAILED, "删除失败!");
        }
    }

    // 用于新增和修改时查询对象
    @RequestMapping(value = "/getRouteRule")
    public String getRouteRule(Model model, Integer id) {
        RouteRuleDto routeRuleDto = null;
        if (id != null && !"".equals(id)) {
            routeRuleDto = routeRuleService.findById(id);
            Integer scope = routeRuleDto.getScope();
            if (scope != null) {
                PartnerDto partnerDto = partnerService.findById(scope.longValue());
                if (partnerDto != null) {
                    routeRuleDto.setScopeName(partnerDto.getName());
                }
            }
            //创建人
            UserDto creator = userService.getUser(Long.valueOf(routeRuleDto.getCreator()));
            //最后修改人
            UserDto lastModifier = userService.getUser(Long.valueOf(routeRuleDto.getLastMender()));
            model.addAttribute("creator", creator);
            model.addAttribute("lastModifier", lastModifier);
        }
        model.addAttribute("routeRuleDto", routeRuleDto);
        return "flight/routeRule/routeRuleModel.base";
    }

    @ResponseBody
    @RequestMapping("/save")
    public ResponseInfo save(RouteRuleDto routeRuleDto) {
        try {
            //调用方法判断是否可以新增或修改
            boolean validate = routeRuleService.checkUniqueness(routeRuleDto);
            if (!validate) {
                return new ResponseInfo(FAILED, "已存在tmc/企业级别的对应规则,无法重复添加,请检查！");
            }
            //最后修改人
            routeRuleDto.setLastMender(Integer.parseInt(super.getCurrentUser().getId().toString()));
            //最后修改时间
            routeRuleDto.setLastUpdateDate(new Date());
            if (routeRuleDto.getId() == null) {
                //创建人
                routeRuleDto.setCreator(Integer.parseInt(super.getCurrentUser().getId().toString()));
                //创建时间
                routeRuleDto.setCreateDate(new Date());
            }

            RouteRuleDto resultDto = routeRuleService.save(routeRuleDto);
            return new ResponseInfo(SUCCESS, "保存成功", resultDto);
        } catch (NumberFormatException e) {
            return new ResponseInfo(FAILED, "保存失败，请刷新重试");
        }
    }

    @RequestMapping("/getRouteRuleItem")
    public String getRouteRuleItem(Model model, Integer id, Integer routeRuleId,Integer regionType) {
        //查询配置项
        RouteRuleItemDto itemDto = routeRuleService.getRouteRuleItemById(id);
        //根据配置id查询该tmc下的可用连接账号
        RouteRuleDto routeRuleDto = routeRuleService.findById(routeRuleId);
        if (routeRuleDto != null) {
            //tmc级别
            Long tmcId;
            if (2 == routeRuleDto.getGrade()) {
                tmcId = routeRuleDto.getScope().longValue();
            } else {
                //根据pid查询tmcId
                tmcId = partnerBpService.findTmcIdByPId(routeRuleDto.getScope().longValue());
            }

            List<HubsConnectDto> hubsConnectList = hubsConnectService.findListByTmcId(tmcId,regionType);
            //根据hubsCode分组
            Map<String, List<HubsConnectDto>> connectMap = new HashMap<>(16);
            for (HubsConnectDto dto : hubsConnectList) {
                if (connectMap.containsKey(dto.getHubsCode())) {
                    connectMap.get(dto.getHubsCode()).add(dto);
                } else {
                    List<HubsConnectDto> list = new ArrayList<>();
                    list.add(dto);
                    connectMap.put(dto.getHubsCode(), list);
                }
            }
            model.addAttribute("connectMap", connectMap);
        }

        model.addAttribute("itemDto", itemDto);
        model.addAttribute("routeRuleId", routeRuleId);
        return "flight/routeRule/item.single";
    }

    @ResponseBody
    @RequestMapping("/saveRouteRuleItem")
    public boolean saveRouteRuleItem(RouteRuleItemDto itemDto) {
        return routeRuleService.saveRouteRuleItem(itemDto);
    }

    @ResponseBody
    @RequestMapping("/deleteRouteRuleItem")
    public boolean deleteRouteRuleItem(Integer id) {
        return routeRuleService.deleteRouteRuleItem(id);
    }

}
