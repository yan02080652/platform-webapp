package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.tem.flight.Constants;
import com.tem.flight.api.CabinService;
import com.tem.flight.api.FreightService;
import com.tem.flight.api.RefundChangeRuleService;
import com.tem.flight.dto.cabin.CabinDto;
import com.tem.flight.dto.freight.FreightCondition;
import com.tem.flight.dto.freight.FreightDto;
import com.tem.flight.dto.freight.RefundChangeRuleDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.dto.DictCodeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yuanjianxin
 * @date 2017/8/7 9:34
 */
@Controller
@RequestMapping("/flight/freight")
public class FreightController extends BaseController {

    @Autowired
    private RefundChangeRuleService refundChangeRuleService;
    @Autowired
    private FreightService freightService;
    @Autowired
    private DictService dictService;
    @Autowired
    private CabinService cabinService;

    @RequestMapping(value = "")
    public String index(Model model) {
        return "flight/freight/index.base";
    }

    // 分页查询
    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex, Integer pageSize, FreightCondition condition) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        //查询对象
        QueryDto<FreightCondition> queryDto = new QueryDto<FreightCondition>(pageIndex, pageSize);
        queryDto.setCondition(condition);
        Page<FreightDto> pageList = freightService.queryListWithPage(queryDto);

        model.addAttribute("pageList", pageList);

        return "flight/freight/list.jsp";
    }

    @RequestMapping("/detail")
    public String detail(Model model, Integer id) {
        List<DictCodeDto> cabinGrade = dictService.getCodes(Constants.OTAF_CABIN_GRADE);
        FreightDto freightDto = null;
        if (id != null) {
            freightDto = freightService.getById(id);
        }

        model.addAttribute("freightDto", freightDto);
        model.addAttribute("cabinGrade", cabinGrade);
        model.addAttribute("weekMap", getWeekMap());

        return "flight/freight/detail.base";
    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public String delete(Model model, @PathVariable("id") Integer id) {
        boolean rs = freightService.deleteById(id);
        return rs ? Constants.SUCCESS : Constants.FAILED;
    }

    @RequestMapping("/save")
    @ResponseBody
    public String save(Model model, FreightDto freightDto) {
        if (freightService.isExistFreight(freightDto)) {
            return Constants.EXIST;
        }

        String[] week = super.getRequest().getParameterValues("week");
        if (week.length > 0) {
            StringBuilder st = new StringBuilder();
            for (String day : week) {
                st.append(day).append("/");
            }
            freightDto.setConformityDate(st.substring(0, st.length() - 1));
        }

        boolean rs = freightService.save(freightDto);
        return rs ? Constants.SUCCESS : Constants.FAILED;
    }

    @RequestMapping("/getCabinGrade")
    @ResponseBody
    public String getCabinGrade(String carrierCode, String cabinCode) {
        CabinDto cabinDto = cabinService.findByCabinCodeAndCarrierCode(cabinCode, carrierCode);
        if (cabinDto != null) {
            return cabinDto.getGrade();
        }
        return null;
    }

    @RequestMapping("/refundChangeRule")
    @ResponseBody
    public RefundChangeRuleDto refundChangeRule(String carrierCode, String cabinCode) {
        RefundChangeRuleDto ruleDto = refundChangeRuleService.getRefundChangeRuleByCondition(new FreightCondition(carrierCode, cabinCode));
        return ruleDto;
    }

    private Map<Integer, String> getWeekMap() {
        Map<Integer, String> weekMap = new LinkedHashMap<>();
        weekMap.put(1, "星期一");
        weekMap.put(2, "星期二");
        weekMap.put(3, "星期三");
        weekMap.put(4, "星期四");
        weekMap.put(5, "星期五");
        weekMap.put(6, "星期六");
        weekMap.put(7, "星期日");
        return weekMap;
    }

}
