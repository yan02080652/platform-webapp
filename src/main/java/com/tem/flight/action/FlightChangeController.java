package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.DateUtils;
import com.tem.flight.api.AirportService;
import com.tem.flight.api.CarrierService;
import com.tem.flight.api.CityAirportService;
import com.tem.flight.api.FlightChangeService;
import com.tem.flight.dto.airport.AirportDto;
import com.tem.flight.dto.airport.CityAirportDto;
import com.tem.flight.dto.carrier.CarrierDto;
import com.tem.flight.dto.enums.FlightCacelResonEnum;
import com.tem.flight.dto.enums.FlightStateEnum;
import com.tem.flight.dto.enums.RegionType;
import com.tem.flight.dto.flight.FlightChangeDto;
import com.tem.flight.dto.flight.FlightChangeQueryDto;
import com.tem.flight.form.FlightChangeForm;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DistrictService;
import com.tem.platform.api.dto.DistrictDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 航班动态维护
 * Created by pyy on 2017/5/8.
 */
@Controller
@RequestMapping("flight/change")
public class FlightChangeController extends BaseController {

    @Autowired
    private FlightChangeService flightChangeService;

    @Autowired
    private CarrierService carrierService;

    @Autowired
    private AirportService airportService;

    @Autowired
    private CityAirportService cityAirportService;

    @Autowired
    private DistrictService districtService;

    @RequestMapping(value = "")
    public String index(Model model) {
        String flightNo = super.getStringValue("flightNo_con");
        String fromDate_con = super.getStringValue("fromDate_con");
        Integer pageIndex = super.getIntegerValue("pageIndex");
        Integer pageSize = super.getIntegerValue("pageSize");
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }
        QueryDto<FlightChangeQueryDto> queryDto = new QueryDto<>(pageIndex,pageSize);
        FlightChangeQueryDto flightChangeQueryDto = new FlightChangeQueryDto();
        flightChangeQueryDto.setFlightNo(flightNo);
        if(StringUtils.isEmpty(fromDate_con)){//默认今天
            flightChangeQueryDto.setPlanFromDate(new Date());
        }else if(!"NULL".equals(fromDate_con)){
            try {
                Date fromDate = DateUtils.parse(fromDate_con);
                flightChangeQueryDto.setPlanFromDate(fromDate);
            } catch (Exception ex) {
                throw new RuntimeException("查询时间格式不正确");
            }
        }
        queryDto.setCondition(flightChangeQueryDto);
        queryDto.setSidx("flightDepTimePlanDate");
        queryDto.setSord("desc");
        Page<FlightChangeDto> list = flightChangeService.queryFilghtChange(queryDto);
        model.addAttribute("pageList", list);
        model.addAttribute("flightStates", FlightStateEnum.values());
        model.addAttribute("flightNo_con",flightNo);
        model.addAttribute("fromDate_con",fromDate_con);
        return "flight/flightChange/list.base";
    }

    @RequestMapping(value="detail.html")
    public String detail(Model model){
        Long id = super.getLongValue("id");
        if(id != null){
            FlightChangeDto flightChangeDto = flightChangeService.getById(id);
            model.addAttribute("fc", flightChangeDto);
        }

        model.addAttribute("flightStates", FlightStateEnum.values());
        model.addAttribute("cancelResons", FlightCacelResonEnum.values());
        List<DistrictDto> cityList = districtService.findCity();
        model.addAttribute("cityList",cityList);
        return "flight/flightChange/detail.base";
    }

    /**
     * 获取航司:C/机场:F/城市:T 传入的机场三字码
     * @return
     */
    @RequestMapping(value="getName")
    @ResponseBody
    public Object getName(){
        String type = super.getStringValue("type");
        String code = super.getStringValue("code");
        if(StringUtils.isEmpty(code)){
            return null;
        }
        if("C".equals(type)){//航司
            CarrierDto carrierDto = carrierService.findByCode(code);
            return  carrierDto;
        }else if ("F".equals(type)){//机场
            AirportDto airportDto = airportService.findAirportByCode(code, RegionType.DOMESTIC.getType());
            return  airportDto;
        }else if ("T".equals(type)){
            List<CityAirportDto> airportCodes = cityAirportService.findByAirportCode(code);
            if(airportCodes.size() > 0){
                return  airportCodes.get(0);
            }
        }
        return null;
    }

    @RequestMapping(value="/save")
    public String save(Model model,FlightChangeForm flightChangeForm){
        Date now = new Date();
        int code = -1;
        String msg = null;
        FlightChangeDto flightChangeDto = null;
        if(flightChangeForm.getId() == null){//新增
            flightChangeDto = TransformUtils.transform(flightChangeForm, FlightChangeDto.class);
            flightChangeDto.setFlightCreateTime(now);
            flightChangeDto.setFlightCategory("0");
            int rs = flightChangeService.addFlightChange(flightChangeDto);
            if(rs == 0){
                msg = "新增失败";
            }else{
                code = 1;
                msg = "新增成功";
            }
        }else{//更新
            flightChangeDto = flightChangeService.getById(flightChangeForm.getId());
            if(flightChangeDto == null){
                code = -1;
                model.addAttribute("message","更新失败，失败原因：不存在的航班动态");
            }else{
                TransformUtils.transform(flightChangeForm,flightChangeDto);
                flightChangeDto.setFlightUpdateTime(now);
                int rs = flightChangeService.updateFlightChange(flightChangeDto);
                if(rs == 0){
                    msg = "更新失败";
                }else{
                    code = 1;
                    msg = "更新成功";
                }
            }
        }
        model.addAttribute("code",code);
        //更新成功后发送消息
        if(code > 0 && new Integer(1).equals(flightChangeForm.getSengMsg())){
            int sendUserCount = this.sendMsg(flightChangeDto.getId());
            model.addAttribute("sendUserCount",sendUserCount);

            msg += "，并已发送给" + sendUserCount + "用户";
        }
        model.addAttribute("message",msg);
        return "redirect:/flight/change";
    }

    @RequestMapping(value = "delete")
    public String delete(Model model){
        Long id = super.getLongValue("id");
        int rs = -1;
        if(id != null){
            rs = flightChangeService.deleteById(id);
        }
        model.addAttribute("code",rs);
        String msg = rs < 1?"删除失败":"删除成功";
        super.addResult(model,rs,msg);
        return "redirect:/flight/change";
    }

    /**
     * 发送航班通知
     * @param flightChangeId
     * @return
     */
    @RequestMapping(value="send")
    @ResponseBody
    public Map<String,Object> send(@RequestParam(required = true)Long flightChangeId){
        Map<String, Object> map = new HashMap<>();
        int rs = this.sendMsg(flightChangeId);
        map.put("count", rs);
        return map;
    };

    private int sendMsg(Long flightChangeId){
        return flightChangeService.sendMsg(flightChangeId);
    }

    /**
     * 查询相同航班动态，唯一性校验
     * @return
     */
    @RequestMapping(value="checkSame")
    @ResponseBody
    public String checkSame(){
        Long id = super.getLongValue("id");
        String flightNo = super.getStringValue("flightNo");
        String fromDate = super.getStringValue("fromDate");
        Map<String, Object> result = new HashMap<>();
        boolean flag = false;
        if(StringUtils.isNotEmpty(flightNo) && StringUtils.isNotEmpty(fromDate)){
            Date fromDate2 = DateUtils.parse(fromDate,"yyyy-MM-dd HH:mm");
            List<FlightChangeDto> flightChangeDtos = flightChangeService.getByNoAndDate(flightNo, fromDate2);
            for (int i = 0; i < flightChangeDtos.size(); i++) {
                FlightChangeDto flightChangeDto = flightChangeDtos.get(i);
                if(id == null || !id.equals(flightChangeDto.getId())){
                    flag = true;
                    break;
                }
            }
        }
        return flag?"true":"false";
    };
}
