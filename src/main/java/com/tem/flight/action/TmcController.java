package com.tem.flight.action;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Config;
import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.flight.Constants;
import com.tem.flight.FbpRtnCodeConst;
import com.tem.flight.api.AircraftService;
import com.tem.flight.api.AirportService;
import com.tem.flight.api.CabinService;
import com.tem.flight.api.CarrierService;
import com.tem.flight.api.FreightService;
import com.tem.flight.api.hubs.FlightHubsService;
import com.tem.flight.api.order.FlightOrderService;
import com.tem.flight.dto.aircraft.AircraftDto;
import com.tem.flight.dto.airport.AirportDto;
import com.tem.flight.dto.cabin.CabinDto;
import com.tem.flight.dto.cabin.FullPriceCabinDto;
import com.tem.flight.dto.carrier.CarrierDto;
import com.tem.flight.dto.enums.OrderTypeEnum;
import com.tem.flight.dto.enums.RegionType;
import com.tem.flight.dto.freight.FreightCondition;
import com.tem.flight.dto.freight.RefundChangeDetailDto;
import com.tem.flight.dto.order.ChangeOrderForm;
import com.tem.flight.dto.order.FlightOrderCondition;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.flight.dto.order.FlightOrderPriceDto;
import com.tem.flight.dto.order.OrderCountDto;
import com.tem.flight.dto.order.OrderFlightDetailDto;
import com.tem.flight.dto.order.OrderOperationLogDto;
import com.tem.flight.dto.order.PassengerDto;
import com.tem.flight.dto.order.enums.OrderOperationStatus;
import com.tem.flight.dto.order.enums.OrderOperationType;
import com.tem.flight.dto.order.enums.OrderShowStatus;
import com.tem.flight.dto.order.enums.OrderStatus;
import com.tem.flight.dto.order.enums.PassengerStatus;
import com.tem.flight.dto.order.enums.ProcessTagType;
import com.tem.flight.dto.order.enums.TicketStatus;
import com.tem.flight.form.FlightForm;
import com.tem.flight.form.TicketFrom;
import com.tem.payment.api.BpAccTradeRecordService;
import com.tem.payment.dto.TradeRecordInfo;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.UserBaseInfo;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.api.ServerChargeService;
import com.tem.pss.api.task.OrderTaskService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.refund.RefundOrderDto;
import com.tem.pss.dto.task.OrderTaskCondition;
import com.tem.pss.dto.task.OrderTaskDto;
import com.tem.pss.dto.task.TaskParamDto;
import com.tem.pss.dto.task.TaskStatus;
import com.tem.pss.dto.task.TaskType;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import com.tem.pss.enums.TicketTypeEnum;
import com.tem.sso.api.SSOService;
import com.tem.supplier.api.SettleChannelService;
import com.tem.supplier.dto.SettleChannelDto;
import com.tem.tmc.TaskCommonService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/flight/tmc")
public class TmcController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(TmcController.class);

    public static final String SUCCESS = "1";
    public static final String FAIL = "-1";
    public static final String INVALID_STATUS = "0";

    @Autowired
    private FlightOrderService flightOrderService;
    @Autowired
    private DictService dictService;
    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private IssueChannelService issueChannelService;
    @Autowired
    private FlightHubsService flightHubsService;
    @Autowired
    private UserService userService;
    @Autowired
    private RefundOrderService refundOrderService;
    @Autowired
    private ServerChargeService serverChargeService;
    @Autowired
    private BpAccTradeRecordService bpAccTradeRecordService;
    @Autowired
    private TaskCommonService taskCommonService;
    @Autowired
    private SSOService ssoService;
    @Autowired
    private FreightService freightService;
    @Autowired
    private AirportService airportService;
    @Autowired
    private CarrierService carrierService;
    @Autowired
    private AircraftService aircraftService;
    @Autowired
    private CabinService cabinService;
    @Autowired
    private SettleChannelService settleChannelService;

    @RequestMapping("/index")
    public String index(Model model) {
        return "flight/tmc/index.jsp";
    }

    /**
     * 获取各状态下各个类型的任务数量
     *
     * @param model
     */
    private void getStatusAndTypeCount(Model model, Integer userId) {

        OrderTaskCondition condition = new OrderTaskCondition();
        condition.setOrderBizType(OrderBizType.FLIGHT.name());
        condition.setOperatorId(userId);
        condition.setTmcId(super.getTmcId().toString());

        condition.setTaskStatus(TaskStatus.PROCESSING.toString());
        //处理中任务数量（包含各状态的数量）
        Map<String, Integer> processingMap = orderTaskService.getTaskCountMap(condition);

        condition.setTaskStatus(TaskStatus.ALREADY_PROCESSED.toString());
        //已处理任务数量（包含各状态的数量）
        Map<String, Integer> alreadyMap = orderTaskService.getTaskCountMap(condition);

        //还未领取的任务(包含各状态的数量)
        condition.setTaskStatus(TaskStatus.WAIT_PROCESS.toString());
        condition.setOperatorId(null);
        Map<String, Integer> waitTaskTypeMap = orderTaskService.getTaskCountMap(condition);

        condition.setTaskStatus(TaskStatus.HANG_UP.toString());
        //已处理任务数量（包含各状态的数量）
        Map<String, Integer> hangUpMap = orderTaskService.getTaskCountMap(condition);

        model.addAttribute("processingMap", processingMap);
        model.addAttribute("alreadyMap", alreadyMap);
        model.addAttribute("waitTaskTypeMap", waitTaskTypeMap);
        model.addAttribute("hangUpMap", hangUpMap);

    }


    //全部任务
    @RequestMapping("/allTask")
    public String allTask(Model model, Integer pageIndex, String taskStatus, String taskType, String keyword) {
        if (pageIndex == null) {
            pageIndex = 1;
        }

        if (taskStatus == null) {
            taskStatus = TaskStatus.WAIT_PROCESS.toString();
        }

        QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex, 10);
        OrderTaskCondition condition = new OrderTaskCondition();


        condition.setTaskStatus(taskStatus);
        condition.setTaskType(taskType);
        condition.setOperatorId(null);
        condition.setTmcId(super.getTmcId().toString());
        condition.setOrderBizType(OrderBizType.FLIGHT.name());
        if (StringUtils.isNotBlank(keyword)) {
            condition.setKeyword(keyword);
        }
        queryDto.setCondition(condition);

        Page<OrderTaskDto> pageList = flightOrderService.orderTaskPageListQuery(queryDto);

        this.getStatusAndTypeCount(model, null);

        //舱位map
        Map<String, String> cabinClassMap = getCabinClassMap();
        //出票渠道map
        Map<String, String> issueChannelMap = getIssueChannelMap();

        taskCommonService.getTaskGroupCount(model, super.getTmcId().toString(), super.getCurrentUser().getId().intValue());

        model.addAttribute("pageList", pageList);
        model.addAttribute("cabinClassMap", cabinClassMap);
        model.addAttribute("taskStatus", taskStatus);
        model.addAttribute("issueChannelMap", issueChannelMap);

        return "flight/tmc/allTask.jsp";
    }

    //订单查询页面
    @RequestMapping("/orderList")
    public String orderList(Model model) {
        QueryDto<FlightOrderCondition> queryDto = new QueryDto<FlightOrderCondition>();
        FlightOrderCondition flightOrderCondition = new FlightOrderCondition();
        flightOrderCondition.setTmcId(super.getTmcId());
        queryDto.setCondition(flightOrderCondition);
        List<OrderCountDto> OrderStatuslist = getOrderCountList(queryDto);

        Page<FlightOrderDto> pageList = getPageList(null, null, flightOrderCondition);

        Map<String, String> cabinClassMap = getCabinClassMap();
        //出票渠道map
        Map<String, String> issueChannelMap = getIssueChannelMap();

        model.addAttribute("OrderStatuslist", OrderStatuslist);
        model.addAttribute("pageList", pageList);
        model.addAttribute("cabinClassMap", cabinClassMap);
        model.addAttribute("issueChannelMap", issueChannelMap);

        return "flight/tmc/orderList.jsp";
    }


    //订单查询异步加载的数据
    @RequestMapping("/getOrderTable")
    public String getOrderTable(Model model, Integer pageIndex, Integer pageSize, Long startDate1,
                                Long endDate1, FlightOrderCondition condition) {

        String currentOrderStatus = condition.getOrderStatus() != null ? condition.getOrderStatus()[0] : null;

        if (startDate1 != null) {
            Date start = new Date(startDate1);
            condition.setStartDate(DateUtils.getDayBegin(start));
        }
        if (endDate1 != null) {
            Date end = new Date(endDate1);
            condition.setEndDate(DateUtils.getDayEnd(end));
        }
        if (StringUtils.isBlank(condition.getIssueChannel())) {
            condition.setIssueChannel(null);
        }

        condition.setTmcId(super.getTmcId());

        Page<FlightOrderDto> pageList = getPageList(pageIndex, pageSize, condition);

        Map<String, String> cabinClassMap = getCabinClassMap();
        //出票渠道map
        Map<String, String> issueChannelMap = getIssueChannelMap();


        QueryDto<FlightOrderCondition> queryDto = new QueryDto<FlightOrderCondition>();
        condition.setOrderStatus(null);
        queryDto.setCondition(condition);
        List<OrderCountDto> OrderStatuslist = getOrderCountList(queryDto);

        model.addAttribute("pageList", pageList);
        model.addAttribute("cabinClassMap", cabinClassMap);
        model.addAttribute("OrderStatuslist", OrderStatuslist);
        model.addAttribute("issueChannelMap", issueChannelMap);
        model.addAttribute("currentOrderStatus", currentOrderStatus);

        return "flight/tmc/orderTable.jsp";
    }

    //分页数据
    private Page<FlightOrderDto> getPageList(Integer pageIndex, Integer pageSize, FlightOrderCondition condition) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }
        QueryDto<FlightOrderCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(condition);
        Page<FlightOrderDto> page = flightOrderService.findOrderList(queryDto);
        return page;
    }

    //舱位等级Map
    private Map<String, String> getCabinClassMap() {
        List<DictCodeDto> codes = dictService.getCodes(Constants.OTAF_CABIN_GRADE);
        Map<String, String> cabinClassMap = new HashMap<String, String>();
        for (DictCodeDto codeDto : codes) {
            cabinClassMap.put(codeDto.getItemCode(), codeDto.getItemTxt());
        }
        return cabinClassMap;
    }

    //出票渠道map
    private Map<String, String> getIssueChannelMap() {
        Map<String, String> issueChannelMap = new HashMap<>();
        List<IssueChannelDto> list = issueChannelService.findListByTmcIdAndChannelType(super.getTmcId(), IssueChannelType.FLIGHT.getType());
        for (IssueChannelDto channelDto : list) {
            issueChannelMap.put(channelDto.getCode(), channelDto.getName());
        }
        return issueChannelMap;
    }

    //查询各个状态的订单数量
    private List<OrderCountDto> getOrderCountList(QueryDto<FlightOrderCondition> queryDto) {
        List<OrderCountDto> orderCountList = flightOrderService.findOrderCountList(queryDto);

        List<OrderCountDto> list = new ArrayList<OrderCountDto>();
        OrderShowStatus[] values = OrderShowStatus.values();

        for (OrderShowStatus orderShowStatus : values) {
            OrderCountDto orderCountDto = new OrderCountDto(orderShowStatus, 0);
            for (OrderCountDto countDto : orderCountList) {
                if (orderShowStatus.equals(countDto.getOrderShowStatus())) {
                    orderCountDto.setCount(countDto.getCount());
                    break;
                }
            }

            list.add(orderCountDto);
        }
        return list;
    }


    //我的任务
    @RequestMapping("/getMyTaskTable")
    public String getMyTaskTable(Model model, Integer pageIndex, String taskStatus, String taskType) {

        if (pageIndex == null) {
            pageIndex = 1;
        }

        if (taskStatus == null) {
            taskStatus = TaskStatus.PROCESSING.toString();
        }

        QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex, 10);
        OrderTaskCondition condition = new OrderTaskCondition();

        condition.setTaskStatus(taskStatus);
        condition.setTaskType(taskType);
        condition.setOperatorId(super.getCurrentUser().getId().intValue());
        condition.setOrderBizType(OrderBizType.FLIGHT.name());
        condition.setTmcId(super.getTmcId().toString());
        queryDto.setCondition(condition);


        Page<OrderTaskDto> pageList = flightOrderService.orderTaskPageListQuery(queryDto);

        this.getStatusAndTypeCount(model, super.getCurrentUser().getId().intValue());

        //舱位map
        Map<String, String> cabinClassMap = getCabinClassMap();
        Map<String, String> issueChannelMap = getIssueChannelMap();

        taskCommonService.getTaskGroupCount(model, super.getTmcId().toString(), super.getCurrentUser().getId().intValue());

        model.addAttribute("pageList", pageList);
        model.addAttribute("cabinClassMap", cabinClassMap);
        model.addAttribute("taskStatus", taskStatus);
        model.addAttribute("issueChannelMap", issueChannelMap);

        return "flight/tmc/myTask.jsp";
    }


    //获取订单数据（回填票号和线下出票时弹出model是需要的数据）
    @RequestMapping("/getTicketData")
    public String getTicketData(Model model, String orderId, Integer taskId, String flag) {
        List<IssueChannelDto> channelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
                IssueChannelType.FLIGHT.getType(), LineType.OFF_LINE.getType());

        OrderTaskDto orderTaskDto = orderTaskService.findByTaskId(taskId);
        FlightOrderDto flightOrderDto = flightOrderService.getFlightOrderById(orderTaskDto.getOrderId());
        model.addAttribute("flightOrderDto", flightOrderDto);
        model.addAttribute("channelList", channelList);
        model.addAttribute("taskDto", orderTaskDto);

        return "flight/tmc/ticket.jsp";
    }

    //保存（线下出票和回填票号）
    @ResponseBody
    @RequestMapping("/saveTicket")
    public ResponseInfo saveTicket(@RequestBody TicketFrom formObj) {


        ResponseInfo info = checkTaskStatus(formObj.getTaskId());

        if (!SUCCESS.equals(info.getCode())) {
            return info;
        }
        try {
            FlightOrderDto dto = flightOrderService.getFlightOrderById(formObj.getOrderId());
            if (StringUtils.isNotBlank(formObj.getIssueChannel())) {
                dto.setIssueChannel(formObj.getIssueChannel());
                dto.setHoldSeatsChannel(formObj.getIssueChannel());
            }
            dto.setPnrCode(formObj.getPnr());
            dto.setExternalOrderId(formObj.getExternalOrderId());
            dto.setExternalOrderStatus("ISSUED");

            FlightOrderPriceDto dto2 = dto.getOrderFee();
            //供应商结算价
            dto2.setTotalSettlePrice(formObj.getTotalPrice());
            //重新计算tr价差
            dto2.setTrSpreads(BigDecimalUtils.subtract(dto2.getTotalSalePrice(), dto2.getTotalSettlePrice()));

            dto.setOrderFee(dto2);

            dto.setOrderStatus(OrderStatus.ISSUED);
            dto.setProcessTag(ProcessTagType.NULL);

            OrderTaskDto orderTaskDto = orderTaskService.findByTaskId(formObj.getTaskId());
            orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
            orderTaskDto.setCompleteDate(new Date());
            orderTaskDto.setOperatorId(super.getCurrentUser().getId().intValue());
            orderTaskDto.setOperatorName(super.getCurrentUser().getFullname());

            if (TaskType.WAIT_TICKET.equals(orderTaskDto.getTaskType()) || TaskType.WAIT_URGE.equals(orderTaskDto.getTaskType())) {
                orderTaskDto.setResult("线下出票");
                dto.setOrderShowStatus(OrderShowStatus.ISSUED);
                orderTaskDto.setTicketType(TicketTypeEnum.OFFLINE);
            } else if (TaskType.WAIT_CHANGE.equals(orderTaskDto.getTaskType())) {
                orderTaskDto.setResult("改签成功");
                dto.setOrderShowStatus(OrderShowStatus.CHANGED);
            }

            for (PassengerDto newPassenger : formObj.getPassengerDtos()) {
                for (PassengerDto oldPassenger : dto.getPassengers()) {
                    if (newPassenger.getId().equals(oldPassenger.getId())) {
                        oldPassenger.setTicketStatus(TicketStatus.UNUSED);
                        oldPassenger.setStatus(PassengerStatus.ISSUED);
                        oldPassenger.setTicketNo(newPassenger.getTicketNo());
                    }
                }
            }
            boolean rs = flightOrderService.offLineTicket(dto, orderTaskDto, formObj.isCancelSupplierOrder());
            info.setCode(rs?SUCCESS:FAIL);
        } catch (Exception e) {
            LogUtils.error(logger, "TMC线下出票或回填票号异常，异常信息={}", e);
            info.setCode(FAIL);
        }

        return info;
    }


    //拒单退款
    @ResponseBody
    @RequestMapping("/rejectOrder")
    public ResponseInfo rejectOrder(String orderId, Integer taskId, String userId, String remark) {
        LogUtils.debug(logger, "进入拒单退款,orderId={},taskId={},userId={},remark={}", orderId, taskId, userId, remark);

        ResponseInfo info = checkTaskStatus(taskId);
        if (!SUCCESS.equals(info.getCode())) {
           return info;
        }

        try {
            OrderTaskDto orderTaskDto = orderTaskService.findByTaskId(taskId);
            orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
            orderTaskDto.setCompleteDate(new Date());
            orderTaskDto.setResult("拒单退款");
            orderTaskDto.setOperatorId(super.getCurrentUser().getId().intValue());
            orderTaskDto.setOperatorName(super.getCurrentUser().getFullname());

            boolean rs = flightOrderService.rejectOrder(orderId, remark, orderTaskDto, null);
            LogUtils.debug(logger, "接口返回结果={}", rs);

            info.setCode(rs?SUCCESS:FAIL);
        } catch (Exception e) {
            LogUtils.error(logger, "拒单退款异常，msg={}", e);
            info.setCode(FAIL);
        }

        return info;
    }


    //任务交接
    @ResponseBody
    @RequestMapping("/transfer")
    public boolean transfer(Integer taskId, Integer operatorId, Integer userId, String userName) {
        logger.debug("tmc任务交接，入参:" + taskId + "--" + operatorId + "--" + userId + "--" + userName);
        if (operatorId == null) {
            operatorId = super.getCurrentUser().getId().intValue();
        }
        TaskParamDto dto = new TaskParamDto();
        dto.setTaskId(taskId);
        dto.setOperatorId(operatorId);
        dto.setUserId(userId);
        dto.setUserName(userName);
        dto.setRemork(super.getCurrentUser().getFullname() + "(" + operatorId + ")将任务交接给" + userName + "(" + userId + "),任务id:" + taskId);
        dto.setOrderBizType(OrderBizType.FLIGHT.name());
        dto.setTaskStatus(TaskStatus.PROCESSING);

        boolean rs = orderTaskService.saveTaskAndLog(dto);

        logger.debug("tmc任务交接返回结果=={}", rs);

        return rs;
    }


    //供应拒单重新采票
    @ResponseBody
    @RequestMapping("/reTicket")
    public boolean reTicket(String orderId, Integer taskId) {
        //催单任务完成
        OrderTaskDto taskDto = new OrderTaskDto();
        taskDto.setId(taskId);
        taskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
        taskDto.setCompleteDate(new Date());
        taskDto.setResult("供应拒单重新采票");
        taskDto.setOperatorName(super.getCurrentUser().getFullname());
        taskDto.setOperatorId(super.getCurrentUser().getId().intValue());
        boolean rs = flightOrderService.reTicket(orderId, taskDto);
        return rs;
    }

    //获取订单日志
    @RequestMapping("/getOrderLog")
    public String getOrderLog(Model model, String orderId) {

        List<OrderOperationLogDto> logList = flightOrderService.findOrderLogList(orderId);

        model.addAttribute("logList", logList);

        return "flight/tmc/log.single";
    }

    //支付
    @ResponseBody
    @RequestMapping("/payment")
    public ResponseInfo payment(String orderId, Integer taskId) {
        ResponseInfo info = checkTaskStatus(taskId);
        if (!SUCCESS.equals(info.getCode())) {
            return info;
        }

        TaskParamDto dto = new TaskParamDto();
        dto.setOrderId(orderId);
        dto.setTaskId(taskId);
        dto.setRemork("向供应商支付");
        dto.setOperatorName(super.getCurrentUser().getFullname());
         info = flightOrderService.ccIssueTicket(dto);

        return info;
    }


    //取消订单
    @ResponseBody
    @RequestMapping("/cancelOrder")
    public Map<String, Object> cancelOrder(String orderId,Integer taskId) {
        Map<String, Object> map = new HashMap<>();
        ResponseInfo info = checkTaskStatus(taskId);
        if (!SUCCESS.equals(info.getCode())) {
            map.put("code",FAIL);
            map.put("msg", info.getMsg());
            return map;
        }

        //调用取消订单接口
        return flightOrderService.cancelSupperOrder(orderId, super.getCurrentUser().getFullname());
    }

    //客服强制取消供应商订单，执行清空供应商信息操作
    @ResponseBody
    @RequestMapping("/forceCancel")
    public boolean forceCancel(String orderId) {
        String fullname = super.getCurrentUser().getFullname();
        int userId = super.getCurrentUser().getId().intValue();

        TaskParamDto taskParamDto = new TaskParamDto();
        taskParamDto.setOperatorName(fullname);
        taskParamDto.setRemork("客服" + fullname + "(" + userId + ")强制清空了供应商订单信息");

        boolean rs = flightOrderService.emptySupperData(orderId, taskParamDto, OrderOperationType.FORCE_CANCEL_ORDER);

        return rs;
    }

    //获取订单详情和退票信息
    @RequestMapping("/getOrderDetail")
    public String getOrderDetail(Model model, String refundOrderId, String taskId) {

        RefundOrderDto refundOrderDto = flightOrderService.getRefundOrderById(refundOrderId);

        double serverCharge = 0;//serverChargeService.getServerCharge(refundOrderDto.getPartnerId(), ServerChargeSceneEnum.IN_FLIGHT_REFUND.getValue());

        //出票渠道map
        Map<String, String> issueChannelMap = getIssueChannelMap();

        Map<String, String> cabinClassMap = getCabinClassMap();
        model.addAttribute("cabin", cabinClassMap);
        model.addAttribute(refundOrderDto);
        model.addAttribute("taskId", taskId);
        model.addAttribute("issueChannelMap", issueChannelMap);
        model.addAttribute("serverCharge", serverCharge);

        return "flight/tmc/orderDetail.single";
    }

    //审核通过
    @ResponseBody
    @RequestMapping("/refundAduitPass")
    public boolean refundAduitPass(RefundOrderDto refundOrderDto, Integer taskId) {
        try {
            refundOrderDto.setOperatorName(super.getCurrentUser().getFullname());
            refundOrderDto.setOperatorId(super.getCurrentUser().getId());
            refundOrderDto.setPassed(true);
            refundOrderDto.setAuditEndTime(new Date());

            FlightOrderDto flightOrderDto = flightOrderService.getFlightOrderById(refundOrderDto.getOrderId());
            FlightOrderPriceDto orderFee = flightOrderDto.getOrderFee();

            //可退金额（单）= 销售价+机建费+燃油+升舱+改签
            BigDecimal refundableAmount = BigDecimalUtils.add(orderFee.getSalePrice(), orderFee.getDepartureTax(), orderFee.getFuelTax(), orderFee.getUpgradePrice(), orderFee.getRescheduledPrice());
            refundOrderDto.setRefundableAmount(refundableAmount);

            //扣除金额 （单）= 退票费（单）+ 调整费（单）+ TEM服务费（单）
            BigDecimal decimal = BigDecimalUtils.add(refundOrderDto.getRefundFee(), refundOrderDto.getAdjustFee(), refundOrderDto.getRefundServiceFee());

            //应退金额（单） = 可退金额（单） - 扣除金额 （单）
            refundOrderDto.setActualRefundAmount(BigDecimalUtils.subtract(refundableAmount, decimal));

            //应退总额
            BigDecimal totalAmount = BigDecimalUtils.multiply(refundOrderDto.getActualRefundAmount(), refundOrderDto.getRefundCount());
            refundOrderDto.setRefundTotalAmount(BigDecimalUtils.multiply(totalAmount, -1));

            //供应商退票费（总） = 供应商退票费(单)*退单人数
            BigDecimal totalRefundFee = BigDecimalUtils.multiply(refundOrderDto.getRefundFee(), refundOrderDto.getRefundCount());

            //供应商应退额   = -1*（原订单.供应商结算价/原订单.乘机人个数*退单人数 - 供应商退票费(单)*退单人数）
            BigDecimal supperRefundAmount = BigDecimalUtils.subtract(BigDecimalUtils.multiply(BigDecimalUtils.divide(orderFee.getTotalSettlePrice(),
                    BigDecimal.valueOf(flightOrderDto.getPassengers().size())), refundOrderDto.getRefundCount()), totalRefundFee);
            refundOrderDto.setSupperRefundAmount(BigDecimalUtils.multiply(supperRefundAmount, -1));

            //总调整费
            BigDecimal totalAdjustFee = BigDecimalUtils.multiply(refundOrderDto.getAdjustFee(), refundOrderDto.getRefundCount());

            // 客户票面应退额 = = -1*（正向客户票面销售总价/原订单.乘机人个数*退单人数 - 供应商退票费(总)-调整费(总))
            BigDecimal customerRefundAmount = BigDecimalUtils.subtract(BigDecimalUtils.subtract(
                    BigDecimalUtils.multiply(BigDecimalUtils.divide(orderFee.getTotalSalePrice(),
                            BigDecimal.valueOf(flightOrderDto.getPassengers().size())),
                            refundOrderDto.getRefundCount()), totalRefundFee), totalAdjustFee);
            refundOrderDto.setCustomerRefundAmount(BigDecimalUtils.multiply(customerRefundAmount, -1));

            //服务费
            refundOrderDto.setServicePrice(BigDecimalUtils.multiply(refundOrderDto.getRefundServiceFee(), refundOrderDto.getRefundCount()));

            //tr价差
            refundOrderDto.setTrSpreads(BigDecimalUtils.subtract(refundOrderDto.getCustomerRefundAmount(), refundOrderDto.getSupperRefundAmount()));

            boolean rs = flightOrderService.refundorderAduit(refundOrderDto, taskId);
            if (rs) {
                try {
                    //修改退票人记录中的分摊金额
                    BigDecimal amount1 = BigDecimalUtils.multiply(refundOrderDto.getActualRefundAmount(), -1);
                    refundOrderService.updateRefundPassengrByRefundOrderId(refundOrderDto.getId(), amount1);
                } catch (Exception e) {
                    LogUtils.error(logger, "修改退票人记录中的分摊金额时发生异常，退票单id=={}，异常信息=={}", refundOrderDto.getId(), e);
                }
            }
            return rs;
        } catch (Exception e) {
            LogUtils.error(logger, "TMC客服审核发送异常，异常信息={}", e);
            return false;
        }
    }

    //审核不通过
    @ResponseBody
    @RequestMapping("/refundAduitNoPass")
    public boolean refundAduitNoPass(RefundOrderDto refundOrderDto, Integer taskId) {
        refundOrderDto.setOperatorName(super.getCurrentUser().getFullname());
        refundOrderDto.setOperatorId(super.getCurrentUser().getId());
        refundOrderDto.setPassed(false);
        refundOrderDto.setAuditEndTime(new Date());
        boolean rs = flightOrderService.refundorderAduit(refundOrderDto, taskId);
        return rs;
    }

    //代客下单入口--查询用户界面
    @RequestMapping("/searchUserPage")
    public String searchUser() {
        return "flight/tmc/searchUserPage.jsp";
    }

    //菜单直接的入口
    @RequestMapping("/searchUsers")
    public String searchUsers() {
        return "flight/tmc/searchUserPage.base";
    }

    //用户查询
    @RequestMapping("/userList")
    public String userList(Model model, String keyword) {
        List<UserBaseInfo> userList = userService.findUserBaseInfo(super.getTmcId(), null, keyword);
        model.addAttribute("userList", userList);
        return "flight/tmc/userList.jsp";
    }

    @ResponseBody
    @RequestMapping("/specialLogin")
    public Map<String, Object> specialLogin(Long userId) {

        JSONObject json = new JSONObject();
        //客服人员id
        json.put("data_serviceId", super.getCurrentUser().getId());

        String crc = ssoService.getLoginAuthToken(userId, json);

        Map<String, Object> loginMap = new HashMap<String, Object>();

        loginMap.put("loginUrl", Config.getString("loginUrl"));
        loginMap.put("alp", crc);
        loginMap.put("service", Config.getString("autoLoginService"));

        String[] logoutString = ssoService.getLogoutUrls();

        loginMap.put("logoutUrl", logoutString);

        return loginMap;
    }


    //加载收入明细
    @RequestMapping("/getIncomeDetail")
    public String getIncomeDetail(Model model, String orderId, Long PaymentPlanNo) {
        List<TradeRecordInfo> tradeRecordInfos = bpAccTradeRecordService.listByOrderIdAndPlanId(orderId, PaymentPlanNo);
        model.addAttribute("tradeRecordList", tradeRecordInfos);
        return "flight/tmc/incomeDetail.single";
    }


    //无供应商信息时，点击'再次下单',调用出票接口
    @ResponseBody
    @RequestMapping("/againCreateOrder")
    public boolean againCreateOrder(String orderId, Integer taskId) {

        FlightOrderDto orderDto = flightOrderService.getFlightOrderById(orderId);

        //调用创单接口
        ResponseInfo info = flightHubsService.createOrder(orderDto);
        if (info.getCode().equals(FbpRtnCodeConst.SUCCESS)) {
            return true;
        }
        return false;
    }


    /**
     * 修改任务超时时间
     *
     * @param taskId 任务id
     * @param delay  延时时间：分钟
     */
    @RequestMapping("/updateMaxProcessDate")
    public void updateMaxProcessDate(Integer taskId, Integer delay) {
        OrderTaskDto dto = new OrderTaskDto();
        dto.setId(taskId);
        dto.setMaxProcessDate(DateUtils.addMinute(new Date(), delay));
        dto.setTaskStatus(TaskStatus.PROCESSING);
        orderTaskService.update(dto);
    }


    /**
     * 判断操作人是否为任务领取人
     *
     * @param operatorId 操作人id
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkOperator")
    public boolean checkOperator(String operatorId) {
        Long userId = super.getCurrentUser().getId();
        if (userId.toString().equals(operatorId)) {
            return true;
        }
        return false;
    }

    /**
     * 检查是否存在
     * 1.存在超过30秒还未领取的任务
     * 2.超时未完成的任务
     *
     * @return true:存在， false:不存在
     */
    @ResponseBody
    @RequestMapping("/checkTaskStatus")
    public boolean checkTaskStatus() {
        //查询未领取的任务
        List<OrderTaskDto> list = orderTaskService.findTaskByStatusAndBizType(TaskStatus.WAIT_PROCESS, OrderBizType.FLIGHT);
        for (OrderTaskDto orderTaskDto : list) {
            if (DateUtils.addSecond(orderTaskDto.getCreateDate(), 10).compareTo(new Date()) <= 0) {
                //存在超过30秒还未领取的任务
                return true;
            }
        }
        //查询超时未完成的任务
        list = orderTaskService.findTaskByStatusAndBizType(TaskStatus.PROCESSING, OrderBizType.FLIGHT);
        for (OrderTaskDto orderTaskDto : list) {
            if (orderTaskDto.getMaxProcessDate().compareTo(new Date()) <= 0) {
                return true;
            }
        }

        return false;
    }

    @RequestMapping("/orderDetail")
    public String orderDetail(Model model, String orderId) {
        FlightOrderDto orderDto = flightOrderService.getFlightOrderById(orderId);
        List<IssueChannelDto> channelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
                IssueChannelType.FLIGHT.getType(), LineType.OFF_LINE.getType());

        model.addAttribute("order", orderDto);
        model.addAttribute("channelList", channelList);

        return "flight/tmc/modifyTicket.jsp";
    }

    @RequestMapping(value = "/modifyTicket", produces = "application/json; charset=utf-8")
    @ResponseBody
    public ResponseInfo modifyTicket(@RequestBody TicketFrom formObj) {
        try {
            flightOrderService.modifyTicket(formObj.getOrderId(), formObj.getPassengerDtos(), formObj.getIssueChannel(),
                    formObj.getPnr(), formObj.getExternalOrderId(), formObj.getTotalPrice(),
                    super.getCurrentUser().getFullname());
        } catch (Exception e) {
            LogUtils.error(logger, "修改出票信息异常，异常信息={}", e);
            return new ResponseInfo(FAIL, "系统异常，修改失败！");
        }
        return new ResponseInfo(SUCCESS, "success");
    }

    @ResponseBody
    @RequestMapping("/confirmDone")
    public ResponseInfo confirmDone(String orderId, Integer taskId) {
        try {
            return flightOrderService.confirmDone(orderId, taskId, super.getCurrentUser().getFullname());
        } catch (Exception e) {
            return new ResponseInfo(FAIL, "系统异常,请稍后重试!");
        }
    }

    //-----------------------------------------改签相关-------------------------------------
    @RequestMapping("/getChangeOrderTask")
    public String getChangeOrderTask(String taskId, String orderId, Model model) {

        FlightOrderDto order = flightOrderService.getFlightOrderById(orderId);
        if (order == null || StringUtils.isBlank(order.getOriginalOrderId())) {
            return "redirect:/tmc/index";
        }
        FlightOrderDto originOrder = flightOrderService.getFlightOrderById(order.getOriginalOrderId());

        List<IssueChannelDto> channelTypes = issueChannelService.findListByTmcIdAndChannelType(Long.parseLong(order.getTmcId()), IssueChannelType.FLIGHT.name());
        for (IssueChannelDto channelDto : channelTypes) {
            if (channelDto.getCode().equals(order.getIssueChannel())) {
                order.setSettleChannelId(channelDto.getSettelChannelId());
            }
            if (channelDto.getCode().equals(originOrder.getIssueChannel())) {
                originOrder.setIssueChannelName(channelDto.getName());
                originOrder.setSettleChannelId(channelDto.getSettelChannelId());
            }
        }
        List<SettleChannelDto> settleChannels = settleChannelService.findSettleChannelByTmcId(Long.parseLong(order.getTmcId()), 0);
        List<OrderOperationLogDto> logList = flightOrderService.findOrderLogList(orderId);
        Collections.reverse(logList);
        OrderTaskDto taskDto = orderTaskService.findByTaskId(Integer.parseInt(taskId));

        String originRefundOrderId = null;
        for (PassengerDto passenger : order.getPassengers()) {

            for (PassengerDto originPassenger : originOrder.getPassengers()) {
                if (passenger.getUserId().equals(originPassenger.getUserId())
                        && passenger.getTravellerType().equals(originPassenger.getTravellerType())
                        && StringUtils.isNotEmpty(originPassenger.getRefundOrderId())) {
                    originRefundOrderId = originPassenger.getRefundOrderId();
                    break;
                }
            }

        }
        RefundOrderDto originRefundOrder = null;
        if (StringUtils.isNotEmpty(originRefundOrderId)) {
            originRefundOrder = refundOrderService.getById(originRefundOrderId);
        }

        model.addAttribute("originRefundOrder", originRefundOrder);
        model.addAttribute("taskDto", taskDto);
        model.addAttribute("logList", logList);
        model.addAttribute("settleChannels", settleChannels);
        model.addAttribute("channelTypes", channelTypes);
        model.addAttribute("order", order);
        model.addAttribute("originOrder", originOrder);
        model.addAttribute("taskId", taskId);

        return "flight/tmc/changeOrder.base";
    }

    @RequestMapping("/refuseChange")
    @ResponseBody
    public boolean refuseChange(String orderId, Integer taskId, String remark, String originRefundOrderId) {
        try {
            OrderTaskDto taskDto = orderTaskService.findByTaskId(taskId);
            taskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
            taskDto.setCompleteDate(new Date());
            taskDto.setResult("拒单退款");
            taskDto.setOperatorId(super.getCurrentUser().getId().intValue());
            taskDto.setOperatorName(super.getCurrentUser().getFullname());
            boolean b = flightOrderService.rejectOrder(orderId, remark, taskDto, originRefundOrderId);
            return b;
        } catch (Exception e) {
            LogUtils.error(logger, "拒绝改签发生错误={}", e);
            return false;
        }

    }

    @RequestMapping("/completeChange")
    @ResponseBody
    public boolean completeChange(@RequestBody ChangeOrderForm form) {
        try {
            ResponseInfo responseInfo = flightOrderService.changeTicketDone(form, form.getTaskId(), super.getCurrentUser().getId(), super.getCurrentUser().getFullname());
            if (Constants.SUCCESS.equals(responseInfo.getCode())) {
                return true;
            }
            return false;
        } catch (Exception e) {
            LogUtils.error(logger, "拒绝改签发生错误={}", e);
            return false;
        }

    }

    @RequestMapping("/confirmPay")
    @ResponseBody
    public boolean confirmPay(@RequestBody ChangeOrderForm form) {
        try {
            ResponseInfo responseInfo = flightOrderService.paymentDifference(form, form.getTaskId(), super.getCurrentUser().getFullname());
            if (Constants.SUCCESS.equals(responseInfo.getCode())) {
                return true;
            }
            return false;
        } catch (Exception e) {
            LogUtils.error(logger, "拒绝改签发生错误={}", e);
            return false;
        }
    }

    @ResponseBody
    @RequestMapping("/changePay")
    public Map<String, Object> changePay(Long userId, String orderId) {

        JSONObject json = new JSONObject();
        //客服人员id
        json.put("data_serviceId", super.getCurrentUser().getId());

        String crc = ssoService.getLoginAuthToken(userId, json);

        Map<String, Object> loginMap = new HashMap<String, Object>();

        loginMap.put("loginUrl", Config.getString("loginUrl"));
        loginMap.put("alp", crc);
        loginMap.put("service", Config.getString("paymentUrl") + orderId);

        String[] logoutString = ssoService.getLogoutUrls();

        loginMap.put("logoutUrl", logoutString);

        return loginMap;
    }


    @RequestMapping("/getChangeOrderData")
    public String getChangeOrderData(String orderId, Integer taskId, Model model) {
        FlightOrderDto order = flightOrderService.getFlightOrderById(orderId);
        List<DictCodeDto> cabinGrade = dictService.getCodes(Constants.OTAF_CABIN_GRADE);

        //航司
        CarrierDto carrierDto = carrierService.findByCode(order.getOrderFlights().get(0).getFlightNo().substring(0, 2));
        //机型
        AircraftDto aircraftDto = aircraftService.getByCode(order.getOrderFlights().get(0).getAircraftType());


        FlightOrderDto originOrder = flightOrderService.getFlightOrderById(order.getOriginalOrderId());

        //航司
        CarrierDto originCarrierDto = carrierService.findByCode(originOrder.getOrderFlights().get(0).getFlightNo().substring(0, 2));

        model.addAttribute("carrierDto", JSONObject.toJSONString(carrierDto));
        model.addAttribute("aircraftDto", JSONObject.toJSONString(aircraftDto));
        model.addAttribute("order", JSONObject.toJSONString(order));
        model.addAttribute("orderDto", order);
        model.addAttribute("cabinGrade", cabinGrade);
        model.addAttribute("originOrder", originOrder);
        model.addAttribute("originCarrierDto", originCarrierDto);
        model.addAttribute("taskId", taskId);

        return "flight/tmc/modifyChangeOrder.base";
    }


    @ResponseBody
    @RequestMapping("/updateChangeOrder")
    public boolean updateChangeOrder(FlightForm flightForm) {

        try {
            FlightOrderDto order = flightOrderService.getFlightOrderById(flightForm.getId());

            flightForm.fill(order);

            FlightOrderDto originOrder = flightOrderService.getFlightOrderById(order.getOriginalOrderId());

            String carrier = order.getOrderFlights().get(0).getFlightNo().substring(0, 2);
            String originCarrier = originOrder.getOrderFlights().get(0).getFlightNo().substring(0, 2);

            if (carrier.equals(originCarrier)) {
                order.setOrderType(OrderTypeEnum.TICKET_CHANGES);
            } else {
                CarrierDto carrierDto = carrierService.findByCode(carrier);
                CarrierDto originCarrierDto = carrierService.findByCode(originCarrier);
                if (carrierDto.getGroupCode() != null && originCarrierDto.getGroupCode() != null) {
                    if (carrierDto.getGroupCode().equals(originCarrierDto.getGroupCode())) {
                        order.setOrderType(OrderTypeEnum.TICKET_CHANGES);
                    } else {
                        order.setOrderType(OrderTypeEnum.REFUND_TICKET_REPURCHASE);
                    }
                } else {
                    order.setOrderType(OrderTypeEnum.REFUND_TICKET_REPURCHASE);
                }

            }


            CabinDto cabinDto = cabinService.findByCabinCodeAndCarrierCode(order.getOrderFlights().get(0).getCabinCode(),
                    order.getOrderFlights().get(0).getFlightNo().substring(0, 2));
            order.getOrderFlights().get(0).setCabinClass(cabinDto.getGrade());
            order.getOrderFlights().get(0).setCabinType(cabinDto.getType());
            FullPriceCabinDto fullPriceCabin = flightForm.getFlightDetail().getFullPriceCabin();
            if ("Y".equals(order.getOrderFlights().get(0).getCabinClass())) {
                order.getOrderFlights().get(0).setCabinFullPrice(fullPriceCabin.getYprice());
            } else if ("C".equals(order.getOrderFlights().get(0).getCabinClass())) {
                order.getOrderFlights().get(0).setCabinFullPrice(fullPriceCabin.getCprice());
            } else if ("F".equals(order.getOrderFlights().get(0).getCabinClass())) {
                order.getOrderFlights().get(0).setCabinFullPrice(fullPriceCabin.getFprice());
            }
            for (OrderFlightDetailDto orderFlight : order.getOrderFlights()) {
                AirportDto from = airportService.findCityByCode(orderFlight.getFromAirport());
                orderFlight.setFromCity(from.getCode());
                orderFlight.setFromCityName(from.getCityName());
                AirportDto to = airportService.findCityByCode(orderFlight.getToAirport());
                orderFlight.setToCity(to.getCode());
                orderFlight.setToCityName(to.getCityName());
            }


            flightOrderService.updateFlightOrder(order);

            OrderOperationLogDto dto = new OrderOperationLogDto();
            dto.setOperationDate(new Date());
            dto.setType(OrderOperationType.MODIFY_CHANGE_ORDER);
            dto.setOperator("(客服)" + super.getCurrentUser().getFullname());
            dto.setStatus(OrderOperationStatus.SUCCESS);
            dto.setOrderId(order.getId());
            dto.setDescription("客服修改改签订单数据");
            flightOrderService.addOrderOperationLog(dto);

        } catch (Exception e) {
            LogUtils.error(logger, "修改改签订单发生错误={}", e);
            return false;
        }
        return true;
    }


    @RequestMapping("/refundChangeDetail")
    @ResponseBody
    public RefundChangeDetailDto getRefundChangeDetail(FreightCondition condition) {
        condition.setCid(String.valueOf(super.getPartnerId()));
        RefundChangeDetailDto detailDto = freightService.getRefundChangeDetail(condition);
        return detailDto;
    }

    //根据机场三字码查询对应城市和机场
    @ResponseBody
    @RequestMapping("/getAirportAndCity")
    public String getAirportAndCity(String code) {
        JSONObject json = new JSONObject();
        //查询机场
        AirportDto airport = airportService.findAirportByCode(code.toUpperCase(), RegionType.DOMESTIC.getType());
        json.put("airport", airport);
        return json.toJSONString();
    }

    //根据编码查询航司
    @ResponseBody
    @RequestMapping("/getCarrierByCode")
    public CarrierDto getCarrierByCode(String carrierCode) {
        if (StringUtils.isNotBlank(carrierCode)) {
            return carrierService.findByCode(carrierCode.toUpperCase());
        }
        return null;
    }

    //根据编码查询机型
    @ResponseBody
    @RequestMapping("/getAircraftByCode")
    public AircraftDto getAircraftByCode(String code) {
        if (StringUtils.isNotBlank(code)) {
            return aircraftService.getByCode(code.toUpperCase());
        }
        return null;
    }


    //检查是否属于同一航司下
    @ResponseBody
    @RequestMapping("/checkSameCarrierGroup")
    public boolean checkSameCarrierGroup(String originOrderId, String flightNo) {

        FlightOrderDto originOrder = flightOrderService.getFlightOrderById(originOrderId);

        String carrier = flightNo.substring(0, 2);
        String originCarrier = originOrder.getOrderFlights().get(0).getFlightNo().substring(0, 2);

        if (carrier.equals(originCarrier)) {
            return true;
        } else {
            CarrierDto carrierDto = carrierService.findByCode(carrier);
            CarrierDto originCarrierDto = carrierService.findByCode(originCarrier);
            if (carrierDto.getGroupCode() != null && originCarrierDto.getGroupCode() != null) {
                if (carrierDto.getGroupCode().equals(originCarrierDto.getGroupCode())) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }

        }
    }


    private ResponseInfo checkTaskStatus(int taskId) {
        OrderTaskDto task = orderTaskService.findByTaskId(taskId);

        if (task == null) {
            return new ResponseInfo(FAIL, "任务不存在", "");
        }

        if (TaskStatus.ALREADY_PROCESSED.equals(task.getTaskStatus())) {
            return new ResponseInfo(INVALID_STATUS, "任务已完处理,请勿重复操作", "");
        }

        return new ResponseInfo(SUCCESS, "", "");
    }


}
