package com.tem.flight.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.web.Message;
import com.tem.flight.api.priceRule.PriceRuleService;
import com.tem.flight.dto.enums.PriceRuleGradeEnum;
import com.tem.flight.dto.enums.PriceRuleTypeEnum;
import com.tem.flight.dto.priceRule.PriceRuleDto;
import com.tem.flight.dto.priceRule.PriceRuleQueryDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 加价规则controller
 * 
 * @author yuanjianxin
 *
 */
@Controller
@RequestMapping("/flight/priceRule")
public class PriceRuleController extends BaseController {

	@Autowired
	private PriceRuleService priceRuleService;
	
	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "")
	public String index(Model model) {
		return "flight/priceRule/index.base";
	}

	// 分页查询
	@RequestMapping(value = "/list")
	public String list(Model model, Integer pageIndex, Integer pageSize,Integer partnerId) {
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}

		QueryDto<PriceRuleQueryDto> queryDto = new QueryDto<PriceRuleQueryDto>(
				pageIndex, pageSize);
		PriceRuleQueryDto dto = new PriceRuleQueryDto(partnerId);
		queryDto.setCondition(dto);
		Page<PriceRuleDto> page = priceRuleService.findPageList(queryDto);
		
		for(PriceRuleDto ruleDto : page.getList()){
			Integer cid = ruleDto.getPartnerId();
			if(cid!=null){
				PartnerDto partnerDto = partnerService.findById(cid.longValue());
				if(partnerDto!=null){
					ruleDto.setPartnerName(partnerDto.getName());
				}
			}
		}

		model.addAttribute("pageList", page);

		return "flight/priceRule/priceRuleTable.jsp";
	}

	// 批量删除
	@ResponseBody
	@RequestMapping("/delete")
	public Message delete(String ids) {
		String[] split = ids.split(",");
		List<Integer> list = new ArrayList<Integer>();
		for (String s : split) {
			list.add(Integer.valueOf(s));
		}
		boolean rs = priceRuleService.batchRemoveRule(list);
		if (rs) {
			return Message.success("删除成功");
		} else {
			return Message.error("删除失败，请刷新重试");
		}
	}

	// 用于新增和修改时查询对象
	@RequestMapping(value = "/getPriceRule")
	public String getPriceRule(Model model, Integer id) {
		PriceRuleDto priceRuleDto = null;
		if(id != null){
			priceRuleDto = priceRuleService.getPriceRuleById(id);
			if(priceRuleDto != null){
				//创建人
				UserDto createor = userService.getUser(Long.valueOf(priceRuleDto.getCreator()));
				//企业
				if(priceRuleDto.getPartnerId() != null){
					PartnerDto partnerDto = partnerService.findById(priceRuleDto.getPartnerId().longValue());
					if(partnerDto != null){
						priceRuleDto.setPartnerName(partnerDto.getName());
					}
				}
				model.addAttribute("createor",createor);
			}
		}
		model.addAttribute("gradeEnum",PriceRuleGradeEnum.values());
		model.addAttribute("ruleTypeEnum",PriceRuleTypeEnum.values());
		model.addAttribute("priceRuleDto", priceRuleDto);
		return "flight/priceRule/priceRuleModel.jsp";
	}

	// 保存对象
	@ResponseBody
	@RequestMapping(value = "/save")
	public Message addOrUpdate(Model model, PriceRuleDto priceRuleDto) {
		if(priceRuleDto.getId()==null){
			if(PriceRuleGradeEnum.PLATFORM.equals(priceRuleDto.getGrade())){
				Boolean exist = priceRuleService.existPlatformGradeRule();
				if(exist){
					return Message.error("已经存在平台级别的加价规则，请勿重复添加");
				}
			}else{
				PriceRuleDto ruleDto = priceRuleService.getPriceRuleDtoByPartnerId(priceRuleDto.getPartnerId().longValue());
				if(ruleDto != null){
					return Message.error("该企业已经配置了加价规则，请勿重复添加");
				}
			}
			priceRuleDto.setCreator(super.getCurrentUser().getId().intValue());
		}

		if (priceRuleDto.getExceedStandard() == null) {
			priceRuleDto.setExceedStandard(false);
		}

		boolean rs = priceRuleService.saveRule(priceRuleDto);
		if (rs) {
			return Message.success("保存成功");
		} else {
			return Message.error("保存失败,请刷新重试");
		}
	}

}
