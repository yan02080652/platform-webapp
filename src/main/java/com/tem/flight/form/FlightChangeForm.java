package com.tem.flight.form;

import com.iplatform.common.utils.DateUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 航班动态form
 * Created by pyy on 2017/5/10.
 */
public class FlightChangeForm implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 航班号
     */
    private String flightNo;

    /**
     * 航空公司名称
     */
    private String flightCompany;

    /**
     * 出发地机场三字码
     */
    private String flightDepCode;

    /**
     * 到达地机场三字码
     */
    private String flightArrCode;

    /**
     * 出发地机场名称
     */
    private String flightDepAirport;

    /**
     * 到达地机场名称
     */
    private String flightArrAirport;

    /**
     * 出发地城市名称
     */
    private String flightDepCity;

    /**
     * 到达地城市名称
     */
    private String flightArrCity;

    /**
     * 出发地机场航站楼
     */
    private String flightDepTerminal;

    /**
     * 到达地机场航站楼
     */
    private String flightArrTerminal;

    /**
     * 是否经停
     */
    private Integer stopFlag;

    /**
     * 经停机场名称
     */
    private String stopAirport;

    /**
     * 经停机场三字码
     */
    private String stopAirportCode;

    /**
     * 是否共享
     */
    private Integer shareFlag;

    /**
     * 共享航班号（实际承运航班的航班号）
     */
    private String shareFlightNo;

    /**
     * 航班状态
     */
    private Integer flightState;

    /**
     * 取消/延误原因
     */
    private Integer cancelReson;

    /**
     * 备降后出发机场名
     */
    private String alternateDepAirport;

    /**
     * 备降后出发机场三字码
     */
    private String alternateDepAirportCode;

    /**
     * 出发地登机口
     */
    private String flightDepBoardingPort;

    /**
     * 目的地行李转盘编号
     */
    private String flightArrBaggageCarouselNo;

    /**
     * 计划起飞时间
     */
    private Date flightDepTimePlanDate;

    /**
     * 计划到达时间
     */
    private Date flightArrTimePlanDate;

    /**
     * 经停到达时间
     */
    private Date stopAirportDepTime;

    /**
     * 经停起飞时间
     */
    private Date stopAirportArrTime;

    /**
     * 延误预计起飞时间
     */
    private Date delayFlightDepTime;

    /**
     * 延误预计到达时间
     */
    private Date delayFlightArrTime;

    /**
     * 实际起飞时间
     */
    private Date flightDepTimeDate;

    /**
     * 实际到达时间
     */
    private Date flightArrTimeDate;

    private Integer sengMsg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getFlightCompany() {
        return flightCompany;
    }

    public void setFlightCompany(String flightCompany) {
        this.flightCompany = flightCompany;
    }

    public String getFlightDepCode() {
        return flightDepCode;
    }

    public void setFlightDepCode(String flightDepCode) {
        this.flightDepCode = flightDepCode;
    }

    public String getFlightArrCode() {
        return flightArrCode;
    }

    public void setFlightArrCode(String flightArrCode) {
        this.flightArrCode = flightArrCode;
    }

    public String getFlightDepAirport() {
        return flightDepAirport;
    }

    public void setFlightDepAirport(String flightDepAirport) {
        this.flightDepAirport = flightDepAirport;
    }

    public String getFlightArrAirport() {
        return flightArrAirport;
    }

    public void setFlightArrAirport(String flightArrAirport) {
        this.flightArrAirport = flightArrAirport;
    }

    public String getFlightDepCity() {
        return flightDepCity;
    }

    public void setFlightDepCity(String flightDepCity) {
        this.flightDepCity = flightDepCity;
    }

    public String getFlightArrCity() {
        return flightArrCity;
    }

    public void setFlightArrCity(String flightArrCity) {
        this.flightArrCity = flightArrCity;
    }

    public String getFlightDepTerminal() {
        return flightDepTerminal;
    }

    public void setFlightDepTerminal(String flightDepTerminal) {
        this.flightDepTerminal = flightDepTerminal;
    }

    public String getFlightArrTerminal() {
        return flightArrTerminal;
    }

    public void setFlightArrTerminal(String flightArrTerminal) {
        this.flightArrTerminal = flightArrTerminal;
    }

    public Integer getStopFlag() {
        return stopFlag;
    }

    public void setStopFlag(Integer stopFlag) {
        this.stopFlag = stopFlag;
    }

    public String getStopAirport() {
        return stopAirport;
    }

    public void setStopAirport(String stopAirport) {
        this.stopAirport = stopAirport;
    }

    public String getStopAirportCode() {
        return stopAirportCode;
    }

    public void setStopAirportCode(String stopAirportCode) {
        this.stopAirportCode = stopAirportCode;
    }

    public Integer getShareFlag() {
        return shareFlag;
    }

    public void setShareFlag(Integer shareFlag) {
        this.shareFlag = shareFlag;
    }

    public String getShareFlightNo() {
        return shareFlightNo;
    }

    public void setShareFlightNo(String shareFlightNo) {
        this.shareFlightNo = shareFlightNo;
    }

    public Integer getFlightState() {
        return flightState;
    }

    public void setFlightState(Integer flightState) {
        this.flightState = flightState;
    }

    public Integer getCancelReson() {
        return cancelReson;
    }

    public void setCancelReson(Integer cancelReson) {
        this.cancelReson = cancelReson;
    }

    public String getAlternateDepAirport() {
        return alternateDepAirport;
    }

    public void setAlternateDepAirport(String alternateDepAirport) {
        this.alternateDepAirport = alternateDepAirport;
    }

    public String getAlternateDepAirportCode() {
        return alternateDepAirportCode;
    }

    public void setAlternateDepAirportCode(String alternateDepAirportCode) {
        this.alternateDepAirportCode = alternateDepAirportCode;
    }

    public String getFlightDepBoardingPort() {
        return flightDepBoardingPort;
    }

    public void setFlightDepBoardingPort(String flightDepBoardingPort) {
        this.flightDepBoardingPort = flightDepBoardingPort;
    }

    public String getFlightArrBaggageCarouselNo() {
        return flightArrBaggageCarouselNo;
    }

    public void setFlightArrBaggageCarouselNo(String flightArrBaggageCarouselNo) {
        this.flightArrBaggageCarouselNo = flightArrBaggageCarouselNo;
    }

    public Date getFlightDepTimePlanDate() {
        return flightDepTimePlanDate;
    }

    public void setFlightDepTimePlanDate(Date flightDepTimePlanDate) {
        this.flightDepTimePlanDate = flightDepTimePlanDate;
    }

    public Date getFlightArrTimePlanDate() {
        return flightArrTimePlanDate;
    }

    public void setFlightArrTimePlanDate(Date flightArrTimePlanDate) {
        this.flightArrTimePlanDate = flightArrTimePlanDate;
    }

    public Date getStopAirportDepTime() {
        return stopAirportDepTime;
    }

    public void setStopAirportDepTime(Date stopAirportDepTime) {
        this.stopAirportDepTime = stopAirportDepTime;
    }

    public Date getStopAirportArrTime() {
        return stopAirportArrTime;
    }

    public void setStopAirportArrTime(Date stopAirportArrTime) {
        this.stopAirportArrTime = stopAirportArrTime;
    }

    public Date getDelayFlightDepTime() {
        return delayFlightDepTime;
    }

    public void setDelayFlightDepTime(Date delayFlightDepTime) {
        this.delayFlightDepTime = delayFlightDepTime;
    }

    public Date getDelayFlightArrTime() {
        return delayFlightArrTime;
    }

    public void setDelayFlightArrTime(Date delayFlightArrTime) {
        this.delayFlightArrTime = delayFlightArrTime;
    }

    public Date getFlightDepTimeDate() {
        return flightDepTimeDate;
    }

    public void setFlightDepTimeDate(Date flightDepTimeDate) {
        this.flightDepTimeDate = flightDepTimeDate;
    }

    public Date getFlightArrTimeDate() {
        return flightArrTimeDate;
    }

    public void setFlightArrTimeDate(Date flightArrTimeDate) {
        this.flightArrTimeDate = flightArrTimeDate;
    }

    private Date toDate(String str){
        Date date = null;
        if(StringUtils.isNotEmpty(str)){
            date = DateUtils.parse(str,"yyyy-MM-dd HH:mm");
        }
        return date;
    }

    public void setFlightDepTimePlanDateStr(String flightDepTimePlanDateStr) {
        this.flightDepTimePlanDate = this.toDate(flightDepTimePlanDateStr);
    }

    public void setFlightArrTimePlanDateStr(String flightArrTimePlanDateStr) {
        this.flightArrTimePlanDate = this.toDate(flightArrTimePlanDateStr);
    }

    public void setStopAirportDepTimeStr(String stopAirportDepTimeStr) {
        this.stopAirportDepTime = this.toDate(stopAirportDepTimeStr);
    }

    public void setStopAirportArrTimeStr(String stopAirportArrTimeStr) {
        this.stopAirportArrTime = this.toDate(stopAirportArrTimeStr);
    }

    public void setDelayFlightDepTimeStr(String delayFlightDepTimeStr) {
        this.delayFlightDepTime = this.toDate(delayFlightDepTimeStr);
    }

    public void setDelayFlightArrTimeStr(String delayFlightArrTimeStr) {
        this.delayFlightArrTime = this.toDate(delayFlightArrTimeStr);
    }

    public void setFlightDepTimeDateStr(String flightDepTimeDateStr) {
        this.flightDepTimeDate = this.toDate(flightDepTimeDateStr);
    }

    public void setFlightArrTimeDateStr(String flightArrTimeDateStr) {
        this.flightArrTimeDate = this.toDate(flightArrTimeDateStr);
    }

    public Integer getSengMsg() {
        return sengMsg;
    }

    public void setSengMsg(Integer sengMsg) {
        this.sengMsg = sengMsg;
    }
}
