package com.tem.flight.form;

import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.tem.flight.dto.enums.OrderTypeEnum;
import com.tem.flight.dto.flight.EndorseAndRefundRuleDto;
import com.tem.flight.dto.flight.FlightDetailDto;
import com.tem.flight.dto.flight.FlightPriceDto;
import com.tem.flight.dto.flight.FlightStopOverDto;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.flight.dto.order.OrderEndorseRefundRuleDto;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

public class FlightForm implements Serializable {
    private static final long serialVersionUID = -719541317852201404L;

    private String id;
    /**
     * 航班详情
     */
    private FlightDetailDto flightDetail;
    /**
     * TEM优惠价格
     */
    private BigDecimal discountAmount;
    /**
     * 付款方式
     */
    private String paymentMethod;
    // 起飞时间（字符串）
    private String fromDateString;
    // 达到时间
    private String toDateString;
    // 经停起飞时间
    private String stopOverFromTime;
    // 经停到达时间
    private String stopOverToTime;
    // 订单创建时间（手工订单用到）
    private String createDate;

    // 客服人员id
    private String serviceId;
    // 升舱费
    private BigDecimal upgradePrice;
    // 改签费
    private BigDecimal rescheduledPrice;
    //服务费
    private BigDecimal servicePrice;
    //是否改签订单
    private Boolean changeOrder;
    //原始订单号
    private String originalOrderId;

    //pnr导入存放人员信息
    private String userJsonString;


    public FlightForm() {
    }

    public void fill(FlightOrderDto order){

        order.getOrderFlights().get(0).setFromDate(DateUtils.parse(this.fromDateString, DateUtils.YYYY_MM_DD_HH_MM_SS));
        order.getOrderFlights().get(0).setToDate(DateUtils.parse(this.toDateString, DateUtils.YYYY_MM_DD_HH_MM_SS));

        FlightStopOverDto flightStopOver = this.flightDetail.getFlightStopOver();
        if (flightStopOver != null && StringUtils.isNotBlank(this.stopOverFromTime) && StringUtils.isNotBlank(this.stopOverToTime)) {
            order.getOrderFlights().get(0).setStopCityName(flightStopOver.getCityName());
            order.getOrderFlights().get(0).setStopCity(flightStopOver.getStopCity());
            order.getOrderFlights().get(0).setFromTime(DateUtils.parse(this.stopOverFromTime, DateUtils.YYYY_MM_DD_HH_MM_SS));
            order.getOrderFlights().get(0).setToTime(DateUtils.parse(this.stopOverToTime, DateUtils.YYYY_MM_DD_HH_MM_SS));
        }
        order.getOrderFlights().get(0).setFlightNo(this.flightDetail.getFlightNo());
        order.getOrderFlights().get(0).setCabinCode(this.flightDetail.getCabinList().get(0).getCode());
        order.getOrderFlights().get(0).setFromAirport(this.flightDetail.getFromAirport());
        order.getOrderFlights().get(0).setToAirport(this.flightDetail.getToAirport());
        order.getOrderFlights().get(0).setCarrierName(this.flightDetail.getCarrier().getNameShort());
        order.getOrderFlights().get(0).setFromAirportName(this.flightDetail.getFromAirportName());
        order.getOrderFlights().get(0).setToAirportName(this.flightDetail.getToAirportName());
        order.getOrderFlights().get(0).setFromTerminal(this.flightDetail.getFromTerminal());
        order.getOrderFlights().get(0).setToTerminal(this.flightDetail.getToTerminal());
        if(this.flightDetail.getFullPriceCabin().getYprice() != null) {
            order.getOrderFlights().get(0).setYprice(BigDecimal.valueOf(this.flightDetail.getFullPriceCabin().getYprice()));
        }
        //构建退改签
        EndorseAndRefundRuleDto endorseAndRefundRuleDto = this.flightDetail.getFlightPrice().getEndorseAndRefundRule().get(0);
        OrderEndorseRefundRuleDto orderEndorseRefundRuleDto = order.getEndorseRefundRules().get(0);

        orderEndorseRefundRuleDto.setChangeRule(endorseAndRefundRuleDto.getTicketChangeRemark());
        orderEndorseRefundRuleDto.setRefundRule(endorseAndRefundRuleDto.getTicketBounceRemark());
        orderEndorseRefundRuleDto.setEndorseRule(endorseAndRefundRuleDto.getTicketSignChangeRemark());
        orderEndorseRefundRuleDto.setCancelRemark(endorseAndRefundRuleDto.getTicketCancelRemark());
        orderEndorseRefundRuleDto.setOutRemark(endorseAndRefundRuleDto.getOutRemark());


    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FlightForm(FlightDetailDto flightDetail) {
        this.flightDetail = flightDetail;
    }

    public FlightDetailDto getFlightDetail() {
        return flightDetail;
    }

    public void setFlightDetail(FlightDetailDto flightDetail) {
        this.flightDetail = flightDetail;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }


    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    public String getFromDateString() {
        return fromDateString;
    }

    public void setFromDateString(String fromDateString) {
        this.fromDateString = fromDateString;
    }

    public String getToDateString() {
        return toDateString;
    }

    public void setToDateString(String toDateString) {
        this.toDateString = toDateString;
    }

    public String getStopOverFromTime() {
        return stopOverFromTime;
    }

    public void setStopOverFromTime(String stopOverFromTime) {
        this.stopOverFromTime = stopOverFromTime;
    }

    public String getStopOverToTime() {
        return stopOverToTime;
    }

    public void setStopOverToTime(String stopOverToTime) {
        this.stopOverToTime = stopOverToTime;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getUpgradePrice() {
        return upgradePrice;
    }

    public void setUpgradePrice(BigDecimal upgradePrice) {
        this.upgradePrice = upgradePrice;
    }

    public BigDecimal getRescheduledPrice() {
        return rescheduledPrice;
    }

    public void setRescheduledPrice(BigDecimal rescheduledPrice) {
        this.rescheduledPrice = rescheduledPrice;
    }

    public BigDecimal getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(BigDecimal servicePrice) {
        this.servicePrice = servicePrice;
    }

    public Boolean getChangeOrder() {
        return changeOrder;
    }

    public void setChangeOrder(Boolean changeOrder) {
        this.changeOrder = changeOrder;
    }

    public String getOriginalOrderId() {
        return originalOrderId;
    }

    public void setOriginalOrderId(String originalOrderId) {
        this.originalOrderId = originalOrderId;
    }

    public String getUserJsonString() {
        return userJsonString;
    }

    public void setUserJsonString(String userJsonString) {
        this.userJsonString = userJsonString;
    }
}
