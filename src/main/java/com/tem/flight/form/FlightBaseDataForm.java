package com.tem.flight.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yuanjianxin
 * @date 2017/6/29 17:41
 */
public class FlightBaseDataForm implements Serializable {
    private static final long serialVersionUID = -6234831620576577325L;

    //用户id
    private Long userId;
    //用户类型
    private String userType;
    //费用归属编码
    private String costUnitCode;
    //费用归属所属orgId
    private String orgId;
    //法人id
    private String corporationId;
    //职级
    private String empLevelCode;
    private String email;
    private String mobile;
    private String gender;


    /**********excel字段 start*************/
    //用户名称
    private String userName;
    //证件类型
    private String certType;
    //证件号
    private String certNo;
    //票号
    private String ticketNo;
    //航司编码
    private String carrierCode;
    //航班号
    private String flightNo;
    //机型编码
    private String aircraftCode;
    //舱位编码
    private String cabinCode;
    //出发机场三字码
    private String fromAirportCode;
    //到达机场三字码
    private String toAirportCode;
    //预订时间
    private Date createDate;
    //出发时间
    private Date fromDate;
    //到达时间
    private Date toDate;
    //票面价
    private BigDecimal facePrice;
    //机场建设费
    private BigDecimal departureTax;
    //燃油费
    private BigDecimal fuelTax;
    //总价
    private BigDecimal totalPrice;
    //pnr
    private String pnr;
    //出票渠道
    private String issueChannel;

    /**********excel字段 end*************/


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCostUnitCode() {
        return costUnitCode;
    }

    public void setCostUnitCode(String costUnitCode) {
        this.costUnitCode = costUnitCode;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getCorporationId() {
        return corporationId;
    }

    public void setCorporationId(String corporationId) {
        this.corporationId = corporationId;
    }

    public String getEmpLevelCode() {
        return empLevelCode;
    }

    public void setEmpLevelCode(String empLevelCode) {
        this.empLevelCode = empLevelCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getAircraftCode() {
        return aircraftCode;
    }

    public void setAircraftCode(String aircraftCode) {
        this.aircraftCode = aircraftCode;
    }

    public String getCabinCode() {
        return cabinCode;
    }

    public void setCabinCode(String cabinCode) {
        this.cabinCode = cabinCode;
    }

    public String getFromAirportCode() {
        return fromAirportCode;
    }

    public void setFromAirportCode(String fromAirportCode) {
        this.fromAirportCode = fromAirportCode;
    }

    public String getToAirportCode() {
        return toAirportCode;
    }

    public void setToAirportCode(String toAirportCode) {
        this.toAirportCode = toAirportCode;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public BigDecimal getFacePrice() {
        return facePrice;
    }

    public void setFacePrice(BigDecimal facePrice) {
        this.facePrice = facePrice;
    }

    public BigDecimal getDepartureTax() {
        return departureTax;
    }

    public void setDepartureTax(BigDecimal departureTax) {
        this.departureTax = departureTax;
    }

    public BigDecimal getFuelTax() {
        return fuelTax;
    }

    public void setFuelTax(BigDecimal fuelTax) {
        this.fuelTax = fuelTax;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getIssueChannel() {
        return issueChannel;
    }

    public void setIssueChannel(String issueChannel) {
        this.issueChannel = issueChannel;
    }
}
