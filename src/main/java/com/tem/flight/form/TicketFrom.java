package com.tem.flight.form;

import com.tem.flight.dto.order.PassengerDto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 线下出票填单Form
 * 
 * @author yuanjianxin
 *
 */
public class TicketFrom implements Serializable {

	private static final long serialVersionUID = 1L;

	// 订单id
	private String orderId;
	// 出票渠道
	private String issueChannel;
	// pnr
	private String pnr;
	// 供应单号
	private String externalOrderId;
	// 结算总价
	private BigDecimal totalPrice;
	// 任务id
	private Integer taskId;
	// 乘客
	private List<PassengerDto> passengerDtos;
	//用来判断是"线下出票"还是"回填票号" 1 线下出票    2回填票号  3.改签成功
	private String flag;

	private boolean cancelSupplierOrder;

	public String getIssueChannel() {
		return issueChannel;
	}

	public void setIssueChannel(String issueChannel) {
		this.issueChannel = issueChannel;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getExternalOrderId() {
		return externalOrderId;
	}

	public void setExternalOrderId(String externalOrderId) {
		this.externalOrderId = externalOrderId;
	}

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	public List<PassengerDto> getPassengerDtos() {
		return passengerDtos;
	}

	public void setPassengerDtos(List<PassengerDto> passengerDtos) {
		this.passengerDtos = passengerDtos;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public boolean isCancelSupplierOrder() {
		return cancelSupplierOrder;
	}

	public void setCancelSupplierOrder(boolean cancelSupplierOrder) {
		this.cancelSupplierOrder = cancelSupplierOrder;
	}
}
