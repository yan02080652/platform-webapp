package com.tem.flight.form;

import com.tem.flight.dto.routeRule.RouteRuleDto;
import com.tem.flight.dto.routeRule.RouteRuleItemDto;

import java.io.Serializable;
import java.util.List;

public class RouteRuleForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private RouteRuleDto routeRuleDto;

	private List<RouteRuleItemDto> itemDtos;

	public RouteRuleDto getRouteRuleDto() {
		return routeRuleDto;
	}

	public void setRouteRuleDto(RouteRuleDto routeRuleDto) {
		this.routeRuleDto = routeRuleDto;
	}

	public List<RouteRuleItemDto> getItemDtos() {
		return itemDtos;
	}

	public void setItemDtos(List<RouteRuleItemDto> itemDtos) {
		this.itemDtos = itemDtos;
	}

}
