package com.tem.flight.form;

import com.tem.platform.api.dto.UserDto;

import java.io.Serializable;

/**
 * 用户信息form
 * 
 * @author yuanjianxin
 *
 */
public class UserForm implements Serializable {

	private static final long serialVersionUID = 1L;
	// 用户Id
	private Long userId;
	// 用户名
	private String userName;
	// 电话
	private String mobile;
	// 邮箱
	private String email;
	// 部门路径
	private String deptPath;

	public UserForm() {
	}

	public UserForm(UserDto dto) {
		this.userId = dto.getId();
		this.userName = dto.getFullname();
		this.mobile = dto.getMobile();
		this.email = dto.getEmail();
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeptPath() {
		return deptPath;
	}

	public void setDeptPath(String deptPath) {
		this.deptPath = deptPath;
	}


}
