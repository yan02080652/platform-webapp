package com.tem.flight.form;

import java.io.Serializable;

public class WhitelistUserForm implements Serializable {

    //序号
    public Integer id;

    //类型  员工 非员工
    public String userType;

    //中文名
    public String fullname;

    //英文名、英文性
    public String nameEn;

    //身份证
    public String idNo;
    //其他证件1
    public String certificateno1;
    //其他证件2
    public String certificateno2;

    //生效日期
    public String effectiveDate;
    //失效日期
    public String invalidDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getCertificateno1() {
        return certificateno1;
    }

    public void setCertificateno1(String certificateno1) {
        this.certificateno1 = certificateno1;
    }

    public String getCertificateno2() {
        return certificateno2;
    }

    public void setCertificateno2(String certificateno2) {
        this.certificateno2 = certificateno2;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getInvalidDate() {
        return invalidDate;
    }

    public void setInvalidDate(String invalidDate) {
        this.invalidDate = invalidDate;
    }
}