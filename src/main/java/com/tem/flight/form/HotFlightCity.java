package com.tem.flight.form;

import java.util.LinkedList;
import java.util.List;

public enum HotFlightCity {

    //北京 上海 广州 深圳 成都 杭州 武汉 西安 重庆 青岛 长沙 南京 厦门 昆明 大连 天津 郑州 三亚 济南 福州
    BJS,SHA,CAN,SZX,CTU,HGH,WUH,XIY,CKG,TAO,CSX,NKG,XMN,KMG,DLC,TSN,CGO,SYX,TNA,FOC;

    public static List<String> getAllHotCityCode(){
        List<String> list = new LinkedList<>();
        for (HotFlightCity city : HotFlightCity.values()) {
            list.add(city.name());
        }
        return list;
    }
}
