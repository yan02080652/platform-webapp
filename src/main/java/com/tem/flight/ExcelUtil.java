package com.tem.flight;

import com.iplatform.common.Config;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.List;

public class ExcelUtil {

    public static String getStringValueFromCell(Cell cell) {
        if(cell == null){
            return null;
        }

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_BLANK:
                return null;
            case Cell.CELL_TYPE_NUMERIC:
                return "" + cell.getNumericCellValue();
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();
            case Cell.CELL_TYPE_BOOLEAN:
                return "" + cell.getBooleanCellValue();
            case Cell.CELL_TYPE_FORMULA:
                return cell.getCellFormula();
            default:
                return null;
        }
    }

    public static String parseString(Object str){
        String returnValue = "";
        try {
            returnValue = new BigDecimal(String.valueOf(str)).toString();
            if(StringUtils.endsWith(returnValue, "E10") || StringUtils.endsWith(returnValue, ".0")) {
                returnValue = StringUtils.removeEnd(returnValue, ".0");
            }
        } catch (Exception e) {
            returnValue = ObjectUtils.toString(str);
        }
        return returnValue;
    }

    /**
     * @Title: createExcelTemplate
     * @Description: 生成Excel导入模板
     * @param @param filePath  Excel文件路径
     * @param @param handers   Excel列标题(数组)
     * @param @param downData  下拉框数据(数组)
     * @param @param downRows  下拉列的序号(数组,序号从0开始)
     * @return void
     * @throws
     */
    public static void createExcelTemplate(OutputStream out, String[] handers,
                                           List<String[]> downData, String[] downRows){

        XSSFWorkbook wb = new XSSFWorkbook();//创建工作薄

        //表头样式
        XSSFCellStyle style0 = wb.createCellStyle();
        style0.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style0.setWrapText(true);

        //红色星号字体样式
        XSSFFont fontStyle0 = wb.createFont();
        fontStyle0.setFontName("宋体");
        fontStyle0.setFontHeightInPoints((short)16);
        fontStyle0.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        fontStyle0.setColor(HSSFFont.COLOR_RED);
        fontStyle0.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        style0.setFont(fontStyle0);

        //表头样式
        XSSFCellStyle style1 = wb.createCellStyle();
        style1.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style1.setWrapText(true);

        //一级标题标题字体样式
        XSSFFont fontStyle1 = wb.createFont();
        fontStyle1.setFontName("宋体");
        fontStyle1.setFontHeightInPoints((short)16);
        fontStyle1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

        style1.setFont(fontStyle1);

        //表头样式
        XSSFCellStyle style2 = wb.createCellStyle();
        style2.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式
        style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style2.setWrapText(true);
        //二级标题字体样式
        XSSFFont fontStyle2 = wb.createFont();
        fontStyle2.setFontName("黑体");
        fontStyle2.setFontHeightInPoints((short)20);
        fontStyle2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style2.setFont(fontStyle2);

        //表头样式
        XSSFCellStyle style3 = wb.createCellStyle();
        style3.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式
        style3.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        style3.setWrapText(true);
        //三级标题字体样式
        XSSFFont fontStyle3 = wb.createFont();
        fontStyle3.setFontName("黑体");
        fontStyle3.setFontHeightInPoints((short)12);
        fontStyle3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style3.setFont(fontStyle3);

        //新建sheet
        XSSFSheet sheet1 = wb.createSheet("员工信息采集表");

        XSSFSheet sheet2 = wb.createSheet("参考数据");

        //合并从第rowFrom行columnFrom列
        CellRangeAddress region = new CellRangeAddress(0,0,0,9);
        sheet1.addMergedRegion(region);

        //合并从第rowFrom行columnFrom列
        CellRangeAddress region1 = new CellRangeAddress(1,1,0,9);
        sheet1.addMergedRegion(region1);

        XSSFRow rowFirst = sheet1.createRow(0);//第一个sheet的第一行
        rowFirst.setHeight((short)2800);
        XSSFCell cell1 = rowFirst.createCell(0);
        cell1.setCellValue("填写须知：\n    <1>Excel中带*为必填项,其他为选填项；\n"
                +"    <2>员工编号：员工在企业的唯一标识（不可重复）；\n"
                +"    <3>导入成功后，系统会自动向导入成功的员工手机号或邮箱发送登录密码；\n"
                +"    <4>证件号需要设置为文本格式；");
        cell1.setCellStyle(style1);

        Row rowSecond = sheet1.createRow(1);//第一个sheet的第二行
        rowSecond.setHeight((short)700);
        Cell cell2 = rowSecond.createCell(0);
        cell2.setCellValue("员工信息采集表");
        cell2.setCellStyle(style2);

        CellStyle css = wb.createCellStyle();

        DataFormat format = wb.createDataFormat();

        css.setDataFormat(format.getFormat("@"));

        sheet1.setDefaultColumnStyle(0,css);
        sheet1.setDefaultColumnStyle(1,css);
        sheet1.setDefaultColumnStyle(4,css);
        sheet1.setDefaultColumnStyle(6,css);
        sheet1.setDefaultColumnStyle(7,css);
        sheet1.setDefaultColumnStyle(9,css);


        //生成sheet1内容
        Row rowThird = sheet1.createRow(2);//第一个sheet的第三行为标题
        rowThird.setHeight((short)700);
        //写标题
        for(int i=0;i<handers.length;i++){
            Cell cell = rowThird.createCell(i); //获取第一行的每个单元格
            sheet1.setColumnWidth(i, 4000); //设置每列的列宽
            cell.setCellStyle(style3); //加样式
            cell.setCellValue(handers[i]); //往单元格里写数据

            String commont = null;
            if(i == 2){
                commont = "部门须与"+ Config.getString("companyName") +"商旅系统里面已经添加部门名称一致";
            }else if(i == 3){
                commont = "职级须与"+ Config.getString("companyName") +"商旅系统里面已经添加职级名称一致";
            }

            if(i == 2 || i ==3){

                CreationHelper factory = wb.getCreationHelper();
                Drawing drawing = sheet1.createDrawingPatriarch();
                ClientAnchor anchor = factory.createClientAnchor();

                Comment comment0 = drawing.createCellComment(anchor);
                RichTextString str0 = factory.createRichTextString(commont);
                comment0.setString(str0);
                comment0.setAuthor(Config.getString("companyName"));

                cell.setCellComment(comment0);


//                XSSFDrawing p=sheet1.createDrawingPatriarch();
//                //(int dx1, int dy1, int dx2, int dy2, short col1, int row1, short col2, int row2)
//                //前四个参数是坐标点,后四个参数是编辑和显示批注时的大小.
//                HSSFComment comment=p.createComment(new HSSFClientAnchor(0,0,0,0,(short)3,3,(short)5,6));
//                //输入批注信息
//                comment.setString(new HSSFRichTextString(commont));
//                //添加作者,选中B5单元格,看状态栏
//                comment.setAuthor(Config.getString("companyName"));
//                //将批注添加到单元格对象中
//                cell.setCellComment(comment);
            }

        }


        //设置下拉框数据
        String[] arr = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        int index = 0;
        Row row = null;
        for(int r=0;r < downRows.length;r++){
            String[] dlData = downData.get(r);//获取下拉对象
            int rownum = Integer.parseInt(downRows[r]);

            if(dlData.length == 0){
                continue;
            }


            if(dlData.length < 50 && rownum != 2){ //50以内的下拉
                //255以内的下拉,参数分别是：作用的sheet、下拉内容数组、起始行、终止行、起始列、终止列
                sheet1.addValidationData(setDataValidation(sheet1, dlData, 3, 50000, rownum ,rownum)); //超过255个报错
            } else { //255以上的下拉，即下拉列表元素很多的情况

                //1、设置有效性
                //String strFormula = "Sheet2!$A$1:$A$5000" ; //Sheet2第A1到A5000作为下拉列表来源数据
                String strFormula = "参考数据!$"+arr[index]+"$1:$"+arr[index]+"$5000"; //Sheet2第A1到A5000作为下拉列表来源数据
                sheet2.setColumnWidth(r, 4000); //设置每列的列宽
                //设置数据有效性加载在哪个单元格上,参数分别是：从sheet2获取A1到A5000作为一个下拉的数据、起始行、终止行、起始列、终止列
                sheet1.addValidationData(SetDataValidation(sheet1,strFormula, 3, 50000, rownum, rownum)); //下拉列表元素很多的情况

                //2、生成sheet2内容
                for(int j=0;j<dlData.length;j++){
                    if(index==0){ //第1个下拉选项，直接创建行、列
                        row = sheet2.createRow(j); //创建数据行
                        sheet2.setColumnWidth(j, 4000); //设置每列的列宽
                        row.createCell(0).setCellValue(dlData[j]); //设置对应单元格的值

                    } else { //非第1个下拉选项

                        int rowCount = sheet2.getLastRowNum();
                        //System.out.println("========== LastRowNum =========" + rowCount);
                        if(j<=rowCount){ //前面创建过的行，直接获取行，创建列
                            //获取行，创建列
                            sheet2.getRow(j).createCell(index).setCellValue(dlData[j]); //设置对应单元格的值

                        } else { //未创建过的行，直接创建行、创建列
                            sheet2.setColumnWidth(j, 4000); //设置每列的列宽
                            //创建行、创建列
                            sheet2.createRow(j).createCell(index).setCellValue(dlData[j]); //设置对应单元格的值
                        }
                    }
                }
                index++;
            }

        }

        try {
            wb.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @Title: SetDataValidation
     * @Description: 下拉列表元素很多的情况 (255以上的下拉)
     * @param @param strFormula
     * @param @param firstRow   起始行
     * @param @param endRow     终止行
     * @param @param firstCol   起始列
     * @param @param endCol     终止列
     * @param @return
     * @return HSSFDataValidation
     * @throws
     */
    private static DataValidation SetDataValidation(Sheet sheet ,String strFormula,
                                                    int firstRow, int endRow, int firstCol, int endCol) {


        DataValidationHelper helper = sheet.getDataValidationHelper();
        // 设置数据有效性加载在哪个单元格上。四个参数分别是：起始行、终止行、起始列、终止列
        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);
        DataValidationConstraint constraint = helper.createFormulaListConstraint(strFormula);

        DataValidation dataValidation = helper.createValidation(constraint, regions);


        return dataValidation;
    }


    /**
     *
     * @Title: setDataValidation
     * @Description: 下拉列表元素不多的情况(255以内的下拉)
     * @param @param sheet
     * @param @param textList
     * @param @param firstRow
     * @param @param endRow
     * @param @param firstCol
     * @param @param endCol
     * @param @return
     * @return DataValidation
     * @throws
     */
    private static DataValidation setDataValidation(Sheet sheet, String[] textList, int firstRow, int endRow, int firstCol, int endCol) {

        DataValidationHelper helper = sheet.getDataValidationHelper();
        //加载下拉列表内容
        DataValidationConstraint constraint = helper.createExplicitListConstraint(textList);
        //DVConstraint constraint = new DVConstraint();
        constraint.setExplicitListValues(textList);

        //设置数据有效性加载在哪个单元格上。四个参数分别是：起始行、终止行、起始列、终止列
        CellRangeAddressList regions = new CellRangeAddressList(firstRow, endRow, firstCol, endCol);

        //数据有效性对象
        DataValidation data_validation = helper.createValidation(constraint, regions);
        //DataValidation data_validation = new DataValidation(regions, constraint);

        return data_validation;
    }
}
