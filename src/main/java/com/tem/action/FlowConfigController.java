package com.tem.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.utils.JsonUtils;
import com.iplatform.common.web.Message;
import com.tem.platform.api.*;
import com.tem.platform.api.dto.*;
import com.tem.workflow.api.ProcessDefService;
import com.tem.workflow.dto.ProcessDefDto;
import com.tem.platform.Constants;
import com.tem.platform.action.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * 流程配置control
 * @author pyy
 *
 */
@Controller
@RequestMapping("flow-config")
public class FlowConfigController extends BaseController {
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private ProcessDefService processDefService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private OrgService orgService;
	
	@Autowired
	private OrgGradeService orgGradeService;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping("index.html")
	public String index(Model model){
		Long partnerId = super.getPartnerId();
		if(partnerId == null){
			partnerId = super.getCurrentUser().getPartnerId();
		}
		List<DictCodeDto> categorys = dictService.getPartnerCodes(partnerId, ConstansFlow.FLOW_CATEGORY);
		model.addAttribute("categorys", categorys);
		String categoryId = super.getStringValue("categoryId");
		if(StringUtils.isEmpty(categoryId) && categorys.size()>0){
			categoryId = categorys.get(0).getItemCode();
		}
		if(categoryId != null){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("category", categoryId);
			Integer pageIndex = super.getIntegerValue("pageIndex");
			Integer pageSize = super.getIntegerValue("pageSize");
			if(pageIndex == null){
				pageIndex = 1;
			}
			if(pageSize == null){
				pageSize = 20;
			}
			QueryDto<ProcessDefDto> queryDto=new QueryDto<ProcessDefDto>(pageIndex,pageSize);
			ProcessDefDto condition = new ProcessDefDto();
			condition.setPartnerId(partnerId);
			condition.setCategory(categoryId);
			queryDto.setCondition(condition);
			queryDto.setSidx("UPDATE_DATE");
			Page<ProcessDefDto> list = processDefService.queryListWithPage(queryDto);
			model.addAttribute("pageList", list);
		}
		model.addAttribute("categoryId", categoryId);
		return "flow/config/index.base";
	}

	@ResponseBody
	@RequestMapping("delete")
	public Message delete(){
		Long id = super.getLongValue("id");
		if(id != null && processDefService.deleteById(id)){
			return Message.success("删除成功");
		}
		return Message.error("删除失败");
	}

	@RequestMapping("detail")
	public String detail(Model model){
		Long partnerId = super.getPartnerId();
		if(partnerId == null){
			partnerId = super.getCurrentUser().getPartnerId();
		}
		List<DictCodeDto> orgTypes = dictService.getPartnerCodes(partnerId, Constants.ORG_TYPE);
		model.addAttribute("orgTypes", orgTypes);

		List<DictCodeDto> categorys = dictService.getPartnerCodes(partnerId, ConstansFlow.FLOW_CATEGORY);
		model.addAttribute("categorys", categorys);
		model.addAttribute("categoryId",super.getStringValue("categoryId"));
		Long defId = super.getLongValue("defId");
		if(defId != null){
			model.addAttribute("def",processDefService.getById(defId));
		}
		return "flow/config/detail.base";
	}

	@ResponseBody
	@RequestMapping("getProDef")
	public ProcessDefDto getProDef(Model model){
		Long defId = super.getLongValue("defId");
		if(defId != null){
			ProcessDefDto def = processDefService.getProcDefWithNodesById(defId);
			return def;
		}
		return null;
	}

	@ResponseBody
	@RequestMapping("checkCode")
	public boolean checkCode(){
		String code = super.getStringValue("code");
		if(StringUtils.isEmpty(code)){
			return false;
		}
		return processDefService.checkProcKey(code);
	}

	@ResponseBody
	@RequestMapping("save")
	public String save(String processDef,String type){
		ProcessDefDto pd = JsonUtils.getGson().fromJson(processDef, ProcessDefDto.class);
		Long partnerId = super.getPartnerId();
		if(partnerId == null){
			partnerId = super.getCurrentUser().getPartnerId();
		}
		pd.setPartnerId(partnerId);
		pd.setUpdateDate(new Date());
		pd.setUpdateUser(super.getCurrentUser().getId());
		if("2".equals(type)){
			pd = processDefService.saveAndDeploy(pd);
		}else{
			pd = processDefService.save(pd);
		}
		return pd.getId() + "";
	}


	@ResponseBody
	@RequestMapping("loadObjNames")
	public Map<String, Map<Object, String>> loadObjNames(String userIds,String orgIds,String roleIds,String orggs,String orgTypes){
		Map<String, Map<Object, String>> result = new HashMap<String, Map<Object,String>>();
		//用户集合
		Map<Object, String> userMap = new HashMap<Object, String>();
		if(!StringUtils.isEmpty(userIds)){
			String[] us = userIds.split(",");
			List<Long> userIdsArray = new ArrayList<Long>();
			for (int i = 0; i < us.length; i++) {
				userIdsArray.add(Long.valueOf(us[i]));
			}
			List<UserDto> users = userService.getUsers(userIdsArray);
			for (int i = 0; i < users.size(); i++) {
				UserDto u = users.get(i);
				userMap.put(u.getId(), u.getFullname());
			}
		}
		result.put("users", userMap);

		//单位集合
		Map<Object, String> orgMap = new HashMap<Object, String>();
		if(!StringUtils.isEmpty(orgIds)){
			List<Long> orgIdsArray = new ArrayList<Long>();
			String[] us = orgIds.split(",");
			for (int i = 0; i < us.length; i++) {
				orgIdsArray.add(Long.valueOf(us[i]));
			}
			List<OrgDto> orgs = orgService.getOrgByIds(orgIdsArray);
			for (int i = 0; i < orgs.size(); i++) {
				OrgDto org = orgs.get(i);
				orgMap.put(org.getId(), org.getName());
			}
		}
		result.put("orgs", orgMap);

		//角色集合
		Map<Object, String> roleMap = new HashMap<Object, String>();
		if(!StringUtils.isEmpty(roleIds)){
			String[] us = roleIds.split(",");
			for (int i = 0; i < us.length; i++) {
				Long uid = Long.valueOf(us[i]);
				RoleDto role = roleService.getRole(uid);
				if(role != null){
					roleMap.put(role.getId(), role.getName());
				}
			}
		}
		result.put("roles", roleMap);

		Long partner = super.getPartnerId();
		//组织层面集合
		Map<Object, String> orggMap = new HashMap<Object, String>();
		if(!StringUtils.isEmpty(orggs)){
			String[] us = orggs.split(",");
			for (int i = 0; i < us.length; i++) {
				OrgGradeDto dto = orgGradeService.findByCode(partner, us[i]);
				if(dto != null){
					orggMap.put(dto.getCode(), dto.getName());
				}
			}
		}
		result.put("orggs", orggMap);

		//组织类型集合
		Map<Object, String> orgTypeMap = new HashMap<Object, String>();
		if(!StringUtils.isEmpty(orgTypes)){
			String[] us = orgTypes.split(",");
			for (int i = 0; i < us.length; i++) {
				String orgTypeName = this.dictService.getPartnerCodeTxt(partner, ConstansFlow.DICT_ORG_TYPE, us[i]);
				if(orgTypeName != null){
					orgTypeMap.put(us[i], orgTypeName);
				}
			}
		}
		result.put("orgTypes", orgTypeMap);
		
		return result;
	}
}
