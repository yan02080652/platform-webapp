package com.tem.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.web.Message;
import com.tem.hotel.api.hotel.HotelMateRuleService;
import com.tem.hotel.dto.hotel.HotelMateRuleDto;
import com.tem.hotel.dto.hotel.enums.MateType;
import com.tem.platform.action.BaseController;

/**
 * 在途酒店自有数据整合Controller
 * @author apple
 */
@Controller
@RequestMapping("hotel/ownData")
public class HotelOwnDataController  extends BaseController{
	@Autowired
	private HotelMateRuleService  hotelMateRuleService;
	
	@RequestMapping("/index")
	public String Index(Model model) {
		return "hotel/ownData/index.base";
	}
	
	@RequestMapping("/mateRule")
	public String mateRule(Model model) {
		model.addAttribute("mateTypes",MateType.values());
		return "hotel/ownData/mateRule.jsp";
	}

	@RequestMapping("/addMateRule")
	public String  addMateRule(Model model,String mateType){
		HotelMateRuleDto hotelMateRuleDto= new HotelMateRuleDto();
		hotelMateRuleDto.setMateType(MateType.getByValue(mateType));
		model.addAttribute("hotelMateRuleDto", hotelMateRuleDto);
		return "hotel/ownData/addMateRule.jsp";
	}
	@RequestMapping("/saveMateRule")
	@ResponseBody
	public Message saveMateRule(HotelMateRuleDto  hotelMateRuleDto) {
		ResponseInfo responseInfo = hotelMateRuleService.insertOrUpdateById(hotelMateRuleDto);
		if(responseInfo.isSuccess()) {
			return Message.success("操作成功");
		}else {
			return Message.error("操作失败");
		}
	}
	
	@RequestMapping("/deleteMate")
	@ResponseBody
	public Message deleteMate(Integer id) {
		ResponseInfo responseInfo = hotelMateRuleService.deleteById(id);
		if(responseInfo.isSuccess()) {
			return Message.success("解除成功");
		}else {
			return Message.error("解除失败");
		}
	}
	

	
	@RequestMapping("/searchMate")
	 public String searchMate(Model model, Integer pageIndex, Integer pageSize,String keyWords,String mateType) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<HotelMateRuleDto>  queryDto = new QueryDto<HotelMateRuleDto>(pageIndex,pageSize);
        HotelMateRuleDto condition = new HotelMateRuleDto();
        condition.setKeyWords(keyWords);
        condition.setMateType(MateType.getByValue(mateType));
        queryDto.setCondition(condition);
        ResponseInfo  result = hotelMateRuleService.getListByCondition(queryDto);
        model.addAttribute("pageList",result.getData());
        return "hotel/ownData/mateRuleList.jsp";
    }
}
