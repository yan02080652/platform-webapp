package com.tem.action;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.ConstantCommon;
import com.tem.hotel.HbpRtnCodeConst;
import com.tem.hotel.api.HotelHubService;
import com.tem.hotel.api.HotelOrderQueryHubService;
import com.tem.hotel.api.HotelOrderService;
import com.tem.hotel.api.HotelSmsService;
import com.tem.hotel.api.hotel.HotelHubConnectorService;
import com.tem.hotel.api.order.HotelRefundService;
import com.tem.hotel.api.order.HotelReverseService;
import com.tem.hotel.dto.hotel.enums.ElongOrderStatus;
import com.tem.hotel.dto.hotel.enums.ElongShowStatus;
import com.tem.hotel.dto.hub.HotelOrderDetailQuery;
import com.tem.hotel.dto.order.*;
import com.tem.hotel.dto.order.enums.*;
import com.tem.hotel.query.HotelOrderCancelQuery;
import com.tem.payment.api.BpAccTradeRecordService;
import com.tem.payment.dto.TradeRecordInfo;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.UserBaseInfo;
import com.tem.platform.security.authorize.User;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.task.OrderTaskService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.task.*;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import com.tem.pss.enums.TicketTypeEnum;
import com.tem.supplier.api.SettleChannelDetailService;
import com.tem.supplier.dto.SettleChannelDetailDto;
import com.tem.supplier.enums.ChannelDetailTradeType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.support.ResourceTransactionManager;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * 酒店 tmc 任务台
 *
 * @author heyuanjing
 *
 */
@Controller
@RequestMapping("hotel/tmc")
public class HotelTmcController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(HotelTmcController.class);
	@Autowired
	private OrderTaskService orderTaskService;
	@Autowired
	private HotelOrderService hotelOrderService;
	@Autowired
	private HotelHubService hotelHubService;
	@Autowired
	private HotelRefundService hotelRefundService;
	@Autowired
	private UserService userService;
	@Autowired
	private HotelOrderQueryHubService hotelOrderQueryHubService;
	@Autowired
	private BpAccTradeRecordService bpAccTradeRecordService;
	@Autowired
	private IssueChannelService issueChannelService;
	@Autowired
	private HotelReverseService hotelReverseService;
	@Autowired
	private HotelSmsService hotelSmsService;
	@Autowired
	private HotelHubConnectorService hotelHubConnectorService;
	@Autowired
	private SettleChannelDetailService settleChannelDetailService;
	/**
	 * 首页
	 *
	 * @return
	 */
	@RequestMapping("index")
	public String index() {
		return "hotel/tmc/index.jsp";
	}

	/**
	 * 我的任务 tab
	 * @return
	 */
	@RequestMapping(value = "getMyTaskTable" , method = RequestMethod.POST)
	public String getMyTaskTable(OrderTaskCondition condition,Model model,Integer pageIndex){
		if(pageIndex == null){
			pageIndex = 1;
		}
		String activeTaskStatus = condition.getTaskStatus();

		QueryDto<OrderTaskCondition> queryDto = new QueryDto<>(pageIndex,10);
		condition.setOperatorId(super.getCurrentUser().getId().intValue());
		condition.setOrderBizType(OrderBizType.HOTEL.name());
		condition.setTmcId(String.valueOf(super.getTmcId()));
		queryDto.setCondition(condition);

		Page<OrderTaskDto> pageList = hotelOrderService.orderTaskPageListQuery(queryDto).getData();

//		//处理中任务数量（包含各状态的数量）
		condition.setTaskStatus(TaskStatus.PROCESSING.name());
		Map<String, Integer> typeMap = orderTaskService.getTaskCountMap(condition);
//
		//已处理
		condition.setTaskStatus(TaskStatus.ALREADY_PROCESSED.name());
		Map<String, Integer> alreadyMap = orderTaskService.getTaskCountMap(condition);

//		//待处理任务数量（包含各状态的数量）
		condition.setTaskStatus(TaskStatus.WAIT_PROCESS.name());
		Map<String, Integer> waitTaskTypeMap = orderTaskService.getTaskCountMap(condition);

		model.addAttribute("pageList", pageList);
		model.addAttribute("typeMap", typeMap);
		model.addAttribute("alreadyMap", alreadyMap);
		model.addAttribute("waitTaskTypeMap", waitTaskTypeMap);
		model.addAttribute("taskStatus", activeTaskStatus);
		return "hotel/tmc/myTask.jsp";
	}


	//催单一次
	@RequestMapping(value = "urgeDone",method= RequestMethod.POST)
	@ResponseBody
	public boolean urgeDone(String orderId,String taskId,String remark){
		OrderTaskDto task = new OrderTaskDto();
		task.setId(Integer.valueOf(taskId));
		task.setTaskType(TaskType.WAIT_URGE);
		task.setResult("客服已打电话催单,等待结果");
		task.setTaskStatus(TaskStatus.PROCESSING);
		task.setUpdateDate(Calendar.getInstance().getTime());
		task.setOperatorId(getCurrentUser().getId().intValue());
		task.setOperatorName(getCurrentUser().getFullname());
		task.setTmcId(String.valueOf(super.getTmcId()));
		orderTaskService.update(task);
		//催单一次添加日志
		hotelOrderService.saveOperationLog(new OrderOperationLogDto(orderId, OrderOperationType.REMINDER_ORDER, getCurrentUser().getFullname(), OrderOperationStatus.SUCCESS, "客服催单一次,信息："+remark));

		return true;
	}

	//确认
	@RequestMapping("confirm")
	@ResponseBody
	public ResponseInfo confirm(String orderId,String taskId){
		HotelOrderDto orderDto = hotelOrderService.getOrderById(orderId).getData();
		orderDto.setOrderShowStatus(OrderShowStatus.CONFIRMED);
		orderDto.setOrderStatus(OrderStatus.CONFIRMED);
		ResponseInfo response = hotelOrderService.updateOrder(orderDto);
		if(response.isSuccess()){
			OrderTaskDto task = new OrderTaskDto();
			task.setId(Integer.valueOf(taskId));
			task.setTaskType(TaskType.WAIT_CONFIRM);
			task.setResult("客服已确认");
			task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
			task.setUpdateDate(Calendar.getInstance().getTime());
			task.setCompleteDate(Calendar.getInstance().getTime());
			task.setOperatorId(getCurrentUser().getId().intValue());
			task.setOperatorName(getCurrentUser().getFullname());
			task.setTmcId(String.valueOf(super.getTmcId()));
			orderTaskService.update(task);
			//催单一次添加日志
			hotelOrderService.saveOperationLog(new OrderOperationLogDto(orderId, OrderOperationType.HOTEL_CONFIRMED, getCurrentUser().getFullname(), OrderOperationStatus.SUCCESS, "客服已确认酒店留房"));
			//发送确认短信
			hotelSmsService.confirmSms(orderDto);
		}
		return response;
	}

	//已处理
	@RequestMapping(value = "handled",method= RequestMethod.POST)
	@ResponseBody
	public boolean handled(String orderId,String taskId,String remark){
//		OrderTaskDto task = new OrderTaskDto();
//		task.setId(Integer.valueOf(taskId));
//		task.setTaskType(TaskType.CONFIRMING);
//		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
//		task.setResult("客服已打电话催单,处理完成。信息："+remark);
//		task.setUpdateDate(Calendar.getInstance().getTime());
//		task.setOperatorId(getCurrentUser().getId().intValue());
//		task.setOperatorName(getCurrentUser().getFullname());
//		task.setTmcId(String.valueOf(super.getTmcId()));
//		orderTaskService.update(task);

		//催单完成添加日志
		hotelOrderService.saveOperationLog(new OrderOperationLogDto(orderId, OrderOperationType.REMINDER_ORDER, getCurrentUser().getFullname(), OrderOperationStatus.SUCCESS, "客服催单完成,结果："+remark));
		return true;
	}

	//已完毕
	@RequestMapping(value = "confirmDone",method= RequestMethod.POST)
	@ResponseBody
	public boolean confirmDone(String orderId,String taskId){
		OrderTaskDto task = new OrderTaskDto();
		task.setId(Integer.valueOf(taskId));
		task.setCompleteDate(Calendar.getInstance().getTime());
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		task.setTaskType(TaskType.WAIT_URGE);
		task.setResult("客服已确认");
		task.setUpdateDate(Calendar.getInstance().getTime());
		task.setOperatorId(getCurrentUser().getId().intValue());
		task.setOperatorName(getCurrentUser().getFullname());
		task.setTmcId(String.valueOf(super.getTmcId()));
		orderTaskService.update(task);
		return true;
	}

	//再确认完毕
	@RequestMapping(value = "confirmAgainDone",method= RequestMethod.POST)
	@ResponseBody
	public boolean confirmAgainDone(String orderId,String taskId){
		OrderTaskDto task = new OrderTaskDto();
		task.setId(Integer.valueOf(taskId));
		task.setCompleteDate(Calendar.getInstance().getTime());
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		//task.setTaskType(TaskType.CONFIRM_AGAIN);
		task.setResult("客服已二次确认");
		task.setUpdateDate(Calendar.getInstance().getTime());
		task.setOperatorId(getCurrentUser().getId().intValue());
		task.setOperatorName(getCurrentUser().getFullname());
		task.setTmcId(String.valueOf(super.getTmcId()));
		orderTaskService.update(task);

		hotelOrderService.saveOperationLog(new OrderOperationLogDto(orderId, OrderOperationType.CONFIRM_AGAIN, getCurrentUser().getFullname(), OrderOperationStatus.SUCCESS, "客服二次确认完成"));
		return true;
	}



	//领取任务
	@ResponseBody
	@RequestMapping("/receiveTask")
	public boolean receiveTask(@RequestParam("taskType[]")List<String>	taskType){
		User user = super.getCurrentUser();
		TaskParamDto dto = new TaskParamDto();
		dto.setUserId(user.getId().intValue());
		dto.setUserName(user.getFullname());
		dto.setTaskType(taskType);
		dto.setOrderBizType(OrderBizType.HOTEL.name());
		dto.setTmcId(String.valueOf(super.getTmcId()));

		boolean rs = orderTaskService.receiveTask(dto);

		return rs;
	}

	//任务交接
	@ResponseBody
	@RequestMapping("/transfer")
	public boolean transfer(Integer taskId,Integer operatorId,Integer userId,String userName){
		TaskParamDto dto = new TaskParamDto();
		dto.setTaskId(taskId);
		dto.setOperatorId(operatorId == null ? getCurrentUser().getId().intValue() : operatorId);
		dto.setUserId(userId);
		dto.setUserName(userName);
		dto.setTaskStatus(TaskStatus.PROCESSING);
		dto.setRemork(super.getCurrentUser().getFullname()+"("+operatorId+")将任务交接给"+userName+"("+userId+"),任务id:"+taskId);
		dto.setOrderBizType(OrderBizType.HOTEL.name());
		boolean rs = orderTaskService.saveTaskAndLog(dto);

		return rs;
	}

	/**
	 * 补单弹窗
	 * @param orderId
	 * @param taskId
	 * @param flag
	 * @param model
     * @return
     */
	@RequestMapping("/handRepair")
	public String handRepair(String orderId,String taskId,boolean flag, Model model){
		HotelOrderDetailQuery query = new HotelOrderDetailQuery();
		query.setOrderId(orderId);
		HotelOrderDto order = hotelOrderQueryHubService.getOrderDetail(query).getData();
//		HotelOrderDto order = hotelOrderService.getOrderDetail(orderId).getData();
		model.addAttribute("hotelOrderDto",order);
		List<IssueChannelDto> channel = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
				IssueChannelType.HOTEL.getType(), LineType.OFF_LINE.getType());

		model.addAttribute("dictCodes",channel);
		model.addAttribute("taskId",taskId);
		if(flag){
			return "hotel/tmc/repair2.jsp";
		}
		//查询是否已产生过再确认任务
		boolean againExist = orderTaskService.findIsExistTaskByOrderIdAndTaskType(orderId,TaskType.CONFIRM_AGAIN);
		model.addAttribute("againExist",againExist);
		return "hotel/tmc/repair.jsp";
	}

	/**
	 * 退款审核弹窗
	 * @return
     */
	@RequestMapping("reviewRefund")
	public String reviewRefund(String orderId,String taskId,Model model){
		HotelOrderDto order = hotelOrderService.getOrderDetail(orderId).getData();
		model.addAttribute("order",order);
		model.addAttribute("taskId",taskId);
		return "hotel/tmc/reviewRefund.jsp";
	}

	/**
	 * 退款审核通过
	 * @param orderId
	 * @param orderNote
	 * @param refundAmount
     */
	@RequestMapping("reviewPass")
	@ResponseBody
	public ResponseInfo reviewPass(String orderId, String orderNote, BigDecimal refundAmount,String taskId,boolean allowCancel){
		HotelOrderDto order = hotelOrderService.getOrderById(orderId).getData();
		ResponseInfo responseInfo = null;
		if(allowCancel){
			order.setOrderNote(orderNote);
			order.setRefundAmount(refundAmount);
			RefundDto refund = new RefundDto();
			refund.setOrderId(orderId);
			refund.setPartCacnel(false);
			refund.setRefundReason("线下订单,退款审核通过");
			refund.setRefundReasonType(RefundReasonType.SYS_REFUND);
			responseInfo = hotelRefundService.refund(refund,order);
		}else{
			order.setOrderStatus(OrderStatus.CONFIRMED);
			order.setOrderShowStatus(OrderShowStatus.CONFIRMED);
			hotelOrderService.updateOrder(order);
			responseInfo = hotelSmsService.sendRefuseRefund(order);
		}
		if(HbpRtnCodeConst.SUCCESS.equals(responseInfo.getCode())){
			OrderTaskDto task = new OrderTaskDto();
			task.setId(Integer.valueOf(taskId));
			task.setCompleteDate(Calendar.getInstance().getTime());
			task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
			if(allowCancel){
				createSupplierTransactionRecord(order,1);
				task.setResult("客服退款审核通过");
			}else{
				task.setResult("客服退款审核拒绝");
			}
			task.setUpdateDate(Calendar.getInstance().getTime());
			task.setOperatorId(getCurrentUser().getId().intValue());
			task.setOperatorName(getCurrentUser().getFullname());
			task.setTmcId(String.valueOf(super.getTmcId()));
			orderTaskService.update(task);

			hotelOrderService.saveOperationLog(new OrderOperationLogDto(orderId, OrderOperationType.REVIRE_REFUND, getCurrentUser().getFullname(), OrderOperationStatus.SUCCESS, task.getResult()));

		}
		return responseInfo;
	}

	@RequestMapping("/showLineData")
	public String showLineData(String orderId,String taskId, Model model){

		HotelOrderDto order = hotelOrderService.getOrderDetail(orderId).getData();

		List<IssueChannelDto> channel = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
				IssueChannelType.HOTEL.getType(), LineType.OFF_LINE.getType());

		model.addAttribute("hotelOrderDto",order);
		model.addAttribute("dictCodes",channel);
		model.addAttribute("taskId",taskId);

		return "hotel/tmc/lineData.jsp";
	}

	@RequestMapping("editLineData")
	@ResponseBody
	public String editLineData(HotelOrderDto hotelOrderDto,String confirmNum,String taskId){
		//原订单相关数据
		HotelOrderDto order = hotelOrderService.getOrderDetail(hotelOrderDto.getId()).getData();
		//添加日志
		OrderOperationLogDto log = new OrderOperationLogDto();
		log.setOperator(super.getCurrentUser().getFullname());
		log.setOrderId(hotelOrderDto.getId());
		log.setOperationDate(new Date());
		log.setType(OrderOperationType.UPDATE_LINE_ORDER);
		boolean hasModify = false;
		StringBuilder sb = new StringBuilder();
		try {
			if (!order.getIssueChannel().equals(hotelOrderDto.getIssueChannel())) {
				IssueChannelDto editChannel = issueChannelService.findOffLineChannelByCode(hotelOrderDto.getIssueChannel(), String.valueOf(super.getTmcId()), IssueChannelType.HOTEL.name());
				IssueChannelDto originChannel = issueChannelService.findOffLineChannelByCode(order.getIssueChannel(), String.valueOf(super.getTmcId()), IssueChannelType.HOTEL.name());
				sb.append("供应商： " + originChannel.getName() + " → " + editChannel.getName()).append("<br/>");
				order.setIssueChannel(hotelOrderDto.getIssueChannel());
				hasModify = true;
			}
			if (!order.getExternalOrderId().equals(hotelOrderDto.getExternalOrderId())) {
				sb.append("供应商单号： " + order.getExternalOrderId() + " → " + hotelOrderDto.getExternalOrderId()).append("<br/>");
				order.setExternalOrderId(hotelOrderDto.getExternalOrderId());
				hasModify = true;
			}
			if (StringUtils.isNotBlank(confirmNum) && !confirmNum.equals(order.getCustomers().get(0).getConfirmNum())) {
				sb.append("酒店确认号： " + order.getCustomers().get(0).getConfirmNum() + " → " + confirmNum).append("<br/>");
				for (CustomerDto customer : order.getCustomers()) {
					customer.setConfirmNum(confirmNum);
				}
				hasModify = true;
			}
			if (StringUtils.isNotBlank(hotelOrderDto.getPrepayNote()) && !hotelOrderDto.getPrepayNote().equals(order.getPrepayNote())) {
				sb.append("退改规则： " + order.getPrepayNote() + " → " + hotelOrderDto.getPrepayNote()).append("<br/>");
				order.setPrepayNote(hotelOrderDto.getPrepayNote());
				hasModify = true;
			}
			if (StringUtils.isNotBlank(hotelOrderDto.getOrderNote()) && !hotelOrderDto.getOrderNote().equals(order.getOrderNote())) {
				sb.append("备注： " + order.getOrderNote() + " → " + hotelOrderDto.getOrderNote()).append("<br/>");
				order.setOrderNote(hotelOrderDto.getOrderNote());
				hasModify = true;
			}

			BigDecimal totalCost = new BigDecimal("0");
			DecimalFormat df = new DecimalFormat("#0.00");
			for (OrderNightlyRateDto nightlyRate : hotelOrderDto.getNightlyRates()) {
				totalCost = totalCost.add(nightlyRate.getDayRate());
				for(OrderNightlyRateDto rateDto : order.getNightlyRates()) {
					if(!rateDto.getId().equals(nightlyRate.getId())){
						continue;
					}
					if(!nightlyRate.getDayRate().equals(rateDto.getCost())){
						hasModify = true;
					}
					rateDto.setCost(nightlyRate.getDayRate());
				}
			}

			if (totalCost.doubleValue() != order.getTotalCost().doubleValue()) {
				sb.append("供应商结算价： " + order.getTotalCost() + " → " + df.format(totalCost) ).append("<br/>");
				order.setTotalCost(totalCost);
				hasModify = true;
			}

			log.setDescription(sb.toString());
			log.setStatus(OrderOperationStatus.SUCCESS);
			if(hasModify) {
				hotelOrderService.updateOrder(order);
				hotelOrderService.saveOperationLog(log);
				return ConstantCommon.SUCCESS;
			}
			return ConstantCommon.NO_CHANGE;
		}catch (Exception e) {
			e.printStackTrace();
			return ConstantCommon.FAIL;
		}


	}


	/**
	 * 手动补单
	 * @param hotelOrderDto
	 */
	@RequestMapping("/repair")
	@ResponseBody
	public boolean repair(HotelOrderDto hotelOrderDto,String confirmNum,String taskId,boolean needAgain){
		BigDecimal totalCost = new BigDecimal("0");
		HotelOrderDto order = hotelOrderService.getOrderDetail(hotelOrderDto.getId()).getData();

		for(OrderNightlyRateDto nightlyRate : hotelOrderDto.getNightlyRates()){
			totalCost = totalCost.add(nightlyRate.getDayRate());
			for(OrderNightlyRateDto rateDto : order.getNightlyRates()) {
				if(!rateDto.getId().equals(nightlyRate.getId())){
					continue;
				}
				rateDto.setCost(nightlyRate.getDayRate());
			}
		}

		order.setTotalCost(totalCost);
		order.setIssueChannel(hotelOrderDto.getIssueChannel());
		order.setExternalOrderId(hotelOrderDto.getExternalOrderId());
		order.setPrepayNote(hotelOrderDto.getPrepayNote());
		order.setOrderNote(hotelOrderDto.getOrderNote());
		order.setIssueChannelMsg(hotelOrderDto.getIssueChannelMsg());
		if(StringUtils.isNotBlank(confirmNum)){
			for(CustomerDto customer : order.getCustomers()){
				customer.setConfirmNum(confirmNum);
			}
		}
		order.setOrderShowStatus(OrderShowStatus.CONFIRMED);
		order.setOrderStatus(OrderStatus.CONFIRMED);
		order.setIssuedDate(new Date());

		OrderTaskDto task = new OrderTaskDto();
		task.setId(Integer.valueOf(taskId));
		task.setCompleteDate(Calendar.getInstance().getTime());
		task.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		task.setTaskType(TaskType.WAIT_TICKET);
		task.setResult("客服已补单");
		task.setOperatorId(getCurrentUser().getId().intValue());
		task.setOperatorName(getCurrentUser().getFullname());
		//线下补单标识
		task.setTicketType(TicketTypeEnum.OFFLINE);

		ResponseInfo rtn = hotelOrderService.offlineDealOrder(false, order, task);
		if (HbpRtnCodeConst.SUCCESS.equals(rtn.getCode())) {
			if(needAgain){
				OrderTaskDto taskDto = new OrderTaskDto(OrderBizType.HOTEL, TaskStatus.WAIT_PROCESS,
						TaskType.CONFIRM_AGAIN, order.getId(), DateUtils.addMinute(new Date(), 5),
						order.getTmcId(), Long.valueOf(order.getCId()), order.getIsVip());
				try{
					orderTaskService.createTask(taskDto);
				} catch (Exception e) {
					LogUtils.error(logger, "创建再次确认任务任务,入参:{},异常信息{}", taskDto, e);
				}
			}
			createSupplierTransactionRecord(order,-1);
			return true;
		}

		return false;
	}
	/**
	 * 供货商交易记录创建
	 * @param hotelOrderDto
	 */
	private void createSupplierTransactionRecord(HotelOrderDto hotelOrderDto,Integer direction){
		Long tmcId=Long.valueOf(hotelOrderDto.getTmcId());
		if(!BigDecimal.ZERO.equals(hotelOrderDto.getTotalCost())) {
			SettleChannelDetailDto dto = new SettleChannelDetailDto();
			dto.setTmcId(tmcId);
			IssueChannelDto issueChannelDto=issueChannelService.findOffLineChannelByCode(hotelOrderDto.getIssueChannel(),tmcId.toString(),IssueChannelType.HOTEL.name());
			if(issueChannelDto!=null){
				dto.setChannelId(issueChannelDto.getSettelChannelId());//收支通道
				dto.setSupplyId(Long.valueOf(issueChannelDto.getId()));
				dto.setTradeTime(new Date());
				dto.setCreateDate(new Date());
				dto.setTradeType(ChannelDetailTradeType.ORDER.name());//交易类型
				dto.setTemRefRecord(hotelOrderDto.getId());//tem订单号/账单号
				dto.setSupplyRefRecord(hotelOrderDto.getExternalOrderId());//供应商订单号/票号等
				dto.setDescr(hotelOrderDto.getSummary());//订单描述
				dto.setAmount(hotelOrderDto.getTotalCost().abs().multiply(new BigDecimal(100)).intValue()*direction);
				settleChannelDetailService.save(dto);
			}

		}
	}
	/**
	 * 放弃补单
	 * @param orderId
	 * @return
	 */
	@RequestMapping("/cancelRepair")
	@ResponseBody
	public JSONObject cancelRepair(String orderId, Integer taskId){
		JSONObject json = new JSONObject();

		HotelOrderDto orderDto = hotelOrderService.getOrderById(orderId).getData();

		OrderCancelDto query = new OrderCancelDto();
		query.setTrOrderId(orderDto.getId());
		query.setOrderId(orderDto.getExternalOrderId());
		query.setReason("其他");
		query.setCancelCode("其他");
		hotelHubService.orderCancel(query);


		OrderTaskDto orderTaskDto = new OrderTaskDto();
		orderTaskDto.setId(taskId);
		orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		orderTaskDto.setCompleteDate(new Date());
		orderTaskDto.setResult("客服拒单退款");
		orderTaskDto.setCompleteDate(Calendar.getInstance().getTime());
		orderTaskDto.setOperatorId(getCurrentUser().getId().intValue());
		orderTaskDto.setOperatorName(getCurrentUser().getFullname());

		ResponseInfo responseInfo = hotelOrderService.offlineDealOrder(true, orderDto, orderTaskDto);
		json.put("result",responseInfo.isSuccess());
		json.put("msg",responseInfo.getMsg());
		return json;
	}

	//获取订单日志
	@RequestMapping("/getOrderLog")
	public String getOrderLog(Model model,String orderId){

		List<OrderOperationLogDto> logList = hotelOrderService.findOrderLogList(orderId).getData();

		model.addAttribute("logList", logList);

		return "hotel/tmc/log.single";
	}


	//加载收入明细
	@RequestMapping("/getIncomeDetail")
	public String getIncomeDetail(Model model, String orderId) {
		HotelOrderDto orderDto = hotelOrderService.getOrderById(orderId).getData();
		model.addAttribute("incomeDetail",
				bpAccTradeRecordService.listByOrderIdAndPlanId(orderDto.getId(), Long.valueOf(orderDto.getPaymentPlanNo())));
		return "hotel/tmc/incomeDetail.single";
	}

	//全部任务
	@RequestMapping("/allTask")
	public String allTask(OrderTaskCondition condition,Model model ,Integer pageIndex){
		if(pageIndex==null){
			pageIndex=1;
		}
		String activeTaskStatus = condition.getTaskStatus();


		QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex,10);
		condition.setOrderBizType(OrderBizType.HOTEL.name());
		condition.setTmcId(String.valueOf(super.getTmcId()));
		queryDto.setCondition(condition);
		Page<OrderTaskDto> pageList = hotelOrderService.orderTaskPageListQuery(queryDto).getData();

//		//处理中任务数量（包含各状态的数量）
		condition.setTaskStatus(TaskStatus.PROCESSING.name());
		Map<String, Integer> typeMap = orderTaskService.getTaskCountMap(condition);
//
		//已处理
		condition.setTaskStatus(TaskStatus.ALREADY_PROCESSED.name());
		Map<String, Integer> alreadyMap = orderTaskService.getTaskCountMap(condition);

		//待处理任务数量（包含各状态的数量）
		condition.setTaskStatus(TaskStatus.WAIT_PROCESS.name());
		Map<String, Integer> waitTaskTypeMap = orderTaskService.getTaskCountMap(condition);

		model.addAttribute("pageList", pageList);
		model.addAttribute("typeMap", typeMap);
		model.addAttribute("alreadyMap", alreadyMap);
		model.addAttribute("waitTaskTypeMap", waitTaskTypeMap);
		model.addAttribute("taskStatus",activeTaskStatus);
		return "hotel/tmc/allTask.jsp";
	}

	//订单查询页面
	@RequestMapping("/orderList")
	public String orderList(){

		return "hotel/tmc/orderList.jsp";
	}

	//用户查询
	@RequestMapping("/userList")
	public String userList(Model model,String keyword){

		List<UserBaseInfo> userList = userService.findUserBaseInfo(super.getTmcId(),null, keyword);
		model.addAttribute("userList",userList);
		return "hotel/tmc/userList.jsp";
	}

	@RequestMapping(value = "orderTable",method = RequestMethod.POST)
	public String orderTable(HotelOrderCondition condition,Integer pageIndex,Integer pageSize,Model model){
		String currentOrderStatus = condition.getOrderStatus() != null ? condition.getOrderStatus() : null;
		condition.setTmcId(String.valueOf(super.getTmcId()));

		Page<HotelOrderDto> pageList = getPageList(pageIndex,pageSize,condition);
		condition.setOrderStatus(null);

		QueryDto<HotelOrderCondition> queryDto = new QueryDto<>();
		queryDto.setCondition(condition);
		model.addAttribute("orderStatuslist", getOrderCountList(queryDto));
		model.addAttribute("pageList", pageList);
		model.addAttribute("currentOrderStatus",currentOrderStatus);

		//任务
		List<OrderTaskDto> inOrWaitTaskList = orderTaskService.findTaskByStatusAndBizType(TaskStatus.WAIT_PROCESS, OrderBizType.HOTEL);
		inOrWaitTaskList.addAll(orderTaskService.findTaskByStatusAndBizType(TaskStatus.PROCESSING, OrderBizType.HOTEL));
		if (CollectionUtils.isNotEmpty(pageList.getList())) {
			for (HotelOrderDto h: pageList.getList()) {
				for (OrderTaskDto t: inOrWaitTaskList) {
					if (h.getId().equals(t.getOrderId())) {
						h.setOrderTaskDto(t);
						String operatorName = t.getOperatorName() == null ? "" : "/"+t.getOperatorName();
						h.setTaskInfo( t.getTaskStatus().getMessage() + "/" + t.getTaskType().getMessage() + operatorName);
						break;
					}
				}
			}
		}

		return "hotel/tmc/orderTable.jsp";
	}

	/**
	 * 刷新
	 * @param orderId
	 * @param thirdId
	 * @return
	 */
	@RequestMapping(value = "refreashOrderDetail" ,method = RequestMethod.POST)
	@ResponseBody
	public JSONObject refreashOrderDetail(String orderId,String thirdId){
		JSONObject json = new JSONObject();
		HotelOrderDetailQuery query = new HotelOrderDetailQuery();
		query.setOrderId(orderId);
		query.setThirdId(thirdId);
		ResponseInfo info = hotelOrderQueryHubService.getOrderDetail(query);
		if(info.isSuccess()){
			HotelOrderDto order = info.getData();
			HotelThirdOrder detail = order.getOuterOrder();
			json.put("result", true);
			if(detail==null) {
				json.put("showStatus", detail.getMessage());
				json.put("orderStatus", detail.getStatus());
				json.put("isShow", false);
				return json;
			}
			json.put("showStatus", detail.getMessage());
			json.put("orderStatus", detail.getStatus());
			if(detail.getFinallStatusAble()){
				json.put("isShow", true);
			}else{
				json.put("isShow", false);
			}
		}else{
			json.put("showStatus", "供应商无订单信息");
			json.put("orderStatus", "无");
			json.put("result",true);
			json.put("isShow", false);
		}
		return json;

	}

	/**
	 * 取消订单
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value = "cancelOrder",method = RequestMethod.POST)
	@ResponseBody
	public JSONObject cancelOrder(String orderId, Integer taskId){
		JSONObject json = new JSONObject();
		HotelOrderDto order = hotelOrderService.getOrderById(orderId).getData();

		HotelOrderCancelQuery query = new HotelOrderCancelQuery();
		query.setRefundReasonType(RefundReasonType.SYS_REFUND);
		query.setOrderId(order.getId());
		query.setReason("行程变更");
		ResponseInfo response = hotelReverseService.orderCancel(query);
		if(!response.getCode().equals(HbpRtnCodeConst.SUCCESS) && !response.getCode().equals(HbpRtnCodeConst.REPEAT_CANCEL)){
			json.put("result", false);
			json.put("msg", response.getMsg());
			return json;
		}
		OrderTaskDto dto = new OrderTaskDto();
		dto.setId(taskId);
		dto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		dto.setResult("取消订单成功");
		orderTaskService.update(dto);
		json.put("result", true);
		if(order.getIssueChannelDto() != null && order.getIssueChannelDto().getLineType() == 1){
			json.put("msg", "该订单为线下补单，已创建线下退款审核任务");
		}else{
			json.put("msg", "恭喜你,任务处理成功啦！");
		}
		return json;

	}

	/**
	 * 重新支付
	 * @param orderId
	 * @param taskId
     */
	@RequestMapping(value = "supPay",method = RequestMethod.POST)
	@ResponseBody
	public ResponseInfo supPay(String orderId, Integer taskId){
		ResponseInfo response = hotelOrderService.getOrderById(orderId);
		if(!response.isSuccess()){
			return response;
		}
		HotelOrderDto order = response.getData();
		response = hotelHubService.asyncOrderPay(order);
		if(response.isSuccess()){
			OrderTaskDto dto = new OrderTaskDto();
			dto.setId(taskId);
			dto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
			dto.setResult("转系统出票成功");
			orderTaskService.update(dto);
		}
		return response;
	}



	//查询各个状态的订单数量
	private List<OrderCountDto> getOrderCountList(QueryDto<HotelOrderCondition> queryDto) {
		List<OrderCountDto> orderCountList = hotelOrderService.findOrderCountList(queryDto).getData();
		List<OrderCountDto> list = new ArrayList<OrderCountDto>();

		OrderShowStatus[] values = OrderShowStatus.values();
		for (OrderShowStatus status : values) {
			OrderCountDto orderCountDto = new OrderCountDto(status,0);
			for(OrderCountDto countDto : orderCountList){
				if(status.equals(countDto.getOrderShowStatus())){
					orderCountDto.setCount(countDto.getCount());
					break;
				}
			}
			list.add(orderCountDto);
		}

		return list;
	}



	//分页数据
	private Page<HotelOrderDto> getPageList(Integer pageIndex,Integer pageSize,HotelOrderCondition condition){
		if(pageIndex==null){
			pageIndex = 1;
		}
		if(pageSize==null){
			pageSize = 10;
		}
		QueryDto<HotelOrderCondition> queryDto = new QueryDto<HotelOrderCondition>(pageIndex,pageSize);
		queryDto.setCondition(condition);
		Page<HotelOrderDto> page = hotelOrderService.tmcQueryListWithPage(queryDto).getData();
		return page;
	}

	/**
	 * 修改任务超时时间
	 * @param taskId 任务id
	 * @param delay 延时时间：分钟
	 */
	@RequestMapping("/updateMaxProcessDate")
	public void updateMaxProcessDate(Integer taskId,Integer delay){
		OrderTaskDto dto = new OrderTaskDto();
		dto.setId(taskId);
		dto.setMaxProcessDate(DateUtils.addMinute(new Date(), delay));
		dto.setTaskStatus(TaskStatus.PROCESSING);
		orderTaskService.update(dto);
	}



}