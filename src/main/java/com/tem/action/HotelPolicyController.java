package com.tem.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.tem.hotel.api.hotel.own.OwnHotelBrandService;
import com.tem.hotel.api.order.HotelOrderInterceptScopeService;
import com.tem.hotel.api.protocol.ProtocolHotelRoomService;
import com.tem.hotel.dto.hotel.own.OwnHotelBrandDto;
import com.tem.hotel.dto.hotel.room.HotelRoomDto;
import com.tem.platform.form.ResponseTable;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.web.Message;
import com.tem.hotel.api.hotel.HotelBrandService;
import com.tem.hotel.api.hotel.HotelHubConnectorService;
import com.tem.hotel.api.hotel.HotelPolicyApplyScopeService;
import com.tem.hotel.api.hotel.HotelPolicyDetailService;
import com.tem.hotel.api.hotel.HotelPolicyService;
import com.tem.hotel.dto.HotelHubsDto;
import com.tem.hotel.dto.hotel.HotelBaseDto;
import com.tem.hotel.dto.hotel.HotelBrandDto;
import com.tem.hotel.dto.hotel.HotelPolicyApplyScopeDto;
import com.tem.hotel.dto.hotel.HotelPolicyDetailDto;
import com.tem.hotel.dto.hotel.HotelPolicyDto;
import com.tem.hotel.dto.hotel.enums.PolicyApplyType;
import com.tem.platform.action.BaseController;


@Controller
@RequestMapping("hotel/policy")
public class HotelPolicyController extends BaseController {

    @Autowired
    private HotelPolicyService hotelPolicyService;
    @Autowired
    private HotelPolicyDetailService hotelPolicyDetailService;
    @Autowired
    private HotelPolicyApplyScopeService hotelPolicyApplyScopeService;
    @Autowired
    private OwnHotelBrandService ownHotelBrandService;
    @Autowired
    private HotelHubConnectorService hotelHubConnectorService;

    @Autowired
    private HotelOrderInterceptScopeService hotelOrderInterceptScopeService;
    @RequestMapping("")
    public String index(){
        return "hotel/policy/index.base";
    }

    /**
     * 获取酒店价格政策列表
     * @return
     */
    @RequestMapping("list")
    public String getList(Model model, Integer pageIndex,Integer pageSize, String keywords,Integer status,String validity){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<HotelPolicyDto> query =new QueryDto<>(pageIndex,pageSize);
        HotelPolicyDto dto=new HotelPolicyDto();
        dto.setKeyWords(keywords);
        dto.setStatus(status);
        dto.setValidity(validity);
        dto.setTmcId(super.getTmcId().intValue());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 45);
		try {
			dto.setNowDate(sdf.parse(sdf.format(new Date())));
			dto.setCrisiDate(sdf.parse(sdf.format(c.getTime())));
		} catch (ParseException e) {
			e.printStackTrace();
		}
        query.setCondition(dto);
        Page<HotelPolicyDto> pageList=hotelPolicyService.queryHotelPolicyListWithPage(query).getData();
        for (HotelPolicyDto hotelpolicyDto:pageList.getList() ) {
            hotelpolicyDto.setLessCurrentDate(dto.getNowDate().compareTo(hotelpolicyDto.getSuitDate())==1);
        }
        model.addAttribute("pageList",pageList);
        return "hotel/policy/policyTable.jsp";
    }

    /**
     * 获取酒店价格政策详情
     * @param id
     * @return
     */
    @RequestMapping("detail")
    public String getDetail(Integer id, Model model){
        HotelPolicyDto dto=new HotelPolicyDto();
        if(id!=null){
            dto=hotelPolicyService.findHotelPolicyById(id).getData();
            if(StringUtils.isNotBlank(dto.getHotelBrandId())){
                ResponseInfo res = ownHotelBrandService.getOwnBrandById(Long.valueOf(dto.getHotelBrandId()));
                if(res.isSuccess()&&res.getData()!=null){
                    dto.setBrandName(((OwnHotelBrandDto)res.getData()).getShortName());
                }
            }
        }
        model.addAttribute("hotelPolicyDto",dto);
        return "hotel/policy/policyModel.jsp";
    }

    @RequestMapping("hotelRoomData")
    @ResponseBody
    public ResponseTable getHotelRoomData(Integer policyId){
        Integer pageIndex = super.getIntegerValue("pageIndex");
        Integer pageSize = super.getIntegerValue("pageSize");
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }
        String name = super.getStringValue("name");
        String brandId;
        Integer applyType;
        List<String> hotelIds = new ArrayList<String>();
        ResponseInfo res = hotelPolicyService.findWithApplyScopeAndPolicyDetailById(policyId);
        if(res.isSuccess()){
            HotelPolicyDto policyDto = res.getData();
            brandId = policyDto.getHotelBrandId();
            applyType = policyDto.getApplyType();
            if(applyType!=1){//非适用所有酒店情况
                List<HotelPolicyApplyScopeDto> applyScopeDtos = policyDto.getApplyScopeList();
                for(HotelPolicyApplyScopeDto applyScopeDto : applyScopeDtos) {
                    hotelIds.add(applyScopeDto.getHotelId());
                }
            }
            QueryDto<HotelRoomDto> query=new QueryDto<>(pageIndex,pageSize);
            HotelRoomDto roomDto=new HotelRoomDto();
            roomDto.setBrandId(brandId);
            roomDto.setApplyType(applyType);
            roomDto.setKeyWords(name);
            roomDto.setHotelIds(hotelIds.toArray(new String[hotelIds.size()]));
            query.setCondition(roomDto);
            ResponseInfo ret=hotelOrderInterceptScopeService.getRoomsByCondition(query);
            if(ret.isSuccess()){
                Page<String> page =ret.getData();
                return new ResponseTable(page);
            }

        }
        return new ResponseTable();
    }
    /**
     * 新增修改酒店价格政策
     * @param model
     * @return
     */
    @RequestMapping("update")
    @ResponseBody
    public Message update(Model model,HotelPolicyDto hotelPolicyDto){
        boolean isAdd=hotelPolicyDto.getId()==null?true:false;
        if(hotelPolicyDto.getLevel()==null){
            hotelPolicyDto.setLevel(100);
        }
        if(hotelPolicyDto.getIsShowLabe()==null){
            hotelPolicyDto.setIsShowLabe(0);
        }
        if(isAdd){
            hotelPolicyDto.setCreator(super.getCurrentUser().getFullname());
            hotelPolicyDto.setCreateTime(new Date());
        }else{
            HotelPolicyDto dto=hotelPolicyService.findHotelPolicyById(hotelPolicyDto.getId()).getData();
            if(dto==null){
                return Message.error("修改失败!");
            }
            hotelPolicyDto.setHotelBrandId(dto.getHotelBrandId());
            hotelPolicyDto.setBrandName(dto.getBrandName());
            hotelPolicyDto.setApplyType(dto.getApplyType());
            hotelPolicyDto.setCreator(dto.getCreator());
            hotelPolicyDto.setCreateTime(dto.getCreateTime());
            hotelPolicyDto.setSuitDate(dto.getSuitDate());
            hotelPolicyDto.setUpdater(super.getCurrentUser().getFullname());
            hotelPolicyDto.setUpdateTime(new Date());
        }
        hotelPolicyDto.setTmcId(super.getTmcId().intValue());
        ResponseInfo res=hotelPolicyService.insertHotelPolicy(hotelPolicyDto);
        if(res.isSuccess()) {
            if(isAdd) {
                return Message.success(res.getData().toString());
            }else{
                return Message.success("修改成功!");
            }
        }else{
            if(isAdd) {
                return Message.error("新增失败!");
            }else{
                return Message.error("修改失败!");
            }
        }
    }
    @ResponseBody
    @RequestMapping("updatePolicyOther")
    public Message updatePolicyOther(Model model,Integer id,String hotelBrandName,String hotelBrandId,Integer applyType){
        HotelPolicyDto hotelPolicyDto=hotelPolicyService.findHotelPolicyById(id).getData();
        if(hotelPolicyDto!=null){
            hotelPolicyDto.setApplyType(applyType);
            hotelPolicyDto.setBrandName(hotelBrandName);
            hotelPolicyDto.setHotelBrandId(hotelBrandId);
            hotelPolicyDto.setUpdater(super.getCurrentUser().getFullname());
            hotelPolicyDto.setUpdateTime(new Date());
            ResponseInfo res=hotelPolicyService.updateHotelPolicyHotelMessage(hotelPolicyDto);
            if(res.isSuccess()){
                return Message.success("保存成功!");
            }
        }
        return Message.error("保存失败!");
    }
    /**
     * 删除酒店价格政策
     * @param model
     * @return
     */
    @RequestMapping("delete")
    @ResponseBody
    public Message delete(Model model,Integer id){
        Boolean res=hotelPolicyService.deleteHotelPolicyById(id).getData();
        if(res!=null&&res) {
            return Message.success("删除成功!");
        }
        return Message.error("删除失败!");
    }

    /**
     * 检查政策对应相关酒店信息
     * @return
     */
    @RequestMapping(value="checkHotelInfo")
    @ResponseBody
    public Message checkHotelInfo(Integer policyId,Integer applyType,String brandId){
        HotelPolicyDto hotelPolicyDto=hotelPolicyService.findHotelPolicyById(policyId).getData();
        if(hotelPolicyDto!=null){
            if(applyType==null){
                applyType=hotelPolicyDto.getApplyType();
            }
            if(applyType==null){
                return Message.error("");
            }
            if(PolicyApplyType.Suit.getValue().equals(applyType)||PolicyApplyType.NotSuit.getValue().equals(applyType)) {
                String policyHotels=hotelPolicyApplyScopeService.getAllHotelByPolicyIdAndBrandId(policyId,brandId).getData();
                if(StringUtils.isNotEmpty(policyHotels)) {
                    return Message.success("");
                }
            } else {
                return Message.success("");
            }
        }
        return Message.error("");
    }
    /**
     * 获取酒店价格政策详情列表
     * @param model
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @RequestMapping(value="policyDetailList")
    public String getHotelPolicyDetailList(Model model,Integer policyId, Integer pageIndex,Integer pageSize){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<HotelPolicyDetailDto> queryDto=new QueryDto<>(pageIndex,pageSize);
        HotelPolicyDetailDto hotelPolicyDetailDto=new HotelPolicyDetailDto();
        hotelPolicyDetailDto.setHotelPolicyId(policyId);
        queryDto.setCondition(hotelPolicyDetailDto);
        Page<HotelPolicyDetailDto> pageList=hotelPolicyDetailService.queryHotelPolicyDetailListWithPage(queryDto).getData();
        model.addAttribute("pageList",pageList);
        return "hotel/policy/policyDetailList.jsp";
    }
    /**
     * 获取酒店价格政策细则详情
     * @param id 政策id
     * @param detailId 政策细则id
     * @return
     */
    @RequestMapping(value = "policyDetail")
    public String getHotelPolicyDetail(Integer id,@RequestParam(value="detailId",required = false)Integer detailId, Model model){
        HotelPolicyDetailDto hotelPolicyDetailDto=null;
        if(detailId!=null){
            hotelPolicyDetailDto = hotelPolicyDetailService.findHotelPolicyDetailById(detailId ).getData();
        }else{
            hotelPolicyDetailDto=new HotelPolicyDetailDto();
            hotelPolicyDetailDto.setHotelPolicyId(id);
        }
        String rooms=hotelPolicyDetailDto.getRoomNames();
        List<HotelRoomDto> roomsList=new ArrayList<>();
        if(rooms!=null && rooms.indexOf("all")==-1) {
            for (String item : rooms.split(",")) {
                HotelRoomDto roomDto=new HotelRoomDto();
                roomDto.setName(item);
                roomsList.add(roomDto);
            }
        }
        model.addAttribute("rooms",JSONObject.toJSON(roomsList));
        model.addAttribute("origins",getOriginList());
        model.addAttribute("hotelPolicyDetailDto",hotelPolicyDetailDto);
        return "hotel/policy/policyDetail.jsp";
    }

    /**
     * 获取房型
     * @return
     */
    @ResponseBody
    @RequestMapping("rooms")
    public Message getRooms(Integer id){
        String rooms=hotelPolicyDetailService.getRoomsByPolicyId(id).getData();
        return Message.success(rooms);
    }
    private List<HotelHubsDto> getOriginList(){
        List<HotelHubsDto> list=new ArrayList();
        String hubsCode;
        boolean exists;
        HotelHubsDto dto=new HotelHubsDto() ;
        dto.setHubsCode("");
        dto.setHubsName("请选择");
        list.add(dto);
        HotelHubsDto dto1=new HotelHubsDto() ;
        dto1.setHubsCode("self");
        dto1.setHubsName("独立政策");
        list.add(dto1);
        HotelHubsDto dto2=new HotelHubsDto() ;
        dto2.setHubsCode("all");
        dto2.setHubsName("全部连接");
        list.add(dto2);
        List<HotelHubsDto> hubsDtoList=hotelHubConnectorService.getHubs(super.getTmcId(),false).getData();
        for (int i=0;i<hubsDtoList.size();i++){
            hubsCode=hubsDtoList.get(i).getHubsCode();
            exists=false;
            for (int j=0;j<list.size();j++) {
                if (list.get(j).getHubsCode().equals(hubsCode)) {
                    exists=true;
                }
            }
            if(!exists){
                list.add(hubsDtoList.get(i));
            }
        }
        return list;
    }
    /**
     * 保存酒店政策细则
     */
    @RequestMapping("savePolicyDetail")
    @ResponseBody
    public Message savePolicyDetail(HotelPolicyDetailDto hotelPolicyDetailDto){
        if(hotelPolicyDetailDto.getRoomNames().contains("all")){
            hotelPolicyDetailDto.setRoomNames("all");
        }
        if(hotelPolicyDetailDto.getBreakfastCount().contains("all")){
            hotelPolicyDetailDto.setBreakfastCount("all");
        }
        ResponseInfo res=hotelPolicyDetailService.insertOrUpdateHotelPolicyDetail(hotelPolicyDetailDto);
        if(res.isSuccess()){
            if(hotelPolicyDetailDto.getId()==null){
                return Message.success("新增成功!");
            }
            return Message.success("修改成功!");
        }else{
            if(hotelPolicyDetailDto.getId()==null){
                return Message.error("新增失败!");
            }
           return Message.error("修改失败!");
        }
    }

    /**
     * 删除酒店政策细则
     * @param id
     * @return
     */
    @RequestMapping("deletePolicyDetail")
    @ResponseBody
    public Message deletePolicyDetail(Integer policyId,Integer id){
        ResponseInfo res=hotelPolicyDetailService.deleteHotelPolicyDetailById(id);
        if(res.isSuccess()){
            return Message.success("删除成功!");
        }
        return Message.error("删除失败!");
    }

    /**
     * 获取酒店列表
     * @param policyId 政策id
     * @param brandId 品牌id
     * @param hotelName 酒店名称
     * @return
     */
    @RequestMapping(value="policyHotelList")
    public String getPolicyHotelList(QueryDto<HotelPolicyApplyScopeDto> queryDto ,@RequestParam(value="policyId",required = true) Integer policyId,
           String brandId, String hotelName,Integer pageIndex,Integer pageSize, Model model){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        queryDto=new QueryDto<>(pageIndex,pageSize);
        HotelPolicyApplyScopeDto hotelPolicyApplyScopeDto=new HotelPolicyApplyScopeDto();
        hotelPolicyApplyScopeDto.setOwnPolicyId(policyId);
        hotelPolicyApplyScopeDto.setHotelBrandId(brandId);
        hotelPolicyApplyScopeDto.setHotelName(hotelName);
        queryDto.setCondition(hotelPolicyApplyScopeDto);

        Page<HotelPolicyApplyScopeDto> pageList=hotelPolicyApplyScopeService.queryApplyScopeListWithPage(queryDto).getData();
        model.addAttribute("pageList",pageList);
        model.addAttribute("hotelName",hotelName);
        return "hotel/policy/policyHotel.jsp";
    }
    @RequestMapping("addHotels")
    @ResponseBody
    public Message addHotels(Integer policyId,String hotels, String brandId){
        List<HotelBaseDto> hotelList= JSONObject.parseArray(hotels,HotelBaseDto.class);
        List<HotelBaseDto> addHotelList=new ArrayList<>();
        String hotelIds=hotelPolicyApplyScopeService.getAllHotelByPolicyIdAndBrandId(policyId,brandId).getData();
        if(StringUtils.isNotEmpty(hotelIds)){
            String[] ids=hotelIds.split(",");
            for (HotelBaseDto hotelBaseDto:hotelList) {
                boolean exists=false;
                for (String id:ids){
                    if(id.equals(hotelBaseDto.getHotelId())){
                        exists=true;
                        break;
                    }
                }
                if(!exists){
                    addHotelList.add(hotelBaseDto);
                }
            }
        }else{
            addHotelList=hotelList;
        }
        if(addHotelList.size()==0){
            return Message.success("添加成功!");
        }
        ResponseInfo res=hotelPolicyApplyScopeService.insertApplyScope(policyId,addHotelList,brandId);
        if(res.isSuccess()){
            return Message.success("添加成功!");
        }else{
            return Message.error("添加失败!");
        }
    }
    /**
     * 酒店删除
     * @param ids
     * @return
     */
    @RequestMapping("deleteHotels")
    @ResponseBody
    public Message deletePolicyHotels(String ids){
        List<Integer> hotelIdList=JSONObject.parseArray(ids,Integer.class);
        Integer[] hotelIds=hotelIdList.toArray(new Integer[0]);
        if(hotelIds.length==0) {
            return Message.error("删除错误!");
        }

        ResponseInfo res=hotelPolicyApplyScopeService.deleteApplyScopeById(hotelIds);
        if(res.isSuccess()){
            return Message.success("删除成功!");
        }else{
            return Message.error("删除错误!");
        }
    }

}
