package com.tem.action;

import com.iplatform.common.web.Message;
import com.tem.supplier.api.SettleChannelService;
import com.tem.supplier.condition.ChannelDetailQuery;
import com.tem.supplier.dto.SettleChannelDetailDto;
import com.tem.supplier.dto.SettleChannelDto;
import com.tem.flight.dto.hubs.HubsDto;
import com.tem.hotel.api.hotel.HotelHubConnectorService;
import com.tem.hotel.dto.HotelHubsDto;
import com.tem.hotel.query.HotelHubsConnectQuery;
import com.tem.hotel.result.HotelHubsConnectResult;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("hotel/hubs")
public class HotelHubsController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(HotelHubsController.class);
	
	@Autowired
	private HotelHubConnectorService hotelHubConnectorService;
	@Autowired
	private IssueChannelService issueChannelService;
	@Autowired
	private PartnerService partnerService;
	@Autowired
	private SettleChannelService settleChannelService;
	@RequestMapping("")
	public String index(Model model,Boolean needDisabled){
		if(needDisabled == null)//默认是的
        {
            needDisabled = true;
        }
		List<HotelHubsDto> hubsDtos = hotelHubConnectorService.findAll(needDisabled).getData();
		model.addAttribute("hubsDtos", hubsDtos);
		return "hotel/hubs/hubs.base";
	}
	
	/**
	 * 连接器详情
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/hubsDetail", method = RequestMethod.GET)
	public String hubsDetail(Model model) {
		String code = (String)super.getRequest().getParameter("code");
		HotelHubsDto hubsDto = null;
		if(StringUtils.isNotEmpty(code)){
			hubsDto = hotelHubConnectorService.getByCode(code).getData();
			model.addAttribute("editMode", "update");
		}else{
			hubsDto = new HotelHubsDto();
			model.addAttribute("editMode", "add");
		}
		model.addAttribute("hubsDto", hubsDto);
		return "hotel/hubs/hubsDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkHubsCode")
	public Message checkHubsCode(Model model, @RequestParam("hubsCode") String hubsCode,@RequestParam("editMode") String editMode) {
		if("update".equals(editMode)){//修改下 code不能修改 
			return Message.success("yes");
		}else {
			HotelHubsDto hubsDto = hotelHubConnectorService.getByCode(hubsCode).getData();
			if (hubsDto == null) {
				return Message.success("yes");
			}else{
				return Message.error("no");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrUpdateHubs", method = RequestMethod.POST)
	public Message saveOrUpdateHubs(HotelHubsDto hubsDto,Model model,BindingResult br,String editMode) {
		if(br.hasErrors()){
			return Message.error("数据校验不通过。");
		}
		if("add".equals(editMode)){
			if(hotelHubConnectorService.add(hubsDto).getData()){
				return Message.success("新增成功。");
			}else {
				return Message.error("新增失败。");
			}
		}
		if("update".equals(editMode)){
			if(hotelHubConnectorService.update(hubsDto).getData()){
				return Message.success("修改成功。");
			}else {
				return Message.error("修改失败。");
			}
		}
		return Message.error("系统异常");
	}
	
	@RequestMapping(value = "/loadRight", method = RequestMethod.GET)
	public String loadRight(Model model, @RequestParam("code") String code) {
		HotelHubsDto hubsDto = hotelHubConnectorService.getByCode(code).getData();
		model.addAttribute("hubsDto", hubsDto);

		HotelHubsConnectQuery query = new HotelHubsConnectQuery();
		query.setHubsCode(code);
		List<HotelHubsConnectResult> hubsConnectDtos = hotelHubConnectorService.findConnect(query).getData();
		model.addAttribute("hubsConnectDtos", hubsConnectDtos);

		return "hotel/hubs/hubsRight.jsp";
	}
	
	/**
	 * 删除
	 * @param model
	 * @param code
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteHubs")
	public Message deleteHubs(Model model, @RequestParam("code") String code) {
		if (StringUtils.isEmpty(code)) {
			return Message.error("没有要删除的数据");
		}
		//删除判断子项
		if(hotelHubConnectorService.deleteByCode(code).getData()) {
            return Message.success("删除成功");
        }
		return Message.error("删除失败");
	}

	/**
	 * 新增或修改连接器
	 * @param model
	 * @return
     */
	@RequestMapping(value = "/hubsConnectDetail", method = RequestMethod.GET)
	public String hubsConnectDetail(Model model,String hubsCode,Integer id) {
		//酒店渠道列表
		List<IssueChannelDto> issueChannelList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(), IssueChannelType.HOTEL.getType(), LineType.ON_LINE.getType());

		HotelHubsConnectResult hubsConnectDto = null;
		if (id == null) {//新增
			hubsConnectDto = new HotelHubsConnectResult();
			hubsConnectDto.setHubsCode(hubsCode);
		} else {//修改
			hubsConnectDto = hotelHubConnectorService.getConnectById(id).getData();
			List<IssueChannelDto> issueChannelDtos=issueChannelService.findListByTmcIdAndChannelTypeAndLineType(Long.valueOf(hubsConnectDto.getTmcId()),IssueChannelType.HOTEL.name(),LineType.ON_LINE.getType());
			if(CollectionUtils.isNotEmpty(issueChannelDtos)){
				for ( IssueChannelDto issueChannelDto:issueChannelDtos){
					if(issueChannelDto.getCode().equals(hubsConnectDto.getChannelCode())){
						model.addAttribute("settleChannelName",settleChannelService.getById(issueChannelDto.getSettelChannelId()));
						break;
					}
				}
			}
			PartnerDto tmcPartner = partnerService.findById(Long.valueOf(hubsConnectDto.getTmcId()));
			model.addAttribute("tmcPartner", tmcPartner);
		}
		model.addAttribute("hubsConnectDto", hubsConnectDto);

//		HubsDto hubsDto = hubsService.getByCode(hubsCode); //此处的作用是告诉前台那些接口是可以选择的
//		model.addAttribute("hubsDto", hubsDto);
		model.addAttribute("issueChannelList", issueChannelList);
		return "hotel/hubs/hubsConnectDetail.jsp";
	}

	/**
	 * 新增或更新连接
	 * @param hubsConnectDto
	 * @param model
	 * @param br
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/saveOrupdateHubsConnect", method = RequestMethod.POST)
	public Message saveOrupdateHubsConnect(@ModelAttribute("hubsConnectDto") @Validated HotelHubsConnectResult hubsConnectDto,
										   Model model, BindingResult br) {
		if (br.hasErrors()) {
			return Message.error("数据校验不通过。");
		}

		if (hubsConnectDto.getId() == null) {
			if (hotelHubConnectorService.addOrUpdateConnect(hubsConnectDto).getData()) {
				return Message.success("新增成功。");
			} else {
				return Message.error("新增失败。");
			}
		} else {
			if (hotelHubConnectorService.addOrUpdateConnect(hubsConnectDto).getData()) {
				return Message.success("修改成功。");
			} else {
				return Message.error("修改失败。");
			}
		}
	}

	/**
	 * 删除连接
	 * @param model
	 * @param id
     * @return
     */
	@ResponseBody
	@RequestMapping(value = "/deleteHubsConnect")
	public Message deleteHubsConnect(Model model, @RequestParam Integer id) {
		if (hotelHubConnectorService.deleteConnect(id).getData()) {
			return Message.success("删除成功");
		}
		return Message.error("删除失败");
	}

	/**
	 * 获取该 tmc 可以选择的线上渠道
	 * @param tmcId
     */
	@RequestMapping("getChannels")
	@ResponseBody
	public List<IssueChannelDto> getChannels(String tmcId){
		 return issueChannelService.findListByTmcIdAndChannelTypeAndLineType(Long.valueOf(tmcId),IssueChannelType.HOTEL.name(),LineType.ON_LINE.getType());
	}
	/**
	 * 获取收支渠道
	 * @param settelChannelId
	 */
	@RequestMapping("getSettleChannel")
	@ResponseBody
	public SettleChannelDto getSettleChannels(Long settelChannelId){
		return settleChannelService.getById(settelChannelId);
	}
	

    @ResponseBody
    @RequestMapping(value = "/getHubByCode")
    public HotelHubsDto getHubByCode(String code) {
        return hotelHubConnectorService.getByCode(code).getData();
    }

}
