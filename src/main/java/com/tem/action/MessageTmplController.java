package com.tem.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.web.Message;
import com.tem.notify.api.MessageTmplService;
import com.tem.notify.dto.MessageTmplCondition;
import com.tem.notify.dto.MessageTmplDto;
import com.tem.platform.action.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 消息模板维护	
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("notify/message/tmpl")
public class MessageTmplController extends BaseController {
	
	@Autowired
	private MessageTmplService messageTmplService;
	
	@RequestMapping(value = "")
	public String notifyTmplPage(Model model,@RequestParam(value="pageIndex",required=false)Integer pageIndex,@RequestParam(value="code",required=false)String code,
			@RequestParam(value="title",required=false)String title) {
		
		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<MessageTmplCondition> queryDto = new QueryDto<MessageTmplCondition>(pageIndex,Page.DEFAULT_PAGE_SIZE);

		MessageTmplCondition condition = new MessageTmplCondition();
		
		if(StringUtils.isNotEmpty(code)){
			condition.setCode(code);
		}
		
		if(StringUtils.isNotEmpty(title)){
			condition.setTitle(title);
		}
		queryDto.setCondition(condition);
	   
		Page<MessageTmplDto> messageTmplList = messageTmplService.queryListWithPage(queryDto);
		model.addAttribute("pageList", messageTmplList);
		model.addAttribute("code", code);
		model.addAttribute("title", title);
		return "notify/message/tmplList.base";
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{id}")
	public Message delete(Model model, @PathVariable("id") Long id) {
		if (id == null) {
			return Message.error("不存在的模板");
		}
		messageTmplService.deleteById(id);
		return Message.success("操作成功");
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,@RequestParam(value="id",required=false) Long id) {
		MessageTmplDto messageTmpl = null;
		if(id == null){//新增
			messageTmpl = new MessageTmplDto();
		}else{
			messageTmpl = messageTmplService.selectById(id);
		}
		model.addAttribute("messageTmpl", messageTmpl);
		return "notify/message/messageTmpl.base";
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkCode")
	public Message checkCode(Model model, @RequestParam(value="id",required=false) Long id, @RequestParam(value="code",required=false) String code) {
		if(StringUtils.isEmpty(code)){
			return Message.success("yes");
		}
		MessageTmplDto messageTmpl = messageTmplService.selectByCode(code);
		if (messageTmpl == null) {
			return Message.success("yes");
		}else{
			if(id != null){//说明修改的
				MessageTmplDto messageTmpl2 = messageTmplService.selectById(id);
				if(code.equals(messageTmpl2.getCode())){
					return Message.success("yes");
				}
			}
			return Message.error("no");
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
	public Message saveOrUpdate(MessageTmplDto messageTmplDto) {
		if (messageTmplDto.getId() == null) {
			messageTmplService.insert(messageTmplDto);
			return Message.success("新增成功");
		} else {
			messageTmplService.update(messageTmplDto);
			return Message.success("修改成功");
		}
	}
	
}
