package com.tem.action.form;

/**
 * 订单来源
 * @author sb
 *
 */
public class OriginTypeForm {
	
    private String code;
    
    private String name;
    
    public OriginTypeForm() {}
    
    public OriginTypeForm(String code, String name) {
    	this.code = code;
    	this.name = name;
    }
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
   
}
