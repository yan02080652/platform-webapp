package com.tem.action.form;

import com.tem.hotel.dto.hotel.HotelBaseDto;
import com.tem.hotel.dto.hotel.room.ratePlan.HotelPaymentType;
import com.tem.hotel.dto.hotel.rule.enums.PriceAddType;

import java.math.BigDecimal;
import java.util.List;

public class PriceRuleTemplateFrom {
    private Integer id;

    private String name;

    private String productType;

    private Boolean filterImmediatelyConfirm;

    private PriceAddType noStarType;

    private BigDecimal noStarAmount;

    private PriceAddType economyType;

    private BigDecimal economyAmount;

    private PriceAddType comfortType;

    private BigDecimal comfortAmount;

    private PriceAddType topGradeType;

    private BigDecimal topGradeAmount;

    private PriceAddType luxuriousType;

    private BigDecimal luxuriousAmount;

    private BigDecimal bookingFee;

    private BigDecimal refundFee;

    private String[] productTypes;

    private String brandId;

    private String hotelId;

    /*扩展属性*/
    private String brandsDesc;
    //适用酒店类型 1-适用所有酒店 2-适用已下酒店
    private Integer suitableHotelType;
    private List<HotelBaseDto> hotelList;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
        this.productTypes = productType.split(",");
    }

    public Boolean getFilterImmediatelyConfirm() {
        return filterImmediatelyConfirm;
    }

    public void setFilterImmediatelyConfirm(Boolean filterImmediatelyConfirm) {
        this.filterImmediatelyConfirm = filterImmediatelyConfirm;
    }

    public PriceAddType getEconomyType() {
        return economyType;
    }

    public void setEconomyType(PriceAddType economyType) {
        this.economyType = economyType;
    }

    public BigDecimal getEconomyAmount() {
        return economyAmount;
    }

    public void setEconomyAmount(BigDecimal economyAmount) {
        this.economyAmount = economyAmount;
    }

    public PriceAddType getComfortType() {
        return comfortType;
    }

    public void setComfortType(PriceAddType comfortType) {
        this.comfortType = comfortType;
    }

    public BigDecimal getComfortAmount() {
        return comfortAmount;
    }

    public void setComfortAmount(BigDecimal comfortAmount) {
        this.comfortAmount = comfortAmount;
    }

    public PriceAddType getTopGradeType() {
        return topGradeType;
    }

    public void setTopGradeType(PriceAddType topGradeType) {
        this.topGradeType = topGradeType ;
    }

    public BigDecimal getTopGradeAmount() {
        return topGradeAmount;
    }

    public void setTopGradeAmount(BigDecimal topGradeAmount) {
        this.topGradeAmount = topGradeAmount;
    }

    public PriceAddType getLuxuriousType() {
        return luxuriousType;
    }

    public void setLuxuriousType(PriceAddType luxuriousType) {
        this.luxuriousType = luxuriousType;
    }

    public BigDecimal getLuxuriousAmount() {
        return luxuriousAmount;
    }

    public void setLuxuriousAmount(BigDecimal luxuriousAmount) {
        this.luxuriousAmount = luxuriousAmount;
    }

    public BigDecimal getBookingFee() {
        return bookingFee;
    }

    public void setBookingFee(BigDecimal bookingFee) {
        this.bookingFee = bookingFee;
    }

    public BigDecimal getRefundFee() {
        return refundFee;
    }

    public void setRefundFee(BigDecimal refundFee) {
        this.refundFee = refundFee;
    }

    public String getProducts(){
        String[] array = productType.split(",");
        if(array.length == 0){
            return null;
        }
        String message = "";
        for(String product : array){
            HotelPaymentType payment = HotelPaymentType.valueOf(product.trim());
            message += payment.getMessage() + ",";
        }
        return message.substring(0,message.length() - 1);
    }

    public String[] getProductTypes() {
        return productTypes;
    }

    public void setProductTypes(String[] productTypes) {
        this.productTypes = productTypes;
        if(productTypes.length > 0){
            String ss = "";
            for(String str : productTypes){
                ss += str.trim() + ",";
            }
            this.productType = ss.substring(0,ss.length() - 1);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PriceAddType getNoStarType() {
        return noStarType;
    }

    public void setNoStarType(PriceAddType noStarType) {
        this.noStarType = noStarType;
    }

    public BigDecimal getNoStarAmount() {
        return noStarAmount;
    }

    public void setNoStarAmount(BigDecimal noStarAmount) {
        this.noStarAmount = noStarAmount;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getBrandsDesc() {
        return brandsDesc;
    }

    public void setBrandsDesc(String brandsDesc) {
        this.brandsDesc = brandsDesc;
    }

    public Integer getSuitableHotelType() {
        return suitableHotelType;
    }

    public void setSuitableHotelType(Integer suitableHotelType) {
        this.suitableHotelType = suitableHotelType;
    }

    public List<HotelBaseDto> getHotelList() {
        return hotelList;
    }

    public void setHotelList(List<HotelBaseDto> hotelList) {
        this.hotelList = hotelList;
    }
}
