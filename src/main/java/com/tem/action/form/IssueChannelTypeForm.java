package com.tem.action.form;

import org.apache.commons.pool2.impl.EvictionConfig;

public class IssueChannelTypeForm {
	
	  private String code;
	  
	  private String msg;
	  
	  public IssueChannelTypeForm() {}
	  public IssueChannelTypeForm(String code, String msg) {
		  this.code = code;
		  this.msg = msg;
	  }
	  
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
}
