package com.tem.action.form;

import com.tem.pss.dto.refund.RefundOrderDto;

/**
 * 退款审核form
 * @author wangzq
 */
public class RefundAduitForm {

	//退款dto
	 private RefundOrderDto refundOrderDto;
	 //任务id
	 private Integer taskId;

	public RefundOrderDto getRefundOrderDto() {
		return refundOrderDto;
	}

	public void setRefundOrderDto(RefundOrderDto refundOrderDto) {
		this.refundOrderDto = refundOrderDto;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
}
