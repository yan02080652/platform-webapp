package com.tem.action.form;

import java.io.Serializable;

public class HotelBaseForm implements Serializable{

	private static final long serialVersionUID = 941102431725026017L;

	/**
	 * 主键ID
	 */
    private Integer id;

    /**
     * 酒店ID
     */
    private String hotelId;

	/**
	 * 酒店名称
	 */
	private String name;

	/**
	 * 酒店来源类型
	 */
	private String originType;

	/**
	 * 酒店所在城市id
	 */
	private String cityId;

	/**
	 * 关联城市
	 * 
	 * <pre>
	 * 可能多个城市，逗号分隔；也可能包含City。
	 * </pre>
	 */
	private String cityId2;

	/**
	 * 酒店状态
	 * 
	 * <pre>
	 * 为空默认为OPEN。
	 * OPEN: 可以销售； CLOSE: 已经关闭。
	 * </pre>
	 */
	private String hotelStatus;
	
	private String originTypeName;
	
	private String cityName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHotelId() {
		return hotelId;
	}

	public void setHotelId(String hotelId) {
		this.hotelId = hotelId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOriginType() {
		return originType;
	}

	public void setOriginType(String originType) {
		this.originType = originType;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getCityId2() {
		return cityId2;
	}

	public void setCityId2(String cityId2) {
		this.cityId2 = cityId2;
	}

	public String getHotelStatus() {
		return hotelStatus;
	}

	public void setHotelStatus(String hotelStatus) {
		this.hotelStatus = hotelStatus;
	}

	public String getOriginTypeName() {
		return originTypeName;
	}

	public void setOriginTypeName(String originTypeName) {
		this.originTypeName = originTypeName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
        
}
