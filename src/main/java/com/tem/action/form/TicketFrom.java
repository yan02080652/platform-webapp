package com.tem.action.form;

import com.tem.train.dto.order.TrainPassengerDto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 线下出票填单Form
 * 
 * @author wangzheqi
 *
 */
public class TicketFrom implements Serializable {

	private static final long serialVersionUID = 1L;

	// 订单id
	private String orderId;
	// 出票渠道
	private String issueChannel;
	// 供应单号
	private String outOrderId;
	// 结算总价
	private BigDecimal totalPiace;
	// 任务id
	private Integer taskId;
	// 乘客
	private List<TrainPassengerDto> passengerDtos;
	//用来判断是"线下出票"还是"回填票号" 1 线下出票    2回填票号  3.改签成功
	private String flag;
	//取票号
	private String ticketNo;
	/**
	 * 是否修订订单
	 */
	private Boolean isRevise;
	
	public String getIssueChannel() {
		return issueChannel;
	}

	public void setIssueChannel(String issueChannel) {
		this.issueChannel = issueChannel;
	}


	public BigDecimal getTotalPiace() {
		return totalPiace;
	}

	public void setTotalPiace(BigDecimal totalPiace) {
		this.totalPiace = totalPiace;
	}

	public List<TrainPassengerDto> getPassengerDtos() {
		return passengerDtos;
	}

	public void setPassengerDtos(List<TrainPassengerDto> passengerDtos) {
		this.passengerDtos = passengerDtos;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getOutOrderId() {
		return outOrderId;
	}

	public void setOutOrderId(String outOrderId) {
		this.outOrderId = outOrderId;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public Boolean getIsRevise() {
		return isRevise;
	}

	public void setIsRevise(Boolean isRevise) {
		this.isRevise = isRevise;
	}
}
