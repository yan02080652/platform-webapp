package com.tem.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.LogUtils;
import com.iplatform.common.web.Message;
import com.tem.hotel.api.HotelDistrictService;
import com.tem.hotel.dto.hotel.HotelDistrictDto;
import com.tem.hotel.query.HotelDistrictQuery;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DistrictService;
import com.tem.platform.api.dto.DistrictDto;
import com.tem.train.dto.station.CityLinkEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
@RequestMapping("/hotel/district")
public class HotelDistrictController extends BaseController {
    @Autowired
    private HotelDistrictService hotelDistrictService;

    @Autowired
    private DistrictService districtService;

    private static final Logger logger = LoggerFactory.getLogger(HotelDistrictController.class);

    @RequestMapping("")
    public String index(Integer pageIndex,String name, Boolean isMatching, Model model){
        model.addAttribute("cityLinkEnum", CityLinkEnum.values());
        model.addAttribute("isMatching",isMatching);
        return "/hotel/district/index.base";

    }

    @RequestMapping("/list")
    public String findAllCity(Integer pageIndex, Boolean isMatching, String name, Model model){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        QueryDto<HotelDistrictQuery> queryDto = new QueryDto<>(pageIndex,  Page.DEFAULT_PAGE_SIZE);
        queryDto.setCondition(new HotelDistrictQuery(name, isMatching));
        ResponseInfo responseInfo = hotelDistrictService.queryPageList(queryDto);
        model.addAttribute("pageList", responseInfo.getData());
        return "/hotel/district/districtTable.jsp";
    }

    @RequestMapping("/getDistrict")
    public String getById(Integer id,Integer pageIndex, String name, Boolean isMatching, Model model){
        ResponseInfo responseInfo = hotelDistrictService.queryById(id);
        model.addAttribute("district",responseInfo.getData());
        model.addAttribute("isMatching",isMatching);
        return "/hotel/district/districtModel.jsp";
    }

    @RequestMapping("/update")
    @ResponseBody
    public Message update(HotelDistrictDto district){
        if(district.getDistrictId() != null){
            district.setIsMatching(true);
        }
        ResponseInfo response = hotelDistrictService.update(district);
        if(response.getData()){
            return Message.success("修改城市成功");
        }else{
            return Message.error("修改城市失败");
        }
    }

    @ResponseBody
    @RequestMapping("/batchUpdate")
    public boolean batchInsert() {
        try {
            //在途城市
            List<DistrictDto> list = districtService.findCity();

            //在途城市map
            Map<String, DistrictDto> districtMap = getDistrictMap(list);

            //酒店城市
            ResponseInfo all = hotelDistrictService.findAll();
            List<HotelDistrictDto> hotelList = (List<HotelDistrictDto>) all.getData();
            List<HotelDistrictDto> updateList = new ArrayList<>();
            for(HotelDistrictDto district : hotelList){
                //跳过空对象
                if(district == null){
                    continue;
                }

                String name = district.getNameCn();
                String key = name.contains("(") ? name.substring(0,name.indexOf('(')).trim() : name;
                DistrictDto dto = getFromMap(districtMap, key, district.getType());
                //未匹配的城市-进行匹配
                if(district.getIsMatching() == null || district.getIsMatching()==false){
                    if(dto != null){
                        district.setIsMatching(true);
                        district.setDistrictId(dto.getId());
                        district.setDistrictPinYin(dto.getNameEn());
                        district.setDistrictName(dto.getNameCn());
                    }else{
                        district.setIsMatching(false);
                    }
                }else{
                    //匹配的城市-更新城市名和拼音
                    if(dto != null){
                        district.setDistrictName(dto.getNameCn());
                        district.setDistrictPinYin(dto.getNameEn());
                    }
                    //TODO：匹配的城市被删除-重新匹配
                }
                updateList.add(district);
            }

            Date start = new Date();
            hotelDistrictService.batchUpdate(updateList);
            Date end = new Date();
            LogUtils.info(logger,"更新城市耗时{}",end.getTime()-start.getTime());
        } catch (Exception e) {
            LogUtils.error(logger, "同步城市数据异常,信息:{}", e);
        }
        return true;
    }

    private Map<String, DistrictDto> getDistrictMap(List<DistrictDto> list) {
        Map<String, DistrictDto> map = new HashMap<>();
        for(DistrictDto dto : list){
            if(map.containsKey(dto.getNameCn())){
                continue;
            }
            map.put(dto.getNameCn(),dto);
        }
        return map;
    }

    private DistrictDto getFromMap(Map<String,DistrictDto> map, String name, String type){
        if("province".equals(type)){
            return map.containsKey(name+"省") ? map.get(name+"省") : null;
        }
        if("city".equals(type)){
            if(map.containsKey(name)){
                return map.get(name);
            }
            if(map.containsKey(name+"市")){
                return map.get(name+"市");
            }
        }
        if("county".equals(type)){
            if(map.containsKey(name)){
                return map.get(name);
            }
            if(map.containsKey(name+"市")){
                return map.get(name+"市");
            }
            if(map.containsKey(name+"县")){
                return map.get(name+"县");
            }
        }
        return null;
    }

}
