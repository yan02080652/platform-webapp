package com.tem.action;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.web.Message;
import com.tem.hotel.api.HotelDistrictService;
import com.tem.hotel.api.hotel.HotelBaseService;
import com.tem.hotel.api.hotel.HotelBrandService;
import com.tem.hotel.api.hotel.HotelSettingService;
import com.tem.hotel.api.hotel.own.OwnHotelBrandService;
import com.tem.hotel.api.hotel.own.OwnHotelService;
import com.tem.hotel.dto.hotel.HotelBaseDto;
import com.tem.hotel.dto.hotel.HotelBrandDto;
import com.tem.hotel.dto.hotel.OwnHotelDto;
import com.tem.hotel.dto.hotel.enums.OriginType;
import com.tem.hotel.dto.hotel.own.OwnHotelBrandDto;
import com.tem.hotel.dto.hotel.own.OwnHotelCondition;
import com.tem.hotel.dto.hotel.room.ratePlan.HotelPaymentType;
import com.tem.hotel.dto.hotel.rule.PriceRuleTemplateDto;
import com.tem.hotel.dto.hotel.rule.enums.PriceAddType;
import com.tem.hotel.query.HotelConditionQuery;
import com.tem.platform.action.BaseController;
import com.tem.platform.form.ResponseTable;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 加价规则配置
 * @author heyuanjing
 *
 */
@Controller
@RequestMapping("hotel/priceRuleTemplate")
public class HotelPriceRuleTemplateController extends BaseController {
	
	@Autowired
	private HotelSettingService settingService;
	@Autowired
	private HotelDistrictService hotelDistrictService;
	@Autowired
	private OwnHotelService ownHotelService;
	@Autowired
	private OwnHotelBrandService ownHotelBrandService;
	@RequestMapping("")
	public String index(){
		return "hotel/priceRuleTemplate/index.base";
	}
	
	// 分页查询
	@RequestMapping(value = "/list")
	public String list(Model model, Integer pageIndex, Integer pageSize,String keyword) {
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}
		
		QueryDto<Object> queryDto = new QueryDto<Object>(pageIndex,pageSize);
		
		if (keyword != null && !"".equals(keyword)) {
			queryDto.setField1(keyword);
		}
		
		Page<PriceRuleTemplateDto> pageList = settingService.queryPriceRuleTemplateListWithPage(queryDto).getData();
		
		model.addAttribute("pageList", pageList);
		return "hotel/priceRuleTemplate/ruleTable.jsp";
	}
	
	// 用于新增和修改时查询对象
	@RequestMapping(value = "/getRule")
	public String getCarrier(Model model, String id) {
		List<OwnHotelDto> hotelList=new ArrayList<>();
		List<OwnHotelBrandDto> brandList=new ArrayList<>();
		PriceRuleTemplateDto priceRuleTemplateDto = new PriceRuleTemplateDto();
		if (id != null && !"".equals(id)) {
			priceRuleTemplateDto = settingService.findPriceRuleTemplateById(id).getData();
		}
		if(StringUtils.isNotEmpty(priceRuleTemplateDto.getBrandId())&&!priceRuleTemplateDto.getBrandId().contains("ALL")){
			OwnHotelBrandDto brand=ownHotelBrandService.getOwnBrandById(Long.valueOf(priceRuleTemplateDto.getBrandId())).getData();
			priceRuleTemplateDto.setBrandsDesc(brand.getShortName());
			model.addAttribute("brandsJSONString", JSONObject.toJSON(brand));
			brandList.add(brand);
		}
		String hotelIds=priceRuleTemplateDto.getHotelId();
		priceRuleTemplateDto.setSuitableHotelType(StringUtils.isEmpty(hotelIds)?1:(hotelIds.contains("ALL")?1:2));

		if(priceRuleTemplateDto.getSuitableHotelType()==2){
			hotelList=ownHotelService.findListByIds(hotelIds.split(",")).getData();
		}
		model.addAttribute("brandList",JSONObject.toJSON(brandList));
		model.addAttribute("hotelList",JSONObject.toJSON(hotelList));
		model.addAttribute("priceRuleTemplateDto", priceRuleTemplateDto);
		model.addAttribute("paymentTypes",HotelPaymentType.values());
		model.addAttribute("priceAddTypes",PriceAddType.values());
		return "hotel/priceRuleTemplate/ruleModel.jsp";
	}
	//保存对象
	@ResponseBody
	@RequestMapping(value="save")	
	public Message addOrUpdate(Model model,PriceRuleTemplateDto priceRuleTemplateDto){
		boolean rs = settingService.savePriceRuleTemplate(priceRuleTemplateDto).getData();
		if (rs) {
			return Message.success("保存成功");
		} else {
			return Message.error("保存失败,请刷新重试");
		}
	}
	
	// 批量删除
	@ResponseBody
	@RequestMapping("/delete")
	public Message delete(String ids) {
		String[] split = ids.split(",");
		boolean rs = settingService.batchDeletePriceRuleTemplates(split).getData();
		if (rs) {
			return Message.success("删除成功");
		} else {
			return Message.error("删除失败，请刷新重试");
		}
	}

	/**
	 * 酒店分页查询
	 * @return
	 */
	@ResponseBody
	@RequestMapping("hotelData")
	public ResponseTable hotelData(Integer brandId){
		Integer pageIndex = super.getIntegerValue("pageIndex");
		Integer pageSize = super.getIntegerValue("pageSize");
		if(pageIndex == null){
			pageIndex = 1;
		}
		if(pageSize == null){
			pageSize = 10;
		}
		String name = super.getStringValue("name");
		String cityCode=super.getStringValue("cityCode");
		QueryDto<HotelConditionQuery> queryDto=new QueryDto<>(pageIndex,pageSize);
		HotelConditionQuery condition=new HotelConditionQuery();
		if(StringUtils.isNotBlank(cityCode)) {
			ResponseInfo res = hotelDistrictService.queryHotelDistrictIdsByParentDistrictId(Integer.valueOf(cityCode));
			if (res.isSuccess()) {
				condition.setCityIdList((List<String>) res.getData());
			}
		}
		condition.setHotelName(name);
		condition.setBrandId(brandId);
		queryDto.setCondition(condition);
		Page<OwnHotelDto> page = ownHotelService.queryOwnHotelsByBandIdAndName(queryDto).getData();
		return new ResponseTable(page);
	}
	
}
