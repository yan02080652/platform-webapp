package com.tem.action;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.transform.TransformUtils;
import com.tem.action.form.PriceRuleTemplateFrom;
import com.tem.hotel.api.hotel.HotelBaseService;
import com.tem.hotel.api.hotel.HotelBrandService;
import com.tem.hotel.api.hotel.own.OwnHotelBrandService;
import com.tem.hotel.api.hotel.own.OwnHotelService;
import com.tem.hotel.dto.hotel.HotelBaseDto;
import com.tem.hotel.dto.hotel.HotelBrandDto;
import com.tem.hotel.dto.hotel.OwnHotelDto;
import com.tem.hotel.dto.hotel.own.OwnHotelBrandDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.web.Message;
import com.tem.hotel.HbpRtnCodeConst;
import com.tem.hotel.api.hotel.HotelSettingService;
import com.tem.hotel.dto.hotel.room.ratePlan.HotelPaymentType;
import com.tem.hotel.dto.hotel.rule.PriceRuleDto;
import com.tem.hotel.dto.hotel.rule.PriceRuleTemplateDto;
import com.tem.hotel.dto.hotel.rule.enums.PriceAddType;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("hotel/priceRule")
public class HotelPriceRuleController extends BaseController {
	
	@Autowired
	private HotelSettingService settingService;
	@Autowired
	private PartnerService partnerService;
	@Autowired
	private OwnHotelBrandService ownHotelBrandService;
	@Autowired
	private OwnHotelService ownHotelService;
	@RequestMapping("index")
	public String index(Integer templateId,Model model){
		model.addAttribute("templateId",templateId);
		return "hotel/priceRule/index.base";
	}
	
	// 分页查询
	@RequestMapping(value = "/list")
	public String list(Model model, Integer pageIndex, Integer pageSize,String keyword,Integer templateId,String cId) {
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}
		
		QueryDto<Object> queryDto = new QueryDto<Object>(pageIndex,pageSize);
		
		if (keyword != null && !"".equals(keyword)) {
			queryDto.setField1(keyword);
		}
		if (templateId != null && !"".equals(templateId)) {
			queryDto.setField4(templateId);
		}
		if (cId != null && !"".equals(cId)) {
			queryDto.setField2(cId);
		}
		
		Page<PriceRuleDto> pageList = settingService.queryPriceRuleListWithPage(queryDto).getData();
		
		model.addAttribute("pageList", pageList);
		return "hotel/priceRule/ruleTable.jsp";
	}
	
	// 用于新增和修改时查询对象
	@RequestMapping(value = "/getRule")
	public String getCarrier(Model model, String id,String templateId) {
		PriceRuleDto priceRuleDto = null;
		List<OwnHotelDto> hotelList=new ArrayList<>();
		List<OwnHotelBrandDto> brandList=new ArrayList<>();
		if (id != null && !"".equals(id)) {
			priceRuleDto = settingService.findPriceRuleById(id).getData();
			priceRuleDto.setcName(partnerService.findById(Long.valueOf(priceRuleDto.getcId())).getName());
		}else if(StringUtils.isNotEmpty(templateId)){
			PriceRuleTemplateDto rule = settingService.findPriceRuleTemplateById(templateId).getData();
			priceRuleDto = new PriceRuleDto(rule);
		}else{
			priceRuleDto = new PriceRuleDto();
		}
		if(StringUtils.isNotEmpty(priceRuleDto.getBrandId())&&!priceRuleDto.getBrandId().contains("ALL")){
			OwnHotelBrandDto brand=ownHotelBrandService.getOwnBrandById(Long.valueOf(priceRuleDto.getBrandId())).getData();
			priceRuleDto.setBrandsDesc(brand.getShortName());
			model.addAttribute("brandsJSONString", JSONObject.toJSON(brand));
			brandList.add(brand);
		}
		String hotelIds=priceRuleDto.getHotelId();
		priceRuleDto.setSuitableHotelType(StringUtils.isEmpty(hotelIds)?1:(hotelIds.contains("ALL")?1:2));

		if(priceRuleDto.getSuitableHotelType()==2){
			hotelList=ownHotelService.findListByIds(hotelIds.split(",")).getData();
		}
		model.addAttribute("brandList",JSONObject.toJSON(brandList));
		model.addAttribute("hotelList",JSONObject.toJSON(hotelList));
		model.addAttribute("priceRuleDto", priceRuleDto);
		model.addAttribute("paymentTypes",HotelPaymentType.values());
		model.addAttribute("priceAddTypes",PriceAddType.values());
		model.addAttribute("templates",settingService.getAllTemplates().getData());
		return "hotel/priceRule/ruleModel.jsp";
	}
	
	@RequestMapping("getTemplate")
	@ResponseBody
	public PriceRuleTemplateFrom getTemplate(String id){
		PriceRuleTemplateFrom priceRuleTemplate=TransformUtils.transform(settingService.findPriceRuleTemplateById(id).getData(),PriceRuleTemplateFrom.class);
		if(StringUtils.isNotEmpty(priceRuleTemplate.getBrandId())&&!priceRuleTemplate.getBrandId().contains("ALL")){
			OwnHotelBrandDto brand=ownHotelBrandService.getOwnBrandById(Long.valueOf(priceRuleTemplate.getBrandId())).getData();
			priceRuleTemplate.setBrandsDesc(brand.getShortName());
		}
		String hotelIds=priceRuleTemplate.getHotelId();
		priceRuleTemplate.setSuitableHotelType(StringUtils.isEmpty(hotelIds)?1:(hotelIds.contains("ALL")?1:2));
		List<HotelBaseDto> hotelList=new ArrayList<>();
		if(priceRuleTemplate.getSuitableHotelType()==2){
			hotelList=ownHotelService.findListByIds(hotelIds.split(",")).getData();
		}
		priceRuleTemplate.setHotelList(hotelList);
		return priceRuleTemplate;
	}
	
	//保存对象
	@ResponseBody
	@RequestMapping(value="save")	
	public Message addOrUpdate(Model model,PriceRuleDto priceRuleDto){
		ResponseInfo info = settingService.savePriceRule(priceRuleDto);
		if (HbpRtnCodeConst.SUCCESS.equals(info.getCode())) {
			return Message.success("保存成功");
		} else {
			return Message.error(info.getMsg());
		}
	}
	
	// 批量删除
	@ResponseBody
	@RequestMapping("/delete")
	public Message delete(String ids) {
		String[] split = ids.split(",");
		boolean rs = settingService.batchDeletePriceRule(split).getData();
		if (rs) {
			return Message.success("删除成功");
		} else {
			return Message.error("删除失败，请刷新重试");
		}
	}
}
