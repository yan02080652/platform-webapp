package com.tem.action.protocol;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.PinyinUtil;
import com.tem.hotel.api.HotelDistrictService;
import com.tem.hotel.api.hotel.StaticDataService;
import com.tem.hotel.dto.HotelCityDto;
import com.tem.hotel.dto.hotel.HotelDistrictDto;
import com.tem.hotel.query.HotelDistrictQuery;
import com.tem.platform.action.BaseController;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Controller
@RequestMapping("hotel")
public class HotelCityController extends BaseController {

	private final String[] Alphas = { "ABC", "DEF", "GHI", "JKL", "MNO", "PQR", "STU", "VWX", "YZ" };

	@Autowired
	private StaticDataService staticDataService;
	@Autowired
	private HotelDistrictService hotelDistrictService;
	@RequestMapping(value = "findCityList",method = RequestMethod.GET)
	@ResponseBody
	public ResponseInfo getCityList(String cityName) {
		List<Map<String, String>> rs = new ArrayList<>();
		QueryDto<HotelDistrictQuery> queryDto=new QueryDto<>(1,100);
		HotelDistrictQuery hotelDistrictQuery=new HotelDistrictQuery();
		hotelDistrictQuery.setDistrictName(cityName);
		hotelDistrictQuery.setNotSuitableType("province");
		queryDto.setCondition(hotelDistrictQuery);
		queryDto.setSidx("CODE");
		queryDto.setSord("ASC");
		ResponseInfo res=hotelDistrictService.queryOwnPageList(queryDto);
		if(res.isSuccess()){
			Page<HotelDistrictDto> page =res.getData();
			for (HotelDistrictDto hotelDistrictDto :page.getList()) {
				Map<String, String> map = new HashMap<>();
				map.put("cityName",hotelDistrictDto.getDistrictName());
				map.put("cityCode",String.valueOf(hotelDistrictDto.getDistrictId()));
				rs.add(map);
			}
		}

		return new ResponseInfo("0",rs);
	}

	/**
	 * elong 城市选择器
	 * @return
	 */
	@RequestMapping(value = "cityData", method = RequestMethod.POST)
	@ResponseBody
	public String cityData() {
		Map<String, List<HotelCityDto>> cityData = new HashMap<>();
		List<HotelCityDto> hotCity = new ArrayList<>();
		
		List<HotelCityDto> citys = staticDataService.getAllCities().getData();
		Collections.sort(citys, new  Comparator<HotelCityDto>() {
			@Override
			public int compare(HotelCityDto o1, HotelCityDto o2) {
				String nameEn1 = o1.getNameEn();
				String nameEn2 = o2.getNameEn();
				
				if (StringUtils.isNotBlank(nameEn1)) {
					nameEn1 = nameEn1.substring(0, 1);
				} else {
					nameEn1 = "";
				}
				
				if (StringUtils.isNotBlank(nameEn2)) {
					nameEn2 = nameEn2.substring(0, 1);
				} else {
					nameEn2 = "";
				}
				
				return nameEn1.compareToIgnoreCase(nameEn2);
			}
			
			});
		for (HotelCityDto city : citys) {
			if(city.getIsHot()){
				hotCity.add(city);
			}
			String alpha = city.getNameEn();
			if (StringUtils.isBlank(alpha)) {
				alpha = PinyinUtil.getAlpha(city.getNameCn());
			}
			alpha = alpha.substring(0, 1).toUpperCase();
			for (int i = 0; i < Alphas.length; i++) {
				if (Alphas[i].indexOf(alpha) > -1) {
					List<HotelCityDto> list = cityData.get(Alphas[i]);
					if (CollectionUtils.isEmpty(list)) {
						list = new ArrayList<>();
					}
					list.add(city);
					cityData.put(Alphas[i], list);
					break;
				}
			}
		}
		cityData.put("0", hotCity);
		Map<String, List<HotelCityDto>> resultMap = sortMapByKey(cityData);	
		return JSONObject.toJSONString(resultMap,SerializerFeature.DisableCircularReferenceDetect);

	}
	
	/**
	 * 使用 Map按key进行排序
	 * 
	 * @param cityData
	 * @return
	 */
	public static Map<String, List<HotelCityDto>> sortMapByKey(Map<String, List<HotelCityDto>> cityData) {
		if (cityData == null || cityData.isEmpty()) {
			return null;
		}

		Map<String, List<HotelCityDto>> sortMap = new TreeMap<String, List<HotelCityDto>>(new Comparator<String>() {
			@Override
			public int compare(String str1, String str2) {
				return str1.compareTo(str2);
			}
		});

		sortMap.putAll(cityData);

		return sortMap;
	}

}
