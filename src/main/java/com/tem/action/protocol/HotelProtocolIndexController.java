package com.tem.action.protocol;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.tem.hotel.api.hotel.HotelBaseService;
import com.tem.hotel.api.hotel.StaticDataService;
import com.tem.hotel.api.protocol.ProtocolHotelService;
import com.tem.hotel.dto.HotelCityDto;
import com.tem.hotel.dto.hotel.HotelBaseDto;
import com.tem.hotel.dto.hotel.HotelCondition;
import com.tem.hotel.dto.hotel.HotelDetailDto;
import com.tem.hotel.dto.hotel.HotelDistrictDto;
import com.tem.hotel.dto.hotel.enums.OriginType;
import com.tem.action.form.HotelBaseForm;
import com.tem.action.form.OriginTypeForm;
import com.tem.platform.action.BaseController;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("hotel/protocol")
public class HotelProtocolIndexController extends BaseController {
	@Autowired
	private ProtocolHotelService protocolHotelService ;
	
	@Autowired
	private StaticDataService staticDataService;	
	
	@Autowired
	private HotelBaseService hotelBaseService;	
	
	@RequestMapping("/index")
	public String index(Model model){
		model.addAttribute("originType",OriginType.values());
		return "hotel/protocol/index.base";
		
	}

	// 数据源Map
	private Map<String, String> getOriginMap() {
		Map<String,String> map = new HashMap<String, String>();
		for(OriginType c: OriginType.values()){
			map.put(c.getMessage(), c.getName());
		}
		return map;
	}
	
	@RequestMapping(value="/hotelMatchList")
	public String hotelMatchList(){
		return null;
	}
	@RequestMapping(value="/toHoteRates",method = RequestMethod.GET)
	public String  toHoteRates(Model model,Long hotelId){
		model.addAttribute("hotelId", hotelId);
		return "hotel/protocol/hotelBasicIndex.base";
	}
    @RequestMapping(value="/serachHotel")
	public String serachHotel(Model model, Integer pageIndex,String hotelName, Long protocolComId,String originType,String cityId){
    	if (pageIndex == null) {
			pageIndex = 1;
		}
    
		QueryDto<HotelCondition> queryDto = new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);
		HotelCondition condition = new HotelCondition();
		if ("0".equals(originType)) {
			condition.setOriginType(null);
		}else{
			condition.setOriginType(originType);
		}
		if(StringUtils.isNotEmpty(hotelName)){
			condition.setHotelName(hotelName);
		}
		if(protocolComId != null){
			condition.setProtocolComId(protocolComId);
		}
		if(cityId != null){
			condition.setCityId(cityId);
		}
		queryDto.setCondition(condition);
		
	    Page<HotelBaseDto> pageList = hotelBaseService.queryPageListNew(queryDto).getData();
	    
	    //转译发起人
  		List<HotelBaseDto> hotelBaseDtoList = pageList.getList();
  		List<HotelBaseForm> resultList = new ArrayList<HotelBaseForm>();
  		if(CollectionUtils.isNotEmpty(hotelBaseDtoList)){
  			List<HotelDistrictDto> cityList = staticDataService.getAllOriginCities().getData();
  			HotelBaseForm hotelBaseFrom = null;
  			for(HotelBaseDto hotelBaseDto : hotelBaseDtoList){
  				hotelBaseFrom = TransformUtils.transform(hotelBaseDto, HotelBaseForm.class);
  				hotelBaseFrom.setOriginTypeName(OriginType.getByMessage(hotelBaseDto.getOriginType()).getName());
  				for (HotelDistrictDto city : cityList) {
	    			if (city.getOrginType().equals(hotelBaseDto.getOriginType()) && city.getCode().equals(hotelBaseDto.getCityId())) {
	    				hotelBaseFrom.setCityName(city.getNameCn());
	    				break;
	    			}
	    		}
  				resultList.add(hotelBaseFrom);
  			}
  		}
  		Page<HotelBaseForm> resultPage = new Page<HotelBaseForm>(pageList.getStart(), pageList.getSize(), resultList, pageList.getTotal());
	  		
		model.addAttribute("pageList", resultPage);
		model.addAttribute("originMap", getOriginMap());
    	return "hotel/protocol/serach.jsp";
     }
    @RequestMapping("saveOrUpdateHotel")
    public String saveOrUpdateHotel(HotelDetailDto hotel) {
		protocolHotelService.saveOrUpdateHotelBasicInfo(hotel);
    	return null;
    }
	
}
