package com.tem.action.protocol;

import com.iplatform.common.ResponseInfo;
import com.iplatform.common.web.Message;
import com.tem.hotel.HbpRtnCodeConst;
import com.tem.hotel.api.hotel.HotelDataLoadService;
import com.tem.hotel.api.protocol.ProtocolHotelRoomService;
import com.tem.hotel.api.protocol.ProtocolHotelService;
import com.tem.hotel.dto.hotel.enums.OriginType;
import com.tem.hotel.dto.hotel.room.HotelRoomDto;
import com.tem.platform.action.BaseController;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("hotel/protocol")
public class HotelProtocolController extends BaseController {
	
	@Autowired
	private ProtocolHotelService protocolHotelService;
	
	@Autowired
	private ProtocolHotelRoomService protocolHotelRoomService;

	@Autowired
	private HotelDataLoadService hotelDataLoadService;

	@RequestMapping(value = "editHotelRooms" , method = RequestMethod.POST)
	public String editHotelRooms(Model model,Long hotelId){
		
		return "hotel/protocol/editHotelRooms.jsp";
	}
	@RequestMapping(value = "editPricePlan" , method = RequestMethod.POST)
	public String editPricePlan(Model model,Long hotelId){
		
		return "hotel/protocol/editPricePlan.jsp";
	}
	@RequestMapping(value="toAddHouseType" , method = RequestMethod.POST)
	public String toAddHouseType(){
		
		return "hotel/protocol/addHouseType.jsp";
	}
	@RequestMapping(value="toAddPricingPlan",method = RequestMethod.POST)
	public String  toAddPricingPlan(){
		
		return "hotel/protocol/addPricingPlan.jsp";
	}
	@RequestMapping(value="hotelBasicInformation")
	public String hotelBasicInformation(Model model,Long hotelId){
		return "hotel/protocol/hotelBasicInformation.jsp";
	}
	
	@RequestMapping(value="AddHouseType" , method = RequestMethod.POST)
	@ResponseBody
	public Message AddHouseType(HotelRoomDto dto){
		System.out.println(dto.getHotelId());
		Boolean save = protocolHotelRoomService.save(dto).getData();
		if(save) {
			return Message.success("保存成功");
		}
		return Message.error("保存失败");
	}

	/**
	 * 酒店同步
	 * @param hotelId
	 * @return
     */
	@RequestMapping("syncHotels")
	@ResponseBody
	public ResponseInfo syncHotels(String originType,String hotelId){
		OriginType origin = OriginType.getByMessage(originType);
		if (StringUtils.isBlank(originType) || origin == null) {
			return new ResponseInfo(HbpRtnCodeConst.ERROR,"请选择数据源",null);
		}
		if (StringUtils.isBlank(hotelId) ){
			return new ResponseInfo(HbpRtnCodeConst.ERROR,"请填写酒店ID",null);
		}
		List<String> hotelIds = Arrays.asList(hotelId.split(","));
		return  hotelDataLoadService.syncHotelListWithMatch(hotelIds,origin);
	}

}
