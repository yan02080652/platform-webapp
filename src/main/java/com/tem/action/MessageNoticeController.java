package com.tem.action;


import java.util.UUID;


import com.iplatform.common.Config;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.web.Message;
import com.tem.imgserver.client.ImgClient;
import com.tem.notify.api.MessageNoticeService;
import com.tem.notify.dto.MessageAllNoticeDto;
import com.tem.notify.dto.MessageNoticeCondition;
import com.tem.notify.dto.MessageNoticeDto;
import com.tem.platform.action.BaseController;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 公告/头条信息
 * @author llf 
 *
 */
@Controller
@RequestMapping("notify/notice")
public class MessageNoticeController extends BaseController {
	
	@Autowired
	private MessageNoticeService messageNoticeService;
	
	@RequestMapping(value = "")
	public String notifyNoticePage(Model model,@RequestParam(value="pageIndex",required=false)Integer pageIndex,
			@RequestParam(value="title",required=false)String title) {
		
		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<MessageNoticeCondition> queryDto = new QueryDto<MessageNoticeCondition>(pageIndex,Page.DEFAULT_PAGE_SIZE);

		MessageNoticeCondition condition = new MessageNoticeCondition();
		
		if(StringUtils.isNotEmpty(title)){
			condition.setTitle(title);
		}
		queryDto.setCondition(condition);
	   
		Page<MessageNoticeDto> messageTmplList = messageNoticeService.queryListWithPage(queryDto);
		model.addAttribute("pageList", messageTmplList);
		model.addAttribute("title", title);
		return "notify/notice/noticeList.base";
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{id}")
	public Message delete(Model model, @PathVariable("id") Long id) {
		if (id == null) {
			return Message.error("不存在的模板");
		}
		messageNoticeService.deleteById(id);
		return Message.success("操作成功");
	}
	
	@RequestMapping("/detail")
	public String detail(Model model,@RequestParam(value="id",required=false) Long id) {
		MessageAllNoticeDto messageAllNotice = null;
		if(id == null){//新增
			messageAllNotice = new MessageAllNoticeDto();
		}else{
			messageAllNotice = messageNoticeService.selectById(id);
		}
		model.addAttribute("messageNotice", messageAllNotice);
		return "notify/notice/messageNotice.base";
	}

	@ResponseBody
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
	public Message saveOrUpdate(MessageAllNoticeDto messageAllNoticeDto) {
		
		if (messageAllNoticeDto.getId() == null) {			
			messageNoticeService.insert(messageAllNoticeDto);
			return Message.success("新增成功");
		} else {
			messageNoticeService.update(messageAllNoticeDto);
			return Message.success("修改成功");
		}
	}
	
	//上传图片
	@ResponseBody
	@RequestMapping(value="/picture", method = RequestMethod.POST)	
	public Message savePicture(Model model,MultipartFile picture){
		
		try {
			// 图片上传
			if (picture != null) {
				String fileName = picture.getOriginalFilename();
				if (StringUtils.isNotEmpty(fileName)) {
					// 获取后缀名
					String img_suffix = fileName.substring(fileName.lastIndexOf("."));
					// 生成一个新的图片名
					String newName = UUID.randomUUID() + img_suffix;
					// 图片存放的位置
					String path = Config.getString("noticeRoot");
					
					ImgClient.uploadImg(picture, path,newName);
					model.addAttribute("path", path + "/" + newName);
					
					return Message.success(path + "/" + newName);
				}
			}
			return Message.success("保存成功");
		} catch (Exception e) {
			return Message.error("文件上传出错，请刷新重试");
		}
		
	}

}
