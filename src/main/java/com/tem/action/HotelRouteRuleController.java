package com.tem.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.web.Message;
import com.tem.flight.dto.routeRule.RouteRuleItemDto;
import com.tem.hotel.api.hotel.HotelHubConnectorService;
import com.tem.hotel.api.hotel.HotelRouteRuteService;
import com.tem.hotel.api.hotel.HotelSettingService;
import com.tem.hotel.dto.HotelHubsDto;
import com.tem.hotel.dto.hotel.enums.RuleGradeType;
import com.tem.hotel.dto.hotel.rule.HotelRouteRuleItemDto;
import com.tem.hotel.dto.hotel.rule.RouteRuleDto;
import com.tem.hotel.result.HotelHubsConnectResult;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.PartnerBpService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;

@Controller
@RequestMapping("hotel/routeRule")
public class HotelRouteRuleController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(HotelRouteRuleController.class);
	public static final String FAILED = "-1";
	public static final String SUCCESS = "0";
	@Autowired
	private HotelSettingService settingService;
	@Autowired
	private PartnerService partnerService;
	@Autowired
	private PartnerBpService bpService;
	@Autowired
	private HotelHubConnectorService hotelHubConnectorService;
	@Autowired
	private HotelRouteRuteService hotelRouteRuteService;
    @Autowired
    private DictService dictService;
    @Autowired
    private UserService userService;
    @Autowired
    private PartnerBpService partnerBpService;

	@RequestMapping("")
	public String routeRule(Model model){
		model.addAttribute("gradeMap",RuleGradeType.values());
		return "hotel/routeRule/index.base";

	}
	
	// 分页查询
	@RequestMapping(value = "/list")
	public String list(Model model, Integer pageIndex, Integer pageSize,RouteRuleDto condition) {
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}
		QueryDto<RouteRuleDto> queryDto = new QueryDto<>(pageIndex,pageSize);
		queryDto.setCondition(condition);

		Page<RouteRuleDto> pageList = hotelRouteRuteService.queryRouteRuleListWithPage(queryDto).getData();
	
		model.addAttribute("pageList", pageList);
		return "hotel/routeRule/ruleTable.jsp";
	}
	
//	// 用于新增和修改时查询对象
	@RequestMapping(value = "/getRule")
	public String getRule(Model model, String id) {
		RouteRuleDto routeRuleDto = null;
		if (StringUtils.isNotBlank(id)) {
			routeRuleDto = hotelRouteRuteService.findRouteRuleById(id).getData();
			String scope = routeRuleDto.getScope();
	            if (scope != null) {
	                PartnerDto partnerDto = partnerService.findById(Long.valueOf(scope));
	                if (partnerDto != null) {
	                    routeRuleDto.setScopeName(partnerDto.getName());
	                }
	            }
	            //创建人
	            UserDto creator = userService.getUser(routeRuleDto.getCreater());
	            routeRuleDto.setCreaterName(creator.getFullname());
	            //最后修改人
	            if(routeRuleDto.getLastModifier()!=null) {
	            	UserDto lastModifier = userService.getUser(routeRuleDto.getLastModifier());
	 	            routeRuleDto.setModifierName(lastModifier.getFullname());
	            }
		}else {
			routeRuleDto = new RouteRuleDto();
		}
		
		model.addAttribute("routeRuleDto", routeRuleDto);
		//连接器
//		model.addAttribute("hubs",hotelHubConnectorService.findAll(true));
		return "hotel/routeRule/ruleModel.jsp";
	}
	
//
//	/**
//	 * 根据hubscode 获取连接
//	 * @return
//     */
//	@RequestMapping("getConnects")
//	@ResponseBody
//	public List<HotelHubsConnectResult> getConnects(RouteRuleDto routeRuleDto){
//		HotelHubsConnectQuery query = new HotelHubsConnectQuery();
//		query.setHubsCode(routeRuleDto.GET);
//		query.setIsDisable(false);
//		if(routeRuleDto.getRuleGrade() == RuleGradeType.PLATFORM){
//			query.setTmcId(routeRuleDto.getScope());
//		}else{
//			query.setTmcId(bpService.findTmcIdByPId(Long.valueOf(routeRuleDto.getScope())) + "");
//		}
//		return hotelHubConnectorService.findConnect(query).getData();
//	}

	/**
	 * 根据企业获取连接器
	 * @param routeRuleDto
	 * @return
     */
	@RequestMapping("getHubs")
	@ResponseBody
	public List<HotelHubsDto> getHubs(RouteRuleDto routeRuleDto){
		Long tmcId = null;
		if(routeRuleDto.getRuleGrade() == RuleGradeType.PLATFORM){
			tmcId = Long.valueOf(routeRuleDto.getScope());
		}else{
			tmcId = bpService.findTmcIdByPId(Long.valueOf(routeRuleDto.getScope()));
		}
		return hotelHubConnectorService.getHubs(tmcId,false).getData();
	}

	//保存对象
	@ResponseBody
	@RequestMapping(value="save")
	public ResponseInfo addOrUpdate(RouteRuleDto routeRuleDto){
		boolean result = true;
		if(routeRuleDto.getId()==null) {
			result = hotelRouteRuteService.isExist(Integer.valueOf(routeRuleDto.getScope())).getData();
		}
		if(result) {
			routeRuleDto.setCreater(getCurrentUser().getId());
			routeRuleDto.setLastModifier(getCurrentUser().getId());
			routeRuleDto = hotelRouteRuteService.saveRouteRule(routeRuleDto).getData();
			return new ResponseInfo(SUCCESS, "保存成功", routeRuleDto);
		}else {
			return new ResponseInfo(FAILED, "已存在tmc/企业级别的对应规则,无法重复添加,请检查!", routeRuleDto);
		}
		
	    
	}

	// 批量删除
	@ResponseBody
	@RequestMapping("/delete")
	public Message delete(String ids) {
		String[] split = ids.split(",");
		boolean rs = hotelRouteRuteService.batchDeleteRouteRules(split).getData();
		if (rs) {
			return Message.success("删除成功");
		} else {
			return Message.error("删除失败，请刷新重试");
		}
	}
	
	
	
	@RequestMapping("/getRouteRuleItem")
    public String getRouteRuleItem(Model model, Integer id, Integer routeRuleId,Integer areaType) {
        //查询配置项
        HotelRouteRuleItemDto itemDto = hotelRouteRuteService.getRouteRuleItemById(id).getData();
        //根据配置id查询该tmc下的可用连接账号
        RouteRuleDto routeRuleDto = hotelRouteRuteService.findRouteRuleById(routeRuleId.toString()).getData();
        if (routeRuleDto != null) {
            //tmc级别
            Long tmcId;
            if (1 == routeRuleDto.getRuleGrade().getCode()) {
                tmcId = Long.valueOf(routeRuleDto.getScope());
            } else {
                //根据pid查询tmcId
                tmcId = partnerBpService.findTmcIdByPId(Long.valueOf(routeRuleDto.getScope()));
            }
            List<HotelHubsConnectResult> hubsConnectList = hotelHubConnectorService.findListByTmcId(tmcId,areaType).getData();
            if(hubsConnectList !=null) {
	            //根据hubsCode分组
	            Map<String, List<HotelHubsConnectResult>> connectMap = new HashMap<>(16);
	            for (HotelHubsConnectResult dto : hubsConnectList) {
	                if (connectMap.containsKey(dto.getHubsCode())) {
	                    connectMap.get(dto.getHubsCode()).add(dto);
	                } else {
	                    List<HotelHubsConnectResult> list = new ArrayList<>();
	                    list.add(dto);
	                    connectMap.put(dto.getHubsCode(), list);
	                }
	            }
	            model.addAttribute("connectMap", connectMap);
            }
        }

        model.addAttribute("itemDto", itemDto);
        model.addAttribute("routeRuleId", routeRuleId);
        return "hotel/routeRule/item.single";
    }
	
	
	@ResponseBody
    @RequestMapping("/saveRouteRuleItem")
    public boolean saveRouteRuleItem(HotelRouteRuleItemDto itemDto) {
        return hotelRouteRuteService.saveRouteRuleItem(itemDto).isSuccess();
    }
	
	

    @ResponseBody
    @RequestMapping("/deleteRouteRuleItem")
    public boolean deleteRouteRuleItem(Integer id) {
        return hotelRouteRuteService.deleteRouteRuleItem(id).isSuccess();
    }
}
