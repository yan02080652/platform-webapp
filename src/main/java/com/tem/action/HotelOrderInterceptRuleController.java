package com.tem.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.tem.hotel.api.hotel.own.OwnHotelBrandService;
import com.tem.hotel.dto.hotel.own.OwnHotelBrandCondition;
import com.tem.hotel.dto.hotel.own.OwnHotelBrandDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.cache.utils.RedisCacheUtils;
import com.iplatform.common.web.Message;
import com.tem.hotel.HbpRtnCodeConst;
import com.tem.hotel.api.hotel.HotelBaseService;
import com.tem.hotel.api.hotel.HotelBrandService;
import com.tem.hotel.api.hotel.HotelHubConnectorService;
import com.tem.hotel.api.hotel.StaticDataService;
import com.tem.hotel.api.order.HotelOrderInterceptScopeService;
import com.tem.hotel.api.order.HotelOrderInterceptService;
import com.tem.hotel.dto.HotelHubsDto;
import com.tem.hotel.dto.hotel.HotelBaseDto;
import com.tem.hotel.dto.hotel.HotelBrandDto;
import com.tem.hotel.dto.hotel.enums.PolicyApplyType;
import com.tem.hotel.dto.hotel.enums.StarRateType;
import com.tem.hotel.dto.hotel.room.HotelRoomDto;
import com.tem.hotel.dto.order.HotelOrderInterceptDto;
import com.tem.hotel.dto.order.HotelOrderInterceptScopeDto;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.form.ResponseTable;

/**
 * @author: wangzheqi
 * @description:
 * @date: 创建于: 2017-11-03 15:05
 * @modified By:
 */
@Controller
@RequestMapping("hotel/orderInterceptRule")
public class HotelOrderInterceptRuleController extends BaseController{



    @Autowired
    private HotelOrderInterceptService hotelOrderInterceptService;

    @Autowired
    private StaticDataService staticDataService;

    @Autowired
    private HotelBaseService hotelBaseService;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private UserService userService;

    @Autowired
    private HotelOrderInterceptScopeService hotelOrderInterceptScopeService;

    @Autowired
    private HotelHubConnectorService hotelHubConnectorService;

    @Autowired
    private OwnHotelBrandService ownHotelBrandService;

    @RequestMapping("index")
    public String index(Model model) {
        model.addAttribute("isAdmin",super.isSuperAdmin());
        return "hotel/orderInterceptRule/index.base";
    }

    // 分页查询
    @RequestMapping(value = "/list")
    public String list(Model model, Integer pageIndex, Integer pageSize,String keyWords,Long partnerIds) {
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        QueryDto<HotelOrderInterceptDto> queryDto = new QueryDto<HotelOrderInterceptDto>(pageIndex,pageSize);
        HotelOrderInterceptDto dto = new HotelOrderInterceptDto();
        dto.setKeyWords(keyWords);
        if(!super.isSuperAdmin()) {
        	dto.setTmcId(super.getTmcId());
        }
        dto.setPartnerId(partnerIds);
        queryDto.setCondition(dto);
        Page<HotelOrderInterceptDto> pageList = hotelOrderInterceptService.queryOrderInterceptRuleListWithPage(queryDto).getData();
        if(pageList.getList()!=null) {
        	for(HotelOrderInterceptDto interceptDto:pageList.getList()) {
        		interceptDto.setPartnerName(partnerService.getNameById(interceptDto.getPartnerId()));
        	}
        }
        model.addAttribute("pageList", pageList);
        return "hotel/orderInterceptRule/ruleTable.jsp";
    }

    @RequestMapping("getBasic")
    public String getBasic(Model model, Integer interceptId, boolean isClone) {
        HotelOrderInterceptDto ruleDto = null;
        if (interceptId != null) {
              ruleDto = hotelOrderInterceptService.getOrderInterceptRuleById(interceptId).getData();
              ruleDto.setPartnerName(partnerService.getNameById(ruleDto.getPartnerId()));
              model.addAttribute("hubJSONString",JSONObject.toJSON(ruleDto.getOriginCodes()));
        } else {
            ruleDto = new HotelOrderInterceptDto();
            ruleDto.setStatus(true);
            ruleDto.setTmcId(super.getTmcId());
            ruleDto.setPartnerId(super.getTmcId());
            ruleDto.setApplyType(PolicyApplyType.All.getValue());
            ruleDto.setRoomNames(new String[]{"all"});
            ruleDto.setCreateDate(new Date());
            ruleDto.setCode(produceCode());
            //----------------------------------------------------------------------------
         }
        model.addAttribute("ruleDto", ruleDto);
        //所有渠道
        List<HotelHubsDto> allOrgin=hotelHubConnectorService.getHubs(super.getTmcId(),false).getData();
        //在途自有渠道
        HotelHubsDto  self=new HotelHubsDto();
        self.setHubsName("在途直销政策");
        self.setHubsCode("self");
        allOrgin.add(self);
        model.addAttribute("isClone",isClone);
        model.addAttribute("allOrgin",allOrgin);
        model.addAttribute("starRateTypes", StarRateType.values());
        return "hotel/orderInterceptRule/basicModel.jsp";
    }

    @RequestMapping("getHotelMsg")
    public String getHotelMsg(Model model, Integer interceptId, boolean isClone) {
    	   HotelOrderInterceptDto ruleDto  = hotelOrderInterceptService.getOrderInterceptRuleById(interceptId).getData();
           if (!StringUtils.isEmpty(ruleDto.getBrandId())) {
           ruleDto.setBrandsDesc(((OwnHotelBrandDto)ownHotelBrandService.getOwnBrandById(Long.valueOf(ruleDto.getBrandId())).getData()).getShortName());
           }
           if (ruleDto.getUpdaterId() != null) {
	               ruleDto.setUpdaterName(userService.getUser(ruleDto.getUpdaterId()).getFullname());
	       }
           //设置json字符串
           Set<String> stars=new HashSet<String>();
           if(ruleDto.getStars()!=null) {
         	  for(String star:ruleDto.getStars()) {
         		  StarRateType  srt= StarRateType.getStarRateTypeByContainStar(star);
         		  if(srt!=null) {
         			  stars.add(srt.getStar());
         		  }
         	  }
           }
           String[] rooms=ruleDto.getRoomNames();
           List<HotelRoomDto> roomsList=new ArrayList<>();
           if(rooms!=null &&!rooms[0].equals("all")) {
               for (String item : rooms) {
                   HotelRoomDto roomDto=new HotelRoomDto();
                   roomDto.setName(item);
                   roomsList.add(roomDto);
               }
           }
           model.addAttribute("rooms",JSONObject.toJSON(roomsList));
           model.addAttribute("starsJSONString", JSONObject.toJSON(stars));
           model.addAttribute("hubJSONString",JSONObject.toJSON(ruleDto.getOriginCodes()));
           model.addAttribute("ruleDto", ruleDto);
           model.addAttribute("starRateTypes", StarRateType.values());
        return "hotel/orderInterceptRule/hotelModel.jsp";
    }
    @RequestMapping("toRule")
    public String toRule(Model model, Integer id, boolean isClone) {
        model.addAttribute("id",id);
        model.addAttribute("isClone",isClone);
        return "hotel/orderInterceptRule/ruleModel.jsp";
    }



    //保存基础信息
    @ResponseBody
    @RequestMapping(value="saveBasic")
    public ResponseInfo saveBasic(Model model, HotelOrderInterceptDto ruleDto){

    	ResponseInfo info=new ResponseInfo();
    	initDtoBaisc(ruleDto);
    	if(ruleDto.getId()==null) {
    		ruleDto.setCreateDate(new Date());
            ruleDto.setCreatorId(super.getCurrentUser().getId());
    		info = hotelOrderInterceptService.insertOrderInterceptRule(ruleDto);
    	} else {
    		if(ruleDto.getIsClone()) {
    			ruleDto.setCreateDate(new Date());
                ruleDto.setCreatorId(super.getCurrentUser().getId());
    			ruleDto.setCode(produceCode());
    			info = hotelOrderInterceptService.getDtoByclone(ruleDto);
    		} else {
    			ruleDto.setUpdateDate(new Date());
    			ruleDto.setUpdaterId(super.getCurrentUser().getId());
    			info=hotelOrderInterceptService.updateStatus(ruleDto);
    		}
    	}
        if (HbpRtnCodeConst.SUCCESS.equals(info.getCode())) {
        	info.setMsg("保存成功");
            return info;
        } else {
        	info.setMsg("保存成功");
            return info;
        }
    }


   //保存酒店信息
    @ResponseBody
    @RequestMapping(value="saveHotel")
    public Message saveHotel(Model model, HotelOrderInterceptDto ruleDto){
    	if(ruleDto.getIsLimitStar()==null) {
    	   ruleDto.setIsLimitStar(false);
    	}
    	if(ruleDto.getIsLimitStar()!=null) {
    		if(ruleDto.getIsLimitStar()) {
    		   ruleDto.setStars(null);
    		}
    	}
    	ruleDto.setUpdateDate(new Date());
		ruleDto.setUpdaterId(super.getCurrentUser().getId());
    	ResponseInfo info = hotelOrderInterceptService.updateOrderInterceptRule(ruleDto);
        if (HbpRtnCodeConst.SUCCESS.equals(info.getCode())) {
        	return Message.success("保存成功");
        } else {
        	return Message.error("保存成功");
        }
    }

    //修改拦截状态
    @ResponseBody
    @RequestMapping(value="updateStatus")
    public Message updateStatus(Model model, HotelOrderInterceptDto ruleDto){
        HotelOrderInterceptDto hotelOrderInterceptDto  = hotelOrderInterceptService.getOrderInterceptRuleById(ruleDto.getId()).getData();
        hotelOrderInterceptDto.setCreateDate(new Date());
        hotelOrderInterceptDto.setStatus(ruleDto.getStatus());
    	ResponseInfo info = hotelOrderInterceptService.updateStatus(hotelOrderInterceptDto);
        if (HbpRtnCodeConst.SUCCESS.equals(info.getCode())) {
        	return Message.success("保存成功");
        } else {
        	return Message.error("保存失败");
        }
    }
    //删除对象
    @ResponseBody
    @RequestMapping(value="delete")
    public Message delete(Integer id){
        if (hotelOrderInterceptService.deleteOrderInterceptRuleById(id).getData()) {
            return Message.success("删除成功");
        } else {
            return Message.error("删除失败");
        }
    }

    /**
     * 酒店分页查询
     * @return
     */
    @ResponseBody
    @RequestMapping("hotelData")
    public ResponseTable hotelData(){
        Integer pageIndex = super.getIntegerValue("pageIndex");
        Integer pageSize = super.getIntegerValue("pageSize");
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }
        String name = super.getStringValue("name");
        String cityCode=super.getStringValue("cityCode");
        QueryDto<String> queryDto = new QueryDto<>(pageIndex,pageSize);
        queryDto.setCondition(name);
        queryDto.setField1(cityCode);
        Page<HotelBaseDto> hotelBaseDtoPage = hotelBaseService.queryPageList(queryDto).getData();
        return new ResponseTable(hotelBaseDtoPage);
    }



    /**
     * 品牌分页查询
     * @return
     */
    @ResponseBody
    @RequestMapping("brandData")
    public ResponseTable brandData(){
        Integer pageIndex = super.getIntegerValue("pageIndex");
        Integer pageSize = super.getIntegerValue("pageSize");
        if(pageIndex == null){
            pageIndex = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }
        String name = super.getStringValue("name");
        QueryDto<OwnHotelBrandCondition> queryDto = new QueryDto<>(pageIndex,pageSize);
        OwnHotelBrandCondition condition = new OwnHotelBrandCondition();
        condition.setName(name);
        queryDto.setCondition(condition);
        Page<OwnHotelBrandDto> page = ownHotelBrandService.queryOwnBrandList(queryDto).getData();
        return new ResponseTable(page);
    }

    private String produceCode() {
    	String code;
    	Long temp=RedisCacheUtils.incrBy("hotelOrderInterceptCode",(long) 1);
    	code="I"+(temp+1000);
    	return code;
    }
    /**
     * 获取酒店列表
     * @param policyId 拦截id
     * @param brandId 品牌id
     * @param hotelName 酒店名称
     * @return
     */
    @RequestMapping(value="interceptHotelList")
    public String getInterceptHotelList(Model model, QueryDto<HotelOrderInterceptScopeDto> queryDto ,@RequestParam(value="interceptId",required = true) Integer interceptId,
           String brandId, String hotelName,Integer pageIndex,Integer pageSize){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        queryDto=new QueryDto<>(pageIndex,pageSize);
        HotelOrderInterceptScopeDto hotelOrderInterceptScopeDto=new HotelOrderInterceptScopeDto();
        hotelOrderInterceptScopeDto.setInterceptId(interceptId);
        hotelOrderInterceptScopeDto.setHotelBrandId(brandId);
        hotelOrderInterceptScopeDto.setHotelName(hotelName);
        queryDto.setCondition(hotelOrderInterceptScopeDto);

        Page<HotelOrderInterceptScopeDto> pageList=hotelOrderInterceptScopeService.queryApplyScopeListWithPage(queryDto).getData();
        model.addAttribute("pageList",pageList);
        model.addAttribute("hotelNum",pageList.getList().size());
        model.addAttribute("hotelName",hotelName);
        return "hotel/orderInterceptRule/interceptHotel.jsp";
    }
    @RequestMapping("addHotels")
    @ResponseBody
    public Message addHotels(Integer interceptId,String hotels, String brandId){
        List<HotelBaseDto> hotelList= JSONObject.parseArray(hotels,HotelBaseDto.class);
        List<HotelBaseDto> addHotelList=new ArrayList<>();
        String hotelIds=hotelOrderInterceptScopeService.getAllHotelIdsByInterceptIdAndBrandId(interceptId,brandId).getData();
        if(StringUtils.isNotEmpty(hotelIds)){
            String[] ids=hotelIds.split(",");
            for (HotelBaseDto hotelBaseDto:hotelList) {
                boolean exists=false;
                for (String id:ids){
                    if(id.equals(hotelBaseDto.getHotelId())){
                        exists=true;
                        break;
                    }
                }
                if(!exists){
                    addHotelList.add(hotelBaseDto);
                }
            }
        }else{
            addHotelList=hotelList;
        }
        if(addHotelList.size()==0){
            return Message.success("添加成功!");
        }
        ResponseInfo res=hotelOrderInterceptScopeService.insertApplyScope(interceptId,addHotelList,brandId);
        if(res.isSuccess()){
            return Message.success("添加成功!");
        }else{
            return Message.error("添加失败!");
        }
    }
    /**
     * 酒店删除
     * @param ids
     * @return
     */
    @RequestMapping("deleteHotels")
    @ResponseBody
    public Message deleteInterceptHotels(String ids){
        List<Integer> hotelIdList=JSONObject.parseArray(ids,Integer.class);
        Integer[] hotelIds=hotelIdList.toArray(new Integer[0]);
        if(hotelIds.length==0) {
            return Message.error("删除错误!");
        }
        ResponseInfo res=hotelOrderInterceptScopeService.deleteApplyScopeById(hotelIds);
        if(res.isSuccess()){
            return Message.success("删除成功!");
        }else{
            return Message.error("删除错误!");
        }
    }

    /**
     * 获取酒店房型列表
     * @param brandId 品牌id
     * @param applyType 适用类型
     * @return
     */
    @RequestMapping(value="roomContent")
    @ResponseBody
    public ResponseTable getRoomContent(String brandId,Integer interceptId,Integer applyType,String name,Integer pageIndex,Integer pageSize){
    	 List<HotelOrderInterceptScopeDto> list=new ArrayList<HotelOrderInterceptScopeDto>();
    	 list=hotelOrderInterceptScopeService.getAllHotelByInterceptIdAndBrandId(interceptId, brandId).getData();

    	 List<String> hotelIds=new ArrayList<String>();
    	 for(HotelOrderInterceptScopeDto scopeDto:list) {
    		 hotelIds.add(scopeDto.getHotelId());
    	 }
    	 if (pageIndex == null) {
              pageIndex = 1;
          }
          if (pageSize == null) {
              pageSize = Page.DEFAULT_PAGE_SIZE;
          }
          HotelRoomDto dto=new HotelRoomDto();
          dto.setBrandId(brandId);
          dto.setHotelIds(hotelIds.toArray(new String[hotelIds.size()]));
          dto.setApplyType(applyType);
          dto.setKeyWords(name);
          QueryDto queryDto=new QueryDto<>(pageIndex,pageSize);
          queryDto.setCondition(dto);
          ResponseInfo res=hotelOrderInterceptScopeService.getRoomsByCondition(queryDto);
          if(res.isSuccess()) {
        	  Page<String> result=res.getData();
        	  return new ResponseTable(result);
          }
          return new ResponseTable();
    }

    public void initDtoBaisc(HotelOrderInterceptDto dto) {
    	if(dto.getIsLimitOrigin()==null) {
    		dto.setIsLimitOrigin(false);
    	}
    	if(dto.getIsIncludeWeek()==null) {
    		dto.setIsIncludeWeek(false);
    	}
    	if(dto.getIsIncludeWork()==null) {
    		dto.setIsIncludeWork(false);
    	}
    	if(dto.getIsLimitStar()==null) {
    		dto.setIsLimitStar(false);
     	}
    	if(dto.getLevel().equals("tmc")) {
    		if(super.isSuperAdmin()) {
    			dto.setTmcId(dto.getPartnerId());
    		}else {
    			dto.setPartnerId(dto.getTmcId());
    		}
    	}
    }
}