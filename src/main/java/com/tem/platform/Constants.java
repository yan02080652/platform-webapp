package com.tem.platform;

public class Constants {
	public static final String COMMON = "COMMON";//公共系统标志 
	
	public static final String PLATFORM = "PLATFORM";//当前系统分类 
	
	public static final String IP_SYS_LIST = "IP_SYS_LIST";//系统列表字典
	
	public static final String IP_ROLE_SCOPE = "IP_ROLE_SCOPE";//角色适用范围字典
	
	public static final String IP_SCENE_LIST = "IP_SCENE_LIST";//场景列表
	
	public static final String ORG_TYPE = "IPE_ORG_TYPE";//单位类型
	
	public static final String INVALIDE_MAIL_DOMAIN = "INVALIDE_MAIL_DOMAIN";//无效的企业邮箱域名
	
	public static final String IP_PARTNER_M = "IP_PARTNER_M";//企业客户/业务伙伴信息维护
	
	/**
	 * 行政区级别
	 */
	public static final String IP_DISTRICT_LEVEL ="IP_DISTRICT_LEVEL";
	
	/**
	 * 行政区类型
	 */
	public static final String IP_DISTRICT_TYPE ="IP_DISTRICT_TYPE";
	
	/**
	 * cookie中的key - 场景
	 */
	public static final String COOKIE_KEY_SCENE = "_pt_se_";
	
	/**
	 * session中的key - 场景
	 */
	public static final String SESSION_KEY_SCENE = "IP_SCENE_KEY";
	
	/**
	 * session中的key - partner id
	 */
	public static final String SESSION_KEY_PARTNER = "_ip_pat_id_";
	
	/**
	 * session中的key - partner id
	 */
	public static final String PARAM_KEY_PARTNER = "partnerId";
	
	/**
	 * 多选
	 */
	public static String select_multi = "m";
	
	/**
	 * 单选
	 */
	public static String select_single = "s";
	
	/**
	 * 默认的组织层面  partner_id
	 */
	public static final Long DEFAULT_ORGG_PARTNER = 0L;
	
	/**
	 * 组织管理员角色Code  
	 */
	public static final String IPE_ROLE_ORGM_CODE = "ORG_ADMIN";

	/**
	 * 新增员工后通知登录
	 */
	public static final String USER_NOTIFY = "USER_NOTIFY";

	/**
	 * 酒店供应商
	 */
	public static final String OTAH_HOTEL_SUPPLIER = "OTAH_HOTEL_SUPPLIER";

	/**
	 * 一个有限期七天的cookies
	 */
	public static final String LOGIN_CODE_CHECKED_COOKIES = "LC_";

	/**
	 * 员工重置密码功能
	 */
	public static final String USER_RESET_PASSWORD = "USER_RESET_PASSWORD";
}
