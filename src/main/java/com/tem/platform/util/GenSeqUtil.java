package com.tem.platform.util;

import java.util.Random;

public class GenSeqUtil {
	
	/**
	 * 生成随机的编码
	 * @param index 生成的位数
	 * @return
	 */
	public static String getGenSeq(int index){
		String rs = "";
		if(index <= 0) {
            return rs;
        }
		Random random = new Random();
		for (int i = 0; i < index; i++) {
			rs += random.nextInt(10);
		}
		return rs;
	}
	
}
