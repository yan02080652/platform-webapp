package com.tem.platform.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.iplatform.common.Config;
import com.iplatform.common.utils.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.alibaba.fastjson.JSONObject;
import com.iplatform.common.CompanyConst;
import com.iplatform.common.utils.AESCoder;
import com.iplatform.common.utils.LogUtils;
import com.tem.filter.LoginFilter;
import com.tem.platform.Constants;
import com.tem.platform.api.GrayRuleService;
import com.tem.platform.api.dto.UserDto;
import com.tem.platform.security.authentication.PlatformAuthentication;

/**
 * Created by chenjieming on 2017/9/15.
 */
public class PlatformAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final Logger logger = LoggerFactory.getLogger(LoginFilter.class);

    /**
     * 灰度环境标识
     */
    public static final String KI4SO_CLIENT_GRAY_ENV_COOKIE_KEY="gray_env_admin";

    private LoginRedisManger loginRedisManger;

    @Autowired
    private GrayRuleService grayRuleService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

        String username = request.getParameter("username");

        if(authentication.isAuthenticated()){
            LogUtils.debug(logger,"后台登录，授权成功，更新cookies");

            String cookieKey = Constants.LOGIN_CODE_CHECKED_COOKIES + "_" + DigestUtils.md5(username);

            String encrypt = AESCoder.encrypt2HexString(username, LoginFilter.LOGIN_VERIFY_KEY);

            Cookie ck = new Cookie(cookieKey, encrypt);
            ck.setMaxAge(7 * 24 * 3600);
            response.addCookie(ck);

            Map<String,Object> map = new HashMap<String,Object>();
            map.put("count",-1);

            Object details = authentication.getDetails();
            UserDto user = (UserDto) details;

            boolean flag = validGrayRule(user.getId(), user.getPartnerId(), response);

            String isGrey = request.getParameter("isGrey");

            if(flag && isGrey == null){

                request.getSession().invalidate();

                map.put("count", -99);
            }

            writeToClient(map,response);
        }else{

            String codeCountCache = loginRedisManger.getCodeCountCache(username);
            int count = 0;
            if(codeCountCache != null){
                count = Integer.parseInt(codeCountCache);
            }
            count++;
            if(count >= 3){

                String cookieKey = Constants.LOGIN_CODE_CHECKED_COOKIES + "_" + DigestUtils.md5(username);

                Cookie ck = new Cookie(cookieKey,username);
                ck.setMaxAge(0);
                response.addCookie(ck);
            }
            loginRedisManger.setCodeCount(username, count + "");

            PlatformAuthentication auth = (PlatformAuthentication)authentication;

            LogUtils.debug(logger,"后台登录，授权失败,失败原因{}",auth.getMessage());

            Map<String,Object> map = new HashMap<String, Object>();
            map.put("message",auth.getMessage());
            map.put("count",count);

            writeToClient(map,response);
        }
    }

    public void writeToClient(Object info , HttpServletResponse response) {
        try {
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/json;charset=utf-8");
            String jsonInfo = JSONObject.toJSONString(info);
            PrintWriter writer = response.getWriter();
            writer.print(jsonInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public LoginRedisManger getLoginRedisManger() {
        return loginRedisManger;
    }

    public void setLoginRedisManger(LoginRedisManger loginRedisManger) {
        this.loginRedisManger = loginRedisManger;
    }

    /**
     * 验证是否是灰度环境
     * @return
     */
    private boolean validGrayRule(Long userId, Long partnerId, HttpServletResponse response){

        boolean flag = grayRuleService.validGrayRule(userId, partnerId);

        if(flag){
            Cookie cookie = new Cookie(KI4SO_CLIENT_GRAY_ENV_COOKIE_KEY, 1 + "");
            cookie.setPath("/");

            cookie.setDomain(CompanyConst.ZT_ADMIN_WEBSITE);
            response.addCookie(cookie);
        }else {

            Cookie cookie = new Cookie(KI4SO_CLIENT_GRAY_ENV_COOKIE_KEY, 1 + "");
            cookie.setPath("/");

            cookie.setMaxAge(0);
            cookie.setDomain(CompanyConst.ZT_ADMIN_WEBSITE);
            response.addCookie(cookie);
        }

        return flag;
    }

}
