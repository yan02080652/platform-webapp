package com.tem.platform.util;

import com.iplatform.common.cache.RedisCacheFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 后台登录验证 redis
 */
@Component
public class LoginRedisManger {

	@Autowired
	private RedisCacheFactory redisCacheFactory;

	private static int SMS_EXPIRE = 5;// 单位分钟
	private static int SMS_EXPIRE_COUNT = 30;// 30分钟内输错三次 输入验证码
	private static String ADMIN_LOGIN = "ADMIN:LOGIN:%s";
	private static String ADMIN_LOGIN_COUNT = "ADMIN:LOGIN_COUNT:%s";

	//设置验证码
	public void setCodeCache(String username, String code) {
		try {
			redisCacheFactory.getObject().setEx(String.format(ADMIN_LOGIN, username), SMS_EXPIRE * 60, code);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//获取验证码
	private String getCodeCache(String username) {
		String key = String.format(ADMIN_LOGIN, username);
		try {
			Object rtn = redisCacheFactory.getObject().get(key);
			if (rtn != null) {
				return rtn.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	//设置输入验证码次数
	public void setCodeCount(String username , String count){
		try {
			redisCacheFactory.getObject().setEx(String.format(ADMIN_LOGIN_COUNT, username), SMS_EXPIRE_COUNT * 60, count);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//获取输入验证码次数
	public String getCodeCountCache(String username) {
		String key = String.format(ADMIN_LOGIN_COUNT, username);
		try {
			Object rtn = redisCacheFactory.getObject().get(key);
			if (rtn != null) {
				return rtn.toString();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}


	public boolean isValidateCode(String username, String uCode) {
		String code = getCodeCache(username);

		if (code == null || "".equals(code)) {
			return false;
		}
		if (!code.equals(uCode)) {
			return false;
		}
		return true;
	}
	
}
