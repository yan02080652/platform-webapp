package com.tem.platform.util;

import java.lang.reflect.Type;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.reflect.TypeToken;
import com.iplatform.common.utils.JsonUtils;
import com.tem.member.SettingConstants;

public class TransUtils {

	/**
	 * 过渡转换
	 * TM_PLAN_ENABLE、TM_PLAN_ALLOW_APPROVING_ENABLE、TM_PLAN_BUDGET、TM_PLAN_BUDGET_CONTROL_METHOD
	 * 四个配置之前版本存储json,现改成字符串
	 * @param itemCode
	 * @param valueString
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getTransValue(String itemCode,String valueString){
		if(StringUtils.isEmpty(itemCode) || StringUtils.isEmpty(valueString)){
			return null;
		}
		String value;
		if(valueString.contains("{")){
			Type type = new TypeToken<Object>() {}.getType();
			Map<String,Object> valueMap = (Map<String,Object>)JsonUtils.getGson().fromJson(valueString, type);
			if(SettingConstants.TM_PLAN_ENABLE.equals(itemCode)){
				value = (String) valueMap.get("TRAVEL_PLAN_OPEN");
			}else if(SettingConstants.TM_PLAN_ALLOW_APPROVING_ENABLE.equals(itemCode)){
				value = (String) valueMap.get("TRAVEL_PLAN_ALLOW_APPROVING_OPEN");
			}else if(SettingConstants.TM_PLAN_BUDGET.equals(itemCode)){
				value = (String) valueMap.get("TM_PLAN_BUDGET");
			}else if(SettingConstants.TM_PLAN_BUDGET_CONTROL_METHOD.equals(itemCode)){
				value = (String) valueMap.get("TM_PLAN_BUDGET_CONTROL_METHOD");
			}else{
				value = (String) valueMap.get(itemCode);
			}
		}else{
			value = valueString;
		}
		return value;
	}
	/**
	 * 企业差旅计划、预定时是否允许使用差旅计划进行审批  两个开关   展现
	 * @param itemCode
	 * @param valueString
	 * @return
	 */
	public static String getTransShowValue(String itemCode,String valueString){
		if(StringUtils.isEmpty(itemCode) || valueString == null){
			return null;
		}
		
		if(SettingConstants.TM_PLAN_ENABLE.equals(itemCode) || SettingConstants.TM_PLAN_ALLOW_APPROVING_ENABLE.equals(itemCode)){
			String value;
			if(valueString.contains("{")){
				Type type = new TypeToken<Object>() {}.getType();
				Map<String,Object> valueMap = (Map<String,Object>)JsonUtils.getGson().fromJson(valueString, type);
				if(SettingConstants.TM_PLAN_ENABLE.equals(itemCode)){
					value = (String) valueMap.get("TRAVEL_PLAN_OPEN");
				}else if(SettingConstants.TM_PLAN_ALLOW_APPROVING_ENABLE.equals(itemCode)){
					value = (String) valueMap.get("TRAVEL_PLAN_ALLOW_APPROVING_OPEN");
				}else{
					value = valueString;
				}
			}else{
				value = valueString;
			}
			return value;
		}
		return valueString;
	}
	
}
