package com.tem.platform.action.widget;

import com.tem.platform.api.OrgService;
import com.tem.platform.api.PartnerCorpService;
import com.tem.platform.api.dto.OrgDto;
import com.tem.platform.api.dto.PartnerCorpDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 法人选择器
 * @author jieming
 *
 */
@Controller
@RequestMapping("widget/corp")
public class CorpWgController extends CommonWg {
	
	@Autowired
	private PartnerCorpService	partnerCorpService;
	
	@Autowired
	private OrgService	orgService;
	
	
	@RequestMapping(value="/show.html")
	public String show(Model model){
		model.addAttribute("typeTitle", "选择付款单位");
		model.addAttribute("selectType", "corp");
		model.addAttribute("width", 500);
		return "widget/corp.wg";
	}
	
	@RequestMapping(value = "search")
	public String search(
			Model model,
			@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "pageSize", required = false) Integer pageSize,
			Long partnerId,
			String key) {
		if (start == null) {
			start = 0;
		}
		if (pageSize == null) {
			pageSize = 5;
		}

//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("start", start);
//		params.put("pageSize", pageSize);

//		List<PartnerDto> partnerList = partnerService.getPartnerList(params);
//		Integer count = partnerService.getPartnerCount(params);
//		Page<PartnerDto> pageList = new Page<PartnerDto>(start, pageSize,
//				partnerList, count);
//		pageList.setPageScript("platform.partner.turnTo");
//		model.addAttribute("count", count);
//		model.addAttribute("pageList", pageList);
		List<PartnerCorpDto> pageList = partnerCorpService.getByPartnerId(partnerId);
		model.addAttribute("pageList", pageList);
		
		return "widget/corpBody.jsp";
	}
	
	@RequestMapping("getCorpList")
	@ResponseBody
	public List<PartnerCorpDto> getCorpList(Long partnerId){
		if(partnerId == null){
			return null;
		}
		List<PartnerCorpDto> pageList = partnerCorpService.getByPartnerId(partnerId);
		return pageList;
	}
	
	@ResponseBody
	@RequestMapping("expenseTree")
	public List<OrgDto> expenseTree() {
		Long partnerId = super.getLongValue("partnerId");

		return orgService.expenseTree(partnerId);
	}
	

}
