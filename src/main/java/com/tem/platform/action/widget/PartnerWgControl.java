package com.tem.platform.action.widget;

import com.iplatform.common.web.Page;
import com.tem.platform.api.OrgService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.OrgDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 *	企业选择器
 * @author pyy
 */
@Controller
@RequestMapping("widget/partner")
public class PartnerWgControl extends CommonWg {

	@Autowired
	private PartnerService partnerService;

	@Autowired
	private OrgService orgService;

	@Autowired
	private UserService userService;

	/**
	 * 请求用户选择器内容，分多选和单选，默认单选
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/show.html", method = RequestMethod.GET)
	public String page(Model model) {
		/*String partnerType = super.getStringValue("partnerType");
		String typeTitle = "选择企业";
		if(StringUtils.isNotEmpty(partnerType)){
			typeTitle = "0".equals(partnerType) ? "选择客户": "选择合作伙伴";
		}
		model.addAttribute("typeTitle", typeTitle);*/
		model.addAttribute("selectType", "partner");
		model.addAttribute("width", 500);
		return "widget/partner.wg";
	}


	@RequestMapping(value = "search")
	public String search(
			Model model,
			@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "pageSize", required = false) Integer pageSize,String key) {
		if (start == null) {
			start = 0;
		}
		if (pageSize == null) {
			pageSize = 10;
		}

		Map<String, Object> params = new HashMap<>(16);
		params.put("start", start);
		params.put("pageSize", pageSize);

		Integer partnerType = super.getIntegerValue("partnerType");
		boolean manageTmc = super.isSuperAdmin();
		boolean managePartner = true;
		Long tmcId = super.getCurrentUser().getPartnerId();
		if(manageTmc){
			//主动要求查询tmc下的企业
			Long param_tmcId = super.getLongValue("tmcId");
			if(param_tmcId != null){
				tmcId = param_tmcId;
			}
		}
		boolean showType = false;
		List<PartnerDto> partnerList = new ArrayList<>();
		int count = 0;
		boolean hasPermission = true;
		if(new Integer(1).equals(partnerType)){
			//管理TMC
			if(manageTmc){
				params.put("type", 1);
			}else{
				hasPermission = false;
			}
		}else if(new Integer(0).equals(partnerType)){
			//管理企业
			if(managePartner){
				params.put("type", 0);
				if(!super.isSuperAdmin()){
					params.put("tmcId", tmcId);
				}
			}else{
				hasPermission = false;
			}
		}else if(partnerType == null){
			//查看tmc或企业
			if(manageTmc){
				params.put("ortype", 1);
			}
			if(managePartner){
				//限制TMC
				params.put("ortmcId", tmcId);
			}
			showType = true;
		}

		if(hasPermission){
			if (key != null && !"".equals(key)) {
				String firstSpell = partnerService.getFirstSpell(key);
				params.put("name", key);
				params.put("code", firstSpell);
			}
			partnerList = partnerService.getPartnerList(params);
			count = partnerService.getPartnerCount(params);
		}
		Page<PartnerDto> pageList = new Page<PartnerDto>(start, pageSize,
				partnerList, count);
		pageList.setPageScript("platform.partner.turnTo");
		model.addAttribute("count", count);
		model.addAttribute("pageList", pageList);
		model.addAttribute("showType", showType);
		return "widget/partnerBody.jsp";
	}

	/*@RequestMapping(value = "search")
	public String search(
			Model model,
			@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "pageSize", required = false) Integer pageSize,
			Integer partnerType, String key) {
		if (start == null) {
			start = 0;
		}
		if (pageSize == null) {
			pageSize = 10;
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("start", start);
		params.put("pageSize", pageSize);
		params.put("type", partnerType);

		//不用查询本单位
		boolean isadmin = super.isSuperAdmin();
		boolean dissme = new Integer(1).equals(super.getIntegerValue("dissme"));
		Long partnerId = super.getCurrentUser().getPartnerId();
		boolean addme = false;
		boolean showType = false;
		if(new Integer(1).equals(partnerType)){//选择TMC
			if(!isadmin){//没权限只能查询自己
				params.put("partnerId", partnerId);
			}
		}else if(new Integer(0).equals(partnerType)){//客户
			Long tmcId = super.getLongValue("tmcId");//主动要求查询tmc下的企业
			if ( tmcId == null && !isadmin) {//通过权限控制能否查看其他TMC下的客户
				tmcId = partnerId;
			}
			if ( tmcId != null ) {//选择TMC下的企业，
				params.put("tmcId", tmcId);
			}
		}else{//选择企业
			if (!isadmin) {
				params.put("tmcId", partnerId);
				addme = true;//可以查看本单位企业
			}
			showType = true;
		}

		if (key != null && !"".equals(key)) {
			String firstSpell = partnerService.getFirstSpell(key);
			params.put("name", key);
			params.put("code", firstSpell);
			// params.put("mailDomain", key);
		}
		List<PartnerDto> partnerList = partnerService.getPartnerList(params);
		if(dissme){
			PartnerDto me = null;
			for (PartnerDto p:partnerList) {
				if (p.getId().equals(partnerId)) {
					me = p;
					break;
				}
			}
			if (me != null) {
				partnerList.remove(me);
			}
		}else if(addme && start == 0){
			PartnerDto me = partnerService.findById(partnerId);
			if (me != null) {
				partnerList.add(0, me);
			}
		}

		Integer count = partnerService.getPartnerCount(params);
		Page<PartnerDto> pageList = new Page<PartnerDto>(start, pageSize,
				partnerList, count);
		pageList.setPageScript("platform.partner.turnTo");
		model.addAttribute("count", count);
		model.addAttribute("pageList", pageList);
		model.addAttribute("showType", showType);
		return "widget/partnerBody.jsp";
	}*/


	@RequestMapping(value = "/partnerAndUser.html", method = RequestMethod.GET)
	public String partnerAndUser(Model model) {
		return "widget/partnerAndUser.single";
	}

	@RequestMapping(value = "getPartnerAndChooseInfo", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> getPartnerAndChooseInfo(String partnerIds,String orgIds,String userIds){
		Map<String, Object> rs = new HashMap<>();

		//加载企业信息
		Map<Long, Object> partnerInfo = new HashMap<>();
		//加载已选组织信息
		Map<Long, Object> orgInfo = new HashMap<>();
		//加载已选用户信息
		Map<Long, Object> userInfo = new HashMap<>();

		Set<Long> partnerIdsList = this.turnLit(partnerIds);
		Set<Long> orgIdsList = this.turnLit(orgIds);
		Set<Long> userIdsList = this.turnLit(userIds);

		if(userIdsList.size() > 0){
			List<UserDto> users = userService.getUsers(this.toList(userIdsList));
			for (UserDto user : users) {
				userInfo.put(user.getId(), user);
			}
		}

		if(orgIdsList.size() > 0){
			List<OrgDto> orgs = orgService.getOrgByIds(this.toList(orgIdsList));
			for(OrgDto org:orgs) {
				orgInfo.put(org.getId(), org);
				if(org.getPartnerId() != null){
					partnerIdsList.add(org.getPartnerId());
				}
			}
		}

		if (partnerIdsList.size() > 0) {
			List<PartnerDto> partnerDtos = partnerService.findByBpIds(this.toList(partnerIdsList));
			for(PartnerDto partner:partnerDtos){
				partnerInfo.put(partner.getId(), partner);
			}
		}

		rs.put("partnerInfo", partnerInfo);
		rs.put("orgInfo", orgInfo);
		rs.put("userInfo", userInfo);
		return rs;
	}

	private Set<Long> turnLit(String ids){
		if(StringUtils.isNotEmpty(ids)){
			String[] ids_array = ids.split(",");
			Set<Long> rs = new HashSet<>();
			for (int i = 0; i < ids_array.length; i++) {
				rs.add(Long.valueOf(ids_array[i]));
			}
			return rs;
		}else{
			return new HashSet<>();
		}
	}

	private List<Long> toList(Set<Long> list){
		List<Long> rs = new ArrayList<>();
		for(Long li: list){
			rs.add(li);
		}
		return rs;
	}

}
