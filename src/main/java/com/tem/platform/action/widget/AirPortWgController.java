package com.tem.platform.action.widget;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.tem.flight.api.AirportService;
import com.tem.flight.dto.airport.AirportDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 机场公共选择器
 * Created by pyy on 2017/5/10.
 */
@Controller
@RequestMapping("widget/airPort")
public class AirPortWgController extends CommonWg {

    @Autowired
    private AirportService airportService;

    /**
     * @param model
     * @return
     */
    @RequestMapping(value = "/show.html", method = RequestMethod.GET)
    public String page(Model model) {
        model.addAttribute("typeTitle", "选择机场");
        model.addAttribute("width", 600);
        model.addAttribute("selectType", "airPort");
        return "widget/airPort.wg";
    }

    @RequestMapping(value = "search")
    public String search(Model model) {
        Integer pageIndex = super.getIntegerValue("pageIndex");
        Integer pageSize = super.getIntegerValue("pageSize");
        String keyword = super.getStringValue("key");
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = 10;
        }

        QueryDto<Object> queryDto = new QueryDto<Object>(pageIndex,pageSize);

        if (StringUtils.isNotEmpty(keyword)) {
            queryDto.setField1(keyword);
        }

        Page<AirportDto> pageList = airportService.queryListWithPage(queryDto);
        model.addAttribute("pageList", pageList);
        model.addAttribute("pageScript","platform.airPort.turnTo");
        return "widget/airPortBody.jsp";
    }
}
