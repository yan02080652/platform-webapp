package com.tem.platform.action.widget;

import com.tem.platform.api.OrgService;
import com.tem.platform.api.dto.OrgDto;
import com.tem.platform.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 组件之单位选择器
 * @author pyy
 *
 */

@Controller
@RequestMapping("widget/org")
public class OrgWgController extends CommonWg {
	
	@Autowired
	private OrgService orgService;
	
	/**
	 * 请求组织选择器内容，分多选和单选，默认单选
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/show.html", method = RequestMethod.GET)
	public String page(Model model) {
		String type = super.getStringValue("selectType");
		model.addAttribute("typeTitle", "组织选择器");
		String page = "org";
		if(Constants.select_multi.equals(type)){
			page = "orgs";
		}else{
			model.addAttribute("width", 350);
		}
		//选择模式
		model.addAttribute("selectType", page);
		return "widget/"+page+".wg";
	}
	
	@RequestMapping(value="/getOrgs.aj",method = RequestMethod.GET)
	@ResponseBody
	public List<OrgDto> getOrgs(Model model){
		Long partnerId = super.getPartnerId();
		if(partnerId == null){
			return null;
		}else{
			Long id = super.getLongValue("id");
			return orgService.findOrgs(partnerId,id);
		}
	}
	
	@RequestMapping(value="/getAllOrgs.aj",method = RequestMethod.GET)
	@ResponseBody
	public List<OrgDto> getAllOrgs(Model model){
		Long partnerId = super.getPartnerId();
		if(partnerId == null){
			return null;
		}else{
			return orgService.findAllOrgs(partnerId);
		}
	}
}
