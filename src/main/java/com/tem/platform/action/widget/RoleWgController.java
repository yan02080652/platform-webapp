package com.tem.platform.action.widget;

import com.tem.platform.api.RoleService;
import com.tem.platform.api.dto.RoleDto;
import com.tem.platform.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 组件之单位选择器
 * @author pyy
 *
 */

@Controller
@RequestMapping("widget/role")
public class RoleWgController extends CommonWg {
	
	@Autowired
	private RoleService roleService;
	
	/**
	 * 请求组织选择器内容，分多选和单选，默认单选
	 * @param model
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/show.html", method = RequestMethod.GET)
	public String page(Model model,HttpServletRequest req) {
		String type = req.getParameter("selectType");
		model.addAttribute("typeTitle", "角色选择器");
		String page = "role";
		if(Constants.select_multi.equals(type)){
			page = "roles";
		}else{
			model.addAttribute("width", 350);
		}
		//选择模式
		model.addAttribute("selectType", page);
		return "widget/"+page+".wg";
	}
	
	/**
	 * 获取所有角色
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/getRoles.aj",method = RequestMethod.GET)
	@ResponseBody
	public List<RoleDto> getRoles(Model model){
		Long partnerId = super.getPartnerId();
		if(partnerId == null){
			return null;
		}else{
			boolean include0Partner = true;
			String type = super.getStringValue("type");
			if("tmc".equals(type) || "me".equals(type)){
				include0Partner = false;
			}
			return roleService.getRolesByPartnerId(partnerId, include0Partner);
		}
	}
}
