package com.tem.platform.action.widget;

import com.iplatform.common.web.Page;
import com.tem.platform.api.OrgService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.OrgDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.platform.Constants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 组织之用户选择器
 *
 * @author pyy
 */
@Controller
@RequestMapping("widget/user")
public class UserWgController extends CommonWg {

    @Autowired
    private OrgService orgService;

    @Autowired
    private UserService userService;

    @Autowired
    private PartnerService partnerService;

    /**
     * 请求用户选择器内容，分多选和单选，默认单选
     *
     * @param model
     * @param req
     * @return
     */
    @RequestMapping(value = "/show.html", method = RequestMethod.GET)
    public String page(Model model, HttpServletRequest req) {
        String type = req.getParameter("selectType");
        model.addAttribute("typeTitle", "员工选择器");
        String page = "user";
        Long partnerId = super.getPartnerId();
        //must choose a partnerId
        if (partnerId == null) {
            partnerId = super.setDefaultPartnerId();
        }
        List<OrgDto> orgDtos = orgService.findOrgs(partnerId, null);
        if (orgDtos.size() > 0) {
            OrgDto orgDto = orgDtos.get(0);
            model.addAttribute("orgId", orgDto.getId());
            model.addAttribute("orgName", orgDto.getName());
        }
        if (Constants.select_multi.equals(type)) {
            page = "users";
        } else {
            model.addAttribute("width", 420);
        }
        // 选择模式
        model.addAttribute("selectType", page);
        return "widget/" + page + ".wg";
    }

    @RequestMapping("search")
    public String search(Model model,
                         @RequestParam(value = "start", required = false) Integer start,
                         @RequestParam(value = "pageSize", required = false) Integer pageSize,
                         @RequestParam(value = "orgId", required = false) Integer orgId,
                         @RequestParam(value = "key", required = false) String key,
                         @RequestParam(value = "selectType") String selectType) {
        if (start == null) {
            start = 0;
        }
        if (pageSize == null) {
            pageSize = 8;
        }
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("sort", "CREATE_DATE");
        params.put("dir", "asc");
        params.put("start", start);
        params.put("limit", pageSize);
        params.put("orgId", orgId);
        params.put("fullname", key);
        params.put("title", key);
        List<UserDto> userList = userService.search(params);
        Integer count = userService.searchCount(params);
        if (count == null) {
            count = 0;
        }
        Page<UserDto> pageList = new Page<UserDto>(start, pageSize,
                userList, count);
        //pageList.setPageUrl("user?");
        String userKey = "m".equals(selectType) ? "users" : "user";
        pageList.setPageScript("platform." + userKey + ".turnTo");
        model.addAttribute("count", count);
        model.addAttribute("pageList", pageList);
        return "widget/usersBody.jsp";
    }

    @RequestMapping("searchUser")
    @ResponseBody
    public Map<String, Object> searchUser(Long orgId, String partnerIds, String name) {
        Map<String, Object> rs = new HashMap<>();
        if (orgId != null) {
            Map<String, Object> params = new HashMap<>();
            params.put("orgId", orgId);
            params.put("sort", "CREATE_DATE");
            params.put("dir", "asc");
            params.put("start", 0);
            params.put("limit", 500);
            List<UserDto> users = userService.search(params);
            rs.put("users", users);
        } else {
            if (StringUtils.isNotEmpty(partnerIds) && StringUtils.isNotEmpty(name)) {
                Map<String, Object> params = new HashMap<>();
                params.put("partnerIds", this.turnLit(partnerIds));
                params.put("fullname", name);
                params.put("title", name);
                List<UserDto> users = userService.search(params);

                //查找企业或部门名称
                Set<Long> partnerIdSet = new HashSet<>();
                Set<Long> orgIds = new HashSet<>();
                for (UserDto u : users) {
                    partnerIdSet.add(u.getPartnerId());
                    orgIds.add(u.getOrgId());
                }

                if (orgIds.size() > 0) {
                    List<OrgDto> orgs = orgService.getOrgByIds(this.toList(orgIds));
                    List<PartnerDto> partners = partnerService.findByBpIds(this.toList(partnerIdSet));

                    Map<Long, String> orgNames = new HashMap<>();
                    Map<Long, String> partnerNames = new HashMap<>();
                    for(OrgDto org:orgs){
                        orgNames.put(org.getId(), org.getName());
                    }
                    for (PartnerDto partner : partners) {
                        partnerNames.put(partner.getId(), partner.getName());
                    }
                    rs.put("orgNames", orgNames);
                    rs.put("partnerNames", partnerNames);
                }
                rs.put("users", users);
            }
        }

        return rs;
    }

    private List<Long> turnLit(String ids) {
        if (StringUtils.isNotEmpty(ids)) {
            String[] ids_array = ids.split(",");
            List<Long> rs = new ArrayList<>();
            for (int i = 0; i < ids_array.length; i++) {
                rs.add(Long.valueOf(ids_array[i]));
            }
            return rs;
        } else {
            return new ArrayList<>();
        }
    }

    private List<Long> toList(Set<Long> list) {
        List<Long> rs = new ArrayList<>();
        for (Long li : list) {
            rs.add(li);
        }
        return rs;
    }
}
