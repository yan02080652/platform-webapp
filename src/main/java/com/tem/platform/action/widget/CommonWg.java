package com.tem.platform.action.widget;

import com.tem.platform.api.PartnerService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.Constants;
import com.tem.platform.action.BaseController;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 公共选择器
 * Created by pyy on 2017/10/19.
 */
public class CommonWg extends BaseController {

    @Autowired
    private PartnerService partnerService;

    /**
     * 获取企业id,统一支持参数type
     * 场景一：获取本企业的id
     * 场景二：获取下属TMC或下属企业id
     * @return
     */
    protected Long getPartnerId(){
        String type = super.getStringValue("type");
        return "me".equals(type)?super.getTmcId():super.getPartnerId();
    }

    /**
     * 对于必须需要partnerId的作业提供方法
     * @return
     */
    protected Long setDefaultPartnerId(){
        Long tmcId = super.getTmcId();
        Map<String, Object> params = new HashMap<>(16);
        params.put("start", 0);
        params.put("pageSize", 1);
        params.put("type", 0);
        params.put("tmcId", tmcId);
        List<PartnerDto> partnerList = partnerService.getPartnerList(params);
        //if partnerList.size eq 0,then no data will be searched
        if(partnerList.size() > 0){
            Long partnerId = partnerList.get(0).getId();
            getSession().setAttribute(Constants.SESSION_KEY_PARTNER, partnerId);
            return partnerId;
        }
        return tmcId;
    }
}
