package com.tem.platform.action.widget;

import com.tem.payment.api.AccountManageService;
import com.tem.payment.dto.BpAccountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 账户选择器
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("widget/account")
public class AccountWgController extends CommonWg {
	
	@Autowired
	private AccountManageService accountManageService;
	
	@RequestMapping("getAccountList")
	@ResponseBody
	public List<BpAccountDto> getAccountList(Long partnerId){
		if(partnerId == null){
			return null;
		}
		List<BpAccountDto> bpAccountDtos = accountManageService.getAllByBpId(partnerId);
		return bpAccountDtos;
	}
	

}
