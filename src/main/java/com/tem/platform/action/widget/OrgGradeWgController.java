package com.tem.platform.action.widget;

import com.tem.platform.api.OrgGradeService;
import com.tem.platform.api.dto.OrgGradeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 组织层面选择器
 * @author jieming
 *
 */
@Controller
@RequestMapping("widget/orgGrade")
public class OrgGradeWgController extends CommonWg {
	
	@Autowired
	private OrgGradeService orgGradeService;

	/**
	 * 支持
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/show")
	public List<OrgGradeDto> show(Model model){
		Long partnerId = super.getPartnerId();
		if(partnerId == null){
			return new ArrayList<>();
		}else{
			return orgGradeService.findList(partnerId);
		}
	}
	

}
