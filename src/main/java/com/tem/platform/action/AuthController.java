package com.tem.platform.action;

import com.iplatform.common.utils.JsonUtils;
import com.iplatform.common.web.Message;
import com.tem.platform.api.DictService;
import com.tem.platform.api.MenuService;
import com.tem.platform.api.RoleAuthService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.MenuDto;
import com.tem.platform.api.dto.RoleMenuDto;
import com.tem.platform.api.dto.RoleTransDto;
import com.tem.platform.Constants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 授权action 	
 * @author pyy
 *
 */
@Controller
@RequestMapping("platform/auth")
public class AuthController extends BaseController {
	
	
	@Autowired
	private MenuService menuService;
	
	@Autowired
	private RoleAuthService roleAuthService;
	
	@Autowired
	private DictService dictService;
	
	@RequestMapping(value="",method = RequestMethod.GET)
	public String index(Model model,HttpServletRequest req){
		List<DictCodeDto> dictCodeDtoList = dictService.getCodes(Constants.IP_SCENE_LIST);
		model.addAttribute("roleId", req.getParameter("roleId"));
		model.addAttribute("dictCodeDtoList", dictCodeDtoList);
		return "platform/auth/index.single";
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/getData.do", method = RequestMethod.GET)
	public Map<String, Object> getBaseData(Model model,HttpServletRequest req){
		String roleId = req.getParameter("roleId");
		String sceneId = req.getParameter("sceneId");
		if(StringUtils.isEmpty(roleId) || StringUtils.isEmpty(sceneId)){
			return null;
		}
		
		//菜单列表
		List<MenuDto> partnerMenus = menuService.getAllBySceneId(sceneId);
		Map<String, Object> rs = new HashMap<String, Object>();
		rs.put("menus", partnerMenus);
		
		//已授权的菜单
//		Long partnerId = super.getPartnerId();
		List<RoleMenuDto> roleMenus = roleAuthService.getRoleMenus(Long.valueOf(roleId));
		rs.put("roleMenus", roleMenus);
		return rs;
	}
	
//	@ResponseBody
//	@RequestMapping(value = "/getRoleData.do", method = RequestMethod.GET)
//	public Map<String, Object> getRoleData(Model model,@RequestParam(value="roleId",required=true)Long roleId){
//		Long partnerId = TrUtil.getPartnerId();
//		List<RoleMenuDto> roleMenus = roleAuthService.getAll(partnerId, roleId);
//		Map<String, Object> rs = new HashMap<String, Object>();
//		rs.put("roleMenus", roleMenus);
//		return rs;
//	}
	
	@ResponseBody
	@RequestMapping(value = "/saveRoleMenu.do", method = RequestMethod.POST)
	public Message getRoleData(Model model,
			@RequestParam("roleId")Long roleId,
			@RequestParam("addMenuIds")String addMenuIds,
			@RequestParam("deleteMenuIds")String deleteMenuIds,
							   @RequestParam("codeAvs")String codeAvs){
		Long partnerId = super.getPartnerId();
		//菜单授权
		roleAuthService.saveRoleMenus(partnerId,roleId,addMenuIds,deleteMenuIds);
		//作业授权
		Map<String,String> codeAvMap = JsonUtils.getGson().fromJson(codeAvs,Map.class);
		for (String code : codeAvMap.keySet()) {
			roleAuthService.updateRoleTrans(partnerId, roleId, code, codeAvMap.get(code));
		}
		return Message.success("保存成功");
	}
	
	@RequestMapping(value="/editRoleTrans.do",method=RequestMethod.POST)
	public String showTrans(Model model,
			@RequestParam("roleId")Long roleId,
			@RequestParam("transCode")String transCode){
		Long partnerId = super.getPartnerId();
		RoleTransDto roleTrans = roleAuthService.getRoleTrans(partnerId,roleId, transCode);
		model.addAttribute("roleTrans", roleTrans);
		return "platform/auth/trans.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveRoleMenuTrans.do", method = RequestMethod.POST)
	public Message saveRoleMenuTrans(@RequestParam("roleId")Long roleId,
			@RequestParam("transCode")String transCode,
			@RequestParam("menuId")Long menuId,
			@RequestParam(value="activities",required=false)String activities){
		Long partnerId = super.getPartnerId();
		boolean hasMenuAuth = roleAuthService.hasMenuAuth(partnerId, roleId, menuId);
		String msg = "";
		if(!hasMenuAuth){//增加菜单授权
			roleAuthService.addRoleMenu(partnerId, roleId, menuId);
			msg += "作业菜单、";
		}
		roleAuthService.updateRoleTrans(partnerId, roleId, transCode, activities);
		msg += "作业活动授权成功.";
		return Message.success(msg);
	}
	
}
