package com.tem.platform.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.utils.LogUtils;
import com.iplatform.common.utils.ShortUUID;
import com.tem.notify.api.EventService;
import com.tem.notify.api.MessageRecordService;
import com.tem.notify.api.MqMessageService;
import com.tem.notify.api.SmsService;
import com.tem.notify.condition.MqRecordCondition;
import com.tem.notify.dto.MessageRecordCondition;
import com.tem.notify.dto.MessageRecordDto;
import com.tem.notify.dto.MqMessageDto;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * mq 消息记录
 * @author jieming
 *
 */
@Controller
@RequestMapping("mqRecord")
public class MqRecordController extends BaseController {

	private final Logger logger = LoggerFactory.getLogger(MqRecordController.class);

	@Autowired
	private MqMessageService mqMessageService;

	@Autowired
	private EventService eventService;

	@RequestMapping(value ="index")
	public String index(Model model,Integer pageIndex,MqRecordCondition condition) {
		if(pageIndex == null){
			pageIndex = 1;
		}

		if(condition.getStatus() == null){
            condition.setStatus(2);
        }

		QueryDto<MqRecordCondition> queryDto = new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);

		queryDto.setCondition(condition);
		Page<MqMessageDto> pageList = mqMessageService.queryListWithPage(queryDto);

		model.addAttribute("pageList",pageList );
		model.addAttribute("condition", condition);

		return "platform/mqRecord/index.base";
	}

	@ResponseBody
	@RequestMapping("batchSend")
	public boolean batchSend(String batchId){

		List<MqMessageDto> messages = mqMessageService.getByBatchId(batchId);

		return true;
	}

	@ResponseBody
    @RequestMapping("send")
	public boolean send(String mqMsgId){

		MqMessageDto messageDto = mqMessageService.getById(mqMsgId);

//		EventDataDto event = new EventDataDto();
//
//		event.setEventId(messageDto.getId());
//		event.setBatchId(messageDto.getBatchId());
//		event.setEventCode(messageDto.getEventCode());
//		event.setEventData(messageDto.getEventData());
//
//		eventService.pushEventAgain(event);

		return true;
	}
	
}
