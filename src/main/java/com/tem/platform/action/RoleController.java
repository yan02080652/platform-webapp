package com.tem.platform.action;

import com.iplatform.common.web.Message;
import com.tem.platform.api.DictService;
import com.tem.platform.api.RoleService;
import com.tem.platform.api.UserRoleService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.RoleDto;
import com.tem.platform.Constants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("platform/role")
public class RoleController extends BaseController {

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private DictService dictService;
	
	@RequestMapping("")
	public String role(Model model,HttpServletRequest req){
		String sysId = req.getParameter("sysId");
		if(StringUtils.isEmpty(sysId)){
			sysId = Constants.PLATFORM;
		}
		List<DictCodeDto> dictCodeDtoList = dictService.getCodes(Constants.IP_SYS_LIST);
		model.addAttribute("sysId", sysId);
		model.addAttribute("dictCodeDtoList", dictCodeDtoList);
		model.addAttribute("type", "tmc");
		return "platform/role/role.base";
	}
	
	@ResponseBody
	@RequestMapping(value="/showMenu")
	public List<RoleDto> showMenu(Model model){
		Long partnerId = super.getPartnerId();
		List<RoleDto> list = roleService.getRolesByPartnerId(partnerId, false);
		return list;
	}
	
	@RequestMapping(value = "/detail")
	public String roleDetail(Model model, Long id,Long pid) {
		RoleDto roleDto = null;
		if (id!=null) {
			 roleDto = roleService.getRole(id);
		}else{//新增
			roleDto  =  new RoleDto();
			roleDto.setPid(pid);
		}
		List<DictCodeDto> dictCodeDtoList = dictService.getCodes(Constants.IP_ROLE_SCOPE);
		model.addAttribute("roleScopeList", dictCodeDtoList);
		model.addAttribute("roleDto", roleDto);
		return "platform/role/roleDetail.jsp";
	}
	
	@RequestMapping("/addRole/{id}")
	public String addAuthNode(Model model,@PathVariable("id") Long pid) {
		RoleDto roleDto = new RoleDto();
		roleDto.setPid(pid);
		model.addAttribute("roleDto", roleDto);
		return "platform/role/roleDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrupdate", method = RequestMethod.POST)
	public Map<String, Object> saveOrupdate(@ModelAttribute("roleDto") @Validated RoleDto roleDto,
			Model model,BindingResult br) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		String type = null;
		String txt = null;
		if(br.hasErrors()){
			type = "error";
			txt = "数据校验不通过。";
		}else{
			if (roleDto.getPartnerId() == null) {
				roleDto.setPartnerId(super.getPartnerId());
				Long id = roleService.addRole(roleDto);
				//roleDto.setId(id);
				if (id != null) {
					type = "success";
					txt = "新增成功。";
				} else {
					type = "error";
					txt = "新增失败。";
				}
				
				result.put("id", id);
			} else {
				boolean rs = roleService.updateRole(roleDto);
				if (rs) {
					type = "success";
					txt = "修改成功。";
				} else {
					type = "error";
					txt = "修改失败。";
				}
			}
			
			//result.put("obj", roleDto);
		}
		
		result.put("type", type);
		result.put("txt", txt);
		return result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkRoleCode")
	public Message checkRoleCode(Model model, @RequestParam("code") String code,@RequestParam("id") Long id, @RequestParam("partnerId") Long partnerId) {
		if(StringUtils.isEmpty(code)) {
			return Message.success("yes");
		}

		RoleDto roleDto = roleService.getByCode(partnerId,code);
		if(roleDto == null || roleDto.getId().equals(id)){
			return Message.success("yes");
		}else{
			return Message.error("no");
		}
		
	}
	
	@RequestMapping("/getRole/{id}")
	public String getPartnerOrgGrade(Model model , @PathVariable("id") Long	id){
		if(id!=null){
			RoleDto roleDto = roleService.getRole(id);
			model.addAttribute("roleDto",roleDto);
		}
		return "platform/role/roleDetail.jsp";
	}
	
	
	@ResponseBody
	@RequestMapping("/deleteRole")
	public Message detele( @RequestParam("id") Long id){
		//删除已经关联的用户
		userRoleService.deleteByRoleId(id);
		
		boolean rs = roleService.deleteRole(id);
		if(rs){
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败");
		}
	}
	
	
}