package com.tem.platform.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.web.Message;
import com.tem.platform.api.DictService;
import com.tem.platform.api.DistrictService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.DistrictDto;
import com.tem.platform.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("platform/district")
public class DistrictController extends BaseController {

	@Autowired
	private DistrictService districtService;
	
	@Autowired
	private DictService dictService;
	
	@RequestMapping(value="")
	public String index(Model model){
		model.addAttribute("levelList",getLevelList());
		return "platform/district/index.base";
	}
	
	//分页查询
	@RequestMapping(value="list")
	public String list(Model model,Integer pageIndex , Integer pageSize,Integer level ,String name,Integer districtId){
	   if(pageIndex==null){
		   pageIndex = 1;
	   }
	   if(pageSize==null){
		   pageSize = Page.DEFAULT_PAGE_SIZE;  
	   }
	   
	   QueryDto<Object> queryDto = new QueryDto<Object>(pageIndex, pageSize);
	 
		if(level!=null){
			queryDto.setField1(level.toString());
		}
		if(name!=null && !"".equals(name)){
			queryDto.setField2(name);
		}
		if(districtId!=null){
			queryDto.setField4(districtId);
		}
		
		Page<DistrictDto> pageList = districtService.queryListWithPage(queryDto);

		model.addAttribute("pageList", pageList);
		return "platform/district/districtList.jsp";
	}
	
	//获取所有区域
	@ResponseBody
	@RequestMapping(value="/getTree")
	public List<DistrictDto> getTree(){
		List<DistrictDto> list = districtService.findAll(null);
		return list;
	}
	
	
	//批量删除
	@ResponseBody
	@RequestMapping("/delete")
	public Message delete(String id){
		String[] split = id.split(",");
		Integer[] ids= new Integer[split.length];
		for(int i=0;i<split.length;i++){
			ids[i] = Integer.parseInt(split[i]);
		}
		boolean rs = districtService.deletes(ids);
		if(rs){
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败，请刷新重试");
		}
	}
	
	//用于新增和修改时查询对象
	@RequestMapping(value="/getDistrict")
	public String getDistrict(Model model,Integer id,Integer parentId){
	    DistrictDto districtDto = null;
	    if(id!=null){//修改时
	    	 districtDto = districtService.findById(id);
	    	 //区域类型
	    	 model.addAttribute("typeList", loadTypeList(districtDto.getLevel()));
	    	 //上一级区域列表
	    	 model.addAttribute("parentList", getDistrictList(districtDto.getLevel()));
	    }else if(parentId!=null){//有节点id时，将其作为新增节点的父节点
	    	 //查询父节点
		    DistrictDto parent = districtService.findById(parentId);
		    model.addAttribute("parent", parent);
	    	 //区域类型
	    	model.addAttribute("typeList", loadTypeList((Integer.parseInt(parent.getLevel())+1)+""));
	    	 //上一级区域列表
	    	model.addAttribute("parentList", getDistrictList((Integer.parseInt(parent.getLevel())+1)+""));
	    }
	    model.addAttribute("districtDto", districtDto);
	    //区域级别
	    model.addAttribute("levelList",getLevelList());
	    
		return "platform/district/districtModel.base";
	}
	
	//保存对象
	@ResponseBody
	@RequestMapping(value="save")	
	public Message addOrUpdate(Model model,DistrictDto districtDto){
		if(districtDto.getId()==null){//新增
			if(districtDto.getpId()==-1){//国际区域
				districtDto.setpId(null);
			}
			int resultId = districtService.add(districtDto);
		    if(resultId> 0){
			   return Message.success("添加成功");
		    }
			return Message.error("添加失败,请刷新重试");
		}else{
			boolean rs = districtService.update(districtDto);
			if(rs){
				return Message.success("修改成功");
			}
			return Message.error("修改失败,请刷新重试");
		}

	}
	
	//查询行政区级别（字典项）
	@ResponseBody
	@RequestMapping("/getLevelList")
	public List<DictCodeDto> getLevelList(){
		List<DictCodeDto> list = dictService.getCodes(Constants.IP_DISTRICT_LEVEL);
		return list;
	}
	
	//查询行政区类型（字典项）
	@ResponseBody
	@RequestMapping("/loadTypeList")
	public List<DictCodeDto> loadTypeList(String cust){
		List<DictCodeDto> list = dictService.getCodes(Constants.IP_DISTRICT_TYPE, cust, null);
		return list;
	}
	
	//根据传入的区域类型查询其上一层次区域列表
	@ResponseBody
	@RequestMapping("/getDistrictList")
	public List<DistrictDto> getDistrictList(String level){
		List<DistrictDto> list = null;
		if(level !=null ){
			int levelValue = Integer.parseInt(level);
			levelValue--;
			list = districtService.findAll(levelValue);
		}
		return list;
	}
	
	//验证编码唯一
	@ResponseBody
	@RequestMapping("/checkCode")
	public String checkCode(String code,Integer id) {
		if (code == null || "".equals(code)) {
			return "ok";
		}
		DistrictDto districtDto = districtService.findByCode(code);
		if (districtDto == null) {
			return "ok";
		}else{
			if(districtDto.getId().equals(id)){
				return "ok";
			}else{
				return "no";
			}
		}
	}
	
	//判断是否能够直接删除
	@ResponseBody
	@RequestMapping("/checkDeleteAction")
	public boolean checkDeleteAction(@RequestParam("ids")String idString){
		String[] split = idString.split(",");
		Integer[] ids= new Integer[split.length];
		for(int i=0;i<split.length;i++){
			ids[i] = Integer.parseInt(split[i]);
		}
		for(Integer id : ids){
			List<DistrictDto> list = districtService.findChild(id);
			if(list!=null && list.size() > 0 ){
				return false;
			}
		}
		return true;
	}
	
}
