package com.tem.platform.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.cache.utils.RedisCacheUtils;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.platform.api.WhiteListService;
import com.tem.platform.api.condition.WhiteListCondition;
import com.tem.platform.api.dto.WhiteListDto;
import com.tem.platform.form.WhiteListForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *ip白名单设置
 * 
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("ex/whlist")
public class WhiteListController extends BaseController {
	
	@Autowired
	private	WhiteListService whiteListService;
		
	@RequestMapping(value="")
	public String index(Model model,@RequestParam(required=false) Long id,@RequestParam(value="pageIndex",required=false)Integer pageIndex,@RequestParam(value="keyword",required=false)String keyword){
		WhiteListCondition whiteListCondition = new WhiteListCondition();
		
		if(StringUtils.isNotEmpty(keyword)){
			whiteListCondition.setKeyWord(keyword);
		}
		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<WhiteListCondition> queryDto = new QueryDto<WhiteListCondition>(pageIndex,10);
		queryDto.setCondition(whiteListCondition);
		
		Page<WhiteListDto> whiteListDtoList = whiteListService.queryListWithPage(queryDto);
		
		model.addAttribute("pageList", whiteListDtoList);
		model.addAttribute("keyword", keyword);
		return "platform/whiteList/whiteList.base";
	}
	
	@RequestMapping(value="detail")
	public String detail(Model model,@RequestParam(required=false) Long id){
		WhiteListForm rs = new WhiteListForm();
		WhiteListDto whiteListDto = new WhiteListDto();
		if(id != null){
			whiteListDto = whiteListService.getById(id);
		}
		rs = TransformUtils.transform(whiteListDto, WhiteListForm.class);
		if(rs.getEffectTime() == null){
			rs.setEffectTime(new Date());
		}
		model.addAttribute("whiteListDto", rs);
		return "platform/whiteList/whiteListDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value="saveOrUpdate")
	public	Message	saveOrUpdate(Model model,WhiteListDto whiteListDto,@RequestParam(value="effectTime1",required=false)String effectTime1){
		if(StringUtils.isNotEmpty(effectTime1)){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date effectTime = sdf.parse(effectTime1);
				whiteListDto.setEffectTime(effectTime);
			} catch (ParseException e) {
				return Message.error("日期填写格式不正确,正确格式为：yyyy-MM-dd");
			}
		}else{
			return Message.error("请填写有效期");
		}
		
		if(whiteListDto.getId() == null){//新增
			whiteListService.save(whiteListDto);
		}else{//修改
			whiteListService.update(whiteListDto);
		}
		
		//更新缓存
		WhiteListCondition whiteListCondition = new WhiteListCondition();
		QueryDto<WhiteListCondition> queryDto = new QueryDto<WhiteListCondition>(1,9999);
		queryDto.setCondition(whiteListCondition);
		
		Page<WhiteListDto> whiteListDtoList = whiteListService.queryListWithPage(queryDto);
		RedisCacheUtils.put("PT_WHITE_LIST", whiteListDtoList.getList(), RedisCacheUtils.ONE_MONTH);
		return Message.success("处理成功");
	}
	
	@ResponseBody
	@RequestMapping(value="deleteWhiteList")
	public	Message deleteWhiteList(Model model,@RequestParam("id")Long id){
		whiteListService.delete(id);
		return	Message.success("处理成功");
	}
	
}
