package com.tem.platform.action;

import com.tem.platform.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by chenjieming on 2017/9/20.
 */
@Controller
@RequestMapping("platform/userSecurity")
public class UserSecurityController extends BaseController {

    @Autowired
    private UserService userService;

    //密码弹窗
    @RequestMapping(value="index")
    public String checkPassword(Model model) {
        model.addAttribute("user",getCurrentUser());
        return "platform/userSecurity/index.base";
    }

    //异步验证原始密码是否正确
    @ResponseBody
    @RequestMapping(value = "verifyPassword")
    public Boolean verifyPassword(Model model,@RequestParam String currentPassword) {
        return userService.verifyPassword(super.getCurrentUser().getId(),currentPassword);
    }

    //修改密码
    @ResponseBody
    @RequestMapping(value = "updatePwd", method = RequestMethod.POST)
    public boolean updatePwd(String currentPassword, String newPassword) {
        Long userId = super.getCurrentUser().getId();

        Integer integer = userService.updatePassword(userId, currentPassword, newPassword);

        return integer.equals(0);
    }

}
