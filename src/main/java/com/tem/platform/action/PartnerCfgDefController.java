package com.tem.platform.action;

import com.iplatform.common.web.Message;
import com.tem.platform.api.DictService;
import com.tem.platform.api.PartnercfgCfgItemService;
import com.tem.platform.api.PartnercfgDefService;
import com.tem.platform.api.PartnercfgDictDataService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.PartnercfgCfgItemDto;
import com.tem.platform.api.dto.PartnercfgDefDto;
import com.tem.platform.api.dto.PartnercfgDictDataDto;
import com.tem.platform.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("platform/partnerCfgDef")
public class PartnerCfgDefController extends BaseController {
	
	@Autowired
	private PartnercfgDefService partnercfgDefService;
	
	@Autowired
	private PartnercfgCfgItemService partnercfgCfgItemService;
	
	@Autowired
	private PartnercfgDictDataService partnercfgDictDataService;
	
	@Autowired
	private DictService dictService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String page(Model model,HttpServletRequest req) {
		List<DictCodeDto> dictCodeDtoList = new ArrayList<DictCodeDto>();
		DictCodeDto dicCoodeDto = new DictCodeDto();
		dicCoodeDto.setItemCode(Constants.COMMON);
		dicCoodeDto.setItemTxt("公共系统");
		dictCodeDtoList.add(dicCoodeDto);
		dictCodeDtoList.addAll(dictService.getCodes(Constants.IP_SYS_LIST));
		
		model.addAttribute("sysId", super.getSysId());
		model.addAttribute("dictCodeDtoList", dictCodeDtoList);
		return "platform/partnerCfgDef/partnerCfgDef.base";
	}
	
	@ResponseBody
	@RequestMapping("/getCfgTree/{sysId}")
	public List<PartnercfgDefDto> getCfgTree(@PathVariable("sysId") String sysId){
		List<PartnercfgDefDto> list = partnercfgDefService.getAllBySysId(sysId);
		return list;
	}
	
	@RequestMapping("/getCfgDetail/{id}/{pid}")
	public String getCfgDetail(Model model,@PathVariable("id") Long id,@PathVariable("pid") Long pid){
		PartnercfgDefDto partnercfgDefDto = null;
		if(id!=null && id != 0){
			//查询节点信息
			partnercfgDefDto = partnercfgDefService.selectById(id);
		}else{
			//新增节点
			partnercfgDefDto = new PartnercfgDefDto();
			partnercfgDefDto.setPid(pid==0?null:pid);
		}
		model.addAttribute("partnercfgDefDto",partnercfgDefDto);
		return "platform/partnerCfgDef/partnerCfgDefDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/checkCode/{sysId}")
	public String checkCode(String nodeCode, @PathVariable("sysId")String sysId,Long id) {
		if (nodeCode == null || "".equals(nodeCode)) {
			return "ok";
		}
		PartnercfgDefDto partnercfgDefDto = partnercfgDefService.getByNodeCode(nodeCode);
		if (partnercfgDefDto == null) {
			return "ok";
		}else if(id!=null){
			if(partnercfgDefDto.getId().equals(id)){
				return "ok";
			}
		}else{
			return "no";
		}
		return "no";
	}
	
	
	@ResponseBody
	@RequestMapping("/saveOrupdate/{sysId}/{nodeType}")
	public Message saveOrupdate(PartnercfgDefDto partnercfgDefDto,@PathVariable("sysId")String sysId,
			@PathVariable("nodeType")Long nodeType){
		boolean rs = false;
		if(partnercfgDefDto.getId()==null){
			partnercfgDefDto.setSysId(sysId);
			rs = partnercfgDefService.insert(partnercfgDefDto) > 0;
			if(rs){
				PartnercfgDefDto partnercfgDef = partnercfgDefService.getByNodeCode(partnercfgDefDto.getNodeCode());
				if(partnercfgDef != null){
					return Message.success(partnercfgDef.getId().toString());
				}
				return Message.success("新增成功");
			}else{
				return Message.error("新增失败");
			}
		}else{
			rs = partnercfgDefService.updateSelective(partnercfgDefDto) > 0;
			if(rs){
				return Message.success("修改成功");
			}else{
				return Message.error("修改失败");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping("/deleteNode")
	public Message deleteNode(Long id){
		boolean rs = false;
		if(id!=null){
			rs = partnercfgDefService.deleteById(id) > 0;
		}
		if(rs){
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败，请刷新重试");
		}
	}
	
	
	@RequestMapping("/getCfgDetailByCode/{sysId}/{nodeCode}")
	public String getCfgDetailByCode(Model model,@PathVariable("sysId") String sysId,@PathVariable("nodeCode") String nodeCode){
		PartnercfgDefDto partnercfgDefDto = partnercfgDefService.getByNodeCode(nodeCode);
		model.addAttribute("partnercfgDefDto",partnercfgDefDto);
		return "platform/partnerCfgDef/partnerCfgDefDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/getCfgItemTreeTable/{sysId}/{cfgnodeCode}")
	public List<PartnercfgCfgItemDto> getCfgItemTreeTable(@PathVariable("cfgnodeCode")String cfgnodeCode,@PathVariable("sysId")String sysId){
		List<PartnercfgCfgItemDto> list = partnercfgCfgItemService.getAllByCfgnodeCode(cfgnodeCode);
		return list;
	}
	
	@RequestMapping(value = "/selectPartnerCfgTree")
	public String selectPartnerCfgTree(Model model) {
		return "platform/partnerCfgDef/selectPartnerCfgTree.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/movePartnerCfg")
	public Message movePartnerCfg(Model model, @RequestParam("fromId") Long fromId, @RequestParam("toId") Long toId) {
		PartnercfgDefDto partnercfgDefDto = partnercfgDefService.selectById(fromId);
		if(partnercfgDefDto != null){
			partnercfgDefDto.setPid(toId);
			if(partnercfgDefService.update(partnercfgDefDto)>0) {
                return Message.success("移动成功");
            }
			return Message.error("移动失败");
			
		}else{
			return Message.error("移动失败");
		}
	}
	
	
	@RequestMapping("/addItemNode/{pid}")
	public String addItemNode(Model model,@PathVariable("pid") Long pid) {
		PartnercfgCfgItemDto partnercfgCfgitemDto = new PartnercfgCfgItemDto();
		if(pid == -1){
			partnercfgCfgitemDto.setPid(null);
		}else{
			partnercfgCfgitemDto.setPid(pid);
		}
		model.addAttribute("partnercfgCfgitemDto", partnercfgCfgitemDto);
		model.addAttribute("typeTitle", "新增配置项");
		return "platform/partnerCfgDef/partnercfgCfgitemDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/checkPartnerItemCode")
	public String checkPartnerItemCode(String itemCode, String sysId,Long id) {
		if (itemCode == null || "".equals(itemCode)) {
			return "ok";
		}
		PartnercfgCfgItemDto partnercfgCfgitemDto = partnercfgCfgItemService.getByItemCode(itemCode);
		if (partnercfgCfgitemDto == null) {
			return "ok";
		}else if(id!=null){
			if(partnercfgCfgitemDto.getId().equals(id)){
				return "ok";
			}
		}else{
			return "no";
		}
		return "no";
	}
	
	@ResponseBody
	@RequestMapping("/saveOrupdatePartnerItem/{sysId}/{cfgnodeCode}")
	public Message saveOrupdatePartnerItem(PartnercfgCfgItemDto partnercfgCfgitemDto,@PathVariable("sysId")String sysId,
			@PathVariable("cfgnodeCode")String cfgnodeCode){
		boolean rs = false;
		if(partnercfgCfgitemDto.getId()==null){
			partnercfgCfgitemDto.setCfgnodeCode(cfgnodeCode);
			rs = partnercfgCfgItemService.insert(partnercfgCfgitemDto) > 0; 
			if(rs){
				return Message.success("新增成功");
			}else{
				return Message.error("新增失败");
			}
		}else{
			rs = partnercfgCfgItemService.updateSelective(partnercfgCfgitemDto) > 0;
			if(rs){
				return Message.success("修改成功");
			}else{
				return Message.error("修改失败");
			}
		}
	}
	
	@RequestMapping("/getItemNode/{id}")
	public String getItemNode(Model model,@PathVariable("id") Long id){
		PartnercfgCfgItemDto partnercfgCfgitemDto = partnercfgCfgItemService.selectById(id);
		model.addAttribute("partnercfgCfgitemDto",partnercfgCfgitemDto);
		return "platform/partnerCfgDef/partnercfgCfgitemDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/deletePartnerItem")
	public Message deletePartnerItem(Long id){
		boolean rs = false;
		if(id!=null){
			rs = partnercfgCfgItemService.deleteById(id) > 0;
		}
		if(rs){
			return Message.success("删除配置项成功");
		}else{
			return Message.error("删除配置项失败，请刷新重试");
		}
	}
	
	@ResponseBody
	@RequestMapping("/hasChild")
	public String hasChild(Long id){
		List<PartnercfgCfgItemDto> list = partnercfgCfgItemService.getAllByPid(id);
		if(list.size()>0){//有子节点
			return "no";
		}else{//无子节点
			return "ok";
		}
	}
	
	@ResponseBody
	@RequestMapping("/checkDictCode/{sysId}")
	public String checkDictCode(String refDict, @PathVariable("sysId")String sysId,Long id) {
		if (refDict == null || "".equals(refDict)) {
			return "ok";
		}
		 PartnercfgDefDto partnercfgDefDto = partnercfgDefService.getByRefDict(refDict);
		if (partnercfgDefDto == null) {
			return "ok";
		}else if(id!=null){
			if(partnercfgDefDto.getId().equals(id)){
				return "ok";
			}
		}else{
			return "no";
		}
		return "no";
	}
	
	@ResponseBody
	@RequestMapping("/checkDictData/{sysId}/{dictCode}")
	public boolean  checkDictData(@PathVariable("dictCode")String dictCode,@PathVariable("sysId")String sysId){
		List<PartnercfgDictDataDto> list = partnercfgDictDataService.getByPartnerIdAndDictCode(super.getPartnerId(), dictCode);
		if(list.size()>0){
			return true;
		}else{
			return false;
		}
		
	}
	

}
