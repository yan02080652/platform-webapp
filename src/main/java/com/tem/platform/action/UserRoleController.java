package com.tem.platform.action;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.iplatform.common.web.Message;
import com.tem.platform.action.widget.CommonWg;
import com.tem.platform.api.RoleService;
import com.tem.platform.api.UserRoleService;
import com.tem.platform.api.dto.RoleDto;
import com.tem.platform.api.dto.UserRoleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author chenjieming
 */
@Controller
@RequestMapping("platform/user/userRole")
public class UserRoleController extends CommonWg {

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "")
    public String index(Model model, UserRoleDto userRole) {

        Map<String, Object> params = new HashMap<>(16);
        params.put("sort", "EDIT_DATE");
        params.put("dir", "asc");
        params.put("userId", userRole.getUserId());

        List<UserRoleDto> userRoleList = userRoleService.getList(params);

        Integer count = userRoleService.getUserRoleCount(params);

        model.addAttribute("userId", userRole.getUserId());
        model.addAttribute("count", count);
        model.addAttribute("pageList", userRoleList);

        return "platform/userRole/userRole-manager.jsp";
    }

    @RequestMapping(value = "/load")
    public String list(Model model, Long id, String userId) {

        UserRoleDto vo = new UserRoleDto();
        if (id != null) {
            vo = userRoleService.getUserRole(id);
        } else {
            vo.setValidFrom(new Date());
        }

        String type = super.getStringValue("type");
        if("tmc".equals(type) || "me".equals(type)){
            List<RoleDto> roleDtos = roleService.getRolesByPartnerId(super.getPartnerId(), false);
            if(CollectionUtils.isEmpty(roleDtos)){
                model.addAttribute("showNoRoleMessage","请联系系统管理员添加角色!");
            }
        }

        model.addAttribute("userId", userId);
        model.addAttribute("userRole", vo);
        return "platform/userRole/userRole-model.jsp";
    }

    @ResponseBody
    @RequestMapping(value = "delete")
    public Message delete(Long id) {

        if (userRoleService.deleteUserRole(id)) {
            return Message.success("删除成功");
        }
        return Message.error("删除失败");
    }

    @ResponseBody
    @RequestMapping(value = "save")
    public Message addOrUpdate(UserRoleDto dto) {
    	

        if (dto.getId() == null) {
        	if(dto.getUserId()!=null){
        		List<UserRoleDto> userRoleList = userRoleService.getUserRoles(dto.getUserId());
        		for(int i=0 ; i<userRoleList.size(); i++){
        			if(dto.getRoleId().equals(userRoleList.get(i).getRoleId())){
        				return Message.error("已存在角色，请重新选择！");
        			}
        		}
        	}

            dto.setAssigner(super.getCurrentUser().getId());
            if (userRoleService.addUserRole(dto)) {
                return Message.success("添加成功");
            }
            return Message.error("添加失败");
        } else {
            dto.setEditUser(super.getCurrentUser().getId());
            dto.setEditDate(new Date());

            if (userRoleService.updateUserRole(dto)) {
                return Message.success("更新成功");
            }
            return Message.error("更新失败");
        }
    }
}