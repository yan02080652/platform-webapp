package com.tem.platform.action;

import com.tem.platform.Constants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 访问指定场景
 * @author wls
 *
 */
@Controller
public class SceneController extends BaseController {

//	@RequestMapping(value="admin", method=RequestMethod.GET)
//	public String admin(Model model,HttpServletRequest req, HttpServletResponse res) {
//		return this.setValue("ADMIN", "H2J83h8DFNH28usf82", req, res);
//	}
	
	public String setValue(String sceneId, String cookieValue,HttpServletRequest req, HttpServletResponse res) {
		req.getSession().setAttribute(Constants.SESSION_KEY_SCENE, sceneId);
		
		Cookie c = new Cookie("_pt_se_", cookieValue);
		c.setPath("/");
		c.setMaxAge(-1);
		res.addCookie(c);
		
		return "index.base";
	}
	
	@RequestMapping(value="oper", method=RequestMethod.GET)
	public String oper(Model model,HttpServletRequest req, HttpServletResponse res) {

		getSession().removeAttribute(Constants.SESSION_KEY_PARTNER);

		return this.setValue("OPERATION", "dsf983J92is98K38dk", req, res);
	}
	
	@RequestMapping(value={"service","/"}, method=RequestMethod.GET)
	public String customerService(Model model,HttpServletRequest req, HttpServletResponse res) {
		
		super.getSession().removeAttribute(Constants.SESSION_KEY_PARTNER);

		return this.setValue("SERVICE", "DSJFK32Ijs9k83K8ks", req, res);
	}
	
//	@RequestMapping(value={"tem/index", "/","/favicon.ico"}, method=RequestMethod.GET)
//	public String indexService(Model model,HttpServletRequest req, HttpServletResponse res) {
//		return "index.base";
//	}
}
