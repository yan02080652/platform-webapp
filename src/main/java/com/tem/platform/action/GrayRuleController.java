package com.tem.platform.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.tem.platform.api.GrayRuleService;
import com.tem.platform.api.condition.GrayRuleCondition;
import com.tem.platform.api.dto.GrayRuleDto;
import com.tem.platform.api.enums.ScopeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * 灰度环境配置
 * @author chenjieming
 * @date 2018/1/25
 */
@Controller
@RequestMapping("platform/grayRule")
public class GrayRuleController extends BaseController{

    @Autowired
    private GrayRuleService grayRuleService;

    /**
     * 获取列表
     * @param pageIndex
     * @param scopeType
     * @return
     */
    @RequestMapping("index")
    public String index(Integer pageIndex,String scopeType, Model model){
        if(pageIndex == null){
            pageIndex = 1;
        }

        QueryDto<GrayRuleCondition> queryDto=new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE ,"ID" ,"desc");

        GrayRuleCondition grayRuleCondition = new GrayRuleCondition();
        grayRuleCondition.setScopeType(scopeType);
        queryDto.setCondition(grayRuleCondition);

        Page<GrayRuleDto> pageList = grayRuleService.queryListWithPage(queryDto);
        model.addAttribute("pageList",pageList );
        model.addAttribute("scopeType", scopeType);

        Map<String, String> hashMap = new HashMap<>(16);

        for(ScopeType instance : ScopeType.values()){
            hashMap.put(instance.name(), instance.getMessage());
        }
        model.addAttribute("ScopeMap", hashMap);

        return "platform/grayRule/index.base";
    }

    @RequestMapping("getGrayRule")
    public String getGrayRule(Long id, Model model){
        GrayRuleDto grayRule = grayRuleService.getById(id);
        model.addAttribute("grayRule", grayRule);

        return "platform/grayRule/grayRule-model.jsp";
    }

    /**
     * 保存修改灰度环境
     * @param grayRuleDto
     * @return
     */
    @ResponseBody
    @RequestMapping("save")
    public ResponseInfo save(GrayRuleDto grayRuleDto){
        boolean save = grayRuleService.save(grayRuleDto);
        if(save){
            return new ResponseInfo("0","保存成功");
        }else {
            return new ResponseInfo("1","保持失败");
        }
    }

    @ResponseBody
    @RequestMapping("delete")
    public ResponseInfo delete(Long id){

        boolean flag = grayRuleService.delete(id);
        if(flag){
            return new ResponseInfo("0","删除成功");
        }else {
            return new ResponseInfo("1","删除失败");
        }
    }

}
