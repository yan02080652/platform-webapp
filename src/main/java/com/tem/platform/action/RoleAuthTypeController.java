package com.tem.platform.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.IdWorker;
import com.iplatform.common.web.Message;
import com.tem.platform.api.DictService;
import com.tem.platform.api.RoleAuthService;
import com.tem.platform.api.RoleService;
import com.tem.platform.api.TransService;
import com.tem.platform.api.UserRoleService;
import com.tem.platform.api.condition.RoleAuthTypeCondition;
import com.tem.platform.api.condition.UserCondition;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.OrgDto;
import com.tem.platform.api.dto.OrgUserDto;
import com.tem.platform.api.dto.RoleAuthtypeDto;
import com.tem.platform.api.dto.RoleDto;
import com.tem.platform.api.dto.RoleMenuDto;
import com.tem.platform.api.dto.TransDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.platform.form.RoleGroupForm;
import com.tem.platform.security.authorize.RoleAuthtype;
import com.tem.platform.Constants;
import com.tem.supplier.dto.SettleChannelDto;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("platform/roleAuthType")
public class RoleAuthTypeController extends BaseController {

	@Autowired
	private RoleService roleService;
	
	@Autowired
	private RoleAuthService roleAuthService;
	
	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private TransService transService;
	
	@RequestMapping("")
	public String roleAuthType(Model model,HttpServletRequest req){
		String sysId = req.getParameter("sysId");
		if(StringUtils.isEmpty(sysId)){
			sysId = Constants.PLATFORM;
		}
		List<DictCodeDto> dictCodeDtoList = dictService.getCodes(Constants.IP_SYS_LIST);
		model.addAttribute("sysId", sysId);
		model.addAttribute("dictCodeDtoList", dictCodeDtoList);
		model.addAttribute("type", "tmc");
		return "platform/roleAuthType/roleAuthType.base";
	}
	
	@ResponseBody
	@RequestMapping(value="/showMenu")
	public List<RoleDto> showMenu(Model model){
		Long partnerId = super.getPartnerId();
		List<RoleDto> list = roleService.getRolesByPartnerId(partnerId, false);
		return list;
	}
	
	@RequestMapping(value = "/selectTrans", method = RequestMethod.GET)
	public String selectTrans(Model model) {
		return "platform/roleAuthType/transTree.jsp";
	}
	
	@RequestMapping(value = "list")
    public String list(Model model, Long id, Integer pageIndex, Long partnerId, String value) {
        pageIndex = pageIndex == null ? 1 : pageIndex;
        if(partnerId == null){
        	partnerId = super.getPartnerId();
        }
        
        QueryDto<RoleAuthTypeCondition> queryDto = new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);
        
        RoleAuthTypeCondition roleAuthTypeCondition = new RoleAuthTypeCondition();
        roleAuthTypeCondition.setPartnerId(partnerId);
        roleAuthTypeCondition.setRoleId(id);
        if(value != null){
        	roleAuthTypeCondition.setValue(value);
        }
        queryDto.setCondition(roleAuthTypeCondition);
        Page<RoleAuthtypeDto> pageList = roleAuthService.queryListWithPage(queryDto);
        
        if(pageList.getList().size()!=0&&value != null){        	
        	Long groupId = pageList.getList().get(0).getGroupId();
        	
        	
        	QueryDto<RoleAuthTypeCondition> queryDtos = new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);
        	RoleAuthTypeCondition roleAuthTypeConditions = new RoleAuthTypeCondition();
        	roleAuthTypeConditions.setPartnerId(partnerId);
        	roleAuthTypeConditions.setRoleId(id);
        	roleAuthTypeConditions.setGroupId(groupId);
        	
        	queryDtos.setCondition(roleAuthTypeConditions);
        	
        	Page<RoleAuthtypeDto> pageLists = roleAuthService.queryListWithPage(queryDtos);
        	
        	model.addAttribute("pageList", pageLists);
        }else {
        	model.addAttribute("pageList", pageList);
        }

        return "platform/roleAuthType/role-list.jsp";
    }
	
	
	@RequestMapping("/detail")
    public String detail(Model model, Long groupId, Long partnerId, Long roleId) {
		RoleGroupForm roleGroupForm = new RoleGroupForm();
		if(partnerId!=null){
			roleGroupForm.setPartnerId(partnerId);
		}
		if(roleId!=null){
			roleGroupForm.setRoleId(roleId);
		}
		if(groupId!=null){
			 List<RoleAuthtypeDto> list = roleAuthService.getRoleAuthtypesByGroupId(groupId);
			 if(list.size()>0){				 
				 for(int i = 0; i<list.size(); i++){
					 RoleAuthtypeDto ratd = list.get(i);
					 if(ratd.getFieldId().equals("TRANSID")){
						 roleGroupForm.setValue(ratd.getValue());
						
						 TransDto transDto = transService.getByCode(ratd.getValue());
						 if(transDto!=null){							 
							 roleGroupForm.setActivitys(transDto.getActivitys());
						 }
					 }else if(ratd.getFieldId().equals("ACTIVITY")){
						 roleGroupForm.setId(ratd.getId());
						 roleGroupForm.setGroupId(groupId);;
						 roleGroupForm.setActivity(ratd.getValue());
					 }
						 
				 }
			 }
		}
		
		 model.addAttribute("roleGroupForm", roleGroupForm);
        return "platform/roleAuthType/role-detail.jsp";
    }
	
	@ResponseBody
	@RequestMapping("/save")
	public ResponseInfo save(RoleGroupForm roleGroupForm){
    	
		if(roleGroupForm.getId()==null){
			//新增
			if(roleGroupForm.getValue().equals("")){
				return new ResponseInfo("2", "请选择作业");
			}
			if(roleGroupForm.getPartnerId()==null){
				roleGroupForm.setPartnerId((long) 0);
			}
			boolean b = roleAuthService.addRoleGroup(roleGroupForm.getPartnerId(), roleGroupForm.getRoleId(),
					roleGroupForm.getValue(), roleGroupForm.getActivity());
			if(b){
				return new ResponseInfo("0","保存成功");
			}else{
				return new ResponseInfo("1", "保存失败");
			}
			
		}else{
			//修改
			RoleAuthtypeDto roleAuthtype = new RoleAuthtypeDto();
			roleAuthtype.setId(roleGroupForm.getId());
			roleAuthtype.setPartnerId(roleGroupForm.getPartnerId());
			roleAuthtype.setRoleId(roleGroupForm.getRoleId());
			roleAuthtype.setAuthtypeCode("TRANS");
			roleAuthtype.setGroupId(roleGroupForm.getGroupId());
			roleAuthtype.setFieldId("ACTIVITY");
			if(roleGroupForm.getActivity()!=null){				
				roleAuthtype.setValue(roleGroupForm.getActivity());
			}
			boolean b = roleAuthService.updateRoleActivity(roleAuthtype);
			if(b){
    			return new ResponseInfo("0","修改成功");
    		}else{
    			return new ResponseInfo("1", "修改失败");
    		}
		}
	
	}
	
}