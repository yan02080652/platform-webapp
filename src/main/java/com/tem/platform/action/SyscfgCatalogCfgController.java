package com.tem.platform.action;

import com.iplatform.common.web.Message;
import com.tem.platform.api.*;
import com.tem.platform.api.dto.*;
import com.tem.platform.Constants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Controller
@RequestMapping("platform/syscfgCatalogCfg")
public class SyscfgCatalogCfgController extends BaseController {

	@Autowired
	private SyscfgCatalogService syscfgCatalogService;
	
	@Autowired
	private SyscfgCfgItemService syscfgCfgitemService;
	
	@Autowired
	private SyscfgDictDataService syscfgDictdataService;
	
	@Autowired
	private SyscfgItemDataService syscfgItemdataService;
	
	@Autowired
	private DictService dictService;
	

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String page(Model model,HttpServletRequest req) {
		List<DictCodeDto> dictCodeDtoList = new ArrayList<DictCodeDto>();
		DictCodeDto dicCoodeDto = new DictCodeDto();
		dicCoodeDto.setItemCode(Constants.COMMON);
		dicCoodeDto.setItemTxt("公共系统");
		dictCodeDtoList.add(dicCoodeDto);
		dictCodeDtoList.addAll(dictService.getCodes(Constants.IP_SYS_LIST));
		
		model.addAttribute("sysId", super.getSysId());
		model.addAttribute("dictCodeDtoList", dictCodeDtoList);
		return "platform/syscfgCatalog/syscfgcatalogCfg.base";
	}
	
	@ResponseBody
	@RequestMapping("/getCfgTree/{sysId}")
	public List<SyscfgCatalogDto> getCfgTree(@PathVariable("sysId") String sysId){
		List<SyscfgCatalogDto> list = syscfgCatalogService.getAllBySysId(sysId);
		return list;
	}
	
	@ResponseBody
	@RequestMapping("/getDic")
	public List<DictCodeDto> getDic(@RequestParam("sysId") String sysId,@RequestParam("dicCode") String dicCode){
		List<DictCodeDto> dictCodeDtoList = dictService.getCodes(dicCode);
		return dictCodeDtoList;
	}
	
	@ResponseBody
	@RequestMapping("/getCfgDicTreeTable")
	public List<SyscfgDictDataDto> getCfgDicTreeTable(@RequestParam("sysId") String sysId,@RequestParam("dictCode") String dictCode){
		List<SyscfgDictDataDto> list = syscfgDictdataService.getByDictCode(dictCode);
		return list;
	}
	
	@RequestMapping("/addDicNode/{curDicTreeId}")
	public String addDicNode(Model model, @PathVariable("curDicTreeId") Long curDicTreeId) {
		model.addAttribute("typeTitle", "新增字典项");
		SyscfgDictDataDto syscfgDictdataDto = null;
		if(curDicTreeId == -1){//添加根节点
			syscfgDictdataDto = new SyscfgDictDataDto();
		}else{//添加当前节点的子节点
			syscfgDictdataDto = syscfgDictdataService.selectById(curDicTreeId);
		}
		model.addAttribute("pid", syscfgDictdataDto.getId());
		return "platform/syscfgCatalog/syscfgDic.jsp";
	}
	
	@RequestMapping("/updateDicNode/{curDicTreeId}")
	public String updateDicNode(Model model, @PathVariable("curDicTreeId") Long curDicTreeId) {
		model.addAttribute("typeTitle", "修改字典项");
		SyscfgDictDataDto syscfgDictdataDto = syscfgDictdataService.selectById(curDicTreeId);
		model.addAttribute("pid", syscfgDictdataDto.getPid());
		model.addAttribute("syscfgDictdataDto", syscfgDictdataDto);
		return "platform/syscfgCatalog/syscfgDic.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrupdateSysDic", method = RequestMethod.POST)
	public Message saveOrupdateSysDic(@ModelAttribute("syscfgDictdataDto") @Validated SyscfgDictDataDto syscfgDictdataDto,
			Model model,BindingResult br,HttpServletRequest req) {
		if(br.hasErrors()){
			return Message.error("数据校验不通过。");
		}
		
		syscfgDictdataDto.setUpdateUser(super.getCurrentUser().getId());
		syscfgDictdataDto.setUpdateDate(new Date());
		
		int rs = 0;
		if (syscfgDictdataDto.getId() == null) {
			rs = syscfgDictdataService.insert(syscfgDictdataDto);
			if (rs > 0) {
				return Message.success("新增成功。");
			} else {
				return Message.error("新增失败。");
			}
		} else {
			rs = syscfgDictdataService.updateSelective(syscfgDictdataDto);
			if (rs > 0) {
				return Message.success("修改成功。");
			} else {
				return Message.error("修改失败。");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteSysDic")
	public Message deleteSysDic(Model model, @RequestParam("id") Long id) {
		if (id == null) {
			return Message.error("请先选择一行字典数据");
		}
		
		SyscfgDictDataDto syscfgDictdataDto = syscfgDictdataService.selectById(id);
		//删除判断子项
		List<SyscfgDictDataDto> syscfgDictdataDtoList = syscfgDictdataService.getAllByPid(id);
		if(syscfgDictdataDtoList != null && syscfgDictdataDtoList.size()>0){
			return Message.error("请先删除子节点");
		}else{
			//如果是组合配置项则下面还有值配置 需要删除
			
			//得到配置项,然后删除
			SyscfgCatalogDto syscfgCatalogDto = syscfgCatalogService.getByRefDict(syscfgDictdataDto.getDictCode());
			if(syscfgCatalogDto != null && syscfgCatalogDto.getNodeType() == 3){//组合配置组
				List<SyscfgItemDataDto> syscfgItemdataDtoList = syscfgItemdataService.getByRefDictAndDictitemCode(syscfgCatalogDto.getRefDict(), syscfgDictdataDto.getItemCode());
				if(syscfgItemdataDtoList!= null && syscfgItemdataDtoList.size()>0){
					for(SyscfgItemDataDto syscfgItemdataDto : syscfgItemdataDtoList){
						syscfgItemdataService.deleteById(syscfgItemdataDto.getId());
					}
				}
			}
			if(syscfgDictdataService.deleteById(id)>0) {
                return Message.success("删除成功");
            }
			return Message.error("删除失败");
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkSysDicCode")
	public Message checkSysDicCode(Model model, @RequestParam("itemCode") String itemCode,@RequestParam("id") Long id, @RequestParam("sysId") String sysId, @RequestParam("dictCode") String dictCode) {
		if(id != null){//修改下 code不能修改  id存在说明是修改状态下
			return Message.success("yes");
		}else {
			SyscfgDictDataDto syscfgDictdataDto = syscfgDictdataService.getByDictCodeAndItemCode(dictCode, itemCode);
			if (syscfgDictdataDto == null) {
				return Message.success("yes");
			}else{
				return Message.error("no");
			}
		}
		
	}
	
	@RequestMapping(value = "getSyscfgRight")
	public String getSyscfgRight(Model model,@RequestParam("nodeType") Integer nodeType) {
		String url = "";
		if(nodeType == 0){//目录
			url = "platform/syscfgCatalog/emptyInfo.jsp";
		}else if(nodeType == 1){//数据字典
			url = "platform/syscfgCatalog/syscfgDicMain.jsp";
		}else if(nodeType == 2){//配置组
			url = "platform/syscfgCatalog/syscfgItemMain.jsp";
		}else if(nodeType == 3){//组合配置组
			url = "platform/syscfgCatalog/syscfgDicMain.jsp";
		}else if(nodeType == 4){//其它作业
			
		}
		return url;
	}
	
	/**
	 * 得到配置值
	 * @param cfgnodeCode
	 * @param sysId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getCfgItemTreeTable")
	public List<HashMap<String, Object>> getCfgItemTreeTable(@RequestParam("cfgnodeCode")String cfgnodeCode,@RequestParam("sysId")String sysId,@RequestParam("dictitemCode")String dictitemCode){
		List<HashMap<String, Object>> resultList = null;
		List<SyscfgCfgItemDto> syscfgCfgitemDtoList = syscfgCfgitemService.getAllByCfgnodeCode(cfgnodeCode);//得到该配置定义的所有配置项
		if(syscfgCfgitemDtoList != null && syscfgCfgitemDtoList.size()>0){
			resultList = new ArrayList<HashMap<String,Object>>();
			List<SyscfgItemDataDto> syscfgItemdataDtoList = null;
			if(StringUtils.isNotEmpty(dictitemCode)){//说明是组合配置项
				SyscfgCatalogDto syscfgCatalogDto = syscfgCatalogService.getByNodeCode(cfgnodeCode);
				if(syscfgCatalogDto != null){
					syscfgItemdataDtoList = syscfgItemdataService.getByRefDictAndDictitemCode(syscfgCatalogDto.getRefDict(), dictitemCode);//已配置的配置项
				}
			}else{
				syscfgItemdataDtoList = syscfgItemdataService.getByCfgnodeCode(cfgnodeCode);//已配置的配置项
			}
			
			HashMap<String, Object> map = null;
			for(SyscfgCfgItemDto syscfgCfgitemDto : syscfgCfgitemDtoList){
				 map = new HashMap<String, Object>();
				 map.put("ID", syscfgCfgitemDto.getId());
				 map.put("PID", syscfgCfgitemDto.getPid());
				 map.put("CFGNODE_CODE", syscfgCfgitemDto.getCfgnodeCode());
				 map.put("ITEM_NAME", syscfgCfgitemDto.getItemName());
				 map.put("ITEM_CODE", syscfgCfgitemDto.getItemCode());
				 map.put("ITEM_TYPE", syscfgCfgitemDto.getItemType());
				 map.put("OPTIONAL_VALUES", syscfgCfgitemDto.getOptionalValues());
				 
				 if(syscfgItemdataDtoList != null && syscfgItemdataDtoList.size()>0){
					 for(SyscfgItemDataDto syscfgItemdataDto : syscfgItemdataDtoList){
						 String itemvalue = "";
						 if(syscfgItemdataDto.getCfgitemCode().equals(syscfgCfgitemDto.getItemCode())){
							 if(syscfgCfgitemDto.getItemType()==1 || syscfgCfgitemDto.getItemType()==2){//下拉框和checkbox转译
								 String values = syscfgItemdataDto.getValue();
								 if(values!=null){
									 String[] valuesArr = values.split(";");
									 String optionalValues = syscfgCfgitemDto.getOptionalValues();
									 String[] optionalValuesArr = null;
									 if(StringUtils.isNotEmpty(optionalValues)){
										 optionalValuesArr = optionalValues.split(";");
									 }else{
										 continue;
									 }
									 for(String value : valuesArr){
										 for(String optionalValue : optionalValuesArr){
											 String[] details = optionalValue.split(":");
											 if(value.equals(details[0])){
												 itemvalue = itemvalue + details[1]+" ; ";
												 continue;
											 }
										 }
									 }
								 }else{
									 continue;
								 }
								 if(StringUtils.isNotEmpty(itemvalue)){
									 itemvalue = itemvalue.substring(0, itemvalue.length()-2);
								 }else{
									 itemvalue = syscfgItemdataDto.getValue();//说明是手填的
								 }
							 }else{
								 itemvalue = syscfgItemdataDto.getValue();
							 }
							 map.put("ITEMVALUE", itemvalue);
							 map.put("DETAILID", syscfgItemdataDto.getId());
						 }
					 }
				 }
				 resultList.add(map);
			}
		}
		//List<HashMap<String, Object>> list = syscfgItemdataService.getCfgItemValue(sysId, cfgnodeCode);
		return resultList;
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveorupdateItemData")
	public Message saveorupdateItemData(Model model,HttpServletRequest req) {
		String detailid = req.getParameter("detailid");
		Long id = Long.parseLong(req.getParameter("id"));
		String cfgItemValue = req.getParameter("cfgItemValue");
		String dictitemCode = req.getParameter("dictitemCode");
		
		
		if(StringUtils.isNotEmpty(detailid)){//说明是修改
			SyscfgItemDataDto syscfgItemdataDto = syscfgItemdataService.selectById(Long.parseLong(detailid));
			syscfgItemdataDto.setValue(cfgItemValue);
			syscfgItemdataDto.setUpdateUser(super.getCurrentUser().getId());
			syscfgItemdataDto.setUpdateDate(new Date());
			if (syscfgItemdataService.updateSelective(syscfgItemdataDto) > 0) {
				return Message.success("配置成功！");
			} else {
				return Message.error("配置失败!请刷新重试!");
			}
		}else{//说明是新增
			SyscfgCfgItemDto syscfgCfgitemDto = syscfgCfgitemService.selectById(id);//得到该配置行
			SyscfgItemDataDto syscfgItemdataDto = new SyscfgItemDataDto();
			syscfgItemdataDto.setCfgitemCode(syscfgCfgitemDto.getItemCode());
			syscfgItemdataDto.setCfgnodeCode(syscfgCfgitemDto.getCfgnodeCode());
			syscfgItemdataDto.setValue(cfgItemValue);
			syscfgItemdataDto.setUpdateUser(super.getCurrentUser().getId());
			syscfgItemdataDto.setUpdateDate(new Date());
			
			if(StringUtils.isNotEmpty(dictitemCode)){//说明是组合配置项
				syscfgItemdataDto.setDictitemCode(dictitemCode);
				SyscfgCatalogDto syscfgCatalogDto = syscfgCatalogService.getByNodeCode(syscfgCfgitemDto.getCfgnodeCode());
				if(syscfgCatalogDto != null){
					syscfgItemdataDto.setRefDict(syscfgCatalogDto.getRefDict());
				}
			}
			if(syscfgItemdataService.insert(syscfgItemdataDto)>0){
				return Message.success("配置成功！");
			} else {
				return Message.error("配置失败!请刷新重试!");
			}
		}
	}
	
}
