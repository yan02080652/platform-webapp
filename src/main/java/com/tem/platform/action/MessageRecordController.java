package com.tem.platform.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.utils.LogUtils;
import com.tem.notify.api.MessageRecordService;
import com.tem.notify.api.SmsService;
import com.tem.notify.dto.MessageRecordCondition;
import com.tem.notify.dto.MessageRecordDto;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * @author jieming
 *
 */
@Controller
@RequestMapping("messageRecord")
public class MessageRecordController extends BaseController {

	private final Logger logger = LoggerFactory.getLogger(MessageRecordController.class);

	@Autowired
	private MessageRecordService messageRecordService;

	@Autowired
	private SmsService smsService;
	
	@RequestMapping(value ="index")
	public String index(Model model,Integer pageIndex,MessageRecordCondition condition) {
		if(pageIndex == null){
			pageIndex = 1;
		}

		String tmcName = super.getStringValue("bpTmcName");

		String bpName = super.getStringValue("bpName");


		if (!super.isSuperAdmin()){
			condition.setTmcId(super.getTmcId());
		}

		QueryDto<MessageRecordCondition> queryDto = new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);
		if(StringUtils.isEmpty(condition.getStatus())){
			condition.setStatus(null);
		}
		if(StringUtils.isEmpty(condition.getContent())){
			condition.setContent(null);
		}
		if(StringUtils.isEmpty(condition.getMobile())){
			condition.setMobile(null);
		}
		queryDto.setCondition(condition);
		Page<MessageRecordDto> pageList = messageRecordService.queryListWithPage(queryDto);

		model.addAttribute("pageList",pageList );
		model.addAttribute("condition", condition);

		model.addAttribute("tmcId", condition.getTmcId());
		model.addAttribute("tmcName", tmcName);
		model.addAttribute("partnerId", condition.getPartnerId());
		model.addAttribute("bpName", bpName);

		return "platform/messageRecord/index.base";
	}

	@ResponseBody
	@RequestMapping("smsSend")
	public boolean smsSend(Long id){
		MessageRecordDto record = messageRecordService.getById(id);
		if(record == null){
			return false;
		}

		LogUtils.debug(logger, "再次发送短信，对象为:{}",record);

		boolean flag = smsService.sendSms(record.getMobile(), record.getTitle() + "-再次发送", record.getContent(), record.getPartnerId());
		return flag;
	}


	@RequestMapping("smsIndex")
	public String smsIndex(){
		return "platform/messageRecord/smsIndex.base";
	}

	//直接发送短信
	@ResponseBody
	@RequestMapping("smsContent")
	public boolean smsContent(String mobile,String title,String content, Long partnerId){

		return smsService.sendSms(mobile, title, content,partnerId);
	}
	
}
