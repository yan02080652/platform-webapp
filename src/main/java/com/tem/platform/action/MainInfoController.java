package com.tem.platform.action;

import com.iplatform.common.Config;
import com.iplatform.common.web.Message;
import com.tem.platform.api.*;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.security.authorize.ContextHolder;
import com.tem.platform.security.authorize.Menu;
import com.tem.platform.security.authorize.UserRights;
import com.tem.platform.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;


@Controller
@RequestMapping("platform/mainInfo")
public class MainInfoController extends BaseController {

	@Autowired
	private MenuService menuService;
	
	@Autowired
	private TransService transService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private PartnerCsService partnerCsService;

	private final static String SERVICE = "SERVICE";

	private final static String OPERATION = "OPERATION";

	@ResponseBody
	@RequestMapping("getPartnerName")
	public Map<String,Object> getPartnerName(){
		PartnerDto partnerDto = partnerService.findById(getCurrentUser().getPartnerId());
		Map<String, Object> map = new HashMap<String, Object>();

		String partnerName = partnerDto.getName();
		String sceneId = this.getSceneId();
		if(SERVICE.equals(sceneId)){
			partnerName += "客服协作平台";
		}else if(OPERATION.equals(sceneId)){
			partnerName += "运维平台";
		}

		map.put("partnerName", partnerName);
		return map;
	}
	
	@RequestMapping(value = "/getCurUserMenuInfo", method = RequestMethod.GET)
	public @ResponseBody Map<String,Object> getCurUserMenuInfo() {

		String sceneId = this.getSceneId();

		String sysId = Config.getString("SYS_ID");
		Map<String,Object> result = new HashMap<String,Object>();
		UserRights userRights = ContextHolder.getContext().getUserRights();
		List<Menu> menus = userRights.getMenus();
		List<Menu> destMenus = new ArrayList<Menu>();
		if(menus != null) {
			for(Menu m : menus) {
//				if(sysId.equals(m.getSysId())) {
//					destMenus.add(m);
//				}
				if(sceneId.equals(m.getSceneId())){
					destMenus.add(m);
				}
			}
		}
		
//		Map<String,Object> result = new HashMap<String,Object>();
//		UserRights userRights = ContextHolder.getContext().getUserRights();
//		List<Menu> menus = userRights.getMenus();
//		List<Menu> destMenus = menus;
		Collections.sort(destMenus, new Comparator<Menu>() {
		//	@Override
			public int compare(Menu o1, Menu o2) {
				Integer i1 = o1.getSeq();
				i1 = i1 != null ? i1 : 0;
				Integer i2 = o2.getSeq();
				i2 = i2 != null ? i2 : 0;
				return i1.compareTo(i2);
			}
		});
		result.put("menuList", destMenus);//放置当前用户所能看到的菜单
		
		return result;
	}

	/**
	 * 获取当前的场景
	 * @return
	 */
	private String getSceneId(){
		String sceneId = (String)super.getSession().getAttribute(Constants.SESSION_KEY_SCENE);
		if(sceneId == null) {
			Cookie[] cookies = getRequest().getCookies();
			if(cookies != null) {
				for(Cookie c : cookies) {
					String cname = c.getName();
					String cvalue = c.getValue();
					if(Constants.COOKIE_KEY_SCENE.equals(cname)) {
						sceneId = cvalue;
						super.getSession().setAttribute(Constants.SESSION_KEY_SCENE, sceneId);
					}
				}
			}
		}
		return sceneId;
	}
	
	@RequestMapping(value = "/getPartnerInfo", method = RequestMethod.GET)
	public @ResponseBody Map<String,Object> getPartnerInfo(HttpServletRequest request,HttpServletResponse response) {
		Map<String,Object> result = new HashMap<String,Object>();
		
//		List<PartnerDto> partnerDtoList = null;//当前用户所能看到的企业
//		int auth = PermissionUtil.checkTrans(Constants.IP_PARTNER_M, "ALL");
//		if(auth == 0){//说明有全部的权限
//			partnerDtoList = partnerService.getAll();
//			result.put("auth", "All");
//		}else{//查询该客户所能看到的企业
//			Map<String, String> fields = new HashMap<String, String>();
//			fields.put("TRANSID", Constants.IP_PARTNER_M);
//			Map<Long, Boolean> atuhedOrgMap = PermissionUtil.getAuthorizedOrgs("TRANS", fields);
//
//			partnerDtoList = new ArrayList<PartnerDto>();
//
//			for(Long orgId : atuhedOrgMap.keySet()) {
//				List<PartnerDto> list = partnerCsService.findByBpOrgId(orgId);
//				partnerDtoList.addAll(list);
//			}
//
//			result.put("auth", "M");
//			result.put("partnerDtoList", partnerDtoList);
//		}
//
		Long partnerId = super.getPartnerId();
		if(partnerId != null){
			PartnerDto partnerDto = partnerService.findById(partnerId);
			//往页面传值
			result.put("partnerId", partnerId);
			if(partnerDto != null){
				result.put("partnerName", partnerDto.getName());
			}
		}else{
			result.put("partnerName", "请选择企业");

//			if(partnerDtoList != null && partnerDtoList.size()>0){
//				super.session.setAttribute(Constants.SESSION_KEY_PARTNER,  partnerDtoList.get(0).getId().toString());
//
//				//往页面传值
//				result.put("partnerId", partnerDtoList.get(0).getId());
//				result.put("partnerName", partnerDtoList.get(0).getName());
//			}
		}
		
		return result;
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/changePartner")
	public Message changePartner(Model model, @RequestParam("partnerId") Long partnerId,HttpServletRequest request) {
		if (partnerId == null) {
			return Message.error("请先选择一个企业");
		}
		//重新设置cookie值
		super.getSession().setAttribute(Constants.SESSION_KEY_PARTNER,  partnerId.toString());
		
		return Message.success("切换成功");
	}
	
	
}
