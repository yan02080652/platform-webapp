package com.tem.platform.action;

import com.tem.platform.Constants;
import com.tem.platform.security.authorize.ContextUtil;
import com.tem.platform.security.authorize.PermissionUtil;
import com.tem.platform.security.authorize.User;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * controller-基类
 */
public class BaseController{
	private static String param_sysId = "sysId";
	
	/**
	 * 超级管理员的作业ID
	 */
	private static String SUPER_ADMIN_TRANSID = "IP_PARTNER_ADMIN";

	/**
	 * 只能管理本TMC的权限
	 */
	private final static String TMC_ORG_ME = "me";


	public static final long globalVersion = System.currentTimeMillis();

	private static ThreadLocal<HttpServletResponse> responseLocal = new ThreadLocal<HttpServletResponse>();
	
    @ModelAttribute  
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response, Model model){

		responseLocal.remove();
		responseLocal.set(response);

        //传递type值
		String type = getStringValue("type");
		model.addAttribute("type", type);
		putTmcId(model);
		putGlobalVersion(model);
    }

	public void putTmcId(Model model) {
    	if(this.getCurrentUser() != null){
			model.addAttribute("tmcId", this.getCurrentUser().getPartnerId());
		}
	}

	protected HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
	}

	protected HttpServletResponse getResponse() {
		return responseLocal.get();
	}

	protected HttpSession getSession() {
		return this.getRequest().getSession();
	}

	/**
     * 获取partnerId，如果请求参数中带有partnerId参数，则使用请求的partnerId，<br>
     * 否则使用session中的partnerId，session中的partnerId由页面切换而来
     * @return
     */
    protected Long getPartnerId(){
		Long partnerId = getLongValue(Constants.PARAM_KEY_PARTNER);
		if(partnerId == null && getSession().getAttribute(Constants.SESSION_KEY_PARTNER) != null){
			partnerId = Long.parseLong(getSession().getAttribute(Constants.SESSION_KEY_PARTNER).toString());
		}
		return partnerId;
    }

	/**
	 * 组织架构中用到的选择企业
	 * @return
	 */
	protected Long getPartnerIdForOrg(){
		String type = this.getStringValue("type");
		if(TMC_ORG_ME.equals(type)){
			return getCurrentPartnerId();
		}
		return getPartnerId();
	}

	/**
	 * 获取当前登录用户所属的partnerId
	 * @return
	 */
	protected Long getCurrentPartnerId(){
		return this.getCurrentUser().getPartnerId();
	}
	/**
	 * 获取当前登录用户的企业id(由于登录后台的用户一定是TMC用户，所以企业ID就是tmcId)
	 * @return
	 */
	protected Long getTmcId(){
		return this.getCurrentUser().getPartnerId();
	}
    
    /**
     * 获取当前系统Id,参数key=sysId
     * @return
     */
    protected String getSysId(){
    	String sysId = this.getStringValue(param_sysId);
    	if(StringUtils.isEmpty(sysId)){
    		sysId = Constants.PLATFORM;
    	}
    	return sysId;
    }
    
    protected User getCurrentUser() {
    	User user = ContextUtil.getCurrentUser();
		return user;
	}
    
    protected Long getLongValue(String name){
    	String value = this.getStringValue(name);
    	if(StringUtils.isEmpty(value) || "undefined".equals(value)){
    		return null;
    	}
    	return Long.valueOf(value);
    }
    
    protected Integer getIntegerValue(String name){
    	String value = this.getStringValue(name);
    	if(StringUtils.isEmpty(value)){
    		return null;
    	}
    	return Integer.valueOf(value);
    }
    
    protected String getStringValue(String name){
    	return getRequest().getParameter(name);
    }
    
    public void putGlobalVersion(Model model){
  		model.addAttribute("globalVersion", globalVersion);
  	}

    /**
     * 是否超级管理员
     * @return
     */
    public boolean isSuperAdmin() {
    	int rs = PermissionUtil.checkTrans(SUPER_ADMIN_TRANSID);
    	
    	return rs == 0;
    }

	protected void addResult(Model model,int code,String message) {
		model.addAttribute("code", code);
		model.addAttribute("message", message);
	}


}
