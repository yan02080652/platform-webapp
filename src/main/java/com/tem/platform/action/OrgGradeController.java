package com.tem.platform.action;

import com.iplatform.common.web.Message;
import com.tem.platform.api.OrgGradeService;
import com.tem.platform.api.dto.OrgGradeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("platform/orgGrade")
public class OrgGradeController extends BaseController {
	@Autowired
	private OrgGradeService orgGradeService;
	
	@RequestMapping("")
	public String partnerOrgGrade(){
		return "platform/orgGrade/orgGrade.base";
	}
	
	@ResponseBody
	@RequestMapping(value="/showMenu")
	public List<OrgGradeDto> showMenu(){
		List<OrgGradeDto> list = orgGradeService.findList(super.getPartnerId());
		return list;
	}
	
	@RequestMapping(value = "/detail")
	public String listDetail(Model model, String code,String pcode) {
		OrgGradeDto orgGradeDto = null;
		if(code!=null && !"".equals(code)){
			 orgGradeDto = orgGradeService.findByCode(super.getPartnerId(), code);
		}else{
			 orgGradeDto = new OrgGradeDto();
			 orgGradeDto.setPcode(pcode);
		}
		model.addAttribute("orgGradeDto", orgGradeDto);
		return "platform/orgGrade/orgGradeDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/checkCode")
	public String checkCode(String code,Long id) {
		if (code == null || "".equals(code)) {
			return "ok";
		}
		OrgGradeDto orgGradeDto = orgGradeService.findByCode(super.getPartnerId(), code);
		if (orgGradeDto == null) {
			return "ok";
		}else{
			if(orgGradeDto.getId().equals(id)){
				return "ok";
			}else{
				return "no";
			}
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrupdateOrg", method = RequestMethod.POST)
	public Message saveOrupdate(@ModelAttribute("orgGradeDto") @Validated OrgGradeDto orgGradeDto,
			Model model,BindingResult br) {
		if(br.hasErrors()){
			return Message.error("数据校验不通过。");
		}
		boolean rs = false;
		if (orgGradeDto.getPartnerId() == null) {
			orgGradeDto.setPartnerId(super.getPartnerId());
			if("".equals(orgGradeDto.getPcode())){//顶级节点
				orgGradeDto.setPcode(null);
			}
			rs = orgGradeService.add(orgGradeDto);
			if (rs) {
				return Message.success("新增成功");
			} else {
				return Message.error("新增失败");
			}
		} else {
			rs = orgGradeService.update(orgGradeDto);
			if (rs) {
				return Message.success("修改成功");
			} else {
				return Message.error("修改失败");
			}
		}
	}
	
	@RequestMapping("/getOrgGrade/{id}")
	public String getPartnerOrgGrade(Model model , @PathVariable("id") Long id){
		OrgGradeDto orgGradeDto = orgGradeService.findById(id);
		model.addAttribute("orgGradeDto",orgGradeDto);
		return "platform/orgGrade/orgGradeDetail.jsp";
	}
	
	

	@ResponseBody
	@RequestMapping("/deleteOrg")
	public Message detele( @RequestParam("id") Long id){
		boolean rs = orgGradeService.delete(id);
		if(rs){
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败");
		}
	}
}
