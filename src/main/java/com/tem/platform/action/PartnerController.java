package com.tem.platform.action;

import com.alibaba.fastjson.JSON;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.event.EventProxy;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.iplatform.common.web.Message;
import com.iplatform.common.web.Page;
import com.tem.notify.api.EventService;
import com.tem.platform.api.*;
import com.tem.platform.api.dto.*;
import com.tem.platform.Constants;
import com.tem.platform.form.PartnerTmcForm;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@RequestMapping("platform/partner")
public class PartnerController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(PartnerController.class);
	
	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private PartnerBpService partnerBpService;
	
	@Autowired
	private PartnerCsService partnerCsService;
	
	@Autowired
	private OrgService orgService;
	
//	@Autowired
//	private EventProxy eventProxy;
	
	@RequestMapping(value = "")
	public String partnerPage(Model model,Integer pageIndex,String partnerName) {
		if(pageIndex==null){
			pageIndex = 1;
		}

		Map<String,Object> params=new HashMap<String,Object>();
		params.put("start", (pageIndex-1)*10);
		params.put("pageSize", Page.DEFAULT_PAGE_SIZE);

		if(partnerName!=null && !"".equals(partnerName)){
		   String firstSpell = partnerService.getFirstSpell(partnerName);
		   params.put("name", partnerName);
		   params.put("code", firstSpell);
		}

		String requestType = super.getStringValue("requestType");


		if ("tmc".equals(requestType)) {
			model.addAttribute("requestType", "tmc");
			params.put("type",1);
		}else{
			params.put("tmcId",super.getTmcId());
			params.put("type",0);
		}

		List<PartnerDto> partnerList = partnerService.getPartnerList(params);
		Integer count = partnerService.getPartnerCount(params);
		com.iplatform.common.Page<PartnerDto> pageList=new com.iplatform.common.Page<PartnerDto>(partnerList,pageIndex,Page.DEFAULT_PAGE_SIZE,count);

		model.addAttribute("count",count);
		model.addAttribute("pageList", pageList);
		model.addAttribute("partnerName", partnerName);
		model.addAttribute("pageIndex", pageIndex);
		return "platform/partner/partnerList.base";
	}

	
	@ResponseBody
	@RequestMapping(value = "/delete/{id}")
	public Message delete(Model model, @PathVariable("id") Long id) {
		if (id == null) {
			return Message.error("不存在的企业客户");
		}
		partnerService.delete(id);
		return Message.success("操作成功");
	}
	
	@RequestMapping("/add")
	public String add(Model model) {
		PartnerDto partner = new PartnerDto();
		partner.setCreateDate(new Date());
		partner.setEditDate(new Date());
		model.addAttribute("partner", partner);

		String type = super.getStringValue("requestType");

		if ("tmc".equals(type)) {
			model.addAttribute("requestType", "tmc");
			return "platform/partner/partner.base";
		}else {
			return "platform/partner/normalPartner.base";
		}

	}
	
	@ResponseBody
	@RequestMapping("/getSpell")
	public Map<String, Object> getSpell(String name) {
		Map<String,Object> map = new HashMap<String,Object>();
		String spell = partnerService.getSpell(name);
		String[] split = spell.split(",");
		String firstSpell = partnerService.getFirstSpell(name);
		map.put("spell",split[0]);
		map.put("firstSpell",firstSpell);
		return map;
			
	}
	
	@RequestMapping("/getPartner")
	public String getPartner(Model model, @RequestParam(value="id",required=false)Long id) {
		PartnerDto partner = partnerService.findById(id);
		model.addAttribute("partner", partner);
		
		//修改的时候增加维护企业与tmc的关系
		model.addAttribute("operType", "edit");//标志是修改类型

		String type = super.getStringValue("requestType");
		if ("tmc".equals(type)) {
			model.addAttribute("requestType", "tmc");
			return "platform/partner/partner.base";
		}else {
			return "platform/partner/normalPartner.base";
		}
	}
	
	@ResponseBody
	@RequestMapping("/getPartnerTmc")
	public List<PartnerTmcForm> getPartnerTmc(@RequestParam("partnerId") Long partnerId){
		
		List<PartnerTmcForm> partnerTmcForms = new ArrayList<PartnerTmcForm>();
		
		List<OrgDto> orgs = orgService.findAllCompanyOrgs(partnerId);//得到此客户的所有公司单位
		//下级单位没有配置TMC则使用总公司上的tmc
		if(orgs != null && orgs.size() > 0){
			PartnerTmcForm headPartnerTmcForm = null;
			//得到总公司的TMC
			for(OrgDto orgDto : orgs){
				if("1.0".equals(orgDto.getOrgGrade())){
					List<PartnerBpDto> tempHeads = partnerBpService.findPartnerBpByPartnerOrgId(orgDto.getId());
					if(tempHeads != null && tempHeads.size() > 0){
						PartnerBpDto headPartnerBp = tempHeads.get(0);
						headPartnerTmcForm = new PartnerTmcForm();
						headPartnerTmcForm.setPartnerId(headPartnerBp.getPartnerId());
						headPartnerTmcForm.setId(headPartnerBp.getPartnerOrg().getId());
						headPartnerTmcForm.setPid(headPartnerBp.getPartnerOrg().getPid());
						headPartnerTmcForm.setBpId(headPartnerBp.getBp().getId());
						headPartnerTmcForm.setBpName(headPartnerBp.getBp().getName());
						headPartnerTmcForm.setOrgName(headPartnerBp.getPartnerOrg().getName());
						break;
					}else{
						headPartnerTmcForm =  new PartnerTmcForm();
						headPartnerTmcForm.setPartnerId(partnerId);
						headPartnerTmcForm.setId(orgDto.getId());
						headPartnerTmcForm.setPid(orgDto.getPid());
						headPartnerTmcForm.setOrgName(orgDto.getName());
					}
				}
			}
			partnerTmcForms.add(headPartnerTmcForm);
			
			//循环各个子公司
			for(OrgDto orgDto : orgs){
				if(!"1.0".equals(orgDto.getOrgGrade())){
					List<PartnerBpDto> tempTemps = partnerBpService.findPartnerBpByPartnerOrgId(orgDto.getId());
					PartnerTmcForm partnerTmcForm = null;
					if(tempTemps != null && tempTemps.size() > 0){
						PartnerBpDto partnerBpDto = tempTemps.get(0);
						partnerTmcForm = new PartnerTmcForm();
						partnerTmcForm.setPartnerId(partnerBpDto.getPartnerId());
						partnerTmcForm.setId(partnerBpDto.getPartnerOrg().getId());
						partnerTmcForm.setPid(partnerBpDto.getPartnerOrg().getPid());
						partnerTmcForm.setBpId(partnerBpDto.getBp().getId());
						partnerTmcForm.setBpName(partnerBpDto.getBp().getName());
						partnerTmcForm.setOrgName(partnerBpDto.getPartnerOrg().getName());
						break;
					}else{
						partnerTmcForm =  new PartnerTmcForm();
						partnerTmcForm.setPartnerId(partnerId);
						partnerTmcForm.setId(orgDto.getId());
						partnerTmcForm.setPid(orgDto.getPid());
						partnerTmcForm.setOrgName(orgDto.getName());
					}
					partnerTmcForms.add(partnerTmcForm);
				}else{
					continue;
				}
			}
		}
		
		return partnerTmcForms;
	}
	
	@ResponseBody
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ResponseInfo update(PartnerDto partnerDto) {
		if(StringUtils.isEmpty(partnerDto.getAlias())){
			partnerDto.setAlias(partnerDto.getName());
		}

		partnerDto.setTmcId(String.valueOf(super.getTmcId()));

		if (partnerDto.getId() == null) {
			String firstSpell = partnerService.getFirstSpell(partnerDto.getName());
			partnerDto.setCreateDate(new Date());
			//partnerDto.setEditDate(new Date());
			partnerDto.setCode(firstSpell);
			partnerDto.setCreateUser(super.getCurrentUser().getId());
			//partnerDto.setEditUser(super.getCurrentUser().getId());

			Map<String, Object> rsDate = partnerService.add(partnerDto);
			LogUtils.warn(logger, "创建人Id：{},创建时间{}", super.getCurrentUser().getId().toString(),DateUtils.format(new Date()));

			//TMC分配默认为自己
			savePartnerBpRelation2( (Long) rsDate.get("partnerId"),super.getTmcId());
			Map<String, String> map = new HashMap<>();
			map.put("partnerId", rsDate.get("partnerId").toString());
			String requestType = super.getStringValue("requestType");
			if ("tmc".equals(requestType)) {
				map.put("requestType", "tmc");
			}

			ResponseInfo rs = new ResponseInfo("success",map);

			return rs;
		} else {
			partnerDto.setEditUser(super.getCurrentUser().getId());
			partnerDto.setEditDate(new Date());
			boolean rs = false;
			String firstSpell = partnerService.getFirstSpell(partnerDto.getName());
			partnerDto.setCode(firstSpell);
			rs=partnerService.update(partnerDto);
			if (rs) {
				LogUtils.warn(logger, "修改人Id：{},修改时间{}", super.getCurrentUser().getId().toString(),DateUtils.format(new Date()));
				//默认TMC分配
				//savePartnerBpRelation2( partnerDto.getId(),super.getTmcId());

				Map<String, String> map = new HashMap<>();
				map.put("partnerId", partnerDto.getId().toString());
				String requestType = super.getStringValue("requestType");
				if ("tmc".equals(requestType)) {
					map.put("requestType", "tmc");
				}
				ResponseInfo rsi = new ResponseInfo("success",map);
				return rsi;
			} else {
				return new ResponseInfo("error","修改失败");
			}
		}
	}

	@ResponseBody
	@RequestMapping("/checkMailDomain")
	public String checkMailDomain(@RequestParam(value="mailDomain",required=false)String mailDomain,@RequestParam(value="id",required=false)Long id){
		List<DictCodeDto> list = dictService.getCodes(Constants.INVALIDE_MAIL_DOMAIN);
		for(DictCodeDto dict: list){
			if(dict.getItemTxt().equals(mailDomain)){
				return "no";
			}
		}
		if(id == null){//新增
			PartnerDto partnerDto = partnerService.getByMailDomain(mailDomain);
			if(partnerDto != null){
				return "no";
			}
		}else{
			PartnerDto partnerDto = partnerService.getByMailDomain(mailDomain);
			if(partnerDto != null){
				if(!mailDomain.equals(partnerDto.getMailDomain())){
					return "no";
				}
			}
		}
		return "ok";
	}
	
	@RequestMapping("/partnerBpRelation")
	public String partnerBpRelation(Model model,@RequestParam(value="start",required=false)Integer start,
			@RequestParam(value="pageSize",required=false)Integer pageSize,Long partnerId,Long bpId,String partnerName,String bpName) {
		if(start==null){
			   start = 0;
		   }
		   if(pageSize==null){
			   pageSize = Page.DEFAULT_PAGE_SIZE;  
		   }
		   
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("start", start);
		params.put("pageSize", pageSize);
		
		
	   params.put("partnerId", partnerId);
	   params.put("bpId", bpId);
	   
	   List<PartnerBpDto> list = partnerBpService.findList(params);
	   Long count = partnerBpService.findTotalCount(params);
	   
		Page<PartnerBpDto> pageList=new Page<PartnerBpDto>(start,pageSize,list,count.intValue());
		pageList.setPageUrl("platform/partner/partnerBpRelation?");
		model.addAttribute("count",count);
		model.addAttribute("pageList", pageList);
		model.addAttribute("partnerId",partnerId);
		model.addAttribute("partnerName",partnerName);
		model.addAttribute("bpId",bpId);
		model.addAttribute("bpName",bpName);
		return "platform/partner/partnerBpRelation.base";
	}
	
	@RequestMapping("/bpAddView")
	public String bpAddView(Model model){
		model.addAttribute("title","新增");
		return "platform/partner/bpRelationModel.base";
	}
	
	@RequestMapping("/bpEditView/{partnerOrgId}/{bpId}")
	public String bpEditView(Model model,@PathVariable("partnerOrgId")Long partnerOrgId,@PathVariable("bpId")Long bpId){
		PartnerBpDto partnerBp = partnerBpService.getPartnerBp(partnerOrgId, bpId);
		model.addAttribute("partnerBp",partnerBp);
		model.addAttribute("title","修改");
		return "platform/partner/bpRelationModel.base";
	}
	
	
	@ResponseBody
	@RequestMapping("/addBpRelation")
	public Message addBpRelation(Long partnerOrgId,Long bpId,Long partnerId){
		PartnerBpDto partnerBpDto = new PartnerBpDto();
		partnerBpDto.setPartnerOrgId(partnerOrgId);
		partnerBpDto.setBpId(bpId);
		partnerBpDto.setPartnerId(partnerId);
		
		boolean flag = partnerBpService.isRepeat(partnerOrgId,bpId);
		if(flag){
			partnerBpService.insert(partnerBpDto);
			return Message.success("新增成功.");
		}else{
			return Message.error("新增失败(原因：该对应关系已存在)");
		}
	}
	
	@ResponseBody
	@RequestMapping("/editBpRelation")
	public Message editBpRelation(Long partnerId,Long partnerOrgId,Long bpId,Long oldPartnerOrgId,Long oldBpId){
		//修改后的bp
		PartnerBpDto partnerBpDto = new PartnerBpDto();
		partnerBpDto.setPartnerOrgId(partnerOrgId);
		partnerBpDto.setBpId(bpId);
		partnerBpDto.setPartnerId(partnerId);
		//修改前的bp
		PartnerBpDto oldPartnerBp = new PartnerBpDto();
		oldPartnerBp.setBpId(oldBpId);
		oldPartnerBp.setPartnerId(partnerId);
		oldPartnerBp.setPartnerOrgId(oldPartnerOrgId);
		
		
		boolean flag = partnerBpService.isRepeat(partnerOrgId,bpId);
		if(flag){
			
			//删除Bp以及相应的cs
			partnerBpService.deleteBPRelation(partnerId, oldPartnerOrgId, oldBpId);
			//新插入一条记录
			partnerBpService.insert(partnerBpDto);
			
			return Message.success("修改成功");
		}else{
			//判断是否为自己
			if(oldBpId.equals(bpId) && oldPartnerOrgId.equals(partnerOrgId) ){//没有改变
				return Message.success("修改成功");
			}else{
				return Message.error("修改失败(原因：该对应关系已存在)");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping("/deleteBpRelation")
	public Message deleteBpRelation(Long partnerId,Long partnerOrgId,Long bpId){
		
		boolean flag = partnerBpService.deleteBPRelation(partnerId, partnerOrgId, bpId);
		
		if(flag){
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败");
		}
		
	}
	
	
	@RequestMapping("/partnerCsRelation")
	public String partnerCsRelation(Model model,@RequestParam(value="start",required=false)Integer start,
			@RequestParam(value="pageSize",required=false)Integer pageSize,Long partnerId,Long bpId,String partnerName,String bpName) {
		if(start==null){
			   start = 0;
		   }
		   if(pageSize==null){
			   pageSize = Page.DEFAULT_PAGE_SIZE;  
		   }
		   
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("start", start);
		params.put("pageSize", pageSize);
		
		
	   params.put("partnerId", partnerId);
	   params.put("bpId", bpId);
	   
	   List<PartnerCsDto> list = partnerCsService.findList(params);
	   Long count = partnerCsService.findTotalCount(params);
	   
		Page<PartnerCsDto> pageList=new Page<PartnerCsDto>(start,pageSize,list,count.intValue());
		pageList.setPageUrl("platform/partner/partnerCsRelation?");
		model.addAttribute("count",count);
		model.addAttribute("pageList", pageList);
		model.addAttribute("partnerId",partnerId);
		model.addAttribute("partnerName",partnerName);
		model.addAttribute("bpId",bpId);
		model.addAttribute("bpName",bpName);
		return "platform/partner/partnerCsRelation.base";
	}
	
	
	@ResponseBody
	@RequestMapping("/deleteCsRelation")
	public Message deleteCsRelation(Long partnerOrgId,Long bpId,Long bpOrgId){
		int flag = partnerCsService.delete( partnerOrgId, bpId, bpOrgId);
		if(flag > 0 ){
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败");
		}
	}
	
	 //根据客户和客户组织单元查询bp
	@ResponseBody
	@RequestMapping("/loadBpList")
	public List<PartnerDto> loadBpList(Long partnerId,Long partnerOrgId){
		List<PartnerDto> list = partnerBpService.findBpList(partnerId, partnerOrgId);
		return list;
	}
	
	//根据cliengid获取对于Org根节点
	@ResponseBody
	@RequestMapping("/getOrgDto")
	public OrgDto getOrgDto(Long partnerId){
		OrgDto orgDto = orgService.findOrgs(partnerId, null).get(0);
		return orgDto;
	}
	
	
	@RequestMapping("/csAddView")
	public String csAddView(Model model){
		model.addAttribute("title","新增");
		return "platform/partner/csRelationModel.base";
	}
	
	
	@RequestMapping("/csEditView/{partnerOrgId}/{bpId}/{bpOrgId}")
	public String csEditView(Model model,@PathVariable("partnerOrgId")Long partnerOrgId,@PathVariable("bpId")Long bpId,@PathVariable("bpOrgId")Long bpOrgId){
		PartnerCsDto partnerCs = partnerCsService.getPartnerCs(partnerOrgId, bpId,bpOrgId);
		model.addAttribute("partnerCs",partnerCs);
		model.addAttribute("title","修改");
		return "platform/partner/csRelationModel.base";
	}
	
	@ResponseBody
	@RequestMapping("/addCsRelation")
	public Message addCsRelation(Long partnerOrgId,Long bpId,Long partnerId,Long bpOrgId){
		PartnerCsDto partnerCsDto = new PartnerCsDto();
		partnerCsDto.setPartnerOrgId(partnerOrgId);
		partnerCsDto.setBpId(bpId);
		partnerCsDto.setPartnerId(partnerId);
		partnerCsDto.setBpOrgId(bpOrgId);
		
		boolean flag = partnerCsService.isRepeat(partnerOrgId,bpId,bpOrgId);
		if(flag){
			partnerCsService.insert(partnerCsDto);
			return Message.success("新增成功.");
		}else{
			return Message.error("新增失败(原因：该对应关系已存在)");
		}
	}
	
	@ResponseBody
	@RequestMapping("/editCsRelation")
	public Message editCsRelation(Long partnerId,Long partnerOrgId,Long bpId,Long bpOrgId,Long oldPartnerOrgId,Long oldBpId,Long oldBpOrgId){
		//修改后的bp
		PartnerCsDto partnerCsDto = new PartnerCsDto();
		partnerCsDto.setPartnerOrgId(partnerOrgId);
		partnerCsDto.setBpId(bpId);
		partnerCsDto.setPartnerId(partnerId);
		partnerCsDto.setBpOrgId(bpOrgId);
		
		//修改前的bp
		PartnerCsDto oldPartnerCs = new PartnerCsDto();
		oldPartnerCs.setPartnerId(partnerId);
		oldPartnerCs.setPartnerOrgId(oldPartnerOrgId);
		oldPartnerCs.setBpId(oldBpId);
		oldPartnerCs.setBpOrgId(oldBpOrgId);
	
		boolean flag = partnerCsService.isRepeat(partnerOrgId,bpId,bpOrgId);
		if(flag){
			//删除原纪录，插入新纪录
			partnerCsService.delete(oldPartnerOrgId,oldBpId,oldBpOrgId);
			partnerCsService.insert(partnerCsDto);
			
			return Message.success("修改成功");
		}else{
			//判断是否为自己
			if(oldBpId.equals(bpId) && oldPartnerOrgId.equals(partnerOrgId) && oldBpOrgId.equals(bpOrgId)){//没有改变
				return Message.success("修改成功");
			}else{
				return Message.error("修改失败(原因：该对应关系已存在)");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/savePartnerBpRelation")
	public Message savePartnerBpRelation(@RequestParam(value="id")Long id,@RequestParam(value="bpId")Long bpId) {
		
		Long origTmcId = null;
		OrgDto org = orgService.getOrg(id);
		List<PartnerBpDto> partnerBpDtos = partnerBpService.findPartnerBpByPartnerOrgId(id);
		if(partnerBpDtos != null && partnerBpDtos.size() > 0){
			for(PartnerBpDto partnerBpDto : partnerBpDtos){
				origTmcId = partnerBpDto.getBpId();
				
				partnerCsService.deleteByPartnerOrgId(partnerBpDto.getPartnerOrgId());				
				partnerBpService.deleteBPRelation(partnerBpDto.getPartnerId(), partnerBpDto.getPartnerOrgId(), partnerBpDto.getBpId());
			}
		}
		PartnerBpDto partnerBp = new PartnerBpDto();
		partnerBp.setPartnerId(org.getPartnerId());
		partnerBp.setPartnerOrgId(id);
		partnerBp.setBpId(bpId);
		partnerBpService.insert(partnerBp);
		
		//在客户客服组织关系表中 默认插入一条信息
		PartnerCsDto partnerCs = new PartnerCsDto();
		partnerCs.setPartnerId(org.getPartnerId());
		partnerCs.setPartnerOrgId(id);
		partnerCs.setBpId(bpId);
		//找到这个bp的根组织
		List<OrgDto> orgs = orgService.findAllCompanyOrgs(bpId);//得到此bp的所有公司单位
		if(orgs != null && orgs.size() > 0){
			for(OrgDto orgDto : orgs){
				if("1.0".equals(orgDto.getOrgGrade())){
					partnerCs.setBpOrgId(orgDto.getId());
				}
			}
		}
		partnerCsService.insert(partnerCs);
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("partnerId", org.getPartnerId());
		data.put("origTmcId", origTmcId);
		data.put("newTmcId", bpId);
		String json = JSON.toJSONString(data);
//		eventProxy.pushEventImmediate("PLATFORM_CLIENT_TMC_CHANGE", json);
		
		return Message.success("配置成功！");
	}

	private Message savePartnerBpRelation2(Long partnerId,Long bpId){
		List<OrgDto> os = orgService.findOrgs(partnerId, null);
		Long orgId = null;
		if(os.size() > 0){
			orgId = os.get(0).getId();
		}
		PartnerBpDto partnerBp = new PartnerBpDto();
		partnerBp.setPartnerId(partnerId);
		partnerBp.setPartnerOrgId(orgId);
		partnerBp.setBpId(bpId);
		partnerBpService.insert(partnerBp);

		//在客户客服组织关系表中 默认插入一条信息
		PartnerCsDto partnerCs = new PartnerCsDto();
		partnerCs.setPartnerId(partnerId);
		partnerCs.setPartnerOrgId(orgId);
		partnerCs.setBpId(bpId);
		//找到这个bp的根组织
		List<OrgDto> orgs = orgService.findAllCompanyOrgs(bpId);//得到此bp的所有公司单位
		if(orgs != null && orgs.size() > 0){
			for(OrgDto orgDto : orgs){
				if("1.0".equals(orgDto.getOrgGrade())){
					partnerCs.setBpOrgId(orgDto.getId());
				}
			}
		}
		partnerCsService.insert(partnerCs);
		return Message.success("配置成功！");
	}
	
	@ResponseBody
	@RequestMapping(value = "/deletePartnerBpRelation")
	public Message deletePartnerBpRelation(@RequestParam(value="id")Long id) {
		List<PartnerBpDto> partnerBpDtos = partnerBpService.findPartnerBpByPartnerOrgId(id);
		if(partnerBpDtos != null && partnerBpDtos.size() > 0){
			for(PartnerBpDto partnerBpDto : partnerBpDtos){
				partnerCsService.deleteByPartnerOrgId(partnerBpDto.getPartnerOrgId());
				
				partnerBpService.deleteBPRelation(partnerBpDto.getPartnerId(), partnerBpDto.getPartnerOrgId(), partnerBpDto.getBpId());
			}
		}
		
		return Message.success("删除成功！");
	}
}
