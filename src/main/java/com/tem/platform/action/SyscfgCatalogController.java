package com.tem.platform.action;

import com.iplatform.common.web.Message;
import com.tem.platform.api.*;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.SyscfgCatalogDto;
import com.tem.platform.api.dto.SyscfgCfgItemDto;
import com.tem.platform.api.dto.SyscfgDictDataDto;
import com.tem.platform.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("platform/syscfgCatalog")
public class SyscfgCatalogController extends BaseController {

	@Autowired
	private SyscfgCatalogService syscfgCatalogService;
	
	@Autowired
	private SyscfgCfgItemService syscfgCfgitemService;
	
	@Autowired
	private SyscfgDictDataService syscfgDictdataService;
	
	@Autowired
	private SyscfgItemDataService syscfgItemdataService;
	
	@Autowired
	private DictService dictService;
	

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String page(Model model,HttpServletRequest req) {
		List<DictCodeDto> dictCodeDtoList = new ArrayList<DictCodeDto>();
		DictCodeDto dicCoodeDto = new DictCodeDto();
		dicCoodeDto.setItemCode(Constants.COMMON);
		dicCoodeDto.setItemTxt("公共系统");
		dictCodeDtoList.add(dicCoodeDto);
		dictCodeDtoList.addAll(dictService.getCodes(Constants.IP_SYS_LIST));
		model.addAttribute("sysId", super.getSysId());
		model.addAttribute("dictCodeDtoList", dictCodeDtoList);
		return "platform/syscfgCatalog/syscfgcatalog.base";
	}
	
	@ResponseBody
	@RequestMapping("/getCfgTree")
	public List<SyscfgCatalogDto> getCfgTree(String sysId){
		List<SyscfgCatalogDto> list = syscfgCatalogService.getAllBySysId(sysId);
		return list;
	}
	
	@RequestMapping(value = "/selectSysCfgTree", method = RequestMethod.GET)
	public String selectSysCfgTree(Model model) {
		return "platform/syscfgCatalog/selectSysCfgTree.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/moveSysCfg")
	public Message moveSysCfg(Model model, @RequestParam("fromId") Long fromId, @RequestParam("toId") Long toId) {
		SyscfgCatalogDto syscfgCatalogDto = syscfgCatalogService.selectById(fromId);
		if(syscfgCatalogDto != null){
			syscfgCatalogDto.setPid(toId);
			if(syscfgCatalogService.update(syscfgCatalogDto)>0) {
                return Message.success("移动成功");
            }
			return Message.error("移动失败");
			
		}else{
			return Message.error("移动失败");
		}
	}
	
	@RequestMapping("/getCfgDetail/{id}/{pid}")
	public String getCfgDetail(Model model,@PathVariable("id") Long id,@PathVariable("pid") Long pid){
		SyscfgCatalogDto catalogDto = null;
		if(id!=null && id != 0){
			//查询节点信息
			 catalogDto = syscfgCatalogService.selectById(id);
		}else{
			//新增节点
			catalogDto = new SyscfgCatalogDto();
			catalogDto.setPid(pid==0?null:pid);
		}
		model.addAttribute("catalogDto",catalogDto);
		return "platform/syscfgCatalog/syscfgCatalogDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/checkCode/{sysId}")
	public String checkCode(String nodeCode, @PathVariable("sysId")String sysId,Long id) {
		if (nodeCode == null || "".equals(nodeCode)) {
			return "ok";
		}
		SyscfgCatalogDto syscfgCatalogDto = syscfgCatalogService.getByNodeCode(nodeCode);
		if (syscfgCatalogDto == null) {
			return "ok";
		}else if(id!=null){
			if(syscfgCatalogDto.getId().equals(id)){
				return "ok";
			}
		}else{
			return "no";
		}
		return "no";
	}
	
	
	@ResponseBody
	@RequestMapping("/saveOrupdate/{sysId}/{nodeType}")
	public Message saveOrupdate(SyscfgCatalogDto catalogDto,@PathVariable("sysId")String sysId,
			@PathVariable("nodeType")Long nodeType){
		boolean rs = false;
		if(catalogDto.getId()==null){
			catalogDto.setSysId(sysId);
			rs = syscfgCatalogService.insert(catalogDto) > 0;
			if(rs){
				SyscfgCatalogDto catalog = syscfgCatalogService.getByNodeCode(catalogDto.getNodeCode());
				if(catalog != null){
					return Message.success(catalog.getId().toString());
				}
				return Message.success("新增成功");
			}else{
				return Message.error("新增失败");
			}
		}else{
			rs = syscfgCatalogService.updateSelective(catalogDto) > 0;
			if(rs){
				return Message.success("修改成功");
			}else{
				return Message.error("修改失败");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping("/deleteNode")
	public Message deleteNode(Long id){
		boolean rs = false;
		if(id!=null){
			rs = syscfgCatalogService.deleteById(id) > 0;
		}
		if(rs){
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败，请刷新重试");
		}
	}
	
	
	@RequestMapping("/getCfgDetailByCode/{sysId}/{nodeCode}")
	public String getCfgDetailByCode(Model model,@PathVariable("sysId") String sysId,@PathVariable("nodeCode") String nodeCode){
		SyscfgCatalogDto catalogDto = syscfgCatalogService.getByNodeCode(nodeCode);
		model.addAttribute("catalogDto",catalogDto);
		return "platform/syscfgCatalog/syscfgCatalogDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/getCfgItemTreeTable/{sysId}/{cfgnodeCode}")
	public List<SyscfgCfgItemDto> getCfgItemTreeTable(@PathVariable("cfgnodeCode")String cfgnodeCode,@PathVariable("sysId")String sysId){
		List<SyscfgCfgItemDto> list = syscfgCfgitemService.getAllByCfgnodeCode(cfgnodeCode);
		return list;
	}
	
	
	@RequestMapping("/addItemNode/{pid}")
	public String addItemNode(Model model,@PathVariable("pid") Long pid) {
		SyscfgCfgItemDto syscfgCfgitemDto = new SyscfgCfgItemDto();
		if(pid == -1){
			syscfgCfgitemDto.setPid(null);
		}else{
			syscfgCfgitemDto.setPid(pid);
		}
		model.addAttribute("syscfgCfgitemDto", syscfgCfgitemDto);
		model.addAttribute("typeTitle", "新增配置项");
		return "platform/syscfgCatalog/sysCfgItemDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/checkSysItemCode")
	public String checkSysItemCode(String itemCode, String sysId,Long id) {
		if (itemCode == null || "".equals(itemCode)) {
			return "ok";
		}
		SyscfgCfgItemDto syscfgCfgitemDto = syscfgCfgitemService.getByItemCode(itemCode);
		if (syscfgCfgitemDto == null) {
			return "ok";
		}else if(id!=null){
			if(syscfgCfgitemDto.getId().equals(id)){
				return "ok";
			}
		}else{
			return "no";
		}
		return "no";
	}
	
	@ResponseBody
	@RequestMapping("/saveOrupdateSysItem/{sysId}/{cfgnodeCode}")
	public Message saveOrupdateSysItem(SyscfgCfgItemDto syscfgCfgitemDto,@PathVariable("sysId")String sysId,
			@PathVariable("cfgnodeCode")String cfgnodeCode){
		boolean rs = false;
		if(syscfgCfgitemDto.getId()==null){
			syscfgCfgitemDto.setCfgnodeCode(cfgnodeCode);
			rs = syscfgCfgitemService.insert(syscfgCfgitemDto) > 0; 
			if(rs){
				return Message.success("新增成功");
			}else{
				return Message.error("新增失败");
			}
		}else{
			rs = syscfgCfgitemService.updateSelective(syscfgCfgitemDto) > 0;
			if(rs){
				return Message.success("修改成功");
			}else{
				return Message.error("修改失败");
			}
		}
	}
	
	@RequestMapping("/getItemNode/{id}")
	public String getItemNode(Model model,@PathVariable("id") Long id){
		SyscfgCfgItemDto syscfgCfgitemDto = syscfgCfgitemService.selectById(id);
		model.addAttribute("syscfgCfgitemDto",syscfgCfgitemDto);
		return "platform/syscfgCatalog/sysCfgItemDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/deleteSysItem")
	public Message deleteSysItem(Long id){
		boolean rs = false;
		if(id!=null){
			rs = syscfgCfgitemService.deleteById(id) > 0;
		}
		if(rs){
			return Message.success("删除配置项成功");
		}else{
			return Message.error("删除配置项失败，请刷新重试");
		}
	}
	
	@ResponseBody
	@RequestMapping("/hasChild")
	public String hasChild(Long id){
		List<SyscfgCfgItemDto> list = syscfgCfgitemService.getAllByPid(id);
		if(list.size()>0){//有子节点
			return "no";
		}else{//无子节点
			return "ok";
		}
	}
	
	@ResponseBody
	@RequestMapping("/checkDictCode/{sysId}")
	public String checkDictCode(String refDict, @PathVariable("sysId")String sysId,Long id) {
		if (refDict == null || "".equals(refDict)) {
			return "ok";
		}
		SyscfgCatalogDto syscfgCatalogDto = syscfgCatalogService.getByRefDict(refDict);
		if (syscfgCatalogDto == null) {
			return "ok";
		}else if(id!=null){
			if(syscfgCatalogDto.getId().equals(id)){
				return "ok";
			}
		}else{
			return "no";
		}
		return "no";
	}
	
	@ResponseBody
	@RequestMapping("/checkDictData/{sysId}/{dictCode}")
	public boolean  checkDictData(@PathVariable("dictCode")String dictCode,@PathVariable("sysId")String sysId){
		List<SyscfgDictDataDto> list = syscfgDictdataService.getByDictCode(dictCode);
		if(list.size()>0){
			return true;
		}else{
			return false;
		}
		
	}
	
}
