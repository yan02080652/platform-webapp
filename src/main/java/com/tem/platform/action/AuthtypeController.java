package com.tem.platform.action;

import com.iplatform.common.web.Message;
import com.tem.platform.api.AuthtypeFieldService;
import com.tem.platform.api.AuthtypeService;
import com.tem.platform.api.dto.AuthtypeDto;
import com.tem.platform.api.dto.AuthtypeFieldDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("platform/authconfig")
public class AuthtypeController extends BaseController {

	@Autowired
	private AuthtypeService authtypeService;

	@Autowired
	private AuthtypeFieldService authtypeFieldService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String page(Model model) {
		return "platform/authconfig/authtype.base";
	}
	
	@RequestMapping(value = "/authtype.do", method = RequestMethod.GET)
	public @ResponseBody
	List<AuthtypeDto> getTree() {
		List<AuthtypeDto> authtypeList = authtypeService.getAll();
		return authtypeList;
	}
	
	@RequestMapping("/addAuthNode/{pid}")
	public String addAuthNode(Model model, @PathVariable("pid") Long pid) {
		model.addAttribute("typeTitle", "新增权限类型");
		if(pid != null && pid <= 0){
			pid = null;
		}
		model.addAttribute("pid", pid);
		AuthtypeDto authtypeDto = new AuthtypeDto();
		model.addAttribute("authtypeDto", authtypeDto);
		return "platform/authconfig/authTreeNode.jsp";
	}
	
	@RequestMapping("/updateAuthNode/{id}")
	public String updateAuthNode(Model model, @PathVariable("id") Long id) {
		model.addAttribute("typeTitle", "修改权限类型");
		AuthtypeDto authtypeDto = authtypeService.selectById(id);
		model.addAttribute("pid", authtypeDto.getPid());
		model.addAttribute("authtypeDto", authtypeDto);
		return "platform/authconfig/authTreeNode.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkNodeCode")
	public Message checkNodeCode(Model model, @RequestParam("code") String code,@RequestParam("id") Long id) {
		if(id != null){//修改下 code不能修改  id存在说明是修改状态下
			return Message.success("yes");
		}else {
			AuthtypeDto authtypeDto = authtypeService.selectByCode(code);
			if (authtypeDto == null) {
				return Message.success("yes");
			}else{
				return Message.error("no");
			}
		}
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteAuthNode")
	public Message deleteAuthNode(Model model, @RequestParam("id") Long id) {
		if (id == null) {
			return Message.error("请先选择一个节点");
		}
		//删除判断子项
		AuthtypeDto authtypeDto = authtypeService.selectById(id);
		if(authtypeDto.getNodeType() == 1){//目录文件 则查看下面是否还有子项
			List<AuthtypeDto> rs = authtypeService.selectByPid(authtypeDto.getId());
			if(rs != null && rs.size()>0){
				return Message.error("请先删除子节点");
			}else{
				if(authtypeService.deleteById(id)>0) {
                    return Message.success("删除成功");
                }
				return Message.success("删除失败");
			}
		}else{
			//查看是否有子项
			List<AuthtypeFieldDto> rs = authtypeFieldService.selectByAuthtypeCode(authtypeDto.getCode());
			if(rs != null && rs.size()>0){
				return Message.error("请先删除右侧字段值");
			}else{
				if(authtypeService.deleteById(id)>0) {
                    return Message.success("删除成功");
                }
				return Message.success("删除失败");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrupdateAuth", method = RequestMethod.POST)
	public Message saveOrupdateAuth(@ModelAttribute("authtypeDto") @Validated AuthtypeDto authtypeDto,
			Model model,BindingResult br) {
		if(br.hasErrors()){
			return Message.error("数据校验不通过。");
		}
		
		int rs = 0;
		if (authtypeDto.getId() == null) {
			rs = authtypeService.insertSelective(authtypeDto);
			if (rs > 0) {
				return Message.success("新增权限类型成功。");
			} else {
				return Message.error("新增权限类型失败。");
			}
		} else {
			rs = authtypeService.update(authtypeDto);
			if (rs > 0) {
				return Message.success("修改权限类型成功。");
			} else {
				return Message.error("修改权限类型失败。");
			}
		}
	}
	
	@RequestMapping(value = "/listDetail/{code}", method = RequestMethod.GET)
	public String listDetail(Model model, @PathVariable("code") String code) {
		if (StringUtils.isNotEmpty(code)) {
			AuthtypeDto authtypeDto = authtypeService.selectByCode(code);
			model.addAttribute("authtypeDto", authtypeDto);
			
			List<AuthtypeFieldDto> authtypeFieldDtoList = authtypeFieldService.selectByAuthtypeCode(code);
			model.addAttribute("authtypeFieldDtoList", authtypeFieldDtoList);
		}
		return "platform/authconfig/authtypeField.jsp";
	}
	
	@RequestMapping("/addAuthField/{authtypeCode}")
	public String addAuthField(Model model, @PathVariable("authtypeCode") String authtypeCode) {
		model.addAttribute("typeTitle", "新增权限字段");
		AuthtypeDto authtypeDto = authtypeService.selectByCode(authtypeCode);
		model.addAttribute("authtypeDto", authtypeDto);
		AuthtypeFieldDto authtypeFieldDto = new AuthtypeFieldDto();
		authtypeFieldDto.setAuthtypeCode(authtypeCode);
		model.addAttribute("authtypeFieldDto", authtypeFieldDto);
		return "platform/authconfig/authtypeFieldDetail.jsp";
	}
	
	@RequestMapping("/updateAuthField/{id}")
	public String updateAuthField(Model model, @PathVariable("id") Long id) {
		model.addAttribute("typeTitle", "修改权限字段");
		AuthtypeFieldDto authtypeFieldDto = authtypeFieldService.selectById(id);
		if(authtypeFieldDto != null){
			AuthtypeDto authtypeDto = authtypeService.selectByCode(authtypeFieldDto.getAuthtypeCode());
			model.addAttribute("authtypeDto", authtypeDto);
		}
		model.addAttribute("authtypeFieldDto", authtypeFieldDto);
		return "platform/authconfig/authtypeFieldDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrupdateAuthField", method = RequestMethod.POST)
	public Message saveOrupdateAuthField(@ModelAttribute("authtypeFieldDto") @Validated AuthtypeFieldDto authtypeFieldDto,
			Model model,BindingResult br) {
		if(br.hasErrors()){
			return Message.error("数据校验不通过。");
		}
		
		int rs = 0;
		if (authtypeFieldDto.getId() == null) {
			rs = authtypeFieldService.insertSelective(authtypeFieldDto);
			if (rs > 0) {
				return Message.success("新增权限字段成功。");
			} else {
				return Message.error("新增权限字段失败。");
			}
		} else {
			rs = authtypeFieldService.update(authtypeFieldDto);
			if (rs > 0) {
				return Message.success("修改权限字段成功。");
			} else {
				return Message.error("修改权限字段失败。");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteAuthField/{id}")
	public Message deleteAuthField(Model model, @PathVariable("id") Long id) {
		if (id == null) {
			return Message.error("请先选择一条数据");
		}
		if(authtypeFieldService.deleteById(id)>0) {
            return Message.success("删除成功");
        }
		return Message.error("删除失败");
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkFieldCode")
	public Message checkFieldCode(Model model, @RequestParam("code") String code,@RequestParam("id") Long id,@RequestParam("authtypeCode") String authtypeCode) {
		if(id != null){//修改下 code不能修改  id存在说明是修改状态下
			return Message.success("yes");
		}else {
			//查询在权限对象组下是否有重复的
			AuthtypeFieldDto authtypeFieldDto = authtypeFieldService.selectByAuthtypeCodeAndCode(authtypeCode, code);
			if (authtypeFieldDto == null) {
				return Message.success("yes");
			}else{
				return Message.error("no");
			}
		}
		
	}

}
