package com.tem.platform.action;

import com.iplatform.common.web.Message;
import com.tem.platform.api.*;
import com.tem.platform.api.dto.*;
import com.tem.platform.security.authorize.PermissionUtil;
import com.tem.platform.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * @author chenjieming
 */
@Controller
@RequestMapping("platform/org")
public class OrgController extends BaseController {

    @Autowired
    private OrgService orgService;

    @Autowired
    private OrgGradeService orgGradeService;

    @Autowired
    private DictService dictService;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private PartnerCsService partnerCsService;

    /**
     * type : me 只能看到自己
     * type : tmc 只能看到自己管理的tmc
     * type : partner 只能看到自己管理的企业
     *
     * @param model
     * @return
     */
    @RequestMapping()
    public String list(Model model) {

        model.addAttribute("partnerId", super.getPartnerIdForOrg());
        return "platform/org/index.base";
    }

    @ResponseBody
    @RequestMapping(value = "orgList", method = RequestMethod.GET)
    public List<OrgDto> orgList() {
        Long partnerId = getLongValue(Constants.PARAM_KEY_PARTNER);

        List<OrgDto> allOrgs = orgService.findAllOrgs(partnerId);
        return allOrgs;

//		//判断是否超级管理员
//		boolean superAdmin = super.isSuperAdmin();
//		if(superAdmin){
//			List<OrgDto> allOrgs = orgService.findAllOrgs(super.getPartnerIdForOrg());
//			return allOrgs;
//		}
//
//		Map<String, String> fields = new HashMap<String, String>();
//		fields.put("TRANSID", "IPE_ORG_M");
//		fields.put("ACTIVITY", "M");
//		Map<Long, Boolean> atuhedOrgMap = PermissionUtil.getAuthorizedOrgs("TRANS", fields);
//
//		Long partnerId = super.getPartnerIdForOrg();
//		List<Long> orgIds = new ArrayList<>(atuhedOrgMap.keySet());
//		List<Long> partnerOrgIds = partnerCsService.findServiceOrg(orgIds, partnerId);
//		Set<Long> partnerOrgIdSet = new HashSet<>(partnerOrgIds);
//
//		//管理自己的公司
//		for(Long orgId : orgIds){
//			OrgDto org = orgService.getById(orgId);
//			if(org.getPartnerId().equals(partnerId)){
//				partnerOrgIdSet.add(orgId);
//			}
//		}
//
//	    return getOrderListByOrgIds(partnerOrgIdSet);
    }

//    /**
//     * 获取能管理的组织
//     *
//     * @param partnerOrgIdSet
//     * @return
//     */
//    private List<OrgDto> getOrderListByOrgIds(Set<Long> partnerOrgIdSet) {
//        Map<Long, OrgDto> orgMap = new HashMap<>();
//        List<OrgDto> rs = new ArrayList<>();
//        for (Long orgId : partnerOrgIdSet) {
//            OrgDto org = orgService.getById(orgId);
//
//            Map<String, Object> map = new HashMap<>(16);
//            map.put("partnerId", org.getPartnerId());
//            map.put("path", org.getPath());
//            List<OrgDto> orgDtoList = orgService.getList(map);
//            orgDtoList.add(org);
//
//            for (OrgDto orgDto : orgDtoList) {
//                orgMap.put(orgDto.getId(), orgDto);
//            }
//        }
//        rs.addAll(orgMap.values());
//
//        Collections.sort(rs);
//        return rs;
//    }

    @RequestMapping(value = "orgDetail", method = RequestMethod.GET)
    public String orgDetail(Model model, Long id, Long pid, String orgGrade) {
        Long partnerId = super.getLongValue(Constants.PARAM_KEY_PARTNER);

        OrgDto vo = new OrgDto();
        String orgGradeName = null;

        if (id != null) {
            vo = orgService.getById(id);

            if (vo != null) {
                OrgGradeDto partnerOrgGradeDto = orgGradeService.findByCode(partnerId, vo.getOrgGrade());
                if (partnerOrgGradeDto != null) {
                    orgGradeName = partnerOrgGradeDto.getName();
                    orgGrade = partnerOrgGradeDto.getPcode();
                }
            }
        } else {
            vo.setValidFrom(new Date());
            Calendar c = Calendar.getInstance();
            c.add(Calendar.YEAR, 60);
            vo.setValidTo(c.getTime());
        }

        List<DictCodeDto> dictCodeList = dictService.getPartnerCodes(partnerId, Constants.ORG_TYPE);
        model.addAttribute("cfgList", dictCodeList);
        model.addAttribute("orgGradeName", orgGradeName);
        model.addAttribute("orgGradeCode", orgGrade);
        model.addAttribute("pid", pid);
        model.addAttribute("org", vo);

        //获取角色为单位管理员的角色
        RoleDto role = roleService.getByCode(partnerId, Constants.IPE_ROLE_ORGM_CODE);
        if (role != null) {
            List<UserRoleDto> userRoleList = userRoleService.findUserRoleByOrgId(id);
            List<UserRoleDto> userRoleList1 = new ArrayList<UserRoleDto>();
            for (UserRoleDto userRole : userRoleList) {
                if (userRole.getRoleId().equals(role.getId())) {
                    userRoleList1.add(userRole);
                }
            }
            if (PermissionUtil.checkTrans(id, "IPE_ORG_M", "M") == 0) { //返回0表示有权限
                model.addAttribute("flagAuth", 1);
            }
            model.addAttribute("userRoleList", userRoleList1);
            model.addAttribute("orgId", id);
        }
        return "platform/org/orgDetail.jsp";
    }

    @RequestMapping(value = "/userRoleList")
    public String userRoleList(Model model, Long orgId) {

        Long partnerId = super.getLongValue(Constants.PARAM_KEY_PARTNER);

        RoleDto role = roleService.getByCode(partnerId, Constants.IPE_ROLE_ORGM_CODE);
        if (role != null) {

            List<UserRoleDto> userRoleList = userRoleService.findUserRoleByOrgId(orgId);
            List<UserRoleDto> userRoleList1 = new ArrayList<>();

            for (UserRoleDto userRole : userRoleList) {
                if (userRole.getRoleId().equals(role.getId())) {
                    userRoleList1.add(userRole);
                }
            }
            // 返回0表示有权限
            if (PermissionUtil.checkTrans(orgId, "IPE_ORG_M", "M") == 0) {
                model.addAttribute("flagAuth", 1);
            }
            model.addAttribute("userRoleList", userRoleList1);
            model.addAttribute("orgId", orgId);
        }
        return "platform/org/org_userRole.jsp";
    }

    @ResponseBody
    @RequestMapping(value = "/deleteUserRole")
    public Message deleteUserRole(Long id) {

        if (userRoleService.deleteUserRole(id)) {
            return Message.success("删除成功！");
        }
        return Message.error("删除失败！");
    }

    /**
     * 给选择的用户设置单位管理员
     *
     * @param userRole
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/addUserRole")
    public Message addUserRole(UserRoleDto userRole) {
        Long partnerId = super.getLongValue(Constants.PARAM_KEY_PARTNER);

        RoleDto role = roleService.getByCode(partnerId, Constants.IPE_ROLE_ORGM_CODE);
        if (role != null) {

            userRole.setValidFrom(new Date());
            userRole.setAssigner(super.getCurrentUser().getId());
            userRole.setScope("S,D,C");
            userRole.setRoleId(role.getId());
            if (userRoleService.addUserRole(userRole)) {
                return Message.success("添加成功");
            }
        }
        return Message.error("添加失败");
    }

    @ResponseBody
    @RequestMapping(value = "/saveOrUpdateOrg", method = RequestMethod.POST)
    public Message saveOrUpdateOrg(OrgDto dto) {

        if (dto.getId() == null) {

            dto.setEditUser(super.getCurrentUser().getId());
            orgService.addOrg(dto);
            return Message.success("添加成功");
        } else {

            if (orgService.updateOrg(dto)) {
                return Message.success("修改成功");
            }
            return Message.error("修改失败");
        }
    }

    /*
     * 判断组织编码
     */
    @ResponseBody
    @RequestMapping(value = "checkCode")
    public Boolean checkCode(String code, Long orgId) {

        Long partnerId = super.getLongValue(Constants.PARAM_KEY_PARTNER);

        Map<String, Object> params = new HashMap<>();
        params.put("partnerId", partnerId);
        params.put("code", code);
        params.put("orgId", orgId);
        Integer count = orgService.getOrgCount(params);
        if (count == 0) {
            return true;
        }
        return false;
    }

    /*
     * 判断组织层面
     */
    @ResponseBody
    @RequestMapping(value = "checkOrgGrade")
    public Map<String, Object> checkOrgGrade(String orgGradeCode, Integer isCompany) {

        Long partnerId = super.getLongValue(Constants.PARAM_KEY_PARTNER);

        Map<String, Object> map = new HashMap<>();
        map.put("partnerId", partnerId);
        map.put("pcode", orgGradeCode);

        List<OrgGradeDto> partnerOrgGradeList = orgGradeService.findListByMap(map);

        OrgGradeDto dto = new OrgGradeDto();
        if (partnerOrgGradeList.size() == 2) {
            if (isCompany == 0) {
                dto = partnerOrgGradeList.get(1);
            } else {
                dto = partnerOrgGradeList.get(0);
            }
        } else {
            if (isCompany == 1) {
                dto = partnerOrgGradeList.get(0);
            }
            if (isCompany == 0) {
                dto = partnerOrgGradeList.get(0);
            }
        }

        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("code", dto.getCode());
        resultMap.put("name", dto.getName());
        return resultMap;
    }

}
