package com.tem.platform.action;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.reflect.TypeToken;
import com.iplatform.common.cache.utils.RedisCacheUtils;
import com.iplatform.common.utils.JsonUtils;
import com.iplatform.common.web.Message;
import com.tem.platform.Constants;
import com.tem.platform.api.DictService;
import com.tem.platform.api.PartnercfgCfgItemService;
import com.tem.platform.api.PartnercfgDefService;
import com.tem.platform.api.PartnercfgDictDataService;
import com.tem.platform.api.PartnercfgItemDataService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.PartnercfgCfgItemDto;
import com.tem.platform.api.dto.PartnercfgDefDto;
import com.tem.platform.api.dto.PartnercfgDictDataDto;
import com.tem.platform.api.dto.PartnercfgItemDataDto;
import com.tem.platform.form.PartnerCfgForm;
import com.tem.platform.form.PartnerCfgListModel;
import com.tem.platform.util.TransUtils;


@Controller
@RequestMapping("platform/partnercfgDefCfg")
public class PartnercfgDefCfgController extends BaseController {

	@Autowired
	private PartnercfgDefService partnercfgDefService;
	
	@Autowired
	private PartnercfgCfgItemService partnercfgCfgitemService;
	
	@Autowired
	private PartnercfgDictDataService partnercfgDictdataService;
	
	@Autowired
	private PartnercfgItemDataService partnercfgItemDataService;
	
	@Autowired
	private DictService dictService;
	

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String page(Model model,HttpServletRequest req) {
		List<DictCodeDto> dictCodeDtoList = new ArrayList<DictCodeDto>();
		DictCodeDto dicCoodeDto = new DictCodeDto();
		dicCoodeDto.setItemCode(Constants.COMMON);
		dicCoodeDto.setItemTxt("公共系统");
		dictCodeDtoList.add(dicCoodeDto);
		dictCodeDtoList.addAll(dictService.getCodes(Constants.IP_SYS_LIST));
		
		model.addAttribute("sysId", super.getSysId());
		model.addAttribute("dictCodeDtoList", dictCodeDtoList);
		return "platform/partnerCfgDef/partnercfgcatalogCfg.base";
	}
	
	@ResponseBody
	@RequestMapping("/getCfgTree/{sysId}")
	public List<PartnercfgDefDto> getCfgTree(@PathVariable("sysId") String sysId){
		List<PartnercfgDefDto> list = partnercfgDefService.getAllBySysId(sysId);
		return list;
	}
	
	@ResponseBody
	@RequestMapping("/getCfgDicTreeTable")
	public List<PartnercfgDictDataDto> getCfgDicTreeTable(@RequestParam("sysId") String sysId,@RequestParam("dictCode") String dictCode){
		//得到当前的企业Id
		List<PartnercfgDictDataDto> list = partnercfgDictdataService.getByPartnerIdAndDictCode(super.getPartnerId(), dictCode);
		return list;
	}
	
	@RequestMapping("/addDicNode/{curDicTreeId}")
	public String addDicNode(Model model, @PathVariable("curDicTreeId") Long curDicTreeId) {
		model.addAttribute("typeTitle", "新增字典项");
		PartnercfgDictDataDto partnercfgDictdataDto = null;
		if(curDicTreeId == -1){//添加根节点
			partnercfgDictdataDto = new PartnercfgDictDataDto();
		}else{//添加当前节点的子节点
			partnercfgDictdataDto = partnercfgDictdataService.selectById(curDicTreeId);
		}
		model.addAttribute("pid", partnercfgDictdataDto.getId());
		return "platform/partnerCfgDef/partnercfgDic.jsp";
	}
	
	@RequestMapping("/updateDicNode/{curDicTreeId}")
	public String updateDicNode(Model model, @PathVariable("curDicTreeId") Long curDicTreeId) {
		model.addAttribute("typeTitle", "修改字典项");
		PartnercfgDictDataDto partnercfgDictdataDto = partnercfgDictdataService.selectById(curDicTreeId);
		model.addAttribute("pid", partnercfgDictdataDto.getPid());
		model.addAttribute("partnercfgDictdataDto", partnercfgDictdataDto);
		return "platform/partnerCfgDef/partnercfgDic.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrupdatePartnerDic", method = RequestMethod.POST)
	public Message saveOrupdatePartnerDic(@ModelAttribute("partnercfgDictdataDto") @Validated PartnercfgDictDataDto partnercfgDictdataDto,
			Model model,BindingResult br,HttpServletRequest req) {
		if(br.hasErrors()){
			return Message.error("数据校验不通过。");
		}
		partnercfgDictdataDto.setPartnerId(super.getPartnerId());//配置当前企业ID
		
		partnercfgDictdataDto.setUpdateUser(super.getCurrentUser().getId());
		partnercfgDictdataDto.setUpdateDate(new Date());
		
		int rs = 0;
		if (partnercfgDictdataDto.getId() == null) {
			rs = partnercfgDictdataService.insert(partnercfgDictdataDto);
			if (rs > 0) {
				return Message.success("新增成功。");
			} else {
				return Message.error("新增失败。");
			}
		} else {
			rs = partnercfgDictdataService.updateSelective(partnercfgDictdataDto);
			if (rs > 0) {
				return Message.success("修改成功。");
			} else {
				return Message.error("修改失败。");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/deletePartnerDic")
	public Message deletePartnerDic(Model model, @RequestParam("id") Long id) {
		if (id == null) {
			return Message.error("请先选择一行字典数据");
		}
		
		PartnercfgDictDataDto partnercfgDictdataDto = partnercfgDictdataService.selectById(id);
		//删除判断子项
		List<PartnercfgDictDataDto> partnercfgDictdataDtoList = partnercfgDictdataService.getAllByPid(id);
		if(partnercfgDictdataDtoList != null && partnercfgDictdataDtoList.size()>0){
			return Message.error("请先删除子节点");
		}else{
			//如果是组合配置项则下面还有值配置 需要删除
			
			//得到配置项,然后删除
			PartnercfgDefDto partnercfgDefDto = partnercfgDefService.getByRefDict(partnercfgDictdataDto.getDictCode());
			if(partnercfgDefDto != null && partnercfgDefDto.getNodeType() == 3){//组合配置组
				List<PartnercfgItemDataDto> partnercfgItemdataDtoList = partnercfgItemDataService.getByPartnerIdAndDictitemCode(super.getPartnerIdForOrg(), partnercfgDefDto.getRefDict(), partnercfgDictdataDto.getItemCode());
				if(partnercfgItemdataDtoList!= null && partnercfgItemdataDtoList.size()>0){
					for(PartnercfgItemDataDto partnercfgItemdataDto : partnercfgItemdataDtoList){
						partnercfgItemDataService.deleteById(partnercfgItemdataDto.getId());
					}
				}
			}
			if(partnercfgDictdataService.deleteById(id)>0) {
				return Message.success("删除成功");
			}
			return Message.error("删除失败");
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkPartnerDicCode")
	public Message checkPartnerDicCode(Model model, @RequestParam("itemCode") String itemCode,@RequestParam("id") Long id, @RequestParam("sysId") String sysId, @RequestParam("dictCode") String dictCode) {
		if(id != null){//修改下 code不能修改  id存在说明是修改状态下
			return Message.success("yes");
		}else {
			PartnercfgDictDataDto partnercfgDictdataDto = partnercfgDictdataService.getUnicode(super.getPartnerIdForOrg(), dictCode, itemCode);
			
			if(super.getPartnerId() != 0L){
				if(itemCode.startsWith("x_") || itemCode.startsWith("X_")){
					return Message.error("no");
				}
			}
			if (partnercfgDictdataDto == null) {
				return Message.success("yes");
			}else{
				return Message.error("no");
			}
		}
		
	}
	
	@RequestMapping(value = "getPartnercfgRight")
	public String getPartnercfgRight(Model model,@RequestParam("nodeType") Integer nodeType) {
		String url = "";
		if(nodeType == 0){//目录
			url = "platform/partnerCfgDef/emptyInfo.jsp";
		}else if(nodeType == 1){//数据字典
			url = "platform/partnerCfgDef/partnercfgDicMain.jsp";
		}else if(nodeType == 2){//配置组
			url = "platform/partnerCfgDef/partnercfgItemMain.jsp";
		}else if(nodeType == 3){//组合配置组
			url = "platform/partnerCfgDef/partnercfgDicMain.jsp";
		}else if(nodeType == 4){//其它作业
			
		}
		return url;
	}
	
	/**
	 * 得到配置值
	 * @param cfgnodeCode
	 * @param sysId
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getCfgItemTreeTable")
	public List<HashMap<String, Object>> getCfgItemTreeTable(@RequestParam("cfgnodeCode")String cfgnodeCode,@RequestParam("sysId")String sysId,@RequestParam("dictitemCode")String dictitemCode){
		List<HashMap<String, Object>> resultList = null;
		List<PartnercfgCfgItemDto> partnercfgCfgitemDtoList = partnercfgCfgitemService.getAllByCfgnodeCode(cfgnodeCode);//得到该配置定义的所有配置项
		if(partnercfgCfgitemDtoList != null && partnercfgCfgitemDtoList.size()>0){
			resultList = new ArrayList<HashMap<String,Object>>();
			List<PartnercfgItemDataDto> partnercfgItemdataDtoList = null;
			if(StringUtils.isNotEmpty(dictitemCode)){//说明是组合配置项
				PartnercfgDefDto  partnercfgDefDto = partnercfgDefService.getByNodeCode(cfgnodeCode);
				if(partnercfgDefDto != null){
					partnercfgItemdataDtoList = partnercfgItemDataService.getByPartnerIdAndDictitemCode(super.getPartnerIdForOrg(), partnercfgDefDto.getRefDict(), dictitemCode);//已配置的配置项
				}
			}else{
				partnercfgItemdataDtoList = partnercfgItemDataService.getByPartnerIdAndCfgnodeCode(super.getPartnerIdForOrg(),cfgnodeCode);//已配置的配置项
			}
			
			HashMap<String, Object> map = null;
			for(PartnercfgCfgItemDto partnercfgCfgitemDto : partnercfgCfgitemDtoList){
				 map = new HashMap<String, Object>();
				 map.put("ID", partnercfgCfgitemDto.getId());
				 map.put("PID", partnercfgCfgitemDto.getPid());
				 map.put("CFGNODE_CODE", partnercfgCfgitemDto.getCfgnodeCode());
				 map.put("ITEM_NAME", partnercfgCfgitemDto.getItemName());
				 map.put("ITEM_CODE", partnercfgCfgitemDto.getItemCode());
				 map.put("ITEM_TYPE", partnercfgCfgitemDto.getItemType());
				 map.put("OPTIONAL_VALUES", partnercfgCfgitemDto.getOptionalValues());
				 
				 if(partnercfgItemdataDtoList != null && partnercfgItemdataDtoList.size()>0){
					 for(PartnercfgItemDataDto partnercfgItemdataDto : partnercfgItemdataDtoList){
						 String itemvalue = "";
						 if(partnercfgItemdataDto.getCfgitemCode().equals(partnercfgCfgitemDto.getItemCode())){
							 if(partnercfgCfgitemDto.getItemType()==1 || partnercfgCfgitemDto.getItemType()==2){//下拉框和checkbox转译
								 //特殊编码需要转换  
								 String values = TransUtils.getTransShowValue(partnercfgItemdataDto.getCfgitemCode(), partnercfgItemdataDto.getValue());
								
								 if(values != null){
									 String[] valuesArr = values.split(";");
									 String optionalValues = partnercfgCfgitemDto.getOptionalValues();
									 String[] optionalValuesArr = null;
									 if(StringUtils.isNotEmpty(optionalValues)){
										 optionalValuesArr = optionalValues.split(";");
									 }else{
										 continue;
									 }
									 for(String value : valuesArr){
										 for(String optionalValue : optionalValuesArr){
											 String[] details = optionalValue.split(":");
											 if(value.equals(details[0])){
												 itemvalue = itemvalue + details[1]+"; ";
											 }
										 }
									 }
								 }else{
									 continue;
								 }
								 if(StringUtils.isNotEmpty(itemvalue)){
									 itemvalue = itemvalue.substring(0, itemvalue.length()-2);
								 }else{
									 itemvalue = partnercfgItemdataDto.getValue();//说明是手填的
								 }
							 }else{
								 itemvalue = partnercfgItemdataDto.getValue();
							 }
							 map.put("ITEMVALUE", itemvalue);
							 map.put("DETAILID", partnercfgItemdataDto.getId());
						 }
					 }
				 }
				 resultList.add(map);
			}
		}
		return resultList;
	}
	/**
	 * 将json字符串转成Map
	 * @param value
	 * @return
	 */
	private Map<String, Object> transStringToJson(String value) {
		Type type = new TypeToken<Map<String,Object>>(){}.getType();
		Map<String,Object> jsonValue = JsonUtils.getGson().fromJson(value, type);
		return jsonValue;
	}

	@ResponseBody
	@RequestMapping(value = "/saveorupdateItemData")
	public Message saveorupdateItemData(PartnerCfgListModel cfgModel){

		List<PartnerCfgForm> cfgList = cfgModel.getCfgList();

		boolean flag = true;

		for(PartnerCfgForm cfgForm : cfgList){
			if(cfgForm.getId() == null){
				continue;
			}
			
			Long id = cfgForm.getId();
			
			//checkbox多选框，传入后台数据多值用“,”分割，取第一个
			String detailid = (StringUtils.isEmpty(cfgForm.getDetailid()) || ",".equals(cfgForm.getDetailid()))?null:cfgForm.getDetailid().split(",")[0];
			String cfgItemValue = (StringUtils.isEmpty(cfgForm.getCfgItemValue()) || ",".equals(cfgForm.getCfgItemValue()))?"":cfgForm.getCfgItemValue().split(",")[0];
			String dictitemCode = (StringUtils.isEmpty(cfgForm.getDictitemCode()) || ",".equals(cfgForm.getDictitemCode()))?null:cfgForm.getDictitemCode().split(",")[0];

			if("undefined".equals(detailid)){
				detailid = null;
			}

			if(StringUtils.isNotEmpty(detailid)){//说明是修改
				PartnercfgItemDataDto partnercfgItemdataDto = partnercfgItemDataService.selectById(Long.parseLong(detailid));
				partnercfgItemdataDto.setUpdateUser(super.getCurrentUser().getId());
				partnercfgItemdataDto.setUpdateDate(new Date());
				partnercfgItemdataDto.setValue(cfgItemValue);
				if (partnercfgItemDataService.updateSelective(partnercfgItemdataDto) <= 0) {
					flag = false;
				}else{
					//修改配置，删除缓存
					if(super.getPartnerId() != null){
						RedisCacheUtils.del("MEMBER_" + partnercfgItemdataDto.getCfgitemCode(), super.getPartnerId().toString());
					}
				}
			}else{//说明是新增
				PartnercfgCfgItemDto partnercfgCfgitemDto = partnercfgCfgitemService.selectById(id);//得到该配置行
				PartnercfgItemDataDto partnercfgItemdataDto = new PartnercfgItemDataDto();
				partnercfgItemdataDto.setCfgitemCode(partnercfgCfgitemDto.getItemCode());
				partnercfgItemdataDto.setCfgnodeCode(partnercfgCfgitemDto.getCfgnodeCode());
				partnercfgItemdataDto.setValue(cfgItemValue);
				partnercfgItemdataDto.setPartnerId(getPartnerIdForOrg());
				partnercfgItemdataDto.setUpdateUser(super.getCurrentUser().getId());
				partnercfgItemdataDto.setUpdateDate(new Date());

				if(StringUtils.isNotEmpty(dictitemCode)){//说明是组合配置项
					partnercfgItemdataDto.setDictitemCode(dictitemCode);
					PartnercfgDefDto partnercfgDefDto = partnercfgDefService.getByNodeCode(partnercfgCfgitemDto.getCfgnodeCode());
					if(partnercfgDefDto != null){
						partnercfgItemdataDto.setRefDict(partnercfgDefDto.getRefDict());
					}
				}
				if(partnercfgItemDataService.insert(partnercfgItemdataDto) <= 0){
					flag = false;
				}else{
					//删除0企业缓存配置
					if(super.getPartnerId() != null){
						RedisCacheUtils.del("MEMBER_" + partnercfgItemdataDto.getCfgitemCode(), super.getPartnerId().toString());
					}
				}
			}
			
		}

		if(flag){
			return Message.success("配置成功！");
		}
		return Message.error("配置失败!请刷新重试!");

	}
	
}
