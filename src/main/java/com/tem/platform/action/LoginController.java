package com.tem.platform.action;

import com.google.code.kaptcha.Producer;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.tem.notify.api.MailService;
import com.tem.notify.api.SmsService;
import com.tem.notify.dto.TemplateMailDto;
import com.tem.platform.api.LoginLogService;
import com.tem.platform.api.dto.LoginLogDto;
import com.tem.platform.Constants;
import com.tem.platform.util.GenSeqUtil;
import com.tem.platform.util.LoginRedisManger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author jieming
 *
 */
@Controller
@RequestMapping()
public class LoginController extends BaseController {

	private final static String	USER_VAILID = "USER_VAILID";

	public static final String USER_EMAIL_VALID = "USER_EMAIL_VALID";

	@Autowired
	private LoginLogService	loginLogService;

	@Autowired
	private SmsService smsService;

	@Autowired
	private MailService mailService;

	@Autowired
	private LoginRedisManger loginRedisManger;

	@Autowired
	private Producer captchaProducer;

	private static final String CHECK_MOBILE_PATTERN = "^1[345789]\\d{9}$";

	@RequestMapping(value = "login")
	public String login(){

		return "platform/login/login.jsp";
	}

	@RequestMapping(value = "/login/sendCode")
	@ResponseBody
	public boolean sendCode(String username, String judgeCode) {

		Boolean flag = judgeCode(judgeCode);
		if(!flag){
			return false;
		}

		String code = GenSeqUtil.getGenSeq(6);
		sendEmailCode(username,code);
		sendMobileCode(username,code);

		return true;
	}

	private void sendMobileCode(String mobile, String code){
		if(checkMobileNumber(mobile)){
			smsService.sendSms(mobile, USER_VAILID, new String[]{code,"5"},null);
			loginRedisManger.setCodeCache(mobile, code);
		}
	}

	private void sendEmailCode(String email, String code){
		if(checkEmail(email)){

			TemplateMailDto templateMail = new TemplateMailDto();
			templateMail.setTemplateCode(USER_EMAIL_VALID);

			List<String> arrayList = new ArrayList<String>();
			arrayList.add(email);
			templateMail.setToAddress(arrayList);

			Map<String, Object> args = new HashMap<String, Object>();
			args.put("code", code);
			templateMail.setArgs(args);

			mailService.sendMail(templateMail);
			loginRedisManger.setCodeCache(email, code);
		}
	}

	@RequestMapping(value ="loginLog")
	public String loginLog(Model model,Integer pageIndex,String keyword) {
		if(pageIndex == null){
			pageIndex = 1;
		}
		
		QueryDto<Object> queryDto=new QueryDto<Object>(pageIndex,Page.DEFAULT_PAGE_SIZE ,"LOGIN_DATE" ,"desc");
		queryDto.setField1(keyword);
		
		Page<LoginLogDto> pageList = loginLogService.queryListWithPage(queryDto);
		model.addAttribute("pageList",pageList );
		model.addAttribute("keyword", keyword);
		
		return "platform/login/index.base";
	}

	/**
	 * 获取验证码
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "login/code",method= RequestMethod.GET)
	public ModelAndView getKaptchaImage() throws Exception {
		HttpServletResponse response = getResponse();

		response.setDateHeader("Expires", 0);
		response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		response.addHeader("Cache-Control", "post-check=0, pre-check=0");
		response.setHeader("Pragma", "no-cache");
		response.setContentType("image/jpeg");

		//验证码
		String capText = captchaProducer.createText();
		//验证码存在session中
		getSession().setAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY, capText);

		BufferedImage bi = captchaProducer.createImage(capText);
		ServletOutputStream out = response.getOutputStream();

		ImageIO.write(bi, "jpg", out);
		try {
			out.flush();
		} finally {
			out.close();
		}
		return null;
	}

	private Boolean judgeCode(String judgeCode){

		//session中的验证码
		String capText=(String)getSession().getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);

		if(judgeCode!=null && judgeCode.equalsIgnoreCase(capText)){

			return true;
		}
		return false;
	}


	/**
	 * 验证邮箱
	 *
	 * @param email
	 * @return
	 */
	public static boolean checkEmail(String email) {
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 验证手机号码，11位数字，1开通，第二位数必须是3456789这些数字之一
	 * @param mobileNumber
	 * @return
	 */
	public static boolean checkMobileNumber(String mobileNumber) {
		boolean flag = false;
		try {
			Pattern regex = Pattern.compile(CHECK_MOBILE_PATTERN);
			Matcher matcher = regex.matcher(mobileNumber);
			flag = matcher.matches();
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;

		}
		return flag;
	}
}
