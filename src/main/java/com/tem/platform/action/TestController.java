package com.tem.platform.action;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("platform/test")
public class TestController extends BaseController {
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public String page(Model model,HttpServletRequest req) {
		return "test/test.base";
	}
	
	@RequestMapping(value = "sco.html", method = RequestMethod.GET)
	public String sco(Model model,HttpServletRequest req) {
		return "test/sco.base";
	}

}
