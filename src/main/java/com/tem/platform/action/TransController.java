package com.tem.platform.action;

import com.iplatform.common.web.Message;
import com.tem.platform.api.AuthtypeService;
import com.tem.platform.api.TransAuthtypeService;
import com.tem.platform.api.TransService;
import com.tem.platform.api.dto.AuthtypeDto;
import com.tem.platform.api.dto.TransAuthtypeDto;
import com.tem.platform.api.dto.TransDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("platform/trans")
public class TransController extends BaseController {
	
	@Autowired
	private TransService transService;
	
	@Autowired
	private TransAuthtypeService transAuthtypeService;
	
	@Autowired
	private AuthtypeService authtypeService;
	
	@RequestMapping("")
	public String index(){
		return "platform/trans/trans.base";
	}
	
	@ResponseBody
	@RequestMapping(value="/showMenu")
	public List<TransDto> showMenu(){
		List<TransDto> list = transService.findList();
		return list;
	}
	
	@RequestMapping(value = "/detail")
	public String listDetail(Model model, Long id) {
		TransDto transDto = transService.getById(id);
		List<Long> authtypeIdList = transAuthtypeService.findListByTransId(id);
		if(authtypeIdList.size()>0){
			List<AuthtypeDto> authtypeDtoList = authtypeService.findListByIds(authtypeIdList);
			model.addAttribute("authtypeDtoList", authtypeDtoList);
		}
		model.addAttribute("transDto", transDto);
		return "platform/trans/transRight.jsp";
	}
	
	
	@RequestMapping("/add")
	public String add(Model model, Long pid) {
		TransDto transDto = new TransDto();
		transDto.setPid(pid);
		model.addAttribute("transDto", transDto);
		return "platform/trans/transDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/checkCode")
	public String checkCode(String code, Long id) {
		if (code == null || "".equals(code)) {
			return "ok";
		}
		TransDto transDto = transService.getByCode(code);
		if (transDto == null) {
			return "ok";
		}else if(id!=null){
			if(id.equals(transDto.getId())){
				return "ok";
			}
		}else{
			return "no";
		}
		return "no";
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrupdate", method = RequestMethod.POST)
	public Message saveOrupdate(@ModelAttribute("transDto") @Validated TransDto transDto,
			Model model,BindingResult br) {
		if(br.hasErrors()){
			return Message.error("数据校验不通过。");
		}
		boolean rs = false;
		if (transDto.getId() == null) {
			int trandId =  transService.add(transDto);
			if (trandId > 0) {
				return Message.success(""+trandId);
			} else {
				return Message.error("新增失败");
			}
		} else {
			rs = transService.update(transDto);
			if (rs) {
				return Message.success(""+transDto.getId());
			} else {
				return Message.error("修改失败");
			}
		}
	}
	
	@RequestMapping("/getTrans")
	public String getTrans(Model model , Long id){
		TransDto transDto = transService.getById(id);
		model.addAttribute("transDto",transDto);
		return "platform/trans/transDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping("/delete")
	public Message detele( @RequestParam("id")Long id){
		boolean rs = transService.delete(id);
		if(rs){
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败");
		}
	}
	
	@ResponseBody
	@RequestMapping("/deleteTransType")
	public Message deleteTransType(Long transId,Long authtypeId){
		TransAuthtypeDto transAuthtypeDto = new TransAuthtypeDto();
		transAuthtypeDto.setTransId(transId);
		transAuthtypeDto.setAuthtypeId(authtypeId);
		boolean rs = transAuthtypeService.delete(transAuthtypeDto);
		if(rs){
			return Message.success("删除成功");
		}else{
			return Message.error("删除失败");
		}
	}
	
	@ResponseBody
	@RequestMapping("/addTransAuthtype")
	public Message addTransAuthtype(Long transId,Long authtypeId){
		TransAuthtypeDto transAuthtypeDto = new TransAuthtypeDto();
		transAuthtypeDto.setTransId(transId);
		transAuthtypeDto.setAuthtypeId(authtypeId);
		boolean rs = transAuthtypeService.add(transAuthtypeDto);
		if(rs){
			return Message.success("新增权限项成功");
		}else{
			return Message.error("新增权限项失败");
		}
	}
	
	@RequestMapping("/findAuthtypeList/{id}")
	public String findAuthtypeList(Model model,@PathVariable("id")Long transId){
		
		List<Long> ids = transAuthtypeService.findListByTransId(transId);
		List<AuthtypeDto> authtypeList = authtypeService.findAuthtype(ids);
		model.addAttribute("authtypeList",authtypeList);
		
		return "platform/trans/authtypeTable.jsp";
	}

}
