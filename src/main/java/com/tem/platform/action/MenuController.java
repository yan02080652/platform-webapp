package com.tem.platform.action;

import com.iplatform.common.Config;
import com.iplatform.common.web.Message;
import com.tem.imgserver.client.ImgClient;
import com.tem.platform.api.DictService;
import com.tem.platform.api.MenuService;
import com.tem.platform.api.TransService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.MenuDto;
import com.tem.platform.api.dto.TransDto;
import com.tem.platform.Constants;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;


@Controller
@RequestMapping("platform/menuconfig")
public class MenuController extends BaseController {

	@Autowired
	private MenuService menuService;
	
	@Autowired
	private TransService transService;
	
	@Autowired
	private DictService dictService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String page(Model model,HttpServletRequest req) {
		String sceneId = req.getParameter("sceneId");
		List<DictCodeDto> sysList = dictService.getCodes(Constants.IP_SYS_LIST);
		List<DictCodeDto> sceneList = dictService.getCodes(Constants.IP_SCENE_LIST,super.getSysId(),null);
		if(StringUtils.isEmpty(sceneId) && sceneList != null && sceneList.size()>0){
			sceneId = sceneList.get(0).getItemCode();//得到当前第一个场景
		}
		model.addAttribute("sysId", super.getSysId());
		model.addAttribute("sysList", sysList);
		model.addAttribute("sceneList", sceneList);
		model.addAttribute("sceneId", sceneId);
		return "platform/menu/menu.base";
	}
	
	@RequestMapping(value = "/menu", method = RequestMethod.GET)
	public @ResponseBody
	List<MenuDto> getMenuTree(@RequestParam("sysId") String sysId,@RequestParam("sceneId") String sceneId) {
		List<MenuDto> menuList = menuService.getAllBySysIdAndSceneId(sysId,sceneId);
		return menuList;
	}
	
	@ResponseBody
	@RequestMapping(value = "/checkCode")
	public Message checkCode(Model model, @RequestParam(value="id",required=false) Long id, @RequestParam(value="code",required=false) String code) {
		
		if(StringUtils.isEmpty(code)){
			return Message.success("yes");
		}
		MenuDto menu = menuService.selectByCode(code);
		if (menu == null) {
			return Message.success("yes");
		}else{
			if(id != null){//说明修改的
				MenuDto menu2 = menuService.selectById(id);
				if(code.equals(menu2.getCode())){
					return Message.success("yes");
				}
			}
			return Message.error("no");
		}
		
	}
	
	@RequestMapping(value = "/menuDetail", method = RequestMethod.GET)
	public String menuDetail(Model model, @RequestParam("id") Long id,@RequestParam("pid") Long pid) {
		MenuDto menuDto = null;
		if (id != null && id != 0) {
			menuDto = menuService.selectById(id);
			if(menuDto.getType()==1){//作业
				TransDto transDto = transService.getByCode(menuDto.getDef());
				model.addAttribute("transDto", transDto);
			}
			model.addAttribute("menuDto", menuDto);
		}else{
			menuDto = new MenuDto();
		}
		model.addAttribute("pid", pid==0?null:pid);
		return "platform/menu/menuDetail.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrupdateMenu")
	public Message saveOrupdateMenu(MenuDto menuDto,Model model,HttpServletRequest req,MultipartFile iconFile) {
		
		if(menuDto.getType()==1){//作业 保存code
			menuDto.setDef(req.getParameter("defCode"));
		}
		
		try {
			// 图片上传
			if (iconFile != null) {
				String fileName = iconFile.getOriginalFilename();
				if (StringUtils.isNotEmpty(fileName)) {
					// 获取后缀名
					String img_suffix = fileName.substring(fileName.lastIndexOf("."));
					// 生成一个新的文件名
					String newName = "";
					if(StringUtils.isNotEmpty(menuDto.getCode())){
						newName = menuDto.getCode() + img_suffix;
					}else{
						newName = UUID.randomUUID() + img_suffix;
					}
					// 图片存放的位置
					String path = Config.getString("menuroot");
					
					ImgClient.uploadImg(iconFile, path,newName);
					
					menuDto.setIcon(path + "/" + newName);
				}
			}
		} catch (Exception e) {
			return Message.error("文件上传出错，请刷新重试");
		}
		
		int rs = 0;
		if (menuDto.getId() == null) {
			rs = menuService.insert(menuDto);
			if (rs > 0) {
				return Message.success("新增成功。");
			} else {
				return Message.error("新增失败。");
			}
		} else {
			rs = menuService.updateSelective(menuDto);
			if (rs > 0) {
				return Message.success("修改成功。");
			} else {
				return Message.error("修改失败。");
			}
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteMenuNode")
	public Message deleteMenuNode(Model model, @RequestParam("id") Long id) {
		if (id == null) {
			return Message.error("请先选择一个节点");
		}
		//删除判断子项
		MenuDto menuDto = menuService.selectById(id);
		if(menuDto.getType() == 2){//目录文件 则查看下面是否还有子项
			List<MenuDto> rs = menuService.selectByPid(menuDto.getId());
			if(rs != null && rs.size()>0){
				return Message.error("请先删除子节点");
			}else{
				if(menuService.deleteById(id)>0) {
                    return Message.success("删除成功");
                }
				return Message.error("删除失败");
			}
		}else{
			if(menuService.deleteById(id)>0) {
                return Message.success("删除成功");
            }
			return Message.error("删除失败");
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/deleteIcon")
	public Message deleteIcon(Model model, @RequestParam("id") Long id) {
		MenuDto menuDto = menuService.selectById(id);
		if(StringUtils.isNotEmpty(menuDto.getIcon())){
			ImgClient.deleteImg("menu", menuDto.getIcon().substring(menuDto.getIcon().indexOf("menu/"),menuDto.getIcon().length()-1));
			menuService.deleteMenuIcon(id);
		}
		return Message.success("删除成功");
	}
	
	@RequestMapping(value = "/selectMenu", method = RequestMethod.GET)
	public String selectMenu(Model model) {
		return "platform/menu/menuTree.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value = "/moveMenu")
	public Message moveMenu(Model model, @RequestParam("fromId") Long fromId, @RequestParam("toId") Long toId) {
		MenuDto menuDto = menuService.selectById(fromId);
		if(menuDto != null){
			menuDto.setPid(toId);
			if(menuService.update(menuDto)>0) {
                return Message.success("移动成功");
            }
			return Message.error("移动失败");
			
		}else{
			return Message.error("移动失败");
		}
	}
	
	@RequestMapping(value = "/selectTrans", method = RequestMethod.GET)
	public String selectTrans(Model model) {
		return "platform/menu/transTree.jsp";
	}
}
