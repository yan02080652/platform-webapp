package com.tem.platform.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.platform.api.*;
import com.tem.platform.api.condition.PartnerCheckCondition;
import com.tem.platform.api.dto.*;
import com.tem.platform.form.PartnerCheckingForm;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 企业确认维护	
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("partner/check")
public class PartnerCheckController extends BaseController {
	
	@Autowired
	private PartnerCheckingService partnerCheckingService;
	
	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private OrgService orgService;
	
	@Autowired
	private PartnerCsService partnerCsService;
	
	@Autowired
	private PartnerBpService partnerBpService;
	
	@RequestMapping(value = "list")
	public String index(Model model,@RequestParam(value="pageIndex",required=false)Integer pageIndex,
			@RequestParam(value="name",required=false)String name,
			@RequestParam(value="status",required=false)Integer status) {
		
		pageIndex = pageIndex == null ? 1 : pageIndex;
		QueryDto<PartnerCheckCondition> queryDto = new QueryDto<PartnerCheckCondition>(pageIndex,Page.DEFAULT_PAGE_SIZE);
		PartnerCheckCondition condition = new PartnerCheckCondition();
		
		if(StringUtils.isNotEmpty(name)){
			condition.setName(name);
		}
		if(status == null){//默认为未处理的
			status = 1;
			condition.setStatus(status);
		}else if(status.intValue() == -2){//-2是全部
			condition.setStatus(null);
		}else{
			condition.setStatus(status);
		}
		queryDto.setCondition(condition);
	   
		Page<PartnerCheckingDto> partnerCheckPage = partnerCheckingService.queryListWithPage(queryDto);
		
		//转译人员
		List<PartnerCheckingDto> partnerCheckList = partnerCheckPage.getList();
		List<PartnerCheckingForm> resultList = new ArrayList<PartnerCheckingForm>();
		if(CollectionUtils.isNotEmpty(partnerCheckList)){
			PartnerCheckingForm partnerCheckingForm = null;
			for(PartnerCheckingDto partnerCheckingDto : partnerCheckList){
				partnerCheckingForm = TransformUtils.transform(partnerCheckingDto, PartnerCheckingForm.class);
				if(partnerCheckingDto.getStatus() == null || partnerCheckingDto.getStatus() == 0){
					partnerCheckingForm.setStatusName("待审核");
				}else if(partnerCheckingDto.getStatus() == 1){
					partnerCheckingForm.setStatusName("审核中");
				}else if(partnerCheckingDto.getStatus() == -1){
					partnerCheckingForm.setStatusName("不通过");
				}else if(partnerCheckingDto.getStatus() == 2){
					partnerCheckingForm.setStatusName("审核通过");
				}
				
				if(partnerCheckingDto.getFollowUser() != null){
					UserDto user = userService.getUser(partnerCheckingDto.getFollowUser());
					if(user != null){
						partnerCheckingForm.setFollowUserName(user.getFullname());
					}
				}
				
				if(partnerCheckingDto.getConfirmUser() != null){
					UserDto user = userService.getUser(partnerCheckingDto.getConfirmUser());
					if(user != null){
						partnerCheckingForm.setConfirmUserName(user.getFullname());
					}
				}
				
				if(StringUtils.isNotEmpty(partnerCheckingDto.getIndustry())){
					String industryName = dictService.getCodeTxt("IP_INDUSTRY", partnerCheckingDto.getIndustry());
					partnerCheckingForm.setIndustryName(industryName);
				}
				resultList.add(partnerCheckingForm);
			}
		}
		
		Page<PartnerCheckingForm> resultPage = new Page<PartnerCheckingForm>(partnerCheckPage.getStart(), partnerCheckPage.getSize(), resultList, partnerCheckPage.getTotal());
		
		model.addAttribute("pageList", resultPage);
		model.addAttribute("name", name);
		model.addAttribute("status", status);
		return "platform/partner/partnerCheckList.base";
	}
	
	@ResponseBody
	@RequestMapping(value = "/followPartner")
	public Message followPartner(Model model, @RequestParam(value="id") Long id) {
		PartnerCheckingDto partnerCheckingDto = partnerCheckingService.findById(id);
		if(partnerCheckingDto.getStatus() != null && partnerCheckingDto.getStatus() != 0){
			return Message.error("此客户已经被人认领跟进了，请刷新页面！");
		}
		partnerCheckingDto.setStatus(1);
		partnerCheckingDto.setFollowUser(super.getCurrentUser().getId());
		partnerCheckingDto.setFollowTime(new Date());
		partnerCheckingService.update(partnerCheckingDto);
		return Message.success("跟进成功！");
	}
	
	@ResponseBody
	@RequestMapping(value = "/rejectPartnerCheck")
	public Message rejectPartnerCheck(Model model, @RequestParam(value="id") Long id,@RequestParam(value="pageIndex",required=false)String confirmReason) {
		PartnerCheckingDto partnerCheckingDto = partnerCheckingService.findById(id);
		if(partnerCheckingDto.getStatus() == 2 || partnerCheckingDto.getStatus() == -1){
			return Message.error("此客户已被处理，请刷新页面！");
		}
		if(partnerCheckingDto.getFollowUser().longValue() != super.getCurrentUser().getId().longValue()){
			return Message.error("此客户的跟进人不为你，请不要操作！");
		}
		partnerCheckingDto.setStatus(-1);
		partnerCheckingDto.setConfirmTime(new Date());
		partnerCheckingDto.setConfirmUser(super.getCurrentUser().getId());
		partnerCheckingDto.setConfirmReason(confirmReason);
		partnerCheckingService.update(partnerCheckingDto);
		return Message.success("跟进成功！");
	}
	
	@ResponseBody
	@RequestMapping(value = "/passPartnerCheck")
	public Message passPartnerCheck(Model model, PartnerCheckingDto partnerCheckingDto) {
		if(StringUtils.isEmpty(partnerCheckingDto.getAlias())){
			partnerCheckingDto.setAlias(partnerCheckingDto.getName());
		}
		if(partnerCheckingDto.getStatus() == 2 || partnerCheckingDto.getStatus() == -1){
			return Message.error("此客户已被处理，请刷新页面！");
		}
		if(partnerCheckingDto.getFollowUser().longValue() != super.getCurrentUser().getId().longValue()){
			return Message.error("此客户的跟进人不为你，请不要操作！");
		}
		partnerCheckingDto.setStatus(2);
		partnerCheckingDto.setConfirmTime(new Date());
		partnerCheckingDto.setConfirmUser(super.getCurrentUser().getId());
		partnerCheckingService.update(partnerCheckingDto);
		
		//审核通过创建企业客户
		PartnerCheckingDto partnerChecking = partnerCheckingService.findById(partnerCheckingDto.getId());
		PartnerDto partner = TransformUtils.transform(partnerChecking, PartnerDto.class);
		partner.setType(0L);
		partner.setCreateUser(super.getCurrentUser().getId());
		partner.setCreateDate(new Date());
		partner.setStatus(0);
		partner.setAlias(partnerCheckingDto.getAlias());
		if(StringUtils.isNotEmpty(partnerChecking.getAdminMail())){//取邮箱后缀 作为域名
			String[] ems = partnerChecking.getAdminMail().split("@");
			if(ems.length ==2 ){
				//判断是不是不符合规则的邮箱
				DictCodeDto dict = dictService.getCode("IP_INVALIDE_MAIL_DOMAIN", ems[1]);
				if(dict == null){
					partner.setMailDomain(ems[1]);
				}
			}
		}
		Map<String,Object> map = partnerService.add(partner);
		Long orgId = (Long) map.get("orgId");
		
		//创建tmc分配
		OrgDto org = orgService.getOrg(orgId);
		List<PartnerBpDto> partnerBpDtos = partnerBpService.findPartnerBpByPartnerOrgId(orgId);
		if(partnerBpDtos != null && partnerBpDtos.size() > 0){
			for(PartnerBpDto partnerBpDto : partnerBpDtos){
				partnerCsService.deleteByPartnerOrgId(partnerBpDto.getPartnerOrgId());
				
				partnerBpService.deleteBPRelation(partnerBpDto.getPartnerId(), partnerBpDto.getPartnerOrgId(), partnerBpDto.getBpId());
			}
		}
		PartnerBpDto partnerBp = new PartnerBpDto();
		partnerBp.setPartnerId(org.getPartnerId());
		partnerBp.setPartnerOrgId(orgId);
		partnerBp.setBpId(super.getCurrentUser().getPartnerId());
		partnerBpService.insert(partnerBp);
		
		//在客户客服组织关系表中 默认插入一条信息
		PartnerCsDto partnerCs = new PartnerCsDto();
		partnerCs.setPartnerId(org.getPartnerId());
		partnerCs.setPartnerOrgId(orgId);
		partnerCs.setBpId(super.getCurrentUser().getPartnerId());
		//找到这个bp的根组织
		List<OrgDto> orgs = orgService.findAllCompanyOrgs(super.getCurrentUser().getPartnerId());//得到此bp的所有公司单位
		if(orgs != null && orgs.size() > 0){
			for(OrgDto orgDto : orgs){
				if("1.0".equals(orgDto.getOrgGrade())){
					partnerCs.setBpOrgId(orgDto.getId());
				}
			}
		}
		partnerCsService.insert(partnerCs);
		return Message.success("确认成功！");
	}

	@RequestMapping(value = "checkDetail")
	public String checkDetail(Model model,@RequestParam(value="id",required=false)Long id) {
		PartnerCheckingDto partnerCheckingDto = partnerCheckingService.findById(id);
		PartnerCheckingForm partnerCheckingForm = TransformUtils.transform(partnerCheckingDto, PartnerCheckingForm.class);
		if(partnerCheckingDto.getStatus() == null || partnerCheckingDto.getStatus() == 0){
			partnerCheckingForm.setStatusName("待审核");
		}else if(partnerCheckingDto.getStatus() == 1){
			partnerCheckingForm.setStatusName("审核中");
		}else if(partnerCheckingDto.getStatus() == -1){
			partnerCheckingForm.setStatusName("不通过");
		}else if(partnerCheckingDto.getStatus() == 2){
			partnerCheckingForm.setStatusName("审核通过");
		}
		
		if(partnerCheckingDto.getFollowUser() != null){
			UserDto user = userService.getUser(partnerCheckingDto.getFollowUser());
			if(user != null){
				partnerCheckingForm.setFollowUserName(user.getFullname());
			}
		}
		
		if(partnerCheckingDto.getConfirmUser() != null){
			UserDto user = userService.getUser(partnerCheckingDto.getConfirmUser());
			if(user != null){
				partnerCheckingForm.setConfirmUserName(user.getFullname());
			}
		}
		
		if(StringUtils.isNotEmpty(partnerCheckingDto.getIndustry())){
			String industryName = dictService.getCodeTxt("IP_INDUSTRY", partnerCheckingDto.getIndustry());
			partnerCheckingForm.setIndustryName(industryName);
		}
		if(StringUtils.isNotEmpty(partnerCheckingForm.getAdminMail())){//取邮箱后缀 作为域名
			String[] ems = partnerCheckingForm.getAdminMail().split("@");
			if(ems.length ==2 ){
				//判断是不是不符合规则的邮箱
				DictCodeDto dict = dictService.getCode("IP_INVALIDE_MAIL_DOMAIN", ems[1]);
				if(dict == null){
					partnerCheckingForm.setMailDomain(ems[1]);
				}
			}
		}
		model.addAttribute("partnerCheck", partnerCheckingForm);
		return "platform/partner/partnerCheckDetail.jsp";
	}
	
}
