package com.tem.platform.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.ResponseInfo;
import com.iplatform.common.utils.BigDecimalUtils;
import com.tem.payment.api.SubsidyService;
import com.tem.payment.condition.SubsidyCondition;
import com.tem.payment.dto.SubsidyDto;
import com.tem.platform.form.SubsidyForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 * @author jieming
 *
 */
@Controller
@RequestMapping("platform/subsidy")
public class SubsidyController extends BaseController {

	private final Logger logger = LoggerFactory.getLogger(SubsidyController.class);

	@Autowired
	private SubsidyService subsidyService;
	
	@RequestMapping(value ="index")
	public String index(Model model,Integer pageIndex) {
		if(pageIndex == null){
			pageIndex = 1;
		}

		SubsidyCondition condition = new SubsidyCondition();

		condition.setBpId(super.getPartnerId());

		condition.setTmcId(super.getTmcId());

		QueryDto<SubsidyCondition> queryDto = new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);

		queryDto.setCondition(condition);
		Page<SubsidyDto> pageList = subsidyService.queryListWithPage(queryDto);

		model.addAttribute("pageList",pageList);

		return "platform/subsidy/index.base";
	}

	@RequestMapping("getSubsidy")
	public String getSubsidy(Long id, Model model){
		if(id != null){
			SubsidyDto instance = subsidyService.getById(id);
			model.addAttribute("subsidy", instance);
		}

		model.addAttribute("tmcId", super.getTmcId());

		return "platform/subsidy/subsidy-model.jsp";
	}

	@ResponseBody
	@RequestMapping("save")
	public ResponseInfo save(SubsidyForm subsidyForm){


		SubsidyDto account = subsidyService.getByAccountId(subsidyForm.getAccountId());

		if(account != null && subsidyForm.getId() == null){
			return new ResponseInfo("1", "当前账户已有补贴，不能再次添加", null);
		}

		SubsidyDto subsidyDto = new SubsidyDto();

		subsidyDto.setId(subsidyForm.getId());
		subsidyDto.setTmcId(super.getTmcId());
		subsidyDto.setBpId(subsidyForm.getPartnerId());
		subsidyDto.setAccountId(subsidyForm.getAccountId());

		double countAmountDouble = subsidyForm.getCountAmount();

		int countAmountInt = BigDecimalUtils.multiply(new BigDecimal(countAmountDouble), 100).intValue();

		subsidyDto.setCountAmount(countAmountInt);
		subsidyDto.setRemainAmount(countAmountInt);
		subsidyDto.setUpdateUser(super.getCurrentUser().getId());
		subsidyDto.setUpdateDate(new Date());

		boolean isFlag = subsidyService.save(subsidyDto);

		if(isFlag){
			return new ResponseInfo("0","保存成功", isFlag);
		}else{
			return new ResponseInfo("1", "保存失败", isFlag);
		}
	}

	@ResponseBody
	@RequestMapping("delete")
	public ResponseInfo delete(Long id){

		boolean isFlag = subsidyService.deleteById(id);

		if(isFlag){
			return new ResponseInfo("0","删除成功", isFlag);
		}else{
			return new ResponseInfo("1", "删除失败", isFlag);
		}
	}
	
}
