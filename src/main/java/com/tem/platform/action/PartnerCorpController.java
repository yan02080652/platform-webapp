package com.tem.platform.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.web.Message;
import com.tem.payment.api.AccountManageService;
import com.tem.payment.dto.BpAccountDto;
import com.tem.platform.api.CorpCondition;
import com.tem.platform.api.DictService;
import com.tem.platform.api.PartnerCorpService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.dto.PartnerCorpDto;
import com.tem.platform.form.PartnerCorpForm;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * 企业基本信息维护
 * 
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("partner/corp")
public class PartnerCorpController extends BaseController {
	
	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private PartnerCorpService partnerCorpService;
	
	@Autowired
	private AccountManageService accountManageService;
	
	
	@RequestMapping(value="detail")
	public String detail(Model model, Long id){
		PartnerCorpDto partnerCorpDto=null;
		if(id!=null){
			partnerCorpDto = partnerCorpService.getById(id);
		}else{
			partnerCorpDto = new PartnerCorpDto();
			partnerCorpDto.setPartnerId(super.getPartnerId());
		}
		List<BpAccountDto> bpAccountDtos = accountManageService.getAllByBpId(partnerCorpDto.getPartnerId());
		String partnerName = partnerService.getNameById(partnerCorpDto.getPartnerId());

		PartnerCorpForm partnerCorpForm = TransformUtils.transform(partnerCorpDto, PartnerCorpForm.class);
		if(partnerCorpForm.getPaymentAccount() != null){
			BpAccountDto bpAccountDto = accountManageService.getById(partnerCorpForm.getPaymentAccount());
			if(bpAccountDto != null){
				partnerCorpForm.setPaymentAccountName(bpAccountDto.getAccountName());
			}
		}
		model.addAttribute("partnerCorpDto", partnerCorpForm);
		model.addAttribute("bpAccounts", bpAccountDtos);
		model.addAttribute("partnerName", partnerName);
		return "platform/partner/partnerCorpDetail.jsp";
	}
	
	@RequestMapping(value="add")
	public String add(Model model){
		PartnerCorpDto partnerCorpDto =new PartnerCorpDto();

		Long partnerId = getLongValue("partnerId");

		partnerCorpDto.setPartnerId(partnerId);

		List<BpAccountDto> bpAccountDtos = accountManageService.getAllByBpId(partnerCorpDto.getPartnerId());

		model.addAttribute("bpAccounts", bpAccountDtos);

		model.addAttribute("partnerCorpDto", partnerCorpDto);
		return "platform/partner/partnerCorpDetail.base";
	}

	@ResponseBody
	@RequestMapping(value="getAccounts")
	public List<BpAccountDto> getAccounts(Long partnerId ){
		if ( partnerId == null ) {
			partnerId = super.getPartnerId();
		}
		List<BpAccountDto> bpAccountDtos = accountManageService.getAllByBpId(partnerId);
		return bpAccountDtos;
	}
	
	@RequestMapping(value="getPartnerCorpList")
	public String getPartnerCorpList(Model model){
		String partnerId = super.getStringValue("partnerId");
		model.addAttribute("partnerId", partnerId);
		return "platform/partner/partnerCorpList.base";
	}

	@RequestMapping(value = "getCorpList")
	public String getCorpList(Model model,CorpCondition corpCondition,Integer pageIndex ) {

		if (!super.isSuperAdmin() ) {
			corpCondition.setTmcId(super.getTmcId());
		}else {
			corpCondition.setTmcId(null);
		}

		if ( pageIndex == null ) {
			pageIndex = 1;
		}
		QueryDto<CorpCondition> queryDto = new QueryDto<>(pageIndex,Page.DEFAULT_PAGE_SIZE);
		queryDto.setCondition(corpCondition);

		//得到法人公司与发票信息
		Page<PartnerCorpDto> corpDtoPage = partnerCorpService.queryListWithPage(queryDto);

		List<PartnerCorpForm> partnerCorpForms = new ArrayList<PartnerCorpForm>();
		if(CollectionUtils.isNotEmpty(corpDtoPage.getList())){
			PartnerCorpForm partnerCorpForm = new PartnerCorpForm();
			for(PartnerCorpDto partnerCorpDto : corpDtoPage.getList()){
				partnerCorpForm = TransformUtils.transform(partnerCorpDto, PartnerCorpForm.class);
				if(partnerCorpForm.getPaymentAccount() != null){
					BpAccountDto bpAccountDto = accountManageService.getById(partnerCorpForm.getPaymentAccount());
					if(bpAccountDto != null){
						partnerCorpForm.setPaymentAccountName(bpAccountDto.getAccountName());
					}
				}
				String partnerName = partnerService.getNameById(partnerCorpDto.getPartnerId());
				partnerCorpForm.setPartnerName(partnerName);

				partnerCorpForms.add(partnerCorpForm);
			}
		}
		Page<PartnerCorpForm> pageList = new Page<>(corpDtoPage.getStart(), corpDtoPage.getSize(), partnerCorpForms, corpDtoPage.getTotal());
		model.addAttribute("pageList", pageList);
		return "platform/partner/partnerCorpPageList.jsp";
	}
	
	@ResponseBody
	@RequestMapping(value="saveOrUpdateCorp",method=RequestMethod.POST)
	public	Message	saveOrUpdateCorp(Model model,PartnerCorpDto partnerCorpDto){
		if(new Long(-1).equals(partnerCorpDto.getPaymentAccount())){
			partnerCorpDto.setPaymentAccount(null);
		}

		partnerCorpDto.setTmcId(String.valueOf(super.getTmcId()));

		if(partnerCorpService.save(partnerCorpDto)){
			return	Message.success("操作成功");
		}else{
			return	Message.error("操作失败");
		}
	}
	
	@ResponseBody
	@RequestMapping(value="deleteCorp",method=RequestMethod.POST)
	public	Message	deleteCorp(Model model,@RequestParam(required=false) Long id){
		if(partnerCorpService.deleteById(id)){
			return	Message.success("操作成功");
		}else{
			return	Message.error("操作失败");
		}
	}
	
	
}
