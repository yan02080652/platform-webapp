package com.tem.platform.form;

import java.io.Serializable;

public class RoleGroupForm implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private Long partnerId;
	//角色id
	private Long roleId;
	//类型
	private String authtypeCode;
	//授权组
	private Long groupId;
	//子字段编码
	private String fieldId;
	//子字段值
	private String value;
	//活动
	private String activity;
	//活动参考值
	private String activitys;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public String getAuthtypeCode() {
		return authtypeCode;
	}
	public void setAuthtypeCode(String authtypeCode) {
		this.authtypeCode = authtypeCode;
	}
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public String getFieldId() {
		return fieldId;
	}
	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getActivity() {
		return activity;
	}
	public void setActivity(String activity) {
		this.activity = activity;
	}
	public String getActivitys() {
		return activitys;
	}
	public void setActivitys(String activitys) {
		this.activitys = activitys;
	}

	

}
