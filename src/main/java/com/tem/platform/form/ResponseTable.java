package com.tem.platform.form;

import com.iplatform.common.Page;
import com.iplatform.common.ResponseInfo;

import java.util.List;

/**
 * 返回表格分页信息
 * Created by pyy on 2017/11/6.
 */
public class ResponseTable extends ResponseInfo{

    private Integer statusCode;

    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public ResponseTable() {
        super();
    }

    public ResponseTable(Page page) {
        this(page,0,"ok");
    }

    public ResponseTable(Page page,Integer statusCode,String message) {
        this.setMsg(message);
        this.setStatusCode(statusCode);
        if (page != null) {
            this.setData(page.getList());
            this.count = page.getTotal();
        }
    }
}
