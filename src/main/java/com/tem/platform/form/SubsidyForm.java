package com.tem.platform.form;

import java.io.Serializable;

public class SubsidyForm implements Serializable{

    private Long id;

    /**
     * 企业Id
     */
    private Long partnerId;

    /**
     * 账号Id
     */
    private Long accountId;

    /**
     * 补贴累计金额
     */
    private double countAmount;

    /**
     * 未清账可用金额
     */
    private double remainAmount;


    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public double getCountAmount() {
        return countAmount;
    }

    public void setCountAmount(double countAmount) {
        this.countAmount = countAmount;
    }

    public double getRemainAmount() {
        return remainAmount;
    }

    public void setRemainAmount(double remainAmount) {
        this.remainAmount = remainAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
