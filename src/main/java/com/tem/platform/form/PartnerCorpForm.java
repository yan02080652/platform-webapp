package com.tem.platform.form;

import java.io.Serializable;

public class PartnerCorpForm implements Serializable{
    
	private static final long serialVersionUID = 1L;

	private Long id;

    private Long partnerId;

    private String partnerName;

    /**
     * 发票抬头
     */
    private String invoiceTitle;

    /**
     * 公司地址
     */
    private String address;

    /**
     * 公司电话
     */
    private String telephone;

    /**
     * 社会信用代码
     */
    private String socialCreditCode;

    /**
     * 开户行
     */
    private String accountBank;

    /**
     * 开户账号
     */
    private String accountNumber;
    
    /**
     * 企业支付账户
     */
    private Long paymentAccount;
    
    private String paymentAccountName;

    public String getPartnerName() {
        return partnerName;
    }

    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(Long partnerId) {
        this.partnerId = partnerId;
    }

    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle == null ? null : invoiceTitle.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone == null ? null : telephone.trim();
    }

    public String getSocialCreditCode() {
        return socialCreditCode;
    }

    public void setSocialCreditCode(String socialCreditCode) {
        this.socialCreditCode = socialCreditCode == null ? null : socialCreditCode.trim();
    }

    public String getAccountBank() {
        return accountBank;
    }

    public void setAccountBank(String accountBank) {
        this.accountBank = accountBank == null ? null : accountBank.trim();
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber == null ? null : accountNumber.trim();
    }

	public Long getPaymentAccount() {
		return paymentAccount;
	}

	public void setPaymentAccount(Long paymentAccount) {
		this.paymentAccount = paymentAccount;
	}

	public String getPaymentAccountName() {
		return paymentAccountName;
	}

	public void setPaymentAccountName(String paymentAccountName) {
		this.paymentAccountName = paymentAccountName;
	}
    
}