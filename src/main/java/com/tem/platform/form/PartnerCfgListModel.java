package com.tem.platform.form;

import java.util.List;

/**
 *
 * @author chenjieming
 * @date 2018/3/19
 */
public class PartnerCfgListModel {

    List<PartnerCfgForm> cfgList;

    public List<PartnerCfgForm> getCfgList() {
        return cfgList;
    }

    public void setCfgList(List<PartnerCfgForm> cfgList) {
        this.cfgList = cfgList;
    }

}
