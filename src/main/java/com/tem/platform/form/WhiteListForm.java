package com.tem.platform.form;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author zhangtao
 *
 */
public class WhiteListForm implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private Long bpId;
	
	private String bpName;

    private String ip;

    private String name;

    private Date effectTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBpId() {
		return bpId;
	}

	public void setBpId(Long bpId) {
		this.bpId = bpId;
	}

	public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Date getEffectTime() {
        return effectTime;
    }

    public void setEffectTime(Date effectTime) {
        this.effectTime = effectTime;
    }

	public String getBpName() {
		return bpName;
	}

	public void setBpName(String bpName) {
		this.bpName = bpName;
	}
    
    
}