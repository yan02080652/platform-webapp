package com.tem.platform.form;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 灰度配置
 * @author chenjieming
 */
public class GrayRuleForm implements Serializable {

    private Long id;

    /**
     * 生效范围类型:生效范围类型，取值：PARTNER-企业，USER-用户,ALL-所有人
     */
    private String scopeType;

    /**
     * 生效范围值
     */
    private Long scopeValue;

    /**
     * 生效范围值的名称
     */
    private String scopeValueName;

    /**
     * 生效状态，0-访问灰度环境; 1-不访问灰度环境
     */
    private Integer status;

    /**
     * 有效期开始
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date validFrom;

    /**
     * 有效期结束
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date validTo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getScopeType() {
        return scopeType;
    }

    public void setScopeType(String scopeType) {
        this.scopeType = scopeType == null ? null : scopeType.trim();
    }

    public Long getScopeValue() {
        return scopeValue;
    }

    public void setScopeValue(Long scopeValue) {
        this.scopeValue = scopeValue;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public String getScopeValueName() {
        return scopeValueName;
    }

    public void setScopeValueName(String scopeValueName) {
        this.scopeValueName = scopeValueName;
    }
}