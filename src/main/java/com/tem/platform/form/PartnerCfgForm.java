package com.tem.platform.form;

/**
 *
 * @author chenjieming
 * @date 2018/3/19
 */
public class PartnerCfgForm {

    private String detailid;

    private Long id;

    private String cfgItemValue;

    private String dictitemCode;

    public String getDetailid() {
        return detailid;
    }

    public void setDetailid(String detailid) {
        this.detailid = detailid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCfgItemValue() {
        return cfgItemValue;
    }

    public void setCfgItemValue(String cfgItemValue) {
        this.cfgItemValue = cfgItemValue;
    }

    public String getDictitemCode() {
        return dictitemCode;
    }

    public void setDictitemCode(String dictitemCode) {
        this.dictitemCode = dictitemCode;
    }
}
