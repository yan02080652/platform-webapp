package com.tem.order.util;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iplatform.common.utils.LogUtils;

public class ExportExcel {

		private final Logger logger = LoggerFactory.getLogger(ExportExcel.class);

		private static Pattern NUMBER_PATTERN = Pattern.compile("^((-[1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$");

		private static Pattern NUMBER_PATTERN_1 = Pattern.compile("^(([1-9]{1}\\d*)|([0]{1}))(\\.(\\d){0,2})?$");

		public <T> void exportExcel(List<ExportInfo> exportInfoList,
									OutputStream out, String pattern) throws IOException {
			XSSFWorkbook workbook = new XSSFWorkbook();

	        for (ExportInfo exportInfo : exportInfoList) {
				String title = exportInfo.getTitle();
				Collection<T> dataSet = (Collection<T>) exportInfo.getDataset();
				String[] headers = exportInfo.getHeaders();
				
				XSSFSheet sheet = workbook.createSheet(title);
				createSheet(headers, dataSet, pattern, workbook, sheet);
			}
	        
	        try {
	            workbook.write(out);  
	        }
	        catch (IOException e) {
	            e.printStackTrace();  
	        }
	    }

		private <T> void createSheet(String[] headers, Collection<T> dataSet, String pattern,
									 XSSFWorkbook workbook, XSSFSheet sheet){

			//表头样式
	        XSSFCellStyle style = workbook.createCellStyle();
	        style.setFillForegroundColor(new XSSFColor(Color.WHITE));
			XSSFFont fontStyle = workbook.createFont();
			fontStyle.setFontName("黑体");
			fontStyle.setFontHeightInPoints((short)14);
	        style.setFont(fontStyle);

	        //内容样式
			XSSFCellStyle style1 = workbook.createCellStyle();
			style1.setFillForegroundColor(new XSSFColor(Color.WHITE));

			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
	  
	        //产生表格标题行
			Row row = sheet.createRow(0);
			for (short i = 0; i < headers.length; i++) {
				Cell cell = row.createCell(i);
				cell.setCellStyle(style);  
	            XSSFRichTextString text = new XSSFRichTextString(headers[i]);  
	            cell.setCellValue(text);  
	        }
			
			Field[] fields = null;
			if(dataSet.size() > 0) {
				fields = dataSet.iterator().next().getClass().getDeclaredFields();
			}
			
			String[] extraArr = new String[]{"orderId","originOrderId","orderNo","id","passengerCount",
					"pnr","titckNos","externalOrderId","supName","relevantOrders","preOrderId","insuranceId","policyNo", "billId","costCenterName"};
			Set<String> list= new HashSet<String>(Arrays.asList(extraArr));
	  
	        Iterator<T> it = dataSet.iterator();
	        int index = 0;

	        long begin = System.currentTimeMillis();
	        while (it.hasNext()) {
	        	long begin1 = System.currentTimeMillis();

	            index++;  
	            row = sheet.createRow(index);  
	            T t = (T) it.next();  
	            
	            for (short i = 0; i < fields.length; i++) {
	            	
					Cell cell = row.createCell(i);
					long end1 = System.currentTimeMillis();
					//LogUtils.debug(logger,"一cell耗时：{}",end1 - begin1);
	                cell.setCellStyle(style1);
	                Field field = fields[i];  
	                
	                try  
	                {
						String fieldName = field.getName();

						Object value = field.get(t);

	                    String textValue = null;
	                    if (value instanceof Date) {
	                        Date date = (Date) value;
	                        textValue = sdf.format(date);  
	                    }else{
	                    	if(value != null ){
	                    		textValue = value.toString();
	                    	}else{
	                    		textValue = null;
	                    	}
	                    }
	                    
	                    if("id".equals(fieldName)){
	                    	textValue = (index) + "";
		                }
	                    
	                    //利用正则表达式判断textValue是否全部由数字组成
	                    if (textValue != null)  
	                    {
	                        Matcher matcher = NUMBER_PATTERN.matcher(textValue);
	                        
	                        Matcher matcher1 = NUMBER_PATTERN_1.matcher(textValue);
	                        
	                        boolean matcher2  = matcher.matches() || matcher1.matches();
	                        
	                        if (matcher2 && !list.contains(fieldName)) {
	                            //是数字当作double处理
	                            cell.setCellValue(Double.parseDouble(textValue));
	                        } else {
	                            cell.setCellValue(textValue);
	                        }
	                    }
	                } catch (Exception e) {
	                    e.printStackTrace();  
	                } finally {
	                    // 清理资源
	                }
	                //sheet.autoSizeColumn(i);
	            }
	        }
	        //设置每个sheet每一行的宽度,自动
	        if(fields != null){
	        	 for (short i = 0; i < fields.length; i++) {
	 	        	sheet.autoSizeColumn(i);
	 	        }
	        }
	        long end = System.currentTimeMillis();
			LogUtils.debug(logger,"所以excel赋值耗时：{}",end - begin);
	    }

}
