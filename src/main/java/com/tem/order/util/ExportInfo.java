package com.tem.order.util;

import java.io.Serializable;
import java.util.List;

public class ExportInfo<T> implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private String title;
	
	private	String[] headers;
	
	private	List<T> dataset;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String[] getHeaders() {
		return headers;
	}

	public void setHeaders(String[] headers) {
		this.headers = headers;
	}

	public List<T> getDataset() {
		return dataset;
	}

	public void setDataset(List<T> dataset) {
		this.dataset = dataset;
	}

	
}
