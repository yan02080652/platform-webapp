package com.tem.order.form;

import java.util.Date;

/**
 * zhangtao
 */
public class RefundOrderSummaryForm {

	public Integer id;
	
	/**
     * 退单号
     */
    public String orderId;
    
    /**
     * 原始订单号
     */
    public String preOrderId;
    
    /**
     * 退单类型
     */
    public String orderType;
    
    /**
     * 退款方式
     */
    public String paymentMethod;
    /**
     * 退款申请人
     */
    public String userName;
    /**
     * 退款申请时间
     */
    public Date createDate;
    
    /**
     * 所属客户
     */
    public String partnerName;
    /**
     * 退款金额
     */
    public Double totalAmount;
    
    /**
     * 供应商应退额
     */
    public Double supperRefundAmount;
    
    /**
     * 订单状态
     */
    public String orderState;
    /**
     * 客户结算账户ID
     */
    public Long billId;
    /**
	 * 成本中心名称
	 */
	public String costCenterName;
	
    /**
     * 供应商流水号
     */
    public String externalOrderId;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPreOrderId() {
		return preOrderId;
	}

	public void setPreOrderId(String preOrderId) {
		this.preOrderId = preOrderId;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getExternalOrderId() {
		return externalOrderId;
	}

	public void setExternalOrderId(String externalOrderId) {
		this.externalOrderId = externalOrderId;
	}

	public Double getSupperRefundAmount() {
		return supperRefundAmount;
	}

	public void setSupperRefundAmount(Double supperRefundAmount) {
		this.supperRefundAmount = supperRefundAmount;
	}

	public String getCostCenterName() {
		return costCenterName;
	}

	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}
	
}
