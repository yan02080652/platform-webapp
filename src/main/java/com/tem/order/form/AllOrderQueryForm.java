package com.tem.order.form;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.iplatform.common.utils.DateUtils;
import com.tem.flight.dto.order.FlightOrderCondition;
import com.tem.flight.dto.order.enums.DateType;
import com.tem.hotel.dto.order.HotelOrderCondition;
import com.tem.international.order.InternationCondition;
import com.tem.payment.enums.PayChannel;
import com.tem.product.condition.GeneralOrderCondition;
import com.tem.product.dto.insurance.InsuranceOrderCondition;
import com.tem.pss.dto.refund.RefundOrderCondition;
import com.tem.pss.enums.TravelTypeEnum;
import com.tem.train.dto.order.TrainOrderCondition;

/**
 * Created by chenjieming on 2017/8/1.
 */
public class AllOrderQueryForm {

    /**
     * 关键词
     */
    private String keyword;

    /**
     * 出票渠道
     */
    private String issueChannel;

    /**
     * 下单开始时间
     */
    private String startDate;

    /**
     * 下单结束时间
     */
    private String endDate;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 企业
     */
    private Long queryBpId;

    /**
     * 仅公司支付
     */
    private boolean isCompPay;

    /**
     * 退单业务类型
     */
    private String type;


    private String tmcId;
    
    /**
     * 出行性质
     */
    private TravelTypeEnum travelType;
    

	public TravelTypeEnum getTravelType() {
		return travelType;
	}

	public void setTravelType(TravelTypeEnum travelType) {
		this.travelType = travelType;
	}

	public String getTmcId() {
        return tmcId;
    }

    public void setTmcId(String tmcId) {
        this.tmcId = tmcId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getIssueChannel() {
        return issueChannel;
    }

    public void setIssueChannel(String issueChannel) {
        this.issueChannel = issueChannel;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Long getQueryBpId() {
        return queryBpId;
    }

    public void setQueryBpId(Long queryBpId) {
        this.queryBpId = queryBpId;
    }

    public boolean isCompPay() {
        return isCompPay;
    }

    public void setIsCompPay(boolean compPay) {
        isCompPay = compPay;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    //机票查询条件转换
    public FlightOrderCondition toFlightOrderCondition(){
        Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
        Date start = DateUtils.parse(startDate, DateUtils.YYYY_MM_DD);

        FlightOrderCondition condition = new FlightOrderCondition();
        condition.setDateType(DateType.CREATE_ORDER_DATE);
        condition.setStartDate(start);
        condition.setEndDate(end);
        if(StringUtils.isNotBlank(keyword)){
            condition.setOrderNumber(keyword);
        }
        if(StringUtils.isNotBlank(orderStatus) && !"0".equals(orderStatus)){
            condition.setOrderStatus(orderStatus.split(","));
        }
        if(travelType != null) {
        	condition.setTravelType(travelType);
        }
        if(StringUtils.isNotBlank(issueChannel) && !"0".equals(issueChannel)){
            condition.setIssueChannel(issueChannel);
        }
        if(queryBpId != null){
            condition.setPantnerId(queryBpId);
        }
        if(isCompPay){
            condition.setPaymentMethod(PayChannel.BP_ACCOUNT.name());
        }

        if (StringUtils.isNotEmpty(tmcId) ) {
            condition.setTmcId(Long.valueOf(tmcId));
        }
        return condition;
    }

    public TrainOrderCondition toTrainOrderCondition(){

        TrainOrderCondition condition = new TrainOrderCondition();
        Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
        Date start = DateUtils.parse(startDate, DateUtils.YYYY_MM_DD);
        condition.setDateType(com.tem.train.dto.order.enums.DateType.CREATE_ORDER_DATE);
        condition.setStartDate(start);
        condition.setEndDate(end);
        if(StringUtils.isNotBlank(keyword)){
            condition.setOrderNumber(keyword);
        }
        if(StringUtils.isNotBlank(orderStatus) && !"0".equals(orderStatus)){
            condition.setOrderShowStatus(orderStatus.split(","));
        }
        if(StringUtils.isNotBlank(issueChannel) && !"0".equals(issueChannel)){
            condition.setIssueChannel(issueChannel);
        }
        if(queryBpId != null){
            condition.setPartnerId(queryBpId);
        }
        if(isCompPay){
            condition.setPaymentMethod(PayChannel.BP_ACCOUNT.name());
        }

        if ( StringUtils.isNotEmpty(tmcId)  ) {
            condition.setTmcId(Long.valueOf(tmcId));
        }
        if(travelType != null) {
        	condition.setTravelType(travelType.name());
        }
        return condition;
    }

    public HotelOrderCondition toHotelOrderCondition(){
        HotelOrderCondition condition = new HotelOrderCondition();

        Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
        Date start = DateUtils.parse(startDate, DateUtils.YYYY_MM_DD);

        condition.setDateType("CREATE_ORDER_DATE");
        condition.setStart(start);
        condition.setEnd(end);

        if(StringUtils.isNotBlank(keyword)){
            condition.setOrderNumber(keyword);
        }
        if(StringUtils.isNotBlank(orderStatus) && !"0".equals(orderStatus)){
            String[] statusString = orderStatus.split(",");
            com.tem.hotel.dto.order.enums.OrderShowStatus[] status = new com.tem.hotel.dto.order.enums.OrderShowStatus[statusString.length];
            for(int i = 0 ; i < statusString.length ; i++){
                status[i] = com.tem.hotel.dto.order.enums.OrderShowStatus.valueOf(statusString[i]);
            }
            condition.setStatus(status);
        }
        if(StringUtils.isNotBlank(issueChannel) && !"0".equals(issueChannel)){
            condition.setIssueChannel(issueChannel);
        }
        if(queryBpId != null){
            condition.setPartnerId(queryBpId);
        }
        if(isCompPay){
            condition.setPaymentMethod(PayChannel.BP_ACCOUNT.name());
        }
        if ( StringUtils.isNotEmpty(tmcId)  ) {
            condition.setTmcId(tmcId);
        }
        if(travelType != null) {
        	condition.setTravelType(travelType);
        }
        return condition;
    }

    //退单
    public RefundOrderCondition toRefundOrderCondition(){

        Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
        Date start = DateUtils.parse(startDate, DateUtils.YYYY_MM_DD);
        RefundOrderCondition condition = new RefundOrderCondition();
        condition.setStartDate(start);
        condition.setEndDate(end);
        if(StringUtils.isNotBlank(keyword)){
            condition.setKeyword(keyword);
        }
        if(StringUtils.isNotBlank(orderStatus) && !"0".equals(orderStatus)){
            condition.setStatus(orderStatus.split(","));
        }
        if(StringUtils.isNotEmpty(type)&&!"0".equals(type)){
            condition.setOrderBizType(type);
        }
        if(queryBpId != null){
            condition.setPartnerId(queryBpId);
        }
        if(isCompPay){
            condition.setRefundChannel(PayChannel.BP_ACCOUNT.name());
        }
        if ( StringUtils.isNotEmpty(tmcId) ) {
            condition.setTmcId(tmcId);
        }
        if(travelType != null) {
        	condition.setTravelType(travelType);
        }
       
        return condition;
    }

    public GeneralOrderCondition toGeneralOrderCondition(){
        GeneralOrderCondition condition = new GeneralOrderCondition();

        Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
        Date start = DateUtils.parse(startDate, DateUtils.YYYY_MM_DD);

        condition.setFormDate(start);
        condition.setToDate(end);

        if(StringUtils.isNotBlank(keyword)){
            condition.setKeyword(keyword);
        }
        if(StringUtils.isNotBlank(orderStatus) && !"0".equals(orderStatus)){
            condition.setOrderStatus(orderStatus.split(","));
        }
        if(queryBpId != null){
            condition.setcId(queryBpId.toString());
        }
        if(StringUtils.isNotBlank(issueChannel) && !"0".equals(issueChannel)){
            condition.setIssueChannel(issueChannel);
        }
        if(isCompPay){
            condition.setPaymentMethod(PayChannel.BP_ACCOUNT.name());
        }
        if ( StringUtils.isNotEmpty(tmcId)  ) {
            condition.setTmcId(Long.valueOf(tmcId));
        }
        if(travelType != null) {
        	condition.setTravelType(travelType);
        }
        
        return condition;
    }

    public InsuranceOrderCondition toInsuranceOrderCondition(){
        Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
        Date start = DateUtils.parse(startDate, DateUtils.YYYY_MM_DD);

        InsuranceOrderCondition condition = new InsuranceOrderCondition();
        condition.setStartDate(start);
        condition.setEndDate(end);
        if(StringUtils.isNotBlank(keyword)){
            condition.setId(keyword);
        }
        if(StringUtils.isNotBlank(orderStatus) && !"0".equals(orderStatus)){
            condition.setOrderStatus(orderStatus.split(","));
        }
        if(StringUtils.isNotBlank(issueChannel) && !"0".equals(issueChannel)){//供应商
            condition.setProviderCode(issueChannel);
        }
        if(queryBpId != null){
            condition.setPaternerId(queryBpId.toString());
        }
        if(isCompPay){
            condition.setPaymentType(PayChannel.BP_ACCOUNT.name());
        }
        if ( StringUtils.isNotEmpty(tmcId)  ) {
            condition.setTmcId(tmcId);
        }
        if(travelType != null) {
        	condition.setTravelType(travelType);
        }
        return condition;
    }

    public InternationCondition toInternationalConditon(){

        InternationCondition condition = new InternationCondition();
        Date end  = DateUtils.parse(endDate+" 23:59:59", DateUtils.YYYY_MM_DD_HH_MM_SS);
        Date start = DateUtils.parse(startDate, DateUtils.YYYY_MM_DD);

        condition.setDateType(DateType.CREATE_ORDER_DATE);
        condition.setStartDate(start);
        condition.setEndDate(end);
        if(StringUtils.isNotBlank(keyword)){
            condition.setOrderNumber(keyword);
        }
        if(StringUtils.isNotBlank(orderStatus) && !"0".equals(orderStatus)){
            condition.setOrderStatus(orderStatus.split(","));
        }
        if(StringUtils.isNotBlank(issueChannel) && !"0".equals(issueChannel)){
            condition.setIssueChannel(issueChannel);
        }
        if(queryBpId != null){
            condition.setPantnerId(queryBpId);
        }
        if(isCompPay){
            condition.setPaymentMethod(PayChannel.BP_ACCOUNT.name());
        }

        if ( StringUtils.isNotEmpty(tmcId) ) {
            condition.setTmcId(Long.valueOf(tmcId));
        }
        if(travelType != null) {
        	condition.setTravelType(travelType);
        }
        return condition;
    }
}
