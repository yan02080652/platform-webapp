package com.tem.order.form;

import java.util.Date;

/**
 * zhangtao
 */
public class FlightOrderSummaryForm {

	public Integer id;
	
	/**
     * 订单号
     */
    public String orderId;
    
    /**
     * 支付方式
     */
    public String paymentMethod;
    /**
     * 预订人
     */
    public String userName;
    /**
     * 预订时间
     */
    public Date createDate;
    /**
     * 乘机人
     */
    public String passengers;
    /**
     * 乘机人数
     */
    public Integer passengerCount;
   
    /**
     * 航司
     */
    public String carrierName;
    /**
     * 舱位名称
     */
    public String berth;
    /**
     * 航班号
     */
    public String flightNo;
    /**
     * 起飞时间
     */
    public Date fromDate;
    
    /**
     * 行程
     */
    public String trip;
    
    /**
     * 所属客户
     */
    public String partnerName;
    /**
     * 客户结算价
     */
    public Double totalAmount;
    
    /**
     * 所属供应商
     */
    public String supName;
    /**
     * 供应商结算价
     */
    public Double supSettleAmount;
    
    /**
     * 订单状态
     */
    public String orderState;
    
    /**
     * 相关的订单
     */
    public String relevantOrders;
    
    /**
     * 供应商订单号
     */
    public String externalOrderId;
    /**
     * pnr
     */
    public String pnr;
    
    /**
     * 票号
     */
    public String titckNos;
    
    
    /**
     * 客户账单ID
     */
    public Long billId;
   
    /**
     * 成本中心名称
     */
    public String costCenterName;
    
    /**
     * 大客户协议编码
     */
    public String customerCode;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getPassengers() {
        return passengers;
    }

    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    public Integer getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(Integer passengerCount) {
        this.passengerCount = passengerCount;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getSupSettleAmount() {
		return supSettleAmount;
	}

	public void setSupSettleAmount(Double supSettleAmount) {
		this.supSettleAmount = supSettleAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getExternalOrderId() {
		return externalOrderId;
	}

	public void setExternalOrderId(String externalOrderId) {
		this.externalOrderId = externalOrderId;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getTitckNos() {
		return titckNos;
	}

	public void setTitckNos(String titckNos) {
		this.titckNos = titckNos;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getSupName() {
		return supName;
	}

	public void setSupName(String supName) {
		this.supName = supName;
	}

	public String getRelevantOrders() {
		return relevantOrders;
	}

	public void setRelevantOrders(String relevantOrders) {
		this.relevantOrders = relevantOrders;
	}

	public String getBerth() {
		return berth;
	}

	public void setBerth(String berth) {
		this.berth = berth;
	}

	public String getTrip() {
		return trip;
	}

	public void setTrip(String trip) {
		this.trip = trip;
	}

    public String getCostCenterName() {
        return costCenterName;
    }

    public void setCostCenterName(String costCenterName) {
        this.costCenterName = costCenterName;
    }

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	
    
}
