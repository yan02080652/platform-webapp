package com.tem.order.form;

import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.flight.dto.order.PassengerDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单主表 仅仅做页面交互使用
 * @author zhangtao
 *
 */
public class OrderForm implements Serializable {
   
	private static final long serialVersionUID = 1L;
	
	/**
	 * 订单日期
	 */
	private Date orderDate;
	
	/**
	 * 订单编号
	 */
	private String orderNo;
	
	/**
	 * 订单类型 FLIGHT机票 HOTEL酒店 TRAIN火车票 INSURANCE保险 REFUND退票
	 */
	private String orderType;
	
	/**
	 * 订单类型
	 */
	private String orderTypeName;
	
	/**
	 * 申请人
	 */
	private String applyUserName;
	
	/**
	 * 所属客户
	 */
	private String partnerName;
	
	/**
	 * 订单金额 客户结算价
	 */
	private Double orderPrice;
	
	/**
	 * 所属供应商
	 */
	private String supName;
	
	/**
	 * 供应商结算价
	 */
	private Double supSettlePrice;
	
	/**
	 * 订单状态
	 */
	private String statusName;
	
	/**
	 * 订单摘要
	 */
	private String desc;
	
	/**
     * 支付方式
     */
    private String paymentMethod;
    
    /**
     * 乘机人
     */
    private String passengers;
    /**
     * 乘机人数
     */
    private Integer passengerCount;
   
    /**
     * 航司
     */
    private String carrierName;
    /**
     * 舱位名称
     */
    private String berth;
    /**
     * 航班号
     */
    private String flightNo;
    /**
     * 起飞时间
     */
    private Date fromDate;
    
    /**
     * 行程
     */
    private String trip;
    
    /**
     * 相关的订单
     */
    private String relevantOrders;
    
    /**
     * 供应商订单号
     */
    private String externalOrderId;
    
    /**
     * pnr
     */
    private String pnr;
    
    /**
     * 票号
     */
    private String titckNos;
    
    /**
     * 所属订单ID 保险上用的
     */
	private Long preOrderId;
	
	/**
     * 保单生效日期
     */
    private Date effectiveDate;
    /**
     * 保单失效日期
     */
    private Date expireDate;
    
    /**
     * 酒店名称
     */
    private String hotelName;

	/**
	 * 成本中心名称
	 */
	public String costCenterName;

	/**
	 * 所对应的退单
	 */
	private RefundOrderForm refundOrderForm;
	
	public OrderForm() {
		super();
	}

	public OrderForm(FlightOrderDto flightOrderDto) {
		this.orderDate = flightOrderDto.getCreateDate();
		this.orderNo = flightOrderDto.getId(); 
		this.orderType = "FLIGHT";
		this.orderTypeName = "机票订单";
		this.applyUserName = flightOrderDto.getUserName();
		this.partnerName = flightOrderDto.getcName();
		if(flightOrderDto.getOrderFee() != null && flightOrderDto.getOrderFee().getTotalOrderPrice() != null){
			this.orderPrice = flightOrderDto.getOrderFee().getTotalOrderPrice().doubleValue();
			this.supSettlePrice = flightOrderDto.getOrderFee().getTotalSettlePrice().doubleValue();
		}else{
			this.supSettlePrice = 0d;
		}
		//this.supName = flightOrderDto.getIssueChannel();
		this.statusName = flightOrderDto.getOrderShowStatus().getMessage();
		this.desc = flightOrderDto.getSummary();
		//this.paymentMethod = flightOrderDto.getPaymentMethod();
		
		
		String passengers = "";
		String titckNos = "";
		if(CollectionUtils.isNotEmpty(flightOrderDto.getPassengers())){
			this.passengerCount = flightOrderDto.getPassengers().size();
			for(PassengerDto passengerDto : flightOrderDto.getPassengers()){
				if(StringUtils.isNotEmpty(passengerDto.getName())){
					passengers = passengers + passengerDto.getName() + "、";
				}
				if(StringUtils.isNotEmpty(passengerDto.getTicketNo())){
					titckNos = titckNos + passengerDto.getTicketNo() + "、";
				}
			}
			if(StringUtils.isNotEmpty(titckNos)){
				titckNos = titckNos.substring(0,titckNos.length()-1);
			}
			if(StringUtils.isNotEmpty(passengers)){
				passengers = passengers.substring(0,passengers.length()-1);
			}
			
		}
		this.titckNos = titckNos;
		this.passengers = passengers;
		if(CollectionUtils.isNotEmpty(flightOrderDto.getOrderFlights())){
			this.carrierName = flightOrderDto.getOrderFlights().get(0).getCarrierName();
			this.flightNo = flightOrderDto.getOrderFlights().get(0).getFlightNo();
			this.fromDate = flightOrderDto.getOrderFlights().get(0).getFromDate();
			this.trip = flightOrderDto.getOrderFlights().get(0).getFromCityName() + "-" + flightOrderDto.getOrderFlights().get(0).getToCityName();
		}
	   //this.berth = "";
	   this.externalOrderId = flightOrderDto.getExternalOrderId();
	   this.pnr = flightOrderDto.getPnrCode();
		
	}
	
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getApplyUserName() {
		return applyUserName;
	}

	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}

	public Double getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(Double orderPrice) {
		this.orderPrice = orderPrice;
	}

	public String getOrderTypeName() {
		return orderTypeName;
	}

	public void setOrderTypeName(String orderTypeName) {
		this.orderTypeName = orderTypeName;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public RefundOrderForm getRefundOrderForm() {
		return refundOrderForm;
	}

	public void setRefundOrderForm(RefundOrderForm refundOrderForm) {
		this.refundOrderForm = refundOrderForm;
	}
	
	

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getSupName() {
		return supName;
	}

	public void setSupName(String supName) {
		this.supName = supName;
	}

	public Double getSupSettlePrice() {
		return supSettlePrice;
	}

	public void setSupSettlePrice(Double supSettlePrice) {
		this.supSettlePrice = supSettlePrice;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getPassengers() {
		return passengers;
	}

	public void setPassengers(String passengers) {
		this.passengers = passengers;
	}

	public Integer getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(Integer passengerCount) {
		this.passengerCount = passengerCount;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getBerth() {
		return berth;
	}

	public void setBerth(String berth) {
		this.berth = berth;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public String getTrip() {
		return trip;
	}

	public void setTrip(String trip) {
		this.trip = trip;
	}

	public String getRelevantOrders() {
		return relevantOrders;
	}

	public void setRelevantOrders(String relevantOrders) {
		this.relevantOrders = relevantOrders;
	}

	public String getExternalOrderId() {
		return externalOrderId;
	}

	public void setExternalOrderId(String externalOrderId) {
		this.externalOrderId = externalOrderId;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getTitckNos() {
		return titckNos;
	}

	public void setTitckNos(String titckNos) {
		this.titckNos = titckNos;
	}

	public Long getPreOrderId() {
		return preOrderId;
	}

	public void setPreOrderId(Long preOrderId) {
		this.preOrderId = preOrderId;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public int getSize(){
		if(this.refundOrderForm != null){
			return 2;
		}else{
			return 1;
		}
	}

	public String getCostCenterName() {
		return costCenterName;
	}

	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}
}