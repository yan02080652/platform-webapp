package com.tem.order.form;

import java.util.Date;

/**
 * zhangtao
 */
public class GeneralOrderSummaryForm {

	public Integer id;
	
	/**
     * 订单号
     */
    public String orderId;
    
    /**
     * 支付方式
     */
    public String paymentMethod;
    /**
     * 预订人
     */
    public String userName;
    /**
     * 申请时间
     */
    public Date createDate;
    /**
     * 出行人数
     */
    public Integer passengerCount;
   
    /**
     * 出行时间
     */
    public String fromDate;
    
    /**
     * 所属客户
     */
    public String partnerName;
    /**
     * 客户结算价
     */
    public Double totalAmount;
    
    /**
     * 所属供应商
     */
    public String supName;
    /**
     * 供应商结算价
     */
    public Double supSettleAmount;
    
    /**
     * 订单状态
     */
    public String orderState;
    
    /**
     * 客户账单ID
     */
    public Long billId;
    
    /**
     * 供应商订单号
     */
    public String externalOrderId;

    /**
     * 成本中心名称
     */
    public String costCenterName;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(Integer passengerCount) {
        this.passengerCount = passengerCount;
    }

	public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getSupSettleAmount() {
		return supSettleAmount;
	}

	public void setSupSettleAmount(Double supSettleAmount) {
		this.supSettleAmount = supSettleAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getExternalOrderId() {
		return externalOrderId;
	}

	public void setExternalOrderId(String externalOrderId) {
		this.externalOrderId = externalOrderId;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getSupName() {
		return supName;
	}

	public void setSupName(String supName) {
		this.supName = supName;
	}

    public String getCostCenterName() {
        return costCenterName;
    }

    public void setCostCenterName(String costCenterName) {
        this.costCenterName = costCenterName;
    }

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}
    
}
