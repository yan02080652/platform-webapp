package com.tem.order.form;

import java.util.Date;

/**
 * zhangtao
 */
public class TrainOrderSummaryForm {

	public Integer id;
	
	/**
     * 订单号
     */
    public String orderId;
    /**
     * 原始订单号
     */
    public String originOrderId;
    
    /**
     * 订单类型：出票订单 改签订单  退票订单
     */
    public String orderShowType;
    /**
     * 支付方式
     */
    public String paymentMethod;
    /**
     * 预订人
     */
    public String userName;
    /**
     * 预订时间
     */
    public Date createDate;
    /**
     * 出行人
     */
    public String passengers;
    /**
     * 出行人数
     */
    public Integer passengerCount;
    
    /**
     * 行程
     */
    public String trip;
   
    /**
     * 车次号
     */
    public String trainNo;
    /**
     * 出发时间
     */
    public Date fromDate;
    
    /**
     * 所属客户
     */
    public String partnerName;
    /**
     * 客户结算价
     */
    public Double totalAmount;
    
    /**
     * 所属供应商
     */
    public String supName;
    /**
     * 供应商结算价
     */
    public Double supSettleAmount;
    
    /**
     * 订单状态
     */
    public String orderState;
    /**
     * 客户账单ID
     */
    public Long billId;
    
    /**
     * 供应商订单号
     */
    public String externalOrderId;
    
    /**
     * 票号
     */
    public String titckNos;

    /**
     * 成本中心名称
     */
    public String costCenterName;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getPassengers() {
        return passengers;
    }

    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    public Integer getPassengerCount() {
        return passengerCount;
    }

    public void setPassengerCount(Integer passengerCount) {
        this.passengerCount = passengerCount;
    }


    public String getTrainNo() {
		return trainNo;
	}

	public void setTrainNo(String trainNo) {
		this.trainNo = trainNo;
	}

	public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Double getSupSettleAmount() {
		return supSettleAmount;
	}

	public void setSupSettleAmount(Double supSettleAmount) {
		this.supSettleAmount = supSettleAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getExternalOrderId() {
		return externalOrderId;
	}

	public void setExternalOrderId(String externalOrderId) {
		this.externalOrderId = externalOrderId;
	}

	public String getTitckNos() {
		return titckNos;
	}

	public void setTitckNos(String titckNos) {
		this.titckNos = titckNos;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getSupName() {
		return supName;
	}

	public void setSupName(String supName) {
		this.supName = supName;
	}

	public String getTrip() {
		return trip;
	}

	public void setTrip(String trip) {
		this.trip = trip;
	}

    public String getCostCenterName() {
        return costCenterName;
    }

    public void setCostCenterName(String costCenterName) {
        this.costCenterName = costCenterName;
    }

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public String getOriginOrderId() {
		return originOrderId;
	}

	public void setOriginOrderId(String originOrderId) {
		this.originOrderId = originOrderId;
	}

	public String getOrderShowType() {
		return orderShowType;
	}

	public void setOrderShowType(String orderShowType) {
		this.orderShowType = orderShowType;
	}
    
}
