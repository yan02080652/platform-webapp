package com.tem.order.form;

import java.util.Date;


/**
 * 
 */
public class CarOrderSummaryForm {
	
	/**
     * 订单号
     */
    public String orderId;
    /**
     * 行程情况
     */
    public String tripInfo;
    /**
     * 预订人
     */
    public String userName;
    /**
     * 下单时间
     */
    public Date createDate;
    /**
     *所属客户
     */
    public String company;
    /**
     * 客户结算价
     */
    public String totalAmount;
    
    /**
     * 所属供应商
     */
    public String originType;
    /**
     * 供应商结算价
     */
    public String totalSettlePrice;
    
    /**
     * 状态
     */
    public String orderShowStatus;


	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTripInfo() {
		return tripInfo;
	}

	public void setTripInfo(String tripInfo) {
		this.tripInfo = tripInfo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getOriginType() {
		return originType;
	}

	public void setOriginType(String originType) {
		this.originType = originType;
	}

	public String getTotalSettlePrice() {
		return totalSettlePrice;
	}

	public void setTotalSettlePrice(String totalSettlePrice) {
		this.totalSettlePrice = totalSettlePrice;
	}

	public String getOrderShowStatus() {
		return orderShowStatus;
	}

	public void setOrderShowStatus(String orderShowStatus) {
		this.orderShowStatus = orderShowStatus;
	}
    
    
    
}
