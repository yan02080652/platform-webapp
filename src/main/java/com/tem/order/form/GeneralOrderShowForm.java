package com.tem.order.form;

/**
 * zhangtao
 */
public class GeneralOrderShowForm extends GeneralOrderSummaryForm {

    public boolean isTopLine;

    public String summary;

    public boolean isTopLine() {
        return isTopLine;
    }

    public void setTopLine(boolean topLine) {
        isTopLine = topLine;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
