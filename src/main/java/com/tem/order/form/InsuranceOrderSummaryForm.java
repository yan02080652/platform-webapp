package com.tem.order.form;

import java.util.Date;

/**
 * zhangtao
 */
public class InsuranceOrderSummaryForm {

	public Integer id;
	
	/**
     * 保险单号
     */
	public String insuranceId;
    /**
     * 关联订单号
     */
	public String orderId;
    
    /**
     * 支付方式
     */
	public String paymentMethod;
    /**
     * 申请人
     */
	public String userName;
    /**
     * 申请时间
     */
	public Date createDate;
    /**
     * 被保险人员
     */
	public String passengers;
    /**
     * 人数
     */
	public Integer passengerCount;
   
    /**
     * 保单生效日期
     */
	public Date effectiveDate;
    /**
     * 保单失效日期
     */
	public Date expireDate;
    /**
     * 航班号
     */
	public String flightNo;
    
    /**
     * 所属客户
     */
	public String partnerName;
    /**
     * 客户结算价
     */
	public Double totalAmount;
    
    /**
     * 所属供应商
     */
	public String supName;
    /**
     * 供应商结算价
     */
	public Double supSettleAmount;
    
    /**
     * 订单状态
     */
	public String orderState;
	
	/**
	 * 客户账单ID
	 */
	public Long billId;
    
    /**
     * 供应商保单号
     */
	public String policyNo;

	/**
	 * 成本中心名称
	 */
	public String costCenterName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getInsuranceId() {
		return insuranceId;
	}

	public void setInsuranceId(String insuranceId) {
		this.insuranceId = insuranceId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getPassengers() {
		return passengers;
	}

	public void setPassengers(String passengers) {
		this.passengers = passengers;
	}

	public Integer getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(Integer passengerCount) {
		this.passengerCount = passengerCount;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getSupName() {
		return supName;
	}

	public void setSupName(String supName) {
		this.supName = supName;
	}

	public Double getSupSettleAmount() {
		return supSettleAmount;
	}

	public void setSupSettleAmount(Double supSettleAmount) {
		this.supSettleAmount = supSettleAmount;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getCostCenterName() {
		return costCenterName;
	}

	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}

	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}
	
}
