package com.tem.order.form;

/**
 * zhangtao
 */
public class FlightOrderShowForm extends FlightOrderSummaryForm {

    /**
     * 是否主订单
     */
    public boolean isTopLine;

    /**
     * 订单摘要
     */
    public String summary;

    public boolean isTopLine() {
        return isTopLine;
    }

    public void setTopLine(boolean topLine) {
        isTopLine = topLine;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
