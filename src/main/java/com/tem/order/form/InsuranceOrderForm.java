package com.tem.order.form;

import com.iplatform.common.utils.DateUtils;
import com.tem.product.dto.insurance.InsuranceOrderDetailDto;
import com.tem.product.dto.insurance.InsuranceOrderOperationLogDto;
import com.tem.product.dto.insurance.InsuranceStatus;
import com.tem.product.dto.insurance.OrderOriginType;
import org.apache.commons.collections.CollectionUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 保险订单信息
 * 
 * @author TR1
 * 
 */
public class InsuranceOrderForm implements Serializable {

	private static final long serialVersionUID = 1L;
    /**
     * 主键ID
     */
	private Long id;
    /**
     * 所属订单ID  *
     */
	private Long orderId;
    /**
     * 供应商编码
     */
	private String providerCode;
	
	/**
	 * 保险商品ID *
	 */
	private String productId;
    /**
     * 供应商保险计划代码
     */
	private String planCode;
    /**
     * 连接器编码
     */
	private String hubCode;
    /**
     * 连接器名称
     */
	private String hubName;
    /**
     * 出单日期
     */
	private Date issueDate;
    /**
     * 保单生效日期 *
     */
	private Date effectiveDate;
    /**
     * 保单结束日期，测试出单产品保险期间只有1天，请正确进行传送
     */
	private Date expireDate;
    /**
     * 目的地信息 *
     */
	private String destination;
    /**
     * 被保险人个数
     */
	private Integer groupSize;
    /**
     * 备注说明
     */
	private String remark;
    /**
     * 保费计算方式，同计划类型:1-普通 2-双人 3-家庭
     */
	private Integer premiumCalType;
    /**
     * 保险价格总价（一个产品）
     */
	private BigDecimal totalPremium;
    /**
     * 航班号/车次号 *
     */
	private String flightNo;
    /**
     * 起飞时间  *
     */
	private Date departureTime;
    /**
     * 企业ID *
     */
	private String partnerId;
    /**
     * 企业名称 *
     */
	private String partnerName;
    /**
     * 创建者 *
     */
	private Long creater;
	
	private String createrName;
    /**
     * 创建时间
     */
	private String createDate;
    /**
     * 支付时间
     */
	private Date paymentTime;
    /**
     * 支付方式
     */
	private String paymentType;
    /**
     * 取消时间
     */
	private Date cancelTime;
    /**
     * 保单状态
     */
	private InsuranceStatus status;
    /**
     * 投保人类型
     */
	private Integer policyHolderType;
    /**
     * 投保人名称
     */
	private String policyHolderName;
    /**
     * 投保人证件类型
     */
	private Integer policyHolderIdType;
    /**
     * 投保人证件号码
     */
	private String policyHolderIdNo;
    /**
     * 投保人出生日期
     */
	private Date policyHolderBirthDate;
    /**
     * 投保人性别
     */
	private String policyHolderGender;
    /**
     * 投保人联系电话
     */
	private String policyHolderTelephone;
    /**
     * 投保人具体联系地址
     */
	private String policyHolderAddress;
	 /**
     * 投保人邮政编码
     */
	private String policyHolderPostCode;
	 /**
     * 投保人电子邮件
     */
	private String policyHolderEmail;
	 /**
     * 保险订单详情列表
     */
	private List<InsuranceOrderDetailDto> insuranceOrderDetails;
	/**
	 * 支付计划号
	 */
	private String paymentPlanNo;
	/**
	 * 能否公司支付
	 */
	private Boolean companyPay;
	
	 /**
     * 客户账单ID
     */
    private Long billId;
    
    /**
     * 客户结算状态
     */
    private String billLiqStatus;
    
    /**
     * 供应商结算状态
     */
    private String supplierLiqStatus;
    
	/**
	 * 订单来源类型
	 */
	private OrderOriginType orderOriginType;
	
	/**
	 * 支付账户id
	 */
	private Long paymentAccoutId;
	
	//====================非数据库字段====================
	/**
	 * 保险名称
	 */
    private String insuranceName;

	/**
	 * 成本中心名称
	 */
	public String costCenterName;

	/**
	 * 保险订单操作日志对象
	 */
	private InsuranceOrderOperationLogDto logDto;
    
	public Long getBillId() {
		return billId;
	}

	public void setBillId(Long billId) {
		this.billId = billId;
	}

	public String getBillLiqStatus() {
		return billLiqStatus;
	}

	public void setBillLiqStatus(String billLiqStatus) {
		this.billLiqStatus = billLiqStatus;
	}

	public String getSupplierLiqStatus() {
		return supplierLiqStatus;
	}

	public void setSupplierLiqStatus(String supplierLiqStatus) {
		this.supplierLiqStatus = supplierLiqStatus;
	}

	// ================非数据库字段===================
	
	/**
	 * 保险名称
	 */
	private String name;
	
	/**
	 * 描述
	 */
	private String	description;
	

	public InsuranceOrderForm() {
		insuranceOrderDetails = new ArrayList<InsuranceOrderDetailDto>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getProviderCode() {
		return providerCode;
	}

	public void setProviderCode(String providerCode) {
		this.providerCode = providerCode;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getHubCode() {
		return hubCode;
	}

	public void setHubCode(String hubCode) {
		this.hubCode = hubCode;
	}

	public String getHubName() {
		return hubName;
	}

	public void setHubName(String hubName) {
		this.hubName = hubName;
	}

	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getGroupSize() {
		return groupSize;
	}

	public void setGroupSize(Integer groupSize) {
		this.groupSize = groupSize;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getPremiumCalType() {
		return premiumCalType;
	}

	public void setPremiumCalType(Integer premiumCalType) {
		this.premiumCalType = premiumCalType;
	}

	public BigDecimal getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(BigDecimal totalPremium) {
		this.totalPremium = totalPremium;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public Long getCreater() {
		return creater;
	}

	public void setCreater(Long creater) {
		this.creater = creater;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public Date getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Date getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(Date cancelTime) {
		this.cancelTime = cancelTime;
	}

	public Integer getPolicyHolderType() {
		return policyHolderType;
	}

	public void setPolicyHolderType(Integer policyHolderType) {
		this.policyHolderType = policyHolderType;
	}

	public String getPolicyHolderName() {
		return policyHolderName;
	}

	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}

	public Integer getPolicyHolderIdType() {
		return policyHolderIdType;
	}

	public void setPolicyHolderIdType(Integer policyHolderIdType) {
		this.policyHolderIdType = policyHolderIdType;
	}

	public String getPolicyHolderIdNo() {
		return policyHolderIdNo;
	}

	public void setPolicyHolderIdNo(String policyHolderIdNo) {
		this.policyHolderIdNo = policyHolderIdNo;
	}

	public Date getPolicyHolderBirthDate() {
		return policyHolderBirthDate;
	}

	public void setPolicyHolderBirthDate(Date policyHolderBirthDate) {
		this.policyHolderBirthDate = policyHolderBirthDate;
	}

	public String getPolicyHolderGender() {
		return policyHolderGender;
	}

	public void setPolicyHolderGender(String policyHolderGender) {
		this.policyHolderGender = policyHolderGender;
	}

	public String getPolicyHolderTelephone() {
		return policyHolderTelephone;
	}

	public void setPolicyHolderTelephone(String policyHolderTelephone) {
		this.policyHolderTelephone = policyHolderTelephone;
	}

	public String getPolicyHolderAddress() {
		return policyHolderAddress;
	}

	public void setPolicyHolderAddress(String policyHolderAddress) {
		this.policyHolderAddress = policyHolderAddress;
	}

	public String getPolicyHolderPostCode() {
		return policyHolderPostCode;
	}

	public void setPolicyHolderPostCode(String policyHolderPostCode) {
		this.policyHolderPostCode = policyHolderPostCode;
	}

	public String getPolicyHolderEmail() {
		return policyHolderEmail;
	}

	public void setPolicyHolderEmail(String policyHolderEmail) {
		this.policyHolderEmail = policyHolderEmail;
	}

	public List<InsuranceOrderDetailDto> getInsuranceOrderDetails() {
		return insuranceOrderDetails;
	}

	public void setInsuranceOrderDetails(
			List<InsuranceOrderDetailDto> insuranceOrderDetails) {
		this.insuranceOrderDetails = insuranceOrderDetails;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public InsuranceStatus getStatus() {
		return status;
	}

	public void setStatus(InsuranceStatus status) {
		this.status = status;
	}

	public String getSummary() {
		StringBuilder sb = new StringBuilder();
		sb.append("保险  " +  hubName+"  有效期"+
				DateUtils.format(effectiveDate, "yyyy-MM-dd") + "  至  "+DateUtils.format(expireDate,"yyyy-MM-dd")+"  ");
		List<InsuranceOrderDetailDto> details = this.insuranceOrderDetails;
		for (InsuranceOrderDetailDto detail : details) {
			if(details.indexOf(detail) < details.size() - 1){
				sb.append(detail.getInsuredName()+"、");
			}else{
				sb.append(detail.getInsuredName());
			}
		}
		return sb.toString();
	}

	public String getPaymentPlanNo() {
		return paymentPlanNo;
	}

	public void setPaymentPlanNo(String paymentPlanNo) {
		this.paymentPlanNo = paymentPlanNo;
	}

	public Boolean getCompanyPay() {
		return companyPay;
	}

	public void setCompanyPay(Boolean companyPay) {
		this.companyPay = companyPay;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public OrderOriginType getOrderOriginType() {
		return orderOriginType;
	}

	public void setOrderOriginType(OrderOriginType orderOriginType) {
		this.orderOriginType = orderOriginType;
	}

	public Long getPaymentAccoutId() {
		return paymentAccoutId;
	}

	public void setPaymentAccoutId(Long paymentAccoutId) {
		this.paymentAccoutId = paymentAccoutId;
	}

	public String getInsuranceName() {
		return insuranceName;
	}

	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}

	public InsuranceOrderOperationLogDto getLogDto() {
		return logDto;
	}

	public void setLogDto(InsuranceOrderOperationLogDto logDto) {
		this.logDto = logDto;
	}

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

	public Double getTotalSettlePrice(){
		Double totalSettlePrice = 0d;
		if(CollectionUtils.isNotEmpty(insuranceOrderDetails)){
			for(InsuranceOrderDetailDto insuranceOrderDetailDto : insuranceOrderDetails){
				totalSettlePrice += insuranceOrderDetailDto.getSettlePrice() == null ? 0d : insuranceOrderDetailDto.getSettlePrice().doubleValue();
			}
		}
		return totalSettlePrice;
	}

	public String getCostCenterName() {
		return costCenterName;
	}

	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}
}
