package com.tem.order.form;

public class InternationalFlightOrderShowForm extends InternationalFlightOrderSummaryForm{

    /**
     * 是否主订单
     */
    public boolean isTopLine;

    /**
     * 订单摘要
     */
    public String summary;

    public boolean isTopLine() {
        return isTopLine;
    }

    public void setTopLine(boolean topLine) {
        isTopLine = topLine;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
