package com.tem.order.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplatform.common.Page;
import com.tem.order.form.FlightOrderShowForm;
import com.tem.order.form.GeneralOrderShowForm;
import com.tem.order.form.HotelOrderShowForm;
import com.tem.order.form.InsuranceOrderShowForm;
import com.tem.order.form.TrainOrderShowForm;
import com.tem.payment.enums.PayChannel;
import com.tem.platform.api.PartnerService;
import com.tem.product.api.ProviderService;
import com.tem.product.dto.ProviderDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.refund.RefundOrderDto;
import com.tem.pss.dto.refund.RefundPassengerDto;
import com.tem.train.api.order.TrainOrderService;
import com.tem.train.dto.order.TrainOrderDto;

@Service
public class PartnerOrderListService {

    @Autowired
    private IssueChannelService issueChannelService;

    @Autowired
    private PartnerService partnerService;

    @Autowired
    private ProviderService providerService;

    @Autowired
    private TrainOrderService trainOrderService;
    /**
     * 根据业务线获取供应商集合
     * @param channelType
     * @return
     */
    
    public Map<String,String> getIssueChannelMap(String channelType){
    	Map<String,String> map= new HashMap<String,String>();
    	List<IssueChannelDto> dtoList= issueChannelService.findListByChannelType(channelType);
    	for(IssueChannelDto dto : dtoList){
    		map.put(dto.getCode(),dto.getName());
    	}
    	
    	return map;
    }
    
    //退款订单转换为机票订单
    public FlightOrderShowForm refundOrderToFlight(RefundOrderDto refundOrderDto, FlightOrderShowForm order, Map<String, String> issueChannelMap){
        FlightOrderShowForm orderShowForm = new FlightOrderShowForm();

        orderShowForm.setOrderId(refundOrderDto.getId());
        if(StringUtils.isNotEmpty(refundOrderDto.getRefundChannel())){
        	String[] paymentMethods = refundOrderDto.getRefundChannel().split(",");
        	String methods="";
        	for(String paymentMethod: paymentMethods){
        		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
        	}
        	orderShowForm.setPaymentMethod(methods.substring(0,methods.length()-1));
        }
        orderShowForm.setUserName(refundOrderDto.getContactName());
        orderShowForm.setCreateDate(refundOrderDto.getApplyTime());
        if(refundOrderDto.getStatus() != null){
        	orderShowForm.setOrderState(refundOrderDto.getStatus().getMessage());
        }
        
        if(refundOrderDto.getRefundTotalAmount() !=null && refundOrderDto.getRefundTotalAmount().compareTo(BigDecimal.ZERO) > 0){
            if(refundOrderDto.getRefundTotalAmount() != null){
                orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().negate().doubleValue());
            }

        }else{
            if(refundOrderDto.getRefundTotalAmount() != null){
                orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().doubleValue());
            }
        }

        if(refundOrderDto.getSupperRefundAmount() != null){
            orderShowForm.setSupSettleAmount(refundOrderDto.getSupperRefundAmount().doubleValue());
        }

        orderShowForm.setSummary(refundOrderDto.getOrderSummary());

        if(order != null){
            orderShowForm.setPartnerName(order.getPartnerName());
            orderShowForm.setSupName(order.getSupName());
        }else{
            if(StringUtils.isNotEmpty(refundOrderDto.getIssueChannelType())){
            	orderShowForm.setSupName(issueChannelMap.get(refundOrderDto.getIssueChannelType()));
            }
            if(refundOrderDto.getPartnerId() != null){
                String partnerName = partnerService.getNameById(refundOrderDto.getPartnerId());
                orderShowForm.setPartnerName(partnerName);
            }
        }

        orderShowForm.setCostCenterName(refundOrderDto.getCostCenterName());
        orderShowForm.setBillId(refundOrderDto.getBillId());
        return orderShowForm;
    }

    //退票转换为火车票
    public TrainOrderShowForm refundOrderToTrain(RefundOrderDto refundOrderDto, TrainOrderShowForm order, Map<String, String> issueChannelMap){
        TrainOrderShowForm orderShowForm = new TrainOrderShowForm();
        
        orderShowForm.setOrderId(refundOrderDto.getId());
        if(StringUtils.isNotEmpty(refundOrderDto.getRefundChannel())){
        	String[] paymentMethods = refundOrderDto.getRefundChannel().split(",");
        	String methods="";
        	for(String paymentMethod: paymentMethods){
        		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
        	}
        	orderShowForm.setPaymentMethod(methods.substring(0,methods.length()-1));
        }
        orderShowForm.setUserName(refundOrderDto.getContactName());
        orderShowForm.setCreateDate(refundOrderDto.getApplyTime());
        orderShowForm.setOrderState(refundOrderDto.getStatus().getMessage());

        if(refundOrderDto.getRefundTotalAmount() !=null && refundOrderDto.getRefundTotalAmount().compareTo(BigDecimal.ZERO) > 0){
            orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().negate().doubleValue());
        }else{
            if(refundOrderDto.getRefundTotalAmount() != null){
                orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().doubleValue());
            }
        }

        if(refundOrderDto.getSupperRefundAmount() != null){
            orderShowForm.setSupSettleAmount(refundOrderDto.getSupperRefundAmount().doubleValue());
        }

        orderShowForm.setSummary(refundOrderDto.getOrderSummary());

        if(order != null){
            orderShowForm.setPartnerName(order.getPartnerName());
            orderShowForm.setSupName(order.getSupName());
            // 退单使用正向订单的供应商订单号
            orderShowForm.setExternalOrderId(order.getExternalOrderId());
            // 退单使用正向订单的行程
            orderShowForm.setTrip(order.getTrip());
        }else{
        	if(StringUtils.isNotEmpty(refundOrderDto.getIssueChannelType())){
        		orderShowForm.setSupName(issueChannelMap.get(refundOrderDto.getIssueChannelType()));
        	}
        	
            if(refundOrderDto.getPartnerId() != null){
                String partnerName = partnerService.getNameById(refundOrderDto.getPartnerId());
                orderShowForm.setPartnerName(partnerName);
            }
            //正向订单信息，主要针对额外退单
            TrainOrderDto orderDto= trainOrderService.getTrainOrderById(refundOrderDto.getOrderId());
            if(orderDto != null){
            	 // 退单使用正向订单的供应商订单号
                orderShowForm.setExternalOrderId(orderDto.getOutOrderId());
                // 退单使用正向订单的行程  ：出发站-到达站
                orderShowForm.setTrip(orderDto.getFromStation()+"-"+orderDto.getArriveStation());
            }
        }

        orderShowForm.setCostCenterName(refundOrderDto.getCostCenterName());
        orderShowForm.setBillId(refundOrderDto.getBillId());
        //原始订单号，退单的正向订单号、以及订单类型
        if(StringUtils.isNotEmpty(refundOrderDto.getOrderId())){
        	orderShowForm.setOriginOrderId(refundOrderDto.getOrderId());
        	orderShowForm.setOrderShowType("退票订单");
        }
        // 退单设置出行人名称集合
        StringBuffer bfPassengerName = new  StringBuffer();
        int passengerCount = 0;
        if(refundOrderDto.getPassengersList() != null &&refundOrderDto.getPassengersList().size()>0){
        	for(RefundPassengerDto dto : refundOrderDto.getPassengersList()){
            	if(StringUtils.isNotEmpty(dto.getUserName())){
            		bfPassengerName.append(dto.getUserName()).append("、");
            		passengerCount++;
            	}
            }
        	if(bfPassengerName.length() != 0){
            	orderShowForm.setPassengers(bfPassengerName.substring(0, bfPassengerName.length()-1));
            }
        	if(passengerCount>0){
        		orderShowForm.setPassengerCount(passengerCount);
        	}
        }
        return orderShowForm;
    }

    //退票转换为酒店
    public HotelOrderShowForm refundOrderToHotel(RefundOrderDto refundOrderDto, HotelOrderShowForm order, Map<String, String> issueChannelMap){
        HotelOrderShowForm orderShowForm = new HotelOrderShowForm();

        orderShowForm.setOrderId(refundOrderDto.getId());
        if(StringUtils.isNotEmpty(refundOrderDto.getRefundChannel())){
        	String[] paymentMethods = refundOrderDto.getRefundChannel().split(",");
        	String methods="";
        	for(String paymentMethod: paymentMethods){
        		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
        	}
        	orderShowForm.setPaymentMethod(methods.substring(0,methods.length()-1));
        }
        orderShowForm.setUserName(refundOrderDto.getContactName());
        orderShowForm.setCreateDate(refundOrderDto.getApplyTime());
        orderShowForm.setOrderState(refundOrderDto.getStatus().getMessage());

        if(refundOrderDto.getRefundTotalAmount() !=null && refundOrderDto.getRefundTotalAmount().compareTo(BigDecimal.ZERO) > 0){
            orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().negate().doubleValue());
        }else{
            if(refundOrderDto.getRefundTotalAmount() != null){
                orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().doubleValue());
            }
        }

        if(refundOrderDto.getSupperRefundAmount() != null){
            orderShowForm.setSupSettleAmount(refundOrderDto.getSupperRefundAmount().doubleValue());
        }

        orderShowForm.setSummary(refundOrderDto.getOrderSummary());

        if(order != null){
            orderShowForm.setPartnerName(order.getPartnerName());
            orderShowForm.setSupName(order.getSupName());
        }else{
        	if(StringUtils.isNotEmpty(refundOrderDto.getIssueChannelType())){
        		orderShowForm.setSupName(issueChannelMap.get(refundOrderDto.getIssueChannelType()));
        	}

            if(refundOrderDto.getPartnerId() != null){
                String partnerName = partnerService.getNameById(refundOrderDto.getPartnerId());
                orderShowForm.setPartnerName(partnerName);
            }
        }

        orderShowForm.setCostCenterName(refundOrderDto.getCostCenterName());
        orderShowForm.setBillId(refundOrderDto.getBillId());
        return orderShowForm;
    }

    //退票转换为通用订单
    public GeneralOrderShowForm refundOrderToGeneral(RefundOrderDto refundOrderDto, GeneralOrderShowForm order, Map<String, String> issueChannelMap){
        GeneralOrderShowForm orderShowForm = new GeneralOrderShowForm();

        orderShowForm.setOrderId(refundOrderDto.getId());
        if(StringUtils.isNotEmpty(refundOrderDto.getRefundChannel())){
        	String[] paymentMethods = refundOrderDto.getRefundChannel().split(",");
        	String methods="";
        	for(String paymentMethod: paymentMethods){
        		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
        	}
        	orderShowForm.setPaymentMethod(methods.substring(0,methods.length()-1));
        }
        orderShowForm.setUserName(refundOrderDto.getContactName());
        orderShowForm.setCreateDate(refundOrderDto.getApplyTime());
        orderShowForm.setOrderState(refundOrderDto.getStatus().getMessage());

        if(refundOrderDto.getRefundTotalAmount() !=null && refundOrderDto.getRefundTotalAmount().compareTo(BigDecimal.ZERO) > 0){
            orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().negate().doubleValue());
        }else{
            if(refundOrderDto.getRefundTotalAmount() != null){
                orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().doubleValue());
            }
        }

        if(refundOrderDto.getSupperRefundAmount() != null){
            orderShowForm.setSupSettleAmount(refundOrderDto.getSupperRefundAmount().doubleValue());
        }

        orderShowForm.setSummary(refundOrderDto.getOrderSummary());

        if(order != null){
            orderShowForm.setPartnerName(order.getPartnerName());
            orderShowForm.setSupName(order.getSupName());
        }else{

        	if(StringUtils.isNotEmpty(refundOrderDto.getIssueChannelType())){
        		orderShowForm.setSupName(issueChannelMap.get(refundOrderDto.getIssueChannelType()));
        	}
            
            if(refundOrderDto.getPartnerId() != null){
                String partnerName = partnerService.getNameById(refundOrderDto.getPartnerId());
                orderShowForm.setPartnerName(partnerName);
            }
        }
        orderShowForm.setCostCenterName(refundOrderDto.getCostCenterName());
        orderShowForm.setBillId(refundOrderDto.getBillId());
        return orderShowForm;
    }

    //退票转换为保险订单
    public InsuranceOrderShowForm refundOrderToInsurance(RefundOrderDto refundOrderDto, InsuranceOrderShowForm order){
        InsuranceOrderShowForm orderShowForm = new InsuranceOrderShowForm();

        orderShowForm.setOrderId(refundOrderDto.getId());
        if(StringUtils.isNotEmpty(refundOrderDto.getRefundChannel())){
        	String[] paymentMethods = refundOrderDto.getRefundChannel().split(",");
        	String methods="";
        	for(String paymentMethod: paymentMethods){
        		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
        	}
        	orderShowForm.setPaymentMethod(methods.substring(0,methods.length()-1));
        }
        orderShowForm.setUserName(refundOrderDto.getContactName());
        orderShowForm.setCreateDate(refundOrderDto.getApplyTime());
        orderShowForm.setOrderState(refundOrderDto.getStatus().getMessage());

        if(refundOrderDto.getRefundTotalAmount() !=null && refundOrderDto.getRefundTotalAmount().compareTo(BigDecimal.ZERO) > 0){
            orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().negate().doubleValue());
        }else{
            if(refundOrderDto.getRefundTotalAmount() != null){
                orderShowForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().doubleValue());
            }
        }

        if(refundOrderDto.getSupperRefundAmount() != null){
            orderShowForm.setSupSettleAmount(refundOrderDto.getSupperRefundAmount().doubleValue());
        }

        orderShowForm.setSummary(refundOrderDto.getOrderSummary());

        if(order != null){
            orderShowForm.setPartnerName(order.getPartnerName());
            orderShowForm.setSupName(order.getSupName());
        }else{
            List<ProviderDto> providerDtos = providerService.findAllProvider();//得到所有供应商
            if(StringUtils.isNotEmpty(refundOrderDto.getIssueChannelType())){
                for(ProviderDto providerDto : providerDtos){
                    if(providerDto.getCode().equals(refundOrderDto.getIssueChannelType())){
                        orderShowForm.setSupName(providerDto.getName());
                        break;
                    }
                }
            }

            if(refundOrderDto.getPartnerId() != null){
                String partnerName = partnerService.getNameById(refundOrderDto.getPartnerId());
                orderShowForm.setPartnerName(partnerName);
            }
        }

        orderShowForm.setCostCenterName(refundOrderDto.getCostCenterName());
        orderShowForm.setBillId(refundOrderDto.getBillId());
        return orderShowForm;
    }

    //获取分页
    public <E> List<E> getPageList(List<E> mergeList, List<E> extraRefundOrderList, int pageIndex, int fPage, int totalCount){

        int rCount = extraRefundOrderList.size();
        int fCount = totalCount - rCount;
        int totalPage = totalCount / Page.DEFAULT_PAGE_SIZE + (totalCount % Page.DEFAULT_PAGE_SIZE == 0?0:1);

        //分页
        if(pageIndex == fPage){

            if(pageIndex < totalPage){
                for(int i = 0; i < (pageIndex * 10 - fCount); i ++){
                    mergeList.add(extraRefundOrderList.get(i));
                }
            }else if(pageIndex == totalPage){
                mergeList.addAll(extraRefundOrderList);
            }
        }else if(pageIndex > fPage){
            int aa = (pageIndex-1) * 10 - fCount;
            int bb = 0;
            if(pageIndex < totalPage){
                bb = aa + 10;
            }else if(pageIndex == totalPage){
                bb = rCount;
            }

            for(int cc = aa; cc < bb ; cc++){
                mergeList.add(extraRefundOrderList.get(cc));
            }
        }

        return mergeList;
    }


}
