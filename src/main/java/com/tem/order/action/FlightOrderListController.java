package com.tem.order.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.flight.Constants;
import com.tem.flight.api.AircraftService;
import com.tem.flight.api.CarrierService;
import com.tem.flight.api.order.FlightOrderService;
import com.tem.flight.dto.aircraft.AircraftDto;
import com.tem.flight.dto.carrier.CarrierDto;
import com.tem.flight.dto.enums.CabinGradeEnum;
import com.tem.flight.dto.order.FlightOrderCondition;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.flight.dto.order.OrderFlightDetailDto;
import com.tem.flight.dto.order.PassengerDto;
import com.tem.flight.dto.order.enums.OrderShowStatus;
import com.tem.flight.dto.order.enums.PassengerStatus;
import com.tem.flight.dto.order.enums.PaymentStatus;
import com.tem.order.form.AllOrderQueryForm;
import com.tem.order.form.FlightOrderShowForm;
import com.tem.order.form.FlightOrderSummaryForm;
import com.tem.order.service.PartnerOrderListService;
import com.tem.order.util.ExportInfo;
import com.tem.payment.enums.PayChannel;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.OrgCostcenterService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.product.api.InsuranceOrderService;
import com.tem.product.api.ProductService;
import com.tem.product.dto.insurance.InsuranceInfo;
import com.tem.product.dto.insurance.InsuranceOrderDto;
import com.tem.product.dto.insurance.InsuranceStatus;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.refund.RefundOrderDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.*;

/**
 *
 * @author chenjieming
 * @date 2017/8/1
 */
@Controller
@RequestMapping("order")
public class FlightOrderListController extends BaseController {

    @Autowired
    private IssueChannelService issueChannelService;

    @Autowired
    private ProductService productService;

    @Autowired
    private DictService dictService;

    @Autowired
    private FlightOrderService flightOrderService;

    @Autowired
    private CarrierService carrierService;

    @Autowired
    private AircraftService aircraftService;

    @Autowired
    private InsuranceOrderService insuranceOrderService;

    @Autowired
    private OrgCostcenterService orgCostcenterService;

    @Autowired
    private RefundOrderListController refundOrderListController;

    @Autowired
    private PartnerOrderListService orderListService;

    @Autowired
    private RefundOrderService refundOrderService;

    private final Logger logger = LoggerFactory.getLogger(FlightOrderListController.class);


    @RequestMapping("/flightOrder")
    public String flightOrder(Model model){
        OrderShowStatus[] showStatus = OrderShowStatus.values();
        model.addAttribute("showStatus",showStatus);

        List<IssueChannelDto> issueChannels = issueChannelService.findListByChannelType("FLIGHT");
        model.addAttribute("issueChannels",issueChannels);

        return "order/flightOrder.base";
    }

    @RequestMapping("/flightOrderList")
    public String flightOrderList(Model model,Integer pageIndex, Integer pageSize,AllOrderQueryForm condition){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }

        Page<FlightOrderShowForm> pageList = this.getMergeFlightOrder(pageIndex,pageSize,condition);

        List<FlightOrderShowForm> mergeList = pageList.getList();//退款

        List<FlightOrderShowForm> extraRefundOrderList = this.getExtraRefundOrderList(condition, "10");

        int fPage = pageList.getTotalPage();
        int rCount = extraRefundOrderList.size(),fCount = pageList.getTotal();
        int totalCount = fCount + rCount;

        mergeList = orderListService.getPageList(mergeList, extraRefundOrderList, pageIndex, fPage, totalCount);

        Page<FlightOrderShowForm> pageList4 = new Page<>(pageList.getStart(),pageList.getSize(),
                mergeList, totalCount);

        model.addAttribute("pageList",pageList4);

        model.addAttribute("currentUserId", super.getCurrentUser().getId());

        return "order/flightOrderList.jsp";
    }

    /**
     * 机票正向与逆向订单合并
     * @param pageIndex
     * @param pageSize
     * @param condition
     * @return
     */
    private Page<FlightOrderShowForm> getMergeFlightOrder(Integer pageIndex, Integer pageSize, AllOrderQueryForm condition){
        //查询对象
        QueryDto<FlightOrderCondition> queryDto = new QueryDto<>(pageIndex, pageSize);

        queryDto.setCondition(condition.toFlightOrderCondition());

        try{
            if (StringUtils.isNotEmpty(condition.getKeyword()) ){
                RefundOrderDto refundOrderDto = refundOrderService.getById(condition.getKeyword());
                if ( refundOrderDto.getOrderId() != null ) {
                    queryDto.getCondition().setOrderNumber(refundOrderDto.getOrderId());
                }
            }
        }catch (Exception ex){

        }

        Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("FLIGHT");

        //Page<FlightOrderDto> pageList = flightOrderService.findOrderList(queryDto);
        Page<FlightOrderDto> pageList = flightOrderService.findOrderListToAdminExport(queryDto);
        condition.setType("10");
        condition.setOrderStatus(null);
        Page<RefundOrderDto> refundPageList = refundOrderListController.getMergeRefundList(1, Integer.MAX_VALUE,condition);

        List<FlightOrderShowForm> flightOrderList = transFlightOrderShowFormList(pageList.getList());

        List<FlightOrderShowForm> mergeList = new ArrayList<>();

        for (FlightOrderShowForm flightOrder : flightOrderList){

            mergeList.add(flightOrder);

            for (RefundOrderDto refundOrder : refundPageList.getList()){
                if(flightOrder.getOrderId().equals(refundOrder.getOrderId())){
                    mergeList.add(orderListService.refundOrderToFlight(refundOrder,flightOrder,issueChannelMap));
                }
            }
        }

        return new Page<>(pageList.getStart(),pageList.getSize(),mergeList,pageList.getTotal());
    }

    /**
     * 获取在这个时间段内的额外退单
     * @param condition
     * @param orderType
     * @return
     */
    private List<FlightOrderShowForm> getExtraRefundOrderList(AllOrderQueryForm condition, String orderType){
        List<FlightOrderShowForm> extraOrderInfo = new ArrayList<>();
        if(condition.getStartDate() != null && condition.getEndDate() != null){

        	Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("FLIGHT");

            List<RefundOrderDto> refundOrderList = refundOrderListController.queryExtraRefundOrder(condition, orderType);
            for(RefundOrderDto refundOrderInfo : refundOrderList){
                FlightOrderShowForm orderInfoShow = orderListService.refundOrderToFlight(refundOrderInfo,null,issueChannelMap);
                orderInfoShow.setTopLine(true);
                extraOrderInfo.add(orderInfoShow);
            }
        }
        return extraOrderInfo;
    }

    @RequestMapping("exportFlight")
    public void exportFlight(AllOrderQueryForm condition){
        List<ExportInfo> infoList = new ArrayList<>();

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }
        long start = System.currentTimeMillis();
        Page<FlightOrderShowForm> pageList = this.getMergeFlightOrder(1,Integer.MAX_VALUE,condition);

        List<FlightOrderShowForm> flightOrderList = this.getExtraRefundOrderList(condition, "10");
        pageList.getList().addAll(flightOrderList);

        List<FlightOrderSummaryForm> flightDateSet = TransformUtils.transformList(pageList.getList(),FlightOrderSummaryForm.class);

        long end = System.currentTimeMillis();
        LogUtils.debug(logger, "国内机票导出查询列表数据耗时：{}", end - start);

        String[] flightHeader = {"序号", "订单号","支付方式","预订人","预订日期","乘机人","乘机人数"
                ,"航司","舱位名称","航班号","起飞时间","行程","所属客户","客户结算价","所属供应商","供应商结算价","订单状态","关联单号(退/改/原始)","供应商订单号","PNR","票号","客户账单ID","所属成本中心", "大客户协议编码"};

        ExportInfo<FlightOrderSummaryForm> flightExport = new ExportInfo<>();
        flightExport.setDataset(flightDateSet);
        flightExport.setHeaders(flightHeader);
        flightExport.setTitle("机票订单");
        infoList.add(flightExport);

        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String fileName = "机票订单" + uuid + ".xlsx";

        refundOrderListController.goToExport(infoList,fileName,getRequest(),getResponse());
    }

    /**
     * 机票dto转换为查询显示的Dto
     * @param flightOrderList
     * @return
     */
    private List<FlightOrderShowForm> transFlightOrderShowFormList(List<FlightOrderDto> flightOrderList){
        List<IssueChannelDto> issueChannels = issueChannelService.findList();

        List<FlightOrderShowForm> flightDateSet = new ArrayList<>();

        List<DictCodeDto> berths = dictService.getCodes(Constants.OTAF_CABIN_GRADE);
        int i = 0;
        for(FlightOrderDto flightOrder : flightOrderList){
            FlightOrderShowForm flightOrderSummaryForm = new FlightOrderShowForm();
            i++;
            flightOrderSummaryForm.setId(i);
            flightOrderSummaryForm.setOrderId(flightOrder.getId());
            if(StringUtils.isNotEmpty(flightOrder.getPaymentMethod())){
            	String[] paymentMethods = flightOrder.getPaymentMethod().split(",");
            	String methods="";
            	for(String paymentMethod: paymentMethods){
            		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
            	}
                flightOrderSummaryForm.setPaymentMethod(methods.substring(0,methods.length()-1));
            }
            flightOrderSummaryForm.setUserName(flightOrder.getUserName());
            flightOrderSummaryForm.setCreateDate(flightOrder.getCreateDate());
            String passengers = "";
            String titckNos = "";
            flightOrderSummaryForm.setPassengerCount(0);
            String relevantOrders = "";
            if(StringUtils.isNotEmpty(flightOrder.getOriginalOrderId())){
                relevantOrders = relevantOrders + flightOrder.getOriginalOrderId() + ",";
            }
            boolean isChange = false;//该订单是否退改过
            if(CollectionUtils.isNotEmpty(flightOrder.getPassengers())){
                for(PassengerDto passengerDto : flightOrder.getPassengers()){
                    if(StringUtils.isNotEmpty(passengerDto.getName())){
                        passengers = passengers + passengerDto.getName() + "、";
                    }
                    if(StringUtils.isNotEmpty(passengerDto.getTicketNo())){
                        titckNos = titckNos + passengerDto.getTicketNo() + "、";
                    }

                    if(passengerDto.getStatus()!=null && (passengerDto.getStatus().equals(PassengerStatus.CHANGED)
                            || passengerDto.getStatus().equals(PassengerStatus.CHANGED)
                            || passengerDto.getStatus().equals(PassengerStatus.REFUND_IN_REVIEW)
                            || passengerDto.getStatus().equals(PassengerStatus.CHANGE_REFUND_IN_REVIEW)
                            || passengerDto.getStatus().equals(PassengerStatus.IN_CHANGE)
                            || passengerDto.getStatus().equals(PassengerStatus.CHANGE_IN_REVIEW))){
                        isChange = true;
                    }
                }
                if(isChange){//得到所关联的订单号
                    List<String> relationOrders = flightOrderService.findRelationOrderIdsByOriginalOrderId(flightOrder.getId());
                    if(CollectionUtils.isNotEmpty(relationOrders)){
                        for(String relationOrder : relationOrders){
                            relevantOrders = relevantOrders + relationOrder + ",";
                        }
                    }
                }
                if(StringUtils.isNotEmpty(titckNos)){
                    titckNos = titckNos.substring(0,titckNos.length()-1);
                }
                if(StringUtils.isNotEmpty(passengers)){
                    passengers = passengers.substring(0,passengers.length()-1);
                }
                flightOrderSummaryForm.setPassengerCount(flightOrder.getPassengers().size());

            }
            if(StringUtils.isNotEmpty(relevantOrders)){
                relevantOrders = relevantOrders.substring(0, relevantOrders.length()-1);
                flightOrderSummaryForm.setRelevantOrders(relevantOrders);
            }
            flightOrderSummaryForm.setTitckNos(titckNos);
            flightOrderSummaryForm.setPassengers(passengers);
            if(CollectionUtils.isNotEmpty(flightOrder.getOrderFlights())){
                flightOrderSummaryForm.setCarrierName(flightOrder.getOrderFlights().get(0).getCarrierName());
                flightOrderSummaryForm.setFlightNo(flightOrder.getOrderFlights().get(0).getFlightNo());
                flightOrderSummaryForm.setFromDate(flightOrder.getOrderFlights().get(0).getFromDate());
                flightOrderSummaryForm.setTrip(flightOrder.getOrderFlights().get(0).getFromCityName() + "-" + flightOrder.getOrderFlights().get(0).getToCityName());
                for(DictCodeDto dc : berths){
                    if(dc.getItemCode().equals(flightOrder.getOrderFlights().get(0).getCabinClass())){
                        flightOrderSummaryForm.setBerth(dc.getItemTxt());
                    }
                }
            }
            flightOrderSummaryForm.setTotalAmount(flightOrder.getOrderFee().getTotalOrderPrice().doubleValue());
            flightOrderSummaryForm.setSupSettleAmount(flightOrder.getOrderFee().getTotalSettlePrice() == null ? 0 : flightOrder.getOrderFee().getTotalSettlePrice().doubleValue());
            flightOrderSummaryForm.setOrderState(flightOrder.getOrderShowStatus().getMessage());
            flightOrderSummaryForm.setExternalOrderId(flightOrder.getExternalOrderId());
            flightOrderSummaryForm.setPnr(flightOrder.getPnrCode());

            if(StringUtils.isNotEmpty(flightOrder.getIssueChannel())){
                for(IssueChannelDto issueChannelDto : issueChannels){
                    if(!"FLIGHT".equals(issueChannelDto.getChannelType())) {
                        continue;
                    }
                    if(issueChannelDto.getCode().equals(flightOrder.getIssueChannel()) && issueChannelDto.getTmcId().equals(flightOrder.getTmcId())){
                        flightOrderSummaryForm.setSupName(issueChannelDto.getName());
                        break;
                    }
                }
            }
            flightOrderSummaryForm.setPartnerName(flightOrder.getcName());
            flightOrderSummaryForm.setTopLine(true);
            flightOrderSummaryForm.setSummary(flightOrder.getSummary());
            flightOrderSummaryForm.setCostCenterName(flightOrder.getCostCenterName());
            flightOrderSummaryForm.setBillId(flightOrder.getBillId());
            if(StringUtils.isNotEmpty(flightOrder.getCustomerCode())){
                flightOrderSummaryForm.setCustomerCode(flightOrder.getCustomerCode());
            }
            flightDateSet.add(flightOrderSummaryForm);
        }
        return flightDateSet;
    }

    /**
     * 订单详情页面
     * @param model
     * @param orderId
     * @return
     */
    @RequestMapping("/flightDetail/{orderId}")
    public String flightDetail(Model model,@PathVariable String orderId){
        //订单信息
        FlightOrderDto flightOrderDto = flightOrderService.getFlightOrderById(orderId);

        //计算保险总额
        double price = 0;
        List<InsuranceInfo> insuranceInfoList = productService.findInsuranceInfoList(Long.parseLong(flightOrderDto.getcId()), Constants.FLIHGT_DOMESTIC);
        List<InsuranceOrderDto> insuranceOrderDtos = insuranceOrderService.findInsuranceOrderByBizOrderId(flightOrderDto.getId());
        if(insuranceOrderDtos != null && insuranceOrderDtos.size() > 0) {
            model.addAttribute("hasInsuranceOrder",true);
        }else{
            model.addAttribute("hasInsuranceOrder",false);
        }
        for (InsuranceInfo insuranceInfo : insuranceInfoList) {
            if(insuranceOrderDtos != null && insuranceOrderDtos.size() > 0) {
                for (InsuranceOrderDto insuranceOrderDto : insuranceOrderDtos) {

                    if(insuranceOrderDto.getProductId().equals(String.valueOf(insuranceInfo.getProductId()))) {
                        price += insuranceInfo.getSalePrice().doubleValue() * insuranceOrderDto.getGroupSize();
                    }
                }
            }
        }
        List<String> insuranceOrderDtoIds = new ArrayList<>();
        insuranceOrderDtoIds.add(orderId);
        if(insuranceOrderDtos != null && insuranceOrderDtos.size() > 0) {
            for (InsuranceOrderDto insuranceOrderDto : insuranceOrderDtos) {
                if(insuranceOrderDto.getStatus().equals(InsuranceStatus.WAIT_PAYMENT) || insuranceOrderDto.getStatus().equals(InsuranceStatus.AUTHORIZE_RETURN)){
                    insuranceOrderDtoIds.add(String.valueOf(insuranceOrderDto.getId()));
                }
            }
        }
        //航司信息
        CarrierDto carrierDto = carrierService.findByCode(flightOrderDto.getOrderFlights().get(0).getFlightNo().substring(0,2));
        //舱位
        Map<String,String> gradeMap = new HashMap<>();
        for (CabinGradeEnum value : CabinGradeEnum.values()) {
            gradeMap.put(value.name(),value.getMessage());
        }

        String cabinGrade = dictService.getCodeTxt(Constants.OTAF_CABIN_GRADE, flightOrderDto.getOrderFlights().get(0).getCabinClass());

        //机型
        for (OrderFlightDetailDto orderFlight : flightOrderDto.getOrderFlights()) {
            if(!StringUtils.isBlank(orderFlight.getAircraftType())) {
                AircraftDto aircraftDto = aircraftService.getByCode(orderFlight.getAircraftType());
                if(aircraftDto != null) {
                    orderFlight.setAircraftType(aircraftDto.getName());
                }
            }
        }


        //差旅等级
        PassengerDto passengerDto = flightOrderDto.getPassengers().get(0);
        if(StringUtils.isNotEmpty(passengerDto.getTravelCode())) {
            DictCodeDto travel = dictService.getPartnerCode(Long.valueOf(flightOrderDto.getcId()), "TM_TRAVEL_TYPE", passengerDto.getTravelCode());
            model.addAttribute("travel", travel);
        }

        if(StringUtils.isNotEmpty(passengerDto.getOrgId())) {
            //费用归属
            String orgPath = orgCostcenterService.getCostCenterPathName(Long.valueOf(passengerDto.getOrgId()), passengerDto.getCostUnitCode());
            model.addAttribute("orgPath", orgPath);
        }

       // model.addAttribute("payItem",refundOrderListController.getPayItemWithPaymentNo(flightOrderDto.getPaymentPlanNo()));

        //付款信息
        if ((PaymentStatus.PAYMENT_SUCCESS.equals(flightOrderDto.getPaymentStatus())
                || PaymentStatus.PART_PAYMENT.equals(flightOrderDto.getPaymentStatus()))
                && flightOrderDto.getOrderFee().getTotalOrderPrice().compareTo(BigDecimal.ZERO) > 0) {
            model.addAttribute("bpAmount", flightOrderDto.getBpTotalAmount());
            model.addAttribute("pAmount", BigDecimalUtils.subtract(flightOrderDto.getOrderFee().getTotalOrderPrice(),
                    flightOrderDto.getBpTotalAmount()));
            model.addAttribute("paySuccess", true);
        }

        model.addAttribute("flightOrderDto", flightOrderDto);
        model.addAttribute("cabinGrade", cabinGrade);


        model.addAttribute("carrierDto", carrierDto);
        model.addAttribute("gradeMap", gradeMap);
//        model.addAttribute("aircraftDto", aircraftDto);
        model.addAttribute("currentUserId", super.getCurrentUser().getId());
        model.addAttribute("insuranceTotalPrice", price);
        model.addAttribute("insuranceOrderDtoIds", StringUtils.join(insuranceOrderDtoIds, ","));
        return "order/flightOrderDetail.base";
    }


    @RequestMapping(value = "/flight/showInsurances/{orderId}")
    public String showInsurances(@PathVariable String orderId, Model model) {

        List<InsuranceOrderDto> insuranceOrderDtos = insuranceOrderService.findInsuranceOrderByBizOrderId(orderId);
        List<InsuranceInfo> insuranceInfoList = productService.findInsuranceInfoList(Long.parseLong(insuranceOrderDtos.get(0).getPartnerId()), Constants.FLIHGT_DOMESTIC);
        //保险订单id集合
        List<String> insuranceOrderIds = new ArrayList<>();
        for (InsuranceOrderDto insuranceOrderDto : insuranceOrderDtos) {
            insuranceOrderIds.add(String.valueOf(insuranceOrderDto.getId()));
        }
        model.addAttribute("insuranceInfos", insuranceInfoList);
        model.addAttribute("insuranceOrderDtos", insuranceOrderDtos);
        model.addAttribute("orderIds", StringUtils.join(insuranceOrderIds, ","));
        return "order/insuranceDetails.base";
    }

}
