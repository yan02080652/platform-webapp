package com.tem.order.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.order.form.AllOrderQueryForm;
import com.tem.order.form.GeneralOrderShowForm;
import com.tem.order.form.GeneralOrderSummaryForm;
import com.tem.order.service.PartnerOrderListService;
import com.tem.order.util.ExportInfo;
import com.tem.payment.enums.PayChannel;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.OrgCostcenterService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.product.api.GeneralOrderService;
import com.tem.product.condition.GeneralOrderCondition;
import com.tem.product.dto.general.GeneralOrderDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.refund.RefundOrderDto;

/**
 *
 * @author chenjieming
 * @date 2017/8/1
 */
@Controller
@RequestMapping("order")
public class GeneralOrderListController extends BaseController {

	@Autowired
    private IssueChannelService issueChannelService;
	
    @Autowired
    private GeneralOrderService generalOrderService;

    @Autowired
    private DictService dictService;

    @Autowired
    private OrgCostcenterService orgCostcenterService;

    @Autowired
    private RefundOrderListController refundOrderListController;

    @Autowired
    private PartnerOrderListService orderListService;


    @Autowired
    private RefundOrderService refundOrderService;

    private final Logger logger = LoggerFactory.getLogger(GeneralOrderListController.class);
    
    @RequestMapping("/generalOrder")
    public String generalOrder(Model model){
        com.tem.product.dto.general.enums.OrderShowStatus[] showStatus = com.tem.product.dto.general.enums.OrderShowStatus.values();
        model.addAttribute("showStatus",showStatus);

        List<IssueChannelDto> issueChannels = issueChannelService.findListByChannelType("GENERAL");
       
        model.addAttribute("issueChannels",issueChannels);
        return "order/generalOrder.base";
    }

    @RequestMapping("/generalOrderList")
    public String generalOrderList(Model model,Integer pageIndex, Integer pageSize,AllOrderQueryForm condition){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }

        Page<GeneralOrderShowForm> pageList = this.getMergeGeneralOrder(pageIndex, pageSize, condition);

        List<GeneralOrderShowForm> mergeList = pageList.getList();//退款

        List<GeneralOrderShowForm> extraRefundOrderList = this.getExtraRefundOrderList(condition, "14");

        int fPage = pageList.getTotalPage();
        int rCount = extraRefundOrderList.size(),fCount = pageList.getTotal();
        int totalCount = fCount + rCount;

        mergeList = orderListService.getPageList(mergeList, extraRefundOrderList, pageIndex, fPage, totalCount);

        Page<GeneralOrderShowForm> pageList4 = new Page<>(pageList.getStart(),pageList.getSize(),
                mergeList, totalCount);

        model.addAttribute("pageList",pageList4);

        model.addAttribute("currentUserId", super.getCurrentUser().getId());


        return "order/generalOrderList.jsp";
    }

    /**
     * 正向火车票和退单合并
     * @param pageIndex
     * @param pageSize
     * @param condition
     * @return
     */
    private Page<GeneralOrderShowForm> getMergeGeneralOrder(Integer pageIndex, Integer pageSize, AllOrderQueryForm condition){

        QueryDto<GeneralOrderCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(condition.toGeneralOrderCondition());

        try{
            if (StringUtils.isNotEmpty(condition.getKeyword()) ){
                RefundOrderDto refundOrderDto = refundOrderService.getById(condition.getKeyword());
                if ( refundOrderDto.getOrderId() != null ) {
                    queryDto.getCondition().setKeyword(refundOrderDto.getOrderId());
                }
            }
        }catch (Exception ex){

        }

        Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("GENERAL");
        Page<GeneralOrderDto> pageList = generalOrderService.queryListWithPage(queryDto);
        List<GeneralOrderShowForm> orderList = transGeneralOrderSummaryFormList(pageList.getList());

        condition.setType("14");
        condition.setOrderStatus(null);
        Page<RefundOrderDto> refundPageList = refundOrderListController.getMergeRefundList(1, Integer.MAX_VALUE,condition);

        List<GeneralOrderShowForm> mergeList = new ArrayList<>();

        for (GeneralOrderShowForm order : orderList){

            mergeList.add(order);

            for (RefundOrderDto refundOrder : refundPageList.getList()){
                if(order.getOrderId().equals(refundOrder.getOrderId())){
                    mergeList.add(orderListService.refundOrderToGeneral(refundOrder,order,issueChannelMap));
                }
            }
        }

        return new Page<>(pageList.getStart(),pageList.getSize(),mergeList,pageList.getTotal());
    }

    /**
     * 获取在这个时间段内的额外退单
     * @param condition
     * @param orderType
     * @return
     */
    private List<GeneralOrderShowForm> getExtraRefundOrderList(AllOrderQueryForm condition, String orderType){
        List<GeneralOrderShowForm> extraOrderInfo = new ArrayList<>();
        if(condition.getStartDate() != null && condition.getEndDate() != null){

        	Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("FLIGHT");
        	
            List<RefundOrderDto> refundOrderList = refundOrderListController.queryExtraRefundOrder(condition, orderType);
            for(RefundOrderDto refundOrderInfo : refundOrderList){
                GeneralOrderShowForm orderInfoShow = orderListService.refundOrderToGeneral(refundOrderInfo,null,issueChannelMap);
                orderInfoShow.setTopLine(true);
                extraOrderInfo.add(orderInfoShow);
            }
        }
        return extraOrderInfo;
    }

    @RequestMapping("exportGeneral")
    public void exportGeneral(AllOrderQueryForm condition){
        List<ExportInfo> infoList = new ArrayList<>();

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }
        long start = System.currentTimeMillis();
        Page<GeneralOrderShowForm> pageList = this.getMergeGeneralOrder(1,Integer.MAX_VALUE,condition);

        List<GeneralOrderShowForm> orderList = this.getExtraRefundOrderList(condition, "14");
        pageList.getList().addAll(orderList);

        List<GeneralOrderSummaryForm> dateSet = TransformUtils.transformList(pageList.getList(),GeneralOrderSummaryForm.class);

        long end = System.currentTimeMillis();
        LogUtils.debug(logger, "需求单导出查询列表数据耗时：{}", end - start);
        
        String[] genaralHeader = {"序号", "订单号","支付方式","预订人","预订日期","出行人数"
                ,"行程时间","所属客户","客户结算价","所属供应商","供应商结算价","订单状态","客户账单ID", "供应商订单号", "所属成本中心"};

        ExportInfo<GeneralOrderSummaryForm> generalExport = new ExportInfo<GeneralOrderSummaryForm>();
        generalExport.setDataset(dateSet);
        generalExport.setHeaders(genaralHeader);
        generalExport.setTitle("通用订单");
        infoList.add(generalExport);

        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String fileName = "通用订单" + uuid +  ".xlsx";

        refundOrderListController.goToExport(infoList, fileName, getRequest(), getResponse());
    }

    private List<GeneralOrderShowForm> transGeneralOrderSummaryFormList(List<GeneralOrderDto> generalOrderList){

        List<GeneralOrderShowForm> generalDateSet = new ArrayList<>();
        int i = 0;
        for(GeneralOrderDto generalOrderDto : generalOrderList){
            GeneralOrderShowForm generalOrderSummaryForm = new GeneralOrderShowForm();
            i++;
            generalOrderSummaryForm.setId(i);
            generalOrderSummaryForm.setOrderId(generalOrderDto.getId());
			if(StringUtils.isNotEmpty(generalOrderDto.getPaymentMethod())){
				String[] paymentMethods = generalOrderDto.getPaymentMethod().split(",");
            	String methods="";
            	for(String paymentMethod: paymentMethods){
            		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
            	}
            	generalOrderSummaryForm.setPaymentMethod(methods.substring(0,methods.length()-1));
			}
            generalOrderSummaryForm.setUserName(generalOrderDto.getUserName());
            generalOrderSummaryForm.setCreateDate(generalOrderDto.getCreateDate());
            generalOrderSummaryForm.setPassengerCount(generalOrderDto.getTravellerCount());
            generalOrderSummaryForm.setFromDate(generalOrderDto.getTravellerDate());
            generalOrderSummaryForm.setTotalAmount(generalOrderDto.getTotalOrderPrice()==null? 0 :generalOrderDto.getTotalOrderPrice().doubleValue());
            generalOrderSummaryForm.setSupSettleAmount(generalOrderDto.getTotalSettlePrice() == null ? 0 : generalOrderDto.getTotalSettlePrice().doubleValue());
            generalOrderSummaryForm.setOrderState(generalOrderDto.getOrderShowStatus().getMessage());
            generalOrderSummaryForm.setExternalOrderId(generalOrderDto.getProviderOrderNo());
            generalOrderSummaryForm.setSupName(generalOrderDto.getProviderName());
            generalOrderSummaryForm.setPartnerName(generalOrderDto.getcName());

            generalOrderSummaryForm.setTopLine(true);
            generalOrderSummaryForm.setSummary(generalOrderDto.getSummary());
            generalOrderSummaryForm.setCostCenterName(generalOrderDto.getCostCenterName());
            generalOrderSummaryForm.setBillId(generalOrderDto.getBillId());
            generalDateSet.add(generalOrderSummaryForm);
        }
        return generalDateSet;
    }

    @RequestMapping("/generalDetail/{orderId}")
    public String generalDetail(Model model , @PathVariable("orderId") String orderId){
        GeneralOrderDto generalOrder = generalOrderService.getById(orderId);

        DictCodeDto travel = dictService.getPartnerCode(generalOrder.getcId(), "TM_TRAVEL_TYPE",generalOrder.getTravelCode());
        if(travel != null){
            model.addAttribute("travel", travel.getItemTxt());
        }
        if(generalOrder.getOrgId() != null){
            String orgPath  = orgCostcenterService.getCostCenterPathName(Long.valueOf(generalOrder.getOrgId()), generalOrder.getCostUnitCode());
            model.addAttribute("orgPath", orgPath);
        }

        model.addAttribute("generalOrder", generalOrder);
        return "order/generalOrderDetail.base";
    }

}
