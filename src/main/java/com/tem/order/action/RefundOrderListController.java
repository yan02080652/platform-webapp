package com.tem.order.action;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.utils.LogUtils;
import com.tem.flight.Constants;
import com.tem.flight.api.AircraftService;
import com.tem.flight.api.CarrierService;
import com.tem.flight.api.order.FlightOrderService;
import com.tem.flight.dto.aircraft.AircraftDto;
import com.tem.flight.dto.carrier.CarrierDto;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.hotel.api.HotelOrderService;
import com.tem.hotel.api.order.OrderCustomerService;
import com.tem.hotel.dto.order.CustomerDto;
import com.tem.hotel.dto.order.HotelOrderDto;
import com.tem.order.form.AllOrderQueryForm;
import com.tem.order.form.RefundOrderSummaryForm;
import com.tem.order.util.ExportExcel;
import com.tem.order.util.ExportInfo;
import com.tem.payment.api.PaymentPlanItemService;
import com.tem.payment.dto.PaymentPlanItemDto;
import com.tem.payment.enums.PayChannel;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.dto.refund.RefundOrderCondition;
import com.tem.pss.dto.refund.RefundOrderDto;
import com.tem.pss.dto.refund.RefundStatus;
import com.tem.train.api.order.TrainOrderService;
import com.tem.train.dto.order.TrainOrderDto;
import com.tem.train.dto.order.TrainPassengerDto;

/**
 * 订单查询
 * @author zhangtao
 *
 */
@Controller
@RequestMapping("order")
public class RefundOrderListController extends BaseController {

	@Autowired
	private CarrierService carrierService;
	
	@Autowired
	private AircraftService aircraftService;

	@Autowired
	private TrainOrderService trainOrderService;
	
	@Autowired
	private HotelOrderService hotelOrderService;
	
	@Autowired
	private RefundOrderService refundOrderService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private PartnerService partnerService;
	
	@Autowired
	private OrderCustomerService orderCustomerService;

	@Autowired
	private DictService dictService;

	@Autowired
	private FlightOrderService flightOrderService;

	@Autowired
	private PaymentPlanItemService paymentPlanItemService;

	private final Logger logger = LoggerFactory.getLogger(RefundOrderListController.class);

	@RequestMapping("/refundOrder")
	public String refundOrder(Model model,String state){
		model.addAttribute("orderBizType", OrderBizType.showValues());
		model.addAttribute("refundStatus",RefundStatus.values());
		if (StringUtils.isNotEmpty(state)) {
			model.addAttribute("state", state);
		}

		return "order/refundOrder.base";
	}
	
	@RequestMapping("/refundOrderList")
	public String refundOrderList(Model model,Integer pageIndex, Integer pageSize,AllOrderQueryForm condition){
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = Page.DEFAULT_PAGE_SIZE;
		}

		//判断是否有查看所有tmc的权限，
		if (!isSuperAdmin()){
			condition.setTmcId(super.getTmcId().toString());
		}

		Page<RefundOrderDto> pageList = this.getMergeRefundList(pageIndex,pageSize, condition);
		
		Map<String,String> channelMap = new HashMap<String,String>();
		PayChannel[] values = PayChannel.values();
		for(PayChannel channel : values){
			channelMap.put(channel.name(), channel.getValue());
		}
		model.addAttribute("channelMap", channelMap);
		model.addAttribute("pageList", pageList);
		model.addAttribute("flightCode", OrderBizType.FLIGHT.getCode());
		model.addAttribute("hotelCode", OrderBizType.HOTEL.getCode());
		model.addAttribute("trainCode", OrderBizType.TRAIN.getCode());
		model.addAttribute("generalCode", OrderBizType.GENERAL.getCode());

		return "order/refundOrderList.jsp";
	}

	/**
	 * 获取退款订单信息
	 * @param pageIndex
	 * @param pageSize
	 * @param condition
	 * @return
	 */
    public Page<RefundOrderDto> getMergeRefundList(Integer pageIndex, Integer pageSize, AllOrderQueryForm condition){
        //查询对象
        QueryDto<RefundOrderCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(condition.toRefundOrderCondition());

		try{
			if (StringUtils.isNotEmpty(condition.getKeyword()) ){
				RefundOrderDto refundOrderDto = refundOrderService.getById(condition.getKeyword());
				if ( refundOrderDto.getOrderId() != null ) {
					queryDto.getCondition().setKeyword(refundOrderDto.getOrderId());
				}
			}
		}catch (Exception ex){

		}

		LogUtils.debug(logger, "查询退单,入参：{}", queryDto);

        Page<RefundOrderDto> pageList = refundOrderService.queryListWithPage(queryDto);

		LogUtils.debug(logger, "查询退单，退单条数为：{}", pageList.getList().size());

	    return pageList;
    }

	@RequestMapping("exportRefund")
	public void exportRefund(AllOrderQueryForm condition){
		List<ExportInfo> infoList = new ArrayList<>();

		//判断是否有查看所有tmc的权限，
		if (!isSuperAdmin()){
			condition.setTmcId(super.getTmcId().toString());
		}
		long start = System.currentTimeMillis();
		Page<RefundOrderDto> pageList = this.getMergeRefundList(1,Integer.MAX_VALUE, condition);
		long end = System.currentTimeMillis();
        LogUtils.debug(logger, "退单订单导出查询列表数据耗时：{}", end - start);
        
       	List<RefundOrderSummaryForm> refundDateSet = transRefundOrderSummaryFormList(pageList.getList());
        String[] generalHeader = {"序号", "退单号","原始订单号","退单类型","退款渠道","退款申请人","退款申请时间"
        		,"所属客户","退款金额","供应商应退额","订单状态","客户账单ID","所属成本中心"};

        ExportInfo<RefundOrderSummaryForm> refundExport = new ExportInfo<>();
        refundExport.setDataset(refundDateSet);
        refundExport.setHeaders(generalHeader);
        refundExport.setTitle("退单");
		infoList.add(refundExport);

		String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		String fileName = "退单" + uuid +  ".xlsx";

     	this.goToExport(infoList,fileName,getRequest(),getResponse());

	}

	private List<RefundOrderSummaryForm> transRefundOrderSummaryFormList(List<RefundOrderDto> refundOrderList){
		
		List<RefundOrderSummaryForm> refundDateSet = new ArrayList<RefundOrderSummaryForm>();
		int i = 0;

		RefundOrderSummaryForm refundOrderSummaryForm = null;

		for(RefundOrderDto refundOrderDto : refundOrderList){
			refundOrderSummaryForm = new RefundOrderSummaryForm();
			i++;
			refundOrderSummaryForm.setId(i);
			refundOrderSummaryForm.setOrderId(refundOrderDto.getId());
			refundOrderSummaryForm.setPreOrderId(refundOrderDto.getOrderId());
			OrderBizType orderType = OrderBizType.valueOfByOrderId(refundOrderDto.getOrderId());
			refundOrderSummaryForm.setOrderType(orderType.getMessage());
			if(StringUtils.isNotEmpty(refundOrderDto.getRefundChannel())){
				String[] paymentMethods = refundOrderDto.getRefundChannel().split(",");
            	String methods="";
            	for(String paymentMethod: paymentMethods){
            		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
            	}
            	refundOrderSummaryForm.setPaymentMethod(methods.substring(0,methods.length()-1));
			}
			if(refundOrderDto.getUserId() != null){
				UserDto userDto = userService.getUser(Long.parseLong(refundOrderDto.getUserId()));
				if(userDto != null){
					refundOrderSummaryForm.setUserName(userDto.getFullname());
				}
			}
			refundOrderSummaryForm.setCreateDate(refundOrderDto.getApplyTime());
			refundOrderSummaryForm.setSupperRefundAmount(refundOrderDto.getSupperRefundAmount() == null ? 0D : refundOrderDto.getSupperRefundAmount().doubleValue());
			if(refundOrderDto.getPartnerId() != null){
				PartnerDto partnerDto = partnerService.findById(refundOrderDto.getPartnerId());
				if(partnerDto != null){
					refundOrderSummaryForm.setPartnerName(partnerDto.getName());
				}
			}
			if(refundOrderDto.getRefundTotalAmount() != null){
				refundOrderSummaryForm.setTotalAmount(refundOrderDto.getRefundTotalAmount().doubleValue());
			}
			if(refundOrderDto.getStatus() != null){
				refundOrderSummaryForm.setOrderState(refundOrderDto.getStatus().getMessage());
			}
			refundOrderSummaryForm.setExternalOrderId(refundOrderDto.getOutRefundSerialNo());
			refundOrderSummaryForm.setBillId(refundOrderDto.getBillId());
			refundOrderSummaryForm.setCostCenterName(refundOrderDto.getCostCenterName());
			refundDateSet.add(refundOrderSummaryForm);
		}
		return refundDateSet;
	}

	/**
	 * 退票详情
	 * @param model
	 * @param refundId
	 * @return
	 */
    @RequestMapping("/refundFlightOrderDetail/{refundId}")
    public String refundFlightOrderDetail(Model model, @PathVariable String refundId) {

        RefundOrderDto refundOrderDto = flightOrderService.getRefundOrderById(refundId);
        FlightOrderDto dto = (FlightOrderDto) refundOrderDto.getOrderDto();

        //航司信息
        CarrierDto carrierDto = carrierService.findByCode(dto.getOrderFlights().get(0).getFlightNo().substring(0, 2));
        //舱位
        String cabinGrade = dictService.getCodeTxt(Constants.OTAF_CABIN_GRADE, dto.getOrderFlights().get(0).getCabinClass());
        //机型
        AircraftDto aircraftDto = aircraftService.getByCode(dto.getOrderFlights().get(0).getAircraftType());

        //退单金额显示正数
		if(refundOrderDto.getRefundTotalAmount() != null){
			refundOrderDto.setRefundTotalAmount(new BigDecimal(Math.abs(refundOrderDto.getRefundTotalAmount().doubleValue())));
		}
        model.addAttribute(refundOrderDto);
        model.addAttribute("cabinGrade", cabinGrade);
        model.addAttribute("flightOrderDto", dto);
        model.addAttribute("aircraftDto", aircraftDto);
        model.addAttribute("carrierDto", carrierDto);

        return "order/flightBackDetail.base";
    }
	
    @RequestMapping(value = "/refundTrainDetail/{refundId}")
	public String refundTrainDetail(Model model,@PathVariable("refundId") String refundId){
		RefundOrderDto refund = refundOrderService.getById(refundId);
		
		TrainOrderDto trainOrder = trainOrderService.getTrainOrderById(refund.getOrderId());
		List<TrainPassengerDto> rList =new ArrayList<>();
		for(TrainPassengerDto t : trainOrder.getPassengerList()){
			if(refundId.equals(t.getRefundOrderId())){
				rList.add(t);
			}
		}
		//页面显示正数
		if(refund.getRefundTotalAmount() != null){
			refund.setRefundTotalAmount(new BigDecimal(Math.abs(refund.getRefundTotalAmount().doubleValue())));
		}
		trainOrder.setPassengerList(rList);
		model.addAttribute("refund", refund);
		model.addAttribute("order", trainOrder);
		return "order/trainBackDetail.base";
	}
    
    /**
	 * 退订详情
	 * @param refundId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "refundHotelDetail/{refundId}",method = RequestMethod.GET)
	public String refundHotelDetail(@PathVariable String refundId,Model model){
		RefundOrderDto refundOrder = refundOrderService.getById(refundId);
		HotelOrderDto order = hotelOrderService.getOrderDetail(refundOrder.getOrderId()).getData();
		List<CustomerDto> customers = orderCustomerService.findByRefundId(refundId).getData();
		model.addAttribute("customers",customers);
		model.addAttribute("refundOrder",refundOrder);
		model.addAttribute("order",order);
		return "order/refundHotelDetail.base";
	}
	
	@RequestMapping("refundGeneralDetail/{orderId}")
	public	String refundGeneralDetail(Model model , @PathVariable("orderId") String orderId){
		RefundOrderDto refundOrder = refundOrderService.getById(orderId);
		model.addAttribute("refundOrder", refundOrder);
		return "order/refundGeneralDetail.base";
	}

	/**
	 * 查询额外的退单记录
	 * @param condition
	 * @param orderType
	 * @return
	 */
	public List<RefundOrderDto> queryExtraRefundOrder(AllOrderQueryForm condition, String orderType){
		QueryDto<RefundOrderCondition> queryDto = new QueryDto<>(1,Integer.MAX_VALUE);
		RefundOrderCondition refundCondition = condition.toRefundOrderCondition();
		refundCondition.setOrderBizType(orderType);
		refundCondition.setOrderCreateBeginDate(refundCondition.getStartDate());
		refundCondition.setOrderCreateEndDate(refundCondition.getEndDate());
		queryDto.setCondition(refundCondition);

		Page<RefundOrderDto> pageList = refundOrderService.queryListWithPage(queryDto);

		return pageList.getList();
	}

	/**
	 * 导出订单模板
	 * @param exportInfoList
	 * @param fileName
	 * @param request
	 * @param response
	 */
	public void goToExport(List<ExportInfo> exportInfoList, String fileName,
                           HttpServletRequest request, HttpServletResponse response){
		try
		{
			long begin = System.currentTimeMillis();

			String ctxPath = request.getSession().getServletContext().getRealPath("");
			String downLoadPath = ctxPath+"/resource/excel/"+ fileName;

			ExportExcel ex = new ExportExcel();
			OutputStream out = new FileOutputStream(downLoadPath);

			long begin1 = System.currentTimeMillis();

			ex.exportExcel(exportInfoList, out, "yyyy-MM-dd");

			long end1 = System.currentTimeMillis();
			LogUtils.debug(logger,"导出生成excel,耗时：{}",end1 - begin1);

			out.close();

			//获得请求文件名
			request.setCharacterEncoding("UTF-8");
			BufferedInputStream bis = null;
			BufferedOutputStream bos = null;

			//获取文件的长度
			long fileLength = new File(downLoadPath).length();

			//设置文件输出类型
			response.setContentType("application/octet-stream");
			response.setHeader("Content-disposition", "attachment; filename="
					+ new String(fileName.getBytes("utf-8"), "ISO8859-1"));
			//设置输出长度
			response.setHeader("Content-Length", String.valueOf(fileLength));
			//获取输入流
			bis = new BufferedInputStream(new FileInputStream(downLoadPath));
			//输出流
			bos = new BufferedOutputStream(response.getOutputStream());
			byte[] buff = new byte[2048];
			int bytesRead;
			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
			}
			//关闭流
			bis.close();
			bos.close();

			long end = System.currentTimeMillis();
			LogUtils.debug(logger,"文件的操作,耗时：{}",end - begin);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据支付号获取支付方式
	 * @param paymentNo
	 * @return
	 */
	public Map<String,String> getPayItemWithPaymentNo(String paymentNo){
		Map<String,String> payItem = new HashMap<>();
		if(paymentNo == null){
			return payItem;
		}
		List<PaymentPlanItemDto> items = paymentPlanItemService.getByPlanId(paymentNo);
		if(CollectionUtils.isNotEmpty(items)) {
			String payMethod = "";
			String payChannel = "";
			String tradeId = "";
			for (PaymentPlanItemDto item : items) {
				if ("BP".equals(item.getPaymentType())) {
					payMethod += "公司支付 : 账户支付";
					tradeId = item.getPlanId().toString();
				} else if ("P".equals(item.getPaymentType())) {
					payMethod += "个人支付 : ";
					PayChannel channel = PayChannel.valueOf(item.getPayChannel());
					payChannel += channel.getValue();
					if (items.indexOf(item) + 1 < items.size()) {
						payChannel += " + ";
					}
					tradeId = item.getPlanId().toString();
				}
			}
			payItem.put("payMethod", payMethod + payChannel);
			payItem.put("tradeId", tradeId);
		}
		return payItem;
	}

}
