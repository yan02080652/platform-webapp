package com.tem.order.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.hotel.api.HotelOrderService;
import com.tem.hotel.dto.order.CustomerDto;
import com.tem.hotel.dto.order.HotelOrderCondition;
import com.tem.hotel.dto.order.HotelOrderDto;
import com.tem.hotel.dto.order.enums.PaymentStatus;
import com.tem.order.form.AllOrderQueryForm;
import com.tem.order.form.HotelOrderShowForm;
import com.tem.order.form.HotelOrderSummaryForm;
import com.tem.order.service.PartnerOrderListService;
import com.tem.order.util.ExportInfo;
import com.tem.payment.api.PaymentPlanItemService;
import com.tem.payment.dto.PaymentPlanItemDto;
import com.tem.payment.enums.PayChannel;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.OrgCostcenterService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.refund.RefundOrderDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.*;

/**
 *
 * @author chenjieming
 * @date 2017/8/1
 */
@Controller
@RequestMapping("order")
public class HotelOrderListController extends BaseController {

    @Autowired
    private HotelOrderService hotelOrderService;

    @Autowired
    private DictService dictService;

    @Autowired
    private OrgCostcenterService orgService;

    @Autowired
    private PaymentPlanItemService paymentPlanItemService;

    @Autowired
    private RefundOrderListController refundOrderListController;

    @Autowired
    private PartnerOrderListService orderListService;

    @Autowired
    private IssueChannelService issueChannelService;

    @Autowired
    private RefundOrderService refundOrderService;

    private final Logger logger = LoggerFactory.getLogger(HotelOrderListController.class);
    
    @RequestMapping("/hotelOrder")
    public String hotelOrder(Model model){
        model.addAttribute("orderStatusEnum",com.tem.hotel.dto.order.enums.OrderShowStatus.values());
        this.getIssues(model);

        return "order/hotelOrder.base";
    }

    /**
     * 获取供应商渠道
     * @param model
     */
    private void getIssues(Model model){
        List<IssueChannelDto> issueChannels = issueChannelService.findListByChannelType("HOTEL");
        
        model.addAttribute("issueChannels",issueChannels);
    }

    @RequestMapping("/hotelOrderList")
    public String hotelOrderList(Model model,Integer pageIndex, Integer pageSize, AllOrderQueryForm condition){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }
        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }

        Page<HotelOrderShowForm> pageList = this.getMergeHotelOrder(pageIndex, pageSize, condition);

        List<HotelOrderShowForm> mergeList = pageList.getList();//退款

        List<HotelOrderShowForm> extraRefundOrderList = this.getExtraRefundOrderList(condition, "11");

        int fPage = pageList.getTotalPage();
        int rCount = extraRefundOrderList.size(),fCount = pageList.getTotal();
        int totalCount = fCount + rCount;

        mergeList = orderListService.getPageList(mergeList, extraRefundOrderList, pageIndex, fPage, totalCount);

        Page<HotelOrderShowForm> pageList4 = new Page<>(pageList.getStart(),pageList.getSize(),
                mergeList, totalCount);

        model.addAttribute("pageList",pageList4);

        model.addAttribute("currentUserId", super.getCurrentUser().getId());

        return "order/hotelOrderList.jsp";
    }

    /**
     * 正向酒店和退单合并
     * @param pageIndex
     * @param pageSize
     * @param condition
     * @return
     */
    private Page<HotelOrderShowForm> getMergeHotelOrder(Integer pageIndex, Integer pageSize, AllOrderQueryForm condition){

        QueryDto<HotelOrderCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(condition.toHotelOrderCondition());

        try{
            if (StringUtils.isNotEmpty(condition.getKeyword()) ){
                RefundOrderDto refundOrderDto = refundOrderService.getById(condition.getKeyword());
                if (refundOrderDto.getOrderId() != null) {
                    queryDto.getCondition().setOrderNumber(refundOrderDto.getOrderId());
                }
            }
        }catch (Exception ex){

        }
        Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("HOTEL");
        Page<HotelOrderDto> pageList = hotelOrderService.tmcQueryListWithPage(queryDto).getData();
        List<HotelOrderShowForm> orderList = transHotelOrderSummaryFormList(pageList.getList());

        condition.setType("11");
        condition.setOrderStatus(null);
        Page<RefundOrderDto> refundPageList = refundOrderListController.getMergeRefundList(1, Integer.MAX_VALUE,condition);

        List<HotelOrderShowForm> mergeList = new ArrayList<>();

        for (HotelOrderShowForm order : orderList){

            mergeList.add(order);

            for (RefundOrderDto refundOrder : refundPageList.getList()){
                if(order.getOrderId().equals(refundOrder.getOrderId())){
                    mergeList.add(orderListService.refundOrderToHotel(refundOrder,order,issueChannelMap));
                }
            }
        }

        return new Page<>(pageList.getStart(),pageList.getSize(),mergeList,pageList.getTotal());
    }

    /**
     * 获取在这个时间段内的额外退单
     * @param condition
     * @param orderType
     * @return
     */
    private List<HotelOrderShowForm> getExtraRefundOrderList(AllOrderQueryForm condition, String orderType){
        List<HotelOrderShowForm> extraOrderInfo = new ArrayList<>();
        if(condition.getStartDate() != null && condition.getEndDate() != null){
        	Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("HOTEL");
            List<RefundOrderDto> refundOrderList = refundOrderListController.queryExtraRefundOrder(condition, orderType);
            for(RefundOrderDto refundOrderInfo : refundOrderList){
                HotelOrderShowForm orderInfoShow = orderListService.refundOrderToHotel(refundOrderInfo,null,issueChannelMap);
                orderInfoShow.setTopLine(true);
                extraOrderInfo.add(orderInfoShow);
            }
        }
        return extraOrderInfo;
    }

    @RequestMapping("exportHotel")
    public void exportHotel(AllOrderQueryForm condition){
        List<ExportInfo> infoList = new ArrayList<>();

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }
        long start = System.currentTimeMillis();
        Page<HotelOrderShowForm> pageList = this.getMergeHotelOrder(1,Integer.MAX_VALUE,condition);

        List<HotelOrderShowForm> orderList = this.getExtraRefundOrderList(condition, "11");
        pageList.getList().addAll(orderList);

        List<HotelOrderSummaryForm> dateSet = TransformUtils.transformList(pageList.getList(),HotelOrderSummaryForm.class);
        long end = System.currentTimeMillis();
        LogUtils.debug(logger, "酒店订单导出查询列表数据耗时：{}", end - start);
        
        String[] hotelHeader = {"序号", "订单号","支付方式","预订人","预订日期","入住人","入住人数"
                ,"酒店名称","入住时间","离店时间","所属客户","客户结算价","所属供应商","供应商结算价","订单状态","客户账单ID", "供应商订单号","房间号","所属成本中心"};

        ExportInfo<HotelOrderSummaryForm> hotelExport = new ExportInfo<>();
        hotelExport.setDataset(dateSet);
        hotelExport.setHeaders(hotelHeader);
        hotelExport.setTitle("酒店订单");
        infoList.add(hotelExport);

        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String fileName = "酒店订单" + uuid +  ".xlsx";

        refundOrderListController.goToExport(infoList, fileName, getRequest(), getResponse());
    }

    /**
     * 酒店转换
     * @param hotelOrderList
     * @return
     */
    private List<HotelOrderShowForm> transHotelOrderSummaryFormList(List<HotelOrderDto> hotelOrderList){

    	Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("HOTEL");
    	
        List<HotelOrderShowForm> hotelDateSet = new ArrayList<>();
        int i = 0;
        for(HotelOrderDto hotelOrderDto : hotelOrderList){
            HotelOrderShowForm hotelOrderSummaryForm = new HotelOrderShowForm();
            i++;
            hotelOrderSummaryForm.setId(i);
            hotelOrderSummaryForm.setOrderId(hotelOrderDto.getId());
            if(StringUtils.isNotEmpty(hotelOrderDto.getPaymentMethod())){
            	String[] paymentMethods = hotelOrderDto.getPaymentMethod().split(",");
            	String methods="";
            	for(String paymentMethod: paymentMethods){
            		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
            	}
            	hotelOrderSummaryForm.setPaymentMethod(methods.substring(0,methods.length()-1));
            }
            hotelOrderSummaryForm.setUserName(hotelOrderDto.getUserName());
            hotelOrderSummaryForm.setCreateDate(hotelOrderDto.getCreateDate());
            String passengers = "";
            String titckNos = "";
            hotelOrderSummaryForm.setPassengerCount(0);
            if(CollectionUtils.isNotEmpty(hotelOrderDto.getCustomers())){
                for(CustomerDto passengerDto : hotelOrderDto.getCustomers()){
                    if(StringUtils.isNotEmpty(passengerDto.getName())){
                        passengers = passengers + passengerDto.getName() + "、";
                    }
                    if(StringUtils.isNotEmpty(passengerDto.getRoomNo())){
                        titckNos = titckNos + passengerDto.getRoomNo() + "、";
                    }
                }
                if(StringUtils.isNotEmpty(titckNos)){
                    titckNos = titckNos.substring(0,titckNos.length()-1);
                }
                if(StringUtils.isNotEmpty(passengers)){
                    passengers = passengers.substring(0,passengers.length()-1);
                }
                hotelOrderSummaryForm.setPassengerCount(hotelOrderDto.getCustomers().size());

            }
            hotelOrderSummaryForm.setTitckNos(titckNos);
            hotelOrderSummaryForm.setPassengers(passengers);
            hotelOrderSummaryForm.setHotelName(hotelOrderDto.getHotelName());
            hotelOrderSummaryForm.setFromDate(DateUtils.format(hotelOrderDto.getArrivalDate()));
            hotelOrderSummaryForm.setToDate(DateUtils.format(hotelOrderDto.getDepartureDate()));

            hotelOrderSummaryForm.setTotalAmount(hotelOrderDto.getTotalPrice()==null? 0 :hotelOrderDto.getTotalPrice().doubleValue());
            hotelOrderSummaryForm.setSupSettleAmount(hotelOrderDto.getTotalCost() == null ? 0 : hotelOrderDto.getTotalCost().doubleValue());
            hotelOrderSummaryForm.setOrderState(hotelOrderDto.getOrderShowStatus().getStatus());
            hotelOrderSummaryForm.setExternalOrderId(hotelOrderDto.getExternalOrderId());

            if(StringUtils.isNotEmpty(hotelOrderDto.getHotelOriginType())){
                hotelOrderSummaryForm.setSupName(issueChannelMap.get(hotelOrderDto.getHotelOriginType()));
            }
            hotelOrderSummaryForm.setPartnerName(hotelOrderDto.getCName());

            hotelOrderSummaryForm.setTopLine(true);
            hotelOrderSummaryForm.setSummary(hotelOrderDto.getSummary());
            hotelOrderSummaryForm.setCostCenterName(hotelOrderDto.getCostCenterName());
            hotelOrderSummaryForm.setBillId(hotelOrderDto.getBillLiqId());
            hotelDateSet.add(hotelOrderSummaryForm);
        }
        return hotelDateSet;
    }

    @RequestMapping(value = "hotelDetail/{orderId}")
    public String hotelDetail(@PathVariable String orderId, Model model){
        HotelOrderDto order = hotelOrderService.getOrderDetail(orderId).getData();
        if(!order.isPerson()){
            //差旅类型
            DictCodeDto travel = dictService.getPartnerCode(Long.valueOf(order.getCId()), "TM_TRAVEL_TYPE", order.getCustomers().get(0).getTravelCode());
            //费用类型
            String costName = orgService.getCostCenterPathName(Long.valueOf(order.getCustomers().get(0).getOrgId()),order.getCustomers().get(0).getCostUnitCode());
            model.addAttribute("travel",travel);
            model.addAttribute("costName",costName);
        }
        if(order.getPaymentPlanNo() != null){
        	if(PaymentStatus.PAYMENT_SUCCESS.equals(order.getPaymentStatus())){
				model.addAttribute("paySuccess", true);
	        	BigDecimal bpTotalAmount=order.getBpTotalAmount()==null?BigDecimal.ZERO:order.getBpTotalAmount();
				model.addAttribute("tradeId",order.getPaymentPlanNo());
				if(BigDecimalUtils.subtract(order.getFinalTotalPrice(), bpTotalAmount).compareTo(BigDecimal.ZERO)>0){
					model.addAttribute("personalPayAmount",BigDecimalUtils.subtract(order.getFinalTotalPrice(), bpTotalAmount).doubleValue());
				}
			}
        }
        model.addAttribute("now",Calendar.getInstance().getTime());
        model.addAttribute("order",order);
        model.addAttribute("operations",hotelOrderService.findOrderLogList(orderId).getData());
        if(order.isManual()) {
        	model.addAttribute("prepayRule",order.getOrderPrepayRule().getDescription());
        }
        return "order/hotelOrderDetail.base";
    }

}
