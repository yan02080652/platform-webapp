package com.tem.order.action.form;

import java.io.Serializable;

public class InternationalFlightDetailForm implements Serializable{

    private String id;

    private String orderId;

    private String digital;

    private String flightNo;

    private String aircraftType;

    private String cabinCode;

    private String fromDate;

    private String toDate;

    private String fromAirportCode;

    private String fromTerminal;

    private String stopOver;

    private String stopCityName;

    private String stopTime;

    private String fromTime;

    private String toTime;

    private String toAirportCode;

    private String toTerminal;

    private String share;

    private String realFlightNo;


    //行李类型 1 无 2 件数 3 重量
    private Integer baggageType;

    //type==2
    private Integer pieceWeight;
    private Integer piece;

    //type==3
    private Integer weight;


    //退票规则
    private String refundRule;
    //改签规则
    private String changeRule;

    //特殊重点规则
    private String specialChangeRule;

    private String specialRefundRule;

    //转机点
    private boolean turningPoint;
    //行李直达
    private boolean baggageDirect;
    //过境签
    private boolean transitVisa;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getDigital() {
        return digital;
    }

    public void setDigital(String digital) {
        this.digital = digital;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getAircraftType() {
        return aircraftType;
    }

    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    public String getCabinCode() {
        return cabinCode;
    }

    public void setCabinCode(String cabinCode) {
        this.cabinCode = cabinCode;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getFromAirportCode() {
        return fromAirportCode;
    }

    public void setFromAirportCode(String fromAirportCode) {
        this.fromAirportCode = fromAirportCode;
    }

    public String getFromTerminal() {
        return fromTerminal;
    }

    public void setFromTerminal(String fromTerminal) {
        this.fromTerminal = fromTerminal;
    }

    public String getStopOver() {
        return stopOver;
    }

    public void setStopOver(String stopOver) {
        this.stopOver = stopOver;
    }

    public String getStopCityName() {
        return stopCityName;
    }

    public void setStopCityName(String stopCityName) {
        this.stopCityName = stopCityName;
    }

    public String getStopTime() {
        return stopTime;
    }

    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public String getToAirportCode() {
        return toAirportCode;
    }

    public void setToAirportCode(String toAirportCode) {
        this.toAirportCode = toAirportCode;
    }

    public String getToTerminal() {
        return toTerminal;
    }

    public void setToTerminal(String toTerminal) {
        this.toTerminal = toTerminal;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getRealFlightNo() {
        return realFlightNo;
    }

    public void setRealFlightNo(String realFlightNo) {
        this.realFlightNo = realFlightNo;
    }

    public String getRefundRule() {
        return refundRule;
    }

    public void setRefundRule(String refundRule) {
        this.refundRule = refundRule;
    }

    public String getChangeRule() {
        return changeRule;
    }

    public void setChangeRule(String changeRule) {
        this.changeRule = changeRule;
    }

    public Integer getBaggageType() {
        return baggageType;
    }

    public void setBaggageType(Integer baggageType) {
        this.baggageType = baggageType;
    }

    public Integer getPieceWeight() {
        return pieceWeight;
    }

    public void setPieceWeight(Integer pieceWeight) {
        this.pieceWeight = pieceWeight;
    }

    public Integer getPiece() {
        return piece;
    }

    public void setPiece(Integer piece) {
        this.piece = piece;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getSpecialChangeRule() {
        return specialChangeRule;
    }

    public void setSpecialChangeRule(String specialChangeRule) {
        this.specialChangeRule = specialChangeRule;
    }

    public String getSpecialRefundRule() {
        return specialRefundRule;
    }

    public void setSpecialRefundRule(String specialRefundRule) {
        this.specialRefundRule = specialRefundRule;
    }

    public boolean getTurningPoint() {
        return turningPoint;
    }

    public void setTurningPoint(boolean turningPoint) {
        this.turningPoint = turningPoint;
    }

    public boolean getBaggageDirect() {
        return baggageDirect;
    }

    public void setBaggageDirect(boolean baggageDirect) {
        this.baggageDirect = baggageDirect;
    }

    public boolean getTransitVisa() {
        return transitVisa;
    }

    public void setTransitVisa(boolean transitVisa) {
        this.transitVisa = transitVisa;
    }
}
