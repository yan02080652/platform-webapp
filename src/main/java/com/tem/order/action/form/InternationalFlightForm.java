package com.tem.order.action.form;

import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.tem.international.dto.InternationalFlightOrderDto;
import com.tem.international.dto.InternationalOrderFlightDetailDto;
import com.tem.international.dto.InternationalOrderPassengerDto;
import com.tem.international.enums.*;
import com.tem.international.interFlight.InternationalRefundChangeDetailDto;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Administrator
 */
public class InternationalFlightForm implements Serializable {

    private String id;

    private String cName;

    private String supplierName;

    private String issueChannel;

    private String supplierNo;

    private String pnrCode;

    private String singleTicketPrice;

    private String singleTaxFee;

    private String singleSettelPrice;

    private String singleSettelTaxFee;

    private String singleExtraFee;

    private String contactorName;

    private String contactorMobile;

    private String contactorEmail;

    private String tripType;

    //航段信息
    private List<InternationalFlightDetailForm> detailDtos;

    //乘机人信息
    private List<InternationalPassengerForm> passengerDtos;

    public InternationalFlightForm() {
    }

    public InternationalFlightOrderDto toOrderDto(){

        InternationalFlightOrderDto orderDto = new InternationalFlightOrderDto();
        orderDto.setId(id);
        orderDto.setcName(cName);
        orderDto.setSupplierName(supplierName);
        orderDto.setIssueChannel(issueChannel);
        orderDto.setPnrCode(pnrCode);
        orderDto.setExternalOrderId(supplierNo);
        orderDto.setSingleSettelPrice(new BigDecimal(singleSettelPrice));
        orderDto.setSingleSettelTaxFee(new BigDecimal(singleSettelTaxFee));
        orderDto.setSingleTicketPrice(new BigDecimal(singleTicketPrice));
        orderDto.setSingleTaxFee(new BigDecimal(singleTaxFee));
        orderDto.setSingleExtraFee(new BigDecimal(singleExtraFee));
        orderDto.setContactorName(contactorName);
        orderDto.setContactorEmail(contactorEmail);
        orderDto.setContactorMobile(contactorMobile);
        orderDto.setTripType(TripType.valueOf(tripType));

        for (InternationalFlightDetailForm form : detailDtos) {

            InternationalOrderFlightDetailDto detailDto = new InternationalOrderFlightDetailDto();
            detailDto.setId(StringUtils.isNotBlank(form.getId()) ? Integer.parseInt(form.getId()) : null);
            detailDto.setOrderId(form.getOrderId());
            detailDto.setDigital(StringUtils.isNotBlank(form.getDigital()) ? Integer.parseInt(form.getDigital()) : (detailDtos.indexOf(form) + 1));
            detailDto.setFlightNo(form.getFlightNo());
            detailDto.setAircraftType(form.getAircraftType());
            detailDto.setCabinCode(form.getCabinCode());
            detailDto.setInterDay(form.getFromDate().equals(form.getToDate()));
            detailDto.setFromDate(DateUtils.parse(form.getFromDate(),"yyyyMMdd"));
            detailDto.setToDate(DateUtils.parse(form.getToDate(),"yyyyMMdd"));
            detailDto.setFromTime(form.getFromTime());
            detailDto.setToTime(form.getToTime());
            detailDto.setFromAirportCode(form.getFromAirportCode());
            detailDto.setToAirportCode(form.getToAirportCode());
            detailDto.setFromTerminal(form.getFromTerminal());
            detailDto.setToTerminal(form.getToTerminal());
            detailDto.setStopOver(Boolean.valueOf(form.getStopOver()));
            detailDto.setStopTime(form.getStopTime());
            detailDto.setStopCityName(form.getStopCityName());
            detailDto.setShare(Boolean.valueOf(form.getShare()));
            detailDto.setRealFlightNo(form.getRealFlightNo());

            detailDto.setTurningPoint(form.getTurningPoint());

            detailDto.setBaggageDirect(form.getBaggageDirect());
            detailDto.setTransitVisa(form.getTransitVisa());


            //TODO delete
            detailDto.setRefundRule(form.getRefundRule());
            detailDto.setChangeRule(form.getChangeRule());
            detailDto.setSpecialRule(form.getSpecialChangeRule());
            detailDto.setBaggageRule(form.getBaggageType()+"");

            orderDto.getDetails().add(detailDto);

            //退改相关信息
            InternationalRefundChangeDetailDto refundChangeDetailDto = new InternationalRefundChangeDetailDto();
            if(form.getBaggageType() == 1) {
                refundChangeDetailDto.setBaggage("0");
                refundChangeDetailDto.setBaggagePieces("0");
            } else if(form.getBaggageType() == 2) {
                refundChangeDetailDto.setBaggage(form.getPieceWeight()+"");
                refundChangeDetailDto.setBaggagePieces(form.getPiece()+"");
            } else if(form.getBaggageType() == 3) {
                refundChangeDetailDto.setBaggagePieces("0");
                refundChangeDetailDto.setBaggage(form.getWeight()+"");
            }
            refundChangeDetailDto.setSpecialChangeRule(form.getSpecialChangeRule());
            refundChangeDetailDto.setSpecialRefundRule(form.getSpecialRefundRule());

            refundChangeDetailDto.setChangeRule(form.getChangeRule());
            refundChangeDetailDto.setRefundRule(form.getRefundRule());

            orderDto.getRefundRuleDtos().add(refundChangeDetailDto);

        }

        for (InternationalPassengerForm form : passengerDtos) {
            InternationalOrderPassengerDto passengerDto = new InternationalOrderPassengerDto();
            passengerDto.setId(StringUtils.isNotBlank(form.getId()) ? Integer.parseInt(form.getId()) : null);
            passengerDto.setOrderId(form.getOrderId());
            passengerDto.setUserId(StringUtils.isBlank(form.getUserId()) ? null : Long.parseLong(form.getUserId()));
            passengerDto.setName(form.getName());
            passengerDto.setSurName(form.getSurName());
            passengerDto.setGivenName(form.getGivenName());
            passengerDto.setGender("M".equals(form.getGender()) ? Gender.M : Gender.F);
            passengerDto.setNationality(form.getNationality());
            passengerDto.setBirthday(DateUtils.parse(form.getBirthday()));
            passengerDto.setCredentialType(CredentialType.valueOf(form.getCredentialType()));
            passengerDto.setCredentialNo(form.getCredentialNo());
            passengerDto.setMobile(form.getMobile());
            passengerDto.setDateOfExpiry(form.getDateOfExpiry());
            passengerDto.setTicketNo(form.getTicketNo());
            passengerDto.setEmpLevelCode(form.getEmpLevelCode());
            passengerDto.setTravellerType(form.getTravellerType());
            passengerDto.setTravelCode(form.getTravelType());
            passengerDto.setCostUnitCode(form.getCostUnitCode());
            passengerDto.setPassengerType(PassengerType.valueOf(form.getPassengerType()));
            passengerDto.setOrgId(form.getOrgId());
            passengerDto.setCorporationId(form.getCorporationId());
            //分摊金额
            passengerDto.setAmount(BigDecimalUtils.add(new BigDecimal[]{new BigDecimal(singleExtraFee),new BigDecimal(singleTicketPrice),new BigDecimal(singleTaxFee)}));
            //录入订单
            passengerDto.setTicketStatus(TicketStatus.USED);
            passengerDto.setStatus(InternationalPassengerStatus.ISSUED);

            orderDto.getPassengers().add(passengerDto);
        }

        return orderDto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIssueChannel() {
        return issueChannel;
    }

    public void setIssueChannel(String issueChannel) {
        this.issueChannel = issueChannel;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierNo() {
        return supplierNo;
    }

    public void setSupplierNo(String supplierNo) {
        this.supplierNo = supplierNo;
    }

    public String getPnrCode() {
        return pnrCode;
    }

    public void setPnrCode(String pnrCode) {
        this.pnrCode = pnrCode;
    }

    public String getSingleTicketPrice() {
        return singleTicketPrice;
    }

    public void setSingleTicketPrice(String singleTicketPrice) {
        this.singleTicketPrice = singleTicketPrice;
    }

    public String getSingleTaxFee() {
        return singleTaxFee;
    }

    public void setSingleTaxFee(String singleTaxFee) {
        this.singleTaxFee = singleTaxFee;
    }

    public String getSingleSettelPrice() {
        return singleSettelPrice;
    }

    public void setSingleSettelPrice(String singleSettelPrice) {
        this.singleSettelPrice = singleSettelPrice;
    }

    public String getSingleSettelTaxFee() {
        return singleSettelTaxFee;
    }

    public void setSingleSettelTaxFee(String singleSettelTaxFee) {
        this.singleSettelTaxFee = singleSettelTaxFee;
    }

    public String getSingleExtraFee() {
        return singleExtraFee;
    }

    public void setSingleExtraFee(String singleExtraFee) {
        this.singleExtraFee = singleExtraFee;
    }

    public String getContactorName() {
        return contactorName;
    }

    public void setContactorName(String contactorName) {
        this.contactorName = contactorName;
    }

    public String getContactorMobile() {
        return contactorMobile;
    }

    public void setContactorMobile(String contactorMobile) {
        this.contactorMobile = contactorMobile;
    }

    public String getContactorEmail() {
        return contactorEmail;
    }

    public void setContactorEmail(String contactorEmail) {
        this.contactorEmail = contactorEmail;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public List<InternationalFlightDetailForm> getDetailDtos() {
        return detailDtos;
    }

    public void setDetailDtos(List<InternationalFlightDetailForm> detailDtos) {
        this.detailDtos = detailDtos;
    }

    public List<InternationalPassengerForm> getPassengerDtos() {
        return passengerDtos;
    }

    public void setPassengerDtos(List<InternationalPassengerForm> passengerDtos) {
        this.passengerDtos = passengerDtos;
    }
}
