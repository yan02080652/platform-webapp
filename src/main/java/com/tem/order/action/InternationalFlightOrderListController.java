package com.tem.order.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.flight.Constants;
import com.tem.flight.api.AirportService;
import com.tem.flight.api.CabinService;
import com.tem.flight.api.CarrierService;
import com.tem.flight.dto.airport.AirportDto;
import com.tem.flight.dto.cabin.CabinDto;
import com.tem.flight.dto.carrier.CarrierDto;
import com.tem.international.InternationalRuleUtils;
import com.tem.international.SegmentUtils;
import com.tem.international.api.InternationalFlightOrderService;
import com.tem.international.dto.InternationalFlightOrderDto;
import com.tem.international.dto.InternationalOrderFlightDetailDto;
import com.tem.international.dto.InternationalOrderPassengerDto;
import com.tem.international.enums.CredentialType;
import com.tem.international.enums.OrderShowStatus;
import com.tem.international.enums.TripType;
import com.tem.international.interFlight.InternationalRefundChangeDetailDto;
import com.tem.international.order.BaggageGroup;
import com.tem.international.order.InternationCondition;
import com.tem.international.order.SegmentSimplyInfo;
import com.tem.order.action.form.InternationalFlightForm;
import com.tem.order.form.AllOrderQueryForm;
import com.tem.order.form.InternationalFlightOrderShowForm;
import com.tem.order.form.InternationalFlightOrderSummaryForm;
import com.tem.order.util.ExportInfo;
import com.tem.payment.enums.PayChannel;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.OrgCostcenterService;
import com.tem.platform.api.OrgService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.condition.UserCondition;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.platform.api.dto.OrgCostcenterDto;
import com.tem.platform.api.dto.OrgDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.OrderIllegalInfoService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.OrderIllegalInfoDto;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping(value = "order")
public class InternationalFlightOrderListController extends BaseController {

    public final Logger logger = LoggerFactory.getLogger(InternationalFlightOrderListController.class);

    @Autowired
    private InternationalFlightOrderService internationalFlightOrderService;

    @Autowired
    private IssueChannelService issueChannelService;

    @Autowired
    private DictService dictService;

    @Autowired
    private OrgCostcenterService orgCostcenterService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrgService orgService;

    @Autowired
    private AirportService airportService;

    @Autowired
    private RefundOrderListController refundOrderListController;

    @Autowired
    private CarrierService carrierService;

    @Autowired
    private OrderIllegalInfoService orderIllegalInfoService;

    @Autowired
    private CabinService cabinService;



    @RequestMapping("/internationalFlightOrder")
    public String index(Model model){

        OrderShowStatus[] showStatus = new OrderShowStatus[]{OrderShowStatus.WAIT_PAYMENT,OrderShowStatus.ISSUED};
        model.addAttribute("showStatus",showStatus);

        List<IssueChannelDto> issueChannels = issueChannelService.findListByChannelType("INTERNATIONAL_FLIGHT");
        model.addAttribute("issueChannels",issueChannels);

        return "order/internationalFlight/internationalFlightOrder.base";
    }

    @RequestMapping("internationalFlightList")
    public String list(Model model, Integer pageIndex, Integer pageSize, AllOrderQueryForm condition){

        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }

        QueryDto<InternationCondition> dto = new QueryDto<>(pageIndex,pageSize);
        dto.setCondition(condition.toInternationalConditon());

        Page<InternationalFlightOrderDto> page = internationalFlightOrderService.findOrderList(dto);
        List<InternationalFlightOrderShowForm> list = transOrdersFormList(page.getList());

        Page<InternationalFlightOrderShowForm> page1 = new Page<>(list,pageIndex, pageSize,page.getTotal());
        model.addAttribute("pageList",page1);

        model.addAttribute("currentUserId", super.getCurrentUser().getId());

        return "order/internationalFlight/internationalFlightList.jsp";
    }


    private List<InternationalFlightOrderShowForm> transOrdersFormList(List<InternationalFlightOrderDto> orderDtos){

        List<InternationalFlightOrderShowForm> flightDateSet = new ArrayList<>();

        List<DictCodeDto> berths = dictService.getCodes(Constants.OTAF_CABIN_GRADE);
        int i = 0;
        for(InternationalFlightOrderDto flightOrder : orderDtos){
            InternationalFlightOrderShowForm flightOrderSummaryForm = new InternationalFlightOrderShowForm();
            i++;
            flightOrderSummaryForm.setId(i);
            flightOrderSummaryForm.setOrderId(flightOrder.getId());
            if(StringUtils.isNotEmpty(flightOrder.getPaymentMethod())){
            	String[] paymentMethods = flightOrder.getPaymentMethod().split(",");
            	String methods="";
            	for(String paymentMethod: paymentMethods){
            		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
            	}
                flightOrderSummaryForm.setPaymentMethod(methods.substring(0,methods.length()-1));
            }
            flightOrderSummaryForm.setUserName(flightOrder.getUserName());
            flightOrderSummaryForm.setCreateDate(flightOrder.getCreateTime());
            String passengers = "";
            String titckNos = "";
            flightOrderSummaryForm.setPassengerCount(flightOrder.getPassengers().size());

            if(CollectionUtils.isNotEmpty(flightOrder.getPassengers())) {
                for(InternationalOrderPassengerDto passengerDto : flightOrder.getPassengers()) {
                    if (StringUtils.isNotEmpty(passengerDto.getName())) {
                        passengers = passengers + passengerDto.getName() + "、";
                    }
                    if(StringUtils.isNotEmpty(passengerDto.getTicketNo())){
                        titckNos = titckNos + passengerDto.getTicketNo() + "、";
                    }
                }
            }
            if(StringUtils.isNotEmpty(titckNos)){
                titckNos = titckNos.substring(0,titckNos.length()-1);
            }
            if(StringUtils.isNotEmpty(passengers)){
                passengers = passengers.substring(0,passengers.length()-1);
            }
            flightOrderSummaryForm.setTitckNos(titckNos);
            flightOrderSummaryForm.setPassengers(passengers);
            if(CollectionUtils.isNotEmpty(flightOrder.getDetails())){
                flightOrderSummaryForm.setCarrierName(flightOrder.getDetails().get(0).getCarrierName());
                flightOrderSummaryForm.setFlightNo(flightOrder.getDetails().get(0).getFlightNo());
                flightOrderSummaryForm.setFromDate(flightOrder.getDetails().get(0).getFromDate());

                String trip = "";
                for (InternationalOrderFlightDetailDto detailDto : flightOrder.getDetails()) {
                    trip += detailDto.getFromCityName() + "-" +detailDto.getToCityName() + "  ";
                }
                flightOrderSummaryForm.setTrip(trip);

                for(DictCodeDto dc : berths){
                    if(dc.getItemCode().equals(flightOrder.getDetails().get(0).getCabinClass())){
                        flightOrderSummaryForm.setBerth(dc.getItemTxt());
                    }
                }
            }
            flightOrderSummaryForm.setTotalAmount(flightOrder.getTotalSalePrice().doubleValue());
            flightOrderSummaryForm.setSupSettleAmount(flightOrder.getTotalSettlePrice() == null ? 0 : flightOrder.getTotalSettlePrice().doubleValue());
            flightOrderSummaryForm.setOrderState(flightOrder.getOrderShowStatus().getMessage());
            flightOrderSummaryForm.setExternalOrderId(flightOrder.getExternalOrderId());
            flightOrderSummaryForm.setPnr(flightOrder.getPnrCode());

            flightOrderSummaryForm.setSupName(flightOrder.getSupplierName());

            flightOrderSummaryForm.setPartnerName(flightOrder.getcName());
            flightOrderSummaryForm.setTopLine(true);
            flightOrderSummaryForm.setSummary(flightOrder.getSummary());
            flightOrderSummaryForm.setCostCenterName(flightOrder.getCostCenterName());
            flightOrderSummaryForm.setBillId(flightOrder.getBillId());
            flightDateSet.add(flightOrderSummaryForm);
        }
        return flightDateSet;

    }

    @RequestMapping(value = "internaltionalFlightOrder/{orderId}")
    public String detail(@PathVariable String orderId, Model model){

        InternationalFlightOrderDto order = internationalFlightOrderService.findFlightOrderById(orderId);

        //出行类别
        List<DictCodeDto> travelTypes = dictService.getPartnerCodes(Long.parseLong(order.getcId()), "TM_TRAVEL_TYPE");
        //预订人的成本中心
        List<OrgCostcenterDto> costCenter = orgCostcenterService.findByUserId(Long.parseLong(order.getcId()), Long.parseLong(order.getUserId()));
        //职级列表
        List<DictCodeDto> empLevelList = dictService.getPartnerCodes(Long.parseLong(order.getcId()), "TM_EMPLOYEE_LEVEL");

        List<OrgDto> top = orgService.findOrgs(Long.parseLong(order.getcId()), null);

        String centerName = null;
        if(StringUtils.isNotEmpty(order.getPassengers().get(0).getCostUnitCode())) {
            centerName = orgCostcenterService.getCostCenterName(Long.parseLong(order.getPassengers().get(0).getCostUnitCode()));
        }

        List<IssueChannelDto> channels = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(Long.parseLong(order.getTmcId()), IssueChannelType.INTERNATIONAL_FLIGHT.name(), LineType.OFF_LINE.getType());

        model.addAttribute("channels",channels);

        model.addAttribute("centerName",centerName);
        model.addAttribute("credentialTypeMap", CredentialType.toMap());
        model.addAttribute("tripTypes", TripType.values());
        model.addAttribute("order",order);
        model.addAttribute("travelTypes",travelTypes);
        model.addAttribute("costCenter",costCenter);
        model.addAttribute("empLevelList",empLevelList);
        model.addAttribute("topPatern", top.get(0));
        model.addAttribute("numMap",numToChn());
        model.addAttribute("weights", SegmentUtils.weights);
        model.addAttribute("pieces", SegmentUtils.pieces);

        return "order/internationalFlight/internationalFlightDetail.base";
    }

    private Map<Integer,String> numToChn(){
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"一");
        map.put(2,"二");
        map.put(3,"三");
        map.put(4,"四");
        map.put(5,"五");
        map.put(6,"六");
        map.put(7,"七");
        map.put(8,"八");
        map.put(9,"九");
        map.put(10,"十");

        return map;
    }


    @RequestMapping(value = "updateInternationalOrder")
    @ResponseBody
    public boolean update(@RequestBody InternationalFlightForm orderForm){

        //修改后数据
        InternationalFlightOrderDto formDto = orderForm.toOrderDto();
        //销售总价
        formDto.setTotalSalePrice(BigDecimalUtils.multiply(
                BigDecimalUtils.add(new BigDecimal[]{formDto.getSingleTicketPrice(), formDto.getSingleTaxFee(), formDto.getSingleExtraFee()})
                , formDto.getPassengers().size()));
        //结算总价
        formDto.setTotalSettlePrice(BigDecimalUtils.multiply(
                BigDecimalUtils.add(new BigDecimal[]{formDto.getSingleSettelPrice(), formDto.getSingleSettelTaxFee()})
                , formDto.getPassengers().size()));
        //总服务费
        formDto.setServicePrice(BigDecimalUtils.multiply(formDto.getSingleExtraFee(), formDto.getPassengers().size()));
        formDto.setServiceId(String.valueOf(super.getCurrentUser().getId()));

        //重新设置城市 name code 数据等
        for (InternationalOrderFlightDetailDto detailDto : formDto.getDetails()) {
            AirportDto fromAirport = airportService.findAirportByCode(detailDto.getFromAirportCode(),null);
            if(fromAirport != null) {
                detailDto.setFromCityName(fromAirport.getCityName());
                detailDto.setFromAirportName(fromAirport.getNameCn());
            }
            AirportDto toAirport = airportService.findAirportByCode(detailDto.getToAirportCode(),null);
            if(toAirport != null) {
                detailDto.setToCityName(toAirport.getCityName());
                detailDto.setToAirportName(toAirport.getNameCn());
            }

            if(detailDto.getFlightNo().length() > 2) {
                CarrierDto carrierDto = carrierService.findByCode(detailDto.getFlightNo().substring(0, 2));
                if(carrierDto != null) {
                    detailDto.setCarrierName(carrierDto.getNameShort());
                }
                CabinDto cabinDto = cabinService.findByCabinCodeAndCarrierCode(detailDto.getCabinCode(), detailDto.getFlightNo().substring(0, 2));
                if(cabinDto != null) {
                    detailDto.setCabinType(cabinDto.getType());
                    detailDto.setCabinClass(cabinDto.getGrade());
                }
            }

        }

        boolean boo = false;
        try {
            boo = internationalFlightOrderService.modifyOrder(formDto);
        } catch (Exception e) {
            LogUtils.error(logger,"修改国际机票订单发生异常 异常信息={}", e );
            return boo;
        }
        return boo;
    }

    @RequestMapping("exportInterFlight")
    public void exportFlight(AllOrderQueryForm condition){
        List<ExportInfo> infoList = new ArrayList<>();

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }
        long start = System.currentTimeMillis();
        QueryDto<InternationCondition> dto = new QueryDto<>(1,Integer.MAX_VALUE);
        dto.setCondition(condition.toInternationalConditon());

        Page<InternationalFlightOrderDto> page = internationalFlightOrderService.findOrderList(dto);
        List<InternationalFlightOrderShowForm> list = transOrdersFormList(page.getList());

        List<InternationalFlightOrderSummaryForm> exportList = TransformUtils.transformList(list, InternationalFlightOrderSummaryForm.class);
        long end = System.currentTimeMillis();
        LogUtils.debug(logger, "国际机票导出查询列表数据耗时：{}", end - start);
        String[] flightHeader = {"序号", "订单号","支付方式","预订人","预订日期","乘机人","乘机人数"
                ,"航司","舱位名称","航班号","起飞时间","行程","所属客户","客户结算价","所属供应商","供应商结算价","订单状态","客户账单ID","关联单号(退/改/原始)","供应商订单号","PNR","票号","所属成本中心"};

        ExportInfo<InternationalFlightOrderSummaryForm> flightExport = new ExportInfo<>();
        flightExport.setDataset(exportList);
        flightExport.setHeaders(flightHeader);
        flightExport.setTitle("国际机票");
        infoList.add(flightExport);

        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String fileName = "国际机票" + uuid + ".xlsx";

        refundOrderListController.goToExport(infoList,fileName,getRequest(),getResponse());

    }

    @ResponseBody
    @RequestMapping("inter/findUserByCondition")
    public Map<String,Object> findUserByCondition(String key,Long cId){

        Map<String,Object> data = new HashMap<>();
        List<UserDto> userList = userService.findUserList(cId, key);
        List<OrgDto> orgList = new ArrayList<>();


        if(org.apache.commons.lang3.StringUtils.isNotBlank(key)) {
            Map<String, Object> map = new HashMap<>();
            map.put("partnerId", super.getPartnerId());
            map.put("status", 0);
            map.put("orgName", key);
            orgList = orgService.getList(map);
        } else {
            orgList = orgService.getChildren(orgService.findOrgs(cId, null).get(0).getId());
            userList = userService.findUserListWithOrg(cId, "");
        }

        Map<Long,Integer> orgCountMap = new HashMap<>();
        for(OrgDto orgDto : orgList) {
            orgCountMap.put(orgDto.getId(),userService.getUserCountByOrgId(orgDto.getId()));
        }

        if(CollectionUtils.isEmpty(userList)) {
            userList = userService.findUserListWithOrg(cId, "");
        }

        data.put("users",userList);
        data.put("orgs",orgList);
        data.put("countMap",orgCountMap);

        return data;
    }

    @ResponseBody
    @RequestMapping("inter/findByUserId")
    public UserDto findByUserId(Long id){

        UserDto user = userService.getUser(id);

        return user;
    }

    @RequestMapping(value = "inter/findOrgAndUserByOrgId")
    @ResponseBody
    public Map<String,Object> findOrgAndUserByOrgId(Long orgId,Long cId){
        Map<String,Object> map = new HashMap<>();
        List<OrgDto> top = orgService.findOrgs(cId, null);
        QueryDto<UserCondition> queryDto = new QueryDto<>();
        UserCondition condition = new UserCondition();
        condition.setOrgId(top.get(0).getId());
        condition.setPartnerId(cId);
        queryDto.setCondition(condition);
        Page<UserDto> userDtoPage = userService.queryListWithPage(queryDto);
        map.put("topUser",userDtoPage.getList());
        if(orgId == null) {

            List<OrgDto> children = orgService.getChildren(top.get(0).getId());
            Map<Long,Integer> orgCountMap = new HashMap<>();
            for(OrgDto orgDto : children) {
                orgCountMap.put(orgDto.getId(),userService.getUserCountByOrgId(orgDto.getId()));
            }
            QueryDto<UserCondition> query = new QueryDto<>();
            UserCondition con = new UserCondition();
            con.setPartnerId(cId);
            con.setOrgId(orgId);
            query.setCondition(con);
            Page<UserDto> userpage = userService.queryListWithPage(query);
            map.put("orgs",children);
            map.put("users",userpage.getList());
            map.put("countMap",orgCountMap);
        }else {

            List<OrgDto> children = orgService.getChildren(orgId);
            QueryDto<UserCondition> query = new QueryDto<>();
            UserCondition con = new UserCondition();
            con.setPartnerId(cId);
            con.setOrgId(orgId);
            query.setCondition(con);
            Page<UserDto> userpage = userService.queryListWithPage(query);
            map.put("orgs",children);
            map.put("users",userpage.getList());
            Map<Long,Integer> orgCountMap = new HashMap<>();
            for(OrgDto orgDto : children) {
                orgCountMap.put(orgDto.getId(),userService.getUserCountByOrgId(orgDto.getId()));
            }
            map.put("countMap",orgCountMap);
        }
        return map;
    }

    @ResponseBody
    @RequestMapping("inter/expenseTree")
    public List<OrgDto> expenseTree(@RequestParam("cId") Long cId) {

        return orgService.expenseTree(cId);
    }

    @RequestMapping("inter/orderDetail/{orderId}")
    public String orderDetail(Model model, @PathVariable String orderId){

        //订单详情
        InternationalFlightOrderDto orderDto = internationalFlightOrderService.findFlightOrderById(orderId);

        //违规详情
        List<OrderIllegalInfoDto> illegalInfoList = orderIllegalInfoService.getIllegalInfoByOrderId(orderId);

        List<String> dayStr = getWeekList(orderDto);

//        if(orderDto.getPaymentMethod()!=null){
//            PayChannel payChannel = PayChannel.valueOf(orderDto.getPaymentMethod());
//            String name = payChannel.getValue();
//            orderDto.setPaymentMethod(name);
//        }

        if(StringUtils.isNotEmpty(orderDto.getPaymentMethod())){
            String[] paymentMethods = orderDto.getPaymentMethod().split(",");
            String methods="";
            for(String paymentMethod: paymentMethods){
                methods += PayChannel.valueOf(paymentMethod).getValue() +"/";
            }
            orderDto.setPaymentMethod(methods.substring(0,methods.length()-1));
        }

        //退改规定是否公用同一航段
        Boolean isShare = true;
        List<InternationalOrderFlightDetailDto> details = orderDto.getDetails();
        for(int i = 0; i < details.size(); i++){
            if(!String.valueOf(details.get(i).getRefundRule()).equals(String.valueOf(details.get(0).getRefundRule()))) {
                isShare = false;
            }
        }

        if(StringUtils.isNotEmpty(orderDto.getPassengers().get(0).getTravelCode())) {
            //差旅类型
            DictCodeDto dictCodeDto = dictService.getPartnerCode(Long.parseLong(orderDto.getcId()), Constants.TM_TRAVEL_TYPE, orderDto.getPassengers().get(0).getTravelCode());
            if (dictCodeDto != null) {
                model.addAttribute("travelType", dictCodeDto.getItemTxt());
            }
        }
        if(StringUtils.isNotEmpty(orderDto.getPassengers().get(0).getOrgId())) {
            //费用类型
            String orgName = orgService.getOrgNameByPath(orgService.getById(Long.valueOf(orderDto.getPassengers().get(0).getOrgId())).getPath());
            model.addAttribute("orgName", orgName);
        }

        if(CollectionUtils.isNotEmpty(orderDto.getRefundRuleDtos())) {
            InternationalRefundChangeDetailDto stringentRule = InternationalRuleUtils.getStringentRule(orderDto.getRefundRuleDtos());
            Map<BaggageGroup, List<SegmentSimplyInfo>> groupMaps = InternationalRuleUtils.getBaggageGroups(orderDto.getRefundRuleDtos());
            model.addAttribute("stringentRule",stringentRule);
            model.addAttribute("baggageMap",groupMaps);
        }

        model.addAttribute("isShare",isShare);
        model.addAttribute("orderDto", orderDto);
        model.addAttribute("weekList", dayStr);
        model.addAttribute("illegalInfoList", illegalInfoList);
        model.addAttribute("hidden",CollectionUtils.isEmpty(illegalInfoList));

        boolean modify = true;
        if (OrderShowStatus.WAIT_PAYMENT.equals(orderDto.getOrderShowStatus())
                || OrderShowStatus.WAIT_OFFER.equals(orderDto.getOrderShowStatus())) {
            if (orderDto.getTask() != null && orderDto.getTask().getOrderId() != null) {
                if (!super.getCurrentUser().getId().toString().equals(orderDto.getTask().getOrderId())) {
                    modify = false;
                }
            }
        }
        model.addAttribute("modify", modify);

        return "flight/tmc/international/internationalOrderDetail.base";
    }
    

    private List<String> getWeekList(InternationalFlightOrderDto orderDto){
        List<String> dayStr = new ArrayList<>();
        for(InternationalOrderFlightDetailDto detail : orderDto.getDetails()){
            Date flyDate = detail.getFlyDate();
            int weekDay = DateUtils.getWeekDay(flyDate);
            dayStr.add(Arrays.asList("日","一","二","三","四","五","六").get(weekDay));
            if(StringUtils.isEmpty(detail.getStopTime())){
                continue;
            }
            //修改经停时间为 1h20m
            String stopTime = detail.getStopTime();

            long num = Integer.valueOf(stopTime) * 1000L*60L;

            StringBuilder strBuilder = new StringBuilder();
            //天数
            long day = num / (24 * 60 * 60 * 1000);
            //小时数
            long hour = (num%(24 * 60 * 60 * 1000))/(60 * 60 * 1000);
            //分钟数
            long min = (num % (60 * 60 * 1000)) / (60 * 1000);
            if(day != 0){
                strBuilder.append(day).append("d");
            }
            if(hour!=0){
                strBuilder.append(hour).append("h");
            }
            if(min != 0){
                strBuilder.append(min).append("m");
            }

            detail.setStopTime(strBuilder.toString());
        }
        return dayStr;
    }
}