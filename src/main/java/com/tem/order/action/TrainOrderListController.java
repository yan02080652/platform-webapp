package com.tem.order.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.iplatform.common.utils.BigDecimalUtils;
import com.tem.pss.enums.TravelTypeEnum;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.order.form.AllOrderQueryForm;
import com.tem.order.form.TrainOrderShowForm;
import com.tem.order.form.TrainOrderSummaryForm;
import com.tem.order.service.PartnerOrderListService;
import com.tem.order.util.ExportInfo;
import com.tem.payment.enums.PayChannel;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.DictService;
import com.tem.platform.api.OrgCostcenterService;
import com.tem.platform.api.dto.DictCodeDto;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.refund.RefundOrderDto;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import com.tem.train.api.order.TrainOrderService;
import com.tem.train.dto.order.TrainOrderCondition;
import com.tem.train.dto.order.TrainOrderDto;
import com.tem.train.dto.order.TrainPassengerDto;
import com.tem.train.dto.order.enums.TicketType;

/**
 * @author chenjieming
 * @date 2017/8/1
 */
@Controller
@RequestMapping("order")
public class TrainOrderListController extends BaseController {

    @Autowired
    private TrainOrderService trainOrderService;

    @Autowired
    private DictService dictService;

    @Autowired
    private OrgCostcenterService orgCostcenterService;

    @Autowired
    private RefundOrderListController refundOrderListController;

    @Autowired
    private PartnerOrderListService orderListService;

    @Autowired
    private RefundOrderService refundOrderService;

    @Autowired
    private IssueChannelService issueChannelService;

    public final Logger logger = LoggerFactory.getLogger(TrainOrderListController.class);
    
    @RequestMapping("/trainOrder")
    public String trainOrder(Model model) {
        model.addAttribute("showStatus", com.tem.train.dto.order.enums.OrderShowStatus.values());
        this.getIssues(model);

        return "order/trainOrder.base";
    }

    @RequestMapping("/trainOrderList")
    public String trainOrderList(Model model, Integer pageIndex, Integer pageSize, AllOrderQueryForm condition) {

        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()) {
            condition.setTmcId(super.getTmcId().toString());
        }

        Page<TrainOrderShowForm> pageList = this.getMergeTrainOrder(pageIndex, pageSize, condition);

        //退款
        List<TrainOrderShowForm> mergeList = pageList.getList();

        List<TrainOrderShowForm> extraRefundOrderList = this.getExtraRefundOrderList(condition, "12");

        int fPage = pageList.getTotalPage();
        int rCount = extraRefundOrderList.size(), fCount = pageList.getTotal();
        int totalCount = fCount + rCount;

        mergeList = orderListService.getPageList(mergeList, extraRefundOrderList, pageIndex, fPage, totalCount);

        Page<TrainOrderShowForm> pageList4 = new Page<>(pageList.getStart(), pageList.getSize(),
                mergeList, totalCount);

        model.addAttribute("pageList", pageList4);

        model.addAttribute("userId", super.getCurrentUser().getId());

        //获得出票渠道
        List<com.tem.pss.dto.IssueChannelDto> onLineList = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(),
                IssueChannelType.TRAIN.getType(), LineType.ON_LINE.getType());
        if (!CollectionUtils.isEmpty(onLineList)) {
            Map<String, String> onLineMap = new HashMap<>(16);
            for (com.tem.pss.dto.IssueChannelDto channel : onLineList) {
                onLineMap.put(channel.getCode(), channel.getName());
            }
            model.addAttribute("issueChannelMap", onLineMap);
        }

        return "order/trainOrderList.jsp";
    }

    /**
     * 火车票的出票渠道
     *
     * @param model
     */
    private void getIssues(Model model) {
        List<IssueChannelDto> issues = issueChannelService.findListByChannelType("TRAIN");
        model.addAttribute("issues", issues);
    }

    /**
     * 正向火车票和退单合并
     *
     * @param pageIndex
     * @param pageSize
     * @param condition
     * @return
     */
    private Page<TrainOrderShowForm> getMergeTrainOrder(Integer pageIndex, Integer pageSize, AllOrderQueryForm condition) {

        QueryDto<TrainOrderCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(condition.toTrainOrderCondition());

        try {
            if (StringUtils.isNotEmpty(condition.getKeyword())) {
                RefundOrderDto refundOrderDto = refundOrderService.getById(condition.getKeyword());
                if (refundOrderDto.getOrderId() != null) {
                    queryDto.getCondition().setOrderNumber(refundOrderDto.getOrderId());
                }
            }
        } catch (Exception ex) {

        }

        Page<TrainOrderDto> pageList = trainOrderService.findOrderList(queryDto);
        List<TrainOrderShowForm> trainOrderList = transTrainOrderSummaryFormList(pageList.getList());

        condition.setType("12");
        condition.setOrderStatus(null);
        Page<RefundOrderDto> refundPageList = refundOrderListController.getMergeRefundList(1, Integer.MAX_VALUE, condition);

        List<TrainOrderShowForm> mergeList = new ArrayList<>();

        Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("TRAIN");
        
        for (TrainOrderShowForm order : trainOrderList) {

            mergeList.add(order);

            for (RefundOrderDto refundOrder : refundPageList.getList()) {
                if (order.getOrderId().equals(refundOrder.getOrderId())) {
                    mergeList.add(orderListService.refundOrderToTrain(refundOrder, order,issueChannelMap));
                }
            }
        }

        return new Page<>(pageList.getStart(), pageList.getSize(), mergeList, pageList.getTotal());
    }

    /**
     * 获取在这个时间段内的额外退单
     *
     * @param condition
     * @param orderType
     * @return
     */
    private List<TrainOrderShowForm> getExtraRefundOrderList(AllOrderQueryForm condition, String orderType) {
        List<TrainOrderShowForm> extraOrderInfo = new ArrayList<>();
        if (condition.getStartDate() != null && condition.getEndDate() != null) {

        	Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("TRAIN");
        	
            List<RefundOrderDto> refundOrderList = refundOrderListController.queryExtraRefundOrder(condition, orderType);
            for (RefundOrderDto refundOrderInfo : refundOrderList) {
                TrainOrderShowForm orderInfoShow = orderListService.refundOrderToTrain(refundOrderInfo, null,issueChannelMap);
                orderInfoShow.setTopLine(true);
                extraOrderInfo.add(orderInfoShow);
            }
        }
        return extraOrderInfo;
    }

    @RequestMapping("exportTrain")
    public void exportTrain(AllOrderQueryForm condition) {
        List<ExportInfo> infoList = new ArrayList<>();

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }
        long start = System.currentTimeMillis();
        Page<TrainOrderShowForm> pageList = this.getMergeTrainOrder(1, Integer.MAX_VALUE, condition);

        List<TrainOrderShowForm> orderList = this.getExtraRefundOrderList(condition, "12");
        pageList.getList().addAll(orderList);

        List<TrainOrderSummaryForm> dateSet = TransformUtils.transformList(pageList.getList(), TrainOrderSummaryForm.class);
        long end = System.currentTimeMillis();
        LogUtils.debug(logger, "火车订单导出查询列表数据耗时：{}", end - start);
        
        String[] trainHeader = {"序号", "订单号", "原始订单号", "订单类型", "支付方式", "预订人", "预订日期", "出行人", "出行人数", "行程"
                , "车次号", "开车时间", "所属客户", "客户结算价", "所属供应商", "供应商结算价", "订单状态","客户账单ID", "供应商订单号", "票号","所属成本中心"};

        ExportInfo<TrainOrderSummaryForm> trainExport = new ExportInfo<>();
        trainExport.setDataset(dateSet);
        trainExport.setHeaders(trainHeader);
        trainExport.setTitle("火车订单");
        infoList.add(trainExport);

        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String fileName = "火车票订单" + uuid + ".xlsx";

        refundOrderListController.goToExport(infoList, fileName, getRequest(), getResponse());
    }

    /**
     * 火车票dto转换为列表显示form
     *
     * @param trainOrderList
     * @return
     */
    private List<TrainOrderShowForm> transTrainOrderSummaryFormList(List<TrainOrderDto> trainOrderList) {

    	Map<String,String> issueChannelMap = orderListService.getIssueChannelMap("TRAIN");
    	
        List<TrainOrderShowForm> flightDateSet = new ArrayList<>();
        int i = 0;
        for (TrainOrderDto trainOrderDto : trainOrderList) {
            TrainOrderShowForm trainOrderSummaryForm = new TrainOrderShowForm();
            i++;
            trainOrderSummaryForm.setId(i);
            trainOrderSummaryForm.setOrderId(trainOrderDto.getId());
            if (StringUtils.isNotEmpty(trainOrderDto.getPaymentMethod())) {
            	String[] paymentMethods = trainOrderDto.getPaymentMethod().split(",");
            	String methods="";
            	for(String paymentMethod: paymentMethods){
            		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
            	}
            	trainOrderSummaryForm.setPaymentMethod(methods.substring(0,methods.length()-1));
            }
            trainOrderSummaryForm.setUserName(trainOrderDto.getUserName());
            trainOrderSummaryForm.setCreateDate(trainOrderDto.getCreateDate());
            String passengers = "";
            String titckNos = "";
            trainOrderSummaryForm.setPassengerCount(0);
            if (CollectionUtils.isNotEmpty(trainOrderDto.getPassengerList())) {
                for (TrainPassengerDto passengerDto : trainOrderDto.getPassengerList()) {
                    if (StringUtils.isNotEmpty(passengerDto.getName())) {
                        passengers = passengers + passengerDto.getName() + "、";
                    }
                    if (passengerDto.getIsNoSeat() != null && passengerDto.getIsNoSeat()) {
                        //无座
                        if (passengerDto.getTrainBox() != null) {
                            titckNos = titckNos + passengerDto.getTrainBox() + "无座" + "、";
                        }
                    } else if (passengerDto.getIsNoSeat() != null) {
                        if (passengerDto.getTrainBox() != null && passengerDto.getSeatNo() != null) {
                            titckNos = titckNos + passengerDto.getTrainBox() + passengerDto.getSeatNo() + "、";
                        }
                    }
                }
                if (StringUtils.isNotEmpty(titckNos)) {
                    titckNos = titckNos.substring(0, titckNos.length() - 1);
                }
                if (StringUtils.isNotEmpty(passengers)) {
                    passengers = passengers.substring(0, passengers.length() - 1);
                }
                trainOrderSummaryForm.setPassengerCount(trainOrderDto.getPassengerList().size());

            }
            trainOrderSummaryForm.setTitckNos(titckNos);
            trainOrderSummaryForm.setPassengers(passengers);
            trainOrderSummaryForm.setTrainNo(trainOrderDto.getTrainCode());
            trainOrderSummaryForm.setFromDate(trainOrderDto.getFromTime());
            trainOrderSummaryForm.setTrip(trainOrderDto.getFromStation() + "-" + trainOrderDto.getArriveStation());

            trainOrderSummaryForm.setTotalAmount(trainOrderDto.getTotalAmount() == null ? 0 : trainOrderDto.getTotalAmount().doubleValue());
            trainOrderSummaryForm.setSupSettleAmount(trainOrderDto.getTotalSettlePrice() == null ? 0 : trainOrderDto.getTotalSettlePrice().doubleValue());
            trainOrderSummaryForm.setOrderState(trainOrderDto.getOrderShowStatus().getMessage());
            trainOrderSummaryForm.setExternalOrderId(trainOrderDto.getOutOrderId());

            if (trainOrderDto.getIssueChannelType() != null) {
                trainOrderSummaryForm.setSupName(issueChannelMap.get(trainOrderDto.getIssueChannelType()));
            }
            trainOrderSummaryForm.setPartnerName(trainOrderDto.getcName());
            trainOrderSummaryForm.setSummary(trainOrderDto.getSummary());
            trainOrderSummaryForm.setTopLine(true);

            trainOrderSummaryForm.setCostCenterName(trainOrderDto.getCostCenterName());
            trainOrderSummaryForm.setBillId(trainOrderDto.getBillId());

            // 添加原始订单号和订单状态
            if(StringUtils.isNotEmpty(trainOrderDto.getOldOrderId())){
                trainOrderSummaryForm.setOriginOrderId(trainOrderDto.getOldOrderId());
                trainOrderSummaryForm.setOrderShowType("改签订单");
            }else{
            	trainOrderSummaryForm.setOrderShowType("出票订单");
            }
            
            flightDateSet.add(trainOrderSummaryForm);
        }
        return flightDateSet;
    }

    /**
     * 订单详情页面
     *
     * @param model
     * @param orderId
     * @return
     */
    @RequestMapping("/trainDetail/{orderId}")
    public String trainDetail(Model model, @PathVariable String orderId) {
        TrainOrderDto trainOrderDto = trainOrderService.getTrainOrderById(orderId);
        String ticketDetail = "";
        int count = trainOrderDto.getPassengerList().size();
        int adultCount = 0;
        for (TrainPassengerDto pass : trainOrderDto.getPassengerList()) {
            if (pass.getTicketType().equals(TicketType.ADULT)) {
                adultCount++;
            }
        }
        if (adultCount > 0) {
            ticketDetail = adultCount + "名成人";
        }
        if (count - adultCount > 0) {
            ticketDetail += "," + (count - adultCount) + "名儿童";
        }
       if(TravelTypeEnum.BUSINESS == trainOrderDto.getTravelType()){
           //差旅等级
           TrainPassengerDto passengerDto = trainOrderDto.getPassengerList().get(0);
           DictCodeDto travel = dictService.getPartnerCode(Long.valueOf(trainOrderDto.getcId()), "TM_TRAVEL_TYPE", passengerDto.getTravelCode());
           String orgId = passengerDto.getOrgId();

           //费用归属
           String orgPath = orgCostcenterService.getCostCenterPathName(Long.valueOf(orgId), passengerDto.getCostUnitCode());
           model.addAttribute("travel", travel);
           model.addAttribute("orgPath", orgPath);
       }
        if(trainOrderDto.getBpTotalAmount()!=null && trainOrderDto.getTotalAmount().doubleValue()-trainOrderDto.getBpTotalAmount().doubleValue() > 0){
            model.addAttribute("personalPayAmount",trainOrderDto.getTotalAmount().doubleValue()-trainOrderDto.getBpTotalAmount().doubleValue());
        }
        model.addAttribute("payItem", refundOrderListController.getPayItemWithPaymentNo(trainOrderDto.getPaymentPlanNo()));
        model.addAttribute("trainOrderDto", trainOrderDto);
        model.addAttribute("ticketDetail", ticketDetail);
        model.addAttribute("amount", trainOrderDto.getPassengerList().get(0).getAmount().doubleValue());
        model.addAttribute("refundBalance", BigDecimalUtils.divide(trainOrderDto.getTotalRefundBalance(), BigDecimal.valueOf(trainOrderDto.getPassengerList().size())).doubleValue());

        return "order/trainOrderDetail.base";
    }

}
