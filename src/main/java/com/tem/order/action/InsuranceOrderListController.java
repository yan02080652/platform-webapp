package com.tem.order.action;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.transform.TransformUtils;
import com.iplatform.common.utils.DateUtils;
import com.iplatform.common.utils.LogUtils;
import com.tem.order.form.AllOrderQueryForm;
import com.tem.order.form.InsuranceOrderForm;
import com.tem.order.form.InsuranceOrderShowForm;
import com.tem.order.form.InsuranceOrderSummaryForm;
import com.tem.order.service.PartnerOrderListService;
import com.tem.order.util.ExportInfo;
import com.tem.payment.enums.PayChannel;
import com.tem.platform.action.BaseController;
import com.tem.platform.api.UserService;
import com.tem.platform.api.dto.UserDto;
import com.tem.product.api.InsuranceOrderService;
import com.tem.product.api.ProviderService;
import com.tem.product.dto.ProviderDto;
import com.tem.product.dto.insurance.InsuranceOrderCondition;
import com.tem.product.dto.insurance.InsuranceOrderDetailDto;
import com.tem.product.dto.insurance.InsuranceOrderDto;
import com.tem.product.dto.insurance.InsuranceStatus;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.dto.refund.RefundOrderDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author chenjieming
 * @date 2017/8/1
 */
@Controller
@RequestMapping("order")
public class InsuranceOrderListController extends BaseController {

    private final Logger logger = LoggerFactory.getLogger(InsuranceOrderListController.class);

    @Autowired
    private InsuranceOrderService insuranceOrderService;

    @Autowired
    private ProviderService providerService;

    @Autowired
    private UserService userService;

    @Autowired
    private RefundOrderListController refundOrderListController;

    @Autowired
    private PartnerOrderListService orderListService;

    @Autowired
    private RefundOrderService refundOrderService;

    @RequestMapping("/insuranceOrder")
    public String insuranceOrder(Model model){
        InsuranceStatus[] showStatus = InsuranceStatus.values();
        model.addAttribute("showStatus",showStatus);
        List<ProviderDto> providerDtos = providerService.findAllProvider();//得到所有供应商
        model.addAttribute("providerDtos", providerDtos);

        return "order/insuranceOrder.base";
    }

    @RequestMapping("/insuranceOrderList")
    public String insuranceOrderList(Model model,Integer pageIndex, Integer pageSize,AllOrderQueryForm condition){
        if (pageIndex == null) {
            pageIndex = 1;
        }
        if (pageSize == null) {
            pageSize = Page.DEFAULT_PAGE_SIZE;
        }

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }

        Page<InsuranceOrderShowForm> pageList = this.getMergeInsuranceOrder(pageIndex, pageSize, condition);

        List<InsuranceOrderShowForm> mergeList = pageList.getList();//退款

        List<InsuranceOrderShowForm> extraRefundOrderList = this.getExtraRefundOrderList(condition, "13");

        int fPage = pageList.getTotalPage();
        int rCount = extraRefundOrderList.size(),fCount = pageList.getTotal();
        int totalCount = fCount + rCount;

        mergeList = orderListService.getPageList(mergeList, extraRefundOrderList, pageIndex, fPage, totalCount);

        Page<InsuranceOrderShowForm> pageList4 = new Page<>(pageList.getStart(),pageList.getSize(),
                mergeList, totalCount);

        model.addAttribute("pageList",pageList4);

        return "order/insuranceOrderList.jsp";
    }

    /**
     * 正向保险和退单合并
     * @param pageIndex
     * @param pageSize
     * @param condition
     * @return
     */
    private Page<InsuranceOrderShowForm> getMergeInsuranceOrder(Integer pageIndex, Integer pageSize, AllOrderQueryForm condition){

        QueryDto<InsuranceOrderCondition> queryDto = new QueryDto<>(pageIndex, pageSize);
        queryDto.setCondition(condition.toInsuranceOrderCondition());

        try{
            if (StringUtils.isNotEmpty(condition.getKeyword()) ){

                LogUtils.debug(logger, "通过关键字来查询正向订单，关键字为：{}",condition.getKeyword());
                RefundOrderDto refundOrderDto = refundOrderService.getById(condition.getKeyword());
                LogUtils.debug(logger, "", "查询结果为：{}",refundOrderDto);

                if (refundOrderDto.getOrderId() != null) {
                    queryDto.getCondition().setId(refundOrderDto.getOrderId());
                }
            }
        }catch (Exception e){
            LogUtils.error(logger, "异常信息为：{}",e.getMessage());
        }

        long begin = System.currentTimeMillis();

        Page<InsuranceOrderDto> pageList = insuranceOrderService.findOrderList(queryDto);

        long end = System.currentTimeMillis();
        LogUtils.debug(logger, "查询保险接口，接口耗时：{}",end - begin);

        List<InsuranceOrderShowForm> orderList = transInsuranceOrderSummaryFormList(pageList.getList());

        condition.setType("13");
        condition.setOrderStatus(null);

        long begin1 = System.currentTimeMillis();

        Page<RefundOrderDto> refundPageList = refundOrderListController.getMergeRefundList(1, Integer.MAX_VALUE,condition);

        
        long end1 = System.currentTimeMillis();
        LogUtils.debug(logger, "退单查询接口，接口耗时：{}",end1 - begin1);

        List<InsuranceOrderShowForm> mergeList = new ArrayList<>();

        long begin2 = System.currentTimeMillis();
        for (InsuranceOrderShowForm order : orderList){

            mergeList.add(order);

            for (RefundOrderDto refundOrder : refundPageList.getList()){
                if(order.getInsuranceId().equals(refundOrder.getOrderId())){
                    mergeList.add(orderListService.refundOrderToInsurance(refundOrder,order));
                }
            }
        }

        long end2 = System.currentTimeMillis();
        LogUtils.debug(logger, "保险订单，数据转换，接口耗时：{}",end2 - begin2);

        return new Page<>(pageList.getStart(),pageList.getSize(),mergeList,pageList.getTotal());
    }

    /**
     * 获取在这个时间段内的额外退单
     * @param condition
     * @param orderType
     * @return
     */
    private List<InsuranceOrderShowForm> getExtraRefundOrderList(AllOrderQueryForm condition, String orderType){
        List<InsuranceOrderShowForm> extraOrderInfo = new ArrayList<>();
        if(condition.getStartDate() != null && condition.getEndDate() != null){

            List<RefundOrderDto> refundOrderList = refundOrderListController.queryExtraRefundOrder(condition, orderType);
            for(RefundOrderDto refundOrderInfo : refundOrderList){
                InsuranceOrderShowForm orderInfoShow = orderListService.refundOrderToInsurance(refundOrderInfo,null);
                orderInfoShow.setTopLine(true);
                extraOrderInfo.add(orderInfoShow);
            }
        }
        return extraOrderInfo;
    }

    @RequestMapping("exportInsurance")
    public void exportInsurance(AllOrderQueryForm condition){
        long begin = System.currentTimeMillis();

        //判断是否有查看所有tmc的权限，
        if (!isSuperAdmin()){
            condition.setTmcId(super.getTmcId().toString());
        }

        List<ExportInfo> infoList = new ArrayList<>();
        Page<InsuranceOrderShowForm> pageList = this.getMergeInsuranceOrder(1,Integer.MAX_VALUE,condition);

        long end = System.currentTimeMillis();
        LogUtils.debug(logger, "获取合并后的保险订单和退单,耗时：{}",end - begin);

        long begin1 = System.currentTimeMillis();

        List<InsuranceOrderShowForm> orderList = this.getExtraRefundOrderList(condition, "13");
        pageList.getList().addAll(orderList);

        long end1 = System.currentTimeMillis();
        LogUtils.debug(logger, "获取额外的退单,耗时：{}",end1 - begin1);

        List<InsuranceOrderSummaryForm> dateSet = TransformUtils.transformList(pageList.getList(),InsuranceOrderSummaryForm.class);

        String[] insuranceHeader = {"序号", "保险单号","关联订单号","支付方式","下单人","下单日期","被保人员","人数"
                ,"保单生效日期","保单失效日期","航班号","所属客户","客户结算价","所属供应商","供应商结算价","订单状态","客户账单ID","供应商保单号","所属成本中心"};

        ExportInfo<InsuranceOrderSummaryForm> insuranceExport = new ExportInfo<InsuranceOrderSummaryForm>();
        insuranceExport.setDataset(dateSet);
        insuranceExport.setHeaders(insuranceHeader);
        insuranceExport.setTitle("保险订单");
        infoList.add(insuranceExport);

        long export = System.currentTimeMillis();
        LogUtils.debug(logger, "从请求方法到开始导出订单, 耗时时间：{}",export - begin);

        String uuid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        String fileName = "保险订单" + uuid +  ".xlsx";

        refundOrderListController.goToExport(infoList, fileName, getRequest(), getResponse());
    }

    private List<InsuranceOrderShowForm> transInsuranceOrderSummaryFormList(List<InsuranceOrderDto> insuranceOrderList){
        List<InsuranceOrderShowForm> insuranceDateSet = new ArrayList();

        List<ProviderDto> providerDtos = providerService.findAllProvider();//得到所有供应商
        int i = 0;
        for(InsuranceOrderDto insuranceOrder : insuranceOrderList){
            InsuranceOrderShowForm insuranceOrderSummaryForm = new InsuranceOrderShowForm();
            i++;
            insuranceOrderSummaryForm.setId(i);
            insuranceOrderSummaryForm.setInsuranceId(insuranceOrder.getId().toString());
            insuranceOrderSummaryForm.setOrderId(insuranceOrder.getOrderId().toString());
            if(StringUtils.isNotEmpty(insuranceOrder.getPaymentType())){
            	String[] paymentMethods = insuranceOrder.getPaymentType().split(",");
            	String methods="";
            	for(String paymentMethod: paymentMethods){
            		methods +=PayChannel.valueOf(paymentMethod).getValue() +"/";
            	}
            	insuranceOrderSummaryForm.setPaymentMethod(methods.substring(0,methods.length()-1));
            }
            if(insuranceOrder.getCreater() != null){
                UserDto user = userService.getUser(insuranceOrder.getCreater());
                if(user != null){
                    insuranceOrderSummaryForm.setUserName(user.getFullname());
                }
            }
            if(StringUtils.isNotEmpty(insuranceOrder.getCreateDate())){
                insuranceOrderSummaryForm.setCreateDate(DateUtils.parse(insuranceOrder.getCreateDate(),"yyyy-MM-dd HH:mm:ss"));
            }

            String passengers = "";
            String policyNos = "";
            Double totalSettlePrice = 0d;
            insuranceOrderSummaryForm.setPassengerCount(insuranceOrder.getInsuranceOrderDetails().size());
            if(CollectionUtils.isNotEmpty(insuranceOrder.getInsuranceOrderDetails())){
                for(InsuranceOrderDetailDto insuranceOrderDetailDto : insuranceOrder.getInsuranceOrderDetails()){
                    if(StringUtils.isNotEmpty(insuranceOrderDetailDto.getInsuredName())){
                        passengers = passengers + insuranceOrderDetailDto.getInsuredName() + "、";
                    }
                    if(StringUtils.isNotEmpty(insuranceOrderDetailDto.getPolicyNo())){
                        policyNos = policyNos + insuranceOrderDetailDto.getPolicyNo() + "、";
                    }

                    totalSettlePrice += insuranceOrderDetailDto.getSettlePrice() == null ? 0d : insuranceOrderDetailDto.getSettlePrice().doubleValue();
                }

                if(StringUtils.isNotEmpty(passengers)){
                    passengers = passengers.substring(0,passengers.length()-1);
                }
                if(StringUtils.isNotEmpty(policyNos)){
                    policyNos = policyNos.substring(0,policyNos.length()-1);
                }

            }
            insuranceOrderSummaryForm.setPassengers(passengers);
            insuranceOrderSummaryForm.setPolicyNo(policyNos);
            insuranceOrderSummaryForm.setSupSettleAmount(totalSettlePrice);
            insuranceOrderSummaryForm.setEffectiveDate(insuranceOrder.getEffectiveDate());
            insuranceOrderSummaryForm.setExpireDate(insuranceOrder.getExpireDate());
            insuranceOrderSummaryForm.setFlightNo(insuranceOrder.getFlightNo());
            insuranceOrderSummaryForm.setPartnerName(insuranceOrder.getPartnerName());
            insuranceOrderSummaryForm.setTotalAmount(insuranceOrder.getTotalPremium() == null ? 0d : insuranceOrder.getTotalPremium().doubleValue());
            if(StringUtils.isNotEmpty(insuranceOrder.getProviderCode())){
                for(ProviderDto providerDto : providerDtos){
                    if(providerDto.getCode().equals(insuranceOrder.getProviderCode())){
                        insuranceOrderSummaryForm.setSupName(providerDto.getName());
                        break;
                    }
                }
            }
            insuranceOrderSummaryForm.setOrderState(insuranceOrder.getStatus().getMessage());

            insuranceOrderSummaryForm.setTopLine(true);
            insuranceOrderSummaryForm.setSummary(insuranceOrder.getSummary());
            insuranceOrderSummaryForm.setCostCenterName(insuranceOrder.getCostCenterName());
            insuranceOrderSummaryForm.setBillId(insuranceOrder.getBillId());
            
            insuranceDateSet.add(insuranceOrderSummaryForm);
        }
        return insuranceDateSet;
    }

    /**
     * 订单详情页面
     * @param model
     * @param orderId
     * @return
     */
    @RequestMapping("/insuranceDetail/{orderId}")
    public String insuranceDetail(Model model,@PathVariable String orderId){
        InsuranceOrderDto insuranceOrderDto = insuranceOrderService.findByInsuranceMainId(orderId);
        model.addAttribute("insuranceOrderDto", TransformUtils.transform(insuranceOrderDto, InsuranceOrderForm.class));
        if(StringUtils.isNotEmpty(insuranceOrderDto.getPaymentType())){
            String[] paymentMethods = insuranceOrderDto.getPaymentType().split(",");
            String methods = "";
            for(String paymentMethod: paymentMethods){
                methods += PayChannel.valueOf(paymentMethod).getValue() +"/";
            }
            model.addAttribute("payMethed", methods.substring(0,methods.length() - 1));
        }
        return "order/insuranceOrderDetail.base";
    }

}
