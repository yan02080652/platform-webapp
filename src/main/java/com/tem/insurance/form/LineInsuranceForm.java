package com.tem.insurance.form;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author hufei
 *  线下处理 投保 撤保表单对象
 */
public class LineInsuranceForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//保险订单id
	private String orderId;
	//任务id
	private Integer taskId;
	//用户id
	private String userId;
	//处理结果 1成功投保  2 拒单退款 3成功撤保 4 拒绝撤保
	private String result;
	//保单号
	private String policyNo;
	//任务类型  0 代表线下投保      1代表线下撤保
	private Integer taskType;
	//投保渠道
	private String channel;
	//应付总价
	private BigDecimal totalPremium;
	//供应商订单号
	private String supplierOrderNo;
	//拒绝保险原因
	private String reson;
	
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public Integer getTaskType() {
		return taskType;
	}
	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public BigDecimal getTotalPremium() {
		return totalPremium;
	}
	public void setTotalPremium(BigDecimal totalPremium) {
		this.totalPremium = totalPremium;
	}
	public String getSupplierOrderNo() {
		return supplierOrderNo;
	}
	public void setSupplierOrderNo(String supplierOrderNo) {
		this.supplierOrderNo = supplierOrderNo;
	}
	public String getReson() {
		return reson;
	}
	public void setReson(String reson) {
		this.reson = reson;
	}
	
}
