package com.tem.insurance.action;

import com.iplatform.common.OrderBizType;
import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.utils.BigDecimalUtils;
import com.iplatform.common.utils.DateUtils;
import com.tem.flight.api.order.FlightOrderService;
import com.tem.flight.dto.order.FlightOrderDto;
import com.tem.insurance.common.Constants;
import com.tem.insurance.form.LineInsuranceForm;
import com.tem.payment.api.BpAccTradeRecordService;
import com.tem.payment.dto.TradeRecordInfo;
import com.tem.platform.action.BaseController;
import com.tem.platform.security.authorize.User;
import com.tem.product.api.InsuranceOrderService;
import com.tem.product.api.ProductService;
import com.tem.product.api.ProviderService;
import com.tem.product.dto.insurance.*;
import com.tem.pss.api.IssueChannelService;
import com.tem.pss.api.RefundOrderService;
import com.tem.pss.api.task.OrderTaskService;
import com.tem.pss.dto.IssueChannelDto;
import com.tem.pss.dto.task.OrderTaskCondition;
import com.tem.pss.dto.task.OrderTaskDto;
import com.tem.pss.dto.task.TaskParamDto;
import com.tem.pss.dto.task.TaskStatus;
import com.tem.pss.enums.IssueChannelType;
import com.tem.pss.enums.LineType;
import com.tem.tmc.TaskCommonService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.*;

@Controller
@RequestMapping(value="/insurance/tmc")
public class InsuranceTmcController extends BaseController {
	
	private static final Logger logger = LoggerFactory.getLogger(InsuranceTmcController.class);
	
	@Autowired
	private InsuranceOrderService insuranceOrderService;
	
	@Autowired
	private OrderTaskService orderTaskService;
	
	@Autowired
	private RefundOrderService refundOrderService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private FlightOrderService flightOrderService;
	
	@Autowired
	private ProviderService providerService;
	
	@Autowired
	private BpAccTradeRecordService bpAccTradeRecordService;
	
	@Autowired
	private TaskCommonService taskCommonService;

	@Autowired
	private IssueChannelService issueChannelService;
	
	@RequestMapping(value="/index")
	public String index(Model model){
		
		return "insurance/tmc/index.jsp";
	}
	
	@RequestMapping(value="/getMyTaskTable")
	public String getMyTask(Model model,Integer pageIndex,String taskStatus,String taskType){
		
		if(pageIndex==null){
			pageIndex=1;
		}
		
		if(taskStatus==null){
			taskStatus = TaskStatus.PROCESSING.toString();
		}
		
		QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex,10);
		OrderTaskCondition condition = new OrderTaskCondition();
		condition.setTmcId(String.valueOf(super.getTmcId()));
		condition.setTaskStatus(taskStatus);
		condition.setTaskType(taskType);
		condition.setOperatorId(super.getCurrentUser().getId().intValue());
		condition.setOrderBizType(OrderBizType.INSURANCE_MAIN.name());
		queryDto.setCondition(condition);
		
		Page<OrderTaskDto> pageList = insuranceOrderService.orderTaskPageListQuery(queryDto);
		
		this.getStatusAndTypeCount(model,super.getCurrentUser().getId().intValue());
		
		taskCommonService.getTaskGroupCount(model,super.getTmcId().toString(),super.getCurrentUser().getId().intValue());
		
		model.addAttribute("pageList", pageList);
		model.addAttribute("taskStatus", taskStatus);
		
		return "insurance/tmc/myTask.jsp";
	}
	
	//全部任务
	@RequestMapping("/allTask")
	public String allTask(Model model ,Integer pageIndex,String taskStatus,String taskType,String keyword){
		if(pageIndex==null){
			pageIndex=1;
		}
		if(taskStatus==null){
			taskStatus = TaskStatus.WAIT_PROCESS.toString();
		}
		
		QueryDto<OrderTaskCondition> queryDto = new QueryDto<OrderTaskCondition>(pageIndex,10);
		OrderTaskCondition condition = new OrderTaskCondition();
		
		condition.setTmcId(String.valueOf(super.getTmcId()));
		condition.setTaskStatus(taskStatus);
		condition.setTaskType(taskType);
		condition.setOperatorId(null);
		condition.setOrderBizType(OrderBizType.INSURANCE_MAIN.name());
		if(StringUtils.isNotBlank(keyword)){
			condition.setKeyword(keyword);
		}
		queryDto.setCondition(condition);
		
		Page<OrderTaskDto> pageList = insuranceOrderService.orderTaskPageListQuery(queryDto);
		
		this.getStatusAndTypeCount(model,null);
		
		model.addAttribute("pageList", pageList);
		model.addAttribute("taskStatus", taskStatus);
		
		return "insurance/tmc/allTask.jsp";
	}
	
	//订单查询页面
	@RequestMapping("/orderList")
	public String orderList(Model model){
		InsuranceOrderCondition condition = new InsuranceOrderCondition();
		condition.setTmcId(String.valueOf(super.getTmcId()));
		QueryDto<InsuranceOrderCondition> queryDto = new QueryDto<InsuranceOrderCondition>(1,10);
		queryDto.setCondition(condition);
		Page<InsuranceOrderDto> pageList = insuranceOrderService.findOrderList(queryDto);
		List<InsuranceOrderCountDto> OrderStatuslist = getOrderCountList(queryDto);
		
		model.addAttribute("pageList", pageList);
		Map<String,List<String>> nameMap = insuranceOrderService.findAllParentNames();
		
		model.addAttribute("nameMap", nameMap);
		model.addAttribute("OrderStatuslist", OrderStatuslist);
		return "insurance/tmc/orderList.jsp";
	}
	
	//查询各个状态的订单数量
	private List<InsuranceOrderCountDto> getOrderCountList(QueryDto<InsuranceOrderCondition> queryDto) {
		List<InsuranceOrderCountDto> orderCountList = insuranceOrderService.findOrderCountList(queryDto);
		
		List<InsuranceOrderCountDto> list = new ArrayList<InsuranceOrderCountDto>();
		InsuranceStatus[] values = InsuranceStatus.values();
		
		for(InsuranceStatus insuranceStatus : values){
			InsuranceOrderCountDto insuranceOrderCountDto = new InsuranceOrderCountDto(insuranceStatus,0);
			for(InsuranceOrderCountDto countDto : orderCountList){
				if(insuranceStatus.equals(countDto.getOrderShowStatus())){
					insuranceOrderCountDto.setCount(countDto.getCount());
					break;
				}
			}
			
			list.add(insuranceOrderCountDto);
		}
		return list;
	}
	
	@RequestMapping("/getOrderTable")
	public String getOrderTable(Model model,Integer pageIndex,Integer pageSize,InsuranceOrderCondition condition){
		
		if(pageIndex==null){
			pageIndex = 1;
		}
		if(pageSize==null){
			pageSize = 10;
		}
		
		Map<String,List<String>> nameMap = insuranceOrderService.findAllParentNames();
		
		QueryDto<InsuranceOrderCondition> queryDto = new QueryDto<InsuranceOrderCondition>(pageIndex,pageSize);
		condition.setTmcId(String.valueOf(super.getTmcId()));
		queryDto.setCondition(condition);
		Page<InsuranceOrderDto> pageList = insuranceOrderService.findOrderList(queryDto);
		
		condition.setOrderStatus(null);
		queryDto.setCondition(condition);
		List<InsuranceOrderCountDto> OrderStatuslist = getOrderCountList(queryDto);
		
		model.addAttribute("pageList", pageList);
		model.addAttribute("OrderStatuslist", OrderStatuslist);
		model.addAttribute("nameMap", nameMap);
		
		return "insurance/tmc/orderTable.jsp";
	}
	
	//待客下单
	@RequestMapping("/searchUserPage")
	public String searchUser(){
		return "insurance/tmc/searchUserPage.jsp";
	}
	
	//获取退保失败的保险数据
	@RequestMapping("/getRefundInsuranceFailData")
	public String getRefundInsuranceFailData(Model model,String orderId,Integer taskId,String flag,String userIds){
		InsuranceOrderDto insuranceOrderDto = insuranceOrderService.findByInsuranceMainId(orderId);
		List<InsuranceOrderDetailDto> orderDetails = insuranceOrderDto.getInsuranceOrderDetails();
		String[] failUserIds = userIds.split(",");
		for (int i = 0;i < orderDetails.size();i++) {
			boolean hasFail = true;
			for (String userId : failUserIds) {
				if(userId.equals(String.valueOf(orderDetails.get(i).getInsuredId()))){
					hasFail = false;
				}
			}
			if(hasFail) {
				orderDetails.remove(i);
				i--;
			}
		}
		Map<String,List<String>> nameMap = insuranceOrderService.findAllParentNames();
		List<InsuranceInfo> insuranceInfoList = productService.findInsuranceInfoList(getPartnerId(), Constants.FLIHGT_DOMESTIC);
		Map<String,String> descripeMap = new HashMap<String,String>();
		for (InsuranceInfo insuranceInfo : insuranceInfoList) {
			descripeMap.put(String.valueOf(insuranceInfo.getProductId()), insuranceInfo.getDescription());
		}
		FlightOrderDto flightOrder = flightOrderService.getFlightOrderById(String.valueOf(insuranceOrderDto.getOrderId()));
		
		model.addAttribute("flightOrder", flightOrder);
		model.addAttribute("descripeMap", descripeMap);
		model.addAttribute("insuranceOrder", insuranceOrderDto);
		model.addAttribute("nameMap", nameMap);
		model.addAttribute("orderId", orderId);
		model.addAttribute("taskId", taskId);
		model.addAttribute("userIds", userIds);
		
		return "insurance/tmc/refundInsuranceFail.single";
	}
	
	//获取投保失败的保险数据
	@RequestMapping("/getInsuranceFailData")
	public String getInsuranceFailData(Model model,String orderId,Integer taskId,String flag){
		InsuranceOrderDto insuranceOrderDto = insuranceOrderService.findByInsuranceMainId(orderId);
		List<InsuranceOrderDetailDto> orderDetails = insuranceOrderDto.getInsuranceOrderDetails();
		for(int i = 0;i<orderDetails.size();i++) {
			if(!ProviderPolicyStatus.ISSUE_FAIL.equals(orderDetails.get(i).getProviderPolicyStatus())) {
				orderDetails.remove(i);
				i--;
			}
		}
		List<InsuranceInfo> insuranceInfoList = productService.findInsuranceInfoList(getPartnerId(), Constants.FLIHGT_DOMESTIC);
		Map<String,String> descripeMap = new HashMap<String,String>();
		for (InsuranceInfo insuranceInfo : insuranceInfoList) {
			descripeMap.put(String.valueOf(insuranceInfo.getProductId()), insuranceInfo.getDescription());
		}
		FlightOrderDto flightOrder = flightOrderService.getFlightOrderById(String.valueOf(insuranceOrderDto.getOrderId()));
		Map<String,List<String>> nameMap = insuranceOrderService.findAllParentNames();
		
		List<IssueChannelDto> list = issueChannelService.findListByTmcIdAndChannelTypeAndLineType(super.getTmcId(), IssueChannelType.INSURANCE.getType(), LineType.OFF_LINE.getType());

		model.addAttribute("flightOrder", flightOrder);
		model.addAttribute("descripeMap", descripeMap);
		model.addAttribute("insuranceOrder", insuranceOrderDto);
		model.addAttribute("nameMap", nameMap);
		model.addAttribute("channelList", list);
		model.addAttribute("orderId", orderId);
		model.addAttribute("taskId", taskId);
		
		return "insurance/tmc/insureFail.single";
	}
	
	
	/**
	 * 获取各状态下各个类型的任务数量
	 * @param model
	 */
	private void getStatusAndTypeCount(Model model,Integer userId){
		
		OrderTaskCondition condition = new OrderTaskCondition();
		condition.setOrderBizType(OrderBizType.INSURANCE_MAIN.name());
		condition.setOperatorId(userId);
		condition.setTmcId(String.valueOf(super.getTmcId()));
		condition.setTaskStatus(TaskStatus.PROCESSING.toString());
		//处理中任务数量（包含各状态的数量）
		Map<String, Integer> processingMap = orderTaskService.getTaskCountMap(condition);
		
		condition.setTaskStatus(TaskStatus.ALREADY_PROCESSED.toString());
		//已处理任务数量（包含各状态的数量）
		Map<String, Integer> alreadyMap = orderTaskService.getTaskCountMap(condition);
		
		//还未领取的任务(包含各状态的数量)
		condition.setTaskStatus(TaskStatus.WAIT_PROCESS.toString());
		condition.setOperatorId(null);
		Map<String, Integer> waitTaskTypeMap = orderTaskService.getTaskCountMap(condition);
		Map<String,List<String>> nameMap = insuranceOrderService.findAllParentNames();
		
		
		model.addAttribute("nameMap", nameMap);
		model.addAttribute("processingMap",processingMap);
		model.addAttribute("alreadyMap",alreadyMap);
		model.addAttribute("waitTaskTypeMap",waitTaskTypeMap);
	}
	
	/**
	 * 检查是否存在
	 * 1.存在超过10秒还未领取的任务
	 * 2.超时未完成的任务
	 * @return true:存在， false:不存在
	 */
	@ResponseBody
	@RequestMapping("/checkTaskStatus")
	public boolean checkTaskStatus(){
		//查询未领取的任务
		List<OrderTaskDto> list = orderTaskService.findTaskByStatusAndBizType(TaskStatus.WAIT_PROCESS,OrderBizType.INSURANCE_MAIN);
		for (OrderTaskDto orderTaskDto : list) {
			if(DateUtils.addSecond(orderTaskDto.getCreateDate(), 10).compareTo(new Date()) <= 0){
				//存在超过30秒还未领取的任务
				return true;
			}
		}
		//查询超时未完成的任务
		 list = orderTaskService.findTaskByStatusAndBizType(TaskStatus.PROCESSING,OrderBizType.INSURANCE_MAIN);
		for (OrderTaskDto orderTaskDto : list) {
			if(orderTaskDto.getMaxProcessDate().compareTo(new Date()) <= 0){
				return true;
			}
		}
		
		return false;
	}
	
	//任务交接
	@ResponseBody
	@RequestMapping("/transfer")
	public boolean transfer(Integer taskId,Integer operatorId,Integer userId,String userName){
		logger.debug("tmc任务交接，入参:"+taskId+"--"+operatorId+"--"+userId+"--"+userName);
		if(operatorId == null){
			operatorId = super.getCurrentUser().getId().intValue();
		}
		TaskParamDto dto = new TaskParamDto();
		dto.setTaskId(taskId);
		dto.setOperatorId(operatorId);
		dto.setUserId(userId);
		dto.setUserName(userName);
		dto.setRemork(super.getCurrentUser().getFullname()+"("+operatorId+")将任务交接给"+userName+"("+userId+"),任务id:"+taskId);
		dto.setOrderBizType(OrderBizType.INSURANCE_MAIN.name());
		dto.setTaskStatus(TaskStatus.PROCESSING);
		
		boolean rs = orderTaskService.saveTaskAndLog(dto);
		
		logger.debug("tmc任务交接返回结果=={}",rs);
		
		return rs;
	}
	
	/**
	 * 判断操作人是否为任务领取人
	 * @param operatorId 操作人id
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/checkOperator")
	public boolean checkOperator(String operatorId){
		Long userId = super.getCurrentUser().getId();
		if(userId.toString().equals(operatorId)){
			return true;
		}
		return false;
	}
	
	/**
	 * 修改任务超时时间
	 * @param taskId 任务id
	 * @param delay 延时时间：分钟
	 */
	@RequestMapping("/updateMaxProcessDate")
	public void updateMaxProcessDate(Integer taskId,Integer delay){

		OrderTaskDto dto = new OrderTaskDto();
		dto.setId(taskId);
		dto.setMaxProcessDate(DateUtils.addMinute(new Date(), delay));
		dto.setTaskStatus(TaskStatus.PROCESSING);
		orderTaskService.update(dto);
	}
	
	//领取任务
	@ResponseBody
	@RequestMapping("/receiveTask")
	public boolean receiveTask(@RequestParam("taskType[]")List<String>	taskType){
		User user = super.getCurrentUser();
		TaskParamDto dto = new TaskParamDto();
		dto.setUserId(user.getId().intValue());
		dto.setUserName(user.getFullname());
		dto.setTaskType(taskType);
		dto.setOrderBizType(OrderBizType.INSURANCE_MAIN.name());
		dto.setTmcId(String.valueOf(super.getTmcId()));
		boolean rs = orderTaskService.receiveTask(dto);
		
		return rs;
	}
	
	@ResponseBody
	@RequestMapping(value="aginInsure")
	public boolean againInsure(String orderId,Integer taskId,String flag){
		
		OrderTaskDto orderTaskDto = new OrderTaskDto();
		orderTaskDto.setId(taskId);
		orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		orderTaskDto.setCompleteDate(new Date());
		orderTaskDto.setOperatorId(super.getCurrentUser().getId().intValue());
		orderTaskDto.setOperatorName(super.getCurrentUser().getFullname());
		orderTaskDto.setTmcId(String.valueOf(super.getTmcId()));
		boolean boo = insuranceOrderService.aginInsure(orderId,orderTaskDto);
		
		return boo;
	}
	
	@ResponseBody
	@RequestMapping(value="aginRefundInsure")
	public boolean aginRefundInsure(String orderId,Integer taskId,String flag,String userIds,String refundOrderId){
		
		OrderTaskDto orderTaskDto = new OrderTaskDto();
		orderTaskDto.setId(taskId);
		orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		orderTaskDto.setCompleteDate(new Date());
		orderTaskDto.setOperatorId(super.getCurrentUser().getId().intValue());
		orderTaskDto.setOperatorName(super.getCurrentUser().getFullname());
		orderTaskDto.setTmcId(String.valueOf(super.getTmcId()));
		boolean boo = insuranceOrderService.aginRefundInsure(orderId,orderTaskDto,userIds,refundOrderId);
		
		return boo;
	}
	
	//获取订单日志
	@RequestMapping("/getOrderLog")
	public String getOrderLog(Model model,String orderId){
		
		List<InsuranceOrderOperationLogDto> logList = insuranceOrderService.findOrderLogList(orderId);
		model.addAttribute("logList", logList);
		
		return "insurance/tmc/log.single";
	}
	
	@ResponseBody
	@RequestMapping(value="lineRefundInsurance")
	public boolean lineRefundInsurance(@RequestBody List<LineInsuranceForm> forms){
		
		InsuranceOrderDto insuranceOrderDto = insuranceOrderService.findByInsuranceMainId(forms.get(0).getOrderId());
		List<InsuranceOrderDetailDto> orderDetails = insuranceOrderDto.getInsuranceOrderDetails();
		for(int i = 0;i< orderDetails.size();i++) {
			InsuranceOrderDetailDto detailDto = orderDetails.get(i);
			boolean hasOfflineRefund = false;
			for(LineInsuranceForm form : forms){
				if(detailDto.getInsuredId().equals(Long.parseLong(form.getUserId()))) {
					hasOfflineRefund = true;
				}
			}
			if(!hasOfflineRefund) {
				orderDetails.remove(i);
				i--;
			}else{
				for(LineInsuranceForm form : forms){
					if(detailDto.getInsuredId().equals(Long.parseLong(form.getUserId()))) {
						if("3".equals(String.valueOf(form.getResult()))) {
							detailDto.setStatus(InsuranceStatus.REFUNDED);
							detailDto.setProviderPolicyStatus(ProviderPolicyStatus.REFUNDED);
						}else{
							detailDto.setOfflineRejectReason(form.getReson());
							detailDto.setStatus(InsuranceStatus.ISSUED);
						}
					}
				}
			}
			
		}
		
		OrderTaskDto orderTaskDto = new OrderTaskDto();
		orderTaskDto.setId(forms.get(0).getTaskId());
		orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		orderTaskDto.setCompleteDate(new Date());
		orderTaskDto.setOperatorId(super.getCurrentUser().getId().intValue());
		orderTaskDto.setOperatorName(super.getCurrentUser().getFullname());		
		orderTaskDto.setTmcId(String.valueOf(super.getTmcId()));
		boolean rs = insuranceOrderService.lineRefundInsurance(insuranceOrderDto,orderTaskDto);
		
		return rs;
	}
	
	@ResponseBody
	@RequestMapping(value="lineInsurance")
	public boolean lineInsurance(@RequestBody List<LineInsuranceForm> forms){
		
		InsuranceOrderDto insuranceOrderDto = insuranceOrderService.findByInsuranceMainId(forms.get(0).getOrderId());
		List<InsuranceOrderDetailDto> orderDetails = insuranceOrderDto.getInsuranceOrderDetails();
		
		boolean successInsure = false;
		int successNum = 0;
		for(int i = 0;i < orderDetails.size();i++) {
			InsuranceOrderDetailDto detailDto = orderDetails.get(i);
			boolean hasOfflineInsure = false;
			for(LineInsuranceForm form : forms){
				if(detailDto.getInsuredId().equals(Long.parseLong(form.getUserId()))) {
					hasOfflineInsure = true;
				}
			}
			if(!hasOfflineInsure) {
				orderDetails.remove(i);
				i--;
			}else{
				for(LineInsuranceForm form : forms){
					
					if(detailDto.getInsuredId().equals(Long.parseLong(form.getUserId()))) {
						if("1".equals(String.valueOf(form.getResult()))){ //成功投保 填写保单号
							successInsure = true;
							successNum++;
							detailDto.setPolicyNo(form.getPolicyNo());
							detailDto.setStatus(InsuranceStatus.ISSUED);
							detailDto.setIssueChannelId(Integer.parseInt(form.getChannel()));
							//detailDto.setProviderPolicyStatus(ProviderPolicyStatus.ISSUED);
						}
						if("2".equals(String.valueOf(form.getResult()))){ //拒单退款说明理由
							detailDto.setStatus(InsuranceStatus.CANCELED);  //TODO 拒单退款 尚未投保 设置已取消
							//detailDto.setProviderPolicyStatus(ProviderPolicyStatus.ISSUE_FAIL);
							detailDto.setOfflineRejectReason(form.getReson());
						}
					}
				}
			}
			
		}
		if(successInsure) { //线下处理有成功投保的  更改订单状态

			insuranceOrderDto.setStatus(InsuranceStatus.ISSUED);
			BigDecimal settelPirce = BigDecimalUtils.divide(forms.get(0).getTotalPremium(), new BigDecimal(successNum));
			for (InsuranceOrderDetailDto detail : orderDetails) {
				if(InsuranceStatus.ISSUED.equals(detail.getStatus())) {
					detail.setSettlePrice(settelPirce);
				}
			}


		} else {
			insuranceOrderDto.setStatus(InsuranceStatus.CANCELED);
		}
		
		OrderTaskDto orderTaskDto = new OrderTaskDto();
		orderTaskDto.setId(forms.get(0).getTaskId());
		orderTaskDto.setTaskStatus(TaskStatus.ALREADY_PROCESSED);
		orderTaskDto.setCompleteDate(new Date());
		orderTaskDto.setOperatorId(super.getCurrentUser().getId().intValue());
		orderTaskDto.setOperatorName(super.getCurrentUser().getFullname());		
		orderTaskDto.setTmcId(String.valueOf(super.getTmcId()));
		boolean rs = insuranceOrderService.lineInsurance(insuranceOrderDto,orderTaskDto);
		
		return rs;
	}
	
	//收出明细
	@RequestMapping("/getIncomeDetail")
	public String getIncomeDetail(Model model, String orderId,Long paymentPlanNo) {
		List<TradeRecordInfo> tradeRecordList = bpAccTradeRecordService.listByOrderIdAndPlanId(orderId, paymentPlanNo);
		model.addAttribute("tradeRecordList",tradeRecordList);
		return "flight/tmc/incomeDetail.single";
	}
	
	
}
