package com.tem.insurance.common;



/**
 * @author hufei
 *	保险后台处理结果
 */
public enum HandleResult {
	
	INSURE_SUCCESS(1,"成功投保"),
	
	REJECT_INSURE(2,"拒单退款"),
	
	REFUND_INSURANCE_SUCCESS(3,"成功撤保"),
	
	REJECT_REFUND_INSURANCE(4,"拒绝撤保");
	
	private Integer code;
	
	private String message;
	
	private HandleResult(Integer code,String message){
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
