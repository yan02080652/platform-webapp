package com.tem.insurance.common;
/**
 * 机票业务常量定义
 * @author yuanjianxin
 *
 */
public class Constants {
	/**
	 * 国内机票保险常量标识
	 */
	public static final String FLIHGT_DOMESTIC = "AIRT_D";
	/**
	 * 国际机票保险常量标识
	 */
	public static final String FLIGHT_INTERNATIONAL = "AIRT_I";
	
	

}
