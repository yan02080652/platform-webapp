package com.tem.tmc;

import com.alibaba.fastjson.JSONObject;
import com.tem.pss.api.ServerPartnerService;
import com.tem.pss.api.task.OrderTaskService;
import com.tem.pss.dto.task.OrderTaskDto;
import com.tem.pss.dto.task.TaskParamDto;
import com.tem.pss.dto.task.TaskStatus;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yuanjianxin
 * @date 2017/10/24 14:37
 */
@Component
public class TaskCommonService {
    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private ServerPartnerService serverPartnerService;

    /**
     *   查询出当前处理人处理中的任务列表分组数量
     * @param model
     */
    public void getTaskGroupCount(Model model,String tmcId,int userId) {
        TaskParamDto paramDto = new TaskParamDto();
        paramDto.setTmcId(tmcId);
        paramDto.setOperatorId(userId);
        paramDto.setTaskStatus(TaskStatus.PROCESSING);

        List<OrderTaskDto> processingTask = orderTaskService.findTaskList(paramDto);
        //根据业务类型进行数量统计分组
        Map<String, Integer> processingMap = new HashMap<>(16);
        for (OrderTaskDto orderTaskDto : processingTask) {
            String bizType = orderTaskDto.getOrderBizType().name();
            if (processingMap.containsKey(bizType)){
                Integer count = processingMap.get(bizType);
                processingMap.put(bizType,++count);
            }else{
                processingMap.put(bizType,1);
            }
        }

        model.addAttribute("bizProcessingMap",processingMap);

        //查询出未被领取的任务列表
        paramDto.setOperatorId(null);
        paramDto.setTaskStatus(TaskStatus.WAIT_PROCESS);
        List<Long> partnerList = serverPartnerService.findCsServerPartner(Long.valueOf(userId));
        //如果没有分配服务企业，则直接返回
        if (CollectionUtils.isEmpty(partnerList)){
            return;
        }
        paramDto.setPartnerList(partnerList);
        List<OrderTaskDto> waitProcessTask = orderTaskService.findTaskList(paramDto);
        //根据业务类型分组，然后在根据各个任务类型分组
        Map<String, Map<String, Integer>> waitProcessMap = new HashMap<>(16);
        for (OrderTaskDto orderTaskDto : waitProcessTask) {
            String bizType = orderTaskDto.getOrderBizType().name();
            String taskType = orderTaskDto.getTaskType().name();
            if (waitProcessMap.containsKey(bizType)) {
                Map<String, Integer> typeMap = waitProcessMap.get(bizType);
                if (typeMap.containsKey(taskType)) {
                    Integer count = typeMap.get(taskType);
                    typeMap.put(taskType, ++count);
                }else{
                    typeMap.put(taskType, 1);
                }
            }else{
                Map<String, Integer> typeMap = new HashMap<>(16);
                typeMap.put(taskType, 1);
                waitProcessMap.put(bizType, typeMap);
            }
        }

        //各个业务任务总数
        Map<String, Integer> countMap = new HashMap<>(16);
        for (Map.Entry<String, Map<String, Integer>> entry : waitProcessMap.entrySet()) {
            Integer count = 0;
            for (Map.Entry<String, Integer> countEntry : entry.getValue().entrySet()) {
                count  += countEntry.getValue();
            }
            countMap.put(entry.getKey(),count);
        }

        waitProcessMap.put("COUNT",countMap);

        model.addAttribute("bizWaitProcessMap",waitProcessMap);
    }

}
