package com.tem.tmc;

import com.iplatform.common.OrderBizType;
import com.iplatform.common.cache.utils.RedisCacheUtils;
import com.iplatform.common.utils.DateUtils;
import com.tem.platform.action.BaseController;
import com.tem.platform.security.authorize.PermissionUtil;
import com.tem.platform.security.authorize.User;
import com.tem.pss.api.ServerPartnerService;
import com.tem.pss.api.task.OrderTaskService;
import com.tem.pss.dto.serverPartner.ServerSkillDto;
import com.tem.pss.dto.task.OrderTaskDto;
import com.tem.pss.dto.task.TaskParamDto;
import com.tem.pss.dto.task.TaskStatus;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yuanjianxin
 * @date 2017/10/23 14:06
 */
@Controller
@RequestMapping("/tmc")
public class BaseTmcController extends BaseController {

    private static final int MAX_TASK_COUNT = 5;
    private static final String TASK_CACHE_KEY = "TASK:%s";

    @Autowired
    private OrderTaskService orderTaskService;
    @Autowired
    private TaskCommonService taskCommonService;
    @Autowired
    private ServerPartnerService serverPartnerService;

    @RequestMapping("/index")
    public String index(Model model) {
        taskCommonService.getTaskGroupCount(model, super.getTmcId().toString(), super.getCurrentUser().getId().intValue());
        ServerSkillDto skill = serverPartnerService.findCsSkill(super.getCurrentUser().getId());
        model.addAttribute("skill", skill);
        return "tmc/task.base";
    }

    //领取任务
    @ResponseBody
    @RequestMapping("/receiveTask")
    public boolean receiveTask(@RequestParam("taskType[]") List<String> taskType) {
        if (CollectionUtils.isEmpty(taskType)) {
            return false;
        }

        List<Long> partnerList = serverPartnerService.findCsServerPartner(super.getCurrentUser().getId());
        if (CollectionUtils.isEmpty(partnerList)) {
            return false;
        }

        User user = super.getCurrentUser();
        TaskParamDto dto = new TaskParamDto();
        dto.setUserId(user.getId().intValue());
        dto.setUserName(user.getFullname());
        dto.setTmcId(super.getTmcId().toString());
        dto.setPartnerList(partnerList);

        Map<String, List<String>> map = new HashMap<>(16);
        for (String s : taskType) {
            String[] split = s.split("\\.");
            if (map.containsKey(split[0])) {
                map.get(split[0]).add(split[1]);
            } else {
                List<String> list = new ArrayList<>();
                list.add(split[1]);
                map.put(split[0], list);
            }
        }

        dto.setObj(map);

        boolean rs = orderTaskService.receiveTask(dto);

        return rs;
    }

    /**
     * 检查当前处理人是否有超过5个处理中的任务
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/autoReceive")
    public boolean autoReceive() {
        TaskParamDto paramDto = new TaskParamDto();
        paramDto.setTmcId(super.getTmcId().toString());
        paramDto.setOperatorId(super.getCurrentUser().getId().intValue());
        paramDto.setTaskStatus(TaskStatus.PROCESSING);
        List<OrderTaskDto> processingTask = orderTaskService.findTaskList(paramDto);
        if (processingTask.size() >= MAX_TASK_COUNT) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 检查当前所有任务中待处理的任务中是否有超时的任务
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkAllTaskInWaitProcess")
    public boolean checkAllTaskInWaitProcess() {
        TaskParamDto paramDto = new TaskParamDto();
        paramDto.setTmcId(super.getTmcId().toString());
        paramDto.setTaskStatus(TaskStatus.WAIT_PROCESS);
        List<Long> partnerList = serverPartnerService.findCsServerPartner(super.getCurrentUser().getId());
        paramDto.setPartnerList(partnerList);
        List<OrderTaskDto> processingTask = orderTaskService.findTaskList(paramDto);

        String[] types = new String[]{"FLIGHT", "HOTEL", "TRAIN", "INSURANCE", "GENERAL"};

        List<String> list = new ArrayList<>();

        for (String type : types) {
            if (PermissionUtil.checkTrans("COMMON_TMC_SERVICE", type + "_M") == 0) {
                list.add(type);
            }
        }

        String type = StringUtils.join(list, "/");
        for (OrderTaskDto task : processingTask) {
            if (type.contains(task.getOrderBizType().name()) && DateUtils.addSecond(task.getCreateDate(), 10).compareTo(new Date()) <= 0) {
                return true;
            }
        }
        return false;

    }

    /**
     * 检查该客服人员当前处理任务中是否有超时的任务
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/checkMyTaskInProcess")
    public boolean checkMyTaskInProcess() {
        TaskParamDto paramDto = new TaskParamDto();
        paramDto.setTmcId(super.getTmcId().toString());
        paramDto.setOperatorId(super.getCurrentUser().getId().intValue());
        paramDto.setTaskStatus(TaskStatus.PROCESSING);
        List<OrderTaskDto> processingTask = orderTaskService.findTaskList(paramDto);

        for (OrderTaskDto task : processingTask) {
            if (task.getMaxProcessDate() != null && task.getMaxProcessDate().compareTo(new Date()) < 0) {
                return true;
            }
        }
        return false;

    }

    /**
     * 判断当前处理人所负责的企业是否有新任务
     *
     * @return
     */
    @RequestMapping(value = "/hasNewTask", produces = {"text/event-stream;charset=utf-8"})
    @ResponseBody
    public String hasNewTask() {
        //服务的企业
        List<Long> partnerList = serverPartnerService.findCsServerPartner(super.getCurrentUser().getId());

        //服务的业务
        ServerSkillDto skill = serverPartnerService.findCsSkill(super.getCurrentUser().getId());
        List<OrderBizType> bizList = getHasBizTypeList(skill);

        //新增任务列表缓存
        List<OrderTaskDto> tasks = RedisCacheUtils.getHvals(String.format(TASK_CACHE_KEY, super.getTmcId()));

        List<Integer> readTaskList = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(partnerList) && CollectionUtils.isNotEmpty(tasks) && CollectionUtils.isNotEmpty(bizList)) {
            for (OrderTaskDto task : tasks) {
                if (partnerList.contains(Long.valueOf(task.getcId())) && bizList.contains(task.getOrderBizType())) {
                    readTaskList.add(task.getId());
                }
            }
        }

        //返回第一个数据，代表最后一个任务的id
        String result ="";
        if (CollectionUtils.isNotEmpty(readTaskList)) {
            //根据降序id排序
            Collections.sort(readTaskList, new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    return o2 - o1;
                }
            });
            result += readTaskList.get(0);
        }else{
            result = "-1";
        }

        return "data:" + result + "\n\n";

    }

    /**
     * 根据skill返回业务类型集合
     *
     * @param skill
     * @return
     */
    public List<OrderBizType> getHasBizTypeList(ServerSkillDto skill) {
        List<OrderBizType> list = new ArrayList<>();
        if (skill.getDoFlight().equals(1)) {
            list.add(OrderBizType.FLIGHT);
        }
        if (skill.getDoHotel().equals(1)) {
            list.add(OrderBizType.HOTEL);
        }
        if (skill.getDoTrain().equals(1)) {
            list.add(OrderBizType.TRAIN);
        }
        if (skill.getDoInsurance().equals(1)) {
            list.add(OrderBizType.INSURANCE_MAIN);
        }
        if (skill.getDoGeneral().equals(1)) {
            list.add(OrderBizType.GENERAL);
        }
        return list;
    }


    /**
     *  TODO 下面是测试任务产生的测试代码，以后删除
     */
    @RequestMapping("/setCache")
    @ResponseBody
    public String setCache(int id, Long cid, String tmcId, String bizType) {
        try {
            OrderTaskDto dto = new OrderTaskDto();
            dto.setId(id);
            dto.setcId(cid);
            dto.setTmcId(tmcId);
            dto.setOrderBizType(OrderBizType.valueOf(bizType));
            RedisCacheUtils.put(String.format(TASK_CACHE_KEY, tmcId), dto.getId().toString(), dto,60);
            return "ok";
        } catch (IllegalArgumentException e) {
            return "no";
        }
    }

    @RequestMapping("/toCache")
    public String toCache() {
        return "tmc/cache.base";
    }


}
