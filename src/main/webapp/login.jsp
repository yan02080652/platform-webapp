<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	request.setAttribute("CTX_PATH", request.getContextPath());
%>
<html>
<head>
<title>系统登录</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
	var __ctxPath="<%=request.getContextPath()%>";
</script>

<link rel="stylesheet" href="resource/css/bootstrap/bootstrap.min.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/font-awesome/css/font-awesome.min.css?version=${globalVersion}">
<style type="text/css">
	body {
		background: url("resource/image/login/bg.png") repeat; 
	}
	table {
		width: 100%;
		height: 100%;
	}
	.login-container {
		width: 400px;
		height: 360px;
		padding: 60px 80px;
		border: 1px solid #D8D8D8;
		border-radius: 7px 7px 7px 7px;
		box-shadow: 0 1px 4px #D3D3D3;
		background-color: #f8f8f8;
	}
	.form-group {
		text-align: left;
	}
	.login-btn {
		width: 120px;
	}
	.form-actions {
		height: 60px;
		padding-top: 20px;
	}
	label {
		font-size: 14px;
	}
</style>
</head>
<body>
	<table border="0">
		<tr>
			<td align="center" valign="middle">
				<div class="login-container">
					<form id="loginForm" class="form-horizontal" name="loginForm" action="${CTX_PATH}/security_login" method="POST">
						  <div class="form-group">
							<label for="input-username">Email或手机</label>
							<input type="text" name="username" class="form-control fa bell" id="input-username" >
						  </div>
						  <div class="form-group"> 
							<label for="input-password">密码</label>
							<input type="password" name="password" class="form-control" id="input-password" >
						  </div>
						  <div class="form-actions">
							<button class="btn btn-info login-btn" onclick="login();">登录</button>
						  </div>
					</form>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>
<script type="text/javascript">
	function login() {
		document.getElementById("loginForm").submit();
	}
</script>