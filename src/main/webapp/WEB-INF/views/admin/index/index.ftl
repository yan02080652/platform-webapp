<@override name="body">
<body class="fixed-sidebar full-height-layout gray-bg">
    <div id="wrapper">
        <!--左侧导航开始-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="nav-close"><i class="fa fa-times-circle"></i>
            </div>
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <span><img alt="image" class="img-circle" style="height:64px;width:64px;" src="${contextPath}\resource\common\img\p3.jpg" /></span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear">
                               <span class="block m-t-xs"><strong class="font-bold">欢迎您！</strong></span>
                               <span class="text-muted text-xs block">管理员<b class="caret"></b></span>
                                </span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="J_menuItem" href="${contextPath}/sys/user/imageEdit">修改头像</a>
                                </li>
                                <li><a class="J_menuItem" href="${contextPath }/sys/user/info">个人资料</a>
                                </li>
                                <li><a class="J_menuItem" href="${contextPath }/iim/contact/index">我的通讯录</a>
                                </li>
                                <li><a class="J_menuItem" href="${contextPath }/iim/mailBox/list">信箱</a>
                                </li> 
                                <li class="divider"></li>
                                <li><a href="${contextPath}/logout">安全退出</a>
                                </li>
                            </ul>
                        </div>
                        <div class="logo-element">OTA
                        </div>
                    </li>
     
     
                  <!-- 动态生成 -->
                  <li>
                        <a href="#">
                            <i class="fa fa-home"></i>
                            <span class="nav-label">主页</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a class="J_menuItem" href="${contextPath}/sysconfig/list" data-index="0">主页示例一</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="${contextPath}/sys/dic/category.html">主页示例二</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="index_v3.html">主页示例三</a>
                            </li>
                            <li>
                                <a class="J_menuItem" href="index_v4.html">主页示例四</a>
                            </li>
                            <li>
                                <a href="index_v5.html" target="_blank">主页示例五</a>
                            </li>
                        </ul>

                    </li>
            
                 
             

                </ul>
            </div>
        </nav>
        <!--左侧导航结束-->
        <!--右侧部分开始-->
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header"><a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="${contextPath}/demos/admin#"><i class="fa fa-bars"></i> </a>
                        <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                            <div class="form-group">
                                <input type="text" placeholder="请输入您需要查找的内容 …" class="form-control" name="top-search" id="top-search">
                            </div>
                        </form>
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-envelope"></i> <span class="label label-warning">123</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages">
                            	
	                                <li class="m-t-xs">
	                                    <div class="dropdown-messages-box">
	                                   
	                                        <a  href="#" onclick='top.openTab("${contextPath}/iim/contact/index?name=123","通讯录", false)' class="pull-left">
	                                            <img alt="image" class="img-circle" src="${contextPath}\resource\common\img\p2.jpg">
	                                        </a>
	                                        <div class="media-body">
	                                            <small class="pull-right">123前</small>
	                                            <strong>123</strong>
	                                            <a class="J_menuItem" href="${contextPath}/iim/mailBox/detail?id=123">123</a>
	                                            <br>
	                                            <a class="J_menuItem" href="${contextPath}/iim/mailBox/detail?id=123">
	                                             123
	                                            </a>
	                                            <br>
	                                            <small class="text-muted">
	                                          
	                                        </div>
	                                    </div>
	                                </li>
	                                <li class="divider"></li>
                                
                                <li>
                                    <div class="text-center link-block">
                                        <a class="J_menuItem" href="${contextPath}/iim/mailBox/list?orderBy=sendtime desc">
                                            <i class="fa fa-envelope"></i> <strong> 查看所有邮件</strong>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <i class="fa fa-bell"></i> <span class="label label-primary">123</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <li>
                                        <div>
                                        	   <a class="J_menuItem" href="${contextPath}/oa/oaNotify/view?id=123&">
                                            	<i class="fa fa-envelope fa-fw"></i> 123
                                               </a>
                                            <span class="pull-right text-muted small">123前</span>
                                        </div> 
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="text-center link-block">
                                       您有123条未读消息 <a class="J_menuItem" href="${contextPath }/oa/oaNotify/self ">
                                            <strong>查看所有 </strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                      
                      <!-- 国际化功能预留接口 -->
                        <li class="dropdown">
							<a id="lang-switch" class="lang-selector dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="true">
								<span class="lang-selected">
										<img  class="lang-flag" src="${contextPath}/resource/common/img/china.png" alt="中国">
										<span class="lang-id">中国</span>
										<span class="lang-name">中文</span>
									</span>
							</a>

							<!--Language selector menu-->
							<ul class="head-list dropdown-menu with-arrow">
								<li>
									<!--English-->
									<a class="lang-select">
										<img class="lang-flag" src="${contextPath}/resource/common/img/china.png" alt="中国">
										<span class="lang-id">中国</span>
										<span class="lang-name">中文</span>
									</a>
								</li>
								<li>
									<!--English-->
									<a class="lang-select">
										<img class="lang-flag" src="${contextPath}/resource/common/img/united-kingdom.png" alt="English">
										<span class="lang-id">EN</span>
										<span class="lang-name">English</span>
									</a>
								</li>
								<li>
									<!--France-->
									<a class="lang-select">
										<img class="lang-flag" src="${contextPath}/resource/common/img/france.png" alt="France">
										<span class="lang-id">FR</span>
										<span class="lang-name">Français</span>
									</a>
								</li>
								<li>
									<!--Germany-->
									<a class="lang-select">
										<img class="lang-flag" src="${contextPath}/resource/common/img/germany.png" alt="Germany">
										<span class="lang-id">DE</span>
										<span class="lang-name">Deutsch</span>
									</a>
								</li>
								<li>
									<!--Italy-->
									<a class="lang-select">
										<img class="lang-flag" src="${contextPath}/resource/common/img/italy.png" alt="Italy">
										<span class="lang-id">IT</span>
										<span class="lang-name">Italiano</span>
									</a>
								</li>
								<li>
									<!--Spain-->
									<a class="lang-select">
										<img class="lang-flag" src="${contextPath}/resource/common/img/spain.png" alt="Spain">
										<span class="lang-id">ES</span>
										<span class="lang-name">Español</span>
									</a>
								</li>
							</ul>
						</li>
                    </ul>
                </nav>
            </div>
            <div class="row content-tabs">
                <button class="roll-nav roll-left J_tabLeft"><i class="fa fa-backward"></i>
                </button>
                <nav class="page-tabs J_menuTabs">
                    <div class="page-tabs-content">
                        <a href="javascript:;" class="active J_menuTab" data-id="">首页</a>
                    </div>
                </nav>
                <button class="roll-nav roll-right J_tabRight"><i class="fa fa-forward"></i>
                </button>
                <div class="btn-group roll-nav roll-right">
                    <button class="dropdown J_tabClose"  data-toggle="dropdown">关闭操作<span class="caret"></span>

                    </button>
                    <ul role="menu" class="dropdown-menu dropdown-menu-right">
                        <li class="J_tabShowActive"><a>定位当前选项卡</a>
                        </li>
                        <li class="divider"></li>
                        <li class="J_tabCloseAll"><a>关闭全部选项卡</a>
                        </li>
                        <li class="J_tabCloseOther"><a>关闭其他选项卡</a>
                        </li>
                    </ul>
                </div>
                <a href="${contextPath}/resource/logout" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
            </div>
            <div class="row J_mainContent" id="content-main">
                <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="${contextPath}/sysconfig/list" frameborder="0" data-id="${contextPath}/sysconfig/list" seamless></iframe>
            </div>
            <div class="footer">
                <div class="pull-left"><a href="http://www.baidu.com">ota</a> &copy; 2015-2025</div>
            </div>
        </div>
        <!--右侧部分结束-->
       
       
    </div>
</body>

</@override>

<@override name="css" append="true">
<!-- 这里追加css-->
</@override>

<@override name="js" append="true">

<script src="${contextPath}/resource/common/inspinia.js?v=3.2.0"></script>
<script src="${contextPath}/resource/common/contabs.js"></script>

</@override>

<#assign headerCode = "商旅">
<#assign title = "商旅管理系统">
<#assign description = "商旅">
<#assign keywords = "商旅">

<@extends name="/layouts/layout_template.ftl"/>