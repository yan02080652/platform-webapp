<div class="modal fade" id="modelDic" role="dialog" 
   aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" 
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">
               模态框（Modal）标题
            </h4>
         </div>
         <div class="modal-body">
            <form modelAttribute="dic" class="form-horizontal" id="formDic" role="form">
            	<input type="hidden" name="id" value="${dic.id}"/>
            	<input type="hidden" name="dcId" value="${dic.dcId}"/>
			   <div class="form-group">
			      <label for="itemTxt" class="col-sm-2 control-label">文本</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="itemTxt" name="itemTxt" value="${dic.itemTxt!}"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="itemCode" class="col-sm-2 control-label">编码</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="itemCode" name="itemCode" value="${dic.itemCode!}"/>
			      </div>
			   </div>
			   <div class="form-group">
				    <label for="itemDesc" class="col-sm-2 control-label">说明</label>
				    <div class="col-sm-10">
				   	 <textarea class="form-control" id="itemDesc" name="itemDesc" rows="3" value="${dic.itemDesc!}"/>
				    </div>
				</div>
				<div class="form-group">
			      <label for="cust1" class="col-sm-2 control-label">自定义值1</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="cust1" name="cust1" value="${dic.cust1!}"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="cust2" class="col-sm-2 control-label">自定义值2</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="cust2" name="cust2" value="${dic.cust2!}"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="itemSeq" class="col-sm-2 control-label">序号</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="itemSeq" name="itemSeq" value="${dic.itemSeq!}"/>
			      </div>
			   </div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
					 	<label class="checkbox-inline" style="line-height:10px;">
					  		<input type="checkbox" id="inlineCheckbox1" value="${dic.selectable!}" name="selectable" <#if dic.selectable?? && dic.selectable == 1>checked</#if>> 是否可选
					   </label>
					   <label class="checkbox-inline" style="line-height:10px;">
					  		<input type="checkbox" id="inlineCheckbox2" value="${dic.visible!}" name="visible" <#if dic.visible?? && dic.visible == 1>checked</#if>> 是否可见
					  </label>
					 </div>
			   </div>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" 
               data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="submit()">
               提交更改
            </button>
         </div>
      </div>
</div>