<div class="panel panel-default" id="dicListPanel">
	<div class="panel-heading">
		<#if dicCategory??>
			<input type="hidden" class="dcId" value="${dicCategory.id}"/>
    		${dicCategory.name!}
    	<#else>
    		<span class="label label-danger">请选择一个配置</span>
		</#if>
	</div>
	<div class="panel-body">
		<#if dicCategory?? && dicCategory.id??>
			<div class="col-sm-12" style="margin-bottom:10px;">
				<div class="pull-left">
					<button class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" onclick="refresh()" title="添加"><i class="fa fa-plus"></i> 新增</button>
				</div>
			</div>
		</#if>
		<div class="col-sm-12">
			<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
			<thead>
				<tr>
					<th class="sort-column itemTxt">文本</th>
					<th class="sort-column itemCode">编码</th>
					<th class="sort-column itemDesc">说明</th>
					<th class="sort-column selectable">是否可选</th>
					<th class="sort-column visible">是否可见</th>
					<th class="sort-column cust1">自定义值1</th>
					<th class="sort-column cust2">自定义值2</th>
					<th class="sort-column itemSeq">序号</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
			<#list dicList! as dic>
				<tr id="${dic.id}" 
				<#if dic.pid??>
					pid="${dic.pid}"
				</#if>
				>
					<td><a href="#">${dic.itemTxt!}</a></td>
					<td>${dic.itemCode!}</td>
					<td>${dic.itemDesc!}</td>
					<td>${dic.selectable!}</td>
					<td>${dic.visible!}</td>
					<td>${dic.cust1!}</td>
					<td>${dic.cust2!}</td>
					<td>${dic.itemSeq!}</td>
					<td>
						<button type="button" class="btn btn-warning btn-xs" onclick="update(${dic.id})">修改</button>
						<button type="button" class="btn btn-danger btn-xs">删除</button>
						<button type="button" class="btn btn-info btn-xs">新增下级</button>
					</td>
				</tr>
			</#list>
			</tbody>
			</table>
		</div>
	</div>
</div>