<@override name="body">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-2">
			<div class="panel panel-default">
				<div class="panel-heading"> <span class="fa fa-cog" aria-hidden="true"></span>字典配置</div>
             	<div class="panel-body">
					<div id="treeCategory" class="ztree container-fluid"></div>
            	</div>
        	</div>
		</div>
		<div id="rightDic" class="col-md-10 animated fadeInRight">
			<#--<iframe id="dicContent" src="${contextPath}/sys/dic/list/-1.html" data-url="${contextPath}/sys/dic/list/" width="100%" height="500" frameborder="0"></iframe> -->
			 <#include "dicList.ftl"/>
		</div>
	</div>
</div>

<div id='modelDiv'></div>
</@override>

<@override name="css" append="true">
<link rel="stylesheet" href="${contextPath}/resource/jquery-ztree/3.5.12/css/zTreeStyle/zTreeStyle.css" type="text/css"/>
<link rel="stylesheet" href="${contextPath}/webpage/admin/dic/category.css" type="text/css"/>
</@override>

<@override name="js" append="true">
<script type="text/javascript" src="${contextPath}/resource/jquery-ztree/3.5.12/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript">
var categoryJson = ${dicCategoryJson} || [];
var ctx = "${contextPath}";
</script>
<script type="text/javascript" src="${contextPath}/webpage/admin/dic/category.js"></script>
<script type="text/javascript" src="${contextPath}/resource/jquery/jquery-form.js"></script>
</@override>

<#assign headerCode = "dic">
<#assign title = "字典管理">
<#assign description = "字典配置">
<#assign keywords = "字典 系统  配置">
<@extends name="/layouts/layout_template.ftl"/>