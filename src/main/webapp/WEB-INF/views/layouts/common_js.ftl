<#--通用js-->
<script type="text/javascript">
    var staticResourcePath = '${staticResourcePath}';
    var contextPath='${contextPath}';
</script>

<script src="${contextPath}/resource/js/jquery-1.11.1.min.js"></script>
<script src="${contextPath}/resource/js/bootstrap.min.js"></script>
<script src="${contextPath}/resource/js/sco.message.js"></script>
<script src="${contextPath}/resource/js/headroom.min.js"></script>
<script src="${contextPath}/resource/js/jQuery.headroom.min.js"></script>
<script src="${contextPath}/resource/js/jquery.ztree.all-3.5.min.js"></script>
<script src="${contextPath}/resource/js/plugins/metisMenu/metisMenu.min.js"></script>
 
<script src="${contextPath}/resource/js/PrototypeExtention.js"></script>
<script src="${contextPath}/resource/js/Math.uuid.js"></script>
<script src="${contextPath}/resource/js/json2.js"></script>
<script src="${contextPath}/resource/js/jquery.form.js"></script>
<script src="${contextPath}/resource/js/ajaxfileupload.js"></script>
<script src="${contextPath}/resource/js/jquery.validate.min.js"></script>
<script src="${contextPath}/resource/js/jquery.validate.extention.js"></script>
<script src="${contextPath}/resource/js/jquery.validate.messages_zh.js"></script>
<script src="${contextPath}/resource/js/thread.js"></script>
<script src="${contextPath}/resource/js/jquery.pjax.js"></script>
<script src="${contextPath}/resource/js/admin/admin.js"></script>	
<script src="${contextPath}/resource/js/admin/application.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("#side-menu ul li a").bind("click",function(){
			$(this).addClass("active");
			$(this).parent("ul").addClass("in");
		});
		
	});
</script>