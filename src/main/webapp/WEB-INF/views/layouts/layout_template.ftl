<#escape x as x?html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <@block name="seo" >
        <#include "seo.ftl">
    </@block>

    <@block name="css" >
        <#include "common_css.ftl">
    </@block>
</head>
<body>
    <@block name="header" >
        <#include "common_header.ftl">
    </@block>
    
    <@block name="body"/>

    <@block name="footer" >
        <#include "common_footer.ftl">
    </@block>

    <@block name="js" >
        <#include "common_js.ftl">
    </@block>
</body>
</html>
</#escape>