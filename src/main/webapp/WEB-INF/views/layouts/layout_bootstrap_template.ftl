<#escape x as x?html>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <@block name="seo" >
        <#include "seo.ftl">
   </@block>
   
   <@block name="css" >
        <#include "common_css.ftl">
   </@block>
   
</head>
<body>
	<@block name="header" >
        <#include "common_nav.ftl"/>
    </@block>
    
	<@block name="nav"/>
	<#include "common_left.ftl"/>
	<@block name="bodyxxx"/>
	<#include "common_footer.ftl">
    <@block name="js" >
        <#include "common_js.ftl">
    </@block>
</body>
</html>
</#escape>