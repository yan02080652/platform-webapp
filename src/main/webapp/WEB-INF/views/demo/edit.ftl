<@override name="body">
<div>

<form id="demoForm" name="demoForm" action="${contextPath}/demos/save.html" method="POST">
<table border="0" width="800" height="500">
	<input type="hidden" name="id" value="${demo.id ! ''}" />
	<tr>
		<td>序号: </td>
		<td>${demo.id ! ''}</td>
	</tr>
	<tr>
		<td>标题:</td>
		<td><input name="title" value="${demo.title ! ''}"/></td>
	</tr>
	<tr>
		<td>简介:</td>
		<td><input name="description" value="${demo.description ! ''}"/></td>
	</tr>
	<tr>
		<td><input type="submit" value="保存" /></td>
		<td><input type="button" value="返回" /></td>
	</tr>
</table>
</form>
</div>
</@override>

<@override name="css" append="true">
<!-- 这里追加css-->
</@override>

<@override name="js" append="true">
<!-- 这里追加js-->
</@override>

<#assign headerCode = "demo">
<#assign title = "demo的列表页">
<#assign description = "demo的列表页">
<#assign keywords = "demo的列表页">

<@extends name="/layouts/layout_template.ftl"/>