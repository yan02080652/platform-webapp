<%@ page contentType="text/html;charset=UTF-8" language="java"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%
	String basePath = request.getContextPath() + "/";
%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="UTF-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="<%=basePath%>">
<title><tiles:insertAttribute name="title" /></title>
<tiles:insertAttribute name="common_css" />
<tiles:insertAttribute name="common_js" />
</head>
<body>
	<tiles:insertAttribute name="header"/>
	<div id="page-wrapper">
		<tiles:insertAttribute name="body" />
	</div>
	<%--<footer>
		<tiles:insertAttribute name="footer" />
	</footer>
	<script type="text/javascript">
		document.getElementById("page-wrapper").style.minHeight = (document.body.scrollHeight-85)+'px';
	</script>--%>
</body>
</html>