<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- jquery -->
<script src="resource/js/jquery/jquery-3.1.0.min.js?version=${globalVersion}"></script>
<script src="resource/js/jquery/jquery-confirm.js?version=${globalVersion}"></script>
<script src="resource/js/jquery/jquery.form.js?version=${globalVersion}"></script>
<!-- <script src="resource/js/jquery/jquery.pjax.js"></script> -->

<!-- jquery自带的验证 -->
<script src="resource/js/jquery/jquery.validate.js?version=${globalVersion}"></script>
<script src="resource/js/jquery/jquery.validate.extention.js?version=${globalVersion}"></script>
<script src="resource/js/jquery/jquery.validate.messages_zh.js?version=${globalVersion}"></script>

<!-- bootstrap -->
<script src="resource/js/bootstrap/bootstrap.min.js?version=${globalVersion}"></script>
<script src="resource/js/bootstrap/bootstrapValidator.js?version=${globalVersion}"></script>
<script src="resource/js/bootstrap/zh_CN.js?version=${globalVersion}"></script>

<!-- plugins -->
<script src="resource/plugins/layer/layer.js?version=${globalVersion}"></script>
<script src="resource/plugins/layui/layui.js?version=${globalVersion}"></script>
<script src="resource/plugins/PrototypeExtention.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.message.js?version=${globalVersion}"></script>
<script src="resource/plugins/json2.js?version=${globalVersion}"></script>
<script src="resource/js/jquery/jquery.ztree.all-3.5.min.js?version=${globalVersion}"></script>
<script type="text/javascript" src="resource/js/bootstrap/fileinput.min.js?version=${globalVersion}"></script>
<script type="text/javascript" src="resource/js/bootstrap/fileinput_locale_zh.js?version=${globalVersion}"></script>
<!-- for TR -->
<script src="resource/js/platform/application.js?version=${globalVersion}"></script>
<script src="resource/js/jquery/require.js?version=${globalVersion}"></script>
<script src="resource/js/platform/requireConfig.js?version=${globalVersion}"></script>
<script src="resource/js/platform/tr-common.js?version=${globalVersion}"></script>
<script src="resource/js/tr-widget/tr-widget.js?version=${globalVersion}"></script>
<script src="resource/js/tr-widget/dialog.js?version=${globalVersion}"></script>