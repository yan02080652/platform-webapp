<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" href="resource/plugins/layui/css/layui.css?version=${globalVersion}">

<link rel="stylesheet" href="resource/css/bootstrap/bootstrap.min.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/bootstrap/bootstrapValidator.css?version=${globalVersion}">

<link rel="stylesheet" href="resource/css/plugins/font-awesome-4.7.0/css/font-awesome.min.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/iconfont/iconfont.css?version=${globalVersion}">

<link rel="stylesheet" href="resource/css/platform/header.css?version=${globalVersion}">

<link rel="stylesheet" href="resource/plugins/sco/css/sco.message.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/jquery/zTreeStyle/metro.css?version=${globalVersion}">

<link rel="stylesheet" href="resource/css/jquery/jquery-confirm.css?version=${globalVersion}">

<link href="resource/css/platform/admin.css?version=${globalVersion}" rel="stylesheet">
<link href="resource/css/platform/tr-widget.css?version=${globalVersion}" rel="stylesheet">