<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%
	String basePath = request.getContextPath() + "/";
	request.setAttribute("CTX_PATH", basePath);
%>

<style>
	.header-w {
		color: #f7f7f7;
	}
	.header-left {
		height: 100%;
	}
	.header-title {
		display: inline-block;
		height: 100%;
		line-height: 40px;
		font-size: 20px;
		font-weight: bold;
	}
	.logo-div {
		display: inline-block;
		width: 30px;
	}
	.header-div {
		vertical-align: top;
		display: inline-block;
	}
	.logout-div {
		width: 50px;
		margin-top:0px;
		line-height:45px;
	}
	.choose-pwd {
		line-height: 45px;
		width: 65px;
	}
</style>
<div id="header">
	<div class="header-w">
		<div class="header-left">
			<div class="logo-div"></div>
			<span class="header-title">

			</span>
		</div>
		<div class="header-right">
 			<div class="header-div">
				<ul class="clearfix" id="menu"/>
			</div>
			<div class="header-div choose-pwd">
				<a href="${CTX_PATH}platform/userSecurity/index">修改密码</a>
			</div>
			<div class="header-div logout-div">
				<a href="${CTX_PATH}security_logout">注销</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $.get('platform/mainInfo/getPartnerName',function(data){
		$(".header-title").html(data.partnerName);
	})

	//加载系统菜单
	$.ajax({
		type : 'GET',
		url : "platform/mainInfo/getCurUserMenuInfo",
		success : function(data) {
			treeData = TR.turnToZtreeData(data.menuList);
			var menuHtml = "";
			menuHtml = getMenuHtml(treeData,menuHtml,false);
			$("#menu").html(menuHtml);
			
			/**
			 * isChild 表示是不是子节点传过来的迭代
			 */
			function getMenuHtml(data,isChild) {
				$.each(data, function(i, d) {	
					if(d.data.status==0){//生效
						if(d.children){//存在子节点
							menuHtml = menuHtml + "<li><a href='javascript:;'>"+d.data.name+"</a><ul>";//目录
							getMenuHtml(d.children,menuHtml,true);
						}else{
							href = '';
							param = '';
                            if(d.data.appendix){
                                param = d.data.appendix;
                            }
							if(d.data.type == 0){//URL菜单
								if(d.data.def){
									if(d.data.def.substring(0,1) == '/'){
										href = '<%=basePath%>'+d.data.def.substring(1,d.data.def.length);
									}else{
										href = '<%=basePath%>'+d.data.def;
									}
								}else{
									href = "javascript:;";
								}
								
							}else if(d.data.type == 1){//作业
								if(d.data.trans && d.data.trans.def){
									if(d.data.trans.def.substring(0,1) == '/'){// 已/开头说明是系统中的
										href = '<%=basePath%>'+d.data.trans.def.substring(1,d.data.trans.def.length);
									}else{
										href = '<%=basePath%>'+d.data.trans.def;
									}
								}else{
									href = "javascript:;";
								}
								
							}else if(d.data.type == 2){//目录
								href = "javascript:;";
							}
							if(param == ''){
								menuHtml = menuHtml + "<li><a href='"+href+"'>"+d.data.name+"</a></li>";
							}else{
								menuHtml = menuHtml + "<li><a href='"+href+"?"+param+"'>"+d.data.name+"</a></li>";
							}
						}
						
					}
				});
				if(isChild){
					menuHtml = menuHtml +"</ul></li>";
				}
				return menuHtml;
			}
			
			
		}
	});
});



</script>
