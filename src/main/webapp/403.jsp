<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	request.setAttribute("CTX_PATH", request.getContextPath());
%>
<html>
<head>
<title>服务器出错</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
	var __ctxPath="<%=request.getContextPath()%>";
</script>

<style type="text/css">
	body {
		
	}
	table {
		width: 100%;
		height: 100%;
	}
	.msg-container {
		width: 500px;
		text-align: left;
		padding: 60px 30px;
		border: 1px solid #D8D8D8;
		border-radius: 7px 7px 7px 7px;
		box-shadow: 0 1px 4px #D3D3D3;
		background-color: #f8f8f8;
		font-weight: bold;
		font-size: 20px;
	}
</style>
</head>
</style>
</head>
<body>
	<table border="0">
		<tr>
			<td align="center" valign="middle">
				<div class="msg-container">
					非法访问。
				</div>
			</td>
		</tr>
	</table>
</body>
</html>