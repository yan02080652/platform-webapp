<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link href="webpage/flow/config/detail.css?version=${globalVersion}" rel="stylesheet">
<div class="panel panel-default">
	<div class="panel-heading">
		<i class="fa fa-cog" aria-hidden="true"></i> ${empty def?'新增工作流':'编辑工作流' }<span
			class="title_status"></span> <a
			href="flow-config/index.html?categoryId=${categoryId }"
			class="pull-right" style="text-decoration: underline;">返回</a>
	</div>
	<div class="panel-body">
		<input type="hidden" value="${def.id }" id="defId" />
		<div class="splitDiv">
			<span>基本信息</span>
		</div>
		<div class="row">
			<form id="processDef_form" class="form-horizontal" method="post">
				<div class="form-group">
					<label class="col-sm-2 control-label">分类目录：</label>
					<div class="col-sm-5">
						<select class="form-control" name="category">
							<option value="">请选择分类</option>
							<c:forEach items="${categorys }" var="cg">
								<c:choose>
									<c:when test="${cg.itemCode eq categoryId}">
										<option selected="selected" value="${cg.itemCode }">${cg.itemTxt }</option>
									</c:when>
									<c:otherwise>
										<option value="${cg.itemCode }">${cg.itemTxt }</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">名称：</label>
					<div class="col-sm-5">
						<input type="text" name="name" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">编码：</label>
					<div class="col-sm-5">
						<input type="text" name="code" class="form-control"
							${empty def?'':'readonly'}>
					</div>
				</div>
				<!-- 
				<div class="form-group">
					<label class="col-sm-2 control-label">自动合并执行：</label>
					<div class="col-sm-5 checkbox">
						<label><input type="checkbox" name="autoMerge"></label>
					</div>
				</div>
				 -->
				<div class="form-group">
					<label class="col-sm-2 control-label">描述：</label>
					<div class="col-sm-5">
						<textarea name="descr" class="form-control"></textarea>
					</div>
				</div>
			</form>
		</div>
		<div class="splitDiv">
			<span>节点配置</span>
		</div>
		<div class="row nodeFieldSet">
			<div class="col-sm-5 node_parent">
				<button type="button" class="btn btn-success fa fa-plus"
					id="addNodeButton" onclick="pageVal.addNode()">添加节点</button>
			</div>
			<div class="col-sm-7">
				<div class="panel panel-default" id="panelDetail">
					<div class="panel-heading">
						节点明细 <i class="fa fa-times pull-right" aria-hidden="true"
							onclick="closeForm(true)"></i>
					</div>
					<div class="panel-body">
						<form id="processNode_form" class="form-horizontal" method="post"
							style="margin-top: 8px;">
							<div class="splitDiv">
								<span>基本信息</span>
							</div>
							<input type="hidden" name="nodeId" />
							<div class="form-group">
								<label class="col-sm-2 control-label">节点名称：</label>
								<div class="col-sm-9">
									<input type="text" name="nodeName1" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">系统作业：</label>
								<div class="col-sm-9">
									<input type="text" name="transId" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">单据状态：</label>
								<div class="col-sm-9">
									<input type="number" name="stage" class="form-control" />
								</div>
							</div>
							<div class="splitDiv">
								<span>设置执行人</span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">指定人员：</label>
								<div class="col-sm-9">
									<div class="input-group">
										<input type="hidden" name="userId">
										<input type="text" class="form-control not-editable" readonly="readonly" name="userId_name">
										<span class="input-group-btn">
											<button class="btn btn-default" type="button" onclick="chooseUsers()">
												<span class="fa fa-user"></span>
											</button>
										</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">指定角色：</label>
								
								<div class="col-sm-9" id="c_orgrole">
									<div class="form-group orgRoleGroup">
										<label class="col-sm-1 control-label second-level-label">单位</label>
										<div class="col-sm-5">
											<input type="hidden" name="orgData">
											<input type="text" class="form-control not-editable" name="orgName" readonly="readonly" onclick="chooseRole_OrgDlg(this)">
										</div>
										
										<label class="col-sm-1 control-label second-level-label">角色</label>
										<div class="col-sm-4">
											<input type="hidden" class="form-control" name="roleData">
											<input type="text" class="form-control not-editable" readonly="readonly" name="roleName" onclick="chooseRoles(this)">
										</div>
										
										<div class="col-sm-1 form-icon">
											<i class="fa fa-times" aria-hidden="true" title="删除此组" onclick="deleteOrgRoleGroup(this)"></i>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">　</label>
								<div class="col-sm-9" style="padding-left:20px;">
									<button type="button" class="btn fa fa-plus" onclick="addOrgRoleGroup()"> 添加一组</button>
								</div>
							</div>
							<!--
							<div class="form-group">
								<label class="col-sm-4 control-label">发起人直管经理：</label>
								<div class="col-sm-5"></div>
							</div> 
							<div class="form-group">
								<label class="col-sm-4 control-label">自由选人：</label>
								<div class="col-sm-5"></div>
							</div>-->
							<div class="splitDiv">
								<span>通知</span>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">邮件通知：</label>
								<div class="col-sm-9 checkbox">
									<label><input type="checkbox" class="ignore" id="el_sendMail" /></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">短信通知：</label>
								<div class="col-sm-9 checkbox">
									<label><input type="checkbox" class="ignore" id="el_sendSms" /></label>
								</div>
							</div>
						</form>
						<div class="splitDiv">
							<span>出口路径</span>
						</div>
						<div class="container-fluid route-table-c" style="padding:10px">
							<table class="route-table table table-bordered table-striped table-hover">
								<tr>
									<th width="8%">#</th>
									<th width="20%">路径名称</th>
									<th width="15%">路径编码</th>
									<th width="20%">目标节点</th>
									<th>路径条件</th>
									<th width="10%">操作</th>
								</tr>
							</table>
							<button class="btn btn-default fa fa-plus" onclick="editNodeRoute()">添加一行</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<span class="center-block bottom_opt"
			style="width: 200px; margin-top: 8px;">
			<c:if test="${def.status eq 1 }">
				<button type="button" class="btn btn-info fa fa-rss btn-lg"
					onclick="saveAndDeploy(2)">重新发布</button>
			</c:if>
			<c:if test="${empty def or def.status eq 2}">
				<button type="button" class="btn btn-info fa fa-floppy-o btn-lg"
					onclick="saveAndDeploy(1)">保存</button>
				<button type="button" class="btn btn-info fa fa-rss btn-lg"
					onclick="saveAndDeploy(2)">发布</button>
			</c:if>
		</span>
	</div>
</div>
<div id="hideModel">
	<!-- 节点dom模板 -->
	<div class="panel panel-success node-sample hideDiv">
		<div class="panel-heading">
			<span class="panel-title nodeName">
				&nbsp;
			</span>
			<span class="pull-right">
				<i class="fa fa-arrow-up optIcon" aria-hidden="true" title="上移"></i>
				<i class="fa fa-arrow-down optIcon" aria-hidden="true" title="下移"></i>
				<i class="fa fa-times optIcon" aria-hidden="true" title="删除"></i>
			</span>
		</div>
		<div class="panel-body">
			<div class="node-executor-c">
				<div class="container-fluid">
					<div class="row" style="min-height:30px; line-height: 30px;">
						<div class="col-sm-2" padding="0">执行人</div>
						<div class="col-sm-10 node-executor"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 节点dom中执行人行模板 -->
	<div class="executor-tmpl hideDiv">
		<div class="row" style="min-height:30px; line-height: 30px;">
			<div class="col-sm-12 node-executor-text"></div>
		</div>
	</div>
	<!-- 出口路径行模板 -->
	<div class="noderoute-tmpl hideDiv">
		<table>
			<tr>
				<td class="route-info route-seq">1</td>
				<td class="route-info route-name">&nbsp;</td>
				<td class="route-info route-code">&nbsp;</td>
				<td class="route-info route-destnode">&nbsp;</td>
				<td class="route-info route-condition">&nbsp;</td>
				<td>
					<button class="btn btn-link btn-xs" onclick="editNodeRoute(this)"><i class="fa fa-pencil-square-o"></i></button>
					<button class="btn btn-link btn-xs" onclick="deleteNodeRoute(this)"><i class="fa fa-times"></i></button>
				</td>
			</tr>
		</table>
	</div>
	
	<!-- 单位选择对话框 -->
	<div class="panel-body orgRole_orgdlg hideDiv">
		<form class="form-horizontal" role="form">
			<div class="form-group">
				<label class="col-sm-3 control-label">指定单位&nbsp;<input type="radio" name="orgMode" value="1" class="radio-inline"></label>
				<div class="col-sm-4">
					<div class="input-group">
						<input type="hidden" id='orgRole_orgdlg_orgId' name="orgId">
						<input type="text" id='orgRole_orgdlg_orgName' class="form-control not-editable" readonly="readonly" name="orgName">
						<span class="input-group-addon" onclick="chooseOrg('#orgRole_orgdlg_orgId', '#orgRole_orgdlg_orgName')"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">指定组织层级&nbsp;<input type="radio" name="orgMode" value="2" class="radio-inline"></label>
				<div class="col-sm-4">
					<div class="input-group">
						<input type="hidden" id='orgRole_orgdlg_orgGrade' name="orgGrade">
						<input type="text" id='orgRole_orgdlg_orgGradeName' class="form-control not-editable" readonly="readonly" name="orgGradeName">
						<span class="input-group-addon" onclick="chooseOrgGrade('#orgRole_orgdlg_orgGrade', '#orgRole_orgdlg_orgGradeName')"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
					</div>
				</div>
				<label class="col-sm-2 control-label">组织类型</label>
				<div class="col-sm-3">
					<div class="input-group">
						<input type="hidden" id='orgRole_orgdlg_orgType1' name="orgType1">
						<input type="text" id='orgRole_orgdlg_orgTypeName1' class="form-control not-editable" readonly="readonly" name="orgTypeName1">
						<span class="input-group-addon" onclick="chooseOrgType('#orgRole_orgdlg_orgType1', '#orgRole_orgdlg_orgTypeName1')"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">相对当前单位层级&nbsp;<input type="radio" name="orgMode" value="3" class="radio-inline"></label>
				<div class="col-sm-4">
					<input type="number" id='orgRole_orgdlg_orgLevel' class="form-control" name="orgLevel">
				</div>
				<label class="col-sm-2 control-label">组织类型</label>
				<div class="col-sm-3">
					<div class="input-group">
						<input type="hidden" id='orgRole_orgdlg_orgType2' name="orgType2">
						<input type="text" id='orgRole_orgdlg_orgTypeName2' class="form-control not-editable" readonly="readonly" name="orgTypeName2">
						<span class="input-group-addon" onclick="chooseOrgType('#orgRole_orgdlg_orgType2', '#orgRole_orgdlg_orgTypeName2')"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
					</div>
				</div>
			</div>
		</form>
	</div>
	
	<!-- 单位角色模版 -->
	<div class="orgRole_group hideDiv">
		<div class="form-group orgRoleGroup">
			<label class="col-sm-1 control-label second-level-label">单位</label>
			<div class="col-sm-5">
				<input type="hidden" name="orgData">
				<input type="text" class="form-control not-editable" name="orgName" readonly="readonly" onclick="chooseRole_OrgDlg(this)">
			</div>
			
			<label class="col-sm-1 control-label second-level-label">角色</label>
			<div class="col-sm-4">
				<input type="hidden" class="form-control" name="roleData">
				<input type="text" class="form-control not-editable" readonly="readonly" name="roleName" onclick="chooseRoles(this)">
			</div>
			
			<div class="col-sm-1 form-icon">
				<i class="fa fa-times" aria-hidden="true" title="删除此组" onclick="deleteOrgRoleGroup(this)"></i>
			</div>
		</div>
	</div>
	<!-- 路径对话框 -->
	<div class="panel-body nodeRouteDlg hideDiv">
		<form class="form-horizontal" role="form">
			<input type="hidden" name="routeId">
			<div class="form-group">
				<label class="col-sm-3 control-label">名称</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="name">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">编码</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="code">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">目标节点</label>
				<div class="col-sm-8">
					<div class="input-group">
						<input type="hidden" name="nodeId">
						<input type="text" class="form-control not-editable" readonly="readonly" name="nodeName1">
						<span class="input-group-addon" onclick="chooseNode(this)"><i class="fa fa-caret-down" aria-hidden="true"></i></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">路径条件</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="expr">
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	var orgTypes = [
		{code: '', name: '不限制'}
		<c:forEach items="${orgTypes}" var="type" varStatus="sta">
			,{code: '${type.itemCode}', name: '${type.itemTxt}'}
		</c:forEach>
	];
</script>
<script type="text/javascript" src="webpage/flow/config/detail.js?version=${globalVersion}"></script>