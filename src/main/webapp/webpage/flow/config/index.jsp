<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="container">
	<div class="row">
		<h4>
			<i class="glyphicon glyphicon-home" aria-hidden="true"></i>企业流程配置
		</h4>
		<div class="form-group" style="margin-left: 30px;">
  		 	<%@ include file="/webpage/common/partnerChange.jsp"%>
  		</div>
	</div>
	<div class="row">
		<div class="col-md-3 list_category" style="padding-left: 0px;">
			<c:forEach items="${categorys }" var="ct">
				<c:choose>
					<c:when test="${ct.itemCode eq  categoryId}">
						<a href="javascript:void()" class="list-group-item active" data-code="${ct.itemCode }">${ct.itemTxt }</a>
					</c:when>
					<c:otherwise>
						<a href="flow-config/index.html?categoryId=${ct.itemCode }"
							class="list-group-item">${ct.itemTxt }</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>
		<div class="col-md-9">
			<a href="flow-config/detail?categoryId=${categoryId }" class="btn btn-info fa fa-plus"> 新增流程</a> 
			<table class="table table-striped table-bordered table-hover" id="userTable" style="margin-top: 5px;">
				<thead>
					<tr>
						<th><label>流程名称 </label></th>
						<th><label>编码</label></th>
						<th width="80"><label>状态</label></th>
						<th width="120"><label>自动合并执行</label></th>
						<th width="120"><label>最后更新时间</label></th>
						<th><label>操作</label></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list }" var="def">
						<tr class="odd gradeX">
							<td>${def.name}</td>
							<td>${def.code}</td>
							<td><c:if test="${def.status eq 1}">正常</c:if> <c:if
									test="${def.status eq 0}">停用</c:if> <c:if
									test="${def.status eq 2}">未发布</c:if></td>
							<td>
								<c:choose>
									<c:when test="${def.autoMerge }">是</c:when>
									<c:otherwise>否</c:otherwise>
								</c:choose>
							</td>
							<td><fmt:formatDate value="${def.updateDate }" pattern="yyyy-MM-dd"/></td>
							<td>
								<a class="fa fa-pencil-square-o" href="flow-config/detail?defId=${def.id}">编辑</a>
								<a class="fa fa-times" href="javascript:deleteProcessDef('${def.id}','${def.name}')">删除</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include
					page="/webpage/common/pagination_ajax.jsp?callback=search_flowConfig"></jsp:include>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
function search_flowConfig(pageIndex){
	var code = $('.list_category .active').data('code');
	window.location.href = "/flow-config/index.html?categoryId="+code+"&pageIndex="+(pageIndex || 1);
};

//删除流程
function deleteProcessDef(id,name){
	layer.confirm('确认删除流程配置【'+name+'】，以及所有附属节点数据？', function(index){
		$.ajax({
			url:'flow-config/delete',
			data:{
				id:id
			},
			success:function(rs){
				if('success' == rs.type){
					layer.msg(rs.txt+',即将刷新.....', {icon: 1});
					setTimeout(function(){
						window.location.reload();
					},600);
				}else{
					layer.msg('删除失败',{icon: 2});
				}
			}
		});
		layer.close(index);
	});
	
	/* $.confirm('确认删除流程配置【'+name+'】，以及所有附属节点数据？',"提示",function(){
		$.ajax({
			url:'flow-config/delete',
			data:{
				id:id
			},
			callback:function(rs){
				console.log(rs)
			}
		});
	}); */
};
</script>