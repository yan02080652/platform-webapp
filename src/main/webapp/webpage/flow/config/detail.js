var pageVal = {
		uuNodeId: -10000,
		getId:function(){
			return ++this.uuNodeId;
		},
		getNode:function(nodeId){
			return this.nodesMap[nodeId];
		},
		addNode:function(){
			this.closeFormEvent = function (){
				var nodeId = pageVal.getId();
				var node = {
					id:nodeId
				};
				pageVal.nodesMap[nodeId] = node;
				appendNodeDom([node]);
				refreshNodesDom();
				editNode(nodeId);
			};
			closeForm(true);
		},
		deleteNode:function(nodeId){
			closeForm(false);
			if(nodeId > 0){
				pageVal.deleteNodeIds[nodeId];
			}
			delete pageVal.nodesMap[nodeId];
			refreshNodesDom();
		},
		getEditingNodeId: function() {
			var nodeId = $('.editing-node').data('nodeId');
			return nodeId;
		},
		getRoute:function(routeId){
			var rs = null;
			$.each(this.nodeRoutesMap, function(nodeId, routes) {
				$.each(routes, function(i, route) {
					if(route.id == routeId) {
						rs = route;
						return false;
					}
				});
				if(rs) {
					return false;
				}
			});
			
			return rs;
		},
		objNames: {
			users: {},
			orgs: {},
			orggs: {},
			orgTypes: {},
			roles: {}
		},
		nodesMap:{},
		nodeRoutesMap: {},
		deleteNodeIds:{}//标记删除的节点数据
};

$(document).ready(function(){
	$.validator.addMethod("checkUniqNodeCode", function(value, element,params) {
		if('' == $.trim(value)){
			return false;
		}
		var curNodeId = $('#processNode_form input[name=nodeId]').val();
		var flag = true;
		$.each(pageVal.nodesMap,function(nodeId,node){
			if(nodeId != curNodeId && node.nodeName == value){
				flag = false;
				return false;
			}
		});
	    return flag;
	}, "节点名称不能重复");
	
	loadData();
	initProcDefFormValidator();
	initNodeFormValidator();
	initRouteFormValidator();
});

//加载数据
function loadData(){
	var defId = $('#defId').val();
	if(!defId){
		var node = {
			id: pageVal.getId(),
			nodeName: '结束',
			nodeType: 'END',
			stage: 99
		};
		
		pageVal.endNodeId = node.id;
		pageVal.nodesMap[node.id] = node;
		return;
	}
	
	pageVal.loading_index = layer.load(1, {
		  shade: [0.5,'#fff'] //0.1透明度的白色背景
	});
	$.ajax({
		url:'flow-config/getProDef',
		data:{
			defId:defId
		},
		dataType:'json',
		success:function(data){
			pageVal.data = data || {};
			initForm();
			layer.close(pageVal.loading_index);
		}
	});
}

//初始化第一个form
function initProcDefFormValidator() {
	var codeValidate = {
		required: true,
		maxlength: 30
	};
	var defId = $('#defId').val();
	if(!defId){
		codeValidate.remote = {
			url: "flow-config/checkCode",     //后台处理程序
			type: "post",               //数据发送方式
			//dataType: "json",           //接受数据格式   
			data: {                     //要传递的数据
			    code: function() {
			    	return $("#processDef_form").find('input[name=code]').val();
			    }
			}
		};
	}
	$("#processDef_form").validate({
		rules: {
			category: {
				required: true
			},
			name: {
				required: true,
				maxlength: 30
			},
			code: codeValidate
		},
		messages:{
			code:{
				remote: '编码已存在'
			}
		},
		submitHandler : function(form) {
			var data = getProcessDefData(form);
			$.ajax({
				url:'flow-config/save',
				type:'post',
				data:{
					processDef:JSON.stringify(data),
					type:pageVal.type
				},
				success:function(rs){
					if(pageVal.type == 1){
						layer.msg('保存成功!');
						setTimeout(function(){
							window.location.href = "/flow-config/detail?defId=" + rs;
						},600);
					}else if(pageVal.type == 2){
						layer.msg('流程已发布, 即将返回.....', {icon: 1});
						setTimeout(function(){
							window.location.href = "/flow-config/index.html?categoryId="+data.category;
						},600);
					}
				}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

//初始化表单数据
function initForm(){
	var data = pageVal.data;
	if(!data){
		return;
	}
	var form = $('#processDef_form');
	var category = form.find('[name=category]');
	var name = form.find('[name=name]');
	var code = form.find('[name=code]');
	var autoMerge = form.find('[name=autoMerge]');
	var descr = form.find('[name=descr]');
	category.val(data.category);
	name.val(data.name);
	code.val(data.code);
	descr.val(data.descr);
	if(data.autoMerge){
		autoMerge.attr('checked','checked');
	}
	var span = null;
	if(data.status == 1){
		span = "<span style='color:green;'>正常</span>";
	}else if(data.status == 2){
		span = "<span>未发布</span>";
	}else if(data.status == 0){
		span = "<span style='color:red;'>已停用</span>";
	}
	$(".title_status").html(span);
	
	var actionUsers = {
		userIds:{},
		orgIds:{},
		roleIds:{},
		orggs: {},
		orgTypes: {}
	};
	
	function insertActionUsers(ud){
		//用户
		if(ud.userId){
			var userIds = ud.userId.split(',');
			$.each(userIds,function(i,userId){
				actionUsers.userIds[userId] = true;
			});
		}
		//单位&&角色
		if(ud.orgRole){
			var ors = ud.orgRole.split(';');
			$.each(ors,function(i,or){
				var or_ = or.split('|');
				actionUsers.orgIds[or_[0]] = true;
				var roles = or_[1].split(',');
				$.each(roles,function(j,rl){
					actionUsers.roleIds[rl] = true;
				});
			});
		}
		//相对层级和单位
		if(ud.orgLevelRole){
			var ors = ud.orgLevelRole.split(';');
			$.each(ors,function(i,or){
				var or_ = or.split('|');
				var roles = or_[1].split(',');
				$.each(roles,function(j,rl){
					actionUsers.roleIds[rl] = true;
				});
				
				var orgType = or_[2];
				if(orgType) {
					actionUsers.orgTypes[orgType] = true;
				}
			});
		}
		//组织层面
		if(ud.orgGradeRole){
			var ors = ud.orgGradeRole.split(';');
			$.each(ors,function(i,or){
				var or_ = or.split('|');
				var orgg = or_[0];
				actionUsers.orggs[orgg] = true;
				
				var roles = or_[1].split(',');
				$.each(roles,function(j,rl){
					actionUsers.roleIds[rl] = true;
				});
				
				var orgType = or_[2];
				if(orgType) {
					actionUsers.orgTypes[orgType] = true;
				}
			});
		}
	}
	
	var nodesData = data.nodes || [];
	var nodesMap = {};
	$.each(nodesData,function(k,node){
		nodesMap[node.id] = node;
		insertActionUsers(node);
	});
	pageVal.nodesMap = nodesMap;
	
	nodesData.sort(function(n1,n2){
		var s1 = n1.seq || 0, s2 = n2.seq || 0;
		return s1 == s2 ? 0 : (s1 > s2 ? 1 : -1);
	});
	
	loadActionUsers(actionUsers);
	
	var nodeRoutesMap = pageVal.nodeRoutesMap;
	var nodeRoutes = data.nodeRoutes || [];
	$.each(nodeRoutes, function(i, route){
		var fromNodeId = route.fromNodeId;
		var routes = nodeRoutesMap[fromNodeId] = nodeRoutesMap[fromNodeId] || [];
		routes.push(route);
	});
	
	bulidNodeDom(nodesData);
}

/**
 * 加载执行人名称
 */
function loadActionUsers(actionUsers){
	var userIds = [];
	$.each(actionUsers.userIds,function(userId,v){
		userIds.push(userId);
	});
	var orgIds = [];
	$.each(actionUsers.orgIds,function(orgId,v){
		orgIds.push(orgId);
	});
	var roleIds = [];
	$.each(actionUsers.roleIds,function(roleId,v){
		roleIds.push(roleId);
	});
	var orggs = [];
	$.each(actionUsers.orggs,function(orgg, v){
		orggs.push(orgg);
	});
	var orgTypes = [];
	$.each(actionUsers.orgTypes,function(orgType, v){
		orgTypes.push(orgType);
	});
	
	$.ajax({
		url:'flow-config/loadObjNames',
		async:false,
		data:{
			userIds:userIds.join(",") || null,
			orgIds:orgIds.join(",") || null,
			roleIds:roleIds.join(",") || null,
			orggs: orggs.join(',') || null,
			orgTypes: orgTypes.join(',') || null
		},
		dataType:'json',
		success:function(result){
			pageVal.objNames = result || {};
		},
		error:function(){
			layer.msg('名称数据加载失败');
			$('.bottom_opt').remove();
		}
	});
}

function bulidNodeDom(nodes){
	var node2 = [];
	var hasEndNode = false;
	$.each(nodes, function(i, node){
		//设置执行人名称
		if('END' == node.nodeType){
			pageVal.endNodeId = node.id;
			hasEndNode = true;
			return;
		}
		
		node2.push(node);
	});
	
	if(!hasEndNode) {
		var node = {
			id: pageVal.getId(),
			nodeName: '结束',
			nodeType: 'END',
			stage: 99
		};
		
		pageVal.endNodeId = node.id;
		pageVal.nodesMap[node.id] = node;
	}
	
	$('.node_parent').children().not('#addNodeButton').remove();
	
	appendNodeDom(node2);
	//去掉不必要的元素
	refreshNodesDom();
	closeForm(false);
}

//填充stepDom
function appendNodeDom(nodes){
	var addNodeButtonDom = $('#addNodeButton');
	var sampleNodeDom = $('#hideModel .node-sample');//节点dom
	$.each(nodes, function(i, node){
		var nodeDom = sampleNodeDom.clone();
		nodeDom.addClass('nodeId_' + node.id);
		nodeDom.addClass('flow-node').data('nodeId',node.id);
		
		addNodeButtonDom.before(nodeDom);
		
		refreshNodeDom(node.id);
		
		nodeDom.click(editFn);
		$('.panel-heading .fa-times', nodeDom).click(deleteFn);
		$('.panel-heading .fa-arrow-up', nodeDom).click(moveUp);
		$('.panel-heading .fa-arrow-down', nodeDom).click(moveDown);
	});
	
	function deleteFn(event){
		var cur = $(this);
		var nodeDom = cur.parentsUntil('.node_parent','.flow-node');
		layer.confirm('确认删除此节点?', {icon: 3, title:'提示'}, function(index){
			var nodeId = nodeDom.data('nodeId');
			nodeDom.remove();
			layer.close(index);
			pageVal.deleteNode(nodeId);
		});
		event.preventDefault();
	};
	
	function editFn(event){
		if(event.isDefaultPrevented()){
			return;
		}else{
			editNode($(this).data('nodeId'));
		}
	};
}

//去掉步骤元素中一些不必要的元素
function refreshNodesDom(){
	var nodeDoms = $('.node_parent .flow-node');
	
	var upDom = "<i class='fa fa-arrow-up optIcon' aria-hidden='true' title='上移'></i>";
	var downDom = "<i class='fa fa-arrow-down optIcon' aria-hidden='true' title='下移'></i>";

	var length = nodeDoms.length;
	$.each(nodeDoms, function(i, nodeDom){
		nodeDom = $(nodeDom);
		var nodeId = nodeDom.data('nodeId');
		var node = pageVal.getNode(nodeId);
		node.seq = i;
		
		var cancelDom = $('.panel-heading .fa-times', nodeDom);
		$('.fa-arrow-up,.fa-arrow-down', nodeDom).remove();
		if(i != 0){
			var ud = $(upDom).click(moveUp);
			ud.insertBefore(cancelDom);
		}
		if(i != length-1){
			var dd = $(downDom).click(moveDown);
			dd.insertBefore(cancelDom);
		}
	});
}

//上移一步
function moveUp(){
	var step = $(this).parentsUntil('.node_parent','.node-sample');
	var preStep = step.prev();
	if(preStep.length>0){
		step.insertBefore(preStep);
		refreshNodesDom();
	}else{
		layer.msg('无法上移');
	}
}

//下移一步
function moveDown(){
	var step = $(this).parentsUntil('.node_parent','.node-sample');
	var nextStep = step.next();
	if(nextStep.length>0){
		step.insertAfter(nextStep);
		refreshNodesDom();
	}else{
		layer.msg('无法下移');
	}
}

//编辑节点
function editNode(nodeId){
	if(!nodeId){
		return;
	}
	$('.editing-node').removeClass('editing-node');
	$('.nodeId_' + nodeId).addClass('editing-node');
	
	var editDom = "<span style='color: #EF5236;' class='edit-ing'>&nbsp;编辑中...</span>";
	var nodeDom = $('.node_parent .nodeId_' + nodeId);
	var node = pageVal.nodesMap[nodeId];
	if(!node || $('.edit-ing', nodeDom).length > 0){//已经在编辑中
		return;
	}else if($('.node_parent .edit-ing').length > 0){//其它节点正在编辑中
		pageVal.closeFormEvent = editNode2;
		closeForm(true);
	}else{//没有任何编辑节点
		editNode2();
	}
	
	function editNode2(){
		$('.nodeName', nodeDom).after(editDom);
		
		initNodeDetailForm(nodeId);
		
		$('#panelDetail').show();
	}
}

//设置node编辑表单的值
function initNodeDetailForm(nodeId) {
	resetNodeForm();
	
	initDetailFormBaseInfo(nodeId);
	initDetailFormExecutor(nodeId);
	initDetailFormNodeRoute(nodeId);
	
	$('#panelDetail').show();
}

//初始化明细表单的基本信息
function initDetailFormBaseInfo(nodeId) {
	var node = pageVal.getNode(nodeId);
	
	//设置基本数据
	var form = $('#processNode_form');
	var formObj = form[0];
	formObj.nodeId.value = node.id || "";
	formObj.nodeName1.value = node.nodeName || "";
	formObj.transId.value = node.transId || "";
	formObj.stage.value = node.stage || "";
	if(nodeId < 0) {
		$('#el_sendMail')[0].checked = true;
		$('#el_sendSms')[0].checked = true;
	} else {
		$('#el_sendMail')[0].checked = 1 == node.sendMail;
		$('#el_sendSms')[0].checked = 1 == node.sendSms;		
	}
}

//初始化明细表单的执行人
function initDetailFormExecutor(nodeId) {
	var node = pageVal.getNode(nodeId);
	var form = $('#processNode_form');
	var formObj = form[0];
	
	//设置执行人数据
	var userId = node.userId || "";
	var userName = getUserNames(node);
	formObj.userId.value = userId;
	formObj.userId_name.value = userName;
	
	function setOrgRoleValue(orgMode, orgData, orgNamesObj, roleData, orgTypeData) {//生成角色行dom
		var org = [orgData];
		var orgName = [];
		if(orgNamesObj) {
			orgName.push(getObjName(orgData, orgNamesObj)); 
		} else {
			orgName.push(orgData);
		}
		if(orgTypeData) {
			org.push(orgTypeData);
			orgName.push(getObjName(orgTypeData, pageVal.objNames.orgTypes));
		}
		
		var c = $('#c_orgrole .orgRoleGroup:last');
		$('input[name="orgData"]', c).val(orgMode + ';' + org.join(','));
		$('input[name="orgName"]', c).val(orgName.join(','));
		
		$('input[name="roleData"]', c).val(roleData);
		$('input[name="roleName"]', c).val(getObjName(roleData, pageVal.objNames.roles));
	}
	
	var firstRowUsed = false;
	function initNodeFormOrgRole(orgMode, orgRoleValue, objNames) {//计算不同类型的角色数据
		if(!orgRoleValue) {
			return;
		}
		
		var orgRoles = orgRoleValue.split(';');
		for(var i = 0; i < orgRoles.length; i++) {
			if(firstRowUsed) {
				addOrgRoleGroup();
			}
			
			var orArray = orgRoles[i].split('|');
			var orgData = orArray[0];
			var roleData = orArray[1];
			var orgTypeData = orArray[2];
			setOrgRoleValue(orgMode, orgData, objNames, roleData, orgTypeData);
			
			firstRowUsed = true;
		}
	}
	initNodeFormOrgRole('1', node.orgRole, pageVal.objNames.orgs);
	initNodeFormOrgRole('2', node.orgGradeRole, pageVal.objNames.orggs);
	initNodeFormOrgRole('3', node.orgLevelRole);
}

//初始化明细表单的出口路径
function initDetailFormNodeRoute(nodeId) {
	//设置节点出口路径
	var table = $('.route-table');
	var trs = $('tr', table);
	for(var i = 1; i < trs.length; i++) {
		$(trs[i]).remove();
	}
	
	var nodeRoutes = pageVal.nodeRoutesMap[nodeId];
	$.each(nodeRoutes, function(i, route) {
		addNodeRouteRow();	
		var lastTr = $('tr:last', table);
		$('.route-info', lastTr).empty();
		
		lastTr.data('routeId', route.id);
		$('.route-seq', lastTr).html(i + 1);
		$('.route-name', lastTr).html(route.name);
		$('.route-code', lastTr).html(route.code);
		$('.route-destnode', lastTr).html(route.toNode.nodeName);
		$('.route-condition', lastTr).html(route.expr);
	});
}

//添加节点出口路径行
function addNodeRouteRow() {
	var table = $('.route-table');
	var lastTr = $('tr:last', table);
	var newTr = $('.noderoute-tmpl table tr').clone();
	$('.route-info', newTr).empty();
	
	var lastSeq = $('.route-seq:last', table).text() || 1;
	lastSeq++;
	$('.route-seq', newTr).text(lastSeq);
	
	lastTr.after(newTr);
}

//取值名称
function getObjName(value, namesObj) {
	var rs = [];
	if(value) {
		var values = value.split(';');
		
		$.each(values, function(i, v) {
			var name = namesObj[v];
			if(name) {
				rs.push(name);
			}
		});
	}
	
	return rs.join(',');
}

//重置节点编辑表单
function resetNodeForm() {
	var form = $('#processNode_form');
	var formObj = form[0];
	formObj.reset();
	
	$("input[type='checked']", form).attr('checked', false);
	$("input[type='text']", form).val('');
	$("input[type='hidden']", form).val('');
	$("input[type='number']", form).val('');
	
	var roleC = $('#c_orgrole');
	var children = roleC.children();
	if(children.length > 1) {
		for(var i = 1; i < children.length; i++) {
			var child = children[i];
			$(child).remove();
		}
	}
}

//结束编辑节点
function endEditNode() {
	var form = $('#processNode_form');
	
	var formData = form.serializeArray();
	var nodeData = formArrayToMap(formData);
	if(!nodeData.nodeId){
		layer.msg('节点不存在');
		window.location.reload();
		return;
	}
	
	var node = {
		id: nodeData.nodeId,
		nodeName: nodeData.nodeName1,
		transId: nodeData.transId,
		stage: nodeData.stage,
		userId: nodeData.userId ? nodeData.userId : null
	};
	
	node.sendMail = $('#el_sendMail')[0].checked ? 1 : 0;
	node.sendSms = $('#el_sendSms')[0].checked ? 1 : 0;
	
	var orgRole = [];
	var orgLevelRole = [];
	var orgGradeRole = [];
	
	var c = $('#c_orgrole').children();
	c.each(function() {
		var dom = $(this);
		var orgData = $('input[name="orgData"]', dom).val();
		var roleData = $('input[name="roleData"]', dom).val();
		
		if(orgData && roleData) {
			var a = orgData.split(';');
			var orgMode = a[0];
			var data = a[1].split(',');
			
			if(orgMode == '1') {
				var v = [data[0], roleData];
				orgRole.push(v.join('|'));
			} else if(orgMode == '2') {
				var v = [data[0], roleData];
				if(data.length == 2) {
					v.push(data[1]);
				}
				orgGradeRole.push(v.join('|'));
			} else if(orgMode == '3') {
				var v = [data[0], roleData];
				if(data.length == 2) {
					v.push(data[1]);
				}
				orgLevelRole.push(v.join('|'));
			}
		}
	});
	node.orgRole = orgRole.join(';');
	node.orgLevelRole = orgLevelRole.join(';');
	node.orgGradeRole = orgGradeRole.join(';');
	
	$.extend(pageVal.nodesMap[node.id], node);
	
	refreshNodeDom(node.id);
}

/**
 * 刷新左边节点的dom渲染
 * @param nodeId
 */
function refreshNodeDom(nodeId) {
	var node = pageVal.getNode(nodeId);
	
	var nodeDom = $('.node_parent .nodeId_' + node.id);
	$('.node-executor', nodeDom).empty();
	$('.nodeName', nodeDom).text(node.nodeName || null);
	
	//增加操作人
	var optNames = [];
	if(node.userId){
		optNames.push('【指定人员：' + getUserNames(node) + '】');
	}
	if(node.orgRole){
		optNames.push('【指定单位角色：' + getOrgRoleNames(node) + '】');
	}
	if(node.orgLevelRole){
		optNames.push('【相对当前单位层级角色：' + getOrgLevelRoleNames(node) + '】');
	}
	if(node.orgGradeRole){
		optNames.push('【指定单位层级角色：' + getOrgGradeRoleNames(node) + '】');
	}
	
	if(optNames.length > 0) {
		for(var i = 0; i < optNames.length; i++) {
			var dom = $('.executor-tmpl .row').clone();
			$('.node-executor-text', dom).append(optNames[i]);
			
			$('.node-executor', nodeDom).append(dom);
		}
	} else {
		$('.node-executor', nodeDom).append('未设置');
	}
}

//取节点直接指定人员id对应的姓名
function getUserNames(node) {
	return getObjName(node.userId, pageVal.objNames.users);
}

//取节点指定组织与角色
function getOrgRoleNames(node) {
	return getNodeRoleNames(node.orgRole, pageVal.objNames.orgs);
}

//取节点指定相对组织层级与角色
function getOrgLevelRoleNames(node) {
	return getNodeRoleNames(node.orgLevelRole);
}

//取节点指定组织层面与角色
function getOrgGradeRoleNames(node) {
	return getNodeRoleNames(node.orgGradeRole, pageVal.objNames.orggs);
}

//计算节点的执行角色的显示名称
function getNodeRoleNames(value, orgNamesObj) {
	var rs = [];
	if(value) {
		var orgRole = value.split(';');
		
		$.each(orgRole, function(i, or) {
			var one = [];
			
			or = or.split('|');
			var val = or[0];
			var roleIds = or[1].split(',');
			var orgType = or[2];
			
			if(orgNamesObj) {
				var name = orgNamesObj[val];
				if(name) {
					one.push(name);
				}
			} else {
				one.push(val);
			}
			
			if(orgType) {
				var orgTypeName = pageVal.objNames.orgTypes[orgType];
				if(orgTypeName) {
					one.push(orgTypeName);
				}
			}
			
			var roleNames = [];
			$.each(roleIds, function(i, rId) {
				var roleName = pageVal.objNames.roles[rId];
				if(roleName) {
					roleNames.push(roleName);
				}
			});
			one.push(roleNames.join(','));
			
			rs.push(one.join("/"));
		});
	}
	
	return rs.join(';');
}

//关掉右边form
function closeForm(calc){
	if(!calc){
		pageVal.closeFormEvent = null;
		$('#processNode_form').resetForm();
		$('#panelDetail').hide();
		$('.node_parent .edit-ing').remove();
		return true;
	}
	if('none' != $('#panelDetail').css('display')){
		$('#processNode_form').submit();
	}else{
		if(pageVal.closeFormEvent){
			pageVal.closeFormEvent();
			pageVal.closeFormEvent = null;
		}
	}
}

//初始化第二个form
function initNodeFormValidator() {
	$("#processNode_form").validate(
	{
		debug:true,
		ignore:'.ignore',
		rules : {
			nodeName1 : {
				required : true,
				minlength : 1,
				maxlength : 30,
				checkUniqNodeCode:''
			},
			transId : {
				required : true,
				maxlength : 50
			},
			stage:{
				maxlength : 4
			}
		},
		submitHandler : function(form) {
			endEditNode();
			//dom关闭
			$('#panelDetail').hide();
			$('.node_parent .edit-ing').remove();
			if(pageVal.closeFormEvent){
				pageVal.closeFormEvent();
				pageVal.closeFormEvent = null;
			}
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

//初始化出口路径form的校验
function initRouteFormValidator() {
	$(".nodeRouteDlg form").validate(
	{
		debug:true,
		ignore:'.ignore',
		rules : {
			nodeName1 : {
				required : true
			},
			name : {
				required : true,
				maxlength : 30
			},
			code:{
				required : true,
				maxlength : 30
			},
			expr:{
				maxlength : 200
			}
		},
		submitHandler : function(form) {
			endEditRoute(form);
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

//数组转Map
function formArrayToMap(array){
	var map = {};
	$.each(array, function(i, ar){
		var val = map[ar.name];
		if(val) {
			var v = ar.value || null;
			if(v) {
				if($.type(val) == 'array') {
					val.push(v);
				} else {
					map[ar.name] = [val, v];
				}
			}
		} else {
			map[ar.name] = ar.value || null;
		}
	});
	return map;
}

//保存、发布
function saveAndDeploy(type){
	pageVal.type = type;
	pageVal.closeFormEvent = function(){
		$('#processDef_form').submit();
	};
	closeForm(true);
}

/**
 * 获取所有数据
 */
function getProcessDefData(defForm){
	var defFromData = $(defForm).serializeArray();
	var def = formArrayToMap(defFromData);
	def.autoMerge = "on" == def.autoMerge;
	
	var data = pageVal.data || {};
	var nodeRoute = [];
	var nodesMap = pageVal.nodesMap;
	var nodes = $(".node_parent .flow-node");
	
	var preNodeId = null;
	$.each(nodes, function(i, nodeDom){
		nodeDom = $(nodeDom);
		var nodeType = i == 0 ? 'START' : "TASK";
		var nodeId = nodeDom.data("nodeId");
		nodesMap[nodeId].nodeType = nodeType;
//		if(preNodeId != null){
//			var name = "提交至" + nodesMap[nodeId].nodeName;
//			var route = {
//				fromNodeId: preNodeId,
//				toNodeId: nodeId,
//				code: preNodeId + '_to_' + nodeId,
//				name: name
//			};
//			nodeRoute.push(route);
//		}
		preNodeId = nodeId;
	});
	
	//添加结束节点
	if(preNodeId != null){
		if(!pageVal.endNodeId){
			pageVal.endNodeId = pageVal.getId();
			var endNode = {
				id: pageVal.endNodeId,
				nodeName: '结束',
				nodeType: 'END',
				stage: 99
			};
			nodesMap[pageVal.endNodeId] = endNode;
		}
		
//		var route = {
//			fromNodeId: preNodeId,
//			toNodeId: pageVal.endNodeId,
//			code: preNodeId + '_to_End',
//			name: "结束"
//		};
//		nodeRoute.push(route);
	}
	
	var allNodes = [];
	$.each(nodesMap, function(nodeId, node){
		allNodes.push(node);
	});
	var allRoutes = [];
	$.each(pageVal.nodeRoutesMap, function(nodeId, routes){
		allRoutes = allRoutes.concat(routes || []);
	});
	
	$.extend(data, def);
	$.extend(data, {
		nodes: allNodes,
		//nodeRoutes: nodeRoute
		nodeRoutes: allRoutes
	});
	
	data.updateDate = null;
	
	return data;
}


//****************执行人选择********************************//

/**
 * 1.用户多选
 */
function chooseUsers(){
	var nodeId = $('#processNode_form [name=nodeId]').val();
	var node = pageVal.nodesMap[nodeId];
	if(!node){
		layer('节点信息不存在');
		return;
	}
	var objNames = pageVal.objNames;
	var selected = [];
	if(node.userId){
		var userIds = node.userId.split(',');
		$.each(userIds,function(i,userId){
			var name = objNames.users[userId];
			if(name){
				selected.push({
					id:userId,
					fullname:name
				});
			}
		});
	}
	
	TR.select('users',{
		selected:selected
	},function(data){
		var ids = [];
		var names = [];
		if(data){
			$.each(data,function(i,user){
				pageVal.objNames.users[user.id] = user.fullname;
				ids.push(user.id);
				names.push(user.fullname);
			});
		}
		$('#processNode_form [name=userId]').val(ids.join(','));
		$('#processNode_form [name=userId_name]').val(names.join(','));
	});
}

//选择【单位&角色】中的单位
function chooseRole_OrgDlg(dom){
	beforeChooseRole_OrgDlg(dom);
	layer.open({
		type: 1,
		zIndex: 901,
		title: '选择角色所属单位',
		area: ['680px', '350px'],
		btn: ['确认','取消'],
		content: $('#hideModel .orgRole_orgdlg'),
		yes:function(index){
			var nodeId = pageVal.getEditingNodeId();
			if(!nodeId) {
				return;
			}
			
			var data = [];
			var name = [];
			var form = $('.orgRole_orgdlg form');
			var formObj = form[0];
			var orgMode = $("input[type='radio']:checked", form).val();
			if(orgMode == '1') {
				var orgId = formObj.orgId.value;
				var orgName = formObj.orgName.value;
				if(!orgId) {
					alert('请选择组织单位。');
					return;
				}
				data.push(orgId);
				name.push(orgName);
				pageVal.objNames.orgs[orgId] = orgName;
			} else if(orgMode == '2') {
				var orgGrade = formObj.orgGrade.value;
				var orgGradeName = formObj.orgGradeName.value;
				var orgType1 = formObj.orgType1.value;
				var orgTypeName1 = formObj.orgTypeName1.value;
				if(!orgGrade) {
					alert('请选择组织层级。');
					return;
				}
				data.push(orgGrade);
				name.push(orgGradeName);
				if(orgType1) {
					data.push(orgType1);
					name.push(orgTypeName1);
					pageVal.objNames.orgTypes[orgType1] = orgTypeName1;
				}
				pageVal.objNames.orggs[orgGrade] = orgGradeName;
			} else if(orgMode == '3') {
				var orgLevel = formObj.orgLevel.value;
				var orgLevelName = orgLevel;
				var orgType2 = formObj.orgType2.value;
				var orgTypeName2 = formObj.orgTypeName2.value;
				if(!orgLevel) {
					alert('请输入相对组织层级数。');
					return;
				}
				data.push(orgLevel);
				name.push(orgLevelName);
				if(orgType2) {
					data.push(orgType2);
					name.push(orgTypeName2);
					pageVal.objNames.orgTypes[orgType2] = orgTypeName2;
				}
			}
			
			var rs = [orgMode, data.join(',')].join(';');
			
			var c = $(dom).parent();
			$('input[name="orgData"]', c).val(rs);
			$('input[name="orgName"]', c).val(name.join(','));
			
			layer.close(index);
		}
	});
}

function beforeChooseRole_OrgDlg(dom){
	var form = $('.orgRole_orgdlg form');
	var formObj = form[0];
	$("input[type='radio']", form).each(function() {
		this.checked = false;
	});
	$("input[type='text']", form).val('');
	$("input[type='hidden']", form).val('');
	$("input[type='number']", form).val('');
	
	var c = $(dom).parent();
	var orgData = $('input[name="orgData"]', c).val();
	if(!orgData) {
		return;
	}
	
	var a = orgData.split(';');
	var orgMode = a[0];
	var data = a[1].split(',');
	
	var name = '';
	if(orgMode == '1') {
		$("input[type='radio'][value='1']", form)[0].checked = true;
		formObj.orgId.value = data[0];
		name = getObjName(data[0], pageVal.objNames.orgs);
		formObj.orgName.value = name;
	} else if(orgMode == '2') {
		$("input[type='radio'][value='2']", form)[0].checked = true;
		formObj.orgGrade.value = data[0];
		name = getObjName(data[0], pageVal.objNames.orggs);
		formObj.orgGradeName.value = name;
		
		if(data.length == 2) {
			formObj.orgType1.value = data[1];
			name = getObjName(data[1], pageVal.objNames.orgTypes);
			formObj.orgTypeName1.value = name;
		}
	} else if(orgMode == '3') {
		$("input[type='radio'][value='3']", form)[0].checked = true;
		formObj.orgLevel.value = data[0];
		
		if(data.length == 2) {
			formObj.orgType2.value = data[1];
			name = getObjName(data[1], pageVal.objNames.orgTypes);
			formObj.orgTypeName2.value = name;
		}
	}
}

//选择【单位&角色】中的单位
function chooseOrg(idSelector, nameSelector){
	TR.select('dropdownOrg',{
        dom: $(nameSelector)[0]
    },function(result){
    	var id = null;
		var name = null;
		if(result){
			id = result.id;
			name = result.name;
			pageVal.objNames.orgs[id] = name;
		}
		
		$(idSelector).val(id);
		$(nameSelector).val(name);
    });
}

//选择【单位&角色】中的组织层面
function chooseOrgGrade(idSelector, nameSelector){
	TR.select('dropdownOrgGrade',{
		dom: $(nameSelector)[0]
	},function(data){
		$(idSelector).val(data.code);
		$(nameSelector).val(data.name);
	});
}

//选择【单位&角色】中的组织层面
function chooseOrgType(idSelector, nameSelector){
	TR.select(orgTypes, {
        key:'orgTypeSelector',
        dom: $(nameSelector)[0],
        name: 'name',
        allowBlank: false
    },function(result){
        $(idSelector).val(result.code);
		$(nameSelector).val(result.name);
    });
}

//选择角色
function chooseRoles(dom){
	var c = $(dom).parent();
	var roleDataDom = c.find("input[name='roleData']");
	var roleNameDom = c.find("input[name='roleName']");
	var roleData = roleDataDom.val();
	var roles = [];
	if(roleData){
		var roleIds = roleData.split(',');
		$.each(roleIds, function(i, rid){
			var name = pageVal.objNames.roles[rid];
			if(name){
				roles.push({
					id: rid,
					name: name
				});
			}
		});
	}
	TR.select('roles',{
		selected:roles,
//		partnerId:1,//可选项，不传使用当前用户所使用的partner
//		sysId:'PLATFORM',//可选项，不传使用当前用户所使用的partner
	},function(result){
		var ids = [];
		var names = [];
		if(result){
			$.each(result, function(i,r){
				ids.push(r.id);
				names.push(r.name);
				pageVal.objNames.roles[r.id] = r.name;
			});
		}
		roleDataDom.val(ids.join(','));
		roleNameDom.val(names.join(','));
	});
}

//添加一组
function addOrgRoleGroup(){
	var c = $('#c_orgrole');
	var dom = $('.orgRole_group .orgRoleGroup');
	dom = dom.clone();
	c.append(dom);
}

//删除一组
function deleteOrgRoleGroup(dom){
	var c = $('#c_orgrole');
	var orgDataDoms = $('input[name="orgData"]', c);
	if(orgDataDoms.length <= 1) {
		$('input[name="orgData"]', c).val('');
		$('input[name="orgName"]', c).val('');
		$('input[name="roleData"]', c).val('');
		$('input[name="roleName"]', c).val('');
	} else {
		$(dom).parentsUntil('#c_orgrole', '.orgRoleGroup').remove();
	}
}

//结束编辑路径
function endEditRoute(formObj) {
	var form = $(formObj);
	formObj = form[0];
	
	var routeId = formObj.routeId.value;
	var nodeId = pageVal.getEditingNodeId();
	
	var route = null;
	var isNew = false;
	if(!routeId) {
		routeId = pageVal.getId();
		route = {};
		isNew = true;
	} else {
		route = pageVal.getRoute(routeId);
	}
	
	route.name = formObj.name.value;
	route.code = formObj.code.value;
	route.fromNodeId = nodeId;
	route.toNodeId = formObj.nodeId.value;
	route.toNode = {id: formObj.nodeId.value, nodeName: formObj.nodeName1.value};
	route.expr = formObj.expr.value;
	
	if(isNew) {
		var nodeRoutesMap = pageVal.nodeRoutesMap;
		var routes = nodeRoutesMap[nodeId] = nodeRoutesMap[nodeId] || [];
		routes.push(route);
	}
	
	initDetailFormNodeRoute(nodeId);
	
	layer.close(pageVal.routeDlgIndex);
}

//编辑出口路径行
function editNodeRoute(dom) {
	var routeId = null;
	
	if(dom) {
		var tr = $(dom).parentsUntil('.route-table', 'tr');
		routeId = tr.data('routeId');
	}
	beforeEditNodeRoute(dom);
	
	var index = layer.open({
		type: 1,
		zIndex: 910,
		title: routeId ? '编辑路径' : '新增路径',
		area: ['500px', '350px'],
		btn: ['确认','取消'],
		content: $('#hideModel .nodeRouteDlg'),
		yes:function(index){
			var form = $('.nodeRouteDlg form');
			form.submit();
		}
	});
	
	pageVal.routeDlgIndex = index;
}

//编辑路径前设置值
function beforeEditNodeRoute(dom){
	var form = $('.nodeRouteDlg form');
	var formObj = form[0];
	$("input", form).val('');
	
	if(!dom) {
		return;
	}
	
	var tr = $(dom).parentsUntil('.route-table', 'tr');
	var routeId = tr.data('routeId');
	var route = pageVal.getRoute(routeId);
	
	if(!route) {
		return;
	}
	
	formObj.routeId.value = routeId || '';
	formObj.name.value = route.name || '';
	formObj.code.value = route.code || '';
	formObj.nodeId.value = route.toNode.id || '';
	formObj.nodeName1.value = route.toNode.nodeName || '';
	formObj.expr.value = route.expr || '';
}

//删除出口路径行
function deleteNodeRoute(dom) {
	layer.confirm('确认删除此路径?', {icon: 3, title:'提示'}, function(index){
		var tr = $(dom).parentsUntil('.route-table', 'tr');
		var routeId = tr.data('routeId');
		var nodeId = pageVal.getEditingNodeId();
		var nodeRoutes = pageVal.nodeRoutesMap[nodeId] || [];
		$.each(nodeRoutes, function(i, route) {
			if(route.id == routeId) {
				nodeRoutes.splice(i, 1);
				return false;
			}
		});
		
		tr.remove();
		layer.close(index);
	});
}

//选择出口路径的目标节点
function chooseNode(dom) {
	var editingNodeId = pageVal.getEditingNodeId();
	
	var nodesMap = pageVal.nodesMap;
	var nodes = [];
	$.each(nodesMap, function(id, node) {
		if(node.id != editingNodeId && node.nodeType != 'START') {
			nodes.push(node);
		};
	});
	
	nodes.sort(function(n1, n2){
		var s1 = n1.seq || 0, s2 = n2.seq || 0;
		return s1 == s2 ? 0 : (s1 > s2 ? 1 : -1);
	});
	
	var data = [];
	$.each(nodes, function(i, node) {
		data.push({code: node.id, name: node.nodeName});
	});
	
	TR.select(data, {
        key:'chooseNode',
        dom:dom,
        allowBlank: false
    }, function(result){
        $('.nodeRouteDlg input[name="nodeId"]').val(result.code);
        $('.nodeRouteDlg input[name="nodeName1"]').val(result.name);
    });
}
