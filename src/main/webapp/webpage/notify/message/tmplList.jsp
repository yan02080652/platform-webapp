<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>消息模板列表&nbsp;&nbsp; 共计<span>${pageList.totalPage }页,${pageList.total }</span>条数据   &nbsp;&nbsp;
			</div>
			<div class="col-md-8">
				<form class="form-inline" id="queryform">
					<nav class="text-right">
						<div class=" form-group">
							<input name="code" type="text" value="${code }" class="form-control" placeholder="模板编码" />
							<input name="title" type="text" value="${title }" class="form-control" placeholder="模板标题" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="queryTmpl()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
	
	<hr/>
		<div class="col-sm-12" style="margin-bottom: 10px;">
				<div class="pull-left">
					<a class="btn btn-default" href="notify/message/tmpl/detail" role="button">添加</a>
				</div>
			</div>
		
		<div class="col-sm-12">
			<table id="contentTable"
				class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th class="sort-column" style="min-width: 180px;">模板编码</th>
						<th class="sort-column" >模板名称</th>
						<th class="sort-column" style="max-width: 300px;">描述</th>
						<th style="min-width: 120px">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="tmpl">
						<tr>
							<td>${tmpl.code}</td>
							<td>${tmpl.name}</td>
							<td style="max-width: 310px;">${tmpl.descr}</td>
							<td style="width: 170px;">
								<a class="btn btn-default" href="notify/message/tmpl/detail?id=${tmpl.id }" role="button">修改</a>	
								<a class="btn btn-default"  role="button" onclick="deleteTmpl('${tmpl.id}')">删除</a>	
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryTmpl"></jsp:include>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" >
function deleteTmpl(id) {
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除该模板?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'notify/message/tmpl/delete/'+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						showSuccessMsg(msg.txt);
						window.location.href="/notify/message/tmpl";
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

function queryTmpl(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	var code = $("input[name='code']").val();
	var title = $("input[name='title']").val();
 	/* $.ajax({
        url: 'notify/message/tmpl/query',
        type: 'post',
       // crossDomain: true,
        data: {
        	"pageIndex" : pageIndex,
        	 code:code,
        	 title:title
        },
        success: function (data) {
        }

    }); */
 	$.post("notify/message/tmpl",{
    		"pageIndex" : pageIndex,
   	 		code:code,
   	 		title:title
   	},function(result){
   		$("body").html(result);
 	});
	
	//window.location.href="notify/message/tmpl?pageIndex="+pageIndex+"&code="+code+"&title="+title;
}
function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}


</script>