<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<script type="text/javascript" charset="utf-8" src="${baseRes}/plugins/ueditor/ueditor.config.js?version=${globalVersion}"></script>
<script type="text/javascript" charset="utf-8" src="${baseRes}/plugins/ueditor/ueditor.all.min.js?version=${globalVersion}"> </script>
<script type="text/javascript" charset="utf-8" src="${baseRes}/plugins/ueditor/lang/zh-cn/zh-cn.js?version=${globalVersion}"></script>

<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业客户信息<c:choose><c:when test="${messageTmpl.id != null && messageTmpl.id != ''}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
             	 &nbsp;&nbsp;<a data-pjax href="notify/message/tmpl">返回</a>	
             </div>
             <!-- /.panel-heading -->
             <div class="panel-body">
                 <form id="messageTmpl_form" class="form-horizontal" method="post">
					  <input name="id" id="messageTmplId" type="hidden" value="${messageTmpl.id}"/>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">模板编码</label>
					    <div class="col-sm-6">
					      <input type="text" name="code" class="form-control" value="${messageTmpl.code }" placeholder="编码">
					      <span class="glyphicon glyphicon-warning-sign form-control-feedback hidden" data-container="body" data-toggle="popover" data-trigger="click"></span>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">模板名称</label>
					    <div class="col-sm-6">
					      <input type="text" name="name" class="form-control" value="${messageTmpl.name }" placeholder="模板名称">
					      <span class="glyphicon glyphicon-warning-sign form-control-feedback hidden" data-container="body" data-toggle="popover" data-trigger="click"></span>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">内容主题</label>
					    <div class="col-sm-6">
					      <input type="text" name="title" class="form-control" value="${messageTmpl.title }" placeholder="内容主题">
					      <span class="glyphicon glyphicon-warning-sign form-control-feedback hidden" data-container="body" data-toggle="popover" data-trigger="click"></span>
					    </div>
					  </div>
					   <div class="form-group">
					    <label class="col-sm-2 control-label">供应商</label>
					    <div class="col-sm-6">
					    	<select class="form-control" id="supplySelect" name="supply">
					    		 <option value="">无</opton>
						      	 <option value="CLOUDFENG">云锋</opton>
						      	 <option value="RLY">容联运</opton>
						      	 <option value="DAHANTC">大汉三通</opton>
							</select>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">顺序</label>
					    <div class="col-sm-6">
					    	<input type="number" name="seq" class="form-control" value="${messageTmpl.seq }" placeholder="顺序">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">内容输入框选择</label>
					    <div class="col-sm-6">
					    	<select class="form-control" name="inputType" id="inputTypeSelect" onchange="changeInputSelect(this)">
						      	 <option value="1">普通文本框</opton>
						      	 <option value="2">富文本框</opton>
							</select>
					    </div>
					  </div>
					  <div class="form-group" id="editorDiv" style="display: none;">
					    <label class="col-sm-2 control-label">内容</label>
					    <div class="col-sm-6">
					  		<script id="editor" type="text/plain" style="width:690px;height:300px;"></script>
					  	</div>
					  </div>
					  <div class="form-group" id="inputDiv" >
					    <label class="col-sm-2 control-label">内容</label>
					    <div class="col-sm-6">
					    	<textarea class="form-control" name="content" rows="4" >${messageTmpl.content} </textarea>
					  	</div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">示例</label>
					    <div class="col-sm-6">
					    	<textarea class="form-control" name="descr" rows="4" >${messageTmpl.descr} </textarea>
					    </div>
					  </div>
					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-6">
					        <button type="button" class="btn btn-default" onclick="submitMessageTmpl()">提交</button>
					    </div>
					  </div>
					</form>
             </div>
             <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
	</div>
</div>

 <script type="text/javascript">
 
 var ue = UE.getEditor('editor',{  
     //这里可以选择自己需要的工具按钮名称,此处仅选择如下五个  
     toolbars:[[
                'fullscreen', 'source', '|', 'undo', 'redo', '|',
                'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
                'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                'directionalityltr', 'directionalityrtl', 'indent', '|',
                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
                'link', 'unlink', 'anchor', '|', 'imageleft', 'imageright', 'imagecenter', '|',
                'insertframe', 'insertcode', 'pagebreak', '|',
                'horizontal', 'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', '|',
                'preview', 'searchreplace'
            ]]
 });
 
UE.getEditor('editor').ready(function() {
	UE.getEditor('editor').setContent('${messageTmpl.content}');
});
 
 function submitMessageTmpl(){
 	$("#messageTmpl_form").submit();
 }
 
 function changeInputSelect(dom){
	 var selectValue = $(dom).val();
	 if(selectValue == 1){
		 $('#editorDiv').hide();
		 $('#inputDiv').show();
		 $("input[name='content']").val('');
	 }else if (selectValue == 2){
		 $('#inputDiv').hide();
		 $('#editorDiv').show();
		 $("input[name='content']").val('');
	 }
 }

 
$(document).ready(function() {
	
	if($("#messageTmplId").val()){//修改时
		$("#supplySelect").val('${messageTmpl.supply}');
		$("#inputTypeSelect").val('${messageTmpl.inputType}');
		if('${messageTmpl.inputType}' == 1){
			 $('#editorDiv').hide();
			 $('#inputDiv').show();
		}else if('${messageTmpl.inputType}' == 2){
			 $('#inputDiv').hide();
			 $('#editorDiv').show();
		}
	}
	$("#messageTmpl_form").validate({
		rules : {
			name :{
				required : true,
				maxlength : 80
			},
			code :{
				required : true/* ,
				remote : {
					url : "notify/message/tmpl/checkCode",
					type : "post",
					data : {
						id : function() {
							return $("#messageTmplId").val();
						}
					},
					dataFilter : function(data) {
						var resultMes = eval("("+data+")");
						if (resultMes.type == 'success') {
							return true;
						} else {
							return false;
						}
					}
				}*/
			} 
		},
		/* messages : {
			code : {
				remote : "编码已存在",
			}
		}, */
		submitHandler : function(form) {
			if($('#inputTypeSelect').val()== 2){
				$("input[name='content']").val(UE.getEditor('editor').getContent());
			}
			$(form).ajaxSubmit({
				url:'notify/message/tmpl/saveOrUpdate',
				success:function(data){
					if (data.type == "success") {
						window.location.href="/notify/message/tmpl";
						showSuccessMsg(data.txt);
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
});
</script> 