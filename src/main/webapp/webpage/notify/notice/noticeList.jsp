<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>公告/头条列表&nbsp;&nbsp; 共计<span>${pageList.totalPage }页,${pageList.total }</span>条数据   &nbsp;&nbsp;
			</div>
<!-- 			<div class="col-md-8"> -->
<!-- 				<form class="form-inline" id="queryform"> -->
<!-- 					<nav class="text-right"> -->
<!-- 						<div class=" form-group"> -->
<%-- 							<input name="title" type="text" value="${title }" class="form-control" placeholder="标题" /> --%>
<!-- 						</div> -->
<!-- 						<div class="form-group"> -->
<!-- 							<button type="button" class="btn btn-default" -->
<!-- 								onclick="queryNotice()">查询</button> -->
<!-- 							<button type="button" class="btn btn-default" -->
<!-- 								onclick="resetForm()">重置</button> -->
<!-- 						</div> -->
<!-- 					</nav> -->
<!-- 				</form> -->
<!-- 			</div> -->
		</div>
	
	<hr/>
		<div class="col-sm-12" style="margin-bottom: 10px;">
				<div class="pull-left">
					<a class="btn btn-default" href="notify/notice/detail" role="button">添加</a>
				</div>
			</div>
		
		<div class="col-sm-12">
			<table id="contentTable"
				class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th class="sort-column" style="min-width: 100px;">类型</th>
						<th class="sort-column" style="min-width: 100px;">状态</th>
						<th class="sort-column" style="min-width: 320px;">标题</th>
						<th style="min-width: 120px">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="notice">
						<tr>
							<td><c:if test="${notice.msgBizType== 'SYS_NOTICE'}">公告</c:if>
								<c:if test="${notice.msgBizType== 'TEM_TOP'}">头条</c:if>
							</td>
							<td><c:if test="${notice.completeStatus== '0'}">正常使用中</c:if>
								<c:if test="${notice.completeStatus== '1'}">已结束</c:if>
							</td>
							<td>${notice.title}</td>
							<td style="width: 170px;">
								<a class="btn btn-default" href="notify/notice/detail?id=${notice.id }" role="button">修改</a>	
								<a class="btn btn-default"  role="button" onclick="deleteNotice('${notice.id}')">删除</a>	
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryNotice"></jsp:include>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" >
function deleteNotice(id) {
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除该信息?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'notify/notice/delete/'+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						showSuccessMsg(msg.txt);
						window.location.href="/notify/notice";
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

function queryNotice(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	var title = $("input[name='title']").val();
 	/* $.ajax({
        url: 'notify/message/tmpl/query',
        type: 'post',
       // crossDomain: true,
        data: {
        	"pageIndex" : pageIndex,
        	 code:code,
        	 title:title
        },
        success: function (data) {
        }

    }); */
 	$.post("notify/notice",{
    		"pageIndex" : pageIndex,
   	 		title:title
   	},function(result){
   		$("body").html(result);
 	});
	
	//window.location.href="notify/message/tmpl?pageIndex="+pageIndex+"&code="+code+"&title="+title;
}
function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}


</script>