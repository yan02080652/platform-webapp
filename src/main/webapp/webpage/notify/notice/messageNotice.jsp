<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<script type="text/javascript" charset="utf-8" src="${baseRes}/plugins/squire/wangEditor.js?version=${globalVersion}"></script>

<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>公告/头条信息<c:choose><c:when test="${messageNotice.id != null && messageNotice.id != ''}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
             	 &nbsp;&nbsp;<a data-pjax href="notify/notice">返回</a>	
             </div>
             <!-- /.panel-heading -->
             <div class="panel-body">            		
                 <form id="messageNotice_form" class="form-horizontal" method="post">
					  <input name="id" id="messageNoticeId" type="hidden" value="${messageNotice.id}"/>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">类别</label>
					    <div class="col-sm-6">
					      <select class="form-control" id="cateSelect" name="msgBizType">
						      	 <option value="SYS_NOTICE">公告</opton>
						      	 <option value="TEM_TOP">头条</opton>
							</select>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">状态</label>
					    <div class="col-sm-6">
					      <select class="form-control" id="completeStatus" name="completeStatus">
						      	 <option value="0">正常使用中</opton>
						      	 <option value="1">已结束</opton>
							</select>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">标题</label>
					    <div class="col-sm-6">
					      <input type="text" id="title" name="title" class="form-control" maxlength="15" value="${messageNotice.title }" placeholder="标题" />
					      
					      <span class="glyphicon glyphicon-warning-sign form-control-feedback hidden" data-container="body" data-toggle="popover" data-trigger="click"></span>
					    </div>
					  </div>
					  
								<input id="content" name="content" style="display: none"/>
					  <div class="form-group" id="editorDiv">
					    <label class="col-sm-2 control-label">内容</label>
					    <div class="col-sm-6">
							<div id="editor"  >	
							</div>
<!-- 							<textarea id="text1" style="width:100%; height:200px;"></textarea> -->
					  	</div>
					  </div>
					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-6">
					        <button type="button" class="btn btn-default" onclick="submitMessageNotice()">提交</button>
					    </div>
					  </div>
					</form>
					<div class="form-group">
					    <label class="col-sm-2 control-label">上传图片</label>
					     <form id="picture_form" class="form-picture"  enctype="multipart/form-data" method="post">
					    <div class="col-sm-6">
					     <input type="text" name="path" id="path" style="width: 500px"/>
    					  <input type="file" name="picture" id="picture"/><button type="button" class="savePicture" onclick="toPicture()">上传</button>
					    </div>
					    </form>
					  </div>
             </div>
             <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
	</div>
</div>

 <script type="text/javascript">
 

	 var E = window.wangEditor
	 var editor = new E('#editor')
	 // 自定义菜单配置
    editor.customConfig.menus = [
        'head',  // 标题
	    'bold',  // 粗体
	    'fontSize',  // 字号
	    'fontName',  // 字体
	    'italic',  // 斜体
	    'underline',  // 下划线
	    'strikeThrough',  // 删除线
	    'foreColor',  // 文字颜色
	    'backColor',  // 背景颜色
	    'link',  // 插入链接
	    'list',  // 列表
	    'justify',  // 对齐方式
	    'quote',  // 引用
	    'emoticon',  // 表情
	    'image',  // 插入图片
	    'table',  // 表格
	    'code',  // 插入代码
	    'undo',  // 撤销
	    'redo'  // 重复
    ]
//	 editor.customConfig.codeDefaultLang = 'javascript'
// 	 var $text1 = $('#text1')
//      editor.customConfig.onchange = function (html) {
//          // 监控变化，同步更新到 textarea
//          $text1.val(html)
//      }
	 // 配置服务器端地址
//	 editor.customConfig.uploadImgServer = 'notify/notice/picture'
	 
	 editor.create()
 	 editor.txt.html('<p>${messageNotice.content }</p>')
 	 // 初始化 textarea 的值
//      $text1.val(editor.txt.html())
 
 function toPicture(){
		 $("#picture_form").validate({
				submitHandler : function(form) {			
					$("#picture_form").ajaxSubmit({
						url:'notify/notice/picture',
						success:function(data){
							if (data.type == "success") {
								$("input[name='path']").val("http://imgserver.z-trip.cn/" + data.txt);
							} else {
								showErrorMsg(data.txt);
							}
						}
					});
				},
				errorPlacement : setErrorPlacement,
				success : validateSuccess,
				highlight : setHighlight
			});
 	$("#picture_form").submit();
 }	 
 	 
 function submitMessageNotice(){
 	$("#messageNotice_form").submit();
 }
	  
 
$(document).ready(function() {
	
	

 if($("#messageNoticeId").val()){//修改时
		$("#cateSelect").val('${messageNotice.msgBizType}');
		$("#completeStatus").val('${messageNotice.completeStatus}');
		
	}
	$("#messageNotice_form").validate({
		rules : {
			name :{
				required : true,
				maxlength : 80
			},
			code :{
				required : true/* ,
				remote : {
					url : "notify/message/tmpl/checkCode",
					type : "post",
					data : {
						id : function() {
							return $("#messageTmplId").val();
						}
					},
					dataFilter : function(data) {
						var resultMes = eval("("+data+")");
						if (resultMes.type == 'success') {
							return true;
						} else {
							return false;
						}
					}
				}*/
			} 
		},
		/* messages : {
			code : {
				remote : "编码已存在",
			}
		}, */
		submitHandler : function(form) {
			$("input[name='content']").val(editor.txt.html());
			$(form).ajaxSubmit({
				url:'notify/notice/saveOrUpdate',
				success:function(data){
					if (data.type == "success") {
						window.location.href="/notify/notice";
						showSuccessMsg(data.txt);
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
});
</script> 