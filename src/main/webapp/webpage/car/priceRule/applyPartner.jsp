<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="applyPartnerListManage">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span><a href="/car/priceRule">用车价格策略组列表</a> >>
                <a href="/car/priceRule/priceRuleGroup?id=${priceRuleGroupDto.id}">${priceRuleGroupDto.name}</a> >>
                用车价格粗略组适用企业列表
            </div>
            <div id="applyPartnerList" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-inline" method="post" >
                                            <nav class="text-right">
                                                <div class=" form-group">
                                                    <label class="control-label">查询条件：</label>
                                                    <input name="cName" id="c_name" class="form-control" placeholder="企业">
                                                    <input type="hidden" id="c_id">
                                                    <input type="hidden" id="group_id" value="${priceRuleGroupDto.id}">
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default" onclick="search_rule()">查询</button>
                                                    <button type="button" class="btn btn-default" onclick="reset_rule()">重置</button>
                                                </div>
                                            </nav>
                                        </form>
                                    </div>
                                </div>
                                <div  id="applyPartnerList_Table" style="margin-top: 15px">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
    $(function(){
        reloadTable(1);

    });
    $('#c_name').click(function(){

        TR.select('partner',function(data){
            $('#c_name').val(data.name);
            $('#c_id').val(data.id);
        });
    })
    //查询
    function search_rule(){
        if($('#c_id').val().trim()==''){
            alert('请选择企业');
            return;
        }
        reloadTable(1);
    }
    //重置
    function reset_rule(){
        $("#c_id").val("");
        $("#c_name").val("");
        reloadTable(1)
    }
    function reloadTable(pageIndex){

        $.ajax({
            url:"car/priceRule/"+$("#group_id").val()+"/applyPartner/list",
            type: 'post',
            data:{
                partnerId:$('#c_id').val().trim(),
                pageIndex: pageIndex
            },success:function(data){
                $("#applyPartnerList_Table").html(data);
            }
        })

    }
</script>


