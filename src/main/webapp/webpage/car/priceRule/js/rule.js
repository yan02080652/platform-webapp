$(function(){
    reload();
})
function errorMsg(ele, msg) {
    layer.tips(msg, ele, {
        tips: [1, '#3595CC'],
        time: 4000
    });
};
//首次加载策略细则数据
function reload(){
    //查询有几个hubcode
    var hubs = $('.hid_hub');
    for(var i = 0; i< hubs.length;i++){
        reloadRuleList(1,hubs[i].value);
    }
}
//获取策略细则列表
function reloadRuleList(pageIndex,hubsCode){
    var supplierCode,status;
    supplierCode=$('#'+hubsCode+'supplier').val();
    status=$('#'+hubsCode+'status').val();
    $.ajax({
        url: '/car/priceRule/detail/list',
        type: 'post',
        async:false,
        data: {
            hubsCode:hubsCode,
            groupId:$('#rulegroupid').val(),
            supplierCode:supplierCode,
            status:status,
            pageIndex:pageIndex
        },success: function(data){
            $('#priceRuleTable_'+hubsCode).show();
            var searchdiv=$('#priceRuleTable_'+hubsCode).find('#'+hubsCode+'div');

            if(searchdiv!=undefined&&searchdiv!=null&&searchdiv.length>0){
                var div=$("<div></div>");
                div.append(data.replace(/(^\s*)|(\s*$)/g, ""));
                var table=div.find('#'+hubsCode+'div');
                searchdiv.html(table);
            }else{
                $('#priceRuleTable_'+hubsCode).append(data);
            }
        }
    })
}

//设置策略状态
function setStatus(obj,id,status){
    $.post({
        url:"car/priceRule/detail/setStatus",
        data:{id:id,status:status},
        type:"post",
        success:function(data){
            if(data.type=="success"){
                $(obj).attr("onclick","setStatus(this,"+id+","+!status+")");
                $(obj).html(status?"启用":"禁用");
            }else{
                $.alert(data.txt, "提示");
            }
        }
    })
}

//新增/修改策略获取
function getPriceRule(channelcode,id,iscopy){
    var url="car/priceRule/detail/priceRuleDetail?channelCode="+channelcode;
    if(id){
        url+="&id="+id;
    }
    if(iscopy){
        url+="&iscopy="+iscopy;
    }
    $('#detailDiv').load(url, function (context, state) {
        if ('success' == state) {
            $('#priceRuleModel').modal();
        }
    });
}
//删除策略
function deletedPriceRule(channelcode,id){
    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: "确认删除所选记录?",
        confirm: function () {
            $.post({
                url:"car/priceRule/detail/deleted",
                data:{id:id},
                type:"post",
                success:function(data){
                    if(data.type=="success"){
                        $.alert(data.txt, "提示");
                        backPriceRuleMain(channelcode);
                    }else{
                        $.alert(data.txt, "提示");
                    }
                }
            });
        },
        cancel: function () {
        }
    });

}
//返回
function backPriceRuleMain(channelcode){
    reloadRuleList(1,channelcode);
    $('#priceRuleModel').modal('hide');
}


