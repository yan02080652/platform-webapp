$(function(){
    initData();
})


function initData(){
    $(".time-picker").jeDate({
        format: "hh:mm"
    });
    $(".datetime-picker").jeDate({
        format:"YYYY-MM-DD"
    });
    if($(":checkbox[name=interventionType][value=0]").is(":checked")){
        $(".estdiv").show();
    }else{
        $(".estdiv").hide();
    }
    if($(":checkbox[name=interventionType][value=1]").is(":checked")){
        $(".settlediv").show();
    }else{
        $(".settlediv").hide();
    }
    if(getCarCityChoosedData.length>0){
        var cityNames='';
        var cityNamesDesc='';
        $.each(getCarCityChoosedData,function(index,item){
            if (index <= 2) {
                cityNamesDesc += item.name+",";
            }
            if (index == 3) {
                cityNamesDesc = cityNamesDesc.substr(0, cityNamesDesc.length - 1) + '  ...';
            }
            if(index<10) {
                cityNames += item.name + ",";
            }else if(index==11){
                cityNames=cityNames.substr(0,cityNames.length-1)+' ...';
            }
        });
        if (getCarCityChoosedData.length <=2) {
            cityNamesDesc = cityNamesDesc.substr(0, cityNamesDesc.length - 1);
        }
        if(getCarCityChoosedData.length<=10){
            cityNames=cityNames.substr(0,cityNames.length-1);
        }
        $("#cityNames").html(cityNames);
        $("#cityNamesDesc").val(cityNamesDesc);
    }else{
        $("#cityNames").val();
        $("#cityNamesDesc").val();
    }


}
$(":checkbox[name=interventionType][value=0]").change(function(){
    if($(this).is(":checked")){
        $(".estdiv").show();
    }else{
        $(".estdiv").hide();
    }
});
$(":checkbox[name=interventionType][value=1]").change(function(){
    if($(this).is(":checked")){
        $(".settlediv").show();
    }else{
        $(".settlediv").hide();
    }
});

//选择城市
function selCarCityData() {
    new choose.carCity({
        id:'selMultiCarCity',//给id可以防止一直新建对象
        multi:true,
        empty:true,
        getChoosedData:function () {
            return getCarCityChoosedData;
        }
    },function (data) {
        var cityNamesDesc = '';
        var cityNames=''
        getCarCityChoosedData = []
        if (data) {
            $(data).each(function (i, item) {
                getCarCityChoosedData.push({cityId: item.cityId, name: item.name});
                if (i <= 2) {
                    cityNamesDesc += item.name+",";
                }
                if (i == 3) {
                    cityNamesDesc = cityNamesDesc.substr(0, cityNamesDesc.length - 1) + '  ...';
                }
                if(i<10) {
                    cityNames += item.name + ",";
                }else if(i==11){
                    cityNames=cityNames.substr(0,cityNames.length-1)+' ...';
                }
            });
            if (getCarCityChoosedData.length <=3) {
                cityNamesDesc = cityNamesDesc.substr(0, cityNamesDesc.length - 1);
            }
            if(getCarCityChoosedData.length>0&&getCarCityChoosedData.length<10){
                cityNames=cityNames.substr(0,cityNames.length-1);
            }
        }
        $("#cityNamesDesc").val(cityNamesDesc);
        $("#cityNames").html(cityNames);

    });
};

//数据验证
function validateResult() {

    //干预阶段
    if ($("[name='interventionType'][value='0']").is(':checked')) {
        if ($("input[name='estPercent']").val().trim() == '') {
            alert("调整百分比不能为空");
            $("input[name='estPercent']").focus();
            return false;
        }
        if ($("input[name='estAmount']").val().trim() == '') {
            alert("调整金额不能为空");
            $("input[name='estAmount']").focus();
            return false;
        }
    }
    if ($("[name='interventionType'][value='1']").is(':checked')) {
        if ($("input[name='settlePercent']").val().trim() == '') {
            alert("调整百分比不能为空");
            $("input[name='settlePercent']").focus();
            return false;
        }
        if ($("input[name='settleAmount']").val().trim() == '') {
            alert("调整金额不能为空");
            $("input[name='settleAmount']").focus();
            return false;
        }
    }
    //生效时间
    var startdatetxt = $("#startdatetxt").val().trim();
    var enddatetxt = $("#enddatetxt").val().trim();
    if(startdatetxt != '' && enddatetxt != '' && startdatetxt >= enddatetxt){
        alert("生效结束日期必须大于开始日期");
        return false;
    }
    //优先级
    if ($("input[name='priority']").val().trim() == "") {
        alert("优先级不能为空");
        return false;
    }
    return true;
}
function submitPriceRule(){
    if(!validateResult()){
        return;
    }
    $("input[name=groupId]").val(parent.$("#rulegroupid").val());

    //设置城市
    if(getCarCityChoosedData.length>0){
        var cityNames='';
        var cityId='';
        $.each(getCarCityChoosedData,function(index,item){
            cityNames+=item.name+",";
            cityId+=item.cityId+",";
        });
        if(getCarCityChoosedData.length>0){
            cityNames=cityNames.substr(0,cityNames.length-1);
            cityId=cityId.substr(0,cityId.length-1);
        }
        $("input[name=cityId]").val(cityId);
        $("input[name=cityName]").val(cityNames);
    }
    //设置用车类型
    var txt='';
    $(":checkbox[name=carTypecheckbox]").each(function (i, item) {
        if ($(this).is(":checked")) {
            txt+=$(this).val().trim()+",";
        }
    });
    if(txt.length>0){
        txt=txt.substr(0,txt.length-1);
    }
    $("input[name=productId]").val(txt);
    //设置距离
    txt=$("input[name=distanceType]:checked").val();
    if(txt==1){
        txt="+";
    }else {
        txt="-";
    }
    if($("input[name=distancetxt]").val().trim()!='') {
        $("input[name=distance]").val($("input[name=distancetxt]").val().trim() + txt);
    }

    //设置报价
    txt=$("input[name=priceType]:checked").val();
    if(txt==1){
        txt="+";
    }else {
        txt="-";
    }
    if($("input[name=price]").val().trim()!=''){
    $("input[name=estPrice]").val($("input[name=price]").val().trim()+txt);
    $("input[name=settlePrice]").val($("input[name=price]").val().trim()+txt);
    }

    //设置供货商
    txt='';
    $(":checkbox[name=suppliercheckbox]").each(function (i, item) {
        if ($(this).is(":checked")) {
            txt+=$(this).val()+',';
        }
    });
    if(txt.length>0){
        txt=txt.substr(0,txt.length-1);
    }
    $("input[name=supplierCode]").val(txt);


    //设置报价/结算
    if($('[name=interventionType]:checked').length == 0){
        alert("请选择干预类型");
        return false;
    }


    $.ajax({
        type:"POST",
        url:"car/priceRule/detail/save",
        data:$("#priceRuleDetailForm").serialize(),
        async : false,
        error:function(request){
            alert("请求失败，请刷新重试"+request);
        },
        success:function(data){
            if (data.type == 'success') {
                //showSuccessMsg(data.txt);
                $.alert(data.txt,"提示");
                parent.backPriceRuleMain($('[name=channelCode]').val());
            } else {
                $.alert(data.txt,"提示");
            }
        }
    });
}
//正则验证
function verifyRegular(obj,type){
        switch(type){
            case "d+"://正整数
                if(!/^[1-9]\d*$/.test(obj.value)){obj.value='';}
                break;
            case "d"://自然数
                if(!/^[1-9]\d*$/.test(obj.value)){obj.value='';}
                break;
        }

}

