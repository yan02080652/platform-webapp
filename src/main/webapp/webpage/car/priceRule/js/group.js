
$(function () {
    reloadTable(1);
})

//加载策略组列表
function reloadTable(pageIndex) {
    $.ajax({
        url: '/car/priceRule/list',
        type: 'post',
        data:{
            pageIndex: pageIndex
        },success:function(data){
            $("#priceRule_Table").html(data);
        }
    })
}

//新增或编辑
function addGroup(id){
    $.ajax({
        url: '/car/priceRule/addGroup',
        type: 'post',
        data: {
            id: id
        },success: function(data){
            $('#group_detail').html(data);
            $('#group_detail').show();
            $('#mode_group').modal();
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })
}

//保存
function saveGroup(){
    $.ajax({
        url: '/car/priceRule/save',
        type: 'post',
        data: $('#detailForm').serialize(),
        success: function(data){
            if(data.code==1){
                $.alert(data.msg, "提示");
                $('#mode_group').modal('hide');
                reloadTable(1);
            }else{
                $.alert(data.msg, "提示");
            }
        },error: function(){
            $.alert("系统错误", "提示");
        }
    })
}
//删除策略组
function deletedGroup(id){
    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: "确认删除所选记录?",
        confirm: function () {
            $.ajax({
                cache: true,
                type: "POST",
                url: "car/priceRule/delete",
                data: {
                    id: id
                },
                error: function (request) {
                    showErrorMsg("请求失败，请刷新重试");
                },
                success: function (data) {
                    if (data.type == 'success') {
                        $.alert(data.txt, "提示");
                        reloadTable(1);
                    } else {
                        $.alert(data.txt, "提示");
                    }
                }
            });
        },
        cancel: function () {
        }
    });
}

