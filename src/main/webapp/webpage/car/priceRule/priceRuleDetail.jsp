<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="modal fade" id="priceRuleModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:83%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">
                    <c:choose>
                        <c:when test="${not empty priceRuleDto.id}">
                            修改
                        </c:when>
                        <c:otherwise>
                            新增
                        </c:otherwise>
                    </c:choose>

                    ${priceRuleDto.channelName}
                </h4>
            </div>
            <form:form  cssClass="form-horizontal bv-form"  modelAttribute="priceRuleDto" id="priceRuleDetailForm" action="car/priceRule/saveRule">
                <form:hidden path="id"></form:hidden>
                <form:hidden path="groupId"></form:hidden>
                <form:hidden path="channelCode"></form:hidden>
                <form:hidden path="cityId"></form:hidden>
                <form:hidden path="cityName"></form:hidden>
                <form:hidden path="productId"></form:hidden>
                <form:hidden path="distance"></form:hidden>
                <form:hidden path="estPrice"></form:hidden>
                <form:hidden path="settlePrice"></form:hidden>
                <form:hidden path="supplierCode"></form:hidden>

                <div class="modal-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">出发城市</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="cityNamesDesc"  readonly onclick="selCarCityData()" style="width: 100%;"/>
                            </div>
                            <div class="col-sm-6"><label class="radio-inline" id="cityNames"></label></div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">距离/公里</label>
                            <div class="col-md-4">
                                <div class="col-sm-7">
                                    <label class="radio-inline">
                                        <input type="radio" value="1" name="distanceType" <c:if test="${ empty priceRuleDto.distance }">checked</c:if><c:if test="${ not empty priceRuleDto.distance and fn:endsWith(priceRuleDto.distance, '+')}"> checked</c:if> >大于等于
                                    </label>
                                    <label class="radio-inline ">
                                        <input type="radio" value="-1" name="distanceType" <c:if test="${not empty priceRuleDto.distance and fn:endsWith(priceRuleDto.distance, '-')}"> checked</c:if>>小于等于
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <input class="form-control" type="text" name="distancetxt"  value="<c:if test="${not empty priceRuleDto.distance}"> ${fn:substring(priceRuleDto.distance, 0, fn:length(priceRuleDto.distance)-1)} </c:if>"  >
                                </div>
                            </div>
                            <label class="col-sm-1 control-label">报价/元</label>
                            <div class="col-sm-5">
                                <div class="col-sm-6">
                                    <label class="radio-inline">
                                        <input type="radio" value="1" name="priceType" <c:choose><c:when test="${ not empty priceRuleDto.estPrice}"><c:if test="${ fn:endsWith(priceRuleDto.estPrice, '+')}"> checked</c:if></c:when><c:otherwise>checked</c:otherwise></c:choose> >大于等于
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" value="-1" name="priceType" <c:if test="${ not empty priceRuleDto.estPrice and fn:endsWith(priceRuleDto.estPrice, '-')}"> checked</c:if> >小于等于
                                    </label>
                                </div>
                                <div class="col-sm-3">
                                    <input class="form-control" type="text" name="price" value="<c:if test="${not empty priceRuleDto.estPrice}"> ${fn:substring(priceRuleDto.estPrice, 0, fn:length(priceRuleDto.estPrice)-1)} </c:if>"  >
                                </div>
                            </div>
                        </div>

                        <div  class="form-group">
                            <label class="col-sm-2 control-label">供货商</label>
                            <div class="col-sm-4">
                                <c:forEach items="${supplierList}" var="suplist">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="suppliercheckbox"  value="${suplist}"
                                        <c:forEach items="${fn:split(priceRuleDto.supplierCode,',')}" var="supplierCode">
                                               <c:if test="${supplierCode eq suplist}">checked</c:if>
                                        </c:forEach>
                                        > ${suplist.message}
                                    </label>
                                </c:forEach>
                            </div>
                            <label class="col-sm-1 control-label">用车类型</label>
                            <div class="col-sm-5">
                                <c:forEach items="${priceRuleDto.carTypes}" var="carTypeMap">
                                    <label class="checkbox-inline"><input type="checkbox" name="carTypecheckbox"  value="${carTypeMap.key}"
                                        <c:if test="${not empty priceRuleDto.productId}">
                                           <c:forEach items="${fn:split(priceRuleDto.productId,',' )}" var="carType">
                                                <c:if test="${carType eq carTypeMap.key}">checked</c:if>
                                           </c:forEach>
                                        </c:if>>
                                            ${carTypeMap.value}
                                    </label>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">干预阶段</label>

                            <div class="col-md-1">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="0" name="interventionType" <c:if test="${fn:contains(priceRuleDto.interventionType,0)}">checked</c:if> >报价
                                </label>
                            </div>

                            <label class="col-sm-1 control-label">百分比/%</label>
                            <div class="col-md-2 ">
                                <input type="number" class="form-control" name="estPercent" value="${priceRuleDto.estPercent}">
                            </div>
                            <label class="col-sm-1 control-label">金额/元</label>
                            <div class="col-md-2">
                                <input  type="number" class="form-control" name="estAmount" value="${priceRuleDto.estAmount}">
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-1 col-md-offset-2">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="1" name="interventionType" <c:if test="${fn:contains(priceRuleDto.interventionType,1)}">checked</c:if> >结算
                                </label>
                            </div>
                            <label class="col-sm-1 control-label">百分比/%</label>
                            <div class="col-md-2 ">
                                <input type="number" class="form-control" name="settlePercent" value="${priceRuleDto.settlePercent}">
                            </div>
                            <label class="col-sm-1 control-label">金额/元</label>
                            <div class="col-md-2">
                                <input  type="number" class="form-control" name="settleAmount" value="${priceRuleDto.settleAmount}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">生效日期</label>
                            <div class="col-sm-2">
                                <input class=" form-control datetime-picker" id="startdatetxt"  name="startDate" readonly placeholder="选择生效起始日期" value="<fmt:formatDate value="${priceRuleDto.startDate}" pattern="yyyy-MM-dd" />"/>
                            </div>

                            <div class="col-sm-2">
                                <input class="form-control datetime-picker"  id="enddatetxt" name="endDate"   readonly placeholder="选择生效终止日期" value="<fmt:formatDate value="${priceRuleDto.endDate}" pattern="yyyy-MM-dd" />"/>
                            </div>
                            <label class="col-sm-1 control-label">时间段</label>

                            <div class="col-sm-2">
                                <input class=" form-control time-picker" id="starttimetxt" name="startTime"  readonly placeholder="选择生效起始时间" value="${priceRuleDto.startTime}"/>
                            </div>

                            <div class="col-sm-2">
                                <input class="form-control time-picker"  id="endtimetxt" name="endTime"  readonly placeholder="选择生效终止时间" value="${priceRuleDto.endTime}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">状态</label>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" value="true" name="status" ${priceRuleDto.status?"checked":""} >启用
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" value="false" name="status"  ${priceRuleDto.status?"":"checked"} >禁用
                                </label>
                            </div>
                            <label class="col-sm-1 control-label">优先级</label>
                            <div class="col-sm-4">
                                <input class="form-control" name="priority" value="${priceRuleDto.priority}" onKeyUp="verifyRegular(this,'d+')" >
                            </div>
                             </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submitPriceRule()">保存</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </form:form>
        </div>
    </div>
</div>
<script>
    var getCarCityChoosedData=JSON.parse('${cityList}' || '[]');
</script>
<script type="text/javascript"  src="webpage/car/priceRule/js/ruleDetail.js?version=${globalVersion}"></script>
