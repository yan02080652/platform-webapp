<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<input type="hidden" id="rulegroupid"
       value="<c:if test="${ not empty piceRuleGroupMain.id}">${piceRuleGroupMain.id}</c:if>">
<c:if test="${ not empty piceRuleGroupMain.id}">
    <c:forEach items="${hubsCode}" var="hub">
        <input type="hidden" class="hid_hub" value="${hub}"/>
    </c:forEach>
</c:if>
<div id="priceRule">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                修改价格策略组
            </div>
            <div id="priceDetail" class="col-md-12 ">
                <div class="row">
                    <div class="col-md-12">


                    <div class="panel panel-default">
                        <div class="panel-body">

                            <input type="hidden" id="groupid">
                            <div class="form-group">
                                <label class="control-label col-md-4">价格策略组名称: ${piceRuleGroupMain.name }</label>
                                    <label class="control-label col-md-2">创建人:${piceRuleGroupMain.createUser}</label>
                                    <label class="control-label col-md-2">
                                        <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${piceRuleGroupMain.createDate }"/>
                                   </label>
                                    <label class="control-label col-md-2">修改人:${piceRuleGroupMain.updateUser}</label>
                                    <label class="control-label col-md-2">
                                            <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${piceRuleGroupMain.updateTime }"/>
                                    </label>
                            </div>

                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <ul id="myTab" class="nav nav-tabs">
                    <c:forEach items="${hubsCode}" var="hub" varStatus="status">
                        <li <c:if test="${status.first}">class="active"</c:if>>
                            <a href="#${hub}tab" data-toggle="tab" >
                                    ${hub.name}
                            </a>
                        </li>
                    </c:forEach>

                </ul>
                <div id="myTabContent" class="tab-content">
                    <c:forEach items="${hubsCode}" var="hub" varStatus="status">
                        <div class="tab-pane fade <c:if test="${status.first}">in active</c:if>" id="${hub}tab">
                            <div id="priceRuleTable_${hub}" style=" padding-top:10px;display:none"></div>
                        </div>
                    </c:forEach>
                </div>
                <style>
                    #priceRuleTable .form-group.col-md-3 {
                        margin-bottom: 0;
                    }

                    #priceRuleTable h3 {
                        margin: 0;
                    }

                    #priceRuleTable .col-md-12.form-horizontal {
                        padding-top: 16px;
                        padding-bottom: 10px;
                    }

                    /* 按钮 */
                    btn {
                        margin-left: 10px;
                        margin-right: 10px;
                        padding: 2px 10px;
                    }
                </style>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12"><a href="car/priceRule" class="btn btn-default pull-right">返回列表</a></div>
<div id="detailDiv"></div>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>
<script type="text/javascript" src="webpage/car/priceRule/js/rule.js?version=${globalVersion}"></script>