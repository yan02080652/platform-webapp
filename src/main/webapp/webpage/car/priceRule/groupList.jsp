
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<table class="table table-striped table-bordered table-hover" id="priceRuleTable">
    <thead>
    <tr>
        <th><label>策略组名称</label></th>
        <th><label>适用企业</label></th>
        <th><label>修改时间</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="priceRuleGroupDto">
        <tr class="odd gradeX">
            <td> ${priceRuleGroupDto.name}</td>
            <td>${priceRuleGroupDto.applyName}
                <c:if test="${ priceRuleGroupDto.applyName ne '无'}">
                 <a href="/car/priceRule/${priceRuleGroupDto.id}/applyPartner/index">查看</a>
                </c:if>
            </td>
            <td><fmt:formatDate value="${priceRuleGroupDto.updateTime }" pattern="yyy-MM-dd HH:mm:ss"/></td>
            <td class="text-center">
                <a href="/car/priceRule/priceRuleGroup?id=${priceRuleGroupDto.id}">细则</a>&nbsp;&nbsp;
                <a onclick="addGroup(${priceRuleGroupDto.id})">修改</a>
                <a onclick="deletedGroup('${priceRuleGroupDto.id }');">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadTable"></jsp:include>
</div>