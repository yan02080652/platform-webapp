
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
    <div class="col-md-12 form-horizontal">
        <div class="col-md-2"><h4>${priceRuleDto.channelName}</h4></div>
        <div class="form-group col-md-3">
            <label class="control-label col-md-4">供应商:</label>
            <div class="col-md-8">
                <div class="supplier ">
                    <select  id="${priceRuleDto.channelCode}supplier" class="form-control">
                        <option value="">全部</option>
                        <c:forEach items="${supplierList }" var="supplier">
                            <option value="${supplier}">${supplier.message}</option>
                        </c:forEach>
                </select>
            </div>
            </div>
        </div>

        <div class="form-group col-md-3">
            <label class="control-label col-md-4">状态:</label>
            <div class="col-md-8">
            <div class="status">
                <select  id="${priceRuleDto.channelCode}status" class="form-control">
                    <option value="">全部</option>
                    <option value="1">启用</option>
                    <option value="0">禁用</option>
                </select>
            </div>
            </div>
        </div>
        <div class="form-group col-md-3">
            <a class="btn btn-default"  role="button" id="btnsearch${priceRuleDto.channelCode}" onclick="reloadRuleList(1,'${priceRuleDto.channelCode}')" >筛选</a>
            <a class="btn btn-default"  role="button" id="btnadd${priceRuleDto.channelCode}" onclick="getPriceRule('${priceRuleDto.channelCode}')">新增价格策略</a>
        </div>
    </div>
</div>
<div id="${priceRuleDto.channelCode}div">
<table class="table table-striped table-bordered table-hover" id="${priceRuleDto.channelCode}table">
    <thead>
    <tr>
        <th><label>出发城市</label></th>
        <th><label>距离</label></th>
        <th><label>报价</label></th>
        <th><label>供应商</label></th>
        <th><label>用车类型</label></th>
        <th><label>干预阶段</label></th>
        <th><label>百分比P*(1+X)</label></th>
        <th><label>金额P+Y</label></th>
        <th><label>生效日期</label></th>
        <th><label>生效时间</label></th>
        <th><label>状态</label></th>
        <th><label>优先级</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">

        <tr class="odd gradeX">
            <td>
                <c:choose>
                    <c:when test="${empty bean.cityName}">不限</c:when>
                    <c:otherwise>${bean.cityName}</c:otherwise>
                </c:choose>

            </td>
            <td>
                <c:choose>
                    <c:when test="${empty bean.distance}">不限</c:when>
                    <c:otherwise>
                        <c:if test="${fn:endsWith(bean.distance,'+' )}">
                            >= ${fn:substring(bean.distance, 0,fn:length(bean.distance)-1)}
                        </c:if>
                        <c:if test="${fn:endsWith(bean.distance,'-' )}">
                            <= ${fn:substring(bean.distance, 0,fn:length(bean.distance)-1)}
                        </c:if>
                    </c:otherwise>
                </c:choose>

            </td>
            <td>
                <c:choose>
                    <c:when test="${empty bean.settlePrice}">不限</c:when>
                    <c:otherwise><c:if test="${fn:endsWith(bean.settlePrice,'+' )}">
                        >= ${fn:substring(bean.settlePrice, 0,fn:length(bean.settlePrice)-1)}
                    </c:if>
                        <c:if test="${fn:endsWith(bean.settlePrice,'-' )}">
                            <= ${fn:substring(bean.settlePrice, 0,fn:length(bean.settlePrice)-1)}
                        </c:if></c:otherwise>
                </c:choose>
            </td>
            <td>
                <c:forEach items="${supplierList}" var="sl">
                <c:if test="${fn:contains(bean.supplierCode, sl)}">
                    ${sl.message}
                </c:if>
                </c:forEach></td>
            <td>
                <c:forEach items="${carTypes}" var="ct" >
                <c:if test="${fn:contains(bean.productId,ct.code)}">
                    ${ct.message}
                </c:if>
                </c:forEach>
            </td>
            <c:choose>
                <c:when test="${bean.interventionType =='0'}">
                    <td>报价</td>
                    <td>${bean.estPercent} %</td>
                    <td>${bean.estAmount}</td>
                </c:when>
                <c:when test="${bean.interventionType =='0'}">
                    <td>结算</td>
                    <td>${bean.settlePercent} %</td>
                    <td>${bean.settleAmount}</td>
                </c:when>
                <c:otherwise>
                    <td>报价/结算</td>
                    <td>${bean.estPercent} %/ ${bean.settlePercent} %</td>
                    <td>${bean.estAmount} / ${bean.settleAmount}</td>
                </c:otherwise>
            </c:choose>

            <td>
                        <c:choose>
                            <c:when test="${not empty bean.startDate}">
                                <fmt:formatDate value="${bean.startDate}" pattern="yyyyMMdd"/>
                            </c:when>
                            <c:otherwise>
                               不限
                            </c:otherwise>
                        </c:choose>
                        -
                        <c:choose>
                            <c:when test="${not empty bean.endDate}">
                                <fmt:formatDate value="${bean.endDate}" pattern="yyyyMMdd"/>
                            </c:when>
                            <c:otherwise>
                                不限
                            </c:otherwise>
                        </c:choose>

            </td>
            <td>
                <c:choose>
                    <c:when test="${not empty bean.startTime}">
                        ${bean.startTime}
                    </c:when>
                    <c:otherwise>
                        00:00
                    </c:otherwise>
                </c:choose>
                -
                <c:choose>
                    <c:when test="${not empty bean.endTime}">
                        ${bean.endTime}
                    </c:when>
                    <c:otherwise>
                        24:00
                    </c:otherwise>
                </c:choose>
            </td>
            <td><c:if test="${not empty bean.status}"><c:choose>
                <c:when test="${bean.status==false}"> <a onclick="setStatus(this,${bean.id },${!bean.status})" >禁用</a></c:when>
                <c:otherwise> <a onclick="setStatus(this,${bean.id },${!bean.status})" >启用</a></c:otherwise>
            </c:choose></c:if></td>
            <td>${bean.priority}</td>

            <td class="text-center">
                <a onclick="getPriceRule('${priceRuleDto.channelCode}',${bean.id })" >修改</a>
                <a onclick="deletedPriceRule('${priceRuleDto.channelCode}',${bean.id });">删除</a>
                <a onclick="getPriceRule('${priceRuleDto.channelCode}',${bean.id },1)" >复制</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin" style="margin-bottom: 20px" >
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadRuleList">
        <jsp:param name="p1" value="'${priceRuleDto.channelCode}'"></jsp:param>
    </jsp:include>
</div>
</div>