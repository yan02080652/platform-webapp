<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" id="applyPartnerListTable">
    <thead>
    <tr>
        <th><label>适用企业</label></th>
        <th><label>有效期从</label></th>
        <th><label>有效期至</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="priceRuleApplyDto">
        <tr class="odd gradeX">
            <td>${priceRuleApplyDto.partnerName}</td>
            <td> <c:if test="${ not empty priceRuleApplyDto.startTime }"><fmt:formatDate value="${priceRuleApplyDto.startTime }" pattern="yyy-MM-dd HH:mm:ss"/></c:if></td>
            <td><c:if test="${not empty priceRuleApplyDto.endTime }"><fmt:formatDate value="${priceRuleApplyDto.endTime }" pattern="yyy-MM-dd HH:mm:ss"/></c:if></td>
        </tr>
    </c:forEach>
    </tbody>
    <tfoot>
    <tr><tr> <td colspan="6">
        <c:if test="${pageList.total>0 }">
            <div class="form-group"><a class="btn btn-default" href="/partner/priceRule/" >分配价格策略组</a></div>
        </c:if>
    </td></tr></tr>
    </tfoot>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadTable"></jsp:include>
</div>
