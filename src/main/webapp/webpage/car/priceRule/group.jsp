<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="priceRuleGroupMain">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>TMC用车价格策略组列表  &nbsp;&nbsp;
            </div>
            <div id="priceDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <a onclick="addGroup()" class="btn btn-default">新增价格策略组</a>
                                            <a class="btn btn-default"  role="button" href="/partner/priceRule/">分配价格策略组</a>
                                        </div>
                                    </div>
                                </div>
                                <div  id="priceRule_Table" style="margin-top: 15px">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- 详细页面 -->
<div id="group_detail" style="display:none;"></div>
<div id="priceRuleGroupMainDetail" style="display:none;"></div>
<script type="text/javascript" src="webpage/car/priceRule/js/group.js?version=${globalVersion}"></script>
