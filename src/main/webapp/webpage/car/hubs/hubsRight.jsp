<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
 <style>
 #trans-detail table{
 	margin-left: 15px;
 }
 #trans-detail table th{
 	font-size: 14px;
 	text-align: right;
 }
 
</style>
<c:if test="${not empty hubsDto}">
	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>连接器详细信息&nbsp;&nbsp;
		</div>
		<div class="panel-body">
			<div id="trans-detail">
				<table>
					<tr>
						<th width="85px">连接器编码:</th>
						<td width="10%">${hubsDto.hubCode }</td>
						<th width="85px">名称:</th>
						<td width="10%">${hubsDto.hubName }</td>
						<%--<th width="85px">连接器类型:</th>--%>
						<%--<td width="10%"><c:if test="${hubsDto.type == 1}"> 平台</c:if>--%>
					        <%--<c:if test="${hubsDto.type == 2}"> OTA</c:if>--%>
					        <%--<c:if test="${hubsDto.type == 3}"> TMC</c:if>--%>
					        <%--<c:if test="${hubsDto.type == 4}"> IBE</c:if>--%>
					        <%--<c:if test="${hubsDto.type == 5}"> 航司直连</c:if>--%>
					    <%--</td>--%>
					    <th width="85px">是否禁用:</th>
						<td width="10%"><c:if test="${hubsDto.isDisable == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
					        <c:if test="${hubsDto.isDisable == false}"> <input type="checkbox" disabled="disabled"/></c:if>
						</td>
					</tr>
					<%--<tr>--%>
						<%--<th>详细说明:</th>--%>
						<%--<td colspan="7">--%>
							<%--${hubsDto.description}--%>
						<%--</td>--%>
					<%--</tr>--%>
					<%--<tr>--%>
						<%--<th>查询接口:</th>--%>
						<%--<td><c:if test="${hubsDto.isCanQuery == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
					        <%--<c:if test="${hubsDto.isCanQuery == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
						<%--</td>--%>
						<%--<th>验价接口:</th>--%>
						<%--<td><c:if test="${hubsDto.isCanCheckprice == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
					        <%--<c:if test="${hubsDto.isCanCheckprice == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
						<%--</td>--%>
						<%--<th>占座接口:</th>--%>
						<%--<td><c:if test="${hubsDto.isCanSeat == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
					        <%--<c:if test="${hubsDto.isCanSeat == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
						<%--</td>--%>
						<%--<th>支付前验单:</th>--%>
						<%--<td><c:if test="${hubsDto.isCanPaybeforecheck == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
					        <%--<c:if test="${hubsDto.isCanPaybeforecheck == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
						<%--</td>--%>
					<%--</tr>--%>
					<%--<tr>--%>
						<%--<th>出票接口:</th>--%>
						<%--<td><c:if test="${hubsDto.isCanIssue == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
					        <%--<c:if test="${hubsDto.isCanIssue == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
						<%--</td>--%>
						<%--<th>退改签接口:</th>--%>
						<%--<td><c:if test="${hubsDto.isCanChange == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
					        <%--<c:if test="${hubsDto.isCanChange == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
						<%--</td>--%>

					<%--</tr>--%>

				</table>
			</div>
		</div>
	</div>
	
	<%--<div class="panel panel-default" id="authtypeDiv">--%>
		<%--<div class="panel-heading">--%>
			<%--<span class="glyphicon glyphicon-home" aria-hidden="true"></span>所支持的连接&nbsp;&nbsp;--%>
			<%--<button class="btn btn-default"  onclick="addConnect('${hubsDto.hubCode }')" role="button">添加</button>--%>
		<%--</div>--%>
		<%--<div class="panel-body">--%>
		<%--<div class="col-sm-12">--%>
			<%--<table class="table  table-bordered table-hover table-condensed dataTables-example dataTable">--%>
				<%--<thead>--%>
					<%--<tr>--%>
						<%--<th class="sort-column" >连接名称</th>--%>
						<%--<!-- <th class="sort-column" >账号</th>--%>
						<%--<th class="sort-column" >密码</th> -->--%>
						<%--<th class="sort-column" >查询接口</th>--%>
						<%--<th class="sort-column" >验价接口</th>--%>
						<%--<th class="sort-column" >占座接口</th>--%>
						<%--<th class="sort-column" >出票接口</th>--%>
						<%--<th class="sort-column" >退改签接口</th>--%>
						<%--<th class="sort-column" >支付前验单</th>--%>
						<%--<th class="sort-column" >覆盖全航司</th>--%>
						<%--<th class="sort-column" >禁用</th>--%>
						<%--<th>操作</th>--%>
					<%--</tr>--%>
				<%--</thead>--%>
				<%--<tbody>--%>
					<%--<c:forEach items="${hubsConnectDtos }" var="hubsConnectDto">--%>
				         <%--<tr>--%>
				             <%--<td>${hubsConnectDto.name }</td>--%>
				             <%--&lt;%&ndash; <td>${hubsConnectDto.accountName }</td>--%>
				             <%--<td>${hubsConnectDto.accountPwd }</td> &ndash;%&gt;--%>
				             <%--<td>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanQuery == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanQuery == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
				             <%--</td>--%>
				             <%--<td>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanCheckprice == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanCheckprice == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
				             <%--</td>--%>
				             <%--<td>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanSeat == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanSeat == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
				             <%--</td>--%>
				             <%--<td>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanIssue == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanIssue == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
				             <%--</td>--%>
				             <%--<td>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanChange == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanChange == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
				             <%--</td>--%>
				             <%--<td>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanPaybeforecheck == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanPaybeforecheck == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
				             <%--</td>--%>
				             <%--<td>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanAllCarrier == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
				             	<%--<c:if test="${hubsConnectDto.isCanAllCarrier == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
				             <%--</td>--%>
				             <%--<td>--%>
				             	<%--<c:if test="${hubsConnectDto.isDisable == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>--%>
				             	<%--<c:if test="${hubsConnectDto.isDisable == false}"> <input type="checkbox" disabled="disabled"/></c:if>--%>
				             <%--</td>--%>
				             <%--<td>--%>
					        	<%--<c:if test="${hubsDto.isDisable == false}"> --%>
					        		<%--<a onclick="editConnect('${hubsConnectDto.id }')">修改</a>&nbsp;--%>
	             					<%--<a onclick="deleteConnect('${hubsConnectDto.id }')">删除</a>&nbsp;--%>
	             					<%--<c:if test="${hubsConnectDto.isCanAllCarrier == false }">--%>
	             						<%--<a onclick="connectCarrier('${hubsConnectDto.id }')">连接航司</a>--%>
	             					<%--</c:if>--%>
					        	<%--</c:if>--%>
				             <%--</td>--%>
				         <%--</tr>--%>
			       <%--</c:forEach>--%>
					<%----%>
				<%--</tbody>--%>
			<%--</table>--%>
		<%--</div>--%>
		<%--</div>--%>
	<%--</div>--%>
	<div class="panel panel-default" id="authtypeDiv">
		<div class="panel-heading">
			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>所支持的连接&nbsp;&nbsp;
			<button class="btn btn-default" onclick="addConnect('${hubsDto.hubCode }')" role="button">添加</button>
		</div>
		<div class="panel-body">
			<div class="col-sm-12">
				<table class="table  table-bordered table-hover table-condensed dataTables-example dataTable">
					<thead>
					<tr>
						<th class="sort-column">连接名称</th>
						<th class="sort-column">商户名称</th>
						<th class="sort-column">所属tmc</th>
						<th class="sort-column">出票渠道</th>
						<th class="sort-column">禁用</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>
					<if test="${ null!=hubsConnectDtos }">
					<c:forEach items="${hubsConnectDtos }" var="hubsConnectDto">
						<tr>
							<td>${hubsConnectDto.hubsConnectName }</td>
							<td>${hubsConnectDto.merchantName }</td>
							<td>${hubsConnectDto.tmcId }</td>
							<td>${hubsConnectDto.channelCode }</td>
							<td>
								<input type="checkbox" disabled="disabled" ${hubsConnectDto.isDisable ? 'checked="checked"' : ''}/>
							</td>
							<td>
								<c:if test="${hubsDto.isDisable == false}">
									<a onclick="editConnect('${hubsConnectDto.id }')">修改</a>&nbsp;
									<a onclick="deleteConnect('${hubsConnectDto.id }')">删除</a>&nbsp;
								</c:if>
							</td>
						</tr>
					</c:forEach>
					</if>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</c:if>

<div id="modelHubsConnectDiv"></div>
