<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="mode_hubs" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <c:choose>
            	<c:when test="${ empty hubsDto.hubCode}">
            		<h4 class="modal-title">新增连接器</h4>
            	</c:when>
            	<c:otherwise>
            		<h4 class="modal-title">修改连接器</h4>
            	</c:otherwise>
            </c:choose>
         </div>
         <div class="modal-body">
         <form:form modelAttribute="hubsDto" class="form-horizontal" id="detailForm" role="form" action="car/hubs/saveHub" method="post">
               <input type="hidden" name="editMode" id="editMode" value="${editMode}"/>
			   <div class="form-group">
		     	  <label for="code" class="col-sm-3 control-label">连接器编码</label>
		     	  <div class="col-sm-9">
		         	 <input type="text" class="form-control" id="code" name="hubCode" value="${hubsDto.hubCode}" placeholder="编码"/>
		     	  </div>
		  	   </div>
			   <div class="form-group">
			      <label for="hubName" class="col-sm-3 control-label">连接器名称</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" id="hubName" name="hubName" value="${hubsDto.hubName}" placeholder="名称"/>
			      </div>
			   </div>
		  	 	<div class="form-group">
			  	 	<div class="pull-left col-sm-12">
						<div class="form-group">
							<label for="isDisable" class="col-sm-3 control-label">是否禁用</label>
							<div>
								<label class="radio-inline"><form:radiobutton path="isDisable" value="1"/>是</label>
								<label class="radio-inline"><form:radiobutton path="isDisable" value="0" />否</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">支持产品类型</label>
							<label class="checkbox-inline"><form:checkbox path="isSupportFastCar" value="1"/>快车</label>
							<label class="checkbox-inline"><form:checkbox path="isSupportSpecialCar" value="1"/>专车</label>
							<label class="checkbox-inline"><form:checkbox path="isSupportAirportTransfer" value="1"/>接送机</label>
							<label class="checkbox-inline"><form:checkbox path="isSupportTrainTransfer" value="1"/>接送站</label>
							<label class="checkbox-inline"><form:checkbox path="isSupportTaxi" value="1"/>出租车</label>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">供应商</label>
							<c:forEach items="${supplierType}" var="bean" varStatus="status" >
								<label class="checkbox-inline">
								<c:choose>
									<c:when test="${fn:contains(hubsDto.supplierType,bean)}">
										<form:checkbox path="supplierType" value="${bean}" checked="true"/>${bean.message}</label>
									</c:when>
									<c:otherwise>
										<form:checkbox path="supplierType" value="${bean}" />${bean.message}</label>
									</c:otherwise>
								</c:choose>

							</c:forEach>
						</div>
			  	 	</div>
			  	</div>
			 <div class="modal-footer">
				 <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				 <button type="button" class="btn btn-primary" onclick="submitHub()">保存</button>
			 </div>
		 </form:form>
		 </div>
      </div>
</div>
</div>
