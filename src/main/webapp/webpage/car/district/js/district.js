$(function(){
    reloadDistrict(1);
})
function reloadDistrict(pageIndex){
    var loadingList = layer.load();
    //加载列表
    $.post('/car/district/list',
        {
            pageIndex:pageIndex,
            name : $('#name').val(),
            status : $("option:selected").attr('data-code')
        },function(data){
            layer.close(loadingList);
            $("#rule_Table").html(data);
        });
}

//转到添加或修改页面
function getDistrictById(id){
    var name=$('#name').val();
    var  isMatching = $("option:selected").attr('data-code');
    $.post("/car/district/getDistrict",{id:id,isMatching:isMatching,name:name},function(data){
        $("#ruleDetail").html(data);
        $("#ruleMain").hide();
        $("#ruleDetail").show();
    });
}

//重置
function reset_district(){
    $('#name').val("");
    $('select option').eq(0).attr('selected', true);
    reloadDistrict(1);

}
//更新城市数据
function synDistrictData() {
    layer.confirm('确认更新城市数据吗?',function (index) {
        layer.close(index);
        var loading = layer.load();
        $.ajax({
            url: "/car/district/batchUpdate",
            type: "post",
            async: true,
            success: function (data) {
                layer.close(loading);
                if (data) {
                    location.href = "/car/district";
                } else {
                    $.alert('系统繁忙,请稍后重试');
                }
            },error:function () {
                layer.close(loading);
                $.alert('系统繁忙,请稍后重试');
            }
        });
    });

}