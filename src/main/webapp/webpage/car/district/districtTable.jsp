<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" id="ruleTable">
    <thead>
    <tr>
        <th><label>序号</label></th>
        <th><label>城市代码</label></th>
        <th><label>城市中文名</label></th>
        <th><label>在途城市名</label></th>
        <th><label>在途城市中文名全拼</label></th>
        <th><label>在途城市中文简拼</label></th>
        <th><label>最后更新时间</label></th>
        <th><label>状态</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr class="odd gradeX">
            <td>${bean.id}</td>
            <td>${bean.providerCityId}</td>
            <td>${bean.providerCityName}</td>
            <td>${bean.sysCityName}</td>
            <td>${bean.nameEn}</td>
            <td>${bean.nameSimple}</td>

            <td><c:if test="${not empty bean.lastUpdateTime}"><fmt:formatDate value="${bean.lastUpdateTime}" pattern="yyyy-MM-dd HH:mm:ss"/></c:if></td>
            <td>
                <c:if test="${empty bean.status}"></c:if>
                <c:if test="${not empty bean.status}">
                    <c:choose>
                        <c:when test="${ bean.status}">匹配</c:when>
                        <c:otherwise>未匹配</c:otherwise>
                    </c:choose>
                </c:if>
            </td>
            <td class="text-center" >
                <a onclick="getDistrictById('${bean.id }')" >修改</a>
            </td>
        </tr>
    </c:forEach>

    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadDistrict"></jsp:include>
</div>
<script type="text/javascript">
    //checkbox全选居中，固定宽度
    $("table tr").each(function(){
        $(this).find(":first").attr("style","text-align: center;width:50px");
    })
</script>