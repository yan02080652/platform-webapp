<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div id="groupMain">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>TMC用车价格策略组列表 &nbsp;&nbsp;
            </div>
            <div id="rightDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <!-- 查询条件 -->
                                            <div class="form-inline" >
                                                <nav class="text-left">
                                                    <div class="form-group">
                                                        <label class="control-label">关键字:</label>
                                                        <input class="form-control" placeholder="策略组名称" id="groupName" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-default btn-primary"
                                                                onclick="reloadList(1)">搜索
                                                        </button>
                                                        <button type="button" class="btn btn-default"
                                                                onclick="reset_search()">清空条件
                                                        </button>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div id="group_Table" style="margin-top: 15px">
                                    <jsp:include page="group_table.jsp" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="group_detail" style="display: none;"></div>

<script src="webpage/car/price/js/group.js?version=${globalVersion}"></script>