<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<div class="modal fade" id="mode_apply" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <c:choose>
                    <c:when test="${empty rule.id}">
                        <h4 class="modal-title">新增企业适用的用车价格策略组</h4>
                    </c:when>
                    <c:otherwise>
                        <h4 class="modal-title">编辑企业适用的用车价格策略组</h4>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="modal-body">
                <form:form modelAttribute="apply" class="form-horizontal" id="detailForm" role="form">
                    <form:hidden path="id" />
                    <div class="form-group">
                        <label class="col-sm-3 control-label">企业</label>
                        <div class="col-sm-9">
                            <form:label path="partnerName" cssClass="control-label" >${apply.partnerName}</form:label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">价格策略组</label>
                        <div class="col-sm-9">
                            <form:label path="groupName" cssClass="control-label" >${apply.groupName}</form:label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">有效期从</label>
                        <div class="col-sm-9">
                            <form:input path="startTime" cssClass="form-control" readonly="true"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">有效期至</label>
                        <div class="col-sm-9">
                            <form:input path="endTime" cssClass="form-control" readonly="true"/>
                        </div>
                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="saveApply()">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        jeDate("#startTime",{
            format:"YYYY-MM-DD hh:mm:ss",
            isTime:false,
            minDate:"2018-09-01",
            isClear:false,
            isToday:false,
            onClose: false,
        })
        jeDate("#endTime",{
            format:"YYYY-MM-DD hh:mm:ss",
            isTime:false,
            minDate:"2018-09-01",
            isClear:false,
            isToday:false,
            onClose: false,
        })
    })
</script>