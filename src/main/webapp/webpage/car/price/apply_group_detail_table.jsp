<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<table class="table table-bordered" id="applyGroupTable">
    <thead>
    <tr>
        <th>
            <input type="checkbox" id="selectAllGroup">
        </th>
        <th>策略组 </th>
        <th>有效期从</th>
        <th>有效期至</th>
        <th style="width:13%;">优先级</th>
    </tr>
    </thead>
    <tbody>
        <c:forEach items="${groupApplyList}" var="bean">
            <tr class="odd gradeX">
                <td><input type="checkbox" name="ids" value="${bean.groupId }"/></td>
                <td>${bean.groupName}</td>
                <td>
                    <fmt:formatDate value="${bean.startTime}" pattern="yyyy-MM-dd HH:mm:ss" var="startTime"/>
                    <input class="form-control start_time" value="${startTime}" groupId="${bean.groupId}" readonly>
                <td>
                    <fmt:formatDate value="${bean.endTime}" pattern="yyyy-MM-dd HH:mm:ss" var="endTime"/>
                    <input class="form-control end_time" value="${endTime}"  groupId="${bean.groupId}" readonly>
                </td>
                <td><input class="form-control priority" value="${bean.priority}" groupId="${bean.groupId}" ></td>
            </tr>
        </c:forEach>
    </tbody>
</table>

<script type="text/javascript">

    $(function(){
        $('#selectAllGroup').change(function(){
            if ($("#selectAllGroup").is(":checked")) {
                $("#applyGroupTable input[type=checkbox]").prop("checked", true);//所有选择框都选中
            } else {
                $("#applyGroupTable input[type=checkbox]").prop("checked", false);
            }
        })
    })
    //checkbox全选居中，固定宽度
    $("table tr").each(function(){
        $(this).find(":first").attr("style","text-align: center;width:50px");
    })

    jeDate(".start_time",{
        format:"YYYY-MM-DD hh:mm:ss",
        isTime:false,
        minDate:"2018-09-01",
        isClear:false,
        isToday:false,
        onClose: false,
    })

    jeDate(".end_time",{
        format:"YYYY-MM-DD hh:mm:ss",
        isTime:false,
        minDate:"2018-09-01",
        isClear:false,
        isToday:false,
        onClose: false,
    })


</script>