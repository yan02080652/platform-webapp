<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<link rel="stylesheet" href="${baseRes}/plugins/jedate-6.5.0/skin/jedate.css" >
<script src="${baseRes}/plugins/jedate-6.5.0/jedate.min.js"></script>
<div id="groupMain">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>TMC用车价格策略细则 &nbsp;&nbsp;
            </div>
            <div id="rightDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <!-- 查询条件 -->
                                            <div class="form-inline" >
                                                <nav class="text-left">
                                                    <div class="form-group" style="font-size: 17px;">
                                                        <label class="control-label">价格策略组名称:</label>
                                                        <span class="">${group.name}</span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">&nbsp;&nbsp;&nbsp;&nbsp;创建人:</label>
                                                        <span class="">${group.createUser} &nbsp;&nbsp;<fmt:formatDate value="${group.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">&nbsp;&nbsp;&nbsp;&nbsp;修改人:</label>
                                                        <span class="">${group.updateUser} &nbsp;&nbsp;<fmt:formatDate value="${group.updateTime}" pattern="yyyy-MM-dd HH:mm:ss"/></span>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <!-- 渠道商细则 -->
                                <div id="rule_hub"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<c:forEach items="${hubs}" var="hub" >
    <input type="hidden" class="hid_hub" value="${hub}" />
</c:forEach>

<input type="hidden" value="${group.id}" id="groupId">

<div id="rule_detail"></div>

<script src="webpage/car/price/js/rule.js?version=${globalVersion}"></script>