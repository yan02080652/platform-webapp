<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<table class="table table-bordered">
    <thead>
    <tr>
        <th style="width:10%;">出发城市 </th>
        <th style="width:5%;">距离</th>
        <th style="width:5%;">报价</th>
        <th style="width:5%;">供应商</th>
        <th style="width:7%;">用车类型</th>
        <th style="width:15%;">干预阶段</th>
        <th style="width:15%;">生效日期</th>
        <th style="width:15%;">生效时间</th>
        <th style="width:5%;">状态</th>
        <th style="width:5%;">优先级</th>
        <th style="width:8%;">操作</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="bean">
        <tr>
            <td>${bean.cityName }</td>
            <td>${bean.distance}</td>
            <td>${bean.estPrice}</td>
            <td>
                <c:forEach items="${supplierTypes}" var="st" >
                    <c:if test="${fn:contains(bean.supplierCode,st)}">
                        ${st.message}
                    </c:if>
                </c:forEach>
            </td>
            <td>
            <c:forEach items="${carTypes}" var="ct" varStatus="ctStatus">
                <c:if test="${fn:contains(bean.productId,ct.code)}">
                    ${ct.message}
                </c:if>
            </c:forEach>
            </td>
            <td>
                <c:if test="${fn:contains(bean.interventionType,0)}">
                    报价: P*(1+${bean.estPercent}%)${fn:contains(bean.estAmount,"-") ? "":"+"}${bean.estAmount}
                </c:if>
                <c:if test="${fn:contains(bean.interventionType,1)}">
                    结算: P*(1+${bean.settlePercent}%)${fn:contains(bean.settleAmount,"-") ? "":"+"}${bean.settleAmount}
                </c:if>
            </td>
            <td>
                <c:choose>
                    <c:when test="${empty bean.startDate}">不限</c:when>
                    <c:otherwise>
                        <fmt:formatDate value="${bean.startDate}" pattern="yyyyMMdd"/>
                    </c:otherwise>
                </c:choose>
                -
                <c:choose>
                    <c:when test="${empty bean.endDate}">不限</c:when>
                    <c:otherwise>
                        <fmt:formatDate value="${bean.endDate}" pattern="yyyyMMdd"/>
                    </c:otherwise>
                </c:choose>
            </td>
            <td>
            ${empty bean.startTime ? '00:00' : bean.startTime}-${empty bean.endTime ? '24:00' : bean.endTime}</td>
            <td>${bean.status ? "开启" : "禁用"}</td>
            <td>${bean.priority}</td>
            <td class="text-center">
                <a class="detail" onclick="addRule(${bean.id})">修改</a>
                <a class="delete text-danger" onclick="delRule(${bean.id},'${bean.channelCode}')">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="pagin" style="margin-bottom: 20px" >
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadRuleList">
        <jsp:param name="p1" value="'${channelCode}'"></jsp:param>
    </jsp:include>
</div>