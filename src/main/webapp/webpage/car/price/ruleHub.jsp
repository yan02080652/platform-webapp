<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<div class="page-header ruleHub_${channelCode}">
    <form class="form-inline" method="post" >

        <nav class="text-left col-md-2">
            <span class="h3">${channelCode.name} |</span>
        </nav>
        <nav class="text-right col-md-10">
            <div class="form-group">
                <label class="control-label">供应商：</label>
                <select class="form-control" id="supplier_${channelCode}" style="width:150px;">
                    <option value="">全部</option>
                    <c:forEach items="${supplierType}" var="supplier" varStatus="status">
                        <option value="${supplier}">${supplier.message}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">状态：</label>
                <select class="form-control" id="status_${channelCode}" style="width:150px;">
                    <option value="">全部</option>
                    <option value="1">开启</option>
                    <option value="0">禁用</option>
                </select>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-default" onclick="reloadRuleList(1,'${channelCode}')">筛选</button>
                <button type="button" class="btn btn-default" onclick="addRule(null,'${channelCode}')">新增价格策略</button>
            </div>
        </nav>
    </form>

    <div id="rule_table_${channelCode}">
        <jsp:include page="rule_table.jsp" />
    </div>

</div>

<script>
    $(function(){
        reloadRuleList(1,'${channelCode}');
    })
</script>