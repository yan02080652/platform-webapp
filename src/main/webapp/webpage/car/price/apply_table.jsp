<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<table class="table table-bordered" id="">
    <thead>
    <tr>
        <c:if test="${!look}">
            <th style="width:10%;text-align: center">优先级</th>
            <th style="">策略组</th>
        </c:if>
        <c:if test="${look}">
            <th style="">适用企业</th>
        </c:if>
        <th style="">有效期从</th>
        <th style="">有效期至</th>
        <c:if test="${!look}">
            <th style="">操作</th>
        </c:if>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="bean">
        <tr>
            <c:if test="${!look}">
                <td style="text-align: center">${bean.priority }</td>
                <td>${bean.groupName}</td>
            </c:if>
            <c:if test="${look}">
                <td>${bean.partnerName}</td>
            </c:if>
            <td>
                <c:choose>
                    <c:when test="${empty bean.startTime}">
                        不限
                    </c:when>
                    <c:otherwise>
                        <fmt:formatDate value="${bean.startTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
                    </c:otherwise>
                </c:choose>
            </td>
            <td>
                <c:choose>
                    <c:when test="${empty bean.endTime}">
                        不限
                    </c:when>
                    <c:otherwise>
                        <fmt:formatDate value="${bean.endTime}" pattern="yyyy-MM-dd HH:mm:ss"/>
                    </c:otherwise>
                </c:choose>
            </td>
            <c:if test="${!look}">
                <td class="text-center">
                    <a class="detail" onclick="addApply(${bean.id},'${bean.groupName}')">修改</a>
                    <a class="delete text-danger" onclick="delApply(${bean.id})">删除</a>
                </td>
            </c:if>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="pagin" style="margin-bottom: 20px">
    <c:if test="${!look}">
        <button type="button" class="pull-left btn btn-default btn-primary" onclick="applyGroup()">分配价格策略组</button>
    </c:if>
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadList"></jsp:include>
</div>