<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<div class="modal fade" id="mode_group" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <c:choose>
                    <c:when test="${empty priceRuleGroupDto.id}">
                        <h4 class="modal-title">新增策略组</h4>
                    </c:when>
                    <c:otherwise>
                        <h4 class="modal-title">修改策略组</h4>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="modal-body">
                <form:form modelAttribute="priceRuleGroupDto" class="form-horizontal" id="detailForm" role="form">
                    <form:hidden path="id"/>
                    <div class="form-group">
                        <form:label path="name" cssClass="col-sm-2 control-label">名称</form:label>
                        <div class="col-sm-9">
                            <form:input path="name" class="form-control" placeholder="策略组名称"/>
                        </div>
                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="saveGroup()">保存</button>
            </div>
        </div>
    </div>
</div>