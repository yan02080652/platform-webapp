<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<link rel="stylesheet" href="${baseRes}/plugins/jedate-6.5.0/skin/jedate.css" >
<script src="${baseRes}/plugins/jedate-6.5.0/jedate.min.js"></script>

<div id="applyMain">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>TMC企业价格策略管理 &nbsp;&nbsp;
            </div>
            <div id="rightDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <!-- 查询条件 -->
                                            <div class="form-inline" >
                                                <nav class="text-left">
                                                    <div class="form-group">
                                                        <label class="control-label">选择企业:</label>
                                                        <input type="hidden" id="partnerId">
                                                        <input type="hidden" id="groupId" value="${groupId}">
                                                        <input type="text" class="form-control partner" name="cName" value="" placeholder="请选择企业" readonly autocomplete="off"/>
                                                    </div>
                                                    <%--<div class="form-group">--%>
                                                        <%--<button type="button" class="btn btn-default btn-primary"--%>
                                                                <%--onclick="reloadList(1)">搜索--%>
                                                        <%--</button>--%>
                                                        <%--<button type="button" class="btn btn-default"--%>
                                                                <%--onclick="reset_search()">清空条件--%>
                                                        <%--</button>--%>
                                                    <%--</div>--%>
                                                </nav>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div id="apply_table" style="margin-top: 15px">
                                    <%--<jsp:include page="apply_table.jsp" />--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="apply_detail" style="display: none;"></div>
<div id="apply_group_detail" style="display: none;"></div>

<script src="webpage/car/price/js/apply.js?version=${globalVersion}"></script>