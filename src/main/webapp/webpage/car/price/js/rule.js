$(function(){
    var hubs = $('.hid_hub');
    for(var i = 0; i< hubs.length;i++){
        reloadRule(hubs[i].value);
    }

})

//加载不同渠道商细则
function reloadRule(channelCode){
    $.ajax({
        url: '/car/price/ruleHub',
        type: 'post',
        async: false,
        data: {
            channelCode: channelCode
        },success: function(data){
            $('#rule_hub').append(data);
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }

    })
}

//细则列表
function reloadRuleList(pageIndex,channelCode){
    $.ajax({
        url: '/car/price/ruleList',
        type: 'post',
        async: false,
        data: {
            supplierCode: $('#supplier_' + channelCode).val(),
            ruleStatus: $('#status_' + channelCode).val(),
            channelCode: channelCode,
            groupId: $('#groupId').val(),
            pageIndex: pageIndex
        },success: function(data){
            $('#rule_table_' + channelCode).html(data);
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })
}

//新增细则
function addRule(id,channelCode){
    $.ajax({
        url: '/car/price/addRule',
        type: 'post',
        data: {
            id: id,
            channelCode: channelCode,
            groupId: $('#groupId').val(),
        },success: function(data){
            $('#rule_detail').html(data);
            $('#rule_detail').show();
            $('#mode_rule').modal();
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })
}

//保存细则
function saveRule(channelCode){
    //TOOO 参数校验
    if(!checkSubmit()){
        return;
    }


    $.ajax({
        url: '/car/price/saveRule',
        type: 'post',
        data: $('#detailForm').serialize(),
        success: function(data){
            if(data.success){
                $('#mode_rule').modal('hide');
                layer.msg("保存成功",{icon: 1});
                reloadRuleList(1,channelCode);
            }else{
                layer.msg("保存失败:" + data.msg,{icon: 2})
            }
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })

    function checkSubmit(){
        if($('[name=supplierCode]:checked').length == 0){
            layer.msg("请选择供应商",{icon: 0})
            return false;
        }
        if($('[name=productId]:checked').length == 0){
            layer.msg("请选择用车类型",{icon: 0})
            return false;
        }
        if($('[name=interventionType]:checked').length == 0){
            layer.msg("请选择干预类型",{icon: 0})
            return false;
        }
        if($('#distance').val().trim() == ''){
            layer.msg("距离必填",{icon: 0});
            $('#distance').focus();
            return false;
        }
        if($('#estPrice').val().trim() == ''){
            layer.msg("报价必填",{icon: 0});
            $('#estPrice').focus();
            return false;
        }
        if($('#priority').val().trim() == ''){
            layer.msg("优先级必填",{icon: 0});
            $('#priority').focus();
            return false;
        }
        if($('#cityId').val().trim() == ''){
            layer.msg("请选择出发城市",{icon: 0});
            return false;
        }
        if($('#startDate').val() != '' && $('#endDate').val() != '' && $('#startDate').val() >= $('#endDate').val()){
            layer.msg("生效结束日期必须大于开始日期",{icon: 0});
            return false;
        }

        return true;
    }
}

//删除
function delRule(id,channelCode){
    layer.confirm("确认删除?",function(){
        $.ajax({
            url: '/car/price/delRule',
            type: 'post',
            data: {
                id: id
            },success: function(data){
                if(data.success){
                    layer.msg("删除成功",{icon: 1});
                    reloadRuleList(1,channelCode);
                }else{
                    layer.msg("删除失败:" + data.msg,{icon: 2})
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2})
            }
        })
    })
}
