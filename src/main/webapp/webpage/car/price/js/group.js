$(function(){
    reloadList(1);
})

function reset_search(){
    $('#groupName').val('');
    reloadList(1);
}

function reloadList(pageIndex){
    $.ajax({
        url: '/car/price/groupList',
        type: 'post',
        data: {
            groupName: $('#groupName').val()
,           pageIndex: pageIndex
        },success: function(data){
            $('#group_Table').html(data);
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })
}

//新增或编辑
function addGroup(id){
    $.ajax({
        url: '/car/price/addGroup',
        type: 'post',
        data: {
            id: id
        },success: function(data){
            $('#group_detail').html(data);
            $('#group_detail').show();
            $('#mode_group').modal();
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })
}


//删除
function delGroup(id){
    layer.confirm("相关细则均会被删除,是否删除?",function(){
        $.ajax({
            url: '/car/price/delGroup',
            type: 'post',
            data: {
                id: id
            },success: function(data){
                if(data.success){
                    layer.msg("删除成功",{icon: 1});
                    reloadList(1);
                }else{
                    layer.msg("删除失败:" + data.msg,{icon: 2})
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2})
            }
        })
    })
}

//分配
function applyGroup(){

}

//保存
function saveGroup(){
    $.ajax({
        url: '/car/price/saveGroup',
        type: 'post',
        data: $('#detailForm').serialize(),
        success: function(data){
            if(data.success){
                $('#mode_group').modal('hide');
                layer.msg("保存成功",{icon: 1});
                reloadList(1);
            }else{
                layer.msg("保存失败:" + data.msg,{icon: 2})
            }
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })
}

//细则
function rule(id){
    location.href = '/car/price/rule?groupId=' + id;
}

//查看企业
function apply(id){
     location.href = '/car/price/apply?groupId=' + id;
}

