$(function () {
    reloadList(1);

    $(".partner").click(function () {
        TR.select('partner', {
            type: 0
        }, function (result) {
            $("[name='cName']").val(result.name);
            $('#partnerId').val(result.id)

            reloadList(1);
        });
    });
})

function reloadList(pageIndex) {
    if ($('#partnerId').val() == '' && $('#groupId').val() == '') {
        return;
    }

    $.ajax({
        url: '/car/price/applyList',
        type: 'post',
        data: {
            groupId: $('#groupId').val(),
            partnerId: $('#partnerId').val(),
            pageIndex: pageIndex
        }, success: function (data) {
            $('#apply_table').html(data);
        }, error: function () {
            layer.msg("系统异常", {icon: 2})
        }
    })
}

//修改
function addApply(id, groupName) {
    $.ajax({
        url: '/car/price/addApply',
        type: 'post',
        data: {
            id: id,
            groupName: groupName
        }, success: function (data) {
            $('#apply_detail').html(data);
            $('#apply_detail').show();
            $('#mode_apply').modal();
        }, error: function () {
            layer.msg("系统异常", {icon: 2})
        }
    })
}

//删除
function delApply(id) {
    layer.confirm("确认删除?", function () {
        $.ajax({
            url: '/car/price/delApply',
            type: 'post',
            data: {
                id: id
            }, success: function (data) {
                if (data.success) {
                    layer.msg("删除成功", {icon: 1});
                    reloadList(1);
                } else {
                    layer.msg("删除失败:" + data.msg, {icon: 2})
                }
            }, error: function () {
                layer.msg("系统异常", {icon: 2})
            }
        })
    })
}

//企业分配价格策略组
function applyGroup() {
    $.ajax({
        url: '/car/price/addApplyGroup',
        type: 'post',
        data: {
            partnerId: $('#partnerId').val(),
            partnerName: $('.partner').val()
        }, success: function (data) {
            $('#apply_group_detail').html(data);
            $('#apply_group_detail').show();
            $('#mode_apply_group').modal();
        }, error: function () {
            layer.msg("系统异常", {icon: 2})
        }
    })
}

//保存
function saveApply() {
    if($('#startTime').val() != '' && $('#endTime').val() != '' && $('#startTime').val() > $('#endTime').val()){
        layer.msg("有效期范围错误",{icon: 0})
        return;
    }

    $.ajax({
        url: '/car/price/saveApply',
        type: 'post',
        data: $('#detailForm').serialize(),
        success: function (data) {
            if (data.success) {
                $('#mode_apply').modal('hide');
                layer.msg("保存成功", {icon: 1});
                reloadList(1);
            } else {
                layer.msg("保存失败:" + data.msg, {icon: 2})
            }
        }, error: function () {
            layer.msg("系统异常", {icon: 2})
        }
    })
}

//保存分配
function saveApplyGroup() {
    var checkedNum = $("#applyGroupTable input[name='ids']:checked").length;
    if (checkedNum == 0) {
        $.alert("请选择你要保存的策略!", "提示");
        return false;
    }

    var ids = new Array();
    $("#applyGroupTable input[name='ids']:checked").each(function () {
        ids.push($(this).val());
    });

    var forms = new Array();
    for (var i = 0; i < ids.length; i++) {
        var form = {};
        form.groupId = ids[i];
        form.startTime = $('.start_time[groupId=' + form.groupId +']').val();
        form.endTime = $('.end_time[groupId=' + form.groupId +']').val();
        form.priority = $('.priority[groupId=' + form.groupId +']').val().trim();
        form.partnerId = $('#partnerId').val();
        form.partnerName = $('.partner').val();
        forms.push(form);

        if(form.priority == ''){
            layer.msg("优先级必填",{icon: 0});
            $('.priority[groupId=' + form.groupId +']').focus();
            return;
        }
        if(form.startTime != '' && form.endTime != '' && form.startTime > form.endTime){
            layer.msg("有效期范围错误",{icon: 0});
            $('.start_time[groupId=' + form.groupId +']').focus();
            return;
        }
    }

    $.ajax({
        url: '/car/price/saveApplyGroup',
        type: 'post',
        data: $.param({forms: JSON.stringify(forms)}),
        success: function(data){
            if(data.success){
                $('#mode_apply_group').modal('hide');
                layer.msg("保存成功", {icon: 1});
                reloadList(1);
            }
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })

}