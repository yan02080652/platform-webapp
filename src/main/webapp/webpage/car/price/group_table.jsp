<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>


<table class="table table-bordered" id="">
    <thead>
    <tr>
        <th style="width: 25%">策略组名称 </th>
        <th style="width: 25%">适用企业</th>
        <th style="width: 25%">修改时间</th>
        <th style="width: 25%">操作</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="bean">
        <tr>
            <td>${bean.name }</td>
            <td>${bean.applyName}
                <a class="" onclick="apply(${bean.id})">查看</a>
            </td>
            <td><fmt:formatDate value="${bean.updateTime}" pattern="yyyy-MM-dd HH:mm:ss" /> </td>
            <td class="text-center">
                <a class="rule" onclick="rule(${bean.id})">细则</a>
                <a class="detail" onclick="addGroup(${bean.id})">修改</a>
                <a class="delete text-danger" onclick="delGroup(${bean.id})">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="pagin" style="margin-bottom: 20px">
    <button type="button" class="pull-left btn btn-default btn-primary" onclick="addGroup()">新增价格策略组</button>
    <%--<button type="button" class="pull-left btn btn-default btn-primary" onclick="applyGroup()" style="margin-left: 20px;">分配价格策略组</button>--%>
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadList"></jsp:include>
</div>