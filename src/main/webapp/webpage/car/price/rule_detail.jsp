<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<div class="modal fade" id="mode_rule" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <c:choose>
                    <c:when test="${empty rule.id}">
                        <h4 class="modal-title">${rule.channelCode}-新增策略</h4>
                    </c:when>
                    <c:otherwise>
                        <h4 class="modal-title">${rule.channelCode}-修改策略</h4>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="modal-body">
                <form:form modelAttribute="rule" class="form-horizontal" id="detailForm" role="form">
                    <form:hidden path="id"/>
                    <form:hidden path="channelCode" />
                    <form:hidden path="groupId" />
                    <div class="form-group">
                        <form:label path="" cssClass="col-sm-2 control-label">出发城市</form:label>
                        <div class="col-sm-9">
                            <form:input path="cityId" class="form-control" placeholder=""/>
                        </div>
                        <form:hidden path="cityName" />
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">供应商</label>
                        <div class="col-sm-10">
                            <c:forEach items="${supplierType}" var="bean" varStatus="status">
                                <label class="checkbox-inline">
                                    <form:checkbox path="supplierCode" value="${bean}" label="${bean.message}" />
                                </label>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">用车类型</label>
                        <div class="col-sm-10">
                            <c:forEach items="${carType}" var="bean" varStatus="status">
                                <label class="checkbox-inline">
                                    <form:checkbox path="productId" value="${bean.code}" label="${bean.message}" />
                                </label>
                            </c:forEach>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">距离/公里</label>
                        <div class="col-sm-2">
                            <form:select path="distanceOp" cssClass="form-control">
                                <form:option value="+">大于等于</form:option>
                                <form:option value="-">小于等于</form:option>
                            </form:select>
                        </div>
                        <div class="col-sm-2">
                            <form:input path="distance" class="form-control" placeholder=""/>
                        </div>
                        <label class="col-sm-1 control-label">报价/元</label>
                        <div class="col-sm-2">
                            <form:select path="priceOp" cssClass="form-control">
                                <form:option value="+">大于等于</form:option>
                                <form:option value="-">小于等于</form:option>
                            </form:select>
                        </div>
                        <div class="col-sm-2">
                            <form:input path="estPrice" class="form-control" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">干预阶段</label>
                        <div class="col-sm-1">
                            <label class="checkbox-inline">
                                <form:checkbox path="interventionType" label="报价" value="0" />
                            </label>
                        </div>
                        <label class="col-sm-2 control-label">调整百分比/%</label>
                        <div class="col-sm-2">
                            <form:input path="estPercent" class="form-control" placeholder=""/>
                        </div>
                        <label class="col-sm-2 control-label">调整金额/元</label>
                        <div class="col-sm-2">
                            <form:input path="estAmount" class="form-control" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-1 col-sm-offset-2">
                            <label class="checkbox-inline">
                                <form:checkbox path="interventionType" label="结算" value="1" />
                            </label>
                        </div>
                        <label class="col-sm-2 control-label">调整百分比/%</label>
                        <div class="col-sm-2">
                            <form:input path="settlePercent" class="form-control" placeholder=""/>
                        </div>
                        <label class="col-sm-2 control-label">调整金额/元</label>
                        <div class="col-sm-2">
                            <form:input path="settleAmount" class="form-control" placeholder=""/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">状态</label>
                        <div class="col-sm-4">
                            <label class="radio-inline">
                                <form:radiobutton value="1" label="启用" path="status" checked="true"/>
                            </label>
                            <label class="radio-inline">
                                <form:radiobutton value="0" label="禁用" path="status" />
                            </label>
                        </div>
                        <label class="col-sm-1 control-label">优先级</label>
                        <div class="col-sm-4">
                            <form:input path="priority" class="form-control" placeholder=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="col-sm-4 control-label">生效日期</label>
                            <div class="col-sm-4">
                                <form:input path="startDate" class="form-control" placeholder="开始日期" readonly="true"/>
                            </div>
                            <div class="col-sm-4">
                                <form:input path="endDate" class="form-control" placeholder="结束日期" readonly="true"/>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <label class="col-sm-4 control-label">生效时间</label>
                            <div class="col-sm-4">
                                <form:input path="startTime" class="form-control" placeholder="开始时间" readonly="true"/>
                            </div>
                            <div class="col-sm-4">
                                <form:input path="endTime" class="form-control" placeholder="结束时间" readonly="true"/>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="saveRule('${rule.channelCode}')">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        jeDate("#startDate",{
            format:"YYYY-MM-DD",
            isTime:false,
            minDate:"2018-09-01",
            isClear:false,
            isToday:false,
            onClose: false,
        })
        jeDate("#endDate",{
            format:"YYYY-MM-DD",
            isTime:false,
            minDate:"2018-09-01",
            isClear:false,
            isToday:false,
            onClose: false,
        })
        jeDate("#startTime",{
            format:"hh:mm",
            isTime:false,
            minDate:"2018-09-01",
            isClear:false,
            isToday:false,
            onClose: false,
        })
        jeDate("#endTime",{
            format:"hh:mm",
            isTime:false,
            minDate:"2018-09-01",
            isClear:false,
            isToday:false,
            onClose: false,
        })
    })
</script>