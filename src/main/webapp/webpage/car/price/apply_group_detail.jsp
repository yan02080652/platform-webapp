<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<div class="modal fade" id="mode_apply_group" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:50%;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">分配企业适用的用车价格策略组</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="detailForm" role="form">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">企业名称</label>
                        <div class="col-sm-9">
                            <label class="control-label">${partnerName}</label>
                        </div>
                    </div>

                    <jsp:include page="apply_group_detail_table.jsp" />

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="saveApplyGroup()">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            </div>
        </div>
    </div>
</div>