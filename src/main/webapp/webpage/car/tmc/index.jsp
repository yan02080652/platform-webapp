<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.tem.platform.security.authorize.PermissionUtil"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<link rel="stylesheet" href="${baseRes }/plugins/sco/css/scojs.css?version=${globalVersion}">
<link rel="stylesheet" href="webpage/flight/tmc/css/tmc.css?version=${globalVersion}">
<%-- 只有客服经理能看到全部任务列表 --%>
<% session.setAttribute("jurisdiction", PermissionUtil.checkTrans("COMMON_TMC_SERVICE", "HOTEL_M"));   %>

<ul id="myTab" class="nav nav-tabs" data-trigger="tab">
	<%--<li><a onclick="getMyTask()">我的任务</a></li>--%>
	<%--<c:if test="${sessionScope.jurisdiction ==0}">--%>
		<%--<li><a onclick="getAllTask()">全部任务</a></li>--%>
	<%--</c:if>--%>

	<li><a onclick="getOrderList()">订单查询</a></li>
</ul>
<div class="pane-wrapper">
	<%--<!-- 我的任务 -->--%>
	<%--<div id="myTask">--%>
		<%--<jsp:include page="myTask.jsp"></jsp:include>--%>
	<%--</div>--%>
	<%--<!-- 全部任务 -->--%>
	<%--<c:if test="${sessionScope.jurisdiction ==0}">--%>
		<%--<div id="allTask">--%>
		<%--</div>--%>
	<%--</c:if>--%>

	<!-- 订单查询 -->
	<div id="orderList">
	</div>

	<%--用来判断上次刷新前是选择的那个tab页，很繁琐，无奈之举 --%>
	<input type="hidden" id="myTask_pageIndex"/>
	<input type="hidden" id="myTask_task_status"/>
	<input type="hidden" id="myTask_task_type"/>
	<input type="hidden" id="allTask_pageIndex"/>
	<input type="hidden" id="allTask_task_status"/>
	<input type="hidden" id="allTask_task_type"/>
	<input type="hidden" id="is_flush" value="true"/>
</div>
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 
<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tab.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tooltip.js?version=${globalVersion}"></script>
<script src="webpage/car/tmc/js/index.js?version=${globalVersion}"></script>


<script type="text/javascript">
//	getMyTask();
getOrderList();
</script>