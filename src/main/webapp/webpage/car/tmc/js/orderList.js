$(function(){
    submitForm(1);
})

//分页函数
function reloadOrderList(pageIndex){
    submitForm(pageIndex,$("#orderTable").find("li[class='active']").find("span").attr("data-value"));
}

//重置查询条件
function resetSearchForm(){
    $('#searchForm')[0].reset();
    $("#orderStatus li").each(function(){
        $(this).removeClass("active");
    })
    // 再次请求
    submitForm(1);
}

function submitForm(pageIndex,showStatus){

    var queryData = {};
    var dateType = $("#searchField1").val();
    var startDate = $("#car_startDate").val();
    var endDate = $("#car_endDate").val();
    if(dateType == 'CREATE_ORDER_DATE'){//下单时间
        queryData.createFromDate = startDate == '' ? null : startDate;
        queryData.createEndDate = endDate == '' ? null : endDate;
    }else{//出行时间
        queryData.startDate = startDate;
        queryData.endDate = endDate;
    }
    queryData.keyword = $('#car_keyword').val()
    queryData.cname = $('#car_cName').val()

    var index = layer.load(0);

    $.ajax({
        url: "car/tmc/orderTable",
        type: "post",
        data: {
            createFromDate: queryData.createFromDate,
            createEndDate: queryData.createEndDate,
            startDate: queryData.startDate,
            endDate: queryData.endDate,
            keyword: queryData.keyword,
            cname: queryData.cname,
            showStatus: showStatus,
            pageIndex: pageIndex,
            pageSize: 10
        },
        success:function(data){
            layer.close(index);
            $("#orderTable").html(data);
        },
        error:function(){
            layer.close(index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }
    });

}