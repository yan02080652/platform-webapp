//我的任务定时器
var timerFlag ;
//全部任务定时器
var allTask_timer;

$(function(){

})

//订单列表Tab
function getOrderList(){
    clearInterval(timerFlag);
    clearInterval(allTask_timer);
    var load_index = layer.load();
    $.ajax({
        type:"post",
        url:"car/tmc/orderList",
        data:{},
        success:function(data){
            layer.close(load_index);
            $("#orderList").html(data);
        },
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }

    });
}

//明细
function loadIncomeDetail(orderId){

    layer.open({
        type: 2,
        area: ['600px', '400px'],
        title: "收入明细",
        closeBtn: 1,
        shadeClose: true,
        // shade :0,
        content: "car/tmc/getIncomeDetail?orderId="+orderId,
    });
}

//订单操作日志
function logList(orderId){
    layer.open({
        type: 2,
        area: ['800px', '500px'],
        title: "订单日志",
        closeBtn: 1,
        shadeClose: true,
        // shade :0,
        content: "car/tmc/getOrderLog?orderId="+orderId,
    });
}