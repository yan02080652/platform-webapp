<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!-- 搜索条件 -->
<style>
	#orderTable .tab li {
		height: 40px;
		line-height: 40px;
		width: 90px;
		color: #3e3a3d;
		float: left;
		text-align:center;
		cursor: pointer;
	}
	#orderTable .tab li:nth-child(4) {
		height: 40px;
		line-height: 40px;
		width: 110px;
		color: #3e3a3d;
		float: left;
		text-align:center;
		cursor: pointer;
	}
</style>
<div>
	<form class="form-inline" id="searchForm">
			<div class=" form-group">
				<select class="form-control" id="searchField1" >
					<option value="CREATE_ORDER_DATE">下单时间</option>
					<option value="TAKE_OFF_DATE">出行时间</option>
				</select>
				<input  id="car_startDate" class="form-control  Wdate" type="text" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'car_endDate\')}'})" style="width: 120px"/> --
				<input  id="car_endDate" class="form-control  Wdate" type="text" onclick="WdatePicker({minDate:'#F{$dp.$D(\'car_startDate\')}'})" style="width: 120px"/>
				<input  id="car_keyword" type="text" class="form-control" style="width: 139px;"  placeholder="姓名/手机号/订单号/供应单号" />
				<input  id="car_cName" type="text" class="form-control" placeholder="企业名称" />
				<button type="button" class="btn btn-default" onclick="submitForm()">搜索</button>
				<button type="button" class="btn btn-default" onclick="resetSearchForm()">重置</button>
			</div>
	</form>
</div>

<div style="margin-top: 10px" id="orderTable">

</div>
<script src="webpage/car/tmc/js/orderList.js?version=${globalVersion}"></script>