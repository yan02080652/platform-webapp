<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>


<!-- 订单状态选项 -->
<%--<div class="tab" style="margin-bottom: 5px">--%>
<%--<ul class="fix" id="orderStatus">--%>
<%--<c:forEach items="${orderStatuslist }" var="orderStatus">--%>
<%--<c:choose>--%>
<%--<c:when test="${orderStatus.orderShowStatus eq currentOrderStatus }">--%>
<%--<li class="active"><span--%>
<%--data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.status }(${orderStatus.count })</span>--%>
<%--</li>--%>
<%--</c:when>--%>
<%--<c:otherwise>--%>
<%--<li><span--%>
<%--data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.status }(${orderStatus.count })</span>--%>
<%--</li>--%>
<%--</c:otherwise>--%>
<%--</c:choose>--%>
<%--</c:forEach>--%>
<%--</ul>--%>
<%--</div>--%>

<!-- 订单列表 -->

<table class="table table-bordered" id="orderTable">
    <thead>
    <tr>
        <th style="width: 20%">用车信息</th>
        <th style="width: 20%">乘车人信息</th>
        <th style="width: 20%">收支信息</th>
        <th style="width: 20%">处理信息</th>
        <th style="width: 20%">供应信息</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach items="${pageList.list }" var="carOrder">
        <tr>
            <td colspan="5">
                <span>企业：${carOrder.cName } </span>
                <span>预订人：${carOrder.userName }</span>
                <span>订单号：<font color="blue">
                    <a href="/car/order/detail/${carOrder.id }" target="_Blank">${carOrder.id }</a>
                </font></span>
                <span>来源:<font color="blue">${carOrder.orderOriginType.message}</font></span>
                <span>标签：${carOrder.orderShowStatus.message }</span>
                <span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${carOrder.createDate }"/></span>
                <span><a onclick="logList('${carOrder.id}')">日志</a></span>
            </td>
        </tr>
        <tr>
            <td>
                <span>${carOrder.summary}</span>
                <br/>
                <span><fmt:formatDate value="${carOrder.trip.carUseTime }" pattern="yyyy-MM-dd HH:mm"/></span>
            </td>
            <td>
                <span>
                        ${carOrder.passengers}
                </span>
            </td>
            <td>
                <span>应收￥${carOrder.totalAmount }</span>
							<span>
								<c:choose>
                                    <c:when test="${carOrder.paymentStatus eq 'PAYMENT_SUCCESS'}">已收</c:when>
                                    <c:otherwise>未收</c:otherwise>
                                </c:choose>
							</span><br/>
                <%--<span>应付￥${carOrder.totalSettlePrice }</span>--%>
                <%--<span>--%>
                    <%--<c:choose>--%>
                        <%--<c:when test="${carOrder.supPay}">已付</c:when>--%>
                        <%--<c:otherwise>未付</c:otherwise>--%>
                    <%--</c:choose>--%>
                <%--</span><br/>--%>
                <c:if test="${not empty carOrder.paymentPlanNo }">
                    <span><a onclick="loadIncomeDetail('${carOrder.id}')">明细</a></span>
                </c:if>
            </td>
            <td>
                    <span>
                        ${carOrder.taskInfo}<br/>
                        ${carOrder.orderShowStatus.message } - ${carOrder.orderStatus.message }
                    </span><br/>
                    <span>
                        <c:if test="${carOrder.processTag eq 'SYSTEMPROCESSING' }"><a
                                onclick="receive()">客服介入</a></c:if>
                        <c:if test="${carOrder.processTag eq 'CUSTOMERSERVICEPROCESSING' }">${carOrder.orderTaskDto.operatorName }</c:if>
                    </span>
            </td>
            <td>
                <c:if test="${carOrder.operationLog.status eq 'FAIL' }">
                    <a data-trigger="tooltip" data-content="失败原因：${carOrder.operationLog.description }">
                        <font color="red">${carOrder.operationLog.type.message }${carOrder.operationLog.status.message }</font></a><br/>
                </c:if>

                <c:if test="${not empty carOrder.outOrderId }">
                    <a data-trigger="tooltip" data-content="${carOrder.outOrderId }">订单号</a><br/>
                    状态: ${carOrder.outOrderStatus.message } <br/>
                </c:if>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin" style="margin-bottom: 20px">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadOrderList"></jsp:include>
</div>

<script>
    $("#orderStatus li").click(function () {
        submitForm(null, $(this).find("span").attr("data-value"));
    });
</script>