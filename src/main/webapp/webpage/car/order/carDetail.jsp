<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" href="${baseRes}/css/car/carDetail.css">

<div class="car-detail-wrapper">
    <div class="car-info">
        <div class="car-title">用车&nbsp;&nbsp;|&nbsp;&nbsp;${order.order.trip.productType}</div>
        <div class="car-msg">
            <div class="msg-left">
                <div class="msg-travel">
                    <ul>
                        <li class="ell">差旅计划：${order.order.travelPlanNo}</li>
                        <li class="ell">出行性质：${order.order.travelType.msg}</li>
                        <li class="ell">出行类别：${order.order.travelName }</li>
                        <li class="ell">出行事由：${order.order.remark }</li>
                        <li class="ell">费用归属：${order.order.costUnitName}</li>
                        <li class="ell">预定人员：${order.order.passengers}</li>
                    </ul>
                </div>
                <div class="msg-order">
                    <ul>
                        <li class="ell">订单号&nbsp;&nbsp;&nbsp;：${order.order.id}</li>
                        <li class="ell">下单时间：<fmt:formatDate value="${order.order.createDate}" pattern="yyyy-MM-dd HH:mm"/></li>
                        <li class="ell">支付时间：<fmt:formatDate value="${order.order.paymentDate}" pattern="yyyy-MM-dd HH:mm"/></li>
                        <li class="ell">支付方式：${order.order.paymentMethod.message}</li>
                        <li class="ell">预估价格：¥&nbsp;${order.order.estimatePrice}元</li>
                    </ul>
                </div>
                <div class="msg-contact">
                    <ul>
                        <li>联系人：${order.order.contactorName}</li>
                        <li>手机&nbsp;&nbsp;&nbsp;：${order.order.contactorMobile}</li>
                    </ul>
                </div>
            </div>
            <div class="msg-btn">
                <p>${order.order.orderShowStatus.message}</p>
                <span class="btn disabled">
                    <c:if test="${order.order.cancelAble}">
                        可以取消
                    </c:if>
                    <c:if test="${!order.order.cancelAble}">
                        不可取消
                    </c:if>
                </span>
            </div>
        </div>
    </div>
    <div class="travel-info">
        <div class="travel-content">
            <div class="travel-img">
                <div class="icon"><img src="${empty order.order.trip.driverPhoto ? '/resource/image/carDriver.jpg' : order.order.trip.driverPhoto}" alt="" width="36"></div>
                <c:choose>
                    <c:when test="${empty order.order.receiveTime}">
                        无司机信息
                    </c:when>
                    <c:otherwise>
                        <ul>
                            <li>${order.order.trip.driverName}&nbsp;&nbsp;&nbsp;${order.order.trip.driverTel}</li>
                            <li>${order.order.trip.vehicleBrand}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${order.order.trip.vehicleNo}</li>
                            <li>颜色&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${empty order.order.trip.vehicleColor ? '未知' : order.order.trip.vehicleColor}</li>
                        </ul>
                    </c:otherwise>
                </c:choose>

            </div>
            <div class="travel-detail">
                <p class="title"><fmt:formatDate value="${order.order.trip.carUseTime}" pattern="yyyy-MM-dd E"/> &nbsp;预计时长${order.order.trip.actualTimeLength}分钟，行驶${order.order.trip.drivingKilometres}公里</p>
                <ul>
                    <li>
                        <span class="time"><fmt:formatDate value="${order.order.trip.carUseTime}" pattern="HH:mm"/></span>&nbsp;出发<span class="place">${order.order.trip.startName}</span>&nbsp;
                        <span class="place-detail">${order.order.trip.startAddress}</span>
                    </li>
                    <li>
                        <span class="time"><fmt:formatDate value="${order.order.trip.carEndTime}" pattern="HH:mm"/></span>&nbsp;到达<span class="place">${order.order.trip.endName}</span>&nbsp;
                        <span class="place-detail">${order.order.trip.endAddress}</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="money-info">
        <div class="money-info-content">
            <p class="title">金额明细</p>
            <div class="title-box">
                <ul>
                    <li class="col-1">里程费</li>
                    <li class="col-2">等待时长费</li>
                    <li class="col-1">远途费</li>
                    <li class="col-2">动态加价费</li>
                    <li class="col-2">预定服务费</li>
                    <li class="col-1">取消罚金</li>
                    <li class="col-2">总金额</li>
                    <li class="col-4">取消规则</li>
                </ul>
            </div>
            <div class="money-box">
                <c:forEach items="${order.feeDetail}" var="bean" >
                    <c:if test="${bean.type eq 'Mileage'}">
                        <c:set var="Mileage" value="CNY&nbsp;${bean.amount}" />
                    </c:if>
                    <c:if test="${bean.type eq 'Time_Wait'}">
                        <c:set var="Time_Wait" value="CNY&nbsp;${bean.amount}" />
                    </c:if>
                    <c:if test="${bean.type eq 'Faraway'}">
                        <c:set var="Faraway" value="CNY&nbsp;${bean.amount}" />
                    </c:if>
                    <c:if test="${bean.type eq 'Dynamic_Increase'}">
                        <c:set var="Dynamic_Increase" value="CNY&nbsp;${bean.amount}" />
                    </c:if>
                    <c:if test="${bean.type eq 'Car_Service'}">
                        <c:set var="Car_Service" value="CNY&nbsp;${bean.amount}" />
                    </c:if>
                    <c:if test="${bean.type eq 'CANCEL_FINE'}">
                        <c:set var="CANCEL_FINE" value="CNY&nbsp;${bean.amount}" />
                    </c:if>
                </c:forEach>
                <ul>
                    <li class="col-1">${empty Mileage ? '/' : Mileage}</li>
                    <li class="col-2">${empty Time_Wait ? '/' : Time_Wait}</li>
                    <li class="col-1">${empty Faraway ? '/' : Faraway}</li>
                    <li class="col-2">${empty Dynamic_Increase ? '/' : Dynamic_Increase}</li>
                    <li class="col-2">${empty Car_Service ? '/' : Car_Service}</li>
                    <li class="col-1">${empty CANCEL_FINE ? '/' : CANCEL_FINE}</li>
                    <li class="col-2 money">
                        <c:choose>
                            <c:when test="${empty order.order.totalAmount}">
                                /
                            </c:when>
                            <c:otherwise>
                                CNY&nbsp;${order.order.totalAmount}
                            </c:otherwise>
                        </c:choose>
                        </li>
                    <li class="col-4">
                        <p>1、司机接单后开始计算时间，3分钟内可以免费取消订单，司机接单后超过3分钟，需支付<span>5元违约金</span></p>
                        <p>2、司机在开始服务后订单<span>不可取消</span></p>
                        <p>3、订单不支持修改</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="provider-info">
        <p class="title">供应商信息</p>
        <p class="detail">
            <span class="left">供应商结算价&nbsp;&nbsp;&nbsp;&nbsp;
                <c:choose>
                    <c:when test="${empty order.order.totalSettlePrice}">
                        未知
                    </c:when>
                    <c:otherwise>
                        CNY${order.order.totalSettlePrice}
                    </c:otherwise>
                </c:choose>
            </span>
            <span class="right">订单号&nbsp;&nbsp;&nbsp;&nbsp;${order.order.outOrderId}</span>
        </p>
    </div>
</div>
