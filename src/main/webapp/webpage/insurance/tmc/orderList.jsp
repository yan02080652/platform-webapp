<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!-- 搜索条件 -->
<style>
#orderTable .tab li {
	height: 40px;
	line-height: 40px;
	width: 110px;
	color: #3e3a3d;
	float: left;
	text-align:center;
	cursor: pointer;
}
</style>
<div>
	<form class="form-inline" id="searchForm">
			<div class=" form-group">
				<input  id="searchField1" type="text" class="form-control" style="width: 150px;" placeholder="保险所属订单号" />
				<input  id="searchField2" type="text" class="form-control" style="width: 250px;" placeholder="联系人/预订人手机，姓名，邮箱" />
				<input  id="searchField3" type="text" class="form-control" placeholder="保险订单号" />
				<input  id="searchField4" type="text" class="form-control" placeholder="企业名称/TMC" />
				<input  id="searchField5" type="text" class="form-control" placeholder="航班号" />
				<button type="button" class="btn btn-default" onclick="submitForm()">搜索</button>
				<button type="button" class="btn btn-default" onclick="resetSearchForm()">重置</button>
			</div>
	</form>
</div>

<div style="margin-top: 10px" id="orderTable">
	<%@ include file="orderTable.jsp" %>	
</div>
