<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="/webpage/flight/tmc/css/reset.css?version=${globalVersion}" type="text/css">
<link rel="stylesheet" href="/webpage/flight/tmc/css/orderDetail.css?version=${globalVersion}" type="text/css">
<style>
.row {
  margin-top: 30px;
  margin-bottom: 30px;
}
body{
	font-family: "Microsoft YaHei",tahoma,arial,Hiragino Sans GB;
}
.optBtn{
	border-radius:0px;
	border:none;
	width: 120px;
	background-color: #46A1BB;
	color: #FFFFFF;
}

</style>
<div class="container">
	<div class="row">
		<label>保险名称：</label>
		<span>
			<c:forEach items="${nameMap[insuranceOrder.productId] }" var="name" varStatus="vs">
				<c:choose>
					<c:when test="${vs.first }"><span></span></c:when>
					<c:when test="${vs.last }"><span>${name }</span></c:when>
					<c:otherwise>${name}-</c:otherwise>
				</c:choose>
			</c:forEach>
		</span>
		<span style="margin-left: 30px;">投保渠道：</span>
		<span>${insuranceOrder.hubName }</span>
		<span style="margin-left: 30px;">供应商订单号：</span>
		<span>${insuranceOrder.id }</span>
	</div>
	<div class="row">
		<label class="pull-left" style="height:56px">保险说明：</label>
		<div>${descripeMap[insuranceOrder.productId] }</div>
	</div>
	<div class="row">
		<label>航班信息：</label>
		<span>${flightOrder.orderFlights[0].carrierName }</span>
		<span>${flightOrder.orderFlights[0].flightNo }</span>
		<span style="margin-left: 30px;"><fmt:formatDate value="${flightOrder.orderFlights[0].fromDate }" pattern="yyyy.MM.dd HH:mm" />起飞</span>&nbsp;
		<span style="margin-left: 30px;">${flightOrder.orderFlights[0].fromCityName }</span>
		<span>${flightOrder.orderFlights[0].fromAirportName }</span>
		<span>(${flightOrder.orderFlights[0].fromCity })</span>-
		<span>${flightOrder.orderFlights[0].toCityName }</span>
		<span>${flightOrder.orderFlights[0].toAirportName }</span>
		<span>(${flightOrder.orderFlights[0].toCity })</span>
	</div>	
	<div class="row">	
		 	<div class="form-table">
				<table class="table table-bordered">
						<tr>
							<th style="text-align:center;background-color: #999999;color: #FFFFFF">姓名</th>
							<th style="text-align:center;background-color: #999999;color: #FFFFFF">证件</th>
							<th style="text-align:center;background-color: #999999;color: #FFFFFF">保单号</th>
							<th style="text-align:center;background-color: #999999;color: #FFFFFF">处理结果</th>
						</tr>
						<c:forEach items="${insuranceOrder.insuranceOrderDetails }" var="detail">
							<tr class="refundTable">
								<td style="text-align:center;">${detail.insuredName }</td>
								<input name="userId" value="${detail.insuredId }" type="hidden">
								<td style="text-align:center;">${detail.idType.message}:${detail.idNo }</td>
								<td>${detail.policyNo }</td>
								<td>
									<select name="result">
										<option value="3">成功撤保</option>
										<option value="4">拒绝撤保</option>
									</select>
								</td>
							</tr>
						</c:forEach>
						
					</table>
					<div class="hoc-button-wards">
				
					</div>
				</div >
		</div>
		<div class="row">	
			<form class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="rejectRefundReason">拒绝撤保原因：</label>
					<div class="col-sm-8">
						<input type="text" name="rejectRefundReason" id="rejectRefundReason" class="form-control"/>
					</div>
				</div>
				<div class="form-group">
		            <button type="button" class="btn btn-default btn-primary optBtn" style="margin-left: 200px;" onclick="submitTicketFrom()">完成</button>
		            <button type="button" class="btn btn-default optBtn" data-dismiss="modal" style="background-color: #FFFFFF;color:#46A1BB;border:1px solid #46A1BB; margin-left: 120px;" onclick="closeWindow()">放弃</button>
		        </div>
			</form>
		</div>
</div>
<input id="orderId" value="${orderId }" type="hidden">
<input id="taskId" value="${taskId }" type="hidden">
<script>
	function closeWindow(){
		var index = parent.layer.getFrameIndex(window.name);  
		parent.layer.close(index);  
	}
	
	//线下处理cheba
	function submitTicketFrom(){
		
		var reason = $("#rejectRefundReason").val();
		if(reason.trim() == '') {
			var hasFail = false;
			$("select[name='result']").each(function(){
				if($(this).val() == '4') {
					hasFail = true;
				}
			});
			if(hasFail) {
				layer.msg("请填写拒绝撤保原因",{icon:2,offset:'t',time:1000});
				return;
			}
		}
		var orderId = $("#orderId").val();
		var taskId = $("#taskId").val();
		
		var forms = new Array();
		
		$(".refundTable").each(function(){
			var userId = $(this).find("input[name='userId']").val();
			var result = $(this).find("select[name='result']").val();
			var form = {
				orderId:orderId,
				taskId:taskId,
				userId:userId,
				result:result,
				taskType:1,
				reson:reason,
			};
			forms.push(form);
		});
			
		$.ajax({
			type:"post",
			url:"insurance/tmc/lineRefundInsurance",
			data:JSON.stringify(forms),
			contentType: "application/json",
			datatype:"json",
			success:function(data){
				
				if(data){
					layer.msg("线下处理撤保完成",{icon:1,offset:'t',time:1000});
					setTimeout(function(){
						closeWindow();
					},500);
					
				}else{
					layer.msg("系统错误，请刷新重试！",{icon:2,offset:'t',time:1000})
				}
			}
		})
		
	}

</script>
