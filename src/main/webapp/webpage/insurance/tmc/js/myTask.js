$(function(){
	$('.tab1').on('click',function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active')
        $('.content1').show();
        $('.content2').hide();
    })
    $('.tab2').on('click',function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active');
        $('.content1').hide();
        $('.content2').show();
    })
    
    clearInterval(timerFlag); 
    //checkTimeoutTask();
	timerFlag = setTimeout(taskTimer,10*1000);
	/*if(isopen){
		$("#switchBtn").text("关闭");
		$("#autoMsg").text("已开启");
	}else{
		$("#switchBtn").text("开启");
		$("#autoMsg").text("已关闭");
	}*/
})

// 检查当前操作人是否为任务领取人
function checkOperator(operatorId){
	var lawful;
	$.ajax({
		url:"insurance/tmc/checkOperator",
		type:"GET",
		async:false,
		data:{operatorId:operatorId},
		success:function(data){
			lawful =  data;
		}
	});
	
	return lawful;
}


function reloadMyTaskTable(pageIndex){
	var taskStatus = $("#myTaskStatusTab .active").attr("data-value");	
	submitAjax(pageIndex,taskStatus);
	
}

function submitAjax(pageIndex,taskStatus,taskType){
	$("#myTask_pageIndex").val(pageIndex);
	$("#myTask_task_status").val(taskStatus);
	$("#myTask_task_type").val(taskType);
    $("#currentTaskType").val(taskType);
	
	$.ajax({
		type:"post",
		url:"insurance/tmc/getMyTaskTable",
		data:{
			taskStatus:taskStatus,
			pageIndex:pageIndex,
			taskType:taskType
		},
		success:function(data){
			$("#myTask").html(data);
        },
		error:function(){
			layer.msg("系统繁忙，请刷新重试",{icon:2,offset:'t',time:1000})
		}
	});
}

//任务交接
//注：checkTrans boolean类型,普通客服任务交接需要判断是否为本人，传true，客服经理通过全部任务列表分配不需要判断,传false
function transfer(taskId,operatorId,checkTrans){
	var lawful = true;
	if(checkTrans){
		lawful = checkOperator(operatorId);
	}
	if(lawful){
		TR.select('user',{
            type: "me"
		},function(data){
			$.ajax({
				type:"post",
				url:"insurance/tmc/transfer",
				data:{
					taskId:taskId,
					operatorId:operatorId,
					userId:data.id,
					userName:data.fullname
				},
				success:function(rs){
					if(rs){
						layer.msg("任务已转交给"+data.fullname,{icon:1,offset:'t',time:1000})
						setTimeout(function(){
							submitAjax();
						},200);
					}else{
						layer.msg("系统错误，请刷新重试！",{icon:2,offset:'t',time:1000})
					}
				}
			});
		});
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
}



// 定时任务
function taskTimer(){
	var pageIndex = $("#myTask_pageIndex").val();
	var taskStatus = $("#myTask_task_status").val();
	var taskType = $("#myTask_task_type").val();

	if( taskStatus==''){
		taskStatus = 'PROCESSING';
	}
	submitAjax(pageIndex,taskStatus,taskType);
}

function delay(taskId,delay,operatorId){
	var lawful = checkOperator(operatorId);
	if(lawful){
		if(delay != -1){
			$.ajax({
				url:"insurance/tmc/updateMaxProcessDate",
				post:"get",
				data:{taskId:taskId,delay:delay},
				success:function(){
					layer.msg("任务延时成功!",{icon:1,offset:'t',time:1000})
					submitAjax();
				}
			});
		}
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
	
}

function dateAdd(time,strInterval, Number) {   
    var dtTmp = time;
    switch (strInterval) {   
        case 's' :return new Date(Date.parse(dtTmp) + (1000 * Number));  
        case 'n' :return new Date(Date.parse(dtTmp) + (60000 * Number));  
        case 'h' :return new Date(Date.parse(dtTmp) + (3600000 * Number));  
        case 'd' :return new Date(Date.parse(dtTmp) + (86400000 * Number));  
        case 'w' :return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number)); 
        case 'q' :return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number*3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
        case 'm' :return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
        case 'y' :return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
    }  
};

Date.prototype.Format = function(fmt) {
    var o = {
         "M+" : this.getMonth() + 1, //月份  
         "d+" : this.getDate(), //日  
         "H+" : this.getHours(), //小时  
         "m+" : this.getMinutes(), //分  
         "s+" : this.getSeconds(), //秒  
         "q+" : Math.floor((this.getMonth() + 3) / 3), //季度  
         "S" : this.getMilliseconds() //毫秒  
    };
    if (/(y+)/.test(fmt)){
         fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for ( var k in o){
         if (new RegExp("(" + k + ")").test(fmt)){
             fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]): (("00" + o[k]).substr(("" + o[k]).length)));
         }
    }
    return fmt;
}
function lineProcessInsurance(orderId,taskId,flag,operatorId){
	var lawful = checkOperator(operatorId);
	if(lawful){
		layer.open({
			type: 2,
			area: ['800px', '600px'],
			title: "线下投保",
			closeBtn: 1,
			shadeClose: false,
			shade :0,
			skin : 'blue-skin',
			border:[10,0.3,'#46A1BB',true],
			content: "insurance/tmc/getInsuranceFailData?orderId="+orderId+"&taskId="+taskId+"&flag="+flag,
		});
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
}

function lineProcessRefundInsurance(obj,orderId,taskId,flag,operatorId){
	var lawful = checkOperator(operatorId);
	var userIds = new Array();
	$(obj).parents(".orderTask").find("input[name='userId']").each(function(){
		userIds.push($(this).val());
	});
	userIds = userIds.join(",");
	if(lawful){
		layer.open({
			type: 2,
			area: ['800px', '600px'],
			title: "待撤保—线下处理",
			closeBtn: 1,
			shadeClose: false,
			shade :0,
			skin : 'blue-skin',
			border:[10,0.3,'#46A1BB',true],
			content: "insurance/tmc/getRefundInsuranceFailData?orderId="+orderId+"&taskId="+taskId+"&flag="+flag+"&userIds="+userIds,
		});
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
}

function aginInsure(orderId,taskId,flag,operatorId){
	var lawful = checkOperator(operatorId);
	if(lawful){
		layer.confirm('你确定要再次投保吗?', function(index) {
			var loadIndex = layer.load();
			$.ajax({
				type:"post",
				url:"insurance/tmc/aginInsure",
				data:{
					orderId:orderId,
					taskId:taskId,
					flag:flag,
				},
				success:function(data){
					layer.close(loadIndex);
					if(data){
						layer.msg("投保成功！",{icon:4,offset:'t',time:1000});
					}else{
						layer.msg("投保失败",{icon:2,offset:'t',time:1000})
					}
					submitAjax();
				},
				error:function(){
					layer.msg("系统错误，请稍后刷新重试！",{icon:2,offset:'t',time:1000});
					layer.close(loadIndex);
				}
			});
			layer.close(index);
		});
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
}

function aginRefundInsure(obj,orderId,taskId,flag,operatorId,refundOrderId){
	var lawful = checkOperator(operatorId);
	if(lawful){
		
		var userIds = new Array();
		$(obj).parents(".orderTask").find("input[name='userId']").each(function(){
			userIds.push($(this).val());
		});
		userIds = userIds.join(",");
		
		layer.confirm('你确定要再次撤保吗?', function(index) {
			var loadIndex = layer.load();
			$.ajax({
				type:"post",
				url:"insurance/tmc/aginRefundInsure",
				data:{
					orderId:orderId,
					userIds:userIds,
					taskId:taskId,
					flag:flag,
					refundOrderId:refundOrderId,
				},
				success:function(data){
					layer.close(loadIndex);
					if(data){
						layer.msg("撤保成功！",{icon:4,offset:'t',time:1000});
					}else{
						layer.msg("撤保失败",{icon:2,offset:'t',time:1000})
					}
					submitAjax();
				},
				error:function(){
					layer.msg("系统错误，请稍后刷新重试！",{icon:2,offset:'t',time:1000});
					layer.close(loadIndex);
				}
			});
			layer.close(index);
		});
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
}



