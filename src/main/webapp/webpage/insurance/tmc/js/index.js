//我的任务定时器
var timerFlag ;
//全部任务定时器
var allTask_timer;


//我的任务Tab
function getMyTask(){
    clearInterval(allTask_timer);
	$.ajax({
		type:"get",
		url:"insurance/tmc/getMyTaskTable",
		data:{},
		success:function(data){
			$("#myTask").html(data);
		}
	});
}
//全部任务Tab
function getAllTask(){
    clearInterval(timerFlag);
	$.ajax({
		type:"get",
		url:"insurance/tmc/allTask",
		data:{},
		success:function(data){
			$("#allTask").html(data);
		}
	});
}
//订单列表Tab
function getOrderList(){
	$.ajax({
		type:"get",
		url:"insurance/tmc/orderList",
		data:{},
		success:function(data){
			$("#orderList").html(data);
		}
	});
}

//代客下单入口
function getSearchUserList(){
	$.ajax({
		type:"get",
		url:"insurance/tmc/searchUserPage",
		data:{},
		success:function(data){
			$("#searchUserPage").html(data);
		}
	});
}

//订单操作日志
function logList(orderId){
	layer.open({
		  type: 2,
		  area: ['800px', '500px'],
		  title: "订单日志",
		  closeBtn: 1,
		  shadeClose: true,
		 // shade :0,
		  content: "insurance/tmc/getOrderLog?orderId="+orderId,
		});
}

//加载收入明细
function loadIncomeDetail(orderId,paymentPlanNo){
	layer.open({
		  type: 2,
		  area: ['600px', '400px'],
		  title: "收入明细",
		  closeBtn: 1,
		  shadeClose: true,
		 // shade :0,
		  content: "insurance/tmc/getIncomeDetail?paymentPlanNo="+paymentPlanNo+"&orderId="+orderId,
		});
}


