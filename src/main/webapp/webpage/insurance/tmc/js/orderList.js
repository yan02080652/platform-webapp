$(function() {
	$("#orderStatus li").click(function() {
		if ($(this).hasClass("active")) {
			//$(this).removeClass("active");
		} else {
			submitForm(null,$(this).find("span").attr("data-value"));
			$(this).addClass("active");
			$(this).siblings().removeClass("active");
		}
	});
})

//重置查询条件
function resetSearchForm(){
	$('#searchForm')[0].reset();
	$("#orderStatus li").each(function(){
		$(this).removeClass("active");
	})
	// 再次请求
	submitForm();
}

//分页函数
function reloadOrderList(pageIndex){
	submitForm(pageIndex);
}

function submitForm(pageIndex,orderStatus){
	var field1 = $("#searchField1").val();
	var field2 = $("#searchField2").val().trim();
	var field3 = $("#searchField3").val().trim();
	var field4 = $("#searchField4").val().trim();
	var field5 = $("#searchField5").val().trim();
	$.ajax({
		type:"post",
		url:"insurance/tmc/getOrderTable",
		data:{	orderId:field1,
				passengerOrContent:field2,
				id:field3,
				partnerName:field4,
				flightNo:field5,
				pageIndex:pageIndex,
				orderStatus:orderStatus,
			},
		success:function(data){
			$("#orderTable").html(data);
		}
	});
}

//TODO 客服介入
function receive(){
	layer.alert("客服介入")
}

