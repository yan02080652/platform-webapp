$(function(){
	$('.tab1').on('click',function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active')
        $('.content1').show();
        $('.content2').hide();
        $('.content3').hide();
    })
    $('.tab2').on('click',function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active');
        $('.content1').hide();
        $('.content2').show();
        $('.content3').hide();
    })
    $('.tab3').on('click',function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active');
        $('.content1').hide();
        $('.content2').hide();
        $('.content3').show();
    })
    
    	//清空定时器
	clearInterval(allTask_timer);
    
    refreshAllTask();
})

function reloadMyTaskTable(pageIndex){
	var taskStatus = $("#allTaskStatusTab .active").attr("data-value");
	reloadAllTask(pageIndex,taskStatus);
	
}

function reloadAllTask(pageIndex,taskStatus,taskType){
    pageIndex = pageIndex ? pageIndex : 1;
	$("#allTask_pageIndex").val(pageIndex);
	$("#allTask_task_status").val(taskStatus);
	$("#allTask_task_type").val(taskType);
    
	//查询条件
	var field1 = $("#search_Field2").val().trim();
	
	if(!taskStatus){
		taskStatus = $("#allTaskStatusTab .active").attr("data-value");
	}
	
	$.ajax({
		type:"post",
		url:"insurance/tmc/allTask",
		data:{
			taskStatus:taskStatus,
			pageIndex:pageIndex,
			taskType:taskType,
			keyword:field1,
		},
		success:function(data){
			$("#allTask").html(data);
			$("#search_Field2").val(field1);
        },
		error:function(){
			layer.msg("系统繁忙，请刷新重试",{icon:2})
		}
	});
}

//重置查询条件
function resetForm(){
	$('#orderSearchForm')[0].reset();
	// 再次请求
	reloadAllTask();
}

//allTask:自动刷新
var allTask_count = 9;
function refreshAllTask() {
	$("#countDown").text(allTask_count);
	allTask_count--  ;
	if(allTask_count >= 0){
		allTask_timer = setTimeout(refreshAllTask, 1000);
	}else{
		var pageIndex = $("#allTask_pageIndex").val();
		var taskStatus = $("#allTask_task_status").val();
		var taskType = $("#allTask_task_type").val();
        if (taskStatus == '') {
            taskStatus = 'WAIT_PROCESS';
        }
		reloadAllTask(pageIndex,taskStatus,taskType);
	}
}

