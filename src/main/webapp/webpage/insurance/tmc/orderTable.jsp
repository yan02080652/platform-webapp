<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
.tooltip {
  width: 24em;
}
</style>

	<div class="tab" style="margin-bottom: 5px">
		<ul class="fix" id="orderStatus">
			<c:forEach items="${OrderStatuslist }" var="orderStatus">
				<c:choose>
					<c:when test="${orderStatus.orderShowStatus eq currentOrderStatus }">
					<li class="active"><span data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.message }(${orderStatus.count })</span></li>
					</c:when>
					<c:otherwise>
						<li ><span data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.message }(${orderStatus.count })</span></li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</div>

	<div style="margin-top: 10px">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th style="width: 24%">保险信息</th>
				<th style="width: 19%">被保人</th>
				<th style="width: 19%">收支信息</th>
				<th style="width: 19%">处理信息</th>
				<th style="width: 19%">供应商信息</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="orderDto" varStatus="vs">
				<tr>
					<td colspan="5">
						<span>企业：${orderDto.partnerName }</span>
						<span>预订人：${orderDto.policyHolderName }</span>
						<span>订单号：
							<font color="blue">
							<c:if test="${fn:startsWith(orderDto.orderId ,'10')}">
								<a href="/order/flightDetail/${orderDto.orderId }" target="_Blank">${orderDto.orderId }</a>
							</c:if>
							<c:if test="${fn:startsWith(orderDto.orderId ,'17')}">
								<a href="/order/inter/orderDetail/${orderDto.orderId  }" target="_Blank">${orderDto.orderId  }</a>
							</c:if>
							</font>
						</span>
						<span>来源：<font color="blue">${orderDto.orderOriginType.message }</font></span>
						<span>标签：${orderDto.status.message }</span>
						<span> 下单时间：<font color="blue">${orderDto.createDate }</font></span>
					    <span><a onclick="logList('${orderDto.id}')">订单日志</a></span>
				    </td>
				</tr>
				<tr>
					<td>
						<c:forEach items="${nameMap[orderDto.productId] }" var="name" varStatus="vs">
							<c:choose>
								<c:when test="${vs.first }"><span></span></c:when>
								<c:when test="${vs.last }"><span>${name }</span></c:when>
								<c:otherwise>${name }-</c:otherwise>
							</c:choose>
						</c:forEach>
					</td>
					<td>
						<span>
							<c:forEach items="${orderDto.insuranceOrderDetails }" var="orderDetail" varStatus="vs">
								<c:if test="${!vs.last }">${orderDetail.insuredName }/</c:if>
								<c:if test="${vs.last }">${orderDetail.insuredName }</c:if>								
								<input type="hidden" value="${orderDetail.insuredId }" name="userId">
							</c:forEach>
						</span>
					</td>
					<td>
						<span>应收￥${orderDto.insuranceOrderDetails[0].salePrice * orderDto.groupSize }</span>
						<span>
							<c:choose>
								<c:when test="${not empty orderDto.paymentPlanNo }">已收</c:when>
								<c:otherwise>未收</c:otherwise>
							</c:choose>
						</span><br/>
						<span>应付￥${orderDto.insuranceOrderDetails[0].salePrice * orderDto.groupSize }</span>
						<c:if test="${not empty orderDto.paymentPlanNo }">
							<span>已付</span><br/>
							<span><a onclick="loadIncomeDetail('${orderDto.id}','${orderDto.paymentPlanNo }')">明细</a></span>
						</c:if>
						<c:if test="${empty orderDto.paymentPlanNo}">
							<span>未付</span>
						</c:if>
					</td>
					
					<td>
						<%-- <span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>
						<span>处理人：${orderTask.operatorName }</span><br/> --%>
						<span>订单状态：${orderDto.status.message }</span><br>
						<span>${orderDto.taskOperateMsg }</span>
					 </td>
					<td>
						<span>${orderDto.hubName }</span>
					</td>
				</tr>
			</c:forEach>		
		</tbody>
	</table>
</div>
<div class="pagin" style="margin-bottom: 20px">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadOrderList"></jsp:include>
</div>
<script src="/webpage/insurance/tmc/js/orderList.js?version=${globalVersion}"></script>
