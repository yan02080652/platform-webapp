<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="/webpage/flight/tmc/css/reset.css?version=${globalVersion}" type="text/css">
<link rel="stylesheet" href="/webpage/flight/tmc/css/orderDetail.css?version=${globalVersion}" type="text/css">
<style>
.row {
  margin-top: 30px;
  margin-bottom: 30px;
}
body{
	font-family: "Microsoft YaHei",tahoma,arial,Hiragino Sans GB;
}
.optBtn{
	border-radius:0px;
	border:none;
	width: 120px;
	background-color: #46A1BB;
	color: #FFFFFF;
}

</style>
<div class="container">
		<div class="row">
			<label>保险名称：</label>
			<span>
				<c:forEach items="${nameMap[insuranceOrder.productId] }" var="name" varStatus="vs">
					<c:choose>
						<c:when test="${vs.first }"><span></span></c:when>
						<c:when test="${vs.last }"><span>${name }</span></c:when>
						<c:otherwise>${name}-</c:otherwise>
					</c:choose>
				</c:forEach>
			</span>
		</div>
		<div class="row">
			<label class="pull-left" style="height:56px">保险说明：</label>
			<div>${descripeMap[insuranceOrder.productId] }</div>
		</div>
		<div class="row">
			<label>航班信息：</label>
			<span>${flightOrder.orderFlights[0].carrierName }</span>
			<span>${flightOrder.orderFlights[0].flightNo }</span>
			<span style="margin-left: 30px;"><fmt:formatDate value="${flightOrder.orderFlights[0].fromDate }" pattern="yyyy.MM.dd HH:mm" />起飞</span>&nbsp;
			<span style="margin-left: 30px;">${flightOrder.orderFlights[0].fromCityName }</span>
			<span>${flightOrder.orderFlights[0].fromAirportName }</span>
			<span>(${flightOrder.orderFlights[0].fromCity })</span>-
			<span>${flightOrder.orderFlights[0].toCityName }</span>
			<span>${flightOrder.orderFlights[0].toAirportName }</span>
			<span>(${flightOrder.orderFlights[0].toCity })</span>
		</div>	
		<div class="row">	
			 	<div class="form-table">
					<table class="table table-bordered">
							<tr>
								<th style="text-align:center;background-color: #999999;color: #FFFFFF">姓名</th>
								<th style="text-align:center;background-color: #999999;color: #FFFFFF">证件</th>
								<th style="text-align:center;background-color: #999999;color: #FFFFFF">处理结果</th>
								<th style="background-color: #999999"> </th>
							</tr>
							<c:forEach items="${insuranceOrder.insuranceOrderDetails }" var="detail">
								<tr class="userTable">
									<td style="text-align: center;">${detail.insuredName }</td>
									<td>${detail.idType.message}:${detail.idNo }</td>
									<input type="hidden" name="userId" value="${detail.insuredId }">
									<td>
										<select name="result" class="result">
											<option value="1">成功投保</option>
											<option value="2">拒单退款</option>
										</select>
									</td>
									<!-- <td class="policyNo">保单号：<input type="text" name="policyNo"></td> -->
									<!-- <td class="reason">据单原因：<input type="text" name="reason"></td> --> 
								</tr>
							</c:forEach>
							
						</table>
						<div class="hoc-button-wards">
					
						</div>
					</div >
			</div>
			<div class="row">
				<label><font color="red">*</font>投保渠道</label>
				<select style="width: 110px;height: 25px" id="channel">
						<option value="">---</option>
					<c:forEach items="${channelList }" var="channel">
						<option value="${channel.id }">${channel.name }</option>
					</c:forEach>
				</select>
				
				<label style="margin-left: 80px;"><font color="red">*</font>应付总价</label>
				<input type="text" style="height: 25px;" name="totalPremium" id="totalPremium">
				
				
				<label style="margin-left: 80px;">供应商订单号</label>
				<input placeholder="外部平台订单号" type="text" style="height: 25px;" name="supplierOrderNo" id="supplierOrderNo">
				
			</div>
				<span style="color: red;margin-left: 60px;" id="channelNotic" hidden>请选择投保渠道</span>
				<span style="color: red;margin-left: 180px;" id="totalPremiumNotic" hidden>请填写应付总价</span>
			<div class="row">	
				<form class="form-horizontal">
					<div class="form-group">
				            <button type="button" class="btn btn-default btn-primary optBtn" style="margin-left: 200px;" onclick="submitTicketFrom()">完成</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				            <button type="button" class="btn btn-default optBtn" data-dismiss="modal" style="background-color: #FFFFFF;color:#46A1BB;border:1px solid #46A1BB;margin-left: 120px;" onclick="closeWindow()">放弃</button>
			         </div>
				</form>
			</div>
</div>
<input type="hidden" id="orderId" value="${orderId }">
<input type="hidden" id="taskId" value="${taskId }">
<script>
	var hasSuccess = true;
	var allFail = true;
$(function(){
	$(".result").each(function(){
		if($(this).val() == '1') {
			$(this).parents(".userTable").append('<td class="policyNo">保单号：<input type="text" name="policyNo"><span style="color: red;" hidden>请填写保单号</span></td>');
		}else{
			$(this).parents(".userTable").append('<td class="reason">拒单原因：<input type="text" name="reason"><span style="color: red;" hidden>请填写拒单原因</span></td>');
		}
		
	});
	
	$(".result").change(function(){
		
		if($(this).val() == '1') {
			$(this).parents(".userTable").find("td").eq(3).remove();
			$(this).parents(".userTable").append('<td class="policyNo">保单号：<input type="text" name="policyNo"><span style="color: red;" hidden>请填写保单号</span></td>');
			hasSuccess = true;
		}else{
			$(this).parents(".userTable").find("td").eq(3).remove();
			$(this).parents(".userTable").append('<td class="reason">拒单原因：<input type="text" name="reason"><span style="color: red;" hidden>请填写拒单原因</span></td>');
		}
		
		var a = true;
		var b = false;
		$(".result").each(function(){
			if($(this).val() == '1') {//有投保成功的
				a = false;
				b = true;
			}
			allFail = a;
			hasSuccess = b;
		});
		
		if(allFail) {
			$("#channel").val("");
			$('#totalPremium').attr("disabled",true);
			$('#supplierOrderNo').attr("disabled",true);
			$('#channel').attr("disabled",true);
			$("#totalPremiumNotic").hide();
			$("#channelNotic").hide();
		}else{
			$('#channel').removeAttr("disabled");
			$('#supplierOrderNo').removeAttr("disabled");
			$('#totalPremium').removeAttr("disabled");
			
		}
		
	});
	
});

function closeWindow(){
	var index = parent.layer.getFrameIndex(window.name);  
	parent.layer.close(index);  
}

//线下处理撤保
function submitTicketFrom(){
	var orderId = $("#orderId").val();
	var taskId = $("#taskId").val();
	var channel = $("#channel").val();
	var totalPremium = $("#totalPremium").val();
	var supplierOrderNo = $("#supplierOrderNo").val();
	var hasEmptyFill = false;
	
	$(".result").each(function(){
		if($(this).parents(".userTable").find("td").eq(3).find("input").val().trim() == '' ) {
			$(this).parents(".userTable").find("td").eq(3).find("input").next().show();
			hasEmptyFill = true;
		}else{
			$(this).parents(".userTable").find("td").eq(3).find("input").next().hide();
		}
	})
	
	
	$("#totalPremiumNotic").hide();
	$("#channelNotic").hide();
	
	if(hasSuccess) {//有投保保单 需要填写投保渠道与总价
		if(channel == '') {
			$("#channelNotic").show();
			hasEmptyFill = true;
		}
		if(totalPremium == '') {
			/* $("#totalPremiumNotic").css("marginLeft","340px"); */
			$("#totalPremiumNotic").show();
			hasEmptyFill = true;
		}
	}
	if(hasEmptyFill) { //有未填写的必填项
		return;
	}
	
	var loadIndex = layer.load();
	
	var forms = new Array();
	$(".userTable").each(function(){
		var userId = $(this).find("input[name='userId']").val();
		var result = $(this).find("select[name='result']").val();
		
		if(result == 1) {
			var policyNo = $(this).find("input[name='policyNo']").val();
			var form = {
				orderId:orderId,
				taskId:taskId,
				userId:userId,
				result:result,
				taskType:0,
				reson:"",
				policyNo:policyNo,
				channel:channel,
				totalPremium:totalPremium,
				supplierOrderNo:supplierOrderNo,
			};
			forms.push(form);
		}
		if(result == 2) {
			var reason = $(this).find("input[name='reason']").val();
			var form = {
				orderId:orderId,
				taskId:taskId,
				userId:userId,
				result:result,
				taskType:0,
				reson:reason,
				policyNo:"",
				channel:"",
				totalPremium:"",
				supplierOrderNo:"",
			};
			forms.push(form);
		}
		
	});
	
	$.ajax({
		type:"post",
		url:"insurance/tmc/lineInsurance",
		data:JSON.stringify(forms),
		contentType: "application/json",
		datatype:"json",
		success:function(data){
			layer.closeAll();
			if(data){
				layer.closeAll();
				layer.msg("线下处理投保完成",{icon:1,offset:'t',time:1000});
				setTimeout(function(){
					closeWindow();
				},500);
				
			}else{
				layer.msg("系统错误，请刷新重试！",{icon:2,offset:'t',time:1000})
			}
		}
	})
	
}



</script>
