<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
.tooltip {
  width: 24em;
}
.blue-skin .layui-layer-title{background:#46A1BB; color:#FFFFF5; border: none;}
</style>
<div>
	<!-- 状态栏 -->
	<div class="tab pull-left">
		<ul class="fix" id="allTaskStatusTab">
			<li class="tab1 active" data-value="WAIT_PROCESS" onclick="reloadAllTask(null ,'WAIT_PROCESS')">待处理(${waitTaskTypeMap.WAIT_PROCESS==null?0:waitTaskTypeMap.WAIT_PROCESS})</li>
			<li class="tab2" data-value="PROCESSING" onclick="reloadAllTask(null,'PROCESSING')">处理中(${processingMap.PROCESSING==null?0:processingMap.PROCESSING})</li>
			<li class="tab3" data-value="ALREADY_PROCESSED" onclick="reloadAllTask(null,'ALREADY_PROCESSED')">近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})</li>
		</ul>
		<div id="i_all_content">
		<div class="content1">
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_INSURE')">待投保(${waitTaskTypeMap.WAIT_INSURE==null?0:waitTaskTypeMap.WAIT_INSURE })</a>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_REFUND_INSURE')">待撤保(${waitTaskTypeMap.WAIT_REFUND_INSURE==null?0:waitTaskTypeMap.WAIT_REFUND_INSURE})</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</p>
		</div>
		<div class="content2">
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_INSURE')">待投保(${processingMap.WAIT_INSURE==null?0:processingMap.WAIT_INSURE })</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_REFUND_INSURE')">待撤保(${processingMap.WAIT_REFUND_INSURE==null?0:processingMap.WAIT_REFUND_INSURE})</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</p>
		</div>
		<div class="content3">
			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_INSURE')">投保(${alreadyMap.WAIT_INSURE==null?0:alreadyMap.WAIT_INSURE })</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_REFUND_INSURE')">撤保(${alreadyMap.WAIT_REFUND_INSURE==null?0:alreadyMap	.WAIT_REFUND_INSURE})</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</p>
		</div>
		</div>
	</div>
	<!-- 任务类型 -->
	<input type="hidden" id="task_type" value="WAIT_PROCESS"/>
	<input type="hidden" id="task_status" />
	
	<!-- 搜索条件 -->
	<div>
		<form class="form-inline" method="post" id="orderSearchForm">
			<nav>
				<div class=" form-group">
					<input  id="search_Field2" type="text" class="form-control" placeholder="订单号/处理人" style="width: 300px"/>				
					<button type="button" class="btn btn-default" onclick="reloadAllTask()">搜索</button>
					<button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
				</div>
			</nav>
		</form>
	</div>
	<div>
		<label><font color="red" size="4">刷新倒计时:<span id="countDown">9</span>秒</font></label>
	</div>
</div>


<!-- 任务列表 -->
<div style="margin-top: 10px">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th style="width: 15%">保险信息</th>
				<th style="width: 17%">被保人</th>
				<th style="width: 17%">收支信息</th>
				<th style="width: 17%">处理信息</th>
				<th style="width: 17%">供应商信息</th>
				<th style="width: 17%">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="orderTask" varStatus="vs">
				<tr>
					<td colspan="6">
						<span>企业：${orderTask.orderDto.partnerName }</span>
						<span>预订人：${orderTask.orderDto.policyHolderName }</span>
						<span>订单号：<font color="blue">
							<c:if test="${fn:startsWith(orderTask.orderDto.orderId  ,'10')}">
								<a href="/order/flightDetail/${orderTask.orderDto.orderId }" target="_Blank">${orderTask.orderDto.orderId }</a>
							</c:if>
							<c:if test="${fn:startsWith(orderTask.orderDto.orderId  ,'17')}">
								<a href="/order/inter/orderDetail/${orderTask.orderDto.orderId  }" target="_Blank">${orderTask.orderDto.orderId  }</a>
							</c:if>

						</font></span>
						<span>来源：<font color="blue">${orderTask.orderDto.orderOriginType.message }</font></span>
						<span>标签：${orderTask.orderDto.status.message }</span>
						<span> 下单时间：<font color="blue">${orderTask.orderDto.createDate }</font></span>
					    <span><a onclick="logList('${orderTask.orderDto.id}')">订单日志</a></span>
				    </td>
				</tr>
				<tr>
					<td>
						
						<c:forEach items="${nameMap[orderTask.orderDto.productId] }" var="name" varStatus="vs">
							<c:choose>
								<c:when test="${vs.first }"><span></span></c:when>
								<c:when test="${vs.last }"><span>${name }</span></c:when>
								<c:otherwise>${name }-</c:otherwise>
							</c:choose>
						</c:forEach>
					</td>
					<td>
						<span>
							<c:forEach items="${orderTask.orderDto.insuranceOrderDetails }" var="orderDetail" varStatus="vs">
								<c:if test="${!vs.last }">${orderDetail.insuredName }/</c:if>
								<c:if test="${vs.last }">${orderDetail.insuredName }</c:if>								
								<input type="hidden" value="${orderDetail.insuredId }" name="userId">
							</c:forEach>
						</span>
					</td>
					<td>
						<span>应收￥${orderTask.orderDto.insuranceOrderDetails[0].salePrice * orderTask.orderDto.groupSize }</span>
						<span>
							    <c:choose>
									<c:when test="${not empty orderTask.orderDto.paymentPlanNo }">已收</c:when>
									<c:otherwise>未收</c:otherwise>
								</c:choose>
						</span><br/>
						<span>应付￥${orderTask.orderDto.insuranceOrderDetails[0].salePrice * orderTask.orderDto.groupSize }</span>
						<c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
							<span>已付</span><br/>
							<span><a onclick="loadIncomeDetail('${orderTask.orderDto.id}','${orderTask.orderDto.paymentPlanNo }')">明细</a></span>
						</c:if>
						<c:if test="${empty orderTask.orderDto.paymentPlanNo}">
                            <span>未付</span>
						</c:if>
						
					</td>
					<td>
						<span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>
						<span>处理人：${orderTask.operatorName }</span><br/>
						
					 </td>
					<td>
						<c:if test="${orderTask.taskStatus == 'PROCESSING' || orderTask.taskStatus == 'WAIT_PROCESS'}">
							<span><a data-trigger="tooltip" data-content="原因：${orderTask.orderDto.logDto.description }">
								<font style="color: red">
									<c:if test="${orderTask.taskType == 'WAIT_INSURE' }">投保失败</c:if>						
									<c:if test="${orderTask.taskType ==  'WAIT_REFUND_INSURE'}">退保失败</c:if>						
								</font>
							</a></span><br/>
							
						</c:if>
						<c:if test="${orderTask.taskStatus == 'ALREADY_PROCESSED' }">
							<span><a data-trigger="tooltip" data-content="原因：${orderTask.orderDto.logDto.description }">
								<font style="color: red">
									<c:if test="${orderTask.taskType == 'WAIT_INSURE' }">客户处理投保成功</c:if>						
									<c:if test="${orderTask.taskType ==  'WAIT_REFUND_INSURE'}">客服处理退保成功</c:if>						
								</font>
							</a></span><br/>
						</c:if>
						<span>${orderTask.orderDto.hubName }</span>
					</td>
					<td>
						<c:if test="${orderTask.taskStatus == 'PROCESSING' || orderTask.taskStatus == 'WAIT_PROCESS' }">
							<span><a onclick="transfer('${orderTask.id}','${orderTask.operatorId }',false)">分配</a></span>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' }">
							<span>处理结果：${orderTask.result}</span>
						</c:if>
					</td>
				</tr>
			</c:forEach>		
		</tbody>
	</table>
</div>

<div class="pagin" style="margin-bottom: 20px">
	<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="/webpage/insurance/tmc/js/allTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
 $(function(){
	
	var status = "${taskStatus}";
	$("#allTaskStatusTab li").each(function(){
		if($(this).attr("data-value")==status){
		
			$(this).addClass("active");
			$(this).siblings().removeClass("active");
			if(status == 'WAIT_PROCESS'){
				 $('#i_all_content .content1').show();
			     $('#i_all_content .content2').hide();
			     $('#i_all_content .content3').hide();
			}else if(status=='PROCESSING'){
				 $('#i_all_content .content1').hide();
				 $('#i_all_content .content2').show();
			     $('#i_all_content .content3').hide();
			}else if(status=='ALREADY_PROCESSED'){
				 $('#i_all_content .content1').hide();
			     $('#i_all_content .content2').hide();
			     $('#i_all_content .content3').show();
			}
			return false;
		}
	});
	
	var bizProcessingMapStr = '${bizProcessingMap}';
	var bizWaitProcessMapStr = '${bizWaitProcessMap}';
	if(bizProcessingMapStr != '' ){
        var bizProcessingMap = eval('(' + bizProcessingMapStr.split('=').join(':') + ')');
        $("#bizMenu").find("span").each(function () {
            var bizName = $(this).attr("name");
            var count = bizProcessingMap[bizName];
            if (count != undefined){
                $(this).text("("+count+")");
            }
        })
    }
    if(bizWaitProcessMapStr != ''){
        var bizWaitProcessMap = eval('(' + bizWaitProcessMapStr.split('=').join(':') + ')');
		$(".tab_h_item").each(function () {
		    var inputDom = $(this).find("input:eq(0)");
		    var id = inputDom.attr("id");
		    var count  = bizWaitProcessMap["COUNT"][id];
		    if(count != undefined){
                inputDom.parent().find("span").text("("+count+")");
			}

			var ulDom = inputDom.parent().next();
			ulDom.find("input").each(function () {
			    var child_input = $(this);
				var taskType = child_input.attr("task_type");
				var bizGroup = bizWaitProcessMap[id];
				if(bizGroup != undefined){
                    var child_count = bizGroup[taskType];
                    if(child_count != undefined){
                        child_input.parent().find("span").text("("+child_count+")");
                    }
				}
            })
        })

    }
});
</script>