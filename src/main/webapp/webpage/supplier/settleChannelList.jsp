<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table  table-bordered ">
    <thead>
    <tr>
        <th style="text-align:center;vertical-align:middle;"><label>类型</label></th>
        <th style="text-align:center;vertical-align:middle;"><label>名称</label></th>
        <th><label>描述</label></th>
        <th><label>最后修改人</label></th>
        <th><label>最后修改时间</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>

    <tbody>

        <c:forEach items="${pageList.list}" var="settleChannel">
            <tr>
	            <td>${settleChannel.type}</td>
	            <td>${settleChannel.name}</td>
	            <td>${settleChannel.descr}</td>
	            <td>${settleChannel.update_user}</td>
	            <td><fmt:formatDate value="${settleChannel.update_date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
	            <td class="text-center">
	                 <a onclick="settleChannel.detail.detailPage(${settleChannel.id})">修改</a>&nbsp;&nbsp;
	                 <a onclick="settleChannel.list.remove('${settleChannel.id }');">停用</a>
	            </td>
            </tr>
        </c:forEach>

    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../common/pagination_ajax.jsp?callback=settleChannel.list.reloadList"></jsp:include>
</div>