<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .popover.right > .arrow {
        top: 50% !important;
    }

    .control-label .reqMark {
        color: red;
        font-weight: bold;
        font-size: 15px;
    }

    #freightForm .btn-success {
        background-color: #46A2BB;
        border-color: #46A2BB;
    }
</style>

<div class="modal fade" id="settleChannelModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">
                    <c:choose>
                        <c:when test="${not empty settleChannelDto.id}">
                            修改
                        </c:when>
                        <c:otherwise>
                            新增
                        </c:otherwise>
                    </c:choose>
                </h4>
            </div>
            <div class="modal-body">
                <form id="settleChannelForm" class="form-horizontal">
                    <input type="hidden" name="id"  value="${settleChannelDto.id}">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">类型<font color="red">*</font></label>
                        <div class="col-sm-6">
                        <select id="settleChannel" name="type" style="width: 100px;height: 24px;">
							<c:forEach items="${channelType }" var="cType">
								<option value="${cType.key}" <c:if test="${cType.key == settleChannelDto.type}">selected="selected"</c:if>
								<c:if test="${settleChannelDto.type == null && cType.key == 'CASH'}">selected="selected"</c:if>>${cType.value}</option>
							</c:forEach>
						</select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">名称<font color="red">*</font></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" maxlength="50" value="${settleChannelDto.name}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">描述<font color="red">*</font></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="descr" maxlength="200" value="${settleChannelDto.descr}">
                        </div>
                    </div>
                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="settleChannel.detail.saveForm()">保存
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="resource/js/supplier/settleChannel.js?version=${globalVersion}"></script>


<script>
//     $(function () {
//     	settleChannel.detail.init();
//     })
</script>