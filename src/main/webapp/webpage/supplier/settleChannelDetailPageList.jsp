<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/supplier/settleChannelDetailList.css?version=${globalVersion}">
<style>
	.dash_tr td{
		border-top:none !important;
	}
</style>

<div class="result">
	<table class="table orderTable">
		<thead>
			<tr>
			    <th style="display:none;"></th>
				<th class="tipL">入账日期</th>
				<th align="center">收支通道</th>
				<th>交易类型</th>
				<th>收支方向</th>
				<th>金额</th>
				<th>供应商名称</th>
				<th>供应商单号</th>
				<th>TEM单号</th>
				<th>流水号</th>
				<th>摘要</th>
				
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="settleChannelDetail">
				<tr>
				     <td style="display:none;">${settleChannelDetail.id}</td>
				     <td style="text-align:center;width:110px;"><fmt:formatDate value="${settleChannelDetail.tradeTime}" pattern="yyyy-MM-dd" /></td>
					<td style="text-align:center;max-width:200px;">${settleChannelDetail.channelName}</td>
					
					<td style="text-align:center;max-width:200px;"><span>${settleChannelDetail.tradeType}</span></td>
					<td style="text-align:center;">${settleChannelDetail.direction }</td>
					<td style="text-align:center;">${settleChannelDetail.amount}</td>
					<td style="text-align:center;">${settleChannelDetail.supplyName }</td>
					
					<td style="text-align:center;max-width: 140px;">${settleChannelDetail.supplyRefRecord }</td>
					<td style="text-align:center;max-width: 140px;">${settleChannelDetail.temRefRecord }</td>
					<td style="text-align:center;max-width: 140px;">${settleChannelDetail.channelRefRecord }</td>
					<td style="text-align:center;">${settleChannelDetail.descr }</td>
					
					
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<div class="pagin" style="margin-bottom: 15px">
<jsp:include page="../common/pagination_ajax.jsp?callback=reloadSettleChannelDetail"></jsp:include>
</div>
