<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/plugins/layer/skin/layui.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/bootstrap/fileinput.min.css?version=${globalVersion}">
<style>
	.textBox{
		text-align:right;
		color:red;
		font-size:12px;
		width:100%;
		padding-right:20px;
	}
</style>
 <div class="modal-dialog" style="width: 800px;">
      <div class="modal-content" style="width: 800px;">
         
         <div class="modal-body">
         <form  class="form-horizontal" id="ticketForm" method="post"  enctype="multipart/form-data">
         	
			   <div class="form-group">
			      <label  class="col-sm-2 control-label">上传核对订单</label>
			      
					<div class="col-sm-4" id ="file_url_2">
					     <input type="file" name="file" id="file" ><!-- class="file-loading" -->
				    </div>
				  
			   </div>
			</form>
         </div>
          <div><label  class="textBox">注意：仅支持.xls格式文件</label></div>
         <div class="modal-footer">
            
            <button type="button" class="btn btn-default" onclick="submitTicketForm()">开始核对</button>
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancelFile()">取消</button>
         </div>
          
        <div id="downloadArea">
             <div class="modal-footer">
               <a href="" id='downloadUrl' ></a>
             </div>
          </div>
          
      </div>
</div>

<script src="resource/plugins/layer/layui.js?version=${globalVersion}"></script>
<script src="resource/plugins/layer/tree.js?version=${globalVersion}"></script>
<script src="resource/js/tr-widget/expenseAttribution.js?version=${globalVersion}"></script>
<script type="text/javascript">
//默认隐藏下载区域
$.each($('#downloadArea'),function(){
       $(this).hide();
})

//提交审核订单
function submitTicketForm(){
	if(document.getElementById("file").value==null||document.getElementById("file").value==""){
		showPromptBox("请选择核对文件!");
		return;
	}
	$("#ticketForm").validate({
		
	    submitHandler : function(form) {
	    	$('#ticketForm').attr('action','supplier/upload');
			$('#ticketForm').ajaxSubmit({success:function(data){
				if(data){
					//下载链接
					document.getElementById("downloadUrl").href="supplier/download?filename="+data;
					// 设置点击事件 ,自动点击
					var area = document.getElementById("downloadUrl");
					area.setAttribute("onclick",'');  
		            //激发标签点击事件OVER  
		            area.click("return false");
				}else{
					showPromptBox("上传文件异常，请确认后再上传!");
				}
	       	 }});
	    },
	    errorPlacement : setErrorPlacement,
	    success : validateSuccess,
	    highlight : setHighlight
	});
	$("#ticketForm").submit();
}
//点击选中文件取消操作
function cancelFile(){
	var file = $("#file");   
	file.after(file.clone().val(""));
	file.remove(); 
}
$(".file-loading").fileinput({
    language: 'zh', //设置语言
   // uploadUrl: uploadUrl, //上传的地址
    allowedFileExtensions : ['docx', 'doc','pdf','xlsx','xls'],//接收的文件后缀
    showUpload: false, //是否显示上传按钮
    showCaption: false,//是否显示标题
    browseClass: "btn btn-primary", //按钮样式 
    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>", 
});
$(".fileinput-remove").remove();

//右下角提示
function showPromptBox(content){
	layer.open({
		  type: 0,//基本层类型
		  title: "系统提示",
		  closeBtn: 0,//不显示关闭x关闭
		  btn:[],//不显示按钮
		  shade: 0,
		  area: ['250px', '200px'],//大小
		  offset: 'rb', //右下角弹出
		  time: 3000, //3秒后自动关闭
		  shift: 2,//动画0-6
		  content: content, 
		});
}
</script>