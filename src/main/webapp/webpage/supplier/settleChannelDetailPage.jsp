<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/supplier/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/supplier/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/supplier/settleChannelDetailList.css?version=${globalVersion}">

	<!--右侧主体内容-->
	<div class="main ml10">
		<div class="sel">
			<div class="mb20" id="sel1">
				<span class="glyphicon glyphicon-home" aria-hidden="true"></span>收支明细记录  &nbsp;&nbsp;
					<a href="/supplier/channelCheck/check.html" style="color:#338FD0;">返回</a>
					<input type="hidden" name="settleChannel" value="${query.settleChannel}"/>
					<input type="hidden" name="beginDate" value="${query.beginDate}"/>
					<input type="hidden" name="endDate" value="${query.endDate}"/>
				</div>
		</div>
		<div id="settleChannelDetailPageTable">
		</div>
	</div>
<script src="resource/js/supplier/settleChannelDetailPageList.js?version=${globalVersion}"></script>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
