<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/supplier/settleChannelCheckList.css?version=${globalVersion}">
<style>
	.dash_tr td{
		border-top:none !important;
	}
</style>
<div style="margin-top: 30px;margin-bottom:80px;width:100%" class="sel">
	<label style="margin-left: 10px;width:150px;">收入：￥${checkDataForm.incomeMoney },  ${checkDataForm.incomeCount }笔</label> 
	<label style="margin-left: 30px;width:150px;">支出：￥${checkDataForm.disburseMoney },  ${checkDataForm.disburseCount }笔</label> 
	<label style="margin-left: 30px;width:150px;">合计：￥${checkDataForm.totalMoney },  ${checkDataForm.totalCount }笔</label> 
	
	<label style="margin-left: 30px;width:150px;">期末余额：￥ #.##,  #笔</label> 
	
	 
	<span  id="button1" class="button" onclick="settleChannelCheck('${checkDataForm.checkItems }')" style="margin-left: 200px;width:100px;">确认核对</span>
	<span  id="button1" class="button" onclick="cancelChannelCheck('${checkDataForm.checkItems }')" style="margin-left: 50px;width:100px;">取消核对</span>
	
</div>

<script type="text/javascript">
//核对所选择的数据
function settleChannelCheck(checkItems){
	if(checkItems==""){
		$.alert("请选择需要审核的记录!", "提示");
		return;
	}
	//已选核对集合
	$.ajax({
		url:"supplier/channelCheck/checkData.json",
		data:{
			checkItems:checkItems
		},
		type:"POST",
		success:function(data){
			if (data.code == '1') {
                $.alert(data.data, "提示");
                reloadSettleChannelCheck();
            } else {
                $.alert(data.data, "提示");
            }
		},
		error:function(){
			layer.close(loading);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
}
//取消核对所选择的数据
function cancelChannelCheck(checkItems){
	if(checkItems==""){
		$.alert("请选择需要取消的记录!", "提示");
		return;
	}
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "请确认是否要取消核对？"	,
		confirm : function() {
			
			$.ajax({
				url:"supplier/channelCheck/cancelCheckData.json",
				data:{
					checkItems:checkItems
				},
				type:"POST",
				success:function(data){
					if (data.code == '1') {
		                $.alert(data.data, "提示");
		                reloadSettleChannelCheck();
		            } else {
		                $.alert(data.data, "提示");
		            }
				},
				error:function(){
					layer.close(loading);
					layer.msg("系统繁忙，请稍后再试");
				}
			});
			
		}
	});
}


//分页调用的方法
function reloadSettleChannelCheck(){
	//收集参数
	
	//入账起始日期
	var beginDate = $("#begin").val();
	//入账截止日期
	var endDate = $("#end").val();
	//收支通道
	var settleChannel = $("#settleChannel").val();
	//收支通道类型
	var type = $("#type").val();
	//审核状态
	var status = $("#status").val();
	
    var tmcId ='';
   
	var loading = layer.load();

	//分页数据
	$.ajax({
		url:"supplier/channelCheck/checklList.json",
		data:{
			beginDate:beginDate,
    		endDate:endDate,
    		settleChannel:settleChannel,
    		status:status,
	        type:type,
			tmcId:tmcId,
		},
		type:"POST",
		success:function(data){
			layer.close(loading);
			$("#settleChannelCheckTable").html(data);
			$('.drop_ul').hover(function(){
		        $('.drop_ul').removeClass('drop_ul1');
		        $(this).addClass('drop_ul1');
		    });
			
		},
		error:function(){
            layer.close(loading);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
	
}
  
</script>
