<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/supplier/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/supplier/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/supplier/settleChannelCheckList.css?version=${globalVersion}">

	<!--右侧主体内容-->
	<div class="main ml10">
		<div class="sel">
			<div class="mb20" id="sel1">
				<label for="begin">入账日期：</label> <input type="text" id="begin"
					class="date" name="begin" onclick="WdatePicker()"> <label>至</label> <input
					id="end" class="date" type="text" onclick="WdatePicker()"> 
					
					
				<label for="type" style="margin-left: 20px;">收支通道类型：</label> 
				<select id="type" name="type" style="width: 120px;height: 24px;">
					<option value="0">全部</option>
					<c:forEach items="${types }" var="type">
						<option value="${type}">${type.value}</option>
					</c:forEach>
				</select>
				
				
				
				<label for="settleChannel" style="margin-left: 20px;">收支通道：</label> 
				<select id="settleChannel" name="settleChannel" style="width: 120px;height: 24px;">
					<option value="0">全部</option>
					<c:forEach items="${settleChannels }" var="settleChannel">
						<option value="${settleChannel.id}">${settleChannel.name}</option>
					</c:forEach>
				</select>
				
				<label for="status" style="margin-left: 20px;">核对状态：</label> 
				<select id="status" name="status" style="width: 90px;height: 24px;">
					<option value="">全部</option>
					<option value="-1">未核</option>
					<option value="0">已核</option>
				</select>
				
				<span id="search" class="button" onclick="reloadSettleChannelCheck()" style="margin-left: 20px;">搜索</span>
				<span id="reset" class="button" onclick="resetForm()" style="margin-left: 10px;">重置</span>	
				
			</div>
		</div>
		<div id="settleChannelCheckTable">
		</div>
		
		<!-- <div class="sel" style="margin-top: 20px;">
		 	<span id="button1" class="button" onclick="confirmSettleChannelCheck()" style="margin-left: 10px;width: 150px">完成核对已选记录</span>
		 	
		</div> -->
		
		 <ul id="myTab" class="nav nav-tabs" data-trigger="tab" style="margin-top: 20px;">
				<li><a onclick="confirmSettleChannelCheck()">统计已选记录</a></li>
		</ul>
		
		<div id="settleChannelCheckData">
		</div>
	</div>
<script src="resource/js/supplier/settleChannelCheckList.js?version=${globalVersion}"></script>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>

