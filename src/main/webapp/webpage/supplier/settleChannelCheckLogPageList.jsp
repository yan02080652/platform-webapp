<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/supplier/settleChannelDetailList.css?version=${globalVersion}">
<style>
	.dash_tr td{
		border-top:none !important;
	}
</style>

<div class="result">
	<table class="table orderTable">
		<thead>
			<tr>
		        <td style="display:none;"></td>
		        <td style="text-align:center;width:200px;" >核对业务日期</td>
		        <td style="text-align:center;width:200px;" >收支通道</td>
		        <td style="text-align:center;width:200px;">操作时间</td>
		        <td style="text-align:center;width:200px;">操作人</td>
		        <td style="text-align:center;width:140px;">修改状态</td>
		    </tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="settleChannelCheckLog">
				<tr>
				    <td style="display:none;">${settleChannelCheckLog.id}</td>
				     
				    <td style="text-align:center;width:200px;"><fmt:formatDate value="${settleChannelCheckLog.checkedDate}" pattern="yyyy-MM-dd" /></td>
				     
					<td style="text-align:center;max-width:200px;">${settleChannelCheckLog.channelName}</td>
					
					<td style="text-align:center;max-width:200px;"><fmt:formatDate value="${settleChannelCheckLog.createDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
					<td style="text-align:center;">${settleChannelCheckLog.createUserName }</td>
					
					<td style="text-align:center;max-width: 140px;">${settleChannelCheckLog.statusName }</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<div class="pagin" style="margin-bottom: 15px">
<jsp:include page="../common/pagination_ajax.jsp?callback=reloadSettleChannelCheckLog"></jsp:include>
</div>