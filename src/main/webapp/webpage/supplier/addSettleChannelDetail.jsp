<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="modal fade" id="modelSettleChannelDetail" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="closeModal()"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">新增收支明细（调整）</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="settleChannelDetail_model" class="form-horizontal"
						role="form">

						<div class="form-group">
							<label class="col-sm-4 control-label">入账日期:</label>
							<div class="col-sm-7">
								<input type="text" id="tradeTime" class="date" name="tradeTime"
									onclick="WdatePicker()" style="width: 120px; height: 28px;">
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">收支通道:</label>
							<div class="col-sm-7">
								<select id="settleChannelId" name="settleChannelId"
									style="width: 200px; height: 28px;">
									<option value="">-请选择-</option>
									<c:forEach items="${settleChannels }" var="settleChannel">
										<option value="${settleChannel.id}">${settleChannel.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">供应商:</label>
							<div class="col-sm-7">
								<select id="issueChannelId" name="issueChannelId"
									style="width: 200px; height: 28px;">
									<option value="">-请选择-</option>
									<c:forEach items="${issueChannels }" var="issueChannel">
										<option value="${issueChannel.id}">${issueChannel.name}</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">金额:</label>
							<div class="col-sm-7">
								<%-- <input type="money" name="amount" value="<fmt:formatNumber value="${subsidy.countAmount * 0.01}" pattern="###0"/>" class="form-control"> --%>
								<input type="money" name="amount" id="amount" style="width: 200px;"
									class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">供应单号:</label>
							<div class="col-sm-7">
								<input type="text" id="supplyRefRecord" name="supplyRefRecord"
									style="width: 200px;" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">摘要描述:</label>
							<div class="col-sm-7">
								<textarea name="descr" style="width: 300px; height: 100px;"
									class="form-control"></textarea>
							</div>
						</div>

					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" onclick="closeModal()">关闭</button>
				<button type="button" class="btn btn-primary" onclick="saveModal()">确认</button>
			</div>
		</div>
	</div>
</div>

<%-- <script src="resource/js/supplier/settleChannelDetailList.js?version=${globalVersion}"></script> --%>
<script
	src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>

<script type="text/javascript">
	//初始化
	$(function() {
		//默认今天的日期
		if ($("#tradeTime").val() == '') {
			$("#tradeTime").val(new Date().format("yyyy-MM-dd"));
		}

	});

	function showFieldModal(url, callback) {
		$('#bpAccountFieldDiv').load(url, function(context, state) {
			if ('success' == state && callback) {
				callback();
			}
		});
	}

	$(function() {
		$("#settleChannelDetail_model").validate(
				{//提交数据

					rules : {
						tradeTime : {
							required : true
						},
						settleChannelId : {
							required : true
						},

						issueChannelId : {
							required : true
						},
						amount : {
							required : true,
							number : true, //输入必须是数字
							maxlength : 11
						//digits:true
						},
						supplyRefRecord : {
							maxlength : 100
						},
						descr : {
							maxlength : 200
						}
					},
					messages : {
						tradeTime : {
							required : "必选字段"
						},
						amount : {
							digits : "请输入一个金额"
						}
					},
					submitHandler : function(form) {

						$.post("supplier/channelDetail/save", $(
								"#settleChannelDetail_model").serialize(),
								function(data) {
									if (data.code == '0') {
										closeModal();
										layer.msg("保存成功");
										window.location.reload();
									} else {
										layer.msg(data.msg);
									}
								});
					},
					errorPlacement : setErrorPlacement,
					success : validateSuccess,
					highlight : setHighlight
				});

	})

	function saveModal() {
		if ($("#tradeTime").val() == '') {
			layer.msg("入账时间不能为空！",{ time: 1000});
			return false;
		}
		if ($("#settleChannelId").val() == '') {
			layer.msg("请选择收支通道！",{ time: 1000});
			return false;
		}
		if ($("#issueChannelId").val() == '') {
			layer.msg("请选择供应商！",{ time: 1000});
			return false;
		}
		if(!isMoney($("#amount").val())){
			layer.msg("请填入正确的金额！",{ time: 1000});
			return false;
		}
		$("#settleChannelDetail_model").submit();
	}

	function closeModal() {
		$('#modelSettleChannelDetail').modal('hide');
	}

	function isMoney(s) {
		//金额 正数
		//var exp = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/; 
		//金额正负数
		//var exp = /(^([+-]?)[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^([+-]?)(0){1}$)|(^([+-]?)[0-9]\.[0-9]([0-9])?$)/; 
		var exp = /(^([-]?)[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^([-]?)(0){1}$)|(^([-]?)[0-9]\.[0-9]([0-9])?$)/; 
		if(exp.test(s)) {
			return true;
		} else {
			return false;
		}
	}
</script>