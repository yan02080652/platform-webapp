<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/supplier/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/supplier/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/supplier/settleChannelDetailList.css?version=${globalVersion}">

	<!--右侧主体内容-->
	<div class="main ml10">
		<div class="sel">
			<div class="mb20" id="sel1">
				<label for="begin">入账日期：</label> <input type="text" id="begin"
					class="date" name="begin" onclick="WdatePicker()"> <label>-</label> <input
					id="end" class="date" type="text" onclick="WdatePicker()"> 
					
					
				<label for="settleChannel">收支通道：</label> 
				<select id="settleChannel" name="settleChannel" style="width: 100px;height: 24px;">
					<option value="0">全部</option>
					<c:forEach items="${settleChannels }" var="settleChannel">
						<option value="${settleChannel.id}">${settleChannel.name}</option>
					</c:forEach>
				</select>
				
				<label for="issueChannel">供应商：</label> 
				<select id="issueChannel" name="issueChannel" style="width: 130px;height: 24px;">
					<option value="0">全部</option>
					<c:forEach items="${issueChannels }" var="issueChannel">
						<option value="${issueChannel.id}">${issueChannel.name}</option>
					</c:forEach>
				</select>
				
				<label for="tradeType">交易类型：</label> 
				<select id="tradeType" name="tradeType" style="width: 90px;height: 24px;">
					<option value="0">全部</option>
					<c:forEach items="${tradeTypes }" var="tradeType">
						<option value="${tradeType}">${tradeType.value}</option>
					</c:forEach>
				</select>
				
				<%-- <label>交易类型：</label> 
                <span class="selTradeTypesAll" id="allTradeTypes" data-value="0">全部</span> 
					<c:forEach items="${tradeTypes }" var="ss">
						<span data-value="${tp }">${tp.message }</span> 
					</c:forEach> --%>
					
				<label for="keyWords" style=" margin-left: 10px;width: 60px;">关键词：</label>
				<input id="keyWords" class="keyWords" type="text" placeholder="供应单号/TEM单号/流水号/摘要">
				
				<span id="search" class="button" onclick="reloadSettleChannelDetail()">搜索</span>
				<span id="reset" class="button" onclick="resetForm()" style="margin-left: 10px;">重置</span>	
				
				<!-- <span id="exportButton" class="button" onclick="reloadExportOrder()" style="margin-left: 30px;">重置</span>	 -->
			</div>
		</div>
		<div id="settleChannelDetailTable">
		</div>
		
		<div class="sel">
		 	<span id="button1" class="button" onclick="reloadSettleChannelDetail()" style="margin-left: 10px;width: 120px">统计查询结果</span>
		 	<span id="button2" class="button" onclick="reloadExportSettleChannelDetail()" style="margin-left: 30px;width: 120px">下载查询结果</span>
		 	<span id="button3" class="button" href="javascript:;" onclick="addSettleChannelDetail()" style="margin-left: 30px;width: 120px">新增收支明细</span>
		</div>
		
		<div id="settleChannelDetailData">
		</div>
		
		<div id="settleChannelDetail_div"></div>
		
	</div>
<script src="resource/js/supplier/settleChannelDetailList.js?version=${globalVersion}"></script>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
