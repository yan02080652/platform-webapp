<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/supplier/settleChannelCheckList.css?version=${globalVersion}">
<style>
	.dash_tr td{
		border-top:none !important;
	}
</style>


<!-- 检索收支通道，全部已核之后将收起  -->
<c:forEach items="${pageList }" var="settleChannelCheck">
   <c:if test='${settleChannelCheck.statusName eq "已核" and  settleChannelCheck.titleFlag}'> 
	   <script type=”text/javascript”>
       $(function (){
       	hideClick(${settleChannelCheck.channelId });
       }); 
	  </script>
   </c:if>
</c:forEach>
<div class="result">
	<table class="table orderTable" id="settleChannelCheckDataTable">
		<!-- <thead border="1" bordercolor="#DDDDDD"> -->
		<thead>
			<tr >
			    <th style="display:none;"></th>
				<th rowspan="2"  colspan="3" align="center" width=180px;>收支通道</th>
				<th rowspan="2" align="center" width=80px;>核对状态</th>
				<th colspan="2" align="center;" width=100px; style="border-bottom: 1px solid #F7F7F7;">收入</th>
				<th colspan="2" align="center;" width=100px; style="border-bottom: 1px solid #F7F7F7;">支出</th>
				<th colspan="2" align="center;" width=100px; style="border-bottom: 1px solid #F7F7F7;">合计</th>
				<th rowspan="2" align="center" width=80px;>期末余额</th>
				<th rowspan="2" align="center" width=120px;>最后修改人员</th>
				<th rowspan="2" align="center" width=180px;>最后修改时间</th>
				<th rowspan="2" align="center" width=100px;>操作</th>
			</tr>
			<tr>
				<th style="text-align:center;">金额</th>
				<th style="text-align:center;">笔数</th>
				<th style="text-align:center;">金额</th>
				<th style="text-align:center;">笔数</th>
				<th style="text-align:center;">金额</th>
				<th style="text-align:center;">笔数</th>
				
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList }" var="settleChannelCheck">
				<tr <c:if test='${settleChannelCheck.titleFlag}'>style="background-color:#EAEAEA;"</c:if>>
				
				
				    <td style="display:none;">${settleChannelCheck.channelId}</td>
				    
				    <c:choose > 
					    <c:when test="${settleChannelCheck.titleType eq '合计'}">
						    	<td style="border-right:#FFFFFF;width:30px;"></td>	
					    </c:when>
					    <c:otherwise>
						    <c:choose > 
							    <c:when test="${settleChannelCheck.titleFlag}">
								    	<td style="border-right:#FFFFFF;text-align:right;width:30px;"><input type="checkbox" name="channelId" value="${settleChannelCheck.channelId }" onchange="selectAll('${settleChannelCheck.channelId }')" id="${settleChannelCheck.channelId }"  /></td>	
							    </c:when>
							    <c:otherwise>
								    <td <c:choose > 
										    <c:when test="${settleChannelCheck.titleFlag}">
										          onclick="hideClick(${settleChannelCheck.channelId })"
										    </c:when>
										    <c:otherwise>
										    	class="${settleChannelCheck.channelId }"
										    </c:otherwise>
									    </c:choose> 
								    style="border-right:#FFFFFF;width:30px;"></td>	
							    </c:otherwise>
						     </c:choose>
				    	</c:otherwise>
				    </c:choose>
				    
				    
				    <td 
				    
				    <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    
				     style="<c:choose> <c:when test='${settleChannelCheck.titleFlag}'>text-align:left;</c:when>
				    <c:otherwise>text-align:right;</c:otherwise></c:choose> width:120px;border-right:#FFFFFF;border-left:#FFFFFF">
				    
				    <c:if test="${settleChannelCheck.titleType ne '合计'}">
					    <c:choose > 
						    <c:when test="${settleChannelCheck.titleFlag}">
						    	<%-- <input type="checkbox" name="channelId" value="${settleChannelCheck.channelId }" onchange="selectAll('${settleChannelCheck.channelId }')" id="${settleChannelCheck.channelId }"  /> --%>
						    </c:when>
						    <c:otherwise>
						    	<input type="checkbox" name="${settleChannelCheck.channelId }" onchange="changeData()" value="${settleChannelCheck.channelId}|${settleChannelCheck.title}"/>
						    </c:otherwise>
					    </c:choose>
				    </c:if>
				    &nbsp;&nbsp;${settleChannelCheck.title}</td>
				    
				    <td 
				    <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;border-left:#FFFFFF">${settleChannelCheck.titleType}</td>
				    
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					          <%-- <c:if test='${settleChannelCheck.statusName eq "已核" }'> </c:if> --%>
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;<c:if test='${settleChannelCheck.statusName eq "已核" }'>color:#5BAD5B</c:if>">${settleChannelCheck.statusName}</td>
				    
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;">${settleChannelCheck.incomeMoney}</td>
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;">${settleChannelCheck.incomeCount}</td>
				    
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;">${settleChannelCheck.disburseMoney}</td>
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;">${settleChannelCheck.disburseCount}</td>
				    
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;">${settleChannelCheck.totalMoney}</td>
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;">${settleChannelCheck.totalCount}</td>
				    
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				      </c:choose>
					 style="text-align:center;max-width:200px;">${settleChannelCheck.endBalanceMoney}</td>
				    
				    
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;">${settleChannelCheck.updateUser}</td>
				    <td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          onclick="hideClick(${settleChannelCheck.channelId })"
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    style="text-align:center;max-width:200px;"><fmt:formatDate value="${settleChannelCheck.updateDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
				    
				    
					<td <c:choose > 
					    <c:when test="${settleChannelCheck.titleFlag}">
					          <%-- onclick="hideClick(${settleChannelCheck.channelId })" --%>
					    </c:when>
					    <c:otherwise>
					    	class="${settleChannelCheck.channelId }"
					    </c:otherwise>
				    </c:choose>
				    class="lastTd" style="text-align:center;">
					    <c:if test="${settleChannelCheck.titleType != '合计'}">
							 <a class="danhao" style="color: #23527c;" href="supplier/channelCheck/getChannelDetail?channelId=${settleChannelCheck.channelId}&checkedBeginDate=${settleChannelCheck.checkedBeginDate}&checkedEndDate=${settleChannelCheck.checkedEndDate}">明细</a>
							 <a class="danhao" style="color: #23527c;margin-left: 20px;" href="supplier/channelCheck/getChannelCheckLog?channelId=${settleChannelCheck.channelId }&checkedBeginDate=${settleChannelCheck.checkedBeginDate}&checkedEndDate=${settleChannelCheck.checkedEndDate}")" >日志</a>
						 </c:if>
                    </td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>


