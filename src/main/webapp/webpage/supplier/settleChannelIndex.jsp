<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div >
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 收支通道
            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <a class="btn btn-default" onclick="settleChannel.detail.detailPage()" role="button">新增</a>
                                        </div>
                                        <!-- 查询条件 -->
                                        <form class="form-inline" method="post">
                                            <nav class="text-right">
                                                
                                                    <label class="control-label">名称：</label>
                                                    <input type="text" id="channelName" class="channelName"  placeholder="收支通道名称" >
                                          

                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default" onclick="settleChannel.list.reloadList()">查询</button>
                                                    <button type="button" class="btn btn-default" onclick=" settleChannel.list.reset()">重置</button>
                                                </div>
                                            </nav>
                                        </form>
                                    </div>
                                </div>
                                <div id="listData" style="margin-top: 15px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="channelDiv"></div>

<script type="text/javascript" src="resource/js/supplier/settleChannel.js?version=${globalVersion}"></script>
<script>
    $(function(){
    	settleChannel.list.init();
    })
</script>