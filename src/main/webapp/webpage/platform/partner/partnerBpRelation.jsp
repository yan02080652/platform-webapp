<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="panel panel-default" id="pageListPanel">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>客户-合作伙伴关系列表&nbsp;&nbsp; 共计<span>${pageList.totalPage }页,${pageList.total }</span>条数据   &nbsp;&nbsp;
			</div>
			<div class="col-md-8">
				<form class="form-inline" action="platform/partner/partnerBpRelation" method="POST" id="queryform">
					<nav class="text-right">
							<label>客户：</label>
							<div class="form-group col-md-3 input-group">
						      <input type="hidden" name="partnerId" id="partnerId" value="${partnerId }">
						      <input type="text" name="partnerName" id="partnerName" value="${partnerName }" class="form-control"  placeholder="请选择客户" readOnly>
					       	  <div class="input-group-btn">
					             <button type="button" class="btn btn-default" onclick="choosePartner()">...</button>
						      </div>
						  </div>
						  <label>合作伙伴：</label>
						  	<div class="form-group col-md-3 input-group">
						      <input type="hidden" name="bpId" id="bpId" value="${bpId }">
						      <input type="text" name="bpName" id="bpName" value="${bpName }" class="form-control" value=""  placeholder="请选择合作伙伴" readOnly >
					       	  <div class="input-group-btn">
					            <button type="button" class="btn btn-default" onclick="chooseDep()">...</button>
						      </div>
						  </div>
						  
						 <div class="form-group ">
							<button type="button" class="btn btn-default"  onclick="submitForm()">查询</button>
							<button type="button" class="btn btn-default"  onclick="resetForm()">重置</button>
						 </div>
					</nav>
				</form>
			</div>
		</div>
	
	<hr/>
		<div class="col-sm-12" style="margin-bottom: 10px;">
				<div class="pull-left">
					<a class="btn btn-default" href="platform/partner/bpAddView" role="button">添加</a>
				</div>
			</div>
		
		<div class="col-sm-12">
			<table id="contentTable"
				class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th>客户</th>
						<th>客户组织单元</th>
						<th>合作伙伴</th>
						<th style="width: 200px;">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="partnerBp">
						<tr>
							<td>${partnerBp.partner.name}</td>
							<td>${partnerBp.partnerOrg.name}</td>
							<td>${partnerBp.bp.name}</td>
							<td>
								<a class="btn btn-default"  role="button" href='platform/partner/bpEditView/${partnerBp.partnerOrg.id }/${partnerBp.bp.id }'>修改</a>	
								<a class="btn btn-default"  role="button" onclick="deletePartnerBp('${partnerBp.partnerId }','${partnerBp.partnerOrgId}','${partnerBp.bpId}')">删除</a>	
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			
		   <%@ include file="../../common/pagination.jsp"%> 

		</div>
	</div>
</div>
<script type="text/javascript" >
function deletePartnerBp(partnerId,partnerOrgId, bpId) {
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '删除本条纪录的同时会删除其本身及其下属公司(部门)对应的客服关系，确认删除吗？',
		confirm : function() {
			$.ajax({
				type:'post',
				url:'platform/partner/deleteBpRelation',
				data:{
					partnerId:partnerId,
					partnerOrgId:partnerOrgId,
					bpId:bpId,
				},
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						showSuccessMsg(msg.txt);
						reloadPage();
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

//选择企业
function choosePartner(){
	TR.select('partner',{type:0},function(data){
			$("#partnerId").val(data.id);
			$("#partnerName").val(data.name);
	});
}

//选择合作伙伴
function chooseDep(){
	TR.select('partner',{type:1},function(data){
		$("#bpId").val(data.id);
		$("#bpName").val(data.name);
	});
}

function submitForm(){
	$("#queryform").submit();
}

function resetForm(){
	$("#queryform input").val("");
	submitForm();
}


</script>