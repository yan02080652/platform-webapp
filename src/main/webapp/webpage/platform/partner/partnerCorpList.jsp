<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<!-- 用户列表页 -->
<div class="panel panel-default" id="pageListPanel">
<div id="user_list">
	<div class="panel-body">
		<div class="row">
				<div class="col-md-4">
					<span class="glyphicon glyphicon-home" aria-hidden="true"></span>法人管理   &nbsp;&nbsp;
					<a href="platform/partner">返回</a>
				</div>
				<div class="col-md-8">
				   <div class="form-inline">
					   <nav class="text-right">
						   <input type="hidden" name="partnerId" value="${partnerId}">
						<%--<div class="form-group selectHeader" style="margin-left: 30px;">--%>
							<%--<jsp:include page="../../common/businessSelecter.jsp"></jsp:include>--%>
						<%--</div>--%>
							<%--<input type="button" class="btn btn-default search" value="查询"/>--%>
							 <%--<button type="button" class="btn btn-default"--%>
							   <%--onclick="resetPartnerForm()">重置</button>--%>
					   </nav>
					</div>
				</div>
			</div>

			<hr>
			<div class="col-sm-12" style="margin-bottom: 10px;">
				<div class="pull-left" >
					<a class="btn btn-default" href="partner/corp/add?partnerId=${partnerId}" role="button">添加</a>
				</div>
			</div>
			<div class="body-content">


			</div>
		</div>
	</div>

</div>

<script type="text/javascript">

	$(function(){
        reloadCorpOrder();
	});

	$(".search").on("click",function() {
        reloadCorpOrder();
    });

	function modifyById(id){
        var partnerId = $("input[name='partnerId']").val();
	    $.post("partner/corp/detail",{id:id,partnerId:partnerId},function(data){
            $("#page-wrapper").html(data);
		});
	}


	function reloadCorpOrder(pageIndex){
//        var tmcId = $("#queryBpTmcId").val();

        var partnerId = $("input[name='partnerId']").val();
        pageIndex = pageIndex ? pageIndex : 1;

        $.post("partner/corp/getCorpList.html",
         {
//			tmcId:tmcId,
			partnerId:partnerId,
			pageIndex:pageIndex
        },
        function(data){
            $(".body-content").html(data);
        });
    }

    function resetPartnerForm() {
        $(".selectHeader :input").val("");
        reloadCorpOrder();
    }
</script>