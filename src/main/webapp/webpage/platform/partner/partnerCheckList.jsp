<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业审核列表&nbsp;&nbsp; 共计<span>${pageList.totalPage }页,${pageList.total }</span>条数据   &nbsp;&nbsp;
			</div>
			<div class="col-md-8">
				<form class="form-inline" id="queryform">
					<nav class="text-right">
						<div class=" form-group">
							<input name="pageIndex" id="partnerCheckPageIndex" type="hidden" class="form-control" />
							<input name="name" id="queryPartnerName" type="text" value="${name }" class="form-control" placeholder="企业名称" />
							<select class="form-control"  id="checkStatus" name="status" style="width: 150px">
								<option value="-2">全部</opton>
								<option value="-1">未通过</opton>
								<option value="1">待审核,审核中</opton>
								<option value="2">审核通过</opton>
							</select>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="queryList()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
	
	<hr/>
		<div class="col-sm-12">
			<table id="contentTable"
				class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th class="sort-column">企业名称</th>
						<th class="sort-column" >企业地址</th>
						<th class="sort-column">所属行业</th>
						<th class="sort-column">注册时间</th>
						<th class="sort-column">跟进人</th>
						<th class="sort-column">状态</th>
						<th style="min-width: 100px">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="partnerCheck">
						<tr>
							<td>${partnerCheck.name}</td>
							<td>${partnerCheck.province}/${partnerCheck.city}</td>
							<td>${partnerCheck.industryName}</td>
							<td><fmt:formatDate value="${partnerCheck.createDate}" pattern="yyyy-MM-dd HH:mm" /></td>
							<td>${partnerCheck.followUserName}</td>
							<td>
								${partnerCheck.statusName}
							</td>
							<td>
								<c:if test="${empty partnerCheck.status or partnerCheck.status == 0}">
									<a class="btn btn-default" role="button" onclick="followPartner('${partnerCheck.id}')">跟进</a>	
								</c:if>
								<c:if test="${partnerCheck.status == 1}">
									<a class="btn btn-default" role="button" onclick="confirmPartner('${partnerCheck.id}')">确认</a>	
								</c:if>
								<a class="btn btn-default" role="button" onclick="detailPartner('${partnerCheck.id}')">查看</a>	
							</td>	
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryList"></jsp:include>
			</div>
		</div>
	</div>
</div>

<div id="partnerCheckInfoDiv">
	
</div>
<script type="text/javascript" >

$(document).ready(function(){
	$("#checkStatus").val("${status}");
});

function initFormField() {
	$("#formRedundInfo").validate({
		rules:{
			paymentAccount : {
				required : true
			},
			paymentTradeId : {
				required : true
			},
			paymentDestName : {
				required : true
			},
			paymentDestAccount : {
				required : true
			}
		},
		submitHandler : function(form) {
			var dataParam = $('#formRedundInfo').serializeArray();
			var dataInfo = {};
			$.each(dataParam,function(i,d){
				dataInfo[d.name] = d.value;
			});
			
			$.post("payment/refund/conformRefund",dataInfo,
				function(data){
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						window.location.href="/payment/refund";
					}else{
						showErrorMsg(data.txt);
					}
				}
			);
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

function followPartner(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认跟进此审核此客户?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'partner/check/followPartner?id='+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						showSuccessMsg(msg.txt);
						queryList();
					}
				}
			});
		}
	});
}

function confirmPartner(id){
	showFieldModal("partner/check/checkDetail?id=" + id + "", function(a) {
		$('#modelPartnerCheck').modal();
	});
}

function detailPartner(id){
	showFieldModal("partner/check/checkDetail?id=" + id + "", function(a) {
		$('#modelPartnerCheck').modal();
	});
}

function showFieldModal(url, callback) {
	$('#partnerCheckInfoDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function queryList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("#partnerCheckPageIndex").val(pageIndex);
	var queryPartnerName = $("#queryPartnerName").val();
	var status = $("#checkStatus").val();
	
	window.location.href="/partner/check/list?pageIndex="+pageIndex + "&status="+status+ "&name="+queryPartnerName;
}
function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}


</script>