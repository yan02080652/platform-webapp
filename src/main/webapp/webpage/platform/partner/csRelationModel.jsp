<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>客户-合作伙伴客服关系${title}
             	 &nbsp;&nbsp;<a data-pjax href="platform/partner/partnerCsRelation">返回</a>	
             </div>
             <!-- /.panel-heading -->
             <div class="panel-body">
                <form id="csRelationModelForm" class="form-horizontal">
					  <div class="form-group">
					    <label class="col-sm-2 control-label">客户</label>
					    <div class="col-sm-5 input-group" style="padding-left:14px;">
					      <input type="hidden" name="partnerId" id="partnerId" value="${partnerCs.partnerId }">
					      <input type="text"  id="partnerName" class="form-control" value="${partnerCs.partner.name }" readonly="readonly">
					       <div class="input-group-btn">
					         <button type="button" class="btn btn-default" onclick="choosePartner()" id="partnerChooseId">请选择</button>
					       </div>
					    </div>
					  </div>
					  
					   <div class="form-group">
					    <label class="col-sm-2 control-label">客户组织单元:</label>
						<div class="col-sm-5 input-group" style="padding-left:14px;" > 
					        <input type="hidden" name="oldPartnerOrgId" value="${partnerCs.partnerOrgId }" >						
					        <input type="hidden" name="partnerOrgId" id="partnerOrgId"  value="${partnerCs.partnerOrgId }">						
							<input type="text"   id="partnerOrgName" value="${partnerCs.partnerOrg.name }" class="form-control dropdown-org orgInput"  readonly="readonly"> 
							<span class="input-group-btn">  
								<button class="btn btn-default" type="button" onclick="selectDropdownOrg(this)">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
							</span> 
						</div>					    
					  </div>
					  
					   <div class="form-group">
					    <label class="col-sm-2 control-label">合作伙伴</label>
					    <div class="col-sm-5">
					    	<input type="hidden" name="oldBpId" value="${partnerCs.bpId }"/>
					      <select id="bpId" name="bpId" class="form-control"  onchange="loadBpOrg()" style="width: 460px;">
					        <option value="-1">请选择合作伙伴</option>
					      </select>
					    </div>
					  </div>
					  
					    <div class="form-group">
					    <label class="col-sm-2 control-label">合作伙伴客服:</label>
						<div class="col-sm-5 input-group" style="padding-left:14px;" > 
					        <input type="hidden" name="oldBpOrgId" value="${partnerCs.bpOrgId }">						
					        <input type="hidden" name="bpOrgId" id="bpOrgId" value="${partnerCs.bpOrgId }">						
							<input type="text"   id="bpOrgName" value="${partnerCs.bpOrg.name }" class="form-control dropdown-org orgInput"  readonly="readonly"> 
							<span class="input-group-btn">  
								<button class="btn btn-default" type="button" onclick="selectDropdownOrgBp(this)">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
							</span> 
						</div>					    
					  </div>
					
					</form>
					
					<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button  class="btn btn-primary" onclick="submitBpRelation()">保存</button>
					    </div>
					  </div>
             </div>
             <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
	</div>
</div>
 <script type="text/javascript">
 var pageVal = {};
 $(document).ready(function(){
	 var title = '${title}';
	 var pageUrl ;
	 if(title=='修改'){
		 $("#partnerChooseId").attr("disabled",true);
		 var bpId = '${partnerCs.bp.id}';
		 var bpName = '${partnerCs.bp.name}';
		 var $bp = $("<option value='"+bpId+"'>"+bpName+"</option>");
		 $("#bpId").append($bp);
		 $("#bpId").val(bpId);
		 
		 pageUrl = 'platform/partner/editCsRelation';
	 }else{//新增
		 pageUrl ='platform/partner/addCsRelation';
	 }
		$("#csRelationModelForm").validate({
			rules : {
				partnerOrgName : {
					required : true,
				},
				bpName :{
					required : true,
				},
				bpId:{
					type:true
				}
			},
			submitHandler : function(form) {
				$.ajax({
					url:pageUrl,
					data:$("#csRelationModelForm").serialize(),
					success:function(rs){
						if(rs.type == 'success'){
							loadPage("platform/partner/partnerCsRelation", "#page-wrapper", true);
						}else{
							$.alert(rs.txt);
						}
					}
				});
			},
			errorPlacement : setErrorPlacement,
			success : validateSuccess,
			highlight : setHighlight
		});
 })
 
 //根据客户bp:查询bp表中该客户下的所有合作伙伴
function loadBpList(){
	 var partnerId = $("#partnerId").val();
	 if(partnerId!=null && partnerId != ''){
		 $.ajax({
			 type:"post",
			 url:"platform/partner/loadBpList",
			 data:{partnerId:$("#partnerId").val(),partnerOrgId:$("#partnerOrgId").val()},
			 success:function(data){
				$("#bpId option:gt(0)").remove();
				 $(data).each(function(){
					 var $bp = $("<option value='"+this.id+"'>"+this.name+"</option>");
						$("#bpId").append($bp);
				 })
				 
			 }
		 });
	 }
 }
 
 //根据选择的bp加载bp下的组织单元
 function loadBpOrg(){
	 var bpValue =  $("#bpId").val();
	 if(bpValue!=-1){
		 $.ajax({
				url:"platform/partner/getOrgDto",
				type:"post",
				data:{partnerId:bpValue},
				dataType:"json",
				success:function(data){
					$("#bpOrgId").val(data.id);
					$("#bpOrgName").val(data.name);
				}
			})
	 }
 }
 
 
 
 
 function submitBpRelation(){
 	$("#csRelationModelForm").submit();
 }
//选择企业
 function choosePartner(){
 	TR.select('partner',{type:0},function(data){
 		if(!data){
 			//TODO qingkong
 			return;
 		}
 		
 		var changed = data.id != pageVal.oldPartnerId;
 		if(changed){
 			pageVal.oldPartnerId = data.id;
 			$("#partnerId").val(data.id);
 			$("#partnerName").val(data.name);
 			
 			//加载对应企业的组织
 			$.ajax({
 				url:"platform/partner/getOrgDto",
 				type:"post",
 				data:{partnerId:data.id},
 				dataType:"json",
 				success:function(data){
 					$("#partnerOrgId").val(data.id);
 					$("#partnerOrgName").val(data.name);
 					loadBpList();
 				}
 			})
 		}
 	});
 }

 //选择合作伙伴
 function chooseDep(){
 	TR.select('partner',{type:1},function(data){
 		$("#bpId").val(data.id);
 		$("#bpName").val(data.name);
 	});
 }
 

  //选择客户组织单元
 function selectDropdownOrg(dom){
	 var partnerOrgId = $("#partnerOrgId").val();
	 var partnerId = $("#partnerId").val();
	 var forceFlush = false;
	 if(partnerId != pageVal.partnerId){
		 forceFlush = true;
		 pageVal.partnerId = partnerId;
	 }
	 if(partnerId!=''){
		TR.select('dropdownOrg',{
			dom:dom,
			forceFlush:forceFlush,
			partnerId:pageVal.partnerId
		},function(data){
			$("#partnerOrgId").val(data.id);
			$("#partnerOrgName").val(data.name);
			loadBpList();
			
			if(partnerOrgId!=data.id){
				//请空bpOrg
			$("#bpOrgId").val("");				
			$("#bpOrgName").val("");				
				
			}
		});
	 }
	}
  //合作伙伴的下拉组织
 function selectDropdownOrgBp(dom){
	 var partnerId = $("#bpId").val();
	 var forceFlush = false;
	 if(partnerId != pageVal.partnerId){
		 forceFlush = true;
		 pageVal.partnerId = partnerId;
	 }
	 if(partnerId!=''){
		TR.select('dropdownOrg',{
			dom:dom,
			forceFlush:forceFlush,
			partnerId:pageVal.partnerId
		},function(data){
			$("#bpOrgId").val(data.id);
			$("#bpOrgName").val(data.name);
		});
	 }
	}
</script> 