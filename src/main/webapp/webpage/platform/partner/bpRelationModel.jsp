<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>客户-合作伙伴关系${title }
             	 &nbsp;&nbsp;<a data-pjax href="platform/partner/partnerBpRelation">返回</a>	
             </div>
             <!-- /.panel-heading -->
             <div class="panel-body">
                <form id="bpRelationModelForm" class="form-horizontal">
					  <div class="form-group">
					    <label class="col-sm-2 control-label">客户</label>
					    <div class="col-sm-5 input-group" style="padding-left:14px;">
					      <input type="hidden"  name="partnerId" id="partnerId" value="${partnerBp.partnerId }">
					      <input type="text"  id="partnerName" value="${partnerBp.partner.name }" class="form-control"  readonly="readonly">
					       <div class="input-group-btn" >
					         <button type="button" class="btn btn-default" onclick="choosePartner()" id="partnerChooseId">请选择</button>
					       </div>
					    </div>
					  </div>
					   <div class="form-group">
					    <label class="col-sm-2 control-label">客户组织单元:</label>
						<div class="col-sm-5 input-group" style="padding-left:14px;" >
					        <input type="hidden" name="oldPartnerOrgId"  value="${partnerBp.partnerOrg.id }">						
					        <input type="hidden" name="partnerOrgId" id="partnerOrgId" value="${partnerBp.partnerOrg.id }">						
							<input type="text"    id="partnerOrgName" value="${partnerBp.partnerOrg.name }" class="form-control dropdown-org orgInput"  readonly="readonly"> 
							<span class="input-group-btn">  
								<button class="btn btn-default" type="button" onclick="selectDropdownOrg(this)">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
							</span> 
						</div>					    
					  </div>
					  
					    <div class="form-group">
					    <label class="col-sm-2 control-label">合作伙伴</label>
					    <div class="col-sm-5 input-group" style="padding-left:14px;">
					      <input type="hidden" name="oldBpId"  value="${partnerBp.bpId }" class="">
					      <input type="hidden" name="bpId" id="bpId" value="${partnerBp.bpId }">
					      <input type="text" class="form-control" id="bpName" value="${partnerBp.bp.name }"   readonly="readonly">
					       <div class="input-group-btn">
					         <button type="button" class="btn btn-default" onclick="chooseDep()" >请选择</button>
					       </div>
					    </div>
					  </div>
					</form>
					
					<div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button  class="btn btn-primary" onclick="submitBpRelation()">保存</button>
					    </div>
					  </div>
             </div>
             <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
	</div>
</div>
 <script type="text/javascript">
var pageVal = {};
 
 function submitBpRelation(){
 	$("#bpRelationModelForm").submit();
 }
//选择企业
 function choosePartner(){
 	TR.select('partner',{type:0},function(data){
 		if(!data){
 			//TODO qingkong
 			return;
 		}
 		
 		var changed = data.id != pageVal.oldPartnerId;
 		if(changed){
 			pageVal.oldPartnerId = data.id;
 			$("#partnerId").val(data.id);
 			$("#partnerName").val(data.name);
 			
 			//TODO
 			$.ajax({
 				url:"platform/partner/getOrgDto",
 				type:"post",
 				data:{partnerId:data.id},
 				dataType:"json",
 				success:function(data){
 					$("#partnerOrgId").val(data.id);
 					$("#partnerOrgName").val(data.name);
 				}
 			})
 		}
 	});
 }

 //选择合作伙伴
 function chooseDep(){
 	TR.select('partner',{type:1},function(data){
 		$("#bpId").val(data.id);
 		$("#bpName").val(data.name);
 	});
 }
 //选择组织单元
 function selectDropdownOrg(dom){
	 var partnerId = $("#partnerId").val();
	 var forceFlush = false;
	 if(partnerId != pageVal.partnerId){
		 forceFlush = true;
		 pageVal.partnerId = partnerId;
	 }
	 
	 if(partnerId!=''){
		TR.select('dropdownOrg',{
			dom:dom,
			forceFlush:forceFlush,
			partnerId:pageVal.partnerId
		},function(data){
			$("#partnerOrgId").val(data.id);
			$("#partnerOrgName").val(data.name);
		});
	 }
	}
 
 $(document).ready(function(){
	 var title = '${title}';
	 var pageUrl ;
	 if(title=='修改'){
		 $("#partnerChooseId").attr("disabled",true);
		 pageUrl = 'platform/partner/editBpRelation';
	 }else{//新增
		 pageUrl ='platform/partner/addBpRelation';
	 }
	 
		$("#bpRelationModelForm").validate({
			rules : {
				partnerOrgName : {
					required : true,
				},
				bpName :{
					required : true,
				},
			},
			submitHandler : function(form) {
				$.ajax({
					url:pageUrl,
					data:$("#bpRelationModelForm").serialize(),
					success:function(rs){
						if(rs.type == 'success'){
							loadPage("platform/partner/partnerBpRelation", "#page-wrapper", true);
						}else{
							$.alert(rs.txt);
						}
					}
				});
			},
			errorPlacement : setErrorPlacement,
			success : validateSuccess,
			highlight : setHighlight
		});
	 
 });
 
</script> 