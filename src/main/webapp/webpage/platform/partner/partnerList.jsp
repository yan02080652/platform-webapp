<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="panel panel-default" id="pageListPanel">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
				<c:choose>
					<c:when test="${requestType=='tmc'}">
						TMC列表&nbsp;&nbsp;
					</c:when>
					<c:otherwise>
						企业客户列表&nbsp;&nbsp;
					</c:otherwise>
				</c:choose>
				共计<span>${pageList.totalPage }页,${pageList.total }</span>条数据   &nbsp;&nbsp;
			</div>
			<div class="col-md-8">
				<form class="form-inline" action="platform/partner" method="POST" id="queryform">
					<input type="hidden" name="pageIndex" value="${pageIndex}">
					<input type="hidden" name="requestType" value="${requestType}">
					<nav class="text-right">
						<div class=" form-group">
							<input name="partnerName" id="partnerName" type="text" value="${partnerName }" class="form-control" placeholder="请输入企业名称" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="submitPartnerForm()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetPartnerForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>

	<hr/>
		<div class="col-sm-12" style="margin-bottom: 10px;">
				<div class="pull-left">
					<c:choose>
						<c:when test="${requestType=='tmc'}">
							<a class="btn btn-default" href="platform/partner/add?requestType=tmc" role="button">添加</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-default" href="platform/partner/add" role="button">添加</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		
		<div class="col-sm-12">
			<table id="contentTable"
				class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th class="sort-column id sr-only" style="min-width: 80px;">id</th>
						<th class="sort-column" style="min-width: 180px;">企业名称</th>
						<th class="sort-column" >邮箱域名</th>
						<th class="sort-column" >联系人</th>
						<th class="sort-column" >联系电话</th>
						<th class="sort-column" >创建时间</th>
						<th class="sort-column" >创建人</th>
						<th class="sort-column" >最后修改时间</th>
						<th class="sort-column" >最后修改人</th>
						<th class="sort-column" >状态</th>
						<th style="min-width: 120px">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="partner">
						<tr id="${partner.id}">
							<td class="sr-only">${partner.id}</td>
							<td>${partner.name}</td>
							<td>${partner.mailDomain}</td>
							<td>${partner.contact}</td>
							<td>${partner.contactPhone}</td>
							<td>
								<fmt:formatDate value="${partner.createDate}" pattern="yyyy-MM-dd"/>
							</td>
							<td>${partner.crUser.fullname}</td>
							<td>
								<fmt:formatDate value="${partner.editDate}" pattern="yyyy-MM-dd"/>
							</td>
							<td>${partner.edUser.fullname}</td>
							 <td>
								 <c:if test="${partner.status eq 0}">可用</c:if>
                                  <c:if test="${partner.status eq 1}">不可用</c:if>
	                          </td>

							<td>
								<c:choose>
									<c:when test="${requestType=='tmc'}">
										<a class="btn btn-default" href="platform/partner/getPartner?requestType=tmc&id=${partner.id }" role="button">修改</a>
										<a class="btn btn-default" href="partner/corp/getPartnerCorpList?requestType=tmc&partnerId=${partner.id }" role="button">企业法人设置</a>
										<%-- <a class="btn btn-default"  role="button" onclick="deletePartner('${partner.id}','${partner.name}')">删除</a>	 --%>
									</c:when>
									<c:otherwise>
										<a class="btn btn-default" href="platform/partner/getPartner?id=${partner.id }" role="button">修改</a>
										<a class="btn btn-default" href="partner/corp/getPartnerCorpList?requestType=tmc&partnerId=${partner.id }" role="button">企业法人设置</a>
										<%-- <a class="btn btn-default"  role="button" onclick="deletePartner('${partner.id}','${partner.name}')">删除</a>	 --%>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<div class="pagin" style="margin-bottom: 15px">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=submitPartnerForm"></jsp:include>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" >
function deletePartner(id, name) {
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除[' + name + ']?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'platform/partner/delete/'+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						showSuccessMsg(msg.txt);
						submitPartnerForm();
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

function submitPartnerForm(pageIndex){
    pageIndex = pageIndex ? pageIndex : 1;
    $("input[name='pageIndex']").val(pageIndex);
	$("#queryform").submit();
}
function resetPartnerForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}



</script>