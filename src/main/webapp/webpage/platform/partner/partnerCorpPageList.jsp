<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="col-sm-12">
<table  class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
    <thead>
    <tr  >
        <td align="center" >企业名称</td>
        <td align="center" >法人公司名称/发票抬头</td>
        <td align="center">社会信用代码/纳税人识别码</td>
        <td align="center">企业支付账户</td>
        <td align="center">开户行</td>
        <td align="center">开户账号</td>
        <td align="center">操作</td>
    </tr>
    </thead>
    <tbody class="content">
    <c:forEach items="${pageList.list  }" var="partnerCorp">
        <tr>
            <td style="text-align: center">${partnerCorp.partnerName}</td>
            <td style="text-align: center">${partnerCorp.invoiceTitle }&nbsp;</td>
            <td>${partnerCorp.socialCreditCode }&nbsp;</td>
            <td>${partnerCorp.paymentAccountName }&nbsp;</td>
            <td>${partnerCorp.accountBank }&nbsp;</td>
            <td>${partnerCorp.accountNumber }&nbsp;</td>
            <td>
                <a onclick="modifyById(${partnerCorp.id })">修改</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="pagin" style="margin-bottom: 15px">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadCorpOrder"></jsp:include>
</div>
</div>