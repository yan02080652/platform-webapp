<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel-body">
	<div class="row">
		<div class="col-md-12">
           <div class="form-inline">
           		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>法人信息维护   &nbsp;&nbsp;
           		<a href="partner/corp/getPartnerCorpList?partnerId=${partnerCorpDto.partnerId }">返回</a>
			</div>
		</div>
		<hr/>
		<div>
			<div class="col-md-12">
				<div class="panel panel-default">
	             	<div class="panel-body">
	             		<form  class="form-horizontal" draggable="false" id="partnerCorpForm" role="form">
			            	<input type="hidden" name="id" value="${partnerCorpDto.id }"/>
							<input type="hidden" name="partnerId" value="${partnerCorpDto.partnerId }"/>


							<%--<div  class="form-group">--%>
								<%--<label class="col-sm-3 control-label">企业</label>--%>
								<%--<input type="hidden"  required="required"  id="queryBpId" name="partnerId" value="${partnerCorpDto.partnerId }" />--%>
								<%--<div class="col-sm-6">--%>
									<%--<input name="bpName" required="required" class="form-control" type="text"  id="queryBpName" value="${partnerName}" onclick="selectPartner()" placeholder="选择企业" />--%>
								<%--</div>--%>
							<%--</div>--%>
							<div class="form-group">
								<label class="col-sm-3 control-label">支付账户</label>
								<div class="col-sm-6">
									<select class="form-control" id="paymentAccount" name="paymentAccount" >
										<option value="-1">请选择企业支付账户</option>
										<c:forEach items="${bpAccounts}" var="bpAccount">
											<option value="${bpAccount.id }" <c:if test="${partnerCorpDto.paymentAccount eq bpAccount.id}"> selected="selected" </c:if> >${bpAccount.accountName }</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group">
						      <label class="col-sm-3 control-label">发票抬头</label>
						      <div class="col-sm-6">
						         <input type="text" name="invoiceTitle" value="${partnerCorpDto.invoiceTitle }" class="form-control" placeholder="法人公司名称/发票抬头"/>
						      </div>
						   </div>


						   <div class="form-group">
						      <label class="col-sm-3 control-label">公司地址</label>
						      <div class="col-sm-6">
						      		<input type="text" name="address" value="${partnerCorpDto.address }" class="form-control" placeholder="公司地址"/>
						      </div>
						   </div>
						   <div class="form-group">
						      <label class="col-sm-3 control-label">公司电话</label>
						      <div class="col-sm-6">
						         <input type="text" name="telephone" value="${partnerCorpDto.telephone }" class="form-control" placeholder="公司电话"/>
						      </div>
						   </div>
						   <div class="form-group">
							    <label class="col-sm-3 control-label">信用代码</label>
							    <div class="col-sm-6" >
								    <input type="text" name="socialCreditCode" value="${partnerCorpDto.socialCreditCode }" class="form-control" placeholder="社会信用代码/纳税人识别号"/>
							    </div>
							</div>
							<div class="form-group">
						      <label class="col-sm-3 control-label">开户行</label>
						      <div class="col-sm-6">
						          <input type="text" name="accountBank" value="${partnerCorpDto.accountBank }" class="form-control" placeholder="开户行"/>
						      </div>
						   </div>
						   <div class="form-group">
						      <label class="col-sm-3 control-label">开户账号</label>
						      <div class="col-sm-6">
						         <input type="text" name="accountNumber" value="${partnerCorpDto.accountNumber }" class="form-control" placeholder="开户账号"/>
						      </div>
						   </div>
						   
						   <div class="form-group" align="center">
						      	<button type="button" class="btn btn-primary" onclick="submitCorp()">保存
			           			</button>
						   </div>
						</form>
	            	</div>
	        	</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

$(document).ready(function() {
	initForm();
});

function selectAccounts(){
   var partnerId = $("#queryBpId").val();
   if (partnerId.isEmpty()) {
       $.alert('请先选择企业','提示');
       return;
   }
   $.get("partner/corp/getAccounts.json",{partnerId:partnerId},function(data){

       var paymentAccount = $("#paymentAccount");
       paymentAccount.html('');
       var select = "<option value='-1'>请选择企业支付账户</option>";
       $.each(data, function (i,d) {
           var option = "<option value="+d.id+">"+d.accountName+"</option>";
           select += option;
       });
       paymentAccount.append(select);
   });

}

function submitCorp(){
	$("#partnerCorpForm").submit();
} 

function deleteCorp(id){
	
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确定删除该法人？',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "partner/corp/deleteCorp",
				data : {
					id : id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						window.location.href = "/partner/corp/getPartnerCorpList";
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
}

function initForm(){
    var partnerId = ${partnerCorpDto.partnerId };

	$("#partnerCorpForm").validate({
		rules:{
			invoiceTitle:{
				required : true
			},
			socialCreditCode : {
				required : true
			},
			address:{
				required : true
			}
		},
		submitHandler : function(form) {
			$.ajax({
				cache : true,
				type : "POST",
				data : $("#partnerCorpForm").serialize(),
				url : "partner/corp/saveOrUpdateCorp.json?",
				async : false,
				error : function(request) {
				},
				success : function(data) {
					if (data.type == 'success') {
						window.location.href = "/partner/corp/getPartnerCorpList?partnerId="+partnerId;
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

function selectPartner(){

    TR.select('partner',{
        type:0//固定参数
    },function(data){
        $("#queryBpId").val(data.id);
        $("#queryBpName").val(data.name);
        selectAccounts();
    });

}

</script>