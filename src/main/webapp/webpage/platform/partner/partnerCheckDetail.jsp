<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
.inputNewInfo{
	width: 240px;
	padding-left: 0px;
}
</style>

<div class="modal fade" id="modelPartnerCheck" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
   <div class="modal-dialog" style="width: 750px;">
      <div class="modal-content" style="width: 750px;">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">客户详情</h4>
         </div>
         <div class="modal-body">
			<div class="row">
				<form modelAttribute="partnerCheckingDto" class="form-horizontal" id="partnerCheckForm" role="form">
					 <input name="id" id="partnerCheckId" type="hidden" value="${partnerCheck.id }"/>
					 <input name="industry" type="hidden" value="${partnerCheck.industry }"/>
					 <input name="status" type="hidden" value="${partnerCheck.status }"/>
					 <input name="followUser" type="hidden" value="${partnerCheck.followUser }"/>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">企业名称:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="name" class="form-control" value="${partnerCheck.name }">
					    </div>
					    <label class="col-sm-2 control-label nomalFond">所属行业:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" readonly="readonly" class="form-control" value="${partnerCheck.industryName }">
					    </div>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">企业简称:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="alias" class="form-control" value="${partnerCheck.alias }">
					    </div>
					    <label class="col-sm-2 control-label nomalFond">邮箱域名:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="mailDomain" id="mailDomain" class="form-control" value="${partnerCheck.mailDomain }">
					    </div>
					 </div>
					 <div class="form-group">
					     <label class="col-sm-2 control-label nomalFond">企业地址:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" class="form-control" readonly="readonly" value="${partnerCheck.province }/${partnerCheck.city }/${partnerCheck.district }">
					    </div>
					    <label class="col-sm-2 control-label nomalFond">详细地址:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" readonly="readonly" class="form-control" value="${partnerCheck.address }">
					    </div>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">联系人:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="contact" class="form-control" value="${partnerCheck.contact }">
					    </div>
					    <label class="col-sm-2 control-label nomalFond">联系人电话:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="contactPhone" class="form-control" value="${partnerCheck.contactPhone }">
					    </div>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">联系人邮箱:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="contactEmail" class="form-control" value="${partnerCheck.contactEmail }">
					    </div>
					    <label class="col-sm-2 control-label nomalFond">管理员:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="adminName" class="form-control" value="${partnerCheck.adminName }">
					    </div>
					 </div>
					 <div class="form-group">
					     <label class="col-sm-2 control-label nomalFond">管理员电话:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="adminMobile" class="form-control" value="${partnerCheck.adminMobile }">
					    </div>
					    <label class="col-sm-2 control-label nomalFond">管理员邮箱:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="adminMail" class="form-control" value="${partnerCheck.adminMail }">
					    </div>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">审核说明:</label>
					    <div class="col-sm-10" style="padding-left: 0px;width: 611px">
					      <input type="text"  name="confirmReason" class="form-control" >
					    </div>
					 </div>
				</form>
			</div>		
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
            </button>
            <c:if test="${partnerCheck.status == 1 }">
            	<button type="button" class="btn btn-primary" id="passPartnerCheckButton" onclick="passPartnerCheck()">审批通过</button>
            	<button type="button" class="btn btn-primary" id="rejectPartnerCheckButton" onclick="rejectPartnerCheck('${partnerCheck.id }')">审批驳回</button>
            </c:if>
         </div>
      </div>
	</div>
</div>


<script type="text/javascript">
$(document).ready(function() {
	initPartnerCheckField();
});

function passPartnerCheck(){
	$("#passPartnerCheckButton").attr('disabled',true);
	$("#rejectPartnerCheckButton").attr('disabled',true);
	$('#partnerCheckForm').submit();
}

function rejectPartnerCheck(id){
	var confirmReason = $("input[name='confirmReason']").val();
	if(!confirmReason){
		$.alert("请填写审核说明！","提示");
		return;
	}
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认此客户审核不通过?',
		confirm : function() {
			$("#passPartnerCheckButton").attr('disabled',true);
			$("#rejectPartnerCheckButton").attr('disabled',true);
			$.ajax({
				type:'GET',
				url:'partner/check/rejectPartnerCheck',
			    data:{
			    	id:id,
			    	confirmReason : confirmReason
			    },
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						showSuccessMsg(msg.txt);
						queryList();
					}
				}
			});
		}
	});
}

function initPartnerCheckField() {
	$("#partnerCheckForm").validate({
		rules:{
			name : {
				required : true
			},
			contact : {
				required : true
			},
			contactPhone : {
				required : true
			},
			contactEmail : {
				required : true
			},
			adminName : {
				required : true
			},
			adminMobile : {
				required : true
			},
			adminMail : {
				required : true
			},
			mailDomain :{
				/* required : true, */
				mailDomain:true,
				remote : {
					url : "platform/partner/checkMailDomain",
					type : "post",
					data : {
						mailDomain : function() {
							return $("#mailDomain").val();
						}
					},
					dataFilter : function(data) {
						if (data == 'ok') {//表示域名可用
							return true;
						} else {
							return false;
						}
					}
				}
			}
		},
		messages : {
			mailDomain : {
				remote : "域名不可用",
			}
		},
		submitHandler : function(form) {
			var dataParam = $('#partnerCheckForm').serializeArray();
			
			$.post("partner/check/passPartnerCheck",dataParam,
				function(data){
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						queryList();
					}else{
						showErrorMsg(data.txt);
					}
				}
			);
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

</script>