<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.theme.default.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<script src="resource/plugins/jqueryTreeTable/javascripts/src/jquery.treetable.js?version=${globalVersion}" type="text/javascript"></script>

<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业客户信息<c:choose><c:when test="${partner.id != '' && partner.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
             	 &nbsp;&nbsp;<a data-pjax href="platform/partner">返回</a>	
             </div>
             <!-- /.panel-heading -->
             <div class="panel-body">
                 <form id="partner_form" class="form-horizontal" method="post">
					  <input name="id" id="partnerId" type="hidden" value="${partner.id}"/>
					 <input type="hidden" name="requestType" value="${requestType}">
					<div class="form-group hidden">
						<label for="type" class="col-sm-2 control-label">客户类型</label>
						<div class="col-sm-6">
							<select class="form-control" id="type" name="type">
								<option value="0">企业客户</option>
							</select>
						</div>
					</div>

					  <div class="form-group">
					    <label class="col-sm-2 control-label">名称</label>
					    <div class="col-sm-6">
					      <input type="text" name="name" class="form-control" value="${partner.name }" placeholder="企业名称">
					      <span class="glyphicon glyphicon-warning-sign form-control-feedback hidden" data-container="body" data-toggle="popover" data-trigger="click"></span>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">简称</label>
					    <div class="col-sm-6">
					      <input type="text" name="alias" class="form-control" value="${partner.alias }" placeholder="企业简称">
					    </div>
					  </div>
					   <div class="form-group">
					    <label class="col-sm-2 control-label">邮箱域名</label>
					    <div class="col-sm-6">
					      <input type="text" name="mailDomain" id="mailDomain" class="form-control" value="${partner.mailDomain }" placeholder="邮箱域名">
					      <span class="glyphicon glyphicon-warning-sign form-control-feedback hidden" data-container="body" data-toggle="popover" data-trigger="click"></span>
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label class="col-sm-2 control-label">联系人</label>
					    <div class="col-sm-6">
					      <input type="text" name="contact" class="form-control" value="${partner.contact }" placeholder="联系人">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">联系人电话</label>
					    <div class="col-sm-6">
					      <input type="text" name="contactPhone" class="form-control" value="${partner.contactPhone }" placeholder="联系人电话">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">联系人邮箱</label>
					    <div class="col-sm-6">
					      <input type="text" name="contactEmail" class="form-control" value="${partner.contactEmail }" placeholder="联系人邮箱">
					    </div>
					  </div>
					  
					  <div class="form-group">
					   <label class="col-sm-2 control-label">状态</label>
					    <div class="col-sm-6">
					      <select class="form-control" name="status">
					      	<c:choose>
					      		<c:when test="${partner.status==0 || partner.status==null}">
					      			<option value="0"	selected="selected">可用</option>
					         		<option value="1">不可用</option>
					      		</c:when>
					      		<c:otherwise>
					      			 <option value="0">可用</option>
					         		<option value="1" selected="selected" >不可用</option>
					      		</c:otherwise>
					      	</c:choose>
					      </select>
					    </div>
					  </div>
					 <%-- <div class="form-group">
					   <label for="createDate" class="col-sm-2 control-label">创建时间</label>
					    <div class="col-sm-6">
					      <input type="hidden" class="form-control" id="createDate" value="${partner.createDate}"/>
			        	  <input type="text" class="form-control"  value="<fmt:formatDate value="${partner.createDate}" pattern="yyyy-MM-dd"/>" readonly />
					    </div>
					  </div>
					 <div class="form-group">
					    <label for="createUser" class="col-sm-2 control-label">创建人</label>
					    <div class="col-sm-6">
					      <input type="text" class="form-control" id="crUser"  value="${partner.crUser.fullname}" readonly/>
					      <input type="hidden" class="form-control" id="createUser" name="createUser" value="${partner.createUser}" readonly/>
					    </div>
					  </div>
					 <div class="form-group">
					     <label for="editDate" class="col-sm-2 control-label">最后修改时间</label>
					    <div class="col-sm-6">
					      <input type="hidden" class="form-control" id="editDate" value="${partner.editDate}"/>
					      <input type="text" class="form-control"  value="<fmt:formatDate value="${partner.editDate}" pattern="yyyy-MM-dd"/>" readonly />
					    </div>
					  </div>
					 <div class="form-group">
					  	<label for="editUser" class="col-sm-2 control-label">最后修改人</label>
					    <div class="col-sm-6">
					      <input type="text" class="form-control" id="edUser"  value="${partner.edUser.fullname}" readonly/>
					      <input type="hidden" class="form-control" id="editUser" name="editUser" value="${partner.editUser}" readonly/>
					    </div>
					  </div> --%>
					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-6">
					        <c:choose>
								<c:when test="${operType eq 'edit' and partner.type != 1}">
									<button type="button" class="btn btn-default" onclick="submitPartner()">保存基础信息</button>
					 			</c:when>
								<c:otherwise> 
									<button type="button" class="btn btn-default" onclick="submitPartner()">提交</button>
					 			</c:otherwise>
							</c:choose>
					    </div>
					  </div>
					</form>
             </div>
             <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
	</div>
</div>
<%-- ,,暂时先去掉tmc分配 test="${operType eq 'edit'}"--%>
<c:if test="${operType eq 'edit'}">
	<div class="panel panel-default hidden" style="width:65%;" >
        <div class="panel-heading">
            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>TMC分配
        </div>
        <table id="treeTablePartnerTmc" class="table  table-bordered">
			<!-- <thead>
		         <tr>
		           <th style="text-align:center;">组织</th>
		           <th>所属TMC</th>
		           
		           
		         </tr>
		       </thead> -->
		       <tbody id="treeTablePartnerTmcDetail">
		       
		       </tbody>
		</table>
    </div>
</c:if>

 <script type="text/javascript">
 function submitPartner(){
 	$("#partner_form").submit();
 }
 
//选择合作伙伴
 function chooseDep(id){
 	TR.select('partner',{type:1},function(data){
 		var bpName = data.name;
 		$.ajax({
			cache : true,
			type : "POST",
			url : "platform/partner/savePartnerBpRelation",
			data : {
				id : id,
				bpId :data.id
			},
			async : false,
			error : function(request) {
				showErrorMsg("请求失败，请刷新重试");
			},
			success : function(rdata) {
				if (rdata.type == 'success') {
					$("#partnerBpTd"+id).html('<a href="javascript:;" onclick="chooseDep('+id+')">'+bpName+'</a> <font color="red" style="cursor:pointer" title="删除" onclick="deleteDep('+id+')">x</font>');
				} else {
					$.alert(rdata.txt,"提示");
				}
			}
		});
 	});
 }
 
 //删除合作伙伴
 function deleteDep(id){
	 $.ajax({
			cache : true,
			type : "POST",
			url : "platform/partner/deletePartnerBpRelation",
			data : {
				id : id
			},
			async : false,
			error : function(request) {
				showErrorMsg("请求失败，请刷新重试");
			},
			success : function(data) {
				if (data.type == 'success') {
					 $("#partnerBpTd"+id).html('<a href="javascript:;" onclick="chooseDep('+id+')">设置</a>');
				} else {
					$.alert(data.txt,"提示");
				}
			}
		});
 }
 
$(document).ready(function() {
	
	var type = '${partner.type}';
	if(type!='' && type !=null){
		$("#type").val(type);
	}
	
	
	$("#partner_form").validate({
		rules : {
			name : {
				required : true,
				minlength : 1
			},
			contactEmail:{
                email:true,

            },
			contactPhone:{
				mobile:true,

            },
			type :{
				type:true
			},
            mailDomain :{
				/* required : true, */
				mailDomain:true,
				remote : {
					url : "platform/partner/checkMailDomain",
					type : "post",
					data : {
                        mailDomain : function() {
                            return $("#mailDomain").val();
                        },
						id : function() {
							return $("#partnerId").val();
						}
					},
					dataFilter : function(data) {
						if (data == 'ok') {//表示域名可用
							return true;
						} else {
							return false;
						}
					}
				}
			}
		},
		messages : {
            mailDomain : {
				remote : "域名不可用",
			}
		},
		submitHandler : function(form) {
			/* $(form).ajaxSubmit({
				url:'platform/partner/update',
				success:function(data){
					if (data.type == "success") {
						//loadPage("platform/partner", "#page-wrapper", true);
						window.location.href="platform/partner";
						showSuccessMsg(data.txt);
					} else {
						showErrorMsg(data.txt);
					}
				}
			}); */
			
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/partner/update",
				data : $('#partner_form').serialize(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.code == 'success') {
                        layer.msg("保存成功");
                        if (data.data.requestType != undefined) {
                            window.location.href="/platform/partner?requestType=tmc";
                        }else {
                            window.location.href="/platform/partner";
                        }
					} else {
						showErrorMsg(data.msg);
					}
				}
			});
			
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
	
	//如果是修改的话 则显示客户与tmc的关系
	var operType = "${operType}";
	var type = '${partner.type}';
	if(operType == 'edit'){
		var partnerId = '${partner.id}';
		$.ajax({
			type : 'GET',
			url : "platform/partner/getPartnerTmc?partnerId="+partnerId,
			success : function(data) {
				var treeData = TR.turnToZtreeData(data);
				var html = "";
				html = addChild(treeData,html);
				$("#treeTablePartnerTmcDetail").html(html);
				
				function addChild(data) {
					$.each(data, function(i, d) {
						if(d.pid){
							html += "<tr data-tt-id='"+d.id+"' data-tt-parent-id='"+d.pid+"'>";
						}else{
							html += "<tr data-tt-id='"+d.id+"'>";
						}
						if(d.children){//存在子节点
							html += "<td><span class='folder'>"+d.data.orgName+"</span></td><td id='partnerBpTd"+d.data.id+"'>"+(d.data.bpName==null?'<a href="javascript:;" onclick="chooseDep('+d.data.id+')">设置</a>':'<a href="javascript:;" onclick="chooseDep('+d.data.id+')">'+d.data.bpName+'</a> <font color="red" style="cursor:pointer" title="删除" onclick="deleteDep('+d.data.id+')">x</font>')+"</td>";
							html += "</tr>";
							addChild(d.children,html);
						}else{
							html += "<td><span class='file'>"+d.data.orgName+"</span></td><td id='partnerBpTd"+d.data.id+"'>"+(d.data.bpName==null?'<a href="javascript:;" onclick="chooseDep('+d.data.id+')">设置</a>':'<a href="javascript:;" onclick="chooseDep('+d.data.id+')">'+d.data.bpName+'</a> <font color="red" style="cursor:pointer" title="删除" onclick="deleteDep('+d.data.id+')">x</font>')+"</td>";
							html += "</tr>";
						}
					});
					return html;
				}

				var option = {
					expandable: true,
				    expandLevel : 4
				};

				$("#treeTablePartnerTmc").treetable(option);
				jQuery('#treeTablePartnerTmc').treetable('expandAll');

				//Highlight selected row
				$("#treeTablePartnerTmc tbody tr").mousedown(function() {
					$("#treeTablePartnerTmc tr.selected").removeClass("selected");
					$(this).addClass("selected");
				});


				$("#treeTablePartnerTmc.folder").each(function() {
					$(this).parents("tr").droppable({
						 accept: ".file, .folder",
						 drop: function(e, ui) {
						   var droppedEl = ui.draggable.parents("tr");
						   $("#treeTablePartnerTmc").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
						 },
						 hoverClass: "accept",
						 over: function(e, ui) {
						   var droppedEl = ui.draggable.parents("tr");
						   if(this != droppedEl[0] && !$(this).is(".expanded")) {
						     $("#treeTablePartnerTmc").treetable("expandNode", $(this).data("ttId"));
						   }
						 }
					});
				});
			}
		});
	}
	
});
</script> 