<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
 <div class="form-group">
   <label class="col-sm-3 control-label">单位管理员:</label>
   <div class="col-sm-6">
     <div  style="padding: 6px 12px;">
       <c:forEach items="${userRoleList }" var="userRole">
           ${userRole.user.fullname }&nbsp;
          <span class="glyphicon glyphicon-remove" onclick="removeOrgLeader('${userRole.id }','${userRole.orgId }');"></span>&nbsp;
        </c:forEach>
        <c:if test="${flagAuth == 1 }">
          	<a class="btn btn-default" onclick="chooseUser('${orgId}');">选择人员</a>
        </c:if>
     </div>
   </div>
 </div>
 