<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="panel-body" style="padding-top: 0px;padding-bottom: 5px">
		<a class="btn btn-default" href="javascript:void(0)" role="button" id="edit_1" onclick="editNodeMenu();">编辑</a>
        <a class="btn btn-default" href="javascript:void(0)" role="button" id="edit_2" onclick="cancel();" style="display:none;">取消编辑</a>
        <button type="button" class="btn btn-default" disabled="disabled" id="save_1" onclick="submitOrg()">保存</button>
	</div>
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-body">
                 <form id="org_form" class="form-horizontal" method="post">
					  <input name="id" id="orgId" type="hidden" value="${org.id}"/>
					  <input name="pid" id="pid"  type="hidden" value="${pid }"/>
					  <div class="form-group">
					    <label class="col-sm-3 control-label">单位简称:</label>
					    <div class="col-sm-6">
					      <input type="text" name="name" class="form-control" value="${org.name }" readonly="readonly">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-3 control-label">单位全称:</label>
					    <div class="col-sm-6">
					      <input type="text" name="fullname" class="form-control" value="${org.fullname }" readonly="readonly">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-3 control-label">单位说明:</label>
					    <div class="col-sm-6">
					      <textarea rows="3"  class="form-control" name="descr" readonly="readonly">${org.descr }</textarea>
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-3 control-label">单位性质:</label>
					    <div class="col-sm-6">
					      <select id="select-type" class="form-control" name="type" value="${org.type }" disabled="disabled">
					            <option value="">请选择单位性质</option>
						         <c:forEach items="${cfgList }" var="cfgVo">
						            <option value="${cfgVo.itemCode }">${cfgVo.itemTxt}</option>
						         </c:forEach>
					      </select>
					    </div>
					  </div>					  				  
					  <div class="form-group">
					    <label class="col-sm-3 control-label">单位编码:</label>
					    <div class="col-sm-6">
					      <input type="text" name="code" class="form-control" value="${org.code }"  readonly="readonly">
					    </div>
					  </div>
					 <div class="form-group">
					    <label class="col-sm-3 control-label">是否设置成本中心:</label>
					    <div class="col-sm-6">
						  <label class="radio-inline"><input type="radio" name="costCenter" value="0" <c:if test="${org.costCenter eq 0}">checked</c:if> disabled="disabled"/>是</label>
						  <label class="radio-inline"><input type="radio" name="costCenter" value="1" <c:if test="${org.costCenter eq 1}">checked</c:if>  disabled="disabled"/>否</label>					     
					    </div>
					  </div>
					 <div class="form-group">
					       <label class="col-sm-3 control-label">单位类型:</label>
					     <div class="col-sm-6 orgtype">
					       <label class="radio-inline"><input type="radio" name="isCompany" value="0"  <c:if test="${org.isCompany eq 0}">checked</c:if> disabled="disabled"/> 公司</label>
					       <label  class="radio-inline"><input type="radio" name="isCompany" value="1" <c:if test="${org.isCompany eq 1}">checked</c:if> disabled="disabled"/>部门</label>
					     </div>
					 </div>
					 <div class="form-group">
					      <label class="col-sm-3 control-label">组织层面:</label>
					    <div class="col-sm-6">
					      <div class="input-group">
					        <input type="hidden" id="orgGradeCode" value="${orgGradeCode }">
					        <input type="hidden" id="orgGrade_code"  value="${org.orgGrade }" name="orgGrade">
	                        <input type="text"   id="orgGrade_name"  value="${orgGradeName }"  name="orggradeName" class="form-control dropdown-role roleInput" readonly="readonly">
	                         <span class="input-group-btn">
		                      <button class="btn btn-default" type="button" onclick="selectDropdownOrgGrade(this)" disabled="disabled">
			                     <i class="fa fa-caret-down" aria-hidden="true"></i>
		                      </button>
	                         </span>
                          </div>
                       </div>
					 </div>
					  <div class="form-group">
					    <label class="col-sm-3 control-label">有效期开始:</label>
					    <div class="col-sm-6">
					      <input type="text" id="validForm1" name="validFrom" class="form-control" value="<fmt:formatDate value="${org.validFrom }"   pattern="yyyy-MM-dd"/>"  onClick="WdatePicker()"  disabled="disabled">
					    </div>
					  </div>
					 <div class="form-group">
					    <label class="col-sm-3 control-label">有效期结束:</label>
					    <div class="col-sm-6">
					      <input type="text" id="validTo1" name="validTo" class="form-control" value="<fmt:formatDate value="${org.validTo }"   pattern="yyyy-MM-dd"/>" onClick="WdatePicker()"  disabled="disabled" />
					    </div>
					  </div>
					 <div class="form-group">
					    <label class="col-sm-3 control-label">序号:</label>
					    <div class="col-sm-6">
					      <input type="number" name="seq" class="form-control" value="${org.seq }" disabled="disabled"/>
					    </div>
					  </div>					  
					 <div class="form-group">
					    <label class="col-sm-3 control-label">状态:</label>
					    <div class="col-sm-6">
					    <select id="select-status" class="form-control" name="status" disabled="disabled">
					         <option value="0">启用</option>
					         <option value="-1">不启用</option>
					    </select>
					    </div>
					  </div>
					  <div id="org_leader">
					  		<jsp:include page="org_userRole.jsp"></jsp:include>
					  </div>
					</form>
             </div>
             <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
	</div>
</div>
<script>

$(document).ready(function() {

	if('${org.status }' != ""){
	    $("#select-status").val('${org.status }');
	} 
	if('${org.type}' != ""){
		$("#select-type").val('${org.type}');
	}
	$(".orgtype :radio").click(function(){

		var isCompany=$(this).val();
		var orgGradeCode=$("#orgGradeCode").val();

		$.post('platform/org/checkOrgGrade',{
		    "isCompany":isCompany,
			"orgGradeCode":orgGradeCode,
            'partnerId' : partnerId
		},function(data){
				$("#orgGrade_name").val(data.name);
				$("#orgGrade_code").val(data.code);
		});
	});
	
})

function selectDropdownOrgGrade(dom){

	TR.select('dropdownOrgGrade',{
		dom:dom
	},function(data){
		$('#orgGrade_name').val(data.name);
		$('#orgGrade_code').val(data.code);
	});
}

function removeOrgLeader(id,orgId){

	$.post('platform/org/deleteUserRole',{
	    id:id
	},function(data){
		
	});

	$.post('platform/org/userRoleList', {
			"orgId" : orgId,
            'partnerId' : partnerId
		},
		function(data){
		$("#org_leader").html("").html(data);;
	});
}

function chooseUser(orgId){

 	TR.select('user',function(data){
		var userId=data.id;
 		addUser(userId,orgId); 
	});
}

function addUser(userId,orgId){

 	$.post('platform/org/addUserRole',{
 	    "userId":userId,
		"orgId":orgId,
        "partnerId" : partnerId
 	},function(data){
 		
	});

	$.post('platform/org/userRoleList',{
	    "orgId":orgId,
        "partnerId" : partnerId
	},function(data){
		$("#org_leader").html("").html(data);;
	});
}
</script>