/**
 * 组织管理
 */
var canAdd = true;
var curAuthNode;//左边树当前节点

require(['treeview'],function(){
    reloadTree();
});

function reloadTree() {
	$.ajax({
		type : 'GET',
		url : "platform/org/orgList",
		data : {partnerId : partnerId},
		success : function(data) {

			$('#treeOrg').treeview({
				data: TR.turnToTreeViewData(data),
				showBorder:false,
				highlightSelected:true,
				onNodeSelected: function(event, data) {
					if(!canAdd){
                        //取消当前选择
						$('#treeOrg').treeview('unselectNode', [ data.nodeId, { silent: true } ]);

                        //不能编辑时还是选择上一个节点
						if(curMenuNode != null){
							$('#treeOrg').treeview('selectNode', [ curMenuNode.nodeId, { silent: true } ]);
						}

						$.alert("请编辑完当前节点!","提示");
						return;
					}else{

						//刷新右侧配置页面
						curMenuNode = data;
						reloadRightReadOnly(data.id,data.pid);
					}
				}, 
				onNodeUnselected: function(event, data) {
					//刷新右侧配置页面
					if(!canAdd){
						
					}else{
						curMenuNode = null;
						$("#rightMenuDetail").hide();
					}
				}
			});
		}
	});
}

function reloadPage(){
	$("#rightMenuDetail").hide();
}

function reloadRight(id,pid,orgGrade,isCompany) {
	if(!id){
		id = '';
	}
	if(!pid){
		pid = '';
	}
	if(!orgGrade){
		orgGrade='';
	}
	$("#rightMenuDetail").show();

	var path = 'platform/org/orgDetail?id='+ id +'&pid='+ pid +'&orgGrade='+ orgGrade + '&partnerId=' + partnerId;

	$("#rightMenuDetail").load(path,function(){

		$('#org_form').find('input,textarea,select,button').attr('disabled',false);
		$('#org_form').find('input,textarea').attr('readonly',false);
		$('#orgGrade_name').attr('readonly',true);
		$('#edit_1').hide();
		$('#edit_2').show();
		$('#save_1').attr('disabled',false);

		if(isCompany==1){
			$(".orgtype input:radio[value='0']").attr('disabled','true');
		}
		if(isCompany==2){
			$(".orgtype input:radio[value='1']").attr('disabled','true');
		}

		initForm();	 
	});
}

function reloadRightReadOnly(id,pid) {
	if(!id){
		id = 0;
	}
	if(!pid){
		pid = "";
	}
	$("#rightMenuDetail").show();

	var path = 'platform/org/orgDetail?id=' +id +'&pid='+ pid + '&partnerId=' + partnerId;;
	$("#rightMenuDetail").load(path,function(){

		$('#edit_1').show();
		$('#edit_2').hide();
		initForm();	 
	});
}

function addRootMenu() {

	if (!canAdd) {
		$.alert("请先编辑完当前节点!", "提示");
		return;
	}
	//添加顶级节点时 取消当前选中
	if(curMenuNode!=null){
		$('#treeOrg').treeview('unselectNode', [curMenuNode.nodeId, { silent: true }]);
	}
	curMenuNode = null;
	reloadRight(0,0,0,2);
	canAdd = false;
};

function addNodeMenu(){
	if (!canAdd) {
		$.alert("请先编辑完当前节点!", "提示");
		return;
	}
	if(!curMenuNode){
		$.alert("请先选择一个节点!","提示");
		return;
	}
	
	//取第一个节点
    var orgGrade = curMenuNode.data.orgGrade;
    var isCompany = curMenuNode.data.isCompany;
	var pid = curMenuNode.id;
	canAdd = false;

	reloadRight("",pid,orgGrade,isCompany);
}

//修改节点信息
function editNodeMenu() {

	if (!canAdd) {
		$.alert("请先编辑完当前节点!", "提示");
		return;
	}
	if(curMenuNode == null){
		$.alert("请选择一个节点!","提示");
		return;
	}
    var isCompany = curMenuNode.data.isCompany;

	$('#org_form').find('input,textarea,select,button,radio').attr('disabled',false);
	$('#org_form').find('input,textarea').attr('readonly',false);
	$('#orgGrade_name').attr('readonly',true);
	$('#edit_1').hide();
	$('#edit_2').show();
	$('#save_1').attr('disabled',false);

	if(isCompany == 1){
		$(".orgtype input:radio[value='0']").attr('disabled','true');
	}
	if(curMenuNode.parentId == undefined){
		$(".orgtype input:radio[value='1']").attr('disabled','true');
	}

	canAdd = false;

};


//删除节点
function deleteNodeMenu() {
	if (!canAdd) {
		$.alert("请先编辑完当前节点!", "提示");
		return;
	}
	if(curMenuNode==null){
		$.alert("请先选择一个节点!","提示");
		return;
	}
	if(curMenuNode.parentId==undefined){
		$.alert("顶级单位不能删除!","提示");
		return;		
	}
	
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除当前节点?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/org/deleteOrg",
				data : {
					id : curMenuNode.id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadPage();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
};

function cancel(){

    //说明是编辑的 重新点击该节点就可以了
	if(curMenuNode!=null && curMenuNode.id!=null){
		reloadRightReadOnly(curMenuNode.id,curMenuNode.pid);
	}else{
		$("#rightMenuDetail").hide();
	}
	$('#edit_1').show();
	$('#edit_2').hide();

	canAdd = true;
}

function initForm() {
	$("#org_form").validate({
		rules : {
			name : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			fullname:{
				required:true,
				minlength:1,
				maxlength:30
			},
			code:{
				remote:{
					url:"platform/org/checkCode",
					data:{
						orgId:function(){
							return $("#orgId").val();
						},
                        partnerId : partnerId
					}
				}
			},
			isCompany:{
				required:true
			},
			costCenter:{
				required:true
			},
			validFrom:{
				required:true
			},
			validTo:{
				required:true,
				endDate:true
			},
			seq:{
				required:true
			}
		},
        messages:{
        	code:{
               remote:"编码必须唯一！"
            }
        },
		submitHandler : function(form) {

			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/org/saveOrUpdateOrg",
				data : $('#org_form').serialize() + '&partnerId=' + partnerId,
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					canAdd = true;
					if (data.type == 'success') {
						window.location.reload();
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
			
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

function submitOrg() {
	$("#org_form").submit();
}
