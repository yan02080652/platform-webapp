<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 
<script>
	var partnerId = '${partnerId}';
</script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
           	<div class="form-inline">
           		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>组织管理  &nbsp;&nbsp;
				<c:if test="${type ne 'me'}">
					<div class="form-group" style="margin-left: 30px;">
						<%@ include file="/webpage/common/partnerChange.jsp"%>
					</div>
				</c:if>
			</div>
		</div>
		<br>
		<hr/>
		<div>
			<div class="col-md-3">
					<div  style="padding-top: 0px;padding-bottom: 5px"> 
<!-- 						<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addRootMenu();">添加顶级</a> -->
						<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addNodeMenu();">添加下级</a>
					</div>
				<div class="panel panel-default">
	             	<div class="panel-body">
						<ul id="treeOrg" style="padding-left: 10px;"></ul>
	            	</div>
	        	</div>
			</div>
			<div id="rightMenuDetail" class="col-md-9 animated fadeInRight">
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="webpage/platform/org/org.js?version=${globalVersion}"></script>