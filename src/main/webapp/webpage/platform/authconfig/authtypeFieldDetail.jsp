<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="modelAuthField" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">${typeTitle}</h4>
         </div>
         <div class="modal-body">
         <form modelAttribute="authtypeFieldDto" class="form-horizontal" id="formAddAuthField" role="form">
            	<input type="hidden" name="authId" value="${authtypeDto.id}"/>
            	<input type="hidden" id="authFieldId" name="id" value="${authtypeFieldDto.id}"/>
				<div class="form-group">
			      <label for="authtypeCode" class="col-sm-2 control-label">权限编码</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" readonly="readonly" id="authtypeCode" name="authtypeCode" value="${authtypeFieldDto.authtypeCode}"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="code" class="col-sm-2 control-label">编码</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="authFieldCode" name="code" value="${authtypeFieldDto.code}" placeholder="子字段编码"/>
			      </div>
			   </div>
			   <div class="form-group">
				    <label for="name" class="col-sm-2 control-label">名称</label>
				    <div class="col-sm-10">
				   	  <input type="text" class="form-control" name="name" value="${authtypeFieldDto.name}" placeholder="子字段名称"/>
				    </div>
				</div>
				<div class="form-group">
			      <label for="optionValues" class="col-sm-2 control-label">值</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" name="optionValues" value="${authtypeFieldDto.optionValues}" placeholder="值"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="seq" class="col-sm-2 control-label">序号</label>
			      <div class="col-sm-10">
			         <input type="number" class="form-control" name="seq" value="${authtypeFieldDto.seq}" placeholder="序号"/>
			      </div>
			   </div>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="submitAuthField()">保存
            </button>
         </div>
      </div>
</div>

<script>
 $(document).ready(function() {
	var authFieldId=$("#authFieldId").val();
	if(authFieldId!=null&&authFieldId!=''){
		$("#authFieldCode").attr("readonly","readonly");
	}
 });
 </script>