<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 权限对象配置</div>
             	<!-- <div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<tr>
								<td><a href="javascript:void(0)" onclick="addRoot();"
									id="addRoot">添加根节点</a></td>
								<td><a href="javascript:void(0)" onclick="addNode();"
									id="addNode">添加子节点</a></td>
								<td><a href="javascript:void(0)" onclick="editNode();"
									id="editNode">修改</a></td>
								<td><a href="javascript:void(0)" onclick="deleteNode();"
									id="deleteNode">删除</a></td>
							</tr>
						</table>
						<ul id="treeAuth" class="ztree"></ul>
					</div>
            	</div> -->
            	<div class="panel-body" style="padding-bottom: 5px">
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addRoot();">添加顶级</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addNode();">添加下级</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="editNode();">编辑</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="deleteNode();">删除</a>
				</div>
				<ul id="treeAuth" style="padding-left: 10px;"></ul>
        	</div>
		</div>
		<div id="rightAuth" class="col-md-8 animated fadeInRight">
			  <jsp:include page="authtypeField.jsp"></jsp:include> 
		</div>
	</div>
</div>
<div id="authTreeNodeDiv"></div>
<script type="text/javascript" src="webpage/platform/authconfig/authtype.js?version=${globalVersion}"></script>