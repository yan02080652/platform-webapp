<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="panel panel-default">
	<div class="panel-heading">
		<c:choose>
			<c:when test="${not empty authtypeDto && authtypeDto.nodeType==0}">
				<input type="hidden" name="code" value="${authtypeDto.code}" />
              	${authtypeDto.name}(${authtypeDto.code})
       		</c:when>
			<c:otherwise>
				<span class="label label-danger">请选择一个权限配置</span>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="panel-body">
		<c:if test="${not empty authtypeDto && not empty authtypeDto.code && authtypeDto.nodeType==0}">
			<div class="col-sm-12" style="margin-bottom: 10px;">
				<div class="pull-left">
					<button class="btn btn-white btn-sm" data-toggle="tooltip"
						data-placement="left" onclick="addAuthField('${authtypeDto.code}')" title="添加">
						<i class="fa fa-plus"></i> 新增
					</button>
				</div>
			</div>
		</c:if>
		<div class="col-sm-12">
			<table class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th class="sort-column itemTxt" style="min-width: 70px;text-align: center;">子字段编码</th>
						<th class="sort-column itemCode" style="min-width: 90px;text-align: center;">子字段名称</th>
						<th class="sort-column itemDesc" style="max-width: 200px;text-align: center;">可选值列表</th>
						<!-- <th class="sort-column itemSeq" width="50">序号</th> -->
						<th style="min-width: 120px">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${authtypeFieldDtoList}" var="authtypeFieldDto">
						<tr>
							<td>${authtypeFieldDto.code}</td>
							<td>${authtypeFieldDto.name}</td>
							<td class="itemDesc">${authtypeFieldDto.optionValues}</td>
							<%-- <td>${authtypeFieldDto.seq}</td> --%>
							<td>
								<a href="javascript:void(0)" onclick="updateAuthField(${authtypeFieldDto.id})">修改</a>
								<a href="javascript:void(0)" onclick="deleteAuthField('${authtypeFieldDto.id}','${authtypeFieldDto.name}')">删除</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div id="authTreeNodeFieldDiv"></div>