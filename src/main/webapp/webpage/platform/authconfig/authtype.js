var pageVal = {};

pageVal.getNodeData = function() {
	var nodeDataTop = [];
	var childrenData = {};
	$.each(this.data, function(i, d) {
		if (!d.pid) {
			nodeDataTop.push({
				id : d.id,
				text : d.name+"("+d.code+")",
				nodeType : d.nodeType,
				pid:d.pid,
				code : d.code
			});
		} else {
			if (!childrenData[d.pid]) {
				childrenData[d.pid] = [];
			}
			childrenData[d.pid].push({
				id : d.id,
				text : d.name+"("+d.code+")",
				nodeType : d.nodeType,
				pid:d.pid,
				code : d.code
			});
		}
	});

	$.each(nodeDataTop, function(i, n) {
		addChild(n);
	});

	function addChild(n) {
		var children = childrenData[n.id];
		if (children) {
			$.each(children, function(i, c) {
				addChild(c);
			});
			n.nodes = children;
		}
	}
	;
	return nodeDataTop;
};

var curAuthNode;//左边树当前节点

require(['treeview'],function(){//加载bootstrap treeview的资源 以及初始化树的数据
	initMenu();
});


function initMenu(){
	reloadTree();
}

function reloadTree() {
	$.ajax({
		type : 'GET',
		url : "platform/authconfig/authtype.do",
		success : function(data) {
			pageVal.data = data || [];
			$('#treeAuth').treeview({
				data: pageVal.getNodeData(),
				showBorder:false,
				highlightSelected:true,
				//selectedBackColor:'#808080',
				onNodeSelected: function(event, data) {
					curAuthNode = data;
					reloadRight(data.code);
				},
				onNodeUnselected: function(event, data) {
					//刷新右侧配置页面
					curAuthNode = null;
					reloadRight("0");
				}
			});
			
		}
	});
}

//添加根节点,e为json对象；
function addRoot(e) {
	addAuthNode("0");
};

//添加节点,e为json对象；
function addNode(e) {
	if(curAuthNode == null){
		$.alert("请选择一个节点!","提示");
		return;
	}
	if(curAuthNode != null && curAuthNode.nodeType==0){
		$.alert("权限节点不能添加子节点!","提示");
		return;
	}
	addAuthNode(curAuthNode.id);
};
//修改节点信息
function editNode() {
	if(curAuthNode == null){
		$.alert("请选择一个节点!","提示");
		return;
	}
	updateAuthNode(curAuthNode.id);

};
//删除节点
function deleteNode() {
	if(curAuthNode == null){
		$.alert("请选择一个节点!","提示");
		return;
	}
	
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除当前节点?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/authconfig/deleteAuthNode",
				data : {
					id : curAuthNode.id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadTree();
						reloadRight("0");//删除完了之后刷新后面的页面
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
};

function reloadRight(code) {
	var path = 'platform/authconfig/listDetail/' + code;
	$("#rightAuth").load(path);
}


function initForm() {
	$("#formAddAuthNode").validate({
		rules : {
			nodeType1 : {
				type : true,	
			},
			code : {
				required : true,
				minlength : 1,
				maxlength : 30,
				remote : {
					url : "platform/authconfig/checkNodeCode",
					type : "post",
					data : {
						code : function() {
							return $("#authCode").val();
						},
						id: function() {
							return $("#authTypeId").val();
						}
					},
					dataFilter : function(data) {
						var resultMes = eval("("+data+")");
						if (resultMes.type == 'success') {
							return true;
						} else {
							return false;
						}
					}
				}
			},
			name : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			descr : {
				maxlength : 150
			}
		},
		messages : {  
			code : {  
	            remote: "编码已存在！"
	          }
	    },
		submitHandler : function(form) {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/authconfig/saveOrupdateAuth",
				data : $('#formAddAuthNode').serialize()+"&nodeType="+$("#select-nodeType").val(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
					$('#modelAuthNode').modal('hide');
				},
				success : function(data) {
					$('#modelAuthNode').modal('hide');
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadTree();
						reloadRight("0");//添加完了之后刷新后面的页面
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

/**
 * 窗口关闭事件
 */
$('#authTreeNodeDiv').on('hide.bs.modal', function () {
	
});


function submitAuth() {
	$("#formAddAuthNode").submit();
}

function addAuthNode(pid) {
	showModal("platform/authconfig/addAuthNode/" + pid + "", function() {
		$('#modelAuthNode').modal();
		initForm();
	});
}

function updateAuthNode(id) {
	showModal("platform/authconfig/updateAuthNode/" + id + "", function(a) {
		$('#modelAuthNode').modal();
		initForm();
	});
}

function showModal(url, callback) {
	$('#authTreeNodeDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}


function addAuthField(authtypeCode) {
	showFieldModal("platform/authconfig/addAuthField/" + authtypeCode + "", function() {
		$('#modelAuthField').modal();
		initFormField();
	});
}

function updateAuthField(id) {
	showModal("platform/authconfig/updateAuthField/" + id + "", function(a) {
		$('#modelAuthField').modal();
		initFormField();
	});
}

function showFieldModal(url, callback) {
	$('#authTreeNodeFieldDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function submitAuthField() {
	$("#formAddAuthField").submit();
}

function initFormField() {
	$("#formAddAuthField").validate({
		rules : {
			code : {
				required : true,
				minlength : 1,
				maxlength : 30,
				remote : {
					url : "platform/authconfig/checkFieldCode",
					type : "post",
					data : {
						code : function() {
							return $("#authFieldCode").val();
						},
						id: function() {
							return $("#authFieldId").val();
						},
						authtypeCode: function() {
							return $("#authtypeCode").val();
						}
					},
					dataFilter : function(data) {
						var resultMes = eval("("+data+")");
						if (resultMes.type == 'success') {
							return true;
						} else {
							return false;
						}
					}
				}
			},
			name : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			optionValues : {
				minlength : 1,
				maxlength : 150
			}
		},
		messages : {  
			code : {  
	            remote: "编码已存在！"
	          }
	    },
		submitHandler : function(form) {
			$('#modelAuthField').modal('hide');
			setTimeout(function(){
				$.ajax({
					cache : true,
					type : "POST",
					url : "platform/authconfig/saveOrupdateAuthField",
					data : $('#formAddAuthField').serialize(),
					error : function(request) {
						showErrorMsg("请求失败，请刷新重试");
						$('#modelAuthField').modal('hide');
					},
					success : function(data) {
						if (data.type == 'success') {
							showSuccessMsg(data.txt);
							reloadRight(curAuthNode.code);	
						} else {
							showErrorMsg(data.txt);
						}
					}
				});
			}, 1000);
			
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}


function deleteAuthField(id, name) {
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除配置项[' + name + ']?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'platform/authconfig/deleteAuthField/'+id,
				success:function(msg){
					if (msg.type == 'success') {
						showSuccessMsg(msg.txt);
						reloadRight(curAuthNode.code);
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		cancel : function() {
			//alert('the user clicked cancel')
		}
	});
}
