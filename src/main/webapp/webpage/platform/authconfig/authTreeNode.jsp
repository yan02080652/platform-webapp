<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="modelAuthNode" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">${typeTitle}</h4>
         </div>
         <div class="modal-body">
         <form modelAttribute="authtypeDto" class="form-horizontal" id="formAddAuthNode" role="form">
            	<input type="hidden" name="pid" value="${pid}"/>
            	<input type="hidden" id="authTypeId" name="id" value="${authtypeDto.id}"/>
			    <div class="form-group">
			      <label for="nodeType" class="col-sm-2 control-label">节点类型</label>
			      <div class="col-sm-10">
			         <select class="form-control" id="select-nodeType" name="nodeType1">
					          <option value="-1">请选择节点类型</opton>
					          <option value="0">权限对象</option>
					          <option value="1">目录</option>
					    </select>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="code" class="col-sm-2 control-label">编码</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="authCode" name="code" value="${authtypeDto.code}" placeholder="权限类型编码"/>
			      </div>
			   </div>
			   <div class="form-group">
				    <label for="name" class="col-sm-2 control-label">名称</label>
				    <div class="col-sm-10">
				   	  <input type="text" class="form-control" name="name" value="${authtypeDto.name}" placeholder="权限类型名称"/>
				    </div>
				</div>
				<div class="form-group">
			      <label for="descr" class="col-sm-2 control-label">详细描述</label>
			      <div class="col-sm-10">
			         <textarea class="form-control" id="descr" name="descr" rows="3">${authtypeDto.descr} </textarea>
			      </div>
			   </div>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="submitAuth()">保存
            </button>
         </div>
      </div>
</div>
 <script>
 $(document).ready(function() {
	var authCode=$("#authCode").val();
	if(authCode!=null&&authCode!=''){
		$("#select-nodeType").val("${authtypeDto.nodeType}");
		$("#authCode").attr("readonly","readonly");
		
	}
 });
 </script>