<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel panel-default">
	<div class="panel-heading">
				<i class="fa fa-bars" aria-hidden="true"></i> 基础信息
	</div>
	<div class="panel-body" style="padding: 0px;">
		<div class="modal-body" style="padding-bottom: 0px;">
			<form modelAttribute="roleDto" class="form-horizontal" draggable="false" id="roleForm" role="form">
				<input type="hidden" id="roleId" name="id" value="${roleDto.id}" /> 
				<input type="hidden" id="pid" name="pid" value="${roleDto.pid}" /> 
				<input type="hidden" id="partnerId" name="partnerId" value="${roleDto.partnerId}" /> 
				<div class="form-group">
					<label for="nodeType1" class="col-sm-2 control-label">节点类型</label>
					<div class="col-sm-4">
						<select class="form-control" id="nodeType1" name="nodeType1" disabled="true">
							<option value="-1">请选择节点类型</option>
							<option value="1">目录</option>
							<option value="0">角色</option>
						</select>
					</div>
					
					<label for="roleCode" class="col-sm-2 control-label">编码</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" readonly="readonly" id="roleCode" name="code" value="${roleDto.code}" placeholder="角色编码" />
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">名称</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" readonly="readonly" id="name" name="name" value="${roleDto.name}" placeholder="角色名称" />
					</div>
					
					<label for="roleScope" class="col-sm-2 control-label">适用范围</label>
					<div class="col-sm-4">
						<select class="form-control" id="roleScope" name="scope" disabled="true">
							<option value="-1">请选择适用范围</option>
							<c:forEach items="${roleScopeList}" var="roleScopeInfo">
								<option value="${roleScopeInfo.itemCode}">${roleScopeInfo.itemTxt}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="descr" class="col-sm-2 control-label">说明</label>
					<div class="col-sm-10">
						<textarea rows="2" class="form-control" readonly="readonly"
							id="descr" name="descr" placeholder="说明">${roleDto.descr}</textarea>
					</div>
				</div>
				<div class="form-group" align="center">
					<button type="button" class="btn btn-default" disabled="disabled"
						onclick="cancel()">取消</button>
					&nbsp;&nbsp;
					<button type="button" class="btn btn-default" disabled="disabled"
						onclick="submitRole()">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		var roleId = "${roleDto.id}";
		if (roleId != null && roleId != '') {
			$("#nodeType1").val("${roleDto.nodeType}");
			$("#roleScope").val("${roleDto.scope}");
		}
	});
</script>
<c:if test="${not empty roleDto.id}">
	<iframe src="platform/auth?roleId=${roleDto.id }" style="border: none;width: 100%;height: 500px;"></iframe>
</c:if>