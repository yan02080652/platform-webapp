<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="container-fluid">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
		        <div class="form-inline">
	           		<div class="glyphicon glyphicon-home" aria-hidden="true"></div>角色配置   &nbsp;&nbsp;
	           		<div class="form-group" style="margin-left: 30px;">
	          		 	<%@ include file="/webpage/common/partnerChange.jsp"%>
	          		</div>
				</div>
			</div>
			<hr/>
			<div>
				<div class="panel-body" style="padding-top: 0px;padding-bottom: 5px">
					<button class="btn btn-default" onclick="addTopNode();" id="adTopdNode">添加顶级</button>
					<button class="btn btn-default" onclick="addNode();" id="addNode">添加下级</button>
					<button class="btn btn-default" onclick="editNode();" id="editNode">编辑</button>
					<button class="btn btn-default" onclick="deleteNode();" id="deleteNode">删除</button>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
		             	<div class="panel-body">
							<div id="role_tree" class=""></div>
		            	</div>
		        	</div>
				</div>
				<div id="rightDetail" class="col-md-8 animated fadeInRight">
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var currentNode;//左边树当前节点
var canAdd = true;

$(document).ready(function() {
	require(['treeview'],function(){//加载bootstrap treeview的资源 以及初始化树的数据
		setTimeout(function(){
			reloadTree();
		},1000);
	});
});

//重载树
function reloadTree(id) {
	$.ajax({
		type : 'GET',
		url : "platform/role/showMenu",
		data:{"partnerId" : $("#partnerIdHeader").val()},
		success : function(data) {
		var treeMenuData = TR.turnToTreeViewData(data);
			$('#role_tree').treeview({
				data: treeMenuData,
				showBorder:false,
				highlightSelected:true,
				levels:4,
				onNodeSelected: function(event, data) {
					if(!canAdd){
						$('#role_tree').treeview('unselectNode', [ data.nodeId, { silent: true } ]);//取消当前选择
						if(currentNode != null){
							$('#role_tree').treeview('selectNode', [ currentNode.nodeId, { silent: true } ]);//不能编辑时还是选择上一个节点
						}
						$.alert("请编辑完当前节点!","提示");
						return;
					}else{
						//刷新右侧配置页面
						currentNode = data;
						reloadRightReadOnly(data.id,data.pid);
					}
				   
				},
			});
		}
	});
}

	//添加顶级节点
	function addTopNode(e) {
		if(!canAdd){
			$.alert("请编辑完当前节点!","提示");
			return;
	}
	//添加顶级节点时 取消当前选中
	if(currentNode!=null){
		$('#role_tree').treeview('unselectNode', [currentNode.nodeId, { silent: true }]);
	}
	currentNode = null;
	reloadRight(null,null);
	canAdd = false;
};

	//加载右侧面板
	function reloadRight(id,pid) {
		$("#rightDetail").show();
		var path = 'platform/role/detail';
		$("#rightDetail").load(path, {'id' : id,'pid':pid},function(data){
			$("#nodeType1").attr('disabled', false);
			$("#roleScope").attr('disabled', false);
			$('#roleForm  :button,textarea').attr('disabled', false);
			$('#roleForm input,textarea').attr('readonly', false);
			initForm();
		});
	}
	//只读
	function reloadRightReadOnly(id,pid) {
		$("#rightDetail").show();
		var path = 'platform/role/detail';
		$("#rightDetail").load(path, {'id' : id,'pid':pid},function(data){
			$("#nodeType1").attr('disabled', true);
			$('#roleForm :button ,textarea').attr('disabled', true);
			$('#roleForm input,textarea').attr('readonly', true);
			initForm();
		});
	}
	
	function readOnly(){
		$("#nodeType1").attr('disabled', true);
		$('#roleForm :button ,textarea').attr('disabled', true);
		$('#roleForm input,textarea').attr('readonly', true);
	}
	
	//新增下级
	function addNode(){
		if(!canAdd){
			$.alert("请编辑完当前节点!","提示");
			return;
		}
		if(currentNode==null || currentNode.data.nodeType==0){
			$.alert("请选择一个目录节点!","提示");
			return;
		}
		reloadRight(null,currentNode.id);
		canAdd = false;
	}
	
	//修改节点信息
	function editNode() {
		if(!canAdd){
			$.alert("请编辑完当前节点!","提示");
			return;
		}
		if(currentNode==null){
			$.alert("请选择一个节点!","提示");
			return;
		}
		$('#roleForm :button,textarea').attr('disabled', false);
		$('#roleForm input,textarea').attr('readonly', false);
		//$("#roleCode").attr("readonly","readonly");
		$("#roleScope").attr("disabled",false);
		canAdd = false;
	};

	//取消
	function cancel(){
		if(currentNode!=null && currentNode.id!=null){//说明是编辑的 重新点击该节点就可以了
			reloadRightReadOnly(currentNode.id,currentNode.pid);
		}else{
			$("#rightDetail").hide();
		}
		canAdd = true;
	}

	function initForm() {
		$("#roleForm").validate({
			rules : {
				nodeType1 : {
					type : true,	
				},
				scope : {
					type : true,	
				},
				code : {
					//required : true,
					//minlength : 1,
					maxlength : 20,
					remote : {
						url : "platform/role/checkRoleCode",
						type : "post",
						data : {
							code : function() {
								return $("#roleCode").val();
							},
							id : function() {
								return $("#roleId").val();
							},
							partnerId : function() {
								return $("#partnerIdHeader").val();
							}
						},
						dataFilter : function(data) {
                            var resultMes = JSON.parse(data);
							if (resultMes.type == 'success') {
								return true;
							} else {
								return false;
							}
						}
					}
				},
				name : {
					required : true,
					minlength : 1,
					maxlength : 20,
				},
				descr:{
					maxlength : 200
				},
			},
			messages : {
				code : {
					remote : "编码已存在",
				},
			},
			submitHandler : function(form) {
				$.ajax({
					cache : true,
					type : "POST",
					url : "platform/role/saveOrupdate",
					data : $('#roleForm').serialize()+'&nodeType='+$('#nodeType1').val(),
					async : false,
					error : function(request) {
						showErrorMsg("请求失败，请刷新重试");
					},
					success : function(data) {
						if (data.type == 'success') {
							showSuccessMsg(data.txt);
							reloadTree();
							//readOnly();
							if(data.id){//新增刷新
								reloadRightReadOnly(data.id, null);
							}else{
								readOnly();
							}
							canAdd = true;
						} else {
							showErrorMsg(data.txt);
						}
					}
				});
			},
			errorPlacement : setErrorPlacement,
			success : validateSuccess,
			highlight : setHighlight
		});
	}


	//删除节点
	function deleteNode() {
		if(!canAdd){
			$.alert("请编辑完当前节点!","提示");
			return;
		}
		if(currentNode==null){
			$.alert("请选择一个节点!","提示");
			return;
		}
		
		if (currentNode.nodes) {
			$.alert("父节点不允许直接删除,请先删除该节点下的所有子节点后重新操作");
			return;
		} 
		
		if(currentNode.data.partnerId == 0) {
			$.alert("不能删除此角色");
			return;
		}
		
			$.confirm({
				title : '提示',
				confirmButton : '确认',
				cancelButton : '取消',
				content : '确认删除这个节点吗?',
				confirm : function() {
					$.ajax({
						cache : true,
						type : "POST",
						url : "platform/role/deleteRole",
						data : {
							id : currentNode.id
						},
						async : false,
						error : function(request) {
							showErrorMsg("请求失败，请刷新重试");
							canAdd = true;
						},
						success : function(data) {
							if (data.type == 'success') {
								currentNode = null;
								showSuccessMsg(data.txt);
								reloadTree();
								$("#rightDetail").hide();
								canAdd = true;
							} else {
								$.alert(data.txt, "提示");
							}
						}
					});
				},
				cancel : function() {
				}
			});
		}
	

	function submitRole() {
		$("#roleForm").submit();
	}

</script>