<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="container" style="margin-top:100px;">
	<form class="form-horizontal" id="password_form" role="form" method="post" action="platform/userSecurity/updatePwd">
		<div class="form-group">
			<label class="col-sm-2 control-label">姓名</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" readonly value="${user.fullname}" style="width:250px;">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">手机号码</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" readonly value="${user.mobile}" style="width:250px;">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">邮箱</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" readonly value="${user.email}" style="width:250px;">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">原密码</label>
			<div class="col-sm-10">
				<input type="password" class="form-control" style="width:250px;" name="currentPassword" id="currentPassword" placeholder="Old Password"><span id="oldpassTip" style="display:none;color:red;"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">新密码</label>
			<div class="col-sm-10">
				<input type="password" class="form-control" style="width:250px;" name="newPassword" id="newPassword" placeholder="New Password"><span id="newpassTip" style="display:none;color:red;"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">确认新密码</label>
			<div class="col-sm-10">
				<input type="password" class="form-control" style="width:250px;" name="checkNewPassword" placeholder="Confirm New Password"><span id="newpassAgainTip" style="display:none;color:red;"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<button type="submit" class="btn btn-primary" id="submit" style="text-align:center;">确认修改</button>
			</div>
		</div>
	</form>
</div>
<div id="modifySuccess" class="alert alert-success alert-dismissable" style="width:50%;margin-left:40%;display:none;">
	<strong>Success!</strong> 你已成功修改密码！
</div>

<script>
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "请输入不同的密码");

	$(function(){
        $("#password_form").validate({
            rules:{
                currentPassword:{
                    required:true,
                    minlength: 6,
                    remote:{
                        url:'/platform/userSecurity/verifyPassword'
                    }
                },
                newPassword:{
                    required:true,
                    minlength: 6,
                    notEqual:"#currentPassword"
                },
                checkNewPassword:{
                    required:true,
                    minlength: 6,
                    equalTo: "#newPassword"
                }
            },
            messages:{
                currentPassword:{
                    remote:'输入的当前密码错误'
                }
            },
            submitHandler : function(form) {

                $.post('platform/userSecurity/updatePwd',$("#password_form").serialize(),function(data){
                    if(data){
                        layer.msg("密码修改成功");
                        $("#password_form")[0].reset();
                    }
				})

            },errorClass:"promptError"
        });

    })

</script>