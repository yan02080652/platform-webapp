<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	request.setAttribute("CTX_PATH", request.getContextPath());
%>
<html>
<head>
<title>系统登录</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
	var __ctxPath="<%=request.getContextPath()%>";
</script>

<link rel="stylesheet" href="resource/css/bootstrap/bootstrap.min.css?version=${globalVersion}">
<style type="text/css">
	body {
		background: url("resource/image/login/bg.png") repeat;
	}
	table {
		width: 100%;
		height: 100%;
	}
	.login-container {
		width: 400px;
		height: 360px;
		padding: 60px 80px;
		border: 1px solid #D8D8D8;
		border-radius: 7px 7px 7px 7px;
		box-shadow: 0 1px 4px #D3D3D3;
		background-color: #f8f8f8;
	}
	.form-group {
		text-align: left;
	}
	.login-btn {
		width: 120px;
	}
	.form-actions {
		height: 60px;
		padding-top: 20px;
	}
	label {
		font-size: 14px;
	}

	.not_btn {
		background-color: #bbbbbb;
		cursor: default;
	}
	.verifi_btn{
		color: #fff;
		background-color: #5bc0de;
	}
	.verifi_btn:hover{
		color: #fff;
		background-color: #39b3d7;
	}
	.user_login{
		width: 200px;
		padding-bottom: 20px;
		font-size: 20px;
	}
</style>
</head>
<body>
	<table border="0">
		<tr>
			<td align="center" valign="middle">
				<div class="login-container">
					<form id="loginForm" class="form-horizontal" name="loginForm" action="${CTX_PATH}/security_login" method="POST">
						<div class="user_login">
							用户登录
						</div>
						<div class="form-group">
							<input type="text" name="username" placeholder="Email或手机" class="form-control fa bell" id="input-username" >
						</div>
						<div class="form-group">
							<input type="password" name="password" placeholder="密码" class="form-control input-password">
						</div>
					</form>
					<div class="form-actions">
						<button class="btn btn-info login-btn" onclick="login();">登录</button>
					</div>
				</div>
			</td>
		</tr>
	</table>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width: 400px;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title" id="myModalLabel">
						账号验证
					</h4>
				</div>
				<div class="modal-body">

					<form class="form-horizontal" action="${CTX_PATH}/security_login" method="POST">
						<div class="form-group">
							<div class="col-sm-6">
								<img id="img" src="/login/code" class="verifi_code_btn" onclick="changeCode();"/>
							</div>
							<div class="col-sm-5" style="padding-right:0px;">
								<input type="text" name="judgeCode" placeholder="请输入图形验证码" class="form-control input-password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-6" style="padding-right: 0px;">
								<input disabled type="text" class="form-control" id="username_code"></input>
							</div>
							<div class="col-sm-3" style="padding-right:0px;">
								<input type="text" name="code" placeholder="验证码" class="form-control input-password">
							</div>
							<div class="col-sm-2">
								<button class="btn verifi_btn" id="verifi_btn" onclick="sendCode();return false;">获取</button>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" onclick="login(true)">提交</button>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
<script src="resource/js/jquery/jquery-3.1.0.min.js?version=${globalVersion}"></script>
<script src="resource/js/bootstrap/bootstrap.min.js?version=${globalVersion}"></script>
<script type="text/javascript">

	if(!'${param._top_}') {
		var url = window.location.href;
		if(url.indexOf('?') == -1) {
			url = url + "?";
		}
		url = url + "_top_=1";
		window.top.location.href = url;
	}

	function login(hasCode,isGrey) {

        var username = $("input[name='username']").val();
        $('#username_code').val(username);

        var params = $('#loginForm').serialize()
        if(hasCode){
			params += '&code=' + $("input[name='code']").val();
		}

		if(isGrey){
            params += '&isGrey=' + isGrey;
		}

	    $.post('${CTX_PATH}/security_login', params,
			function (data) {

			if(data.count == -1){

                //登陆成功
				var top = window.top;
				top.location.href = "${CTX_PATH}/";
                return;
			} else if(data.count == -100){

			    //需要输入验证码
				$('#myModal').modal();
				return;
            } else if(data.count == -99){

			    //灰度登录
                login(false,'isGrey');
            }

            if(data.message){
                alert(data.message);
            }

	        if(data.count >= 3){
	            window.location.reload();
			}
        });
	}

    $('.input-password').bind('keypress',function(event){
        if(event.keyCode == "13"){
            login();
        }
    });

    //倒计时
    function countDown() {
        var wait = 60;
        var timer = setInterval(function () {
            wait--;
            $('#verifi_btn').text(wait);
            $('#verifi_btn').addClass('not_btn').removeClass('verifi_btn');
            if (wait == 0) {
                clearInterval(timer);
                open = true;
                wait = 60;
                $('#verifi_btn').text('重新获取');
                $('#verifi_btn').removeClass('not_btn').addClass('verifi_btn');
            }
        }, 1000);
    }
    //倒计时操作
    var open = true;

    function sendCode(){

        if(!$("input[name='username']").val()){
            alert("请填写Email或手机");
            return;
        }

        if(open){

            $.ajax({
                "url" : "/login/sendCode",
                "method" : "POST",
                "data" : {
                    "username":$("input[name='username']").val(),
                    "judgeCode": $("input[name='judgeCode']").val()
				},
                "dataType" : "JSON",
                "success" : function(data) {

                    if(!data){
                        return alert("图形验证码错误");
					}else {
                        countDown();//有倒计时
                        open = false;
					}
                }
            });
        }else{
            return;
        }

    }


    //刷新验证码
    function changeCode() {
        $('.verifi_code_btn').hide().attr('src',
            '/login/code?' + Math.floor(Math.random() * 100)).fadeIn();
        event.cancelBubble = true;
    }


</script>