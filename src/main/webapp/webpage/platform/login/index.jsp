<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>登录日志列表&nbsp;&nbsp;
			</div>
			<div class="col-md-10">
				<form class="form-inline" id="queryform" action="loginLog">
					<nav class="text-right">
						<div class=" form-group">
							<input type="hidden" id="pageIndex" name="pageIndex" />
							<input name="keyword" type="text" style="width: 250px" id="queryBpName" value="${keyword }" class="form-control" placeholder="企业名称/用户名称/登录账号/城市" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="queryBillList()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
		<hr/>
		
		<div class="col-sm-12" id="billListDiv">	
			<table style="margin-bottom : 0px" class="table table-bordered">
				<thead>
					<tr>
					<tr>
						<th class="sort-column" >企业名称</th>
						<th class="sort-column" >用户名称</th>
						<th class="sort-column" >登录账号</th>
						<th class="sort-column" >IP</th>
						<th class="sort-column" >时间</th>
						<th style="min-width: 70px">城市</th>
					</tr>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="log">
						<tr>
							<td style="text-align:center;">${log.partnerName}</td>
							<td>${log.fullname}</td>
							<td>${log.username}</td>
							<td>${log.loginIp}</td>
							<td><fmt:formatDate value="${log.loginDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td>${log.loginCity}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryBillList"></jsp:include>
			</div>			
		</div>
	</div>
</div>

<div id="bpAccountFieldDiv"></div>
<script type="text/javascript" >

function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}

function queryBillList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("input[name='pageIndex']").val(pageIndex);
	
	$("#queryform").submit();
}

</script>