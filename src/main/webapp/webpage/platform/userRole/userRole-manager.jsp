<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="row row-bottom">
   <div class="col-lg-3 col-md-3 text-left"> 
		<a class="btn btn-default" onclick="back_user_change();" >返回</a> 
	</div>
	<div class="col-lg-9 col-md-9 text-right"> 
		<a class="btn btn-default"  role="button" onclick="addUserRole('','${userId}');">添加角色</a>
	</div>
</div>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>用户角色维护列表&nbsp;&nbsp; 共计<span>${count }</span>条数据   &nbsp;&nbsp;
             </div>
             <!-- /.panel-heading -->
             <div class="panel-body">
                 <div class="table-responsive">
                     <table class="table table-striped table-bordered table-hover" id="userTable">
                         <thead>
                             <tr>
                                 <th><label>单位</label></th> 
								 <th><label>角色</label></th>
                                 <th><label>授权范围</label></th>
                                 <th><label>有效期开始</label></th>
                                 <th><label>有效期结束</label></th>
                                 <th><label>操作</label></th>
                             </tr>
                         </thead>
                         <tbody>
                         	<c:forEach items="${pageList}" var="userRole">
	                             <tr class="odd gradeX">
	                                 <td>${userRole.org.name }</td> 
	                                 <td>${userRole.role.name }</td>
	                                 <c:set var="userVar" value="${fn:split(userRole.scope,',')}"/>
	                                 <td>
	                                    <c:forEach end="${fn:length(userVar)}" var="i" begin="0">
	                                      <c:if test="${userVar[i] == 'S'}">对该单位授权&nbsp;</c:if>
	                                      <c:if test="${userVar[i] == 'D'}">对直接下级单位授权&nbsp;</c:if>
	                                      <c:if test="${userVar[i] == 'C'}">对所有下级单位授权&nbsp;</c:if>
	                                    </c:forEach>
	                                 </td>
	                                 <td><fmt:formatDate value="${userRole.validFrom }" pattern="yyyy-MM-dd"/></td>
	                                 <td><fmt:formatDate value="${userRole.validTo }"   pattern="yyyy-MM-dd"/></td>
	                                 <td class="text-center">
	                                 	<a onclick="addUserRole('${userRole.id }','${userId}');">修改</a>&nbsp;&nbsp;
	                                 	<a href="javascript:void(0);" onclick="deleteUserRole('${userRole.id }','${userRole.role.name }');">删除</a>
	                                 </td>
	                             </tr>
                            </c:forEach>
                         </tbody>
                     </table>
                 </div>
                 <!-- /.table-responsive -->
             </div>
             <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
	</div>
</div>

<script>
function deleteUserRole(id,name){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除当前角色："+name+"?", 
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/user/userRole/delete",
				data : {
					id : id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						userRoleList('${userId}');
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});	
}
</script>