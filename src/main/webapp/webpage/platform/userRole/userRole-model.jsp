<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>角色信息<c:choose><c:when test="${userRole.id != '' && userRole.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
             	 &nbsp;&nbsp;<a data-pjax onclick="back_userRole();">返回</a>
             </div>
             <!-- /.panel-heading -->
             <div class="panel-body">
                 <form id="userRole_form" class="form-horizontal" method="post">
					  <input name="id" type="hidden" value="${userRole.id}"/>
					  <input name="userId" type="hidden" value="${userId}"/>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">单位:</label>
						<div class="input-group col-md-5" style="padding-left:14px;">
					      <input type="hidden" name="orgId" id="orgId2" value="${userRole.orgId }">
					      <input type="text" name="orgName" id="orgName2" class="form-control dropdown-org orgInput" value="${userRole.org.name }" readonly="readonly">							
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"
									onclick="selectDropdownOrg(this)">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
							</span>
						</div>
					  </div>					  
					  <div class="form-group">
					    <label class="col-sm-2 control-label">角色:</label>
						<div class="input-group col-md-5" style="padding-left:14px;">
							<input type="hidden" name="roleId" id="roleId" value="${userRole.roleId }">
					        <input type="text" name="roleName" id="roleName" class="form-control dropdown-role roleInput" readonly="readonly" value="<c:if test="${showNoRoleMessage != null}">${showNoRoleMessage}</c:if> ${userRole.role.name }">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button" onclick="selectDropdownRole(this)">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</button> 
							</span>
						</div>					    
					  </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">有效期开始:</label>
					    <p id="validForm" style="display:none;"><fmt:formatDate value="${userRole.validFrom }"   pattern="yyyy-MM-dd"/></p> 
					    <div class="col-sm-5">
					      <input type="text" id="validForm1" name="validFrom" class="form-control" value=""  onClick="WdatePicker()">
					    </div>
					  </div>					  
					 <div class="form-group">
					    <label class="col-sm-2 control-label">有效期结束:</label>
					    <p id="validTo" style="display:none;"><fmt:formatDate value="${userRole.validTo }"   pattern="yyyy-MM-dd"/></p> 
					    <div class="col-sm-5">
					      <input type="text" id="validTo1" name="validTo" class="form-control" value="2099-12-31"  onClick="WdatePicker()">
					    </div>
					  </div>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">授权范围:</label>
					    <div class="col-sm-6">
					      <input type="hidden" value="${userRole.scope }" id="scope"/>
					      <label class="checkbox-inline"><input type="checkbox" name="scope" value="S" id="checkbox1">对该单位授权</label>
					      <label class="checkbox-inline"><input type="checkbox" name="scope" value="D" id="checkbox2">对直接下级单位授权</label>
					      <label class="checkbox-inline"><input type="checkbox" name="scope" value="C" id="checkbox3">对所有下级单位授权</label>
					    </div>
					  </div>
					  <div class="form-group">
					    <div class="col-sm-offset-2 col-sm-10">
					      <button type="button" onclick="submit_userRole();" class="btn btn-default">提交</button>
					    </div>
					  </div>
					</form>
             </div>
             <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
	</div>
</div>
 <script>
 function selectDropdownOrg(dom){

		TR.select('dropdownOrg',{
		    type:'${type}',
			dom:dom
		},function(data){
			$("#orgName2").val(data.name);
			$("#orgId2").val(data.id);
		});
}

 function selectDropdownRole(dom){
		TR.select('dropdownRole',{
            type:'${type}',
			dom:dom,
			levels:1
		},function(data){
			$("#roleName").val(data.name);
			$("#roleId").val(data.id);
		});
}
 
function submit_userRole(){
	$("#userRole_form").submit();
}
  
$(document).ready(function() {
    if(!$("#orgId2").val()){
        $("#orgId2").val($("#orgId").val());
	}
	if(!$("#orgName2").val()){
        $("#orgName2").val($("#orgName").val());
	}

	$("#validForm1").val($("#validForm").html());
	var flag=$("#validTo").html();
	if(flag!=""){
     	$("#validTo1").val($("#validTo").html());
	}
	var scope=$("#scope").val().split(',');
	for(var i=0;i<scope.length;i++){
	    	$("[name=scope]:checkbox").each(function(){
	    		if($(this).val()==scope[i]){
	    		   $(this).attr("checked",true);
	    		}
	    	})
	}
});
$(document).ready(function() { 
	   $("#userRole_form").validate({
	        rules : {
	        	orgName:{
	                required:true
	            },
	            roleName:{
	            	required:true
	            },
	            scope:{
	            	required:true
	            },
				validFrom:{
					required:true
				},
				validTo:{
					required:true,
					endDate:true
				}
	        },
	        message:{
	        	scope:{
	        		required:"至少选择一个授权！"
	        	}
	        },
	        submitHandler : function(form) {
	            $(form).ajaxSubmit({
	                url:'platform/user/userRole/save',
	                success:function(data){
	                    if (data.type == "success") {
	                    	userRoleList('${userId}')
	                        showSuccessMsg(data.txt);
	                    } else {
	                        showErrorMsg(data.txt);
	                    }
	                }
	            });
	        },
	        errorPlacement : setErrorPlacement,
	        success : validateSuccess,
	        highlight : setHighlight
	    });
});
</script> 