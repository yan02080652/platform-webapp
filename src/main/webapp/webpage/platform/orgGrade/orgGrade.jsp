<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<div class="container-fluid">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
		        <div class="form-inline">
	           		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>组织层面 &nbsp;&nbsp;
	           		<div class="form-group" style="margin-left: 30px;">
	          		 	<%@ include file="/webpage/common/partnerChange.jsp"%>
	          		</div>
				</div>
			</div>
			<hr/>
			<div>
				<div class="panel-body" style="padding-top: 0px;padding-bottom: 5px">
					<button class="btn btn-default" onclick="addTopNode();" id="addTopNode">添加顶级</button>
					<button class="btn btn-default" onclick="addNode();" id="addNode">添加下级</button>
					<button class="btn btn-default" onclick="editNode();" id="editNode">编辑</button>
					<button class="btn btn-default" onclick="deleteNode();" id="deleteNode">删除</button>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
		             	<div class="panel-body">
							<div id="org_tree" class=""></div>
		            	</div>
		        	</div>
				</div>
				<div id="rightDetail" class="col-md-8 animated fadeInRight">
				</div>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
var currentNode;//左边树当前节点
var canAdd = true;

require(['treeview'],function(){//加载bootstrap treeview的资源 以及初始化树的数据
	reloadTree();
});

//重载树
function reloadTree(id) {
	$.ajax({
		type : 'GET',
		url : "platform/orgGrade/showMenu",
		success : function(data) {
		var treeMenuData = TR.turnToTreeViewData(data,{'id':'code','pid':'pcode'});
			$('#org_tree').treeview({
				data: treeMenuData,
				showBorder:false,
				highlightSelected:true,
				levels:4,
				onNodeSelected: function(event, data) {
					if(!canAdd){
						$('#org_tree').treeview('unselectNode', [ data.nodeId, { silent: true } ]);//取消当前选择
						if(currentNode != null){
							$('#org_tree').treeview('selectNode', [ currentNode.nodeId, { silent: true } ]);//不能编辑时还是选择上一个节点
						}
						$.alert("请编辑完当前节点!","提示");
						return;
					}else{
						//刷新右侧配置页面
						currentNode = data;
						reloadRightReadOnly(data.id,data.pid);
					}
				   
				},
			})
		}
	});
}

//添加顶级节点
function addTopNode(e) {
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
}
//添加顶级节点时 取消当前选中
if(currentNode!=null){
	$('#org_tree').treeview('unselectNode', [currentNode.nodeId, { silent: true }]);
}
currentNode = null;
reloadRight(null,null);
canAdd = false;
};

//加载右侧面板
function reloadRight(code,pcode) {
	$("#rightDetail").show();
	var path = 'platform/orgGrade/detail';
	$("#rightDetail").load(path, {'code' : code,'pcode':pcode},function(data){
		$('#orgGradeForm  :button').attr('disabled', false);
		$('#orgGradeForm input').attr('readonly', false);
		initForm();
	});
}

//只读
function reloadRightReadOnly(id,pid) {
	$("#rightDetail").show();
	var path = 'platform/orgGrade/detail';
	$("#rightDetail").load(path, {'code' : id,'pcode':pid},function(data){
		$('#orgGradeForm :button').attr('disabled', true);
		$('#orgGradeForm input').attr('readonly', true);
		initForm();
	});
}

function readOnly(){
	$("#nodeType1").attr('disabled', true);
	$('#orgGradeForm :button').attr('disabled', true);
	$('#orgGradeForm input').attr('readonly', true);
}

//新增下级
function addNode(){
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	if(currentNode==null || currentNode.data.nodeType==0){
		$.alert("请选择一个节点!","提示");
		return;
	}
	reloadRight(null,currentNode.id);
	canAdd = false;
}

//修改节点信息
function editNode() {
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	if(currentNode==null){
		$.alert("请选择一个节点!","提示");
		return;
	}
	$('#orgGradeForm :button').attr('disabled', false);
	$('#orgGradeForm input').attr('readonly', false);
	$("#code").attr('readonly', true);
	 canAdd = false;
};



	function initForm() {
		$("#orgGradeForm").validate({
			rules : {
				code : {
					required : true,
					minlength : 1,
					maxlength : 10,
					remote : {
						url : "platform/orgGrade/checkCode/",
						type : "post",
						data : {
							code : function() {
								return $("#code").val();
							},
							id:function(){
								return +$("#id").val();
							}
						},
						dataFilter : function(data) {
							if (data == 'ok') {
								return true;
							} else {
								return false;
							}
						}
					}
				},
				name : {
					required : true,
					minlength : 1,
					maxlength : 20
				},
				seq : {
					minlength : 1,
					digits : true,
					maxlength : 10
				},

			},
			messages : {
				code : {
					remote : "编码已存在"
				},
			},
			submitHandler : function(form) {
				$.ajax({
					cache : true,
					type : "POST",
					url : "platform/orgGrade/saveOrupdateOrg",
					data : $('#orgGradeForm').serialize(),
					async : false,
					error : function(request) {
						showErrorMsg("请求失败，请刷新重试");
						$('#mode_org').modal('hide');
					},
					success : function(data) {
						$('#mode_org').modal('hide');
						if (data.type == 'success') {
							showSuccessMsg(data.txt);
							reloadTree();
							readOnly();
							canAdd = true;
						} else {
							showErrorMsg(data.txt);
						}
					}
				});
			},
			errorPlacement : setErrorPlacement,
			success : validateSuccess,
			highlight : setHighlight
		});
	}
	
	//取消
	function cancel(){
		if(currentNode!=null && currentNode.id!=null){//说明是编辑的 重新点击该节点就可以了
			reloadRightReadOnly(currentNode.id,currentNode.pid);
		}else{
			$("#rightDetail").hide();
		}
		canAdd = true;
	}


	//删除节点
	function deleteNode() {
		if(!canAdd){
			$.alert("请编辑完当前节点!","提示");
			return;
		}
		if(currentNode==null){
			$.alert("请选择一个节点!","提示");
			return;
		}
		
		if (currentNode.nodes) {
			$.alert("父节点不允许直接删除,请先删除该节点下的所有子节点后重新操作")
			return;
		} 
		
			$.confirm({
				title : '提示',
				confirmButton : '确认',
				cancelButton : '取消',
				content : '确认删除当前节点吗?',
				confirm : function() {
					$.ajax({
						cache : true,
						type : "POST",
						url : "platform/orgGrade/deleteOrg",
						data : {
							id : currentNode.data.id
						},
						async : false,
						error : function(request) {
							showErrorMsg("请求失败，请刷新重试");
							canAdd = true;
						},
						success : function(data) {
							if (data.type == 'success') {
								showSuccessMsg(data.txt);
								reloadTree();
								$("#rightDetail").hide();
								canAdd = true;
								currentNode = null;
							} else {
								$.alert(data.txt, "提示");
							}
						}
					});
				},
				cancel : function() {
				}
			});
		}

	function submitOrgGrade() {
		$("#orgGradeForm").submit();
	}

// 	//重载树
// 	function reloadTree(code) {
// 		$.ajax({
// 			type : 'GET',
// 			url : "platform/orgGrade/showMenu",
// 			success : function(data) {
// 				pageVal.data = data || [];
// 				zTreeObj = $.fn.zTree.init($("#org_tree"), setting, pageVal.getNodeData());
// 				var treeNodes = zTreeObj.getNodesByParam("code", code, null);
// 				if (treeNodes.length > 0) {
// 					zTreeObj.selectNode(treeNodes[0]);
// 				}
// 			}
// 		});

// 	}
</script>