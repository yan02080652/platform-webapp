<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<div class="panel panel-default">
	<div class="panel-body ">
		<div class="modal-body">
			<form modelAttribute="orgGradeDto" class="form-horizontal" draggable="false" id="orgGradeForm" role="form">
			<input type="hidden" id="partnerId" name="partnerId" value="${orgGradeDto.partnerId}"/>
            	<input type="hidden" name="pcode" id="pcode" value="${orgGradeDto.pcode}"/>
            	<input type="hidden" name="id" id="id" value="${orgGradeDto.id}"/>
			   <div class="form-group">
			      <label for="code" class="col-sm-2 control-label">编码</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="code" name="code" value="${orgGradeDto.code}" placeholder="编码"/>
			      </div>
			   </div>
			   <div class="form-group">
				    <label for="name" class="col-sm-2 control-label">名称</label>
				    <div class="col-sm-10">
				   	  <input type="text" class="form-control" name="name" value="${orgGradeDto.name}" placeholder="名称"/>
				    </div>
				</div>
				<div class="form-group">
			      <label for="seq" class="col-sm-2 control-label">序号</label>
			      <div class="col-sm-10">
			         <input type="number" class="form-control" name="seq" value="${orgGradeDto.seq}" placeholder="序号"/>
			      </div>
			   </div>
				<div class="form-group" align="center">
					<button type="button" class="btn btn-default" disabled="disabled"
						onclick="cancel()">取消</button>
					&nbsp;&nbsp;
					<button type="button" class="btn btn-default" disabled="disabled"
						onclick="submitOrgGrade()">保存</button>
				</div>
			</form>
		</div>
	</div>
</div>
