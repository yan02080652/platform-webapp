<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<style>
/*select*/
.content > .sel {
    font-size: 14px;
    color: #3e3a39;
    font-weight: normal;
    width: 900px;
    /*height: 160px;*/
    border: 1px solid #dbdcdc;
    padding: 20px 30px;
    margin-bottom: 20px;
    margin-top: 10px;
}

a {
    color: inherit;
}
.sel input {
    height: 26px;
}
.sel label{
    display: inline-block;
    width: 70px;
}
.sel span {
    padding: 8px 10px;
    cursor: pointer;
}

.sel span:hover {
    color: white;
    background-color: #46a1bb;
}
.keyWords {
    padding-left: 8px;
    width: 358px;
    color: #888888;
    font-size: 14px;
    margin-right: 20px;
}
.sel .button {
    display: inline-block;
    background-color: #46a1bb;
    color: white;
    width: 90px;
    text-align: center;
    margin-left: 0;
    border-radius: 2px;
}

.sel .button:hover{
    background-color: #0d809b  ;
}

/*表格*/
.result {

}
.table {
   /*  width: 960px; */
}

.tipL{
    padding-left:30px;
}

.tipR{
    padding-right:30px;
}

table td{
    border-bottom: 1px solid #f7f7f7;
}

table  tr:hover{
    background-color: #f9f9f9;
}

table tr > th {
    background-color: #eee;
}

table tr > td, table tr > th {
    /*width: 160px;*/
    height: 40px;
    vertical-align: middle;
    text-align: left;
    padding-left: 20px;
    max-width: 280px;
}

td > p{
	height: 20px;
    padding-bottom: 3px;
}

.addWhiteList {
    color: #fff;
    background-color: #46a1bb;
    margin-bottom: 10px;
    margin-top:10px;
    cursor: pointer;
    width: 88px;
    height: 30px;
    text-align: center;
    line-height: 30px;
    border: none;
    border-radius: 2px;
}

.changeWhiteList {
    color:#46a1bb ;
    cursor: pointer;
    margin-right: 20px;
}

.deleteWhiteList {
    cursor: pointer;
    color:#46a1bb ;
}

.acName{
    width: 270px;
    height: 28px;
    margin-left: 20px;
    padding-left: 8px;
    margin-top: 20px;
    
}
</style>

<div class="main" style="margin-left: 15px;">
    <div class="sel">
    	<form id="whiteListQueryForm" action="ex/whlist">
    	<input name="pageIndex" id="whiteListPageIndex" type="hidden"/>
        <div>
            <label for="keyWords">关键词：</label>
            <input id="keyWords" class="keyWords" name="keyword" type="text" value="${keyword }" placeholder="请输入IP、说明">
            <span id="search" onclick="querywhiteList()" class="button">搜索</span>
        </div>
        </form>
    </div>
    <div class="result">
    	<p onclick="addWhiteList()" class="addWhiteList">+新增</p>
        <table class="table">
            <thead>
            <tr>
                <th class="tipL">IP</th>
                <th>说明</th>
                <th>截止时间</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
	            <c:forEach items="${pageList.list}" var="whiteList">
					<tr>
		                <td>${whiteList.ip }</td>
		                <td>${whiteList.name }</td>
		                <td><fmt:formatDate value="${whiteList.effectTime }" pattern="yyyy-MM-dd" /></td>
		                <td>
		                	<span onclick="editWhiteList('${whiteList.id }')" class="changeWhiteList">修改</span>
		                	<span onclick="deleteWhiteList('${whiteList.id }')" class="deleteWhiteList">删除</span>
		                </td>
		            </tr>
				</c:forEach>
            </tbody>

        </table>
    </div>
    <div class="pagin">
		<jsp:include page="../../common/pagination_ajax.jsp?callback=querywhiteList"></jsp:include>
	</div>
</div>

<div id="whiteListModelDiv"></div>

<script type="text/javascript">
$(document).ready(function() {
	
});

function querywhiteList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("#whiteListPageIndex").val(pageIndex);
	$("#whiteListQueryForm").submit();
}

function addWhiteList(){
	showWhiteListDetailModal("ex/whlist/detail", function() {
		$('#modelWhiteListDetail').modal();
	});
}

function showWhiteListDetailModal(url, callback) {
	$('#whiteListModelDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function editWhiteList(id){
	showWhiteListDetailModal("ex/whlist/detail?id=" + id, function() {
		$('#modelWhiteListDetail').modal();
	});
}

function deleteWhiteList(id){
	layer.confirm('确定删除此数据？',function (index) {
        /*确定操作*/
		$.ajax({
			cache : true,
			type : "POST",
			url : "ex/whlist/deleteWhiteList",
			data : {
				id : id
			},
			async : false,
			error : function(request) {
				showErrorMsg(data.txt);
			},
			success : function(data) {
				if (data.type == 'success') {
					window.location.href = "/ex/whlist";
				}
			}
		});
    },function (index) {
        
    });
}

</script>

