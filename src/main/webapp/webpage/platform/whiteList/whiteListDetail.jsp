<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="modal fade" id="modelWhiteListDetail" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">白名单维护</h4>
         </div>
         <div class="modal-body">
			<div class="row">
				<form modelAttribute="whiteListDto" class="form-horizontal" id="whiteListForm" role="form">
					 <input type="hidden" id="whiteListId" name="id" value="${whiteListDto.id}">
					 <input type="hidden" name="bpId" value="${whiteListDto.bpId }">
					 <div class="form-group">
					    <label class="col-sm-2 control-label">IP:</label>
					    <div class="col-sm-9">
					      <input type="text" name="ip" value="${whiteListDto.ip}" placeholder="IP" title="IP段用-号隔开" class="form-control"/>
					    </div>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">说明:</label>
					    <div class="col-sm-9">
					      <input type="text" name="name" value="${whiteListDto.name}" placeholder="说明" class="form-control"/>
					    </div>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">截止日:</label>
					    <div class="col-sm-9">
					      <input type="text" name="effectTime1" onclick="WdatePicker()" value="<fmt:formatDate value='${whiteListDto.effectTime}' pattern='yyyy-MM-dd' />" pattern="yyyy-MM-dd" placeholder="生效日" class="form-control"/>
					    </div>
					 </div>
				</form>
			</div>		
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="submitWhiteList()">保存
            </button>
         </div>
      </div>
	</div>
</div>

<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>

<script type="text/javascript">
 $(document).ready(function() {
	 initForm();
 });
 
 function submitWhiteList(){
	$("#whiteListForm").submit();
 }

function initForm(){
	$("#whiteListForm").validate({
		rules:{
			bpName:{
				required:true
			},
			effectTime1:{
				required:true
			}
		},
		submitHandler : function(form) {
			var dataParam = $("#whiteListForm").serialize();
			$.post("ex/whlist/saveOrUpdate",dataParam,
				function(data){
					if (data.type == 'success') {
						querywhiteList();
					}else{
						showErrorMsg(data.txt);
					}
				}
			);
		},
		errorClass: "promptError"
	});
};
 
 </script>
