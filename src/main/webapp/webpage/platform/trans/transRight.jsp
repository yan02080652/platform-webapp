<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
 <style>
 #trans-detail table{
 	margin-left: 15px;
 }
 #trans-detail table th{
 	width:75px;
 	font-size: 16px;
 }
 
</style>
<c:if test="${not empty transDto}">
	<!-- 作业信息面板 -->
	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>作业详细&nbsp;&nbsp;
		</div>
		<div class="panel-body">
			<div id="trans-detail">
				<table>
					<tr>
						<th>节点类型:</th>
						<td>
							<c:if test="${transDto.nodeType eq 1 }">目录</c:if> 
							<c:if test="${transDto.nodeType eq 0 }">作业</c:if>
						</td>
					</tr>
					<tr>
						<th>作业名称:</th>
						<td>${transDto.name }</td>
					</tr>
					<tr>
						<th>作业编码:</th>
						<td>${transDto.code }</td>
					</tr>
					<tr>
						<th>作业定义:</th>
						<td>
							<c:choose>
								<c:when test="${empty transDto.def }">无</c:when>
								<c:otherwise>${transDto.def }</c:otherwise>
							</c:choose>
						</td>
					</tr>
					<tr>
						<th>作业活动:</th>
						<td>
							<c:choose>
								<c:when test="${empty transDto.activitys }">无</c:when>
								<c:otherwise>${transDto.activitys }</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	
	<c:if test="${transDto.nodeType eq 0 }">
		<!-- 权限列表面板 -->
		<div class="panel panel-default" id="authtypeDiv">
			<div class="panel-heading">
				<span class="glyphicon glyphicon-home" aria-hidden="true"></span>作业权限列表&nbsp;&nbsp;
			</div>
			<div class="panel-body">
			<div class="col-sm-12" style="margin-bottom: 10px;">
					<div class="pull-left">
						<a class="btn btn-default" onclick="showAuthtypeTable()" role="button">添加</a>
					</div>
				</div>
			
			<div class="col-sm-12">
				<table id="contentTable"
					class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
					<thead>
						<tr>
							<th class="sort-column" >权限类型编码</th>
							<th class="sort-column" >权限类型名称</th>
							<th class="sort-column" >详细描述</th>
							<th style="max-width: 70px">操作</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${ not empty authtypeDtoList }">
							<c:forEach items="${authtypeDtoList}" var="authtypeDto">
							<tr id="${authtypeDto.id}">
								<td>${authtypeDto.code}</td>
								<td>${authtypeDto.name}</td>
								<td>${authtypeDto.descr}</td>
								<td>
									<a class="btn btn-default"  role="button" onclick="deleteTransAuthtype('${transDto.id }','${authtypeDto.id}','${authtypeDto.name}')">删除</a>	
								</td>
							</tr>
						</c:forEach>
							</c:when>
							<c:otherwise>
								<td colspan="4">
									<div class="text-center">没有相关数据!</div>
								</td>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			</div>
		</div>
	</c:if>
</c:if>

<script type="text/javascript">

function deleteTransAuthtype(transId,authtypeId,authtypeName){
	$.confirm({
		title : '提示',
		confirmButton : '确认',
		cancelButton : '取消',
		content : '确认删除权限'+authtypeName+'?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/trans/deleteTransType",
				data : {
					transId :transId,
					authtypeId:authtypeId
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadRight(transId);
					} else {
						$.alert(data.txt, "提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

function showAuthtypeTable() {
	var transId = '${transDto.id}';
	showModal("platform/trans/findAuthtypeList/"+transId, function() {
		$('#authtype_model').modal();
	});
}

//显示Model
function showModal(url, callback) {
	$('#ModelDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function addTransAuthtype(authtypeId){
	var transId = '${transDto.id}';
	$.ajax({
		url:"platform/trans/addTransAuthtype",
		type:"POST",
		data:{
			transId:transId,
			authtypeId:authtypeId
		},
		success:function(data){
			$('#authtype_model').modal('hide');
			if (data.type == 'success') {
				showSuccessMsg(data.txt);
				reloadRight(transId);
			} else {
				$.alert(data.txt, "提示");
			}
		}
	});
	
}

</script>