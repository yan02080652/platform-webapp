<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="mode_trans" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <c:choose>
            	<c:when test="${ empty transDto.id}">
            		<h4 class="modal-title">新增作业节点</h4>
            	</c:when>
            	<c:otherwise>
            		<h4 class="modal-title">修改作业节点</h4>
            	</c:otherwise>
            </c:choose>
         </div>
         <div class="modal-body">
         <form modelAttribute="transDto" class="form-horizontal" id="detailForm" role="form">
            	<input type="hidden" name="pid" id="pid" value="${transDto.pid}"/>
            	<input type="hidden" name="id" id="id" value="${transDto.id}"/>
				   <div class="form-group">
			     	 <label for="nodeType1" class="col-sm-2 control-label">节点类型</label>
			     	 <div class="col-sm-10">
			         	<select class="form-control" id="nodeType1" name="nodeType1">
					          <option value="-1">请选择节点类型</opton>
					          <option value="1">目录</option>
					          <option value="0">作业</option>
					    </select>
			     	 </div>
			  	 </div>
			   <div class="form-group">
			      <label for="name" class="col-sm-2 control-label">名称</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="name" name="name" value="${transDto.name}" placeholder="名称"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="code" class="col-sm-2 control-label">编码</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="code" name="code" value="${transDto.code}" placeholder="编码"/>
			      </div>
			   </div>
				<div class="form-group">
			      <label for="def" class="col-sm-2 control-label">定义</label>
			      <div class="col-sm-10">
			         <textarea class="form-control" id="def" name="def" rows="3" placeholder="定义">${transDto.def} </textarea>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="activitys" class="col-sm-2 control-label">活动</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="activitys" name="activitys" value="${transDto.activitys}" placeholder="格式：活动1:值1;活动2:值2"/>
			      </div>
			   </div>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="submit()">保存</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
         </div>
      </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var transId = "${transDto.id}";
		if(transId!=null && transId!=''){
			$("#nodeType1").val("${transDto.nodeType}");
		}
	});
</script>
