<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="authtype_model" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" 
               data-dismiss="modal" aria-hidden="true">
                  &times;
            </button>
            <h4 class="modal-title">权限项列表</h4>
         </div>
         <div class="modal-body">
         	<table id="contentTable" class="table  table-bordered table-hover table-condensed dataTables-example dataTable">
					<thead>
						<tr>
							<th class="sort-column" >权限类型编码</th>
							<th class="sort-column" >权限类型名称</th>
							<th class="sort-column" >详细描述</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${ not empty authtypeList }">
							<c:forEach items="${authtypeList}" var="authtypeDto">
							<tr id="${authtypeDto.id }">
								<td>${authtypeDto.code}</td>
								<td>${authtypeDto.name}</td>
								<td>${authtypeDto.descr}</td>
							</tr>
						</c:forEach>
							</c:when>
							<c:otherwise>
								<td colspan="4">
									<div class="text-center">没有相关数据!</div>
								</td>
							</c:otherwise>
						</c:choose>
					
					</tbody>
				</table>
         </div>
         <div class="modal-footer">
        	<button type="button" class="btn btn-default" onclick="clickAddButton()">添加</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
         </div>
      </div>
</div>
</div>
<script type="text/javascript">
$(function(){
		$("tbady ,tr").click(function(){
			var transId = $(this).attr("id");
			if(transId!=undefined){
				$("tbady ,tr").removeClass("active");
				$(this).addClass("active");
			}
		})
});

function clickAddButton(){
	var id = $("tr[class='active']").attr("id");
	if(id==undefined){
		$.alert("请先选中你要添加的权限项","提示");
	}else{
		addTransAuthtype(id);
	}
}


</script>
