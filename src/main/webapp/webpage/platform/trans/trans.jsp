<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="glyphicon glyphicon-home" aria-hidden="true"></span>作业配置
				</div>
				<div class="panel-body">
					<button class="btn btn-default" onclick="addTopNode();" id="addTopNode">添加顶级</button>
					<button class="btn btn-default" onclick="addNode();" id="addNode">添加下级</button>
					<button class="btn btn-default" onclick="editNode();" id="editNode">修改</button>
					<button class="btn btn-default" onclick="deleteNode();" id="deleteNode">删除</button>
				</div>
					<div id="trans_tree" style="margin-top: 5px;">
				</div>
			</div>
		</div>
		<!-- 右侧详细面板 -->
		<div id="rightDetail" class="col-md-8 animated fadeInRight">
			<jsp:include page="transRight.jsp"></jsp:include>
		</div>
	</div>
</div>
<div id="ModelDiv"></div>
<script type="text/javascript">

	var currentNode;//左边树当前节点
	
	require(['treeview'],function(){//加载bootstrap treeview的资源 以及初始化树的数据
		reloadTree();
	});
	
	//重载树
	function reloadTree(id) {
		$.ajax({
			type : 'GET',
			url : "platform/trans/showMenu",
			success : function(data) {
				var treeMenuData = TR.turnToTreeViewData(data);
				$('#trans_tree').treeview({
					data : treeMenuData,
					showBorder : false,
					highlightSelected : true,
                    levels:1,
					onNodeSelected : function(event, data) {
						//刷新右侧配置页面
						currentNode = data;
						reloadRight(data.id);
					}

				});
			}
		});
	}

	//加载右侧面板
	function reloadRight(id) {
		var path = 'platform/trans/detail';
		$("#rightDetail").load(path, {
			'id' : id
		});
	}

	//添加顶级节点
	function addTopNode() {
		showTransModal("platform/trans/add", {
			'pid' : null
		}, function() {
			$('#mode_trans').modal();
			initForm();
		});
	};

	//添加下级节点
	function addNode() {
		if (currentNode == null || currentNode.data.nodeType == 0) {
			$.alert("请选择一个目录节点!", "提示");
			return;
		}
		showTransModal("platform/trans/add", {
			'pid' : currentNode.id
		}, function() {
			$('#mode_trans').modal();
			initForm();
		});
	};

	//修改节点信息
	function editNode() {
		if (currentNode == null) {
			$.alert("请选择一个节点!", "提示");
			return;
		}
		showTransModal("platform/trans/getTrans", {
			'id' : currentNode.id
		}, function(a) {
			$('#mode_trans').modal();
			initForm();
		});
	};

	//显示Model
	function showTransModal(url, data, callback) {
		$('#ModelDiv').load(url, data, function(context, state) {
			if ('success' == state && callback) {
				callback();
			}
		});
	}

	function initForm() {
		$("#detailForm").validate(
				{
					rules : {
						nodeType1 : {
							type : true,
						},
						name : {
							required : true,
							minlength : 1,
							maxlength : 50,
						},
						code : {
							required : true,
							minlength : 1,
							maxlength : 50,
							remote : {
								url : "platform/trans/checkCode",
								type : "post",
								data : {
									code : function() {
										return $("#code").val();
									},
									id : function() {
										return $("#id").val();
									}
								},
								dataFilter : function(data) {
									if (data == 'ok') {
										return true;
									} else {
										return false;
									}
								}
							}
						},
					},
					messages : {
						code : {
							remote : "编码已存在"
						}
					},
					submitHandler : function(form) {
						$.ajax({
							cache : true,
							type : "POST",
							url : "platform/trans/saveOrupdate",
							data : $('#detailForm').serialize() + '&nodeType='
									+ $('#nodeType1').val(),
							async : false,
							error : function(request) {
								showErrorMsg("请求失败，请刷新重试");
								$('#mode_trans').modal('hide');
							},
							success : function(data) {
								$('#mode_trans').modal('hide');
								if (data.type == 'success') {
									showSuccessMsg("操作成功");
									reloadTree();
									reloadRight(data.txt);
								} else {
									showErrorMsg(data.txt);
								}
							}
						});
					},
					errorPlacement : setErrorPlacement,
					success : validateSuccess,
					highlight : setHighlight
				});
	}

	//删除节点
	function deleteNode() {
		if (currentNode == null) {
			$.alert("请选择一个节点!", "提示");
			return;
		}

		if (currentNode.nodes) {
			$.alert("父节点不允许直接删除,请先删除该节点下的所有子节点后重新操作");
			return;
		}
		$.confirm({
			title : '提示',
			confirmButton : '确认',
			cancelButton : '取消',
			content : '确认删除当前节点吗?',
			confirm : function() {
				$.ajax({
					cache : true,
					type : "POST",
					url : "platform/trans/delete",
					data : {
						id : currentNode.id
					},
					async : false,
					error : function(request) {
						showErrorMsg("请求失败，请刷新重试");
					},
					success : function(data) {
						if (data.type == 'success') {
							showSuccessMsg(data.txt);
							reloadTree();
							$("#rightDetail").hide();
							currentNode = null;
						} else {
							$.alert(data.txt, "提示");
						}
					}
				});
			},
			cancel : function() {
			}
		});
	}

	function submit() {
		$("#detailForm").submit();
	}
</script>