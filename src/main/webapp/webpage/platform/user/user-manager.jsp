<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="com.tem.platform.security.authorize.PermissionUtil" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">

             <div class="panel-body">
				<div class="row">
					<div class="col-md-12"> 
						<form class="form-inline" method="post" name="user-form">
							<input type="hidden" id="orgId" name="orgId" value="${orgId }"/>
							<input type="hidden" id="orgName"  value=""/>
							<div class=" form-group">
								<label>&nbsp;&nbsp;&nbsp;包含下级单位员工：</label> 
								<input type="hidden"   id="path" name="path" value=""/>
								<label><input type="checkbox" id="flag"/></label>  
							</div>
							<div class=" form-group"> 
								<label>&nbsp;&nbsp;&nbsp;姓名：</label> <input name="fullname" type="text" id="fullname"
									class="form-control" style="width:100px;"/>  
							</div>
							<div class="form-group">
								<label>&nbsp;&nbsp;&nbsp;手机号码：</label> <input name="mobile" type="text" id="mobile"
									class="form-control"  style="width:100px;"/>
							</div>
							<div class="form-group">
								<input type="button" onclick="search_user();" class="form-control" value="查找" />
								<input type="reset" id="reset-button" class="form-control" value="重置" /> 
							</div>
						</form>
					</div>
				</div>
				<div class="row row-bottom">
					<div class="col-md-12 text-right">
						<a class="btn btn-default" onclick="addUser('');" role="button">添加用户</a>
						<c:if test='<%=PermissionUtil.checkTrans("IPE_USER_M", "INIT_PWD") == 0%>'>
							<a class="btn btn-default" onclick="initAllUserPassword('');" role="button">重置所有用户密码</a>
						</c:if>
					</div>
				</div>   
				<div class="table-responsive" id="user_Table">

                 </div>
                
             </div>
      
         </div>
	</div>
</div>