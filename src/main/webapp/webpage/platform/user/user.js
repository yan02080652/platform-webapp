/**
 * 用户管理
 */
//左边树当前节点
var curAuthNode;
var sub = false;

require(['treeview'], function () {
    reloadTree();
});

function reloadTree() {
    $.ajax({
        type: 'GET',
        url: "platform/org/orgList",
        data : {partnerId : partnerId},
        success: function (data) {

            $('#treeUser').treeview({
                data: TR.turnToTreeViewData(data),
                showBorder: false,
                highlightSelected: true,
                onNodeSelected: function (event, data) {

                    curAuthNode = data;
                    $("#flag").attr("checked", false);
                    $("#orgId").val(data.id);
                    $("#orgName").val(data.text);
                    $("#path").val(data.data.path);

                    reloadRight(1);
                },
                onNodeUnselected: function (event, data) {
                    //刷新右侧配置页面
                    curAuthNode = null;
                }
            });
        }
    });
}

//加载
function reloadRight(pageIndex) {
    pageIndex = pageIndex ? pageIndex : 1;

    var orgId = $("#orgId").val();
    var orgName = $("#orgName").val();
    var fullname = $("#fullname").val();
    var mobile = $("#mobile").val();
    var path = $("#path").val();
    var flag = $("#flag").is(":checked") ? 1 : 0;

    $.post('platform/user/list', {
            orgId: orgId,
            fullname: fullname,
            mobile: mobile,
            path: path,
            flag: flag,
            pageIndex: pageIndex
        },
        function (data) {
            $("#user_Table").html("").html(data);
        }
    );
}

//初始化密码
function initPassword(id, name) {
    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: "初始化当前用户密码【" + name + "】?",
        confirm: function () {
            $.ajax({
                cache: true,
                type: "POST",
                url: "platform/user/initPassword",
                data: {
                    id: id
                },
                async: false,
                error: function (request) {
                    showErrorMsg("请求失败，请刷新重试");
                },
                success: function (data) {
                    if (data.type == 'success') {
                        showSuccessMsg(data.txt);
                        reloadRight(1);
                    } else {
                        $.alert(data.txt, "提示");
                    }
                }
            });
        },
        cancel: function () {

        }
    });
};

//初始化所有用户密码
function initAllUserPassword(id, name) {
    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: "确认初始化所有用户密码?",
        confirm: function () {
        	$.alert("重置密码执行中，请勿重复点击，完成后会有提示，可通过浏览器日志查看结果!",'提示');
        	setTimeout(function() {
        		$.ajax({
                    type: "GET",
                    url: "platform/user/initAllUserPassword?_dc=" + new Date().getTime(),
                    error: function (request) {
                        showErrorMsg("请求失败，请刷新重试");
                    },
                    success: function (data) {
                        if (data.type == 'success') {
                        	if(console && console.log) {
                        		console.log(data.txt);
                        	}
                            showSuccessMsg('重置成功，请通过浏览器日志查看结果。');
                            reloadRight(1);
                        } else {
                            $.alert(data.txt, "提示");
                        }
                    }
                });
        	}, 500);
        },
        cancel: function () {
        }
    });
};

//查找用户
function search_user(pageIndex) {
	if (!partnerId) {
        layer.alert("请选择企业");
        return;
    }
    reloadRight(pageIndex);
}

//重置按钮
$("#reset-button").click(function () {
    window.location.reload();
});

//添加修改用户
function addUser(userId) {

    if (!curAuthNode && !userId) {
        layer.alert("请选择单位");
        return;
    }

    $.post("platform/user/load", {
        type:type,
        id: userId
    }, function (data) {
        $("#user_detail").html(data);
    });
    $("#user_list").hide();
    $("#user_detail").show();
}

//返回用户列表 未对内容进行修改
function back_user() {

    reloadRight(1);
    $("#user_list").show();
    $("#user_detail").hide().html("");
    $("#userRole_list").hide().html("");
    $("#userRole_detail").hide().html("");
}

//返回用户列表  内容修改  用户角色修改
function back_user_change() {

    reloadRight(1);
    $("#user_list").show();
    $("#user_detail").hide().html("");
    $("#userRole_list").hide().html("");
    $("#userRole_detail").hide().html("");
}

//用户角色列表
function userRoleList(userId) {

    $.post("platform/user/userRole", {
        type:type,
        userId: userId
    }, function (data) {
        $("#userRole_list").html(data);
    });

    $("#user_list").hide();
    $("#user_detail").hide().html("");
    $("#userRole_list").show();
    $("#userRole_detail").hide().html("");
}

//添加修改用户角色
function addUserRole(id, userId) {

    $.post("platform/user/userRole/load", {
        id: id,
        userId: userId,
        type: type
    }, function (data) {
        $("#userRole_detail").html(data);
    });

    $("#userRole_list").hide();
    $("#userRole_detail").show();
}

//返回用户角色列表
function back_userRole() {

    $("#userRole_list").show();
    $("#userRole_detail").hide().html("");
}