<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<table class="table table-striped table-bordered table-hover" id="userTable">
    <thead>
    <tr>
        <th width="80px"><label>姓名 </label></th>
        <th width="100px"><label>职位</label></th>
        <th width="130px"><label>单位</label></th>
        <th width="85px"><label>手机号码</label></th>
        <th width="80px"><label>账号状态 </label></th>
        <th><label>角色列表</label></th>
        <th width="200px"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="user">
        <tr class="odd gradeX">
            <td>${user.fullname }</td>
            <td>${user.title}</td>
            <td>${user.org.name }</td>
            <td>${user.mobile }</td>
            <td><c:if test="${user.status eq 0}">未审核</c:if>
                <c:if test="${user.status eq 1}">已审核</c:if>
                <c:if test="${user.status eq -10}">已冻结</c:if>
                <c:if test="${user.status eq -99}">已失效</c:if>
            </td>
            <td>
                <c:forEach var="userRole" items="${user.userRole }" varStatus="status">
                    ${userRole.role.name }
                    <c:if test="${!status.last }">,</c:if>
                </c:forEach>
            </td>
            <td class="text-center">
                <a onclick="userRoleList('${user.id }')">角色设置</a>&nbsp;&nbsp;
                <a onclick="addUser('${user.id }')">修改</a>&nbsp;&nbsp;
                <a onclick="initPassword('${user.id }','${user.fullname }');">重置密码</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=search_user"></jsp:include>
</div>