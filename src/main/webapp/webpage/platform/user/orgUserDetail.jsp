<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="modal fade" id="modelOrgUser" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">${typeTitle}</h4>
            </div>
            <div class="modal-body">
                <form modelAttribute="orgUserDto" class="form-horizontal" id="formAddOrgUser" role="form">
                    <input type="hidden" id="orgUserId" name="id" value="${orgUserDto.id }"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">部门</label>
                        <div class="col-sm-7 input-group" style="padding-left:14px;width: 334px;">
                            <input type="hidden" name="orgId" id="orgId1" value="${orgUserDto.orgId }">
                            <input type="text" name="orgName" id="orgName1" class="form-control dropdown-org orgInput"
                                   value="${orgUserDto.org.name }" readonly="readonly">
                            <span class="input-group-btn">
						<button class="btn btn-default" type="button" data-value="${partnerType}" onclick="selectDropdownOrg(this)">
							<i class="fa fa-caret-down" aria-hidden="true"></i>
						</button>
					</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">岗位</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="title" value="${orgUserDto.title }"
                                   placeholder="岗位"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">是否主部门</label>
                        <div class="col-sm-7">
                            <label class="radio-inline"><input type="radio" value="1" name="main"/>是</label>
                            <lable class="radio-inline"><input type="radio" value="0" name="main"/>否</lable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">序号</label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" name="seq" value="${orgUserDto.seq }"
                                   placeholder="序号"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="submitOrgUser()">保存
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        var orgUserId = $("#orgUserId").val();
        if (orgUserId != null && orgUserId != '') {
            $("input:radio[value='${orgUserDto.main}']").attr('checked', 'true');
        }
    });
</script>