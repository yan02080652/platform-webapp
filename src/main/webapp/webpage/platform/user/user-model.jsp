<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>用户信息<c:choose><c:when
                    test="${user.id != '' && user.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
                &nbsp;&nbsp;<a data-pjax onclick="back_user();">返回</a>
            </div>
            <div class="panel-body">
                <form id="user_form" class="form-horizontal bv-form">
                    <input name="orgId" id="orgId2" type="hidden"/>
                    <input name="id" id="userId" type="hidden" value="${user.id}"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">姓名:</label>
                        <div class="col-sm-6">
                            <input type="text" name="fullname" class="form-control" value="${user.fullname }"
                                   placeholder="姓名">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">用户编码:</label>
                        <div class="col-sm-6">
                            <input type="text" name="empCode" class="form-control" value="${user.empCode }"
                                   placeholder="用户编码">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">用户类型:</label>
                        <div class="col-sm-6">
                            <select class="form-control" id="select-type" name="type">
                                <option value="0">可登录员工</option>
                                <option value="1">不可登录员工</option>
                                <option value="2">系统登录账号</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">性别:</label>
                        <div class="col-sm-6">
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="0"/> 男
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="1"/> 女
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="2"/> Other
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">是否Vip:</label>
                        <div class="col-sm-6">
                            <label class="radio-inline">
                                <input type="radio" name="vipLevel" value="0"/> 否
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="vipLevel" value="1"/> 是
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">绑定手机:</label>
                        <div class="col-sm-6">
                            <input type="text" name="mobile" id="check_mobile" class="form-control"
                                   value="${user.mobile }" placeholder="手机号码">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">绑定邮箱:</label>
                        <div class="col-sm-6">
                            <input type="text" name="email" id="check_email" class="form-control" value="${user.email }"
                                   placeholder="绑定邮箱">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">账号状态:</label>
                        <div class="col-sm-6">
                            <select id="select-status" class="form-control" name="status">
                                <option value="1">已审核</option>
                                <option value="0">未审核</option>
                                <option value="-10">已冻结</option>
                                <option value="-99">已失效</option>
                            </select>
                        </div>
                    </div>
                </form>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a class="btn btn-default" id="submit_user" onclick="submitUser()">保存基本信息</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>用户部门岗位
                &nbsp;&nbsp;&nbsp;&nbsp;<a data-pjax onclick="addOrgUser();">新增</a>
            </div>
        </div>
        <table id="treeTablePartnerTmc" class="table  table-bordered">
            <thead>
            <tr>
                <th style="text-align:center;">部门</th>
                <th style="text-align:center;">职位</th>
                <th style="text-align:center;">是否主部门</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${orgUsers }" var="orgUser">
                <tr>
                    <td>${orgUser.org.name }</td>
                    <td>${orgUser.title }</td>
                    <c:choose>
                        <c:when test="${orgUser.main == 1}">
                            <td><input type="checkbox" name="mainOrg" value="1" disabled="disabled" checked="checked">
                            </td>
                        </c:when>
                        <c:otherwise>
                            <td><input type="checkbox" name="mainOrg" value="0" disabled="disabled"></td>
                        </c:otherwise>
                    </c:choose>
                    <td>
                        <a onclick="editOrgUser('${orgUser.id }')">修改</a>&nbsp;&nbsp;
                         <c:if test="${isOnly > 1}">
                        <a onclick="deleteOrgUser('${orgUser.id }')">删除</a>&nbsp;&nbsp;
                        </c:if>
                        <c:if test="${orgUser.main == 0}">
                            <a onclick="setMainOrg('${orgUser.id }')">设为主部门</a>&nbsp;&nbsp;
                        </c:if>

                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>
<div id="orgUserDiv"></div>
<script>
	
    jQuery.validator.addMethod("checkPhoneAndEmail", function () {
        return $("#check_email").val() != "" || $("#check_mobile").val() != "";
    }, "手机和邮箱必须一个有值！");

    function selectDropdownOrg(dom) {
        TR.select('dropdownOrg', {
            dom: dom,
            type: $(dom).attr('data-value')
        }, function (data) {
            $("#orgName1").val(data.name);
            $("#orgId1").val(data.id);
            $("#orgId2").val(data.id);
        });
    }

    function addOrgUser() {
        if ($("#userId").val() != null && $("#userId").val() != '') {
            showModal("platform/user/addOrgUser", function () {
                $('#modelOrgUser').modal();
                initOrgUserForm();
            });
        } else {
            $.alert("请先保存基本信息!", "提示");
        }
    }

    function editOrgUser(id) {
        if ($("#userId").val() != null && $("#userId").val() != '') {
            showModal("platform/user/editOrgUser/" + id + "", function (a) {
                $('#modelOrgUser').modal();
                initOrgUserForm();
            });
        } else {
            $.alert("请先保存基本信息!", "提示");
        }
    }	
    function deleteOrgUser(id) {
        var userId = $("#userId").val();
        $.ajax({
            cache: true,
            type: "POST",
            url: "platform/user/deleteOrgUser",
            data: {
                id: id
            },
            async: false,
            error: function (request) {
                showErrorMsg("请求失败，请刷新重试");
            },
            success: function (data) {
                if (data.type == 'success') {
                    addUser(userId);
                } else {
                    $.alert(data.txt, "提示");
                }
            }
        });
    }

    function setMainOrg(id) {
        var userId = $("#userId").val();
        $.ajax({
            cache: true,
            type: "POST",
            url: "platform/user/setMainOrg",
            data: {
                id: id
            },
            async: false,
            error: function (request) {
                showErrorMsg("请求失败，请刷新重试");
            },
            success: function (data) {
                if (data.type == 'success') {
                    addUser(userId);
                } else {
                    $.alert(data.txt, "提示");
                }
            }
        });
    }

    function showModal(url, callback) {
        $('#orgUserDiv').load(url, function (context, state) {
            if ('success' == state && callback) {
                callback();
            }
        });
    }

    function initOrgUserForm() {
        $("#formAddOrgUser").validate({
            rules: {
                title: {
                    required: true,
                    minlength: 1
                },
                orgName: {
                    required: true,
                    remote: {
                        url: "platform/user/checkOrgUser",
                        data: {
                            userId: function () {
                                return $('#userId').val();
                            },
                            orgId: function () {
                                return $('#orgId1').val();
                            },
                            orgUserId: function () {
                                return $('#orgUserId').val();
                            }
                        }
                    }
                },
                main: {
                    required: true
                }
            },
            messages: {
                orgName: {
                    remote: "已关联了此部门！"
                }
            },
            submitHandler: function (form) {
                var userId = $("#userId").val();

                var data = $('#formAddOrgUser').serialize();
                data = data + "&userId=" + userId + "&partnerId=" + partnerId;

                $.ajax({
                    cache: true,
                    type: "POST",
                    url: "platform/user/saveOrUpdateOrgUser",
                    data: data,
                    async: false,
                    error: function (request) {
                        showErrorMsg("请求失败，请刷新重试");
                    },
                    success: function (data) {

                        if (data.type == 'success') {
                            $('#modelOrgUser').modal('hide');
                            setTimeout(function () {
                                addUser(userId);
                            }, 500);

                        } else {
                            showErrorMsg(data.txt);
                        }
                    }
                });

            },
            errorPlacement: setErrorPlacement,
            success: validateSuccess,
            highlight: setHighlight
        });
    }

    function submitOrgUser() {
        $('#formAddOrgUser').submit();
    }

    $(document).ready(function () {

        $("#orgId1").val($("#orgId").val());
        $("#orgId2").val($("#orgId").val());
        $("#orgName1").val($("#orgName").val());
        var userId = $("#userId").val();
        if (userId != null && userId != '') {
            $("#select-type").val("${user.type}");
            $("#select-status").val("${user.status }");
            $("input[name='gender'][value='${user.gender}']").attr('checked', 'true');
            $("input[name='vipLevel'][value='${user.vipLevel}']").attr('checked', 'true');
        }

    });

    function submitUser() {
        sub = false;
        $('#user_form').submit();
    }

    $(document).ready(function () {
        $("#user_form").validate({
            rules: {
                fullname: {
                    required: true,
                    minlength: 1
                },
                title: {
                    required: true,
                    minlength: 1
                },
                empCode: {
                    remote: {
                        url: 'platform/user/checkEmpCode',
                        data: {
                            userId: function () {
                                return $('#userId').val();
                            },
                            partnerId: partnerId
                        }
                    }
                },
                email: {
                    email: true,
                },
                mobile: {
                    mobile: true,
                    remote: {
                        url: "platform/user/checkMobile",
                        data: {
                            userId: function () {
                                return $('#userId').val();
                            },
                            partnerId: partnerId
                        }
                    }
                },
                gender: {
                    required: true
                },
                orgName: {
                    required: true
                }
            },
            messages: {
                mobile: {
                    remote: "此手机号码已经注册！"
                },
                empCode: {
                    remote: "此编码已经使用！"
                }
            },
            submitHandler: function (form) {
                if (sub == true) {
                    return;
                }
                sub = true;
                $.ajax({
                    cache: true,
                    type: "POST",
                    url: "platform/user/save",
                    data: $('#user_form').serialize() + "&partnerId=" + partnerId,
                    async: false,
                    error: function (request) {
                        showErrorMsg("请求失败，请刷新重试");
                    },
                    success: function (data) {

                        if (data.type == 'success') {
                            $.alert("保存成功!", "提示");
                            setTimeout(function () {
                                addUser(data.txt);
                            }, 500);
                        } else {
                            showErrorMsg(data.txt);
                        }

                    }
                });
            },
            errorPlacement: setErrorPlacement,
            success: validateSuccess,
            highlight: setHighlight
        });
    });
</script> 