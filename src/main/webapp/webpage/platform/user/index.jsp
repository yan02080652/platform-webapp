<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<script>
    var partnerId = '${partnerId}';
</script>
<!-- 用户列表页 -->
<div id="user_list">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-inline">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span>用户管理 &nbsp;&nbsp;
                    <c:if test="${type ne 'me'}">
                        <div class="form-group" style="margin-left: 30px;">
                            <%@ include file="/webpage/common/partnerChange.jsp" %>
                        </div>
                    </c:if>
                </div>
            </div>
            <br>
            <hr/>
            <div>
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <ul id="treeUser" style="padding-left: 10px;"></ul>
                        </div>
                    </div>
                </div>
                <div id="rightMenuDetail" class="col-md-9 animated fadeInRight">
                    <jsp:include page="user-manager.jsp"></jsp:include>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 用户详细页面 -->
<div id="user_detail" style="display:none;">
</div>
<!--用户角色列表页 -->
<div id="userRole_list" style="display:none;">
</div>
<!--用户角色详细页 -->
<div id="userRole_detail" style="display:none;">
</div>
<script>
    var type = '${type}';
</script>
<script type="text/javascript" src="webpage/platform/user/user.js?version=${globalVersion}"></script>