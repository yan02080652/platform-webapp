<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ page import="com.tem.platform.security.authorize.PermissionUtil" %>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>mq消息记录&nbsp;&nbsp;
			</div>
			<div class="col-md-10">
				<form class="form-inline" id="queryform" action="mqRecord/index">
					<nav class="text-right">
						<input type="hidden" id="pageIndex" name="pageIndex" />
						<%--<div class=" form-group input-group">--%>
							<%--<input name="startDate" type="text" value="<fmt:formatDate value="${condition.startDate }" pattern="yyyy-MM-dd"/>" onclick="WdatePicker()" class="form-control" style="width: 105px" placeholder="开始时间" /><span class="input-group-btn"/>----%>
							<%--<input name="endDate" type="text"  value="<fmt:formatDate value="${condition.endDate }" pattern="yyyy-MM-dd"/>" onclick="WdatePicker()" class="form-control" style="width: 105px" placeholder="结束时间" /><span class="input-group-btn"/>--%>
						<%--</div>--%>

						<div class=" form-group input-group">
							<input name="eventId" type="text" value="${condition.eventId}" class="form-control"placeholder="消息主键" />
						</div>
						<div class=" form-group">
							发送状态
							<select class="form-control" name="status" placeholder="发送状态">
								<option value="0">发送中</option>
								<option value="1">发送成功</option>
								<option value="2">发送失败</option>
							</select>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default" onclick="queryList()">查询</button>
							<button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
		<hr/>
		
		<div class="col-sm-12" id="billListDiv">	
			<table style="margin-bottom : 0px" class="table table-bordered">
				<thead>
					<tr>
					<tr>
						<th class="sort-column" >主键</th>
						<th class="sort-column" >事件编码</th>
						<th class="sort-column" style="max-width: 300px;">发送内容</th>
						<th class="sort-column" >批次号</th>
						<th class="sort-column" >状态</th>
						<th class="sort-column" >发送时间</th>
						<th style="min-width: 70px">操作</th>
					</tr>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="log">
						<tr>
							<td style="text-align:center;">${log.id}</td>
							<td style="text-align:center;">${log.eventCode}</td>
							<td style="max-width: 300px;" title="${log.eventData}">
									${log.eventData}
							</td>
							<td>${log.batchId}</td>
							<td>
								<c:if test="${log.state eq '0'}">待发送</c:if>
								<c:if test="${log.state eq '1'}">发送成功</c:if>
								<c:if test="${log.state eq '2'}">发送失败</c:if>
							</td>
							<td><fmt:formatDate value="${log.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td>
								<c:if test="${log.state eq '2'}">
									<a class="btn btn-default" onclick="send('${log.id}');" href="javascript:;" role="button">再次发送</a>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryList"></jsp:include>
			</div>			
		</div>
	</div>
</div>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script type="text/javascript" >

$(function () {
    var messageStatus = '${condition.status}';
    $("select[name='status']").val(messageStatus);

});

function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}

function queryList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("input[name='pageIndex']").val(pageIndex);
	
	$("#queryform").submit();
}

function send(mqMsgId) {
	$.post('mqRecord/send',{
        mqMsgId : mqMsgId
	},function (rs) {
		if(rs){
		    layer.msg("发送成功");
            window.location.reload();
		}else{
            layer.msg("发送失败");
		}
    })
}

</script>