/**
 * 用户管理
 */
//左边树当前节点
var currentNode;
var sub = false;
var canAdd = true;
var curTransTreeNode;
var transType;

require(['treeview'], function () {
    reloadTree();
});


function reloadTree(id) {
	$.ajax({
		type : 'GET',
		url : "platform/roleAuthType/showMenu",
		data:{"partnerId" : $("#partnerIdHeader").val()},
		success : function(data) {
		var treeMenuData = TR.turnToTreeViewData(data);
			$('#role_tree').treeview({
				data: treeMenuData,
				showBorder:false,
				highlightSelected:true,
				levels:4,
				onNodeSelected: function(event, data) {
					if(!canAdd){
						$('#role_tree').treeview('unselectNode', [ data.nodeId, { silent: true } ]);//取消当前选择
						if(currentNode != null){
							$('#role_tree').treeview('selectNode', [ currentNode.nodeId, { silent: true } ]);//不能编辑时还是选择上一个节点
						}
						$.alert("请编辑完当前节点!","提示");
						return;
					}else{
						//刷新右侧配置页面
						currentNode = data;
						reloadRightList(1,data.id,$("#partnerIdHeader").val());
					}
				   
				},
			});
		}
	});
}

//换页
function search_group(pageIndex) {
	
	reloadRightList(pageIndex,currentNode.id,$("#partnerIdHeader").val());
}

//加载
function reloadRightList(pageIndex,id,partnerId) {
	
	if(!$("#partnerIdHeader").val()){

		partnerId = 0;
	}
	
    $.post('platform/roleAuthType/list', {
    		id:id,
    		partnerId:partnerId,
            pageIndex: pageIndex
        },
        function (data) {
            $("#role_Table").html("").html(data);
        }
    );
}

//选择作业
function openTransDia(num) {
	transType = num;
	showModalTrans("platform/roleAuthType/selectTrans", function() {
		
		$('#modelTransNode').modal();
		$.ajax({
			type : 'GET',
			url : "platform/trans/showMenu",
			success : function(data) {
				var treeMenuData = TR.turnToTreeViewData(data);
				$('#selectTransTree').treeview({
					data: treeMenuData,
					showBorder:false,
					highlightSelected:true,
					//selectedBackColor:'#808080',
					onNodeSelected: function(event, data) {
						curTransTreeNode = data;
					},
					onNodeUnselected: function(event, data) {
						curTransTreeNode = null;
					}
				});
			}
		});
	});
}

function showModalTrans(url, callback) {
	
	$('#transTreeDiv').load(url, function(context, state) {
			
		if ('success' == state && callback) {
			callback();
		}
	});
}

function selectTranNode(){
	if(curTransTreeNode == null || curTransTreeNode.data.nodeType!=0){
		$.alert("请先选择一个作业节点!","提示");
		return;
	}
	$('#modelTransNode').modal('hide');
	if(transType == '1'){		
		$('#def').val(curTransTreeNode.data.name+"("+curTransTreeNode.data.code+")");
		$('#defCode').val(curTransTreeNode.data.code);
	}else if(transType == '2'){
		$('#trans-def').val(curTransTreeNode.data.name+"("+curTransTreeNode.data.code+")");
		$('#value').val(curTransTreeNode.data.code);
		$('#trans-activity').val(curTransTreeNode.data.activitys);
	}	
	
}

//查找作业
function searchRole() {
	if (!currentNode) {
        layer.alert("请选择角色");
        return;
    }
	var id = currentNode.id;
	var partnerId = $("#partnerIdHeader").val();
	if(!$("#partnerIdHeader").val()){
		partnerId = 0;
	}
	
	var value = $("#defCode").val();
    reloadRightList1(id,partnerId,value);
}

function reloadRightList1(id,partnerId,value) {
	
    $.post('platform/roleAuthType/list', {
    		id:id,
    		partnerId:partnerId,
            pageIndex: 1,
            value: value
        },
        function (data) {
        	
            $("#role_Table").html("").html(data);
        }
    );
}


//添加修改授权组
function addGroup(groupId) {
	if(!currentNode){
		layer.alert("请选择角色");
        return;
	}	
	showGroupModal("platform/roleAuthType/detail?groupId=" + groupId +"&partnerId=" + $("#partnerIdHeader").val() +"&roleId="+currentNode.id , function() {
		$('#modeGroup').modal();
		initForm();
	});
	
};

//显示Model
function showGroupModal(url, data, _callback) {
	$('#addRoleDiv').load(url, data, function(context, state) {
		if ('success' == state && _callback) {
			_callback();
		}
	});
}

function initForm() {
    var rules = {
        value: {
            required: true
        },
       
    };


    $("#roleGroupForm").validate({
        rules: rules,
        errorPlacement: setErrorPlacement,
        success: validateSuccess,
        highlight: setHighlight,
        submitHandler: function (form) {
        	
        	 $.post("platform/roleAuthType/save",$("#roleGroupForm").serialize(),
                     function(data){
                         if (data.code == '0') {
                        	 $('#modeGroup').modal('hide');
                             layer.msg("保存成功");
                             reloadRightList(1,currentNode.id,$("#partnerIdHeader").val());
                         }else{
                        	 alert(data.data);
                             layer.msg(data.data);
                         }
                     }
                 );
        }
    });
};

function saveForm() {
    $("#roleGroupForm").submit();
}

