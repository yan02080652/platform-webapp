<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="modelTransNode" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">选择作业</h4>
         </div>
         <div class="modal-body">
         	<ul id="selectTransTree" class=""></ul>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="selectTranNode()">确定
            </button>
         </div>
      </div>
</div>