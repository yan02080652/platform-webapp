<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .popover.right > .arrow {
        top: 50% !important;
    }

    .control-label .reqMark {
        color: red;
        font-weight: bold;
        font-size: 15px;
    }

    #freightForm .btn-success {
        background-color: #46A2BB;
        border-color: #46A2BB;
    }
</style>

<div class="modal fade" id="modeGroup" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">
                    <c:choose>
                        <c:when test="${not empty roleGroupForm.id}">
                            修改
                        </c:when>
                        <c:otherwise>
                            新增
                        </c:otherwise>
                    </c:choose>
                </h4>
            </div>
            <div class="modal-body">
                <form id="roleGroupForm" class="form-horizontal">
                    <input type="hidden" name="id"  value="${roleGroupForm.id}">
                    <input type="hidden" name="partnerId"  value="${roleGroupForm.partnerId}">
                    <input type="hidden" name="roleId"  value="${roleGroupForm.roleId}">
                    <input type="hidden" name="groupId"  value="${roleGroupForm.groupId}">
                    <div class="form-group">		
						<label for="def" class="col-sm-3 control-label" >作业:</label> 	
						<div class="col-sm-6"  >
						    <div id="menuDefDiv" class="col-sm-12 input-group" >
						   	  <input type="hidden" id="value" class="form-control" name="value" value="${roleGroupForm.value}"/>
						   	  <input type="text" id="trans-def" class="form-control"  name="def" value="${roleGroupForm.value}" readonly="readonly" style="width:200px;" placeholder="选择作业"/>
		            		  </button>
		            		  <div class="input-group-btn">
						         <button type="button" id="selectTransButton" class="btn btn-default"  onclick="openTransDia('2')">选择
						       </div>
						    </div>
					    </div>
					</div>
					
                    <div class="form-group">
                        <label class="col-sm-3 control-label">作业活动:</label>
                        <div class="col-sm-6">
                            <input id="activity" type="text" class="form-control" name="activity" value="${roleGroupForm.activity}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">作业活动值:</label>
                        <div class="col-sm-6">
                            <input type="text" id="trans-activity" class="form-control" name="activitys" readonly="readonly" value="${roleGroupForm.activitys}">
                        </div>
                    </div>
                   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="saveForm()">保存
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="webpage/platform/roleAuthType/roleAuthType.js?version=${globalVersion}"></script>


<script>
//     $(function () {
//     	settleChannel.detail.init();
//     })
</script>