<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">

             <div class="panel-body">
				<div class="row">
					<div class="col-md-12"> 
						<form class="form-inline" method="post" name="user-form">
							<div class="form-group">
								
								<label for="def" class="col-sm-3 " align="center">作业编码:</label> 
							
							
								<div class="col-sm-8" style="padding-left: 0px;" align="center">
								    <div id="menuDefDiv" class="col-sm-12 input-group" >
								   	  <input type="hidden" id="defCode" class="form-control" name="defCode" value="${menuDto.def}"/>
								   	  <input type="text" id="def" class="form-control"  name="def" value="${menuDto.def}" style="width:300px;" readonly="readonly" placeholder="选择作业"/>
				            		  </button>
				            		  <div class="input-group-btn">
								         <button type="button" id="selectTransButton" class="btn btn-default"  onclick="openTransDia('1')">选择作业
								       </div>
								    </div>
							    </div>
							
							</div>
							
							<div class="form-group" style="padding-left:60px;">
								<input type="button" onclick="searchRole();" class="form-control" value="查询" />
							</div>
						</form>
					</div>
				</div>
				<div class="row row-bottom">
					<div class="col-md-12 text-right">
						<a class="btn btn-default" onclick="addGroup('');" role="button">添加授权组</a>
					</div>
				</div>   
				<div class="table-responsive" id="role_Table">

                 </div>
                
             </div>
      <div id="addRoleDiv"></div>
         </div>
	</div>
</div>