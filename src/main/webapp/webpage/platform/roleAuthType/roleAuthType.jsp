<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="container-fluid" >
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
		        <div class="form-inline">
	           		<div class="glyphicon glyphicon-home" aria-hidden="true"></div>作业设置  &nbsp;&nbsp;
	           		<div class="form-group" style="margin-left: 30px;">
	          		 	<%@ include file="/webpage/common/partnerChange.jsp"%>
	          		</div>
				</div>
			</div>
			<hr/>
			<div>
				<div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <ul id="role_tree" style="padding-left: 10px;"></ul>
                        </div>
                    </div>
                </div>
					 <div id="rightDetail" class="col-md-9 animated fadeInRight">
                    	<jsp:include page="role-manager.jsp"></jsp:include>
                	</div>
				<div id="transTreeDiv"></div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript" src="webpage/platform/roleAuthType/roleAuthType.js?version=${globalVersion}"></script>