<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<table class="table table-striped table-bordered table-hover" id="userTable">
    <thead>
    <tr>
        <th ><label>授权组标识 </label></th>
        <th ><label>子字段</label></th>
        <th ><label>子字段值</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="roleAuth">
        <c:if test="${roleAuth.fieldId eq 'TRANSID'}">
        <tr class="odd gradeX">
            <td rowspan="2" class="text-center"><a onclick="addGroup('${roleAuth.groupId }')">${roleAuth.groupId }</a></td>
            <td class="text-center">作业
               </td>
            <td>${roleAuth.value }</td> 
            
       </tr>
       </c:if>
            <c:if test="${roleAuth.fieldId eq 'ACTIVITY'}">
            <tr class="odd gradeX">
            <td  style= "text-align: center"> 
             	   活动</td>
            <td>${roleAuth.value }</td> 
            
        	</tr>
            </c:if>
    </c:forEach>
    </tbody>
</table>

<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=search_group"></jsp:include>
</div>