<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ page import="com.tem.platform.security.authorize.PermissionUtil" %>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>短信消息记录&nbsp;&nbsp;
			</div>
			<div class="col-md-10">
				<form class="form-inline" id="queryform" action="messageRecord/index">
					<nav class="text-right">
						<input type="hidden" id="pageIndex" name="pageIndex" />
						<div class="form-group selectHeader" style="margin-left: 30px;">
							<jsp:include page="../../common/businessSelecter.jsp"></jsp:include>
						</div>
						<div class=" form-group input-group">
							<input name="startDate" type="text" value="<fmt:formatDate value="${condition.startDate }" pattern="yyyy-MM-dd"/>" onclick="WdatePicker()" class="form-control" style="width: 105px" placeholder="开始时间" /><span class="input-group-btn"/>--
							<input name="endDate" type="text"  value="<fmt:formatDate value="${condition.endDate }" pattern="yyyy-MM-dd"/>" onclick="WdatePicker()" class="form-control" style="width: 105px" placeholder="结束时间" /><span class="input-group-btn"/>
						</div>
						<div class=" form-group">
							发送状态
							<select class="form-control" name="status" placeholder="发送状态">
								<option value="">全部</option>
								<option value="0">发送失败</option>
								<option value="1">发送中</option>
								<option value="2">发送成功</option>
							</select>
						</div>
						<br>
						<div class=" form-group" style="margin:10px;">
							手机号码
							<input name="mobile" type="text" id="mobile" value="${condition.mobile }"
									class="form-control" style="width:150px;"/>
						</div>
						<div class=" form-group">
							短信内容
							<input name="content" type="text" id="content" value="${condition.content }"
									class="form-control" style="width:300px;"/>
						</div>

						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="queryBillList()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetForm()">重置</button>

							<c:if test='<%=PermissionUtil.checkTrans("MESSAGE_RECORD","M") == 0%>'>
								<button type="button" class="btn btn-default" onclick="goToSendSmS()">发送短信</button>
							</c:if>
						</div>
					</nav>
				</form>
			</div>
		</div>
		<hr/>
		
		<div class="col-sm-12" id="billListDiv">	
			<table style="margin-bottom : 0px" class="table table-bordered">
				<thead>
					<tr>
					<tr>

						<th class="sort-column" >标题</th>
						<th class="sort-column" >内容</th>
						<th class="sort-column" >手机号码</th>
						<th class="sort-column" >发送时间</th>
						<!-- <th class="sort-column" >完成时间</th> -->
						<th class="sort-column" >状态</th>
						<th class="sort-column" style="min-width: 110px" >说明</th>
						<th style="min-width: 70px">操作</th>
					</tr>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="log">
						<tr>
							<td style="text-align:center;">${log.title}</td>
							<td style="max-width: 300px;">${log.content}</td>
							<td>${log.mobile}</td>
							<td><fmt:formatDate value="${log.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<%-- <td><fmt:formatDate value="${log.finishTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td> --%>
							<td>
								<c:if test="${log.status eq '0'}">发送失败</c:if>
								<c:if test="${log.status eq '1'}">发送中</c:if>
								<c:if test="${log.status eq '2'}">发送成功</c:if>
							</td>
							<td>
								${log.failReason}
							</td>
							<td><a class="btn btn-default" onclick="sendSms('${log.id}');" href="javascript:;" role="button">再次发送</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryBillList"></jsp:include>
			</div>			
		</div>
	</div>
</div>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script type="text/javascript" >

$(function () {
    var messageStatus = '${condition.status}';
    $("select[name='status']").val(messageStatus);

    var tmcId = '${tmcId}';
    var tmcName = '${tmcName}';

    var partnerId = '${partnerId}';
    var bpName = '${bpName}';

    if (!partnerId.isEmpty()){
        $("#queryBpId").val(partnerId);
        $("#queryBpName").val(bpName);
    }

    if(!tmcId.isEmpty()) {
        $("#queryBpTmcId").val(tmcId);
        $("#queryBpTmcName").val(tmcName);
    }

});

function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}

function queryBillList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("input[name='pageIndex']").val(pageIndex);
	
	$("#queryform").submit();
}

function sendSms(id) {
	$.post('messageRecord/smsSend',{
	    id : id
	},function (rs) {
		if(rs){
		    layer.msg("发送成功");
            window.location.reload();
		}else{
            layer.msg("发送失败");
		}
    })
}

function goToSendSmS() {
	window.location.href= '/messageRecord/smsIndex';
}

</script>