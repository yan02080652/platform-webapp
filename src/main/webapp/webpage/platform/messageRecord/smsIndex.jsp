<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel panel-default">
	<div class="panel-heading">
				<i class="fa fa-bars" aria-hidden="true"></i> 发送短信
		<a style="color:#337ab7" href="messageRecord/index">返回</a>
	</div>
	<div class="panel-body" style="padding: 0px;">
		<div class="modal-body" style="padding-bottom: 0px;">
			<form class="form-horizontal" draggable="false" id="smsContentForm" role="form">

				<div  class="form-group">
					<label class="col-sm-2 control-label">企业：</label>
					<input type="hidden"  required="required"  id="queryBpId" name="partnerId" />
					<div class="col-sm-4">
						<input name="bpName" required="required" class="form-control" type="text"  id="queryBpName" onclick="selectPartner()" placeholder="选择企业" />
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">手机号码</label>
					<div class="col-sm-4">
						<input type="text" required="required" class="form-control" name="mobile" placeholder="多个手机号码,请用逗号隔开" />
					</div>
				</div>
				<div class="form-group">
					<label  class="col-sm-2 control-label">标题</label>
					<div class="col-sm-4">
						<input type="text" required="required" class="form-control" id="roleCode" name="title" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">短信内容</label>
					<div class="col-sm-4">
						<textarea class="form-control" required="required"
							 name="content" rows="5"></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-2"></div>
					<div class="col-sm-4">
						<button type="button" class="btn btn-default" onclick="sendContent()">发送</button>
					</div>

				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
    function sendContent() {
        if(!$("#smsContentForm").valid()){
            return;
		}

        $.post('messageRecord/smsContent',
            $('#smsContentForm').serialize()
        ,function (rs) {
            layer.msg("发送成功");
            window.location.href='/messageRecord/index';
        })
    }


    function selectPartner(){

        TR.select('partner',{
            type:0//固定参数
        },function(data){
            $("#queryBpId").val(data.id);
            $("#queryBpName").val(data.name);
        });
    }


</script>