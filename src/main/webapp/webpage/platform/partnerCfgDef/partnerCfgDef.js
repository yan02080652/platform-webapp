var curPartnerCfgTree;//定义当前左边配置树当前节点
var canAdd = true;
var curMoveTreeNode;//要移动的树当前节点

var allTreePartnerCfgData;//树的所有数据


require(['treeview'],function(){//加载bootstrap treeview的资源 以及初始化树的数据
	initPartnerCfg();
});

function initPartnerCfg(){
	loadPartnerCfgTree();
}

function changeSysId(){
	reloadPage();
}

function loadPartnerCfgTree(id){
	var sysId = $("#sysId").val();
	$.getJSON('platform/partnerCfgDef/getCfgTree/'+sysId,function(data){
		var treePartnerCfgData = TR.turnToTreeViewData(data,{namePro:"nodeName"});
		allTreePartnerCfgData = treePartnerCfgData;
		$('#treePartnerCfgDef').treeview({
			data: treePartnerCfgData,
			showBorder:false,
			highlightSelected:true,
			//selectedBackColor:'#808080',
			onNodeSelected: function(event, data) {
				if(!canAdd){
					$('#treePartnerCfgDef').treeview('unselectNode', [ data.nodeId, { silent: true } ]);//取消当前选择
					if(curPartnerCfgTree != null){
						$('#treePartnerCfgDef').treeview('selectNode', [ curPartnerCfgTree.nodeId, { silent: true } ]);//不能编辑时还是选择上一个节点
					}
					$.alert("请编辑完当前节点!","提示");
					return;
				}else{
					//刷新右侧配置页面
					curPartnerCfgTree = data;
					reloadRightReadOnly(data.id,data.pid);
				}
			   
			},
			onNodeUnselected: function(event, data) {
				//刷新右侧配置页面
				if(!canAdd){
					
				}else{
					curPartnerCfgTree = null;
					$("#rightPartnerCfgDefUpper").hide();
				}
			}
		});
		if(typeof(id) != 'undefined' && id!=null){
			var rsData = TR.getCurNodeId(treePartnerCfgData,id);
			curPartnerCfgTree = rsData;
			$('#treePartnerCfgDef').treeview('selectNode', [rsData.nodeId, { silent: true } ]);
			reloadRightReadOnly(rsData.id,rsData.pid);
		}
		
	});
}

function reloadPage(){
	loadPartnerCfgTree();
	curPartnerCfgTree = null;
	canAdd = true;
	$("#rightPartnerCfgDefUpper").hide();
}

function reloadRight(id, pid) {
	if (!id) {
		id = 0;
	}
	if (!pid) {
		pid = 0;
	}
	$("#rightPartnerCfgDefUpper").show();
	var path = 'platform/partnerCfgDef/getCfgDetail/' + id + "/" + pid;
	$("#rightPartnerCfgDefUpper").load(path, function(data) {

		$('#formCfgDetail select,button,textarea').attr('disabled', false);
		$('#formCfgDetail input,textarea').attr('readonly', false);

		initForm();
	});
}

function reloadRightReadOnly(id, pid) {
	if (!id) {
		id = 0;
	}
	if (!pid) {
		pid = 0;
	}
	$("#rightPartnerCfgDefUpper").show();
	var path = 'platform/partnerCfgDef/getCfgDetail/' + id + "/" + pid;
	$("#rightPartnerCfgDefUpper").load(path, function(data) {
		initForm();
		if(!id){
			$("#rightPartnerCfgDefUpper").hide();
		}
		
		var nodeType = curPartnerCfgTree.data.nodeType;
		showViewAndAddRules(nodeType);
		if(nodeType==2 || nodeType == 3){
			getCfgItemTreeTable($("#sysId").val(),curPartnerCfgTree.data.nodeCode);
		}
	});
}

function reloadRightByCode(sysId, nodeCode,nodeType) {
	$("#rightPartnerCfgDefUpper").show();
	var path = 'platform/partnerCfgDef/getCfgDetailByCode/' + sysId + "/" + nodeCode;
	$("#rightPartnerCfgDefUpper").load(path, function() {
		initForm();
		showViewAndAddRules(nodeType);
	});
}


// 添加根节点
function addRoot() {
	if (!canAdd) {
		$.alert("请先编辑完当前节点!", "提示");
		return;
	}
	canAdd = false;
	//添加顶级节点时 取消当前选中
	if(curPartnerCfgTree!=null){
		$('#treePartnerCfgDef').treeview('unselectNode', [curPartnerCfgTree.nodeId, { silent: true }]);
		curPartnerCfgTree = null;
	}
	
	reloadRight(0, 0);
};

//添加子节点
function addNode(){
	if(!canAdd){
		$.alert("请先编辑完当前节点!","提示");
		return;
	}
	if(curPartnerCfgTree == null || curPartnerCfgTree.data.nodeType != 0){
		$.alert("请先选择一个目录节点!","提示");
		return;
	}
	var pid = curPartnerCfgTree.id;
	canAdd = false;
	
	reloadRight(0,pid);
}

function submitCfg() {
	//提交之前，判断字典编码是否改变，如果改变，判断原来字典下是否有值，有值就不能修改。
	var type = $("#nodeType1").val();
	if(type==1||type==3){//字典和组合配置组
		var oldDictCode = $("#oldDictCode").text();
		var newDictCode = $("#refDict").val();
		
		if(oldDictCode!=newDictCode && oldDictCode!=''){//修改了字典编码
			   $.ajax({
					url:"platform/partnerCfgDef/checkDictData/"+$("#sysId").val()+"/"+oldDictCode,
					type:"post",
					success:function(data){
						if(data){
							 $.alert("原字典下有数据，请先删除该字典下的所有数据后再进行修改","提示");
							 //$("#refDict").val(oldDictCode);
							return;
						}else{
							 $("#formCfgDetail").submit();
						}
					}
				});
		}else{
			  $("#formCfgDetail").submit();
		}
	}else{
		$("#formCfgDetail").submit();
	}
}

function initForm() {
	$("#formCfgDetail").validate(
			{
				rules : {
					nodeName1 : {
						required : true,
						minlength : 1,
						maxlength : 100
					},
					nodeType1 : {
						type : true,
					},
					nodeCode : {
						required : true,
						minlength : 1,
						maxlength : 30,
						remote : {
							url : "platform/partnerCfgDef/checkCode/"
									+ $("#sysId").val(),
							type : "post",
							data : {
								nodeCode : function() {
									return $("#nodeCode").val();
								},
								id : function() {
									return $("#id").val();
								}
							},
							dataFilter : function(data) {
								if (data == 'ok') {
									return true;
								} else {
									return false;
								}
							}
						}
					},
				},
				messages : {
					nodeCode : {
						remote : "编码已存在",
					},
					refDict:{
						remote:"编码已存在",
					}
				},
				submitHandler : function(form) {
					$.ajax({
						cache : true,
						type : "POST",
						url : "platform/partnerCfgDef/saveOrupdate/"
								+ $("#sysId").val() + "/"
								+ $("#nodeType1").val(),
						data : $('#formCfgDetail').serialize()+"&nodeName="+ $("#nodeName1").val(),
						async : false,
						error : function(request) {
							showErrorMsg("请求失败，请刷新重试");

						},
						success : function(data) {
							if (data.type == 'success') {
								showSuccessMsg("新增成功！");
								canAdd = true;
								var id=$("#id").val();
								if(!id){//新增后
									loadPartnerCfgTree(data.txt);
								}else{//编辑后
									reloadRightReadOnly($("#id").val(),$("#pid").val());
								}
							
							} else {
								showErrorMsg(data.txt);
							}
						}
					});

				},
				errorPlacement : setErrorPlacement,
				success : validateSuccess,
				highlight : setHighlight
			});
}

//取消
function cancel(){
	if(curPartnerCfgTree != null){//说明是编辑的 重新点击该节点就可以了
		reloadRightReadOnly(curPartnerCfgTree.id,curPartnerCfgTree.pid);
	}else{
		//将右侧隐藏
		$("#rightPartnerCfgDefUpper").hide();
	}
	canAdd = true;
}

//修改节点信息
function editNode() {
	if(!canAdd){
		$.alert("请先编辑完当前节点!","提示");
		return;
	}
	if(curPartnerCfgTree == null){
		$.alert("请先选择一个节点!","提示");
		return;
	}
	$('#formCfgDetail').find('input,textarea,button').attr('disabled',false).attr('readonly',false);
	$("#nodeCode").attr("readonly",true);
	canAdd = false;

};

function selectPartnerCfgTree() {
	showModal('movePartnerCfgDiv',"platform/partnerCfgDef/selectPartnerCfgTree", function() {
		$('#modelPartnerCfgNode').modal();
		var sysId = $("#sysId").val();
		$.getJSON('platform/partnerCfgDef/getCfgTree/'+sysId,function(data){
			var treePartnerCfgData = TR.turnToTreeViewData(data,{namePro:"nodeName"});
			$('#selectPartnerCfgTree').treeview({
				data: treePartnerCfgData,
				showBorder:false,
				highlightSelected:true,
				//selectedBackColor:'#808080',
				onNodeSelected: function(event, data) {
					curMoveTreeNode = data;
				},
				onNodeUnselected: function(event, data) {
					curMoveTreeNode = null;
				}
			});
		});
	});
}

function moveNodePartnerCfg(){
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	if(curPartnerCfgTree==null){
		$.alert("请选择一个节点!","提示");
		return;
	}
	selectPartnerCfgTree();
	
}

function selectPartnerCfgNode(){
	if(curMoveTreeNode == null ||curMoveTreeNode.data.nodeType!=0){
		$.alert("请选择一个目录节点!","提示");
		return;
	}
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认移动当前节点?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/partnerCfgDef/movePartnerCfg",
				data : {
					fromId : curPartnerCfgTree.id,
					toId : curMoveTreeNode.id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						$('#modelPartnerCfgNode').modal('hide');
						reloadPage();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
	
}


//删除节点
function deleteNode() {
	if(!canAdd){
		$.alert("请先请先编辑完当前节点!","提示");
		return;
	}
	if (curPartnerCfgTree == null) {
		$.alert("请先选择一个节点!","提示");
		return;
	}
	if (curPartnerCfgTree.nodes) {
		$.alert("父节点不允许直接删除,请先删除该节点下的所有子节点后重新操作");
		return;
	}else{
		if (curPartnerCfgTree.data.nodeType == 2 || curPartnerCfgTree.data.nodeType == 3) {
			$.ajax({
				type : 'post',
				url : "platform/partnerCfgDef/getCfgItemTreeTable/" + $("#sysId").val() + "/"+ curPartnerCfgTree.data.nodeCode,
				success:function(data){
					if(data.length>0){
						$.alert("该节点下存在配置项，请先删除配置项后再删除节点","提示");
						return;
					}else{
						if(curPartnerCfgTree.data.nodeType == 3){
							   $.ajax({
									url:"platform/partnerCfgDef/checkDictData/"+$("#sysId").val()+"/"+$("#refDict").val(),
									type:"post",
									success:function(data){
										if(data){
											  $.alert("该节点下有数据字典，请删除字典后重新删除节点","提示");
												return;
										}else{
											deletePartnerCfg(curPartnerCfgTree);
										}
									}
								});
						}else{
							deletePartnerCfg(curPartnerCfgTree);
						}
					}
				}
			});
		}else if(curPartnerCfgTree.data.nodeType == 1){
		   $.ajax({
				url:"platform/partnerCfgDef/checkDictData/"+$("#sysId").val()+"/"+$("#refDict").val(),
				type:"post",
				success:function(data){
					if(data){
						  $.alert("该节点下有数据字典，请删除字典后重新删除节点","提示");
							return;
					}else{
						deletePartnerCfg(curPartnerCfgTree);
					}
				}
			});
		   
		}else{
			deletePartnerCfg(curPartnerCfgTree);
		}
	}
};

function deletePartnerCfg(curPartnerCfgTree){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除当前节点?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/partnerCfgDef/deleteNode",
				data : {
					id : curPartnerCfgTree.id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadPage();
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
}

//显示不同div并为相应字段添加验证规则
function showViewAndAddRules(typeValue){
	if(typeValue == 0){//目录
		$('#formCfgDetail #refDictDiv,#refTransDiv,#ItemTreetable').attr('disabled',true).attr("hidden",true);
	}else if(typeValue == 1){//数据字典
		$('#formCfgDetail #refDictDiv').attr('disabled',false).attr("hidden",false);
		$('#formCfgDetail #refTransDiv,#ItemTreetable').attr('disabled',true).attr("hidden",true);
		$("#refDict").rules("add",{required:true});
		$("#refDict").rules("add",{
			remote : {//验证数据字典编码唯一
				url : "platform/partnerCfgDef/checkDictCode/"
						+ $("#sysId").val(),
				type : "post",
				data : {
					refDict : function() {
						return $("#refDict").val();
					},
					id:function(){
						return $("#id").val();
					}
				},
				dataFilter : function(data) {
					if (data == 'ok') {
						return true;
					} else {
						return false;
					}
				}
			}
		});
	}else if(typeValue == 2){//配置组
		$('#formCfgDetail #refDictDiv').attr('disabled',true).attr("hidden",true);
		$('#formCfgDetail #refTransDiv').attr('disabled',true).attr("hidden",true);
		$('#ItemTreetable').attr('disabled',false).attr("hidden",false);
	}else if(typeValue == 3){//组合配置组
		$('#formCfgDetail #refDictDiv').attr('disabled',false).attr("hidden",false);
		$('#formCfgDetail #refTransDiv').attr('disabled',true).attr("hidden",true);
		$('#ItemTreetable').attr('disabled',false).attr("hidden",false);
		$("#refDict").rules("add",{required:true});
		$("#refDict").rules("add",{
			remote : {//验证数据字典编码唯一
				url : "platform/partnerCfgDef/checkDictCode/"
						+ $("#sysId").val(),
				type : "post",
				data : {
					refDict : function() {
						return $("#refDict").val();
					},
					id:function(){
						return $("#id").val();
					}
				},
				dataFilter : function(data) {
					if (data == 'ok') {
						return true;
					} else {
						return false;
					}
				}
			}
		});
	}else if(typeValue == 4){//其他作业
		$('#formCfgDetail #refDictDiv,#ItemTreetable').attr('disabled',true).attr("hidden",true);
		$('#formCfgDetail #refTransDiv').attr('disabled',false).attr("hidden",false);
		$("#refTrans").rules("add",{required:true});
	}
}

//节点类型改变事件
function onchangeType(){
	var typeValue = $("#nodeType1").val();
	showViewAndAddRules(typeValue);
}


/**********************************树表************************************************/

//标识当前行
var curItemTreeId = -1;

//加载配置树表
function getCfgItemTreeTable(sysId,cfgnodeCode){
	$.ajax({
		type : 'post',
		url : "platform/partnerCfgDef/getCfgItemTreeTable/"+sysId+"/"+cfgnodeCode,
		success : function(data) {
			var treeData = TR.turnToZtreeData(data);
			var html = "";
			html = addChild(treeData,html);
			$("#treeTableItemDetail").html(html);
			
			function addChild(data) {
				$.each(data, function(i, d) {
					if(d.pid){
						html += "<tr onclick='changeCurTreeId("+d.id+")' data-tt-id='"+d.id+"' data-tt-parent-id='"+d.pid+"'>";
					}else{
						html += "<tr onclick='changeCurTreeId("+d.id+")' data-tt-id='"+d.id+"'>";
					}
					
					if(d.children){//存在子节点
						html +="<td><span class='folder'>"+d.data.itemName+"</span></td><td>"+d.data.itemCode+"</td>";
						if(d.data.itemType==0){//目录
							html += "<td>目录</td>";
						}else if(d.data.itemType==1){//checkbox
							html += "<td>多选框</td>";
						}else if(d.data.itemType==2){//下拉列表
							html += "<td>下拉列表(单选)</td>";
						}else {//填空
							html += "<td>填空</td>";
						}
						
						html +="<td>"+d.data.optionalValues+"</td><td>"+d.data.itemNote+"</td>";
						
						html += "</tr>";
						addChild(d.children,html);
						
					}else{
						
						html +="<td><span class='file'>"+d.data.itemName+"</span></td><td>"+d.data.itemCode+"</td>";
						if(d.data.itemType==0){//目录
							html += "<td>目录</td>";
						}else if(d.data.itemType==1){//checkbox
							html += "<td>多选框</td>";
						}else if(d.data.itemType==2){//下拉列表
							html += "<td>下拉列表(单选)</td>";
						}else {//填空
							html += "<td>填空</td>";
						}
						
						html +="<td>"+d.data.optionalValues+"</td><td>"+d.data.itemNote+"</td>";
						
						html += "</tr>";
					}
				});
				return html;
			}

			var option = {
				expandable: true,
			    expandLevel : 4
			};

			$("#treeTablePartnercfgItem").treetable(option);
			jQuery('#treeTablePartnercfgItem').treetable('expandAll');

			$("#treeTablePartnercfgItem tbody tr").mousedown(function() {
				$("tr.selected").removeClass("selected");
				$(this).addClass("selected");
			});


			$("#treeTablePartnercfgItem.folder").each(function() {
				$(this).parents("tr").droppable({
					 accept: ".file, .folder",
					 drop: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   $("#treeTablePartnercfgItem").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
					 },
					 hoverClass: "accept",
					 over: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   if(this != droppedEl[0] && !$(this).is(".expanded")) {
					     $("#treeTablePartnercfgItem").treetable("expandNode", $(this).data("ttId"));
					   }
					 }
				});
			});
		}
	});
}

function changeCurTreeId(id){
	curItemTreeId = id;
}

function showModal(divId,url, callback) {
	$('#'+divId).load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

//添加配置项根节点
function addItemRoot() {
	if (!canAdd) {
		$.alert("请先编辑完当前节点基本信息后再添加配置项!", "提示");
		return;
	}
	showModal("partnerCfgItmeModelDiv","platform/partnerCfgDef/addItemNode/-1", function() {
		$('#partnerCfgItemDetailModel').modal();
		initItemForm();
	});
}

//添加配置项子节点
function addItemNode() {
	if (!canAdd) {
		$.alert("请先编辑完当前节点基本信息后再添加配置项!", "提示");
		return;
	}
	if (curItemTreeId == -1) {
		$.alert("请先选择一个配置行!","提示");
		return;
	}
	
	var type = $("tr[data-tt-id="+curItemTreeId+"] td:eq(2)").text();
	if(type!='目录'){
		$.alert("非目录节点无法添加子配置项!","提示");
		return;
	}
	showModal("partnerCfgItmeModelDiv","platform/partnerCfgDef/addItemNode/" + curItemTreeId + "", function() {
		$('#partnerCfgItemDetailModel').modal();
		initItemForm();
	});
}

//修改配置项节点
function editItemNode(id) {
	if (!canAdd) {
		$.alert("请先编辑完当前节点基本信息后再添加配置项!", "提示");
		return;
	}
	if (curItemTreeId == -1) {
		$.alert("请先选择一个配置行!","提示");
		return;
	}
	showModal("partnerCfgItmeModelDiv","platform/partnerCfgDef/getItemNode/" + curItemTreeId + "", function(a) {
		$('#partnerCfgItemDetailModel').modal();
		initItemForm();
	});
}

//删除配置项
function deleteItemNode() {
	if (!canAdd) {
		$.alert("请先编辑完当前节点基本信息后再添加配置项!", "提示");
		return;
	}
	if (curItemTreeId == -1) {
		$.alert("请先选择一个配置行!","提示");
		return;
	}
	
	$.ajax({
		url:"platform/partnerCfgDef/hasChild",
		type:"post",
		data:{id : curItemTreeId},
		success:function(data){
			if(data=="ok"){//无子节点
				$.confirm({
					title : '提示',
					confirmButton:'确认',
					cancelButton:'取消',
					content : '确认删除当前配置行?',
					confirm : function() {
						$.ajax({
							cache : true,
							type : "POST",
							url : "platform/partnerCfgDef/deletePartnerItem",
							data : {
								id : curItemTreeId
							},
							async : false,
							error : function(request) {
								showErrorMsg("请求失败，请刷新重试");
							},
							success : function(data) {
								if (data.type == 'success') {
									showSuccessMsg(data.txt);
									getCfgItemTreeTable($("#sysId").val(),$("#nodeCode").val());
									curItemTreeId = -1;
								} else {
									showErrorMsg(data.txt);
								}
							}
						});
					},
					cancel : function() {
						
					}
				});
			}else{//有子节点
				$.alert("请先删除本配置项下的子配置项，再删除本配置","提示");
			}
		}
	});
	
}

//配置项提交
function submitPartnerItem(){
	$("#partnerCfgItemForm").submit();
}

function initItemForm() {
	$("#partnerCfgItemForm").validate({
		rules : {
			itemCode : {
				required : true,
				minlength : 1,
				maxlength : 30,
				remote : {
					url : "platform/partnerCfgDef/checkPartnerItemCode",
					type : "post",
					data : {
						itemCode : function() {
							return $("#itemCode").val();
						},
						sysId: function() {
							return $("#sysId").val();
						},
						id:function(){
							return $("#itemId").val();
						}
					},
					dataFilter : function(data) {
						if (data == 'ok') {
							return true;
						} else {
							return false;
						}
					}
				}
			},
			itemName : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			itemType : {
				type:true
			}
		},
		messages : {  
			itemCode : {  
	            remote: "编码已存在"
	          }
	    },
		submitHandler : function(form) {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/partnerCfgDef/saveOrupdatePartnerItem/"+$("#sysId").val()+"/"+$("#nodeCode").val(),
				data : $('#partnerCfgItemForm').serialize(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
					$('#partnerCfgItemDetailModel').modal('hide');
				},
				success : function(data) {
					$('#partnerCfgItemDetailModel').modal('hide');
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						getCfgItemTreeTable($("#sysId").val(),$("#nodeCode").val());
						curItemTreeId = -1;
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}