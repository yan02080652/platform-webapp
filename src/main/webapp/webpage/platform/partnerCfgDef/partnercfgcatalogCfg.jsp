<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<script>
    var tmc_Type = '${type}';
</script>

<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.theme.default.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<script src="resource/plugins/jqueryTreeTable/javascripts/src/jquery.treetable.js?version=${globalVersion}" type="text/javascript"></script>

<div class="container-fluid">

	<div class="col-md-12">
        <div class="form-inline">
          		<div class="glyphicon glyphicon-home" aria-hidden="true"></div>企业配置   &nbsp;&nbsp;
					<c:if test="${type ne 'me'}">
						<div class="form-group" style="margin-left: 30px;">
							<%@ include file="/webpage/common/partnerChange.jsp"%>
						</div>
					</c:if>
          		<div class="form-group" style="margin-left: 30px;">
            		系统
	        	<select class="form-control"  id="partnerCfgSysId" name="sysId" style="width: 150px" onchange="changeSysId()">
	        	 	<c:forEach items="${dictCodeDtoList}" var="dictCodeDto">
						<option value="${dictCodeDto.itemCode}">${dictCodeDto.itemTxt}</option>
					</c:forEach>
				 </select>
		    </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4" style="border-top-width: 50px;padding-top: 50px;">
			<div class="panel panel-default">
			    <div id="treePartnercfgCatalog" style="margin-left: 10px;"></div>
        	</div>
        	
		</div>

		<div class="col-md-8 animated fadeInRight">
			  <div class="row">
				<div class="col-md-12">
					<div id="partnercfgDicMainDiv"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="partnercfgDicItemDataMainDiv"></div>
				</div>
			</div>
		</div>
	</div>
	<div id="partnercfgCatalogDicDiv"></div>
</div>
<script type="text/javascript" src="webpage/platform/partnerCfgDef/partnercfgcatalogCfg.js?version=${globalVersion}"></script>
<script type="text/javascript">

$(document).ready(function(){
	$("#partnerCfgSysId").val("${sysId}");
});
</script>