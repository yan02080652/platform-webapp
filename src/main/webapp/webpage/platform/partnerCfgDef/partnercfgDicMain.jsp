<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel-body" style="padding-bottom: 5px;padding-left: 0px;">
	<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addDicRoot();">添加顶级</a>
	<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addDicNode();">添加下级</a>
	<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="editDicNode();">编辑</a>
	<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="deleteDicNode();">删除</a>
</div>
<table id="treeTablePartnercfgDic" class="table table-bordered" style="width:100%">
	<thead>
         <tr>
           <th style="text-align:center;">字典项编码</th>
           <th>字典名称</th>
           <th>是否可选</th>
           <th>是否可见</th>
           
         </tr>
       </thead>
       <tbody id="treeTablePartnerDicDetail">
       
       </tbody>
</table>