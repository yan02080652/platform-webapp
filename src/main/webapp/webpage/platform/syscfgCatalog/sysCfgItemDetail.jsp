<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="sysCfgItemDetailModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">${typeTitle}</h4>
         </div>
         <div class="modal-body">
         <form modelAttribute="syscfgCfgitemDto" class="form-horizontal" id="sysCfgItemForm" role="form">
            	<input type="hidden" name="id" id="itemId" value="${syscfgCfgitemDto.id}"/>
            	<input type="hidden" name="pid" value="${syscfgCfgitemDto.pid}"/>
            	<input type="hidden" name="cfgnodeCode" value="${syscfgCfgitemDto.cfgnodeCode}"/>
			   <div class="form-group">
			      <label for="itemCode" class="col-sm-3 control-label">配置项编码</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" id="itemCode" name="itemCode" value="${syscfgCfgitemDto.itemCode}" placeholder="配置项编码"/>
			      </div>
			   </div>
			   <div class="form-group">
				    <label for="itemName" class="col-sm-3 control-label">配置项名称</label>
				    <div class="col-sm-9">
				   	  <input type="text" class="form-control" name="itemName"  id="itemName" value="${syscfgCfgitemDto.itemName}" placeholder="配置项名称"/>
				    </div>
				</div>
				  <div class="form-group">
			      <label for="itemType" class="col-sm-3 control-label">配置项类型</label>
			      <div class="col-sm-9">
			         <select class="form-control" id="itemType" name="itemType">
				          <option value="-1">请选择类型</opton>
				          <option value="0">目录</option>
				          <option value="1">多选框</option>
				          <option value="2">下拉列表(单选)</option>
				          <option value="3">填空</option>
					  </select>
			      </div>
			   </div>
			   <div class="form-group">
				    <label for="optionalValues" class="col-sm-3 control-label">可选值</label>
				    <div class="col-sm-9">
 						<textarea class="form-control" name="optionalValues" rows="3" placeholder="可选值（key:value;key2:value2）">${syscfgCfgitemDto.optionalValues}</textarea>
				    </div>
				</div>
				<div class="form-group">
			      <label for="itemNote" class="col-sm-3 control-label">配置说明</label>
			      <div class="col-sm-9">
			         <textarea class="form-control" name="itemNote" rows="3" placeholder="配置说明">${syscfgCfgitemDto.itemNote}</textarea>
			      </div>
			   </div>
			   
			   <div class="form-group">
			      <label for="seq" class="col-sm-3 control-label">序号</label>
			      <div class="col-sm-9">
			         <input type="number" class="form-control" name="seq" value="${syscfgCfgitemDto.seq}" placeholder="序号"/>
			      </div>
			  </div>
			  <div class="modal-footer">
			   <button type="button" class="btn btn-default" onclick="submitSysItem()">保存</button>
         	   <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
       	     </div>
			</form>
         </div>
      </div>
</div>
 <script>
 $(document).ready(function() {
	 var itemId = "${syscfgCfgitemDto.id}";
		if(itemId!=null && itemId!=''){//编辑
			$("#itemType").val("${syscfgCfgitemDto.itemType}");
		}
 });
 </script>