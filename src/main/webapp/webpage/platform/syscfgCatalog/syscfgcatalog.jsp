<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.theme.default.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<script src="resource/plugins/jqueryTreeTable/javascripts/src/jquery.treetable.js?version=${globalVersion}" type="text/javascript"></script>

<div class="container-fluid">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
		        <div class="form-inline">
	           		<div class="glyphicon glyphicon-home" aria-hidden="true"></div>系统配置   &nbsp;&nbsp;
	           		<div class="form-group" style="margin-left: 30px;">
	             		<label for="sysId" class="control-label" >系统</label>
			        	<select class="form-control"  id="sysId" name="sysId" style="width: 150px" onchange="changeSysId()">
			        	 	<c:forEach items="${dictCodeDtoList}" var="dictCodeDto">
								<option value="${dictCodeDto.itemCode}">${dictCodeDto.itemTxt}</opton>
							</c:forEach>
						</select>
				    </div>
				</div>
			</div>
			<hr/>
			<div>
				<div class="panel-body" style="padding-top: 0px;padding-bottom: 5px">
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addRoot();">添加顶级</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addNode();">添加下级</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="editNode();">编辑</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="deleteNode();">删除</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="moveNodeSysCfg();">移动</a>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
		             	<div class="panel-body">
							<div id="treeSyscfgCatalog" class=""></div>
		            	</div>
		        	</div>
				</div>
				<div id="rightSyscfgCatalogUpper" class="col-md-8 animated fadeInRight">
				</div>
			</div>
		</div>
	</div>
</div>
<div id="moveSysCfgDiv"></div>
<div id="syscfgCatalogDiv"></div>
<script type="text/javascript" src="webpage/platform/syscfgCatalog/syscfgcatalog.js?version=${globalVersion}"></script>
<script src="resource/plugins/jqueryTreeTable/javascripts/src/jquery.treetable.js?version=${globalVersion}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#sysId").val("${sysId}");
});
</script>