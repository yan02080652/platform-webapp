var curCfgnodeTree;//定义当前左边配置树当前节点
var curDicTreeId = -1;//当前数据字典列表当前行
var curItemTreeId = -1;//当前配置组列表当前行
var curDictCode="";//定义当前左边字典编码


require(['treeview'],function(){//加载bootstrap treeview的资源 以及初始化树的数据
	initMenu();
});

function initMenu(){
	reloadPage();
}

function changeSysId(){
	reloadPage();
}

function reloadPage(){
	var sysId = $("#sysCfgSysId").val();
	$.ajax({
		type : 'GET',
		url : "platform/syscfgCatalogCfg/getCfgTree/" + sysId,
		success : function(data) {
			$('#treeSyscfgCatalog').treeview({
				data: TR.turnToTreeViewData(data,{namePro:"nodeName"}),
				showBorder:false,
				highlightSelected:true,
				//selectedBackColor:'#808080',
				onNodeSelected: function(event, data) {
					curCfgnodeTree = data;
					
					curDictCode = data.data.refDict;
					var sysId = $("#sysCfgSysId").val();
					var url = "platform/syscfgCatalogCfg/getSyscfgRight?nodeType="+data.data.nodeType;
					
					$('#syscfgDicMainDiv,#syscfgDicItemDataMainDiv').empty();
					if(data.data.nodeType == 0){//目录
						$('#syscfgDicMainDiv').load(url);
					}else if(data.data.nodeType == 1){//数据字典
						$('#syscfgDicMainDiv').load(url, function(context, state) {
							if ('success' == state) {
								getCfgDicTreeTable(sysId,data.data.refDict,data.data.nodeType);
							}
						});
						
					}else if(data.data.nodeType == 2){//配置组
						$('#syscfgDicItemDataMainDiv').load(url, function(context, state) {
							if ('success' == state) {
								getCfgItemTreeTable(sysId,data.data.nodeCode,"");
							}
						});
						
					}else if(data.data.nodeType == 3){//组合配置组
						$('#syscfgDicMainDiv').load(url, function(context, state) {
							if ('success' == state) {
								getCfgDicTreeTable(sysId,data.data.refDict,data.data.nodeType);
							}
						});
						
					}else if(data.data.nodeType == 4){//其它作业
						
					}
				   
				},
				onNodeUnselected: function(event, data) {
					curCfgnodeTree = null;
					$('#syscfgDicMainDiv,#syscfgDicItemDataMainDiv').empty();
				}
			});
		}
	});
	$('#syscfgDicMainDiv,#syscfgDicItemDataMainDiv').empty();
}

/**
 * 加载字典页面
 * @param sysId
 * @param cfgnodeCode
 */
function getCfgDicTreeTable(sysId,dictCode,nodeType){
	$.ajax({
		type : 'GET',
		url : "platform/syscfgCatalogCfg/getCfgDicTreeTable?sysId="+sysId+"&dictCode="+dictCode,
		success : function(data) {
			var treeData = TR.turnToZtreeData(data);
			var html = "";
			html = addChild(treeData,html);
			$("#treeTableDetail").html(html);
			
			function addChild(data) {
				$.each(data, function(i, d) {
					if(d.pid){
						html += "<tr onclick='changeCurTreeId("+d.data.id+",\""+d.data.sysId+"\",\""+d.data.dictCode+"\",\""+d.data.itemCode+"\","+nodeType+")' data-tt-id='"+d.id+"' data-tt-parent-id='"+d.pid+"'>";
					}else{
						html += "<tr onclick='changeCurTreeId("+d.data.id+",\""+d.data.sysId+"\",\""+d.data.dictCode+"\",\""+d.data.itemCode+"\","+nodeType+")' data-tt-id='"+d.id+"'>";
					}
					var cust1 = d.data.cust1==null?"":d.data.cust1;
					var cust2 = d.data.cust2==null?"":d.data.cust2;
					if(d.children){//存在子节点
						html += "<td><span class='folder'>"+d.data.itemCode+"</span></td><td>"+d.data.itemTxt+"</td>";
						if(d.data.selectable && d.data.selectable==1){
							html += "<td><input  type='checkbox' disabled='disabled' value='1' checked='checked' /></td>";
						}else{
							html += "<td><input  type='checkbox' disabled='disabled' value='0' /></td>";
						}
						
						if(d.data.visible && d.data.visible==1){
							html += "<td><input  type='checkbox' disabled='disabled' value='1' checked='checked' /></td>";
						}else{
							html += "<td><input  type='checkbox' disabled='disabled' value='0' /></td>";
						}
						
						html += "<td>"+cust1+"</td><td>"+cust1+"</td>";
						html += "</tr>";
						addChild(d.children,html);
						
					}else{
						html += "<td><span class='file'>"+d.data.itemCode+"</span></td><td>"+d.data.itemTxt+"</td>";
						if(d.data.selectable && d.data.selectable==1){
							html += "<td><input  type='checkbox' disabled='disabled' value='1' checked='checked' /></td>";
						}else{
							html += "<td><input  type='checkbox' disabled='disabled' value='0' /></td>";
						}
						
						if(d.data.visible && d.data.visible==1){
							html += "<td><input  type='checkbox' disabled='disabled' value='1' checked='checked' /></td>";
						}else{
							html += "<td><input  type='checkbox' disabled='disabled' value='0' /></td>";
						}
						html += "<td>"+ cust1 +"</td><td>"+cust2+"</td>";
						html += "</tr>";
					}
				});
				return html;
			}

			var option = {
				expandable: true,
			    expandLevel : 4
			};

			$("#treeTableSyscfgDic").treetable(option);
			jQuery('#treeTableSyscfgDic').treetable('expandAll');

			//Highlight selected row
			$("#treeTableSyscfgDic tbody tr").mousedown(function() {
				$("#treeTableSyscfgDic tr.selected").removeClass("selected");
				$(this).addClass("selected");
			});


			$("#treeTableSyscfgDic.folder").each(function() {
				$(this).parents("tr").droppable({
					 accept: ".file, .folder",
					 drop: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   $("#treeTableSyscfgDic").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
					 },
					 hoverClass: "accept",
					 over: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   if(this != droppedEl[0] && !$(this).is(".expanded")) {
					     $("#treeTableSyscfgDic").treetable("expandNode", $(this).data("ttId"));
					   }
					 }
				});
			});
		}
	});
}


function changeCurTreeId(id,sysId,dictCode,itemCode,nodeType){
	curDicTreeId = id;
	if(nodeType == 3){//组合配置组加载组合配置组页面
		var url = "platform/syscfgCatalogCfg/getSyscfgRight?nodeType=2";
		$('#syscfgDicItemDataMainDiv').load(url, function(context, state) {
			if ('success' == state) {
				
				getCfgItemTreeTable(sysId,curCfgnodeTree.data.nodeCode,itemCode);
			}
		});
	}
}

function addDicRoot() {
	showModal("syscfgCatalogDicDiv","platform/syscfgCatalogCfg/addDicNode/" + -1 + "", function() {
		$('#modelSysDicNode').modal();
		initDicForm();
	});
}

function addDicNode() {
	if (curDicTreeId == -1) {
		$.alert("请先选择一个字典行!","提示");
		return;
	}
	showModal("syscfgCatalogDicDiv","platform/syscfgCatalogCfg/addDicNode/" + curDicTreeId + "", function() {
		$('#modelSysDicNode').modal();
		initDicForm();
	});
}

function editDicNode(id) {
	if (curDicTreeId == -1) {
		$.alert("请先选择一个字典行!","提示");
		return;
	}
	showModal("syscfgCatalogDicDiv","platform/syscfgCatalogCfg/updateDicNode/" + curDicTreeId + "", function(a) {
		$('#modelSysDicNode').modal();
		initDicForm();
	});
}

function deleteDicNode() {
	if (curDicTreeId == -1) {
		$.alert("请先选择一个字典行!","提示");
		return;
	}
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除当前字典行?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/syscfgCatalogCfg/deleteSysDic",
				data : {
					id : curDicTreeId
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						//刷新字典
						getCfgDicTreeTable($("#sysCfgSysId").val(),curDictCode,curCfgnodeTree.data.nodeType);
						$('#syscfgDicItemDataMainDiv').empty();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
}

function showModal(divId,url, callback) {
	$('#'+divId).load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function initDicForm() {
	$("#formSysDicNode").validate({
		rules : {
			itemCode : {
				required : true,
				minlength : 1,
				maxlength : 30,
				remote : {
					url : "platform/syscfgCatalogCfg/checkSysDicCode",
					type : "post",
					data : {
						itemCode : function() {
							return $("#sysDicItemCode").val();
						},
						id: function() {
							return $("#sysDicId").val();
						},
						sysId: function() {
							return $("#sysCfgSysId").val();
						},
						dictCode: function() {
							return curDictCode;
						}
					},
					dataFilter : function(data) {
						var resultMes = eval("("+data+")");
						if (resultMes.type == 'success') {
							return true;
						} else {
							return false;
						}
					}
				}
			},
			itemTxt : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			itemDesc : {
				maxlength : 150
			}
		},messages : {  
			itemCode : {  
	            remote: "编码已存在！"
	          }
	    },
		submitHandler : function(form) {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/syscfgCatalogCfg/saveOrupdateSysDic",
				data : $('#formSysDicNode').serialize()+"&sysId="+$("#sysCfgSysId").val()+"&dictCode="+curDictCode,
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
					$('#modelSysDicNode').modal('hide');
				},
				success : function(data) {
					$('#modelSysDicNode').modal('hide');
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						
						//刷新字典
						getCfgDicTreeTable($("#sysCfgSysId").val(),curDictCode,curCfgnodeTree.data.nodeType);
						$('#syscfgDicItemDataMainDiv').empty();
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

function submitSysDic(){
	$("#formSysDicNode").submit();
}

/**
 * 加载字典页面
 * @param sysId
 * @param cfgnodeCode
 */
function getCfgItemTreeTable(sysId,cfgnodeCode,dictitemCode){
	$.ajax({
		type : 'post',
		url : "platform/syscfgCatalogCfg/getCfgItemTreeTable",
		data : {
			sysId:sysId,
			cfgnodeCode:cfgnodeCode,
			dictitemCode : dictitemCode
		},
		success : function(data) {
			var treeData = TR.turnToZtreeData(data,{id:"ID",pid:"PID"});
			var html = "";
			html = addChild(treeData,html);
			
			$("#treeTableItemDetail").html(html);
			$("botton[id^=editCfgItemHref]").click();
			function addChild(data) {
				$.each(data, function(i, d) {
					var editUrl;
					if(d.data.ITEM_TYPE==0){//目录  //目录不可编辑
						editUrl = "";
					}else {
						editUrl = "<botton id='editCfgItemHref'"+d.id+" onclick='editCfgItem("+d.id+","+d.data.DETAILID+","+d.data.ITEM_TYPE+",\""+d.data.OPTIONAL_VALUES+"\",\""+dictitemCode+"\",\""+sysId+"\")'></botton>";
					}
					
					if(d.pid){
						html += "<tr onclick='changeCurItemTreeId("+d.id+")' data-tt-id='"+d.id+"' data-tt-parent-id='"+d.pid+"'>";
					}else{
						html += "<tr onclick='changeCurItemTreeId("+d.id+")' data-tt-id='"+d.id+"'>";
					}
					if(d.children){//存在子节点
						
						html +="<td> <span class='folder'>"+d.data.ITEM_NAME+"</span></td><td>"+d.data.ITEM_CODE+"</td>";
						html += "<td id='cfgItem"+d.id+"'>"+(typeof(d.data.ITEMVALUE)=='undefined'?"":d.data.ITEMVALUE)+editUrl+"</td></tr>";
						addChild(d.children,html);
						
					}else{
						html +="<td> <span class='file'>"+d.data.ITEM_NAME+"</span></td><td>"+d.data.ITEM_CODE+"</td>";
						html += "<td id='cfgItem"+d.id+"'>"+(typeof(d.data.ITEMVALUE)=='undefined'?"":d.data.ITEMVALUE)+editUrl+"</td></tr>";
					}
				});
				return html;
			}

			var option = {
				expandable: true,
			    expandLevel : 4
			};

			$("#treeTableSyscfgItem").treetable(option);
			jQuery('#treeTableSyscfgItem').treetable('expandAll');

			$("#treeTableSyscfgItem tbody tr").mousedown(function() {
				$("#treeTableSyscfgItem tr.selected").removeClass("selected");
				$(this).addClass("selected");
			});


			$("#treeTableSyscfgItem.folder").each(function() {
				$(this).parents("tr").droppable({
					 accept: ".file, .folder",
					 drop: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   $("#treeTableSyscfgItem").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
					 },
					 hoverClass: "accept",
					 over: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   if(this != droppedEl[0] && !$(this).is(".expanded")) {
					     $("#treeTableSyscfgItem").treetable("expandNode", $(this).data("ttId"));
					   }
					 }
				});
			});
		}
	});
}



function changeCurItemTreeId(id){
	curItemTreeId = id;
}

function editCfgItem(id,detailid,item_type,optional_values,dictitemCode,sysId){
	var html = "";
	if(item_type == 0){//目录
		$("#cfgItem"+id).html("");
	}else if(item_type == 1){//CHECKBOX
		var selectedValues = $("#cfgItem"+id).text();
		if(typeof(optional_values)!='undefined' && optional_values != null && optional_values!=''){
			var optionalValues = optional_values.split(';');
			for(var i=0 ; i<optionalValues.length ; i++){
				var value = optionalValues[i].split(':');
				if(typeof(selectedValues)!='undefined' && selectedValues!=''){
					var svs = selectedValues.split(';');
					var isChecked = false;
					for(var j=0 ; j<svs.length ; j++){
						if(value[1]==svs[j].trim()){
							isChecked = true;
						}
					}
					if(isChecked){
						html += "<input type='checkbox' onchange='sureCfgItem("+id+","+detailid+","+item_type+",\""+optional_values+"\",\""+dictitemCode+"\",\""+sysId+"\")' checked='checked' name='cfgItemValueCode"+ id +"' value='"+value[0]+"' />"+value[1]+"&nbsp;&nbsp;";
					}else{
						html += "<input type='checkbox' onchange='sureCfgItem("+id+","+detailid+","+item_type+",\""+optional_values+"\",\""+dictitemCode+"\",\""+sysId+"\")' name='cfgItemValueCode"+ id +"' value='"+value[0]+"' />"+value[1]+"&nbsp;&nbsp;";
					}
				}else{
					html += "<input type='checkbox' onchange='sureCfgItem("+id+","+detailid+","+item_type+",\""+optional_values+"\",\""+dictitemCode+"\",\""+sysId+"\")' name='cfgItemValueCode"+ id +"' value='"+value[0]+"' />"+value[1]+"&nbsp;&nbsp;";
				}
			}
			
		}else{
			html = "<input type='text' onchange='sureCfgItem("+id+","+detailid+","+item_type+",\""+optional_values+"\",\""+dictitemCode+"\",\""+sysId+"\")' style='width: 100%;' value='"+$("#cfgItem"+id).text()+"' id='cfgItemValueCode"+ id +"' name='cfgItemValueCode"+ id +"'/>";
		}
		$("#cfgItem"+id).css("padding", '5px');
		$("#cfgItem"+id).html(html);
	}else if(item_type == 2){//下拉列表
		if(typeof(optional_values)!='undefined' && optional_values != null && optional_values!=''){
			html = "<div class='input-group'>" +
					"<input type='text' value='"+$("#cfgItem"+id).text()+"' onchange='sureCfgItem("+id+","+detailid+","+item_type+",\""+optional_values+"\",\""+dictitemCode+"\",\""+sysId+"\")' id='cfgItemValueName"+ id +"' class='form-control dropdown-common myDropDownInput'> <span class='input-group-btn'>" +
					"<button class='btn btn-default' type='button'" +
					"onclick='selectMyDropDown(this,"+id+",\""+optional_values+"\",\""+$("#cfgItem"+id).text()+"\","+detailid+","+item_type+",\""+dictitemCode+"\",\""+sysId+"\")'>" +
					"<i class='fa fa-caret-down' aria-hidden='true'></i>" +
					"</button> </span> <input type='hidden' id='cfgItemValueCode"+ id +"' /></div>";
			
		}else{
			html = "<input type='text' onchange='sureCfgItem("+id+","+detailid+","+item_type+",\""+optional_values+"\",\""+dictitemCode+"\",\""+sysId+"\")' style='width: 100%;' value='"+$("#cfgItem"+id).text()+"' id='cfgItemValueCode"+ id +"' name='cfgItemValueCode"+ id +"'/>";
		}
		$("#cfgItem"+id).css("padding", '0px');
		$("#cfgItem"+id).html(html);
	}else if(item_type == 3){//填空
		html = "<input type='text' onchange='sureCfgItem("+id+","+detailid+","+item_type+",\""+optional_values+"\",\""+dictitemCode+"\",\""+sysId+"\")' style='width: 100%;padding:0px;border:1px;' value='"+$("#cfgItem"+id).text()+"' id='cfgItemValueCode"+ id +"' name='cfgItemValueCode"+ id +"'/>";
		$("#cfgItem"+id).html(html);
		
	}
	
}

/**
 * 自定义下拉列表选择
 * @param dom
 */
function selectMyDropDown(dom,id,optional_values,defaltVlaue,detailid,item_type,dictitemCode,sysId){
	var defaltCode = null;
	var dataString = "[";
	var optionalValues = optional_values.split(';');
	for(var i=0 ; i<optionalValues.length ; i++){
		var value = optionalValues[i].split(':');
		if($.trim(defaltVlaue)==value[1]){
			defaltCode = value[0];
		}
		if(i != optionalValues.length-1)
			dataString = dataString + "{code:'"+value[0]+"',name:'"+value[1]+"'},";
		else
			dataString = dataString + "{code:'"+value[0]+"',name:'"+value[1]+"'}";
	}
	dataString = dataString + "]";
	
	var data = eval('(' + dataString + ')');
	
	TR.select(data,{
		key:'code',//多个自定义下拉选择时key值不能重复
		dom:dom,
		name:'name',//default:name
		allowBlank:true//是否允许空白选项
	},function(result){
		$('#cfgItemValueName'+id).val(result?result.name:null);
		$('#cfgItemValueCode'+id).val(result?result.code:null);
		
		sureCfgItem(id,detailid,item_type,optional_values,dictitemCode,sysId);
	});
		
	//设置初始值
	$('#cfgItemValueName'+id).val(defaltVlaue?defaltVlaue:null);
	$('#cfgItemValueCode'+id).val(defaltCode?defaltCode:null);
}

function sureCfgItem(id,detailid,item_type,optional_values,dictitemCode,sysId){
	var cfgItemValueCode = $("#cfgItemValueCode"+id).val();
	var html = $("#cfgItemValueCode"+id).val();
	if(item_type == 0){//目录
		//$("#cfgItem"+id).html("");
	}else if(item_type == 1){//CHECKBOX
		cfgItemValueCode = "";
		html = "";
		var optionalValues = optional_values.split(';');
		$('input[name="cfgItemValueCode'+id+'"]:checked').each(function(){ 
			for(var i=0 ; i<optionalValues.length ; i++){
				var value = optionalValues[i].split(':');
				if(typeof($(this).val())!='undefined' && $(this).val() !=''){
					if($(this).val() == value[0]){
						cfgItemValueCode = cfgItemValueCode+value[0]+";";
						html = html+value[1]+" ; ";
					}
				}
			}
		});
		if(html != ''){
			html = html.slice(0, html.length-2);
			cfgItemValueCode = cfgItemValueCode.slice(0, cfgItemValueCode.length-1);
		}
		//$("#cfgItem"+id).css("padding", '8px');
		//$("#cfgItem"+id).html(html);
	}else if(item_type == 2){//下拉列表
		var cfgItemValueName = $('#cfgItemValueName'+id).val();
		if(cfgItemValueCode){
			var optionalValues = optional_values.split(';');
			for(var i=0 ; i<optionalValues.length ; i++){
				var value = optionalValues[i].split(':');
				if(typeof(cfgItemValueCode)!='undefined' && cfgItemValueCode !=''){
					if(cfgItemValueCode == value[0]){
						html = value[1];
					}
				}
			}
		}else{//说明时自己填的
			html = cfgItemValueName;
			cfgItemValueCode = cfgItemValueName;//给后面传到后台用
		}
		//$("#cfgItem"+id).css("padding", '8px');
		//$("#cfgItem"+id).html(html);
		
	}else if(item_type == 3){//填空
		//$("#cfgItem"+id).html(html);
	}
	
	//提交到后台更改，保存
	$.ajax({
		cache : true,
		type : "POST",
		url : "platform/syscfgCatalogCfg/saveorupdateItemData",
		data : {
			id : id,
			detailid:detailid,
			cfgItemValue:cfgItemValueCode,
			dictitemCode:dictitemCode
		},
		async : false,
		error : function(request) {
			showErrorMsg("请求失败，请刷新重试");
		},
		success : function(data) {
			if (data.type == 'success') {
				getCfgItemTreeTable(sysId,curCfgnodeTree.data.nodeCode,dictitemCode);
				
				//showSuccessMsg(data.txt);
			} else {
				$.alert(data.txt,"提示");
			}
		}
	});
	
}



 
        