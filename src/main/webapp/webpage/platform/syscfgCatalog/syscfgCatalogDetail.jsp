<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<div class="panel panel-default">
	<div class="panel-body ">
		<div class="modal-body">
			<form modelAttribute="catalogDto" class="form-horizontal" draggable="false" id="formCfgDetail" role="form">
            	<input type="hidden" id="id" name="id" value="${catalogDto.id}"/>
            	<input type="hidden" id="pid" name="pid"  value="${catalogDto.pid}"/>
            	<input type="hidden"  name="sysId" value="${catalogDto.sysId}"/>
			   <div class="form-group">
			      <label for="nodeType1" class="col-sm-2 control-label">节点类型</label>
			      <div class="col-sm-9">
			         <select class="form-control" id="nodeType1" name="nodeType1"   disabled="true" onchange="onchangeType()" >
				          <option value="-1">请选择菜单类型</opton>
				          <option value="0">目录</option>
				          <option value="1">数据字典</option>
				          <option value="2">配置组</option>
				          <option value="3">组合配置组</option>
				          <option value="4">其他作业</option>
					  </select>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="nodeCode" class="col-sm-2 control-label pull-left">节点编码</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" readonly="readonly" id="nodeCode" name="nodeCode" value="${catalogDto.nodeCode}" placeholder="节点编码"/>
			      </div>
			   </div>
				<div class="form-group">
			      <label for="nodeName1" class="col-sm-2 control-label">节点名称</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" readonly="readonly" id="nodeName1" name="nodeName1" value="${catalogDto.nodeName}" placeholder="节点名称"/>
			      </div>
			   </div>
			    <div class="form-group" id="refDictDiv" hidden="true">
			      <label for="refDict" class="col-sm-2 control-label">组合字典</label>
			      <div class="col-sm-9">
			      		<span hidden="hidden" id="oldDictCode">${catalogDto.refDict}</span>
			         <input type="text" class="form-control" readonly="readonly" id="refDict" name="refDict" value="${catalogDto.refDict}" placeholder="数据字典编码"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="nodeNote" class="col-sm-2 control-label">配置说明</label>
			      <div class="col-sm-9">
			         <textarea rows="3" class="form-control" readonly="readonly" id="nodeNote" name="nodeNote"  placeholder="配置说明">${catalogDto.nodeNote}</textarea>
			      </div>
			   </div>
			    <div class="form-group" id="refTransDiv" hidden="true">
			      <label for="refTrans" class="col-sm-2 control-label">作业编码</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" readonly="readonly" id="refTrans" name="refTrans" value="${catalogDto.refTrans}" placeholder="作业编码"/>
			      </div>
			   </div>
			    <div class="form-group" align="center">
			   		<button type="button" class="btn btn-default" disabled="disabled" onclick="cancel()">取消</button>&nbsp;&nbsp;
			      	<button type="button" class="btn btn-default" disabled="disabled" onclick="submitCfg()">保存</button>
			   </div>
			</form>
			
			<form modelAttribute="catalogDto" class="form-horizontal" draggable="false" id="formCfgItemTreeTable" role="form">
			  <!-- 配置树表 -->
			   <div class="form-group" id="ItemTreetable" hidden="true">
				   <div class="row">
					<div class="col-sm-11">
						<div class="panel-body" style="padding-bottom: 5px;padding-left: 0px;">
							<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addItemRoot();">添加顶级</a>
							<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addItemNode();">添加下级</a>
							<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="editItemNode();">编辑</a>
							<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="deleteItemNode();">删除</a>
						</div>
						<table id="treeTableSyscfgItem" class="table table-bordered" style="width:100%">
							<thead>
					          <tr>
					            <th>配置项名称</th>
					            <th>配置项编码</th>
					            <th>配置项类型</th>
					            <th>可选值</th>
					            <th>说明</th>
					          </tr>
					        </thead>
					        <tbody id="treeTableItemDetail">
					        
					        </tbody>
						</table>
					</div>
				</div>
			  </div>
			</form>
			 
		</div>
	</div>
</div>
<div id="sysCfgItmeModelDiv"></div>
<script type="text/javascript">
$(document).ready(function(){
	var id = "${catalogDto.id}";
	if(id!=null && id!=''){
		$("#nodeType1").val("${catalogDto.nodeType}");
	}
});

</script>
