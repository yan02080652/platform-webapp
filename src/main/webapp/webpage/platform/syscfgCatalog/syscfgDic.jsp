<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="modelSysDicNode" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">${typeTitle}</h4>
         </div>
         <div class="modal-body">
         <form modelAttribute="syscfgDictdataDto" class="form-horizontal" id="formSysDicNode" role="form">
            	<input type="hidden" name="pid" value="${pid}"/>
            	<input type="hidden" id="sysDicId" name="id" value="${syscfgDictdataDto.id}"/>
			   <div class="form-group">
			      <label for="itemCode" class="col-sm-2 control-label">编码</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" id="sysDicItemCode" name="itemCode" value="${syscfgDictdataDto.itemCode}" placeholder="字典项编码"/>
			      </div>
			   </div>
			   <div class="form-group">
				    <label for="itemTxt" class="col-sm-2 control-label">名称</label>
				    <div class="col-sm-10">
				   	  <input type="text" class="form-control" name="itemTxt" value="${syscfgDictdataDto.itemTxt}" placeholder="字典项名称"/>
				    </div>
				</div>
				<div class="form-group">
			      <label for="itemDesc" class="col-sm-2 control-label">详细描述</label>
			      <div class="col-sm-10">
			         <textarea class="form-control" name="itemDesc" rows="3">${syscfgDictdataDto.itemDesc} </textarea>
			      </div>
			   </div>
			   
			   <div class="form-group">
			      <label for="itemSeq" class="col-sm-2 control-label">序号</label>
			      <div class="col-sm-10">
			         <input type="number" class="form-control" name="itemSeq" value="${syscfgDictdataDto.itemSeq}" placeholder="序号"/>
			      </div>
			  </div>
			  <div class="form-group">
			      <label for="selectable" class="col-sm-2 control-label">是否可选</label>
				  <div class="col-sm-4">
				     <label class="radio-inline"><input  type="radio" value="1" checked="checked" name="selectable" />可选</label>
				     <lable class="radio-inline"><input  type="radio" value="0" name="selectable"/>不可选</lable>
				  </div>
				  <label for="visible" class="col-sm-2 control-label">是否可见</label>
				  <div class="col-sm-4">
				     <label class="radio-inline"><input  type="radio" value="1" checked="checked" name="visible" />可见</label>
				     <lable class="radio-inline"><input  type="radio" value="0" name="visible"/>不可见</lable>
				  </div>
			   </div>
			   <div class="form-group">
				    <label for="cust1" class="col-sm-2 control-label">cust1</label>
				    <div class="col-sm-10">
				   	  <input type="text" class="form-control" name="cust1" value="${syscfgDictdataDto.cust1}" />
				    </div>
				</div>
				<div class="form-group">
				    <label for="cust2" class="col-sm-2 control-label">cust2</label>
				    <div class="col-sm-10">
				   	  <input type="text" class="form-control" name="cust2" value="${syscfgDictdataDto.cust2}" />
				    </div>
				</div>
			  
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="submitSysDic()">保存
            </button>
         </div>
      </div>
</div>
 <script>
 $(document).ready(function() {
	var sysDicItemCode=$("#sysDicItemCode").val();
	if(sysDicItemCode!=null&&sysDicItemCode!=''){//说明是编辑
		
		$("input[type=radio][name=selectable]").each(function() { 
			if ($(this).val() == '${syscfgDictdataDto.selectable}') { 
				$(this).attr("checked", "checked"); 
			} 
		}); 
		$("input[type=radio][name=visible]").each(function() { 
			if ($(this).val() == '${syscfgDictdataDto.visible}') { 
				$(this).attr("checked", "checked"); 
			} 
		}); 
		$("#sysDicItemCode").attr("readonly","readonly");//编码只读
	}
 });
 </script>