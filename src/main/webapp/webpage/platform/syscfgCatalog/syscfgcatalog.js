var curSysCfgTree;//定义当前左边配置树当前节点
var canAdd = true;
var curMoveTreeNode;//要移动的树当前节点

var allTreeSysCfgData;//树的所有数据


require(['treeview'],function(){//加载bootstrap treeview的资源 以及初始化树的数据
	initSysCfg();
});

function initSysCfg(){
	loadSysCfgTree();
}

function changeSysId(){
	reloadPage();
}

function loadSysCfgTree(id){
	var sysId = $("#sysId").val();
	$.getJSON('platform/syscfgCatalog/getCfgTree?sysId='+sysId,function(data){
		var treeSysCfgData = TR.turnToTreeViewData(data,{namePro:"nodeName"});
		allTreeSysCfgData = treeSysCfgData;
		$('#treeSyscfgCatalog').treeview({
			data: treeSysCfgData,
			showBorder:false,
			highlightSelected:true,
			//selectedBackColor:'#808080',
			onNodeSelected: function(event, data) {
				if(!canAdd){
					$('#treeSyscfgCatalog').treeview('unselectNode', [ data.nodeId, { silent: true } ]);//取消当前选择
					if(curSysCfgTree != null){
						$('#treeSyscfgCatalog').treeview('selectNode', [ curSysCfgTree.nodeId, { silent: true } ]);//不能编辑时还是选择上一个节点
					}
					$.alert("请编辑完当前节点!","提示");
					return;
				}else{
					//刷新右侧配置页面
					curSysCfgTree = data;
					reloadRightReadOnly(data.id,data.pid);
				}
			   
			},
			onNodeUnselected: function(event, data) {
				//刷新右侧配置页面
				if(!canAdd){
					
				}else{
					curSysCfgTree = null;
					$("#rightSyscfgCatalogUpper").hide();
				}
			}
		});
		
		if(typeof(id) != 'undefined' && id!=null){
			var rsData = TR.getCurNodeId(treeSysCfgData,id);
			curSysCfgTree = rsData;
			$('#treeSyscfgCatalog').treeview('selectNode', [rsData.nodeId, { silent: true } ]);
			reloadRightReadOnly(rsData.id,rsData.pid);
		}
		
	});
}

function reloadPage(){
	loadSysCfgTree();
	curSysCfgTree = null;
	canAdd = true;
	$("#rightSyscfgCatalogUpper").hide();
}


function reloadRight(id, pid) {
	if (!id) {
		id = 0;
	}
	if (!pid) {
		pid = 0;
	}
	$("#rightSyscfgCatalogUpper").show();
	var path = 'platform/syscfgCatalog/getCfgDetail/' + id + "/" + pid;
	$("#rightSyscfgCatalogUpper").load(path, function(data) {

		$('#formCfgDetail select,button,textarea').attr('disabled', false);
		$('#formCfgDetail input,textarea').attr('readonly', false);

		initForm();
	});
}

function reloadRightReadOnly(id, pid) {
	if (!id) {
		id = 0;
	}
	if (!pid) {
		pid = 0;
	}
	$("#rightSyscfgCatalogUpper").show();
	var path = 'platform/syscfgCatalog/getCfgDetail/' + id + "/" + pid;
	$("#rightSyscfgCatalogUpper").load(path, function(data) {
		initForm();
		if(!id){
			$("#rightSyscfgCatalogUpper").hide();
		}
		
		var nodeType = curSysCfgTree.data.nodeType;
		showViewAndAddRules(nodeType);
		if(nodeType==2 || nodeType == 3){
			getCfgItemTreeTable($("#sysId").val(),curSysCfgTree.data.nodeCode);
		}
	});
}

function reloadRightByCode(sysId, nodeCode,nodeType) {
	$("#rightSyscfgCatalogUpper").show();
	var path = 'platform/syscfgCatalog/getCfgDetailByCode/' + sysId + "/" + nodeCode;
	$("#rightSyscfgCatalogUpper").load(path, function() {
		initForm();
		showViewAndAddRules(nodeType);
	});
}


// 添加根节点
function addRoot() {
	if (!canAdd) {
		$.alert("请先编辑完当前节点!", "提示");
		return;
	}
	//添加顶级节点时 取消当前选中
	if(curSysCfgTree!=null){
		$('#treeSyscfgCatalog').treeview('unselectNode', [curSysCfgTree.nodeId, { silent: true }]);
		curSysCfgTree = null;
	}
	canAdd = false;
	reloadRight(0, 0);
};

//添加子节点
function addNode(){
	if(!canAdd){
		$.alert("请先编辑完当前节点!","提示");
		return;
	}
	if(curSysCfgTree == null || curSysCfgTree.data.nodeType != 0){
		$.alert("请先选择一个目录节点!","提示");
		return;
	}
	var pid = curSysCfgTree.id;
	canAdd = false;
	
	reloadRight(0,pid);
}

function selectSysCfgTree() {
	showModal('moveSysCfgDiv',"platform/syscfgCatalog/selectSysCfgTree/", function() {
		$('#modelSysCfgNode').modal();
		var sysId = $("#sysId").val();
		$.getJSON('platform/syscfgCatalog/getCfgTree?sysId='+sysId,function(data){
			var treeSysCfgData = TR.turnToTreeViewData(data,{namePro:"nodeName"});
			$('#selectSysCfgTree').treeview({
				data: treeSysCfgData,
				showBorder:false,
				highlightSelected:true,
				//selectedBackColor:'#808080',
				onNodeSelected: function(event, data) {
					curMoveTreeNode = data;
				},
				onNodeUnselected: function(event, data) {
					curMoveTreeNode = null;
				}
			});
		});
	});
}

function moveNodeSysCfg(){
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	if(curSysCfgTree==null){
		$.alert("请选择一个节点!","提示");
		return;
	}
	selectSysCfgTree();
	
}

function selectSysCfgNode(){
	if(curMoveTreeNode == null ||curMoveTreeNode.data.nodeType!=0){
		$.alert("请选择一个目录节点!","提示");
		return;
	}
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认移动当前节点?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/syscfgCatalog/moveSysCfg",
				data : {
					fromId : curSysCfgTree.id,
					toId : curMoveTreeNode.id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						$('#modelSysCfgNode').modal('hide');
						reloadPage();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
	
}



function submitCfg() {
	//提交之前，判断字典编码是否改变，如果改变，判断原来字典下是否有值，有值就不能修改。
	var type = $("#nodeType1").val();
	if(type==1||type==3){//字典和组合配置组
		var oldDictCode = $("#oldDictCode").text();
		var newDictCode = $("#refDict").val();
		
		if(oldDictCode!=newDictCode && oldDictCode!=''){//修改了字典编码
			   $.ajax({
					url:"platform/syscfgCatalog/checkDictData/"+$("#sysId").val()+"/"+oldDictCode,
					type:"post",
					success:function(data){
						if(data){
							 $.alert("原字典下有数据，请先删除该字典下的所有数据后再进行修改","提示");
							 //$("#refDict").val(oldDictCode);
							return;
						}else{
							 $("#formCfgDetail").submit();
						}
					}
				});
		}else{
			  $("#formCfgDetail").submit();
		}
	}else{
		$("#formCfgDetail").submit();
	}
		
}

function initForm() {
	$("#formCfgDetail").validate(
			{
				rules : {
					nodeName1 : {
						required : true,
						minlength : 1,
						maxlength : 100
					},
					nodeType1 : {
						type : true,
					},
					nodeCode : {
						required : true,
						minlength : 1,
						maxlength : 30,
						remote : {
							url : "platform/syscfgCatalog/checkCode/"
									+ $("#sysId").val(),
							type : "post",
							data : {
								nodeCode : function() {
									return $("#nodeCode").val();
								},
								id : function() {
									return $("#id").val();
								}
							},
							dataFilter : function(data) {
								if (data == 'ok') {
									return true;
								} else {
									return false;
								}
							}
						}
					},
				},
				messages : {
					nodeCode : {
						remote : "编码已存在",
					},
					refDict:{
						remote:"编码已存在",
					}
				},
				submitHandler : function(form) {
					$.ajax({
						cache : true,
						type : "POST",
						url : "platform/syscfgCatalog/saveOrupdate/"
								+ $("#sysId").val() + "/"
								+ $("#nodeType1").val(),
						data : $('#formCfgDetail').serialize()+"&nodeName="+ $("#nodeName1").val(),
						async : false,
						error : function(request) {
							showErrorMsg("请求失败，请刷新重试");

						},
						success : function(data) {
							if (data.type == 'success') {
								showSuccessMsg("新增成功！");
								canAdd = true;
								var id=$("#id").val();
								if(!id){//新增后
									loadSysCfgTree(data.txt);
									//reloadRightByCode($("#sysId").val(),$("#nodeCode").val(),$("#nodeType1").val());
									//$('#treeSyscfgCatalog').treeview('unselectNode', [ curSysCfgTree.nodeId, { silent: true } ]);//取消当前选择
								}else{//编辑后
									reloadRightReadOnly($("#id").val(),$("#pid").val());
								}
							
							} else {
								showErrorMsg(data.txt);
							}
						}
					});

				},
				errorPlacement : setErrorPlacement,
				success : validateSuccess,
				highlight : setHighlight
			});
}

//取消
function cancel(){
	if(curSysCfgTree != null){//说明是编辑的 重新点击该节点就可以了
		reloadRightReadOnly(curSysCfgTree.id,curSysCfgTree.pid);
	}else{
		//将右侧隐藏
		$("#rightSyscfgCatalogUpper").hide();
	}
	canAdd = true;
}

//修改节点信息
function editNode() {
	if(!canAdd){
		$.alert("请先编辑完当前节点!","提示");
		return;
	}
	if(curSysCfgTree == null){
		$.alert("请先选择一个节点!","提示");
		return;
	}
	$('#formCfgDetail').find('input,textarea,button').attr('disabled',false).attr('readonly',false);
	$("#nodeCode").attr("readonly",true);
	canAdd = false;

};

//删除节点
function deleteNode() {
	if(!canAdd){
		$.alert("请先编辑完当前节点!","提示");
		return;
	}
	if (curSysCfgTree == null) {
		$.alert("请先选择一个节点!","提示");
		return;
	}
	if (curSysCfgTree.nodes) {
		$.alert("父节点不允许直接删除,请先删除该节点下的所有子节点后重新操作");
		return;
	}else{
		if (curSysCfgTree.data.nodeType == 2 || curSysCfgTree.data.nodeType == 3) {
			$.ajax({
				type : 'post',
				url : "platform/syscfgCatalog/getCfgItemTreeTable/" + $("#sysId").val() + "/"+ curSysCfgTree.data.nodeCode,
				success:function(data){
					if(data.length>0){
						$.alert("该节点下存在配置项，请先删除配置项后再删除节点","提示");
						return;
					}else{
						if(curSysCfgTree.data.nodeType == 3){
							   $.ajax({
									url:"platform/syscfgCatalog/checkDictData/"+$("#sysId").val()+"/"+$("#refDict").val(),
									type:"post",
									success:function(data){
										if(data){
											  $.alert("该节点下有数据字典，请删除字典后重新删除节点","提示");
												return;
										}else{
											deleteSysCfg(curSysCfgTree);
										}
									}
								});
						}else{
							deleteSysCfg(curSysCfgTree);
						}
					}
				}
			});
		}else if(curSysCfgTree.data.nodeType == 1){
		   $.ajax({
				url:"platform/syscfgCatalog/checkDictData/"+$("#sysId").val()+"/"+$("#refDict").val(),
				type:"post",
				success:function(data){
					if(data){
						  $.alert("该节点下有数据字典，请删除字典后重新删除节点","提示");
							return;
					}else{
						deleteSysCfg(curSysCfgTree);
					}
				}
			});
		   
		}else{
			deleteSysCfg(curSysCfgTree);
		}
	}
};

function deleteSysCfg(curSysCfgTree){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除当前节点?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/syscfgCatalog/deleteNode",
				data : {
					id : curSysCfgTree.id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadPage();
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
}



//显示不同div并为相应字段添加验证规则
function showViewAndAddRules(typeValue){
	if(typeValue == 0){//目录
		$('#formCfgDetail #refDictDiv,#refTransDiv,#ItemTreetable').attr('disabled',true).attr("hidden",true);
	}else if(typeValue == 1){//数据字典
		$('#formCfgDetail #refDictDiv').attr('disabled',false).attr("hidden",false);
		$('#formCfgDetail #refTransDiv,#ItemTreetable').attr('disabled',true).attr("hidden",true);
		$("#refDict").rules("add",{required:true});
		$("#refDict").rules("add",{
			remote : {//验证数据字典编码唯一
				url : "platform/syscfgCatalog/checkDictCode/"
						+ $("#sysId").val(),
				type : "post",
				data : {
					refDict : function() {
						return $("#refDict").val();
					},
					id:function(){
						return $("#id").val();
					}
				},
				dataFilter : function(data) {
					if (data == 'ok') {
						return true;
					} else {
						return false;
					}
				}
			}
		});
	}else if(typeValue == 2){//配置组
		$('#formCfgDetail #refDictDiv').attr('disabled',true).attr("hidden",true);
		$('#formCfgDetail #refTransDiv').attr('disabled',true).attr("hidden",true);
		$('#ItemTreetable').attr('disabled',false).attr("hidden",false);
	}else if(typeValue == 3){//组合配置组
		$('#formCfgDetail #refDictDiv').attr('disabled',false).attr("hidden",false);
		$('#formCfgDetail #refTransDiv').attr('disabled',true).attr("hidden",true);
		$('#ItemTreetable').attr('disabled',false).attr("hidden",false);
		$("#refDict").rules("add",{required:true});
		$("#refDict").rules("add",{
			remote : {//验证数据字典编码唯一
				url : "platform/syscfgCatalog/checkDictCode/"
						+ $("#sysId").val(),
				type : "post",
				data : {
					refDict : function() {
						return $("#refDict").val();
					},
					id:function(){
						return $("#id").val();
					}
				},
				dataFilter : function(data) {
					if (data == 'ok') {
						return true;
					} else {
						return false;
					}
				}
			}
		});
	}else if(typeValue == 4){//其他作业
		$('#formCfgDetail #refDictDiv,#ItemTreetable').attr('disabled',true).attr("hidden",true);
		$('#formCfgDetail #refTransDiv').attr('disabled',false).attr("hidden",false);
		$("#refTrans").rules("add",{required:true});
	}
}

//节点类型改变事件
function onchangeType(){
	var typeValue = $("#nodeType1").val();
	showViewAndAddRules(typeValue);
}



/**********************************树表************************************************/

//标识当前行
var curItemTreeId = -1;

//加载配置树表
function getCfgItemTreeTable(sysId,cfgnodeCode){
	$.ajax({
		type : 'post',
		url : "platform/syscfgCatalog/getCfgItemTreeTable/"+sysId+"/"+cfgnodeCode,
		success : function(data) {
			var treeData = TR.turnToZtreeData(data);
			var html = "";
			html = addChild(treeData,html);
			$("#treeTableItemDetail").html(html);
			
			function addChild(data) {
				$.each(data, function(i, d) {
					if(d.pid){
						html += "<tr onclick='changeCurTreeId("+d.id+")' data-tt-id='"+d.id+"' data-tt-parent-id='"+d.pid+"'>";
					}else{
						html += "<tr onclick='changeCurTreeId("+d.id+")' data-tt-id='"+d.id+"'>";
					}
					
					if(d.children){//存在子节点
						html +="<td><span class='folder'>"+d.data.itemName+"</span></td><td>"+d.data.itemCode+"</td>";
						if(d.data.itemType==0){//目录
							html += "<td>目录</td>";
						}else if(d.data.itemType==1){//checkbox
							html += "<td>多选框</td>";
						}else if(d.data.itemType==2){//下拉列表
							html += "<td>下拉列表(单选)</td>";
						}else {//填空
							html += "<td>填空</td>";
						}
						
						html +="<td>"+d.data.optionalValues+"</td><td>"+d.data.itemNote+"</td>";
						
						html += "</tr>";
						addChild(d.children,html);
						
					}else{
						
						html +="<td><span class='file'>"+d.data.itemName+"</span></td><td>"+d.data.itemCode+"</td>";
						if(d.data.itemType==0){//目录
							html += "<td>目录</td>";
						}else if(d.data.itemType==1){//checkbox
							html += "<td>多选框</td>";
						}else if(d.data.itemType==2){//下拉列表
							html += "<td>下拉列表(单选)</td>";
						}else {//填空
							html += "<td>填空</td>";
						}
						
						html +="<td>"+d.data.optionalValues+"</td><td>"+d.data.itemNote+"</td>";
						
						html += "</tr>";
					}
				});
				return html;
			}

			var option = {
				expandable: true,
			    expandLevel : 4
			};

			$("#treeTableSyscfgItem").treetable(option);
			jQuery('#treeTableSyscfgItem').treetable('expandAll');

			$("#treeTableSyscfgItem tbody tr").mousedown(function() {
				$("tr.selected").removeClass("selected");
				$(this).addClass("selected");
			});


			$("#treeTableSyscfgItem.folder").each(function() {
				$(this).parents("tr").droppable({
					 accept: ".file, .folder",
					 drop: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   $("#treeTableSyscfgItem").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
					 },
					 hoverClass: "accept",
					 over: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   if(this != droppedEl[0] && !$(this).is(".expanded")) {
					     $("#treeTableSyscfgItem").treetable("expandNode", $(this).data("ttId"));
					   }
					 }
				});
			});
		}
	});
}

function changeCurTreeId(id){
	curItemTreeId = id;
}

function showModal(divId,url, callback) {
	$('#'+divId).load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

//添加配置项根节点
function addItemRoot() {
	if (!canAdd) {
		$.alert("请先编辑完当前节点基本信息后再添加配置项!", "提示");
		return;
	}
	showModal("sysCfgItmeModelDiv","platform/syscfgCatalog/addItemNode/-1", function() {
		$('#sysCfgItemDetailModel').modal();
		initItemForm();
	});
}

//添加配置项子节点
function addItemNode() {
	if (!canAdd) {
		$.alert("请先编辑完当前节点基本信息后再添加配置项!", "提示");
		return;
	}
	if (curItemTreeId == -1) {
		$.alert("请先选择一个配置行!","提示");
		return;
	}
	
	var type = $("tr[data-tt-id="+curItemTreeId+"] td:eq(2)").text();
	if(type!='目录'){
		$.alert("非目录节点无法添加子配置项!","提示");
		return;
	}
	
	showModal("sysCfgItmeModelDiv","platform/syscfgCatalog/addItemNode/" + curItemTreeId + "", function() {
		$('#sysCfgItemDetailModel').modal();
		initItemForm();
	});
}

//修改配置项节点
function editItemNode(id) {
	if (!canAdd) {
		$.alert("请先编辑完当前节点基本信息后再添加配置项!", "提示");
		return;
	}
	if (curItemTreeId == -1) {
		$.alert("请先选择一个配置行!","提示");
		return;
	}
	showModal("sysCfgItmeModelDiv","platform/syscfgCatalog/getItemNode/" + curItemTreeId + "", function(a) {
		$('#sysCfgItemDetailModel').modal();
		initItemForm();
	});
}

//删除配置项
function deleteItemNode() {
	if (!canAdd) {
		$.alert("请先编辑完当前节点基本信息后再添加配置项!", "提示");
		return;
	}
	if (curItemTreeId == -1) {
		$.alert("请先选择一个配置行!","提示");
		return;
	}
	
	$.ajax({
		url:"platform/syscfgCatalog/hasChild",
		type:"post",
		data:{id : curItemTreeId},
		success:function(data){
			if(data=="ok"){//无子节点
				$.confirm({
					title : '提示',
					confirmButton:'确认',
					cancelButton:'取消',
					content : '确认删除当前配置行?',
					confirm : function() {
						$.ajax({
							cache : true,
							type : "POST",
							url : "platform/syscfgCatalog/deleteSysItem",
							data : {
								id : curItemTreeId
							},
							async : false,
							error : function(request) {
								showErrorMsg("请求失败，请刷新重试");
							},
							success : function(data) {
								if (data.type == 'success') {
									showSuccessMsg(data.txt);
									getCfgItemTreeTable($("#sysId").val(),$("#nodeCode").val());
									curItemTreeId = -1;
								} else {
									showErrorMsg(data.txt);
								}
							}
						});
					},
					cancel : function() {
						
					}
				});
			}else{//有子节点
				$.alert("请先删除本配置项下的子配置项，再删除本配置","提示");
			}
		}
	});
	
}

//配置项提交
function submitSysItem(){
	$("#sysCfgItemForm").submit();
}

function initItemForm() {
	$("#sysCfgItemForm").validate({
		rules : {
			itemCode : {
				required : true,
				minlength : 1,
				maxlength : 30,
				remote : {
					url : "platform/syscfgCatalog/checkSysItemCode",
					type : "post",
					data : {
						itemCode : function() {
							return $("#itemCode").val();
						},
						sysId: function() {
							return $("#sysId").val();
						},
						id:function(){
							return $("#itemId").val();
						}
					},
					dataFilter : function(data) {
						if (data == 'ok') {
							return true;
						} else {
							return false;
						}
					}
				}
			},
			itemName : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			itemType : {
				type:true
			}
		},
		messages : {  
			itemCode : {  
	            remote: "编码已存在"
	          }
	    },
		submitHandler : function(form) {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/syscfgCatalog/saveOrupdateSysItem/"+$("#sysId").val()+"/"+$("#nodeCode").val(),
				data : $('#sysCfgItemForm').serialize(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
					$('#sysCfgItemDetailModel').modal('hide');
				},
				success : function(data) {
					$('#sysCfgItemDetailModel').modal('hide');
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						getCfgItemTreeTable($("#sysId").val(),$("#nodeCode").val());
						curItemTreeId = -1;
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}