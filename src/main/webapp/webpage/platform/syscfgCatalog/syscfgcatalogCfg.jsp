<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.theme.default.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<script src="resource/plugins/jqueryTreeTable/javascripts/src/jquery.treetable.js?version=${globalVersion}" type="text/javascript"></script>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading form-inline" > 
					<span class="glyphicon glyphicon-home" aria-hidden="true"></span> 系统配置
					<div class="form-group" style="margin-left: 30px;">
	             		<label for="sysId" class="control-label" >系统</label>
				        	<select class="form-control"  id="sysCfgSysId" name="sysId" style="width: 150px" onchange="changeSysId()">
				        	 	<c:forEach items="${dictCodeDtoList}" var="dictCodeDto">
									<option value="${dictCodeDto.itemCode}">${dictCodeDto.itemTxt}</opton>
								</c:forEach>
							 </select>
				    </div>
				</div>   
				   
			    <div id="treeSyscfgCatalog" style="margin-left: 10px;"></div>    	
        	</div>
        	
		</div>
		<div class="col-md-8 animated fadeInRight">
			  <div class="row">
				<div class="col-md-12">
					<div id="syscfgDicMainDiv"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="syscfgDicItemDataMainDiv"></div>
				</div>
			</div>
		</div>
	</div>
	<div id="syscfgCatalogDicDiv"></div>
</div>
<script type="text/javascript" src="webpage/platform/syscfgCatalog/syscfgcatalogCfg.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#sysCfgSysId").val("${sysId}");
});
</script>