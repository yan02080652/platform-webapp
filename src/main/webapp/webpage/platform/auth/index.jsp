<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link href="webpage/platform/auth/index.css?version=${globalVersion}" rel="stylesheet">
<c:choose>  
<c:when test="${empty roleId}"><center><h3 style="color:red;"><!--请先选择角色  --></h3></center></c:when>  
<c:otherwise>
<input type="hidden" value="${roleId }" id="roleId"/>
<div class="row" style="margin-top:8px;margin-right: 0px;">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-bars" aria-hidden="true"></i> 角色授权 -
				<select id="sceneId" name="sceneId" data-style="btn-info" style="width: 150px;" onchange="pageVal.changeSceneId()">
		        	 	<c:forEach items="${dictCodeDtoList}" var="dictCodeDto">
							<option value="${dictCodeDto.itemCode}">${dictCodeDto.itemTxt}</option>
						</c:forEach>
				</select>
			</div>
			<div class="panel-body">
				<div style="width: 370px;float: left;">
					<p>
						<button type="button" class="btn btn-default fa fa-check" onclick="checkAll()"> 全选</button>
						<button type="button" class="btn btn-default fa fa-times" onclick="unCheckAll()"> 全不选</button>
						<button id="saveMenuBtn" type="button" class="btn btn-info fa fa-floppy-o pull-right" onclick="saveRoleMenu()"> 保存</button>
					</p>
					<div id="menuAuth" style="margin-top: 5px;"></div>
				</div>
				<div id="roleTrans" style="margin-left:10px;display: inline-block;position: fixed;">
					
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="resource/js/bootstrap/bootstrap-select.js?version=${globalVersion}"></script>
<script type="text/javascript" src="webpage/platform/auth/index.js?version=${globalVersion}"></script>
</c:otherwise>  
</c:choose>