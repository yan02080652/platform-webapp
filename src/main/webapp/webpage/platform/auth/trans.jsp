<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<c:choose>
	<c:when test="${empty roleTrans || empty roleTrans.transDto}">
		<h3 class="span12" style="text-align: center; color: red;">作业未定义</h3>
	</c:when>
	<c:otherwise>
		<div class="panel panel-default">
			<div class="panel-heading"><i class="fa fa-shield" aria-hidden="true"> 设置作业【<b>${roleTrans.transDto.name }</b>】操作权限</i></div>
			<div class="panel-body">
				<dl>
					<!-- <dt>授权明细</dt>
					<dd>名称:${roleTrans.transDto.name }</dd>
					<dd>定义:${roleTrans.transDto.def }</dd>
					<dt>
						作业活动<span class="label label-warning" style="margin-left: 10px;">需要菜单授权</span>
					</dt>
					 -->
					<dd>
						<ul class="list-unstyled">
							<c:forEach items="${roleTrans.activityMap}" var="ac">
								<li>
									<div class="checkbox">
										<label class="checkbox"> <input type="checkbox" name="trans_activity" value="${ac.key}" ${fn:contains(roleTrans.roleTransActivity, ac.key)?'checked':''} onchange="showSubmitAcBtn()">
											${ac.value}
										</label>
									</div>
								</li>
							</c:forEach>
						</ul>
					</dd>
					<%--<dd style="display: none;" id="dd_submit_btn">
						<button type="button" class="btn btn-info btn-sm fa fa-check" onclick="submitActivity()"></button>
					</dd>--%>
				</dl>
			</div>
		</div>
	</c:otherwise>
</c:choose>