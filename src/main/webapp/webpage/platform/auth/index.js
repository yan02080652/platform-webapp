var pageVal = {
//		selectDropdownRole:function(dom){
//			TR.select('dropdownRole',{
//				dom:dom,
//				levels:1
//			},function(data){
//				var roleId = data?data.id:null;
//				$('#auth_roleId').val(roleId);
//				$('#auth_roleName').val(data?data.name:null);
//				loadRoleData(roleId);
//			});
//		},
		loadData:function(callback){
			$.getJSON('platform/auth/getData.do',{
				roleId:this.roleId,
				sceneId:this.sceneId,
			},function(data){
				$('.selectRoleBtn').attr('disabled',null);
				callback(data);
			});
		},
		changeSceneId:function(){
			this.sceneId = $('#sceneId').val();
			this.flushTree();
			closeRight();
		},
		flushTree:function(){
			this.roleId = $('#roleId').val();
			this.sceneId = $('#sceneId').val();
            this.setCookie('_role_scene',this.sceneId,'1h');
			if(!this.roleId){
				return;
			}
			if(!this.sceneId){
				$.alert('请选择一个系统','提示');
				return;
			}
			require(['treeview'],function(){
				initMenu();
			});
		},
		getCookie:function(name){
            var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
            if (arr = document.cookie.match(reg))
                return unescape(arr[2]);
            else
                return null;
		},
    	setCookie:function(name, value, time) {
			var strsec = getsec(time);
			var exp = new Date();
			exp.setTime(exp.getTime() + strsec * 1);
			document.cookie = name + "=" + escape(value) + ";expires="
				+ exp.toGMTString();
		},
    	codeAvs:{}
};

//利用cookie缓存一下场景
var scene = pageVal.getCookie('_role_scene');
if(scene) {
    $('#sceneId').val(scene);
}

function getsec(str) {
    var str1 = str.substring(1, str.length) * 1;
    var str2 = str.substring(0, 1);
    if (str2 == "s") {
        return str1 * 1000;
    } else if (str2 == "h") {
        return str1 * 60 * 60 * 1000;
    } else if (str2 == "d") {
        return str1 * 24 * 60 * 60 * 1000;
    }
}



pageVal.flushTree();

pageVal.turnToTreeViewData = function(data, config){
	if (!data) {
		return [];
	}
	config = config || {};
	
	id = config.id || 'id';
	pid = config.pid || 'pid';
	text = config.namePro || 'name';
	var nodeDataTop = [];
	var childrenData = {};
	var roleMenus = pageVal.roleMenus || {};
	$.each(data, function(i, d) {
		var tags = [];
		if(d.type == 1 && d.def){
			tags.push("<span class='fa fa-shield' aria-hidden='true'></span>");
		}
		var node = {
			id : d[id],
			pid : d[pid],
			text : d[text],
			data : d,
			state:{
				checked:!!roleMenus[d.id]
			},
			tags: tags
		};
		if (!node.pid) {
			nodeDataTop.push(node);
		} else {
			if (!childrenData[node.pid]) {
				childrenData[node.pid] = [];
			}
			childrenData[node.pid].push(node);
		}
	});

	$.each(nodeDataTop, function(i, n) {
		addChild(n);
	});

	function addChild(n) {
		var children = childrenData[n.id];
		if (children) {
			$.each(children, function(i, c) {
				addChild(c);
			});
			n.nodes = children;
		}
	}
	return nodeDataTop;
};

function initMenu(){
	pageVal.loadData(function(data){
		data = data || {};
		var menus = data.menus || [];
		
		//当前场景下的菜单
		var tempMenuScene = {};
		$.each(menus,function(i,m){
			tempMenuScene[m.id] = m.sceneId;
		});
		
		//当前场景下的菜单授权
		var roleMenus = {};
		$.each(data.roleMenus,function(i,rm){
			var sc = tempMenuScene[rm.menuId];
			if(pageVal.sceneId == sc){//该菜单授权是否在当前场景中
				roleMenus[rm.menuId] = rm;
			}
		});
		pageVal.roleMenus = roleMenus;
		var treeMenuData = pageVal.turnToTreeViewData(menus);
		$('#menuAuth').treeview({
			data: treeMenuData,
			//highlightSelected:false,
			showCheckbox:true,
			showTags:true,
			onNodeSelected:onNodeSelected,
			onNodeChecked:onNodeSelected,
			onNodeUnchecked:closeRight
		});
	});
}

function checkAll(){
	$('#menuAuth').treeview('checkAll', { silent: true });
}

function unCheckAll(){
	$('#menuAuth').treeview('uncheckAll', { silent: true });
}

//保存菜单授权
function saveRoleMenu(){
	//var roleId = $('#auth_roleId').val();
	if(!pageVal.roleId){
		$.alert('提示','请选择一个角色');
		return;
	}
	if(!pageVal.sceneId){
		$.alert('提示','请选择一个场景');
		return;
	}
	var nodes = $('#menuAuth').treeview('getChecked');
	
	var oldMenuIds = $.extend({}, pageVal.roleMenus);
	var addMenuIds = [];
	$.each(nodes,function(i,n){
		var id = n.id;
		if(oldMenuIds[id]){//相同的忽略
			delete oldMenuIds[id];
		}else{//不同的是新增菜单
			addMenuIds.push(id);
			pageVal.roleMenus[id] = {menuId:id};
		}
		//剩下的是删除菜单
	});
	var deleteMenuIds = [];
	$.each(oldMenuIds,function(k,v){
		deleteMenuIds.push(k);
	});
	
	$('#saveMenuBtn').hide();

    var codeAvs = getCodeAvData();
	$.ajax({
		type:'POST',
		url:'platform/auth/saveRoleMenu.do',
		data:{
			roleId:pageVal.roleId,
			addMenuIds:addMenuIds.join(','),
			deleteMenuIds:deleteMenuIds.join(','),
            codeAvs:JSON.stringify(codeAvs)
		},
		success:function(rs){
			if('success' == rs.type){
				showSuccessMsg('保存成功。');
			}
			
			setTimeout(function(){
				$('#saveMenuBtn').show();
			}, 1500);
		},
		error:function(){
			showErrorMsg('保存失败，请刷新重试。');
            $('#saveMenuBtn').show();
		}
	});
}

function onNodeSelected(event,node){
	var roleId = pageVal.roleId;
	if(!roleId){
		return;
	}

	var rightPanel = $('#roleTrans');
	var menu = node.data;
	if(menu.type == 1 && menu.def && node.state.checked){

		var curTransCode = rightPanel.data('transCode');
		if(menu.def == curTransCode){
			return;
		}
		rightPanel.data('transCode',menu.def);
		rightPanel.data('menuId',menu.id);
		rightPanel.load('platform/auth/editRoleTrans.do',{
			roleId:roleId,
			transCode:menu.def
		},function(){
			setTimeout(function(){
				afterLoadRight();
			}, 50);
			
			//console.log(arguments);
		});
	}else{
		rightPanel.data('transCode',null).empty();
	}
}

//用pageVal.codeAvs 存储所有已编辑活动
function afterLoadRight() {

	//同步权限选中状态
	var codeAvs = pageVal.codeAvs;
    var code = $("#roleTrans").data('transCode');
    var codeAv = codeAvs[code];
    var changed = codeAv && codeAv.changed;

	var value = {};
    var cbs =  $("#roleTrans [name='trans_activity']");
    $.each(cbs,function(i,cb){
        var ac = $(cb).val();
        if(ac){
            if(changed){
                $(cb).prop('checked', !!codeAv.value[ac]);
            }else{
                value[ac] = $(cb).prop('checked');
            }
		}
    });
    if(!changed) {
        codeAvs[code] = codeAv = {
        	value:value
		};
    }

	$('#roleTrans [name=trans_activity]').change(function () {
        codeAv['value'][this.value] = this.checked;
        codeAv.changed = true;
    });
}

function getCodeAvData() {
	var rs = {};
	$.each(pageVal.codeAvs,function (code,codeAv) {
		if(codeAv.changed){
			var avs = [];
            $.each(codeAv.value,function (v,checked) {
            	if(checked) {
                    avs.push(v);
                }
            });
            rs[code] = avs.join(',');
		}
    });
	//TODO 将没有勾中菜单对应的作业活动不处理
	return rs;
}

function closeRight(){
	var rightPanel = $('#roleTrans');
	rightPanel.data('transCode',null);
	rightPanel.data('menuId',null);
	rightPanel.empty();
}

/**
 * 显示提交按钮
 */
function showSubmitAcBtn(){
	$('#dd_submit_btn').show();
}

/**
 * 提交作业菜单授权
 */
function submitActivity(){
	var roleId = pageVal.roleId;
	var rightData = $('#roleTrans').data();
	var activities = [];
	var cbs =  $("#roleTrans [name='trans_activity']:checked");
	$.each(cbs,function(i,cb){
		var ac = $(cb).val();
		if(ac){
			activities.push(ac);
		}
	});
	$.ajax({
		type:'POST',
		url:'platform/auth/saveRoleMenuTrans.do',
		data:{
			roleId:roleId,
			menuId:rightData.menuId,
			transCode:rightData.transCode,
			activities:activities.join(',')
		},
		success:function(rs){
			if('success' == rs.type){
				showSuccessMsg(rs.txt);
				closeRight();
			}else{
				showErrorMsg(rs.txt);
			}
		}
	});
};