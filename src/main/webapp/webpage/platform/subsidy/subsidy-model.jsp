<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="modal fade" id="modelSubsidy" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="closeModal()" aria-hidden="true"> &times;</button>
				<h4 class="modal-title">企业账户补贴<c:if test="${subsidy.id != null}">修改</c:if><c:if test="${subsidy.id == null}">添加</c:if></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="subsidy_model" class="form-horizontal" role="form">
						<input type="hidden" name="id" value="${subsidy.id}">

						<div class="form-group">
							<label class="col-sm-4 control-label">企业:</label>
							<div class="col-sm-7">
								<input name="partnerId" type="hidden" id="queryBpId" value="${subsidy.bpId}"/>
								<input type="text" id="queryBpName" name="queryBpName" value="${subsidy.bpName}" onclick="selectPartner('${tmcId}')" class="form-control" placeholder="请选择企业"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">所属账户:</label>
							<div class="col-sm-7 input-group " style="padding-left: 15px">
								<input name="accountId" type="hidden" value="${subsidy.accountId}"/>
								<input type="text" name="accountName" value="${subsidy.accountName}" readonly onclick="selectMyDropDown()" class="form-control"/>
								<span class="input-group-btn">
									<button class="btn btn-default" id="accountSelect" type="button" onclick="selectMyDropDown()"> <i class="fa fa-caret-down" aria-hidden="true"></i></button>
								</span>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">补贴金额:</label>
							<div class="col-sm-7">
								<input type="number" name="countAmount" min="0" max="10000000" value="<fmt:formatNumber value="${subsidy.countAmount * 0.01}" pattern="###0"/>" class="form-control">
							</div>
						</div>
						<%--<div class="form-group">--%>
							<%--<label class="col-sm-4 control-label">未清账金额:</label>--%>
							<%--<div class="col-sm-7">--%>
								<%--<input type="number"  name="remainAmount" value="${subsidy.remainAmount * 0.01}" class="form-control">--%>
							<%--</div>--%>
						<%--</div>--%>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" onclick="closeModal()">关闭</button>
				<button type="button" class="btn btn-primary" onclick="saveSubsidy()">确认</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

    $(function () {

        $("#subsidy_model").validate({
            rules:{
                queryBpName : {
                    required : true
                },
                accountName : {
                    required : true
                },
                countAmount : {
                    required : true,
                    digits:true
                }
            },
			messages:{
                countAmount :{
                    digits : "请输入一个正整数"
				}
			},
            submitHandler : function(form) {

                $.post("platform/subsidy/save",$("#subsidy_model").serialize(),
                    function(data){
                        if (data.code == '0') {
                            layer.msg("保存成功");
                            window.location.reload();
                            closeModal();
                        }else{
                            layer.msg(data.msg);
                        }
                    }
                );
            },
            errorPlacement : setErrorPlacement,
            success : validateSuccess,
            highlight : setHighlight
        });
    })

    function saveSubsidy(){
        $("#subsidy_model").submit();
    }

    function closeModal(){
        $('#modelSubsidy').modal('hide');
    }

    function selectPartner(tmcId){

        TR.select('partner',{
            type:0,//固定参数
            tmcId:tmcId
        },function(data){
            $("#queryBpId").val(data.id);
            $("#queryBpName").val(data.name);
        });
    }

    function selectMyDropDown(){
        var partnerId = $("#queryBpId").val();
        if(!partnerId){
            $("input[name='accountName']").blur();
            $.alert('请先选择企业！','提示');
            return;
        }

        $('.dropdown-common-list').remove();

        $.get('widget/account/getAccountList',{
            partnerId : partnerId
        },function(data){
            TR.select(data,{
                key:'custom1',//多个自定义下拉选择时key值不能重复
                dom:$('#accountSelect'),
                name:'accountName',//default:name
                allowBlank:true//是否允许空白选项
            },function(result){
                $("input[name='accountId']").val(result ? result.id : null);
                $("input[name='accountName']").val(result ? result.accountName : null);
            });

        });

    }

</script>