<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ page import="com.tem.platform.security.authorize.PermissionUtil" %>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">

			<div class="form-inline">

				<div class="col-md-9">

					<span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业账户补贴&nbsp;&nbsp;
					<div class="form-group">
						<jsp:include page="../../common/partnerChange.jsp"></jsp:include>
					</div>

				</div>


			</div>

		</div>

		<hr/>

		<div class="col-sm-12" id="billListDiv">
			<div class="form-group">
				<a class="btn btn-default" onclick="getSubsidy();" href="javascript:;" role="button">添加</a>
			</div>

			<table style="margin-bottom : 0px" class="table table-bordered">
				<thead>
					<tr>
						<th class="sort-column" >企业名称</th>
						<th class="sort-column" >企业账户</th>
						<th class="sort-column" >补贴金额</th>
						<th class="sort-column" >补贴可用金额</th>
						<th class="sort-column" >更新时间</th>
						<th class="sort-column" >更新人</th>
						<th style="min-width: 70px">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="sub">
						<tr>
							<td style="text-align:center;">${sub.bpName}</td>
							<td>${sub.accountName}</td>
							<td><fmt:formatNumber value="${sub.countAmount * 0.01}" pattern="#,##0.##"/></td>
							<td><fmt:formatNumber value="${sub.remainAmount * 0.01}" pattern="#,##0.##"/></td>
							<td><fmt:formatDate value="${sub.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td>${sub.updateUserName}</td>
							<td><a class="btn btn-default" onclick="getSubsidy('${sub.id}');" href="javascript:;" role="button">修改</a>
								<a class="btn btn-default" onclick="deleteSubsidy('${sub.id}');" href="javascript:;" role="button">删除</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=querySubList"></jsp:include>
			</div>			
		</div>
	</div>
</div>

<form id="subsidy_form" action="platform/subsidy/index" method="post">
	<input name="pageIndex" type="hidden"/>
</form>

<div id="subsidy_div">

</div>

<script type="text/javascript" >

//查询补贴记录
function querySubList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("input[name='pageIndex']").val(pageIndex);

    $("#subsidy_form").submit();
}

function getSubsidy(id){
    $.get('/platform/subsidy/getSubsidy', {
        id : id
    },function (data) {
        $("#subsidy_div").html(data);
        $("#modelSubsidy").modal();
    })
}

//删除配置
function deleteSubsidy(id){
    layer.confirm('删除补贴记录',function () {
        $.post('/platform/subsidy/delete',{
            id : id
        },function () {
            window.location.reload();
        })
    })
}

</script>