<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>灰度环境配置&nbsp;&nbsp;
			</div>
			<div class="col-md-10">
				<form class="form-inline" id="queryform" action="platform/grayRule/index">
					<nav class="text-right">
						<div class=" form-group">
							<input type="hidden" id="pageIndex" name="pageIndex" />
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="queryBillList()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
		<hr/>

		<div class="col-sm-12" style="margin-bottom: 10px;">
			<div class="pull-left">
				<a class="btn btn-default" onclick="getGrayRule()" role="button">添加</a>
			</div>
		</div>

		<div class="col-sm-12">
			<table style="margin-bottom : 0px" class="table table-bordered">
				<thead>
					<tr>
						<th class="sort-column" >生效范围类型</th>
						<th class="sort-column" >生效范围值</th>
						<th class="sort-column" >生效状态</th>
						<th class="sort-column" >生效开始时间</th>
						<th class="sort-column" >生效结束时间</th>
						<th class="sort-column" >操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="grayRule">
						<tr>
							<td style="text-align:center;">${ScopeMap[grayRule.scopeType]}</td>
							<td>${grayRule.scopeName}</td>
							<td>${grayRule.status == 0 ? "可用" : "不可用"}</td>
							<td><fmt:formatDate value="${grayRule.validFrom}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td><fmt:formatDate value="${grayRule.validTo}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td>
								<a href="javascript:;" onclick="getGrayRule('${grayRule.id}')">修改</a>
								<a href="javascript:;" onclick="deleteGrayRule('${grayRule.id}')">删除</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryBillList"></jsp:include>
			</div>			
		</div>
	</div>
</div>

<div id="grayRule_div">

</div>

<script type="text/javascript" >

function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}

function queryBillList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("input[name='pageIndex']").val(pageIndex);
	
	$("#queryform").submit();
}


function getGrayRule(id){
    $.get('/platform/grayRule/getGrayRule', {
        id : id
	},function (data) {
		$("#grayRule_div").html(data);
		$("#modelGrayRule").modal();
    })
}

//删除配置
function deleteGrayRule(id){
    layer.confirm('删除配置',function () {
        $.post('platform/grayRule/delete',{
            id : id
        },function () {
            window.location.reload();
        })
    })
}

</script>