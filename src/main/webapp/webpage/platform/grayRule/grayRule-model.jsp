<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>

<div class="modal fade" id="modelGrayRule" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="closeModal()" aria-hidden="true"> &times;</button>
				<h4 class="modal-title">灰度环境配置</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="grayRule_model" class="form-horizontal" role="form">
						<input type="hidden" name="id" value="${grayRule.id}">
						<div class="form-group">
							<label class="col-sm-4 control-label">生效范围类型:</label>
							<div class="col-sm-7">
								<label class="radio-inline"><input  type="radio"  value="PARTNER" checked="checked" name="scopeType" class="ignore"/>企业</label>
								<lable class="radio-inline"><input  type="radio"  value="USER" name="scopeType" class="ignore"/>个人</lable>
								<lable class="radio-inline"><input  type="radio"  value="ALL" name="scopeType" class="ignore"/>全部</lable>
							</div>
						</div>
						<div class="form-group" id="select_all_div">
							<label class="col-sm-4 control-label">生效范围值:</label>
							<input type="hidden" name="scopeValue" />
							<input type="hidden" name="scopeName">
							<input type="hidden" id = "partnerId">

							<div class="col-sm-7" id = "select_partner_div">
								<input class="form-control" placeholder="选择企业" onclick="choosePartner(this)" value="${grayRule.scopeName}"/>
							</div>

							<div class="col-sm-7" id = "select_user_div" style="display: none;">
								<div class="col-sm-6" style="padding-left:0px;">
									<input class="form-control" placeholder="选择企业" onclick="choosePartner(this)"/>
								</div>
								<div class="col-sm-6" style="padding-right:0px;">
									<input class="form-control" placeholder="选择员工" value="${grayRule.scopeName}" onclick="chooseUser(this)"/>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">生效状态:</label>
							<div class="col-sm-7">
								<label class="radio-inline"><input  type="radio"  value="0" checked="checked" name="status" class="ignore"/>生效</label>
								<lable class="radio-inline"><input  type="radio"  value="1" name="status" class="ignore"/>不生效</lable>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">有效期开始:</label>
							<div class="col-sm-7">
								<input type="text" name="validFrom" class="form-control" value="<fmt:formatDate value="${grayRule.validFrom }" pattern="yyyy-MM-dd"/>"  onClick="WdatePicker()">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">有效期结束:</label>
							<div class="col-sm-7">
								<input type="text"  name="validTo" class="form-control" value="<fmt:formatDate value="${grayRule.validTo }" pattern="yyyy-MM-dd"/>"  onClick="WdatePicker()">
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" onclick="closeModal()">关闭</button>
				<button type="button" class="btn btn-primary" onclick="saveGrayRule()">确认</button>
			</div>
		</div>
	</div>
</div>

<script>

	$(function () {

	    var scopeType = '${grayRule.scopeType}';
	    var status = '${grayRule.status}';
	    if(scopeType){
            $("input[name='scopeType'][value="+scopeType+"]").attr("checked",true);
            select_user_partner(scopeType);
		}
		if(status){
            $("input[name='status'][value="+status+"]").attr("checked",true);
        }

        $("#grayRule_model").validate({
            rules:{
                scopeType : {
                    required : true
                },
                radio : {
                    required : true
                },
                validFrom : {
                    required : true
                },
                validTo : {
                    required : true
                }
            },
            submitHandler : function(form) {

                $.post("platform/grayRule/save",$("#grayRule_model").serialize(),
                    function(data){
                        if (data.code == '0') {
							layer.msg("保存成功");
							window.location.reload();
                            closeModal();
                        }else{
                            layer.msg("保存失败");
                        }
                    }
                );
            },
            errorPlacement : setErrorPlacement,
            success : validateSuccess,
            highlight : setHighlight
        });
    })

	function saveGrayRule(){
		$("#grayRule_model").submit();
	}

    function closeModal(){
        $('#modelGrayRule').modal('hide');
    }

    //选择企业
    function choosePartner(l){
        TR.select('partner',{
            type:0//固定参数
        },function(data){

            $(l).val(data.name);
            console.log(data);
            $("input[name='scopeValue']").val(data.id);
            $("input[name='scopeName']").val(data.name);
            $("#partnerId").val(data.id);

        });
    }

    /**
     * 用户单选
     */
    function chooseUser(l){

        var pid = $("#partnerId").val();
        if (!pid) {
            layer.msg("请先选择企业！")
            return;
        }

        TR.select('user', {
            partnerId : pid
        },function(data){

            console.log(data);

            $(l).val(data.fullname);
			$("input[name='scopeValue']").val(data.id);
			$("input[name='scopeName']").val(data.fullname);
        });
    }

    $('input:radio[name="scopeType"]').change( function(){
        select_user_partner($(this).val());
	})

	function select_user_partner(scopeType) {

        if(scopeType == 'USER'){

            $("#select_all_div").show();
            $("#select_user_div").show();
            $("#select_partner_div").hide();

        }else if(scopeType == 'PARTNER'){

            $("#select_all_div").show();
            $("#select_partner_div").show();
            $("#select_user_div").hide();
        }else {

            $("#select_all_div").hide();
        }
    }


</script>