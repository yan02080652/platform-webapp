<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<link rel="stylesheet" href="resource/css/bootstrap/fileinput.min.css?version=${globalVersion}">

<div class="container-fluid">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12">
		        <div class="form-inline">
	           		<div class="glyphicon glyphicon-home" aria-hidden="true"></div>菜单维护   &nbsp;&nbsp;
	           		<div class="form-group" style="margin-left: 30px;">
	             		<label for="sysId" class="control-label" >系统</label>
			        	<select class="form-control"  id="menuSysId" name="sysId" style="width: 150px" onchange="changeSysId()">
			        	 	<c:forEach items="${sysList}" var="sys">
								<option value="${sys.itemCode}">${sys.itemTxt}</opton>
							</c:forEach>
						</select>
						&nbsp;&nbsp;&nbsp;&nbsp;<label for="sceneId" class="control-label" >场景</label>
			        	<select class="form-control"  id="menuSceneId" name="sceneId" style="width: 150px" onchange="changeSceneId()">
			        	 	<c:forEach items="${sceneList}" var="scene">
								<option value="${scene.itemCode}">${scene.itemTxt}</opton>
							</c:forEach>
						</select>
				    </div>
				</div>
			</div>
			<hr/>
			<div>
				<div class="panel-body" style="padding-top: 0px;padding-bottom: 5px">
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addRootMenu();">添加顶级</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="addNodeMenu();">添加下级</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="editNodeMenu();">编辑</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="deleteNodeMenu();">删除</a>
					<a class="btn btn-default" href="javascript:void(0)" role="button" onclick="moveNodeMenu();">移动</a>
					<!-- <a class="btn btn-default" href="javascript:void(0)" role="button" onclick="moveRootMenu();">移至根菜单</a> -->
				</div>
				<div class="col-md-4">
					<div class="panel panel-default">
		             	<div class="panel-body">
							<div id="treeMenu" class=""></div>
		            	</div>
		        	</div>
				</div>
				<div id="rightMenuDetail" class="col-md-8 animated fadeInRight">
				</div>
			</div>
		</div>
	</div>
</div>
<div id="menuTreeDiv"></div>
<div id="transTreeDiv"></div>
<script type="text/javascript" src="webpage/platform/menu/menu.js?version=${globalVersion}"></script>
<!-- <script type="text/javascript" src="resource/js/bootstrap/fileinput.min.js"></script> -->

<script type="text/javascript">
$(document).ready(function(){
	$("#menuSysId").val("${sysId}");
	$("#menuSceneId").val("${sceneId}");
});

</script>