var curMenuNode;//左边菜单树当前节点
var canAdd = true;
var curMoveTreeNode;//当前选择的移动树的当前节点
var curTransTreeNode;//当前选择的作业树的当前节点

require(['treeview'],function(){//加载bootstrap treeview的资源 以及初始化树的数据
	initMenu();
});


function initMenu(){
	loadMenuTree();
}

function changeSysId(){
	var sysId = $("#menuSysId").val();
	window.location.href="/platform/menuconfig?sysId=" + sysId;
	//reloadPage();
}

function changeSceneId(){
	reloadPage();
}

function loadMenuTree(){
	var sysId = $("#menuSysId").val();
	var sceneId = $("#menuSceneId").val();
	$.getJSON('platform/menuconfig/menu?sysId='+sysId+'&sceneId='+sceneId,function(data){
		var treeMenuData = TR.turnToTreeViewData(data,{text:"nodeName"});
		$('#treeMenu').treeview({
			data: treeMenuData,
			showBorder:false,
			highlightSelected:true,
			//selectedBackColor:'#808080',
			onNodeSelected: function(event, data) {
				if(!canAdd){
					$('#treeMenu').treeview('unselectNode', [ data.nodeId, { silent: true } ]);//取消当前选择
					if(curMenuNode != null){
						$('#treeMenu').treeview('selectNode', [ curMenuNode.nodeId, { silent: true } ]);//不能编辑时还是选择上一个节点
					}
					$.alert("请编辑完当前节点!","提示");
					return;
				}else{
					//刷新右侧配置页面
					curMenuNode = data;
					reloadRightReadOnly(data.id,data.pid);
				}
			   
			},
			onNodeUnselected: function(event, data) {
				//刷新右侧配置页面
				if(!canAdd){
					
				}else{
					curMenuNode = null;
					$("#rightMenuDetail").hide();
				}
			}
		});
	});
}

function reloadPage(){
	loadMenuTree();
	$("#rightMenuDetail").hide();
}

function reloadRight(id,pid) {
	if(!id){
		id = 0;
	}
	if(!pid){
		pid = 0;
	}
	$("#rightMenuDetail").show();
	var path = 'platform/menuconfig/menuDetail?id='+id+'&pid='+pid;
	$("#rightMenuDetail").load(path,function(){
		$('#formMenuDetail').find('input,textarea,select,button').attr('disabled',false);
		//$('.file-loading').attr('disabled',false);
		$('#formMenuDetail').find('input').attr('readonly',false);
		
		 var type =$("#select-menuType").val();
		 if(type ==1){
			$("#selectTransButton").show(); 
		 }else{
			$("#selectTransButton").hide();
		 }
		initForm();	 
	});
}

function reloadRightReadOnly(id,pid) {
	if(!id){
		id = 0;
	}
	if(!pid){
		pid = 0;
	}
	$("#rightMenuDetail").show();
	var path = 'platform/menuconfig/menuDetail?id='+id+'&pid='+pid;
	$("#rightMenuDetail").load(path,function(){
		 var type =$("#select-menuType").val();
		 if(type ==1){
			$("#selectTransButton").show(); 
		 }else{
			$("#selectTransButton").hide();
		 }
		initForm();	 
	});
}

function addRootMenu(e) {
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	//添加顶级节点时 取消当前选中
	if(curMenuNode!=null){
		$('#treeMenu').treeview('unselectNode', [curMenuNode.nodeId, { silent: true }]);
	}
	curMenuNode = null;
	reloadRight(0,0);//0,0
	canAdd = false;
};

function addNodeMenu(){
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	if(curMenuNode==null || curMenuNode.data.type==0 || curMenuNode.data.type==1){
		$.alert("请选择一个目录节点!","提示");
		return;
	}
	reloadRight(0,curMenuNode.id);
	canAdd = false;
}

//修改节点信息
function editNodeMenu() {
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	if(curMenuNode==null){
		$.alert("请选择一个目录节点!","提示");
		return;
	}
	$('#formMenuDetail').find('input,textarea,select,button').attr('disabled',false);
	//$('.file-loading').attr('disabled',false);
	$('#formMenuDetail').find('input').attr('readonly',false);
	
	var type =curMenuNode.data.type;
	 if(type ==1){//作业
		$("#selectTransButton").show(); 
	 }else{
		 $("#selectTransButton").hide();
	 }
	 canAdd = false;

};


//删除节点
function deleteNodeMenu() {
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	if(curMenuNode==null){
		$.alert("请选择一个目录节点!","提示");
		return;
	}
	
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除当前节点?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/menuconfig/deleteMenuNode",
				data : {
					id : curMenuNode.id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadPage();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
	//zTree.removeNode(treeNode, true);
};


function cancel(){
	if(curMenuNode!=null && curMenuNode.id!=null){//说明是编辑的 重新点击该节点就可以了
		reloadRightReadOnly(curMenuNode.id,curMenuNode.pid);
	}else{

		$("#rightMenuDetail").hide();
	}
	canAdd = true;
}

function selectMenu() {
	showModal("platform/menuconfig/selectMenu/", function() {
		$('#modelMenuNode').modal();
		var sysId = $("#menuSysId").val();
		var sceneId = $("#menuSceneId").val();
		$.getJSON('platform/menuconfig/menu?sysId='+sysId+'&sceneId='+sceneId,function(data){
			var treeMenuData = TR.turnToTreeViewData(data);
			$('#selectMenuTree').treeview({
				data: treeMenuData,
				showBorder:false,
				highlightSelected:true,
				//selectedBackColor:'#808080',
				onNodeSelected: function(event, data) {
					curMoveTreeNode = data;
				},
				onNodeUnselected: function(event, data) {
					curMoveTreeNode = null;
				}
			});
		});
	});
}

function showModal(url, callback) {
	$('#menuTreeDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function moveNodeMenu(){
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	if(curMenuNode==null){
		$.alert("请选择一个节点!","提示");
		return;
	}
	selectMenu();
	
}

function moveRootMenu(){
	if(!canAdd){
		$.alert("请编辑完当前节点!","提示");
		return;
	}
	if(curMenuNode==null){
		$.alert("请选择一个节点!","提示");
		return;
	}
	if(curMenuNode.pid == null){
		$.alert("该菜单已是根菜单!","提示");
		return;
	}
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认移动至根菜单?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/menuconfig/moveMenu",
				data : {
					fromId : curMenuNode.id,
					toId : null
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadPage();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
	
}

function selectMenuNode(){
	if(curMoveTreeNode == null ||curMoveTreeNode.data.type==0 || curMoveTreeNode.data.type==1){
		$.alert("请选择一个目录节点!","提示");
		return;
	}
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认移动当前节点?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/menuconfig/moveMenu",
				data : {
					fromId : curMenuNode.id,
					toId : curMoveTreeNode.id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						$('#modelMenuNode').modal('hide');
						reloadPage();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
	
}

function initForm() {
	$("#formMenuDetail").validate({
		ignore: '.ignore',
		rules : {
			code :{
				remote : {
					url : "platform/menuconfig/checkCode",
					type : "post",
					data : {
						id : function() {
							return $("#menu-id").val();
						},
						code : function() {
							return $("#menuCode").val();
						}
					},
					dataFilter : function(data) {
						var resultMes = eval("("+data+")");
						if (resultMes.type == 'success') {
							return true;
						} else {
							return false;
						}
					}
				}
			},
			name : {
				required : true,
				minlength : 1,
				maxlength : 30
			},
			type : {
				type : true,	
			},
			openMode : {
				type : true,	
			}
		} ,
		messages : {  
			code : {  
	            remote: "编码已存在！"
	          }
	    },
		submitHandler : function(form) {
			/*$.ajax({
				cache : true,
				type : "POST",
				url : "platform/menuconfig/saveOrupdateMenu",
				data : $('#formMenuDetail').serialize()+"&sysId="+$("#menuSysId").val()+"&sceneId="+$("#menuSceneId").val(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						canAdd = true;
						reloadPage();
					} else {
						showErrorMsg(data.txt);
					}
				}
			});*/
			
			$('#formMenuDetail').ajaxSubmit({success:function(data){
       		  if (data.type == 'success') {
       			canAdd = true;
				reloadPage();
			  } else {
				showErrorMsg(data.txt);
			  }
       		 
       	 }});
			
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

function submitMenu() {
	$("#menuDetailSysId").val($("#menuSysId").val());
	$("#menuDetailSceneId").val($("#menuSceneId").val());
	$("#formMenuDetail").submit();
}

//选择菜单类型后事件
function onSelectMenuType(){
	var type = $("#select-menuType").val();
	if(type == 2){//目录
		$("#menu-def").attr('disabled',false);
		$("#selectTransButton").hide();
		$("#menu-defcode").val("");
		$("#menu-def").val("");
	}else if(type == 0){//菜单
		$("#menu-def").attr('disabled',false);
		$("#selectTransButton").hide();
		$("#menu-defcode").val("");
		$("#menu-def").val("");
	}else if(type == 1){//作业
		$("#menu-def").attr('disabled',true);
		$("#selectTransButton").show();
		$("#menu-defcode").val("");
		$("#menu-def").val("");
		
	}
}

function openTransDia() {
	showModalTrans("platform/menuconfig/selectTrans", function() {
		$('#modelTransNode').modal();
		$.ajax({
			type : 'GET',
			url : "platform/trans/showMenu",
			success : function(data) {
				var treeMenuData = TR.turnToTreeViewData(data);
				$('#selectTransTree').treeview({
					data: treeMenuData,
					showBorder:false,
					highlightSelected:true,
					//selectedBackColor:'#808080',
					onNodeSelected: function(event, data) {
						curTransTreeNode = data;
					},
					onNodeUnselected: function(event, data) {
						curTransTreeNode = null;
					}
				});
			}
		});
	});
}

function showModalTrans(url, callback) {
	$('#transTreeDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function selectTranNode(){
	if(curTransTreeNode == null || curTransTreeNode.data.nodeType!=0){
		$.alert("请先选择一个作业节点!","提示");
		return;
	}
	$('#modelTransNode').modal('hide');
	
	$('#menu-def').val(curTransTreeNode.data.name+"("+curTransTreeNode.data.code+")");
	$('#menu-defcode').val(curTransTreeNode.data.code);
	
}

function changeNodeName(){
	/*nodes = zTreeObj.getSelectedNodes();
	nodes[0].name = $('#menuName').val();
	zTreeObj.updateNode(nodes[0]);*/
}




