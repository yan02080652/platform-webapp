<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="modal-body">
			<form modelAttribute="menuDto" action="platform/menuconfig/saveOrupdateMenu" method="post" class="form-horizontal" draggable="false" id="formMenuDetail" role="form"  enctype="multipart/form-data">
            	<input type="hidden" id="menu-id" name="id" value="${menuDto.id}"/>
            	<input type="hidden" name="pid" value="${pid}"/>
            	<input type="hidden" id="menuDetailSysId" name="sysId" />
            	<input type="hidden" id="menuDetailSceneId" name="sceneId"/>
				<div class="form-group">
			      <label for="name" class="col-sm-3 control-label">菜单文本</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" readonly="readonly" onblur="changeNodeName()" id="menuName" name="name" value="${menuDto.name}"  placeholder="菜单文本"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="name" class="col-sm-3 control-label">菜单编码</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" readonly="readonly" id="menuCode" name="code" value="${menuDto.code}"  placeholder="菜单编码"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="type" class="col-sm-3 control-label">菜单类型</label>
			      <div class="col-sm-9">
			         <select class="form-control" id="select-menuType" onchange="onSelectMenuType()"  disabled="disabled" name="type">
				          <option value="-1">请选择菜单类型</opton>
				          <option value="0">URL菜单</option>
				          <option value="1">作业</option>
				          <option value="2">目录</option>
					  </select>
			      </div>
			   </div>
			   <div class="form-group">
				    <label for="def" class="col-sm-3 control-label">菜单定义</label>
				    <div class="col-sm-9" style="padding-left: 0px;">
					    <div id="menuDefDiv" class="col-sm-12 input-group" style="padding-left:14px;">
					   	  <input type="hidden" id="menu-defcode" class="form-control" name="defCode" value="${menuDto.def}"/>
					   	  <input type="text" id="menu-def" class="form-control" readonly="readonly" name="def" value="${menuDto.def}" placeholder="菜单定义"/>
	            		  </button>
	            		  <div class="input-group-btn">
					         <button type="button" id="selectTransButton" class="btn btn-default" disabled="disabled" onclick="openTransDia()">选择作业
					       </div>
					    </div>
				    </div>
				</div>
				<div class="form-group">
			      <label for="appendix" class="col-sm-3 control-label">附加参数</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" readonly="readonly" name="appendix" value="${menuDto.appendix}" placeholder="附加参数"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="openMode" class="col-sm-3 control-label">打开方式</label>
			      <div class="col-sm-9">
			         <select class="form-control" id="select-openMode" disabled="disabled" name="openMode">
				          <option value="-1">请选择打开方式</opton>
				          <option value="0">内嵌打开</option>
				          <option value="1">新窗口打开</option>
				          <option value="2">iframe打开</option>
					  </select>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="orgSelect" class="col-sm-3 control-label">关联组织选择</label>
			      <div class="col-sm-9">
			         <select class="form-control" id="select-orgSelect" disabled="disabled" name="orgSelect">
				          <option value="-1">请选择</opton>
				          <option value="0">需要</option>
				          <option value="1">不需要</option>
				 	</select>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="seq" class="col-sm-3 control-label">序号</label>
			      <div class="col-sm-3">
			         <input type="number" class="form-control" readonly="readonly" name="seq" value="${menuDto.seq}" placeholder="序号"/>
			      </div>
			      <label for="status" class="col-sm-2 control-label">状态</label>
				  <div class="col-sm-4">
				     <label class="radio-inline"><input  type="radio" disabled="disabled" value="0" checked="checked" name="status" class="ignore"/>生效</label>
				     <lable class="radio-inline"><input  type="radio" disabled="disabled" value="-1" name="status" class="ignore"/>不生效</lable>
				  </div>
			   </div>
			   <div class="form-group">
					<label for="icon" class="col-sm-3 control-label">图标预览</label> 
					<c:if test="${ not empty menuDto.icon}">
						<div class="col-sm-2">
				         	<img width="40" height="40" alt="" src="http://${SYS_CONFIG['imgserver'] }/${menuDto.icon}">
				        </div>
				    </c:if>
					<div class="col-sm-4">
				         <input type="hidden" class="form-control" name="icon" value="${menuDto.icon}"/>
				         <input type="file" name="iconFile" class="file-loading">
				    </div>
            		<button type="button" class="btn btn-default" disabled="disabled" onclick="deleteIcon('${menuDto.id}')">删除图标 </button>
			   </div>
			   
			   <div class="form-group" align="center">
			   		<button type="button" class="btn btn-default" disabled="disabled" onclick="cancel()">取消
            		</button>&nbsp;&nbsp;
			      	<button type="button" class="btn btn-primary" disabled="disabled" onclick="submitMenu()">保存
           			</button>
			   </div>
			</form>
		</div>
	</div>
</div>
 <script>
 $(document).ready(function() {
	 var menuName = $("#menuName").val();
	 if(menuName){
		 var type ="${menuDto.type}";
		 if(type ==1){//作业
			 $("#menu-defcode").val("${menuDto.def}");
			 $("#menu-def").val("${transDto.name}"+"("+"${menuDto.def})");
		 }
		 
		$("#select-menuType").val("${menuDto.type}");
		$("#select-openMode").val("${menuDto.openMode}");
		$("#select-orgSelect").val("${menuDto.orgSelect}");
		$("input:radio[value='${menuDto.status}']").attr('checked','true');
	 }
	 
	 $(".file-loading").fileinput({
        language: 'zh', //设置语言
       // uploadUrl: uploadUrl, //上传的地址
        allowedFileExtensions : ['jpg', 'png','gif'],//接收的文件后缀
        showUpload: false, //是否显示上传按钮
        showCaption: false,//是否显示标题
        browseClass: "btn btn-primary", //按钮样式             
        previewFileIcon: "<i class='glyphicon glyphicon-king'></i>", 
    });
	
	 $(".fileinput-remove").remove();
	 //$(".file-actions").remove();
	 //$(".file-footer-caption").remove();
	 
 });
 
 function deleteIcon(id){
	 if(!id){
		return;
	 }
	 
	 $.ajax({
		cache : true,
		type : "POST",
		url : "platform/menuconfig/deleteIcon",
		data : {
			id:id
		},
		async : false,
		error : function(request) {
			showErrorMsg("请求失败，请刷新重试");
		},
		success : function(data) {
			if (data.type == 'success') {
				showSuccessMsg(data.txt);
				reloadRight(curMenuNode.id,curMenuNode.pid);
			} else {
				$.alert(data.txt,"提示");
			}
		}
	});
 }
 </script>