<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>区域信息<c:choose><c:when test="${districtDto.id != '' && districtDto.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
					 <%--&nbsp;&nbsp;<a data-pjax onclick="backMain()">返回</a>--%>
             </div>
             <div class="panel-body">
                 <form id="districtForm" class="form-horizontal bv-form">
                 		<input type="hidden" name="id" id="id" value="${districtDto.id }"/>

					 <div class="form-group" id="parentArea">
						 <label for="type" class="col-sm-2 control-label">父级行政区域</label>
						 <div class="col-sm-6">
							 <select class="form-control" id="pId" name="pId">
								 <option value="-1">请选择</option>
								 <c:if test="${not empty parentList }">
									 <c:forEach items="${parentList }" var="district">
										 <option value="${district.id }">${district.nameCn }</option>
									 </c:forEach>
								 </c:if>
							 </select>
						 </div>
					 </div>

					  <div class="form-group">
							<label  class="col-sm-2 control-label">区域级别</label>
							<div class="col-sm-6">
								<select class="form-control" id="disLevel" name="level" onchange="onchangeLevel()">
									<option value="-1">请选择</option>
									  	<c:if test="${not empty levelList }">
								        	  	<c:forEach items="${levelList }" var="level">
								          			<option value="${level.itemCode }">${level.itemTxt }</option>
								         		</c:forEach>
								          	</c:if>
						 	 	</select>
				     	 </div>
				  	 </div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">中文名称</label>
						 <div class="col-sm-6">
							 <input type="text" name="nameCn" class="form-control" value="${districtDto.nameCn }" placeholder="中文名称">
						 </div>
					 </div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">英文名称</label>
						 <div class="col-sm-6">
							 <input type="text" name="nameEn" class="form-control" value="${districtDto.nameEn }" placeholder="英文名称">
						 </div>
					 </div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">拼音首字母</label>
						 <div class="col-sm-6">
							 <input type="text" name="nameSimply" class="form-control" value="${districtDto.nameSimply }" placeholder="拼音首字母">
						 </div>
					 </div>


					 <%--<div class="form-group">--%>
						 <%--<label class="col-sm-2 control-label">区域编码</label>--%>
						 <%--<div class="col-sm-6">--%>
							 <%--<input type="text" name="code" id="code" value="${districtDto.code }" class="form-control" value="" placeholder="区域编码">--%>
						 <%--</div>--%>
					 <%--</div>--%>

					 <div class="form-group">
							<label for="type" class="col-sm-2 control-label">区域类型</label>
							<div class="col-sm-6">
								<select class="form-control" id="type" name="type">
									<option value="-1">请选择</option>
										<c:if test="${not empty typeList }">
								        	  	<c:forEach items="${typeList }" var="type">
								          			<option value="${type.itemCode }">${type.itemTxt }</option>
								         		</c:forEach>
								          	</c:if>
						 	 	</select>
				     	 </div>
				  	 </div>
					  
					  <div class="form-group">
					    <label class="col-sm-2 control-label">排序号</label>
					    <div class="col-sm-6">
					      <input type="number" name="sort" class="form-control" value="${districtDto.sort }" placeholder="排序号">
					    </div>
					  </div>
					  
					<div class="form-group" id="isHotDiv">
						<label class="col-sm-2 control-label">是否热门城市</label>
						<div class="col-sm-6">
							<label class="radio-inline"><input type="radio" name="isHot" value="1" />是</label>
							<label class="radio-inline"><input type="radio" name="isHot" value="0" checked="checked" />否</label> 
						</div>
					</div>
					
					<div class="form-group" id="isDisableDiv">
						<label class="col-sm-2 control-label">状态</label>
						<div class="col-sm-6">
							<label class="radio-inline"><input type="radio" name="isDisable" value="1" />禁用</label>
							<label class="radio-inline"><input type="radio" name="isDisable" value="0" checked="checked"/>不禁用</label> 
						</div>
					</div>
			</form>
				<div class="form-group">
					<div class="col-sm-offset-7">
						<a class="btn btn-default"  onclick="submitDistrict()">提交</a>
					</div>
				</div>
			</div>
         </div> 
	</div>
</div>
<script type="text/javascript">

//提交表单
function submitDistrict(){
	 $('#districtForm').submit();
}

$(document).ready(function() {
	var id = '${districtDto.id}';
	if(id!=''){
		if('${districtDto.isHot}' == 'true'){
			$("#isHotDiv input:radio[value=1]").attr('checked','true');
		}else{
			$("#isHotDiv input:radio[value=0]").attr('checked','true');
		}
		if('${districtDto.isDisable}' == 'true'){
			$("#isDisableDiv input:radio[value=1]").attr('checked','true');
		}else{
			$("#isDisableDiv input:radio[value=0]").attr('checked','true');
		}
		
		$("#disLevel").val('${districtDto.level}').attr("disabled",true);
		$("#type").val('${districtDto.type}');
		$("#pId").val('${districtDto.pId}');
	
		if('${districtDto.level}'=='0'){
			$("#parentArea").attr("disabled",true).attr("hidden",true);
			$("#isHotDiv").attr("disabled",true).attr("hidden",true);
		}else{
			$("#parentArea").attr("disabled",false).attr("hidden",false);
			$("#isHotDiv").attr("disabled",false).attr("hidden",false);
		}
		
	}	
	
	var level = "${parent.level}";
	var pid = "${parent.id}";
	if(level && pid){
		$("#disLevel").val(parseInt(level)+1);
		$("#pId").val(pid);
	}
		
	
	$("#districtForm").validate({
		rules : {
//        	code : {
//                minlength : 1,
//                maxlength:20,
//                remote:{
//                	type:'post',
//                	url:'platform/district/checkCode',
//                	data:{
//                		code:function(){
//                			return $('#code').val();
//                		},
//                		id:function(){
//	                		return $('#id').val();
//	                	},
//                	},
//					dataFilter : function(data) {
//						if (data == 'ok') {
//							return true;
//						} else {
//							return false;
//						}
//					}
//                }
//            },
            nameCn : {
                required : true,
                minlength : 1,
                maxlength:30
            },
        },
//        messages:{
//            code:{
//               remote:"此编码已经存在"
//            }
//        },
        submitHandler : function(form) {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/district/save",
				data : $('#districtForm').serialize(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
//						$("#districtMain").show();
//						$("#districtDetail").hide();
//						reloadTree();
//						reloadRight();
						layer.msg("操作成功");
					} else {
						showErrorMsg(data.txt);
					}
				}
			});    
        },
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight
    });
});
  
</script>