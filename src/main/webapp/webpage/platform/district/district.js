var currentNode;//左边树当前节点
var levelArray={};//行政区级别数组
var typeArray={};//行政区类型数据
//var distinctId;//当前选择的节点id

require(['treeview'],function(){//加载bootstrap treeview的资源 以及初始化树的数据
	initMenu();
});

function initMenu(){
	reloadTree();
	initLevelArray();
	initTypeArray();
}

function initLevelArray(){
	$.post("platform/district/getLevelList",function(data){
		$(data).each(function(){
			levelArray[this.itemCode] = this.itemTxt;
		})
	});
	
}

function initTypeArray(){
	$.post("platform/district/loadTypeList",function(data){
		$(data).each(function(){
			typeArray[this.itemCode] = this.itemTxt;
		})
	});
	
}

//加载树
function reloadTree() {
	$.ajax({
		type : 'GET',
		url : "platform/district/getTree",
		success : function(data) {
			$('#district_tree').treeview({
				data: TR.turnToTreeViewData(data,{'id':'id','pid':'pId','namePro':'nameCn'}),
				showBorder:false,
				highlightSelected:true,
				levels: 1,
				onNodeSelected: function(event, data) {
					currentNode = data;
					reloadRight(data.id);
				}, 
				onNodeUnselected: function(event, data) {
					currentNode= null;
				}
			});
		}
	});
	reloadRight();
	
}

//加载右侧面板
function reloadRight(districtId,pageIndex){
	var level = $("#level").val();
	var name = $("#name").val();
	if(currentNode){
		districtId = currentNode.id;
	}
	$.post('platform/district/list',{
		name:name,
		level:level,
		districtId:districtId,
		pageIndex:pageIndex
		},function(data){
		$("#district_Table").html(null);
		$("#district_Table").html(data);
		
		$.each($('.districtLevel'),function(i,dom){
			var txt = $(dom).text();
			if(txt){
				$(dom).text(levelArray[txt]);
			}
		});
		
		$.each($('.districtType'),function(i,dom){
			var txt = $(dom).text();
			if(txt){
				var rs = txt == '-1'?'--':typeArray[txt];
				$(dom).text(rs);
			}
		});
	});
}

//查找用户
function search_district(){
	if(currentNode){
		$('#district_tree').treeview('toggleNodeSelected',currentNode.nodeId);
		currentNode =null;
	}
	reloadRight();
}

//重置
function reset_search(){
	$("#name").val("");
	$("#level").val("");
	if(currentNode){
		$('#district_tree').treeview('toggleNodeSelected',currentNode.nodeId);
		currentNode =null;
	}
	reloadRight();
}

//转到添加或修改页面
function getDistrict(id){

	var parentId = '';
	if(currentNode){
		parentId  = currentNode.id
	}

	if(!id && !parentId){
		layer.alert("请先选择左侧行政区域");
		return;
	}

	window.open('platform/district/getDistrict?id=' + id + '&parentId=' + parentId);
}

//检查是否可以删除
function checkDeleteAction(ids,callback){
	$.post("platform/district/checkDeleteAction",{ids:ids},function(data){
		callback(data);
	});
}

//批量删除
function batchDelete(){
    //判断至少选了一项
    var checkedNum = $("#DistrictTable input[name='ids']:checked").length;
    if(checkedNum==0){
        $.alert("请选择你要删除的记录!","提示");
        return false;
    }
    
    var ids = new Array();
    $("#DistrictTable input[name='ids']:checked").each(function(){
    	ids.push($(this).val());
    });
    
   checkDeleteAction(ids.toString(),function(rs){
	   if(!rs){
		   $.alert("删除列表中有存在下级区域的记录，不能直接删除，请先删除其所有下级区域!","提示");
	       return false; 
	   }
	   deleteDistrict(ids.toString());
   });
}

//单个删除
function deleted(id) {
	checkDeleteAction(id,function(rs){
		if(!rs){
			   $.alert("该记录存在下级区域，不能直接删除，请先删除其所有下级区域!","提示");
		       return false; 
		   }
		deleteDistrict(id);
	});
};

//确认删除
function deleteDistrict(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除所选记录?",
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "platform/district/delete",
				data : {
					id : id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadRight();
						reloadTree();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

//checkbox全选
function selectAll(){
	if ($("#selectAll").is(":checked")) {
        $(":checkbox").prop("checked", true);//所有选择框都选中
    } else {
        $(":checkbox").prop("checked", false);
    }
}
//返回
function backMain(){
	$("#districtMain").show();
	$("#districtDetail").hide().html("");
}

//区域类型下拉框改变时触发
//隐藏/显示对应的字段并添加/删除相应验证规则
function onchangeLevel(){
	var level = $("#disLevel").val();
	if(level!=-1){
		if(level == 0){//国际区域
			$("#parentArea").attr("disabled",true).attr("hidden",true);
			$("#isHotDiv").attr("disabled",true).attr("hidden",true);
			loadTypeList(level);
		}else{//国家/省份/城市/区县
			$("#parentArea").attr("disabled",false).attr("hidden",false);
			$("#isHotDiv").attr("disabled",false).attr("hidden",false);
			$("#pId").rules("add",{type:true});
			loadTypeList(level);
			loadDistrictList(level);
		}
	}
}

//加载区域类型字段下拉列表
function loadTypeList(level,callback){
	 $.ajax({
		 type:"post",
		 url:"platform/district/loadTypeList",
		 data:{'cust':level},
		 success:function(data){
			$("#type option:gt(0)").remove();
			 $(data).each(function(){
				 var $option = $("<option value='"+this.itemCode+"'>"+this.itemTxt+"</option>");
					$("#type").append($option);
			 });
			 
			 if(callback){
				 callback();
			 }
			 
		 }
	 });
}

//加载对应区域级别上一级的区域集合
function loadDistrictList(level,callback){
	 $.ajax({
		 type:"post",
		 url:"platform/district/getDistrictList",
		 data:{'level':level},
		 success:function(data){
			$("#pId option:gt(0)").remove();
			 $(data).each(function(){
				 var $option = $("<option value='"+this.id+"'>"+this.nameCn+"</option>");
					$("#pId").append($option);
			 })
			 if(callback){
				 callback();
			 }
		 }
	 });
}

//分页调用的方法
function reloadList(pageIndex){
	reloadRight(null,pageIndex);
}

