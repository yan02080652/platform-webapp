<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default"> 
             <div class="panel-body">
				<div class="row">
					<div class="col-md-12"> 
						<form class="form-inline" method="post" >
							<nav class="text-right">
								<div class="pull-left">
								<a class="btn btn-default" onclick="getDistrict('')" role="button">添加区域</a>
								<a class="btn btn-default" onclick="batchDelete()" role="button">批量删除</a>
								</div>
								 <div class="form-group">
								    <label class="control-label">区域级别：</label>
								     <select class="form-control" id="level" name="level" style="min-width: 100px">
								          <option value="">全部</option>
								          	<c:if test="${not empty levelList }">
								        	  	<c:forEach items="${levelList }" var="level">
								          			<option value="${level.itemCode }">${level.itemTxt }</option>
								         		</c:forEach>
								          	</c:if>
								     </select>
								  </div>
								<div class=" form-group">
								 <label class="control-label">区域名称：</label>
									<input name="name" id="name" type="text" value="${name }" class="form-control" placeholder="中文名称/英文名称/拼音简称" />
								</div>
								<div class="form-group">
									<button type="button" class="btn btn-default" onclick="search_district()">查询</button>
									<button type="button" class="btn btn-default" onclick="reset_search()">重置</button>
								</div>
							</nav>
						</form>
					</div>
				</div>
				<div class="table-responsive" id="district_Table" style="margin-top: 15px;">

                 </div>
             </div>
         </div>
	</div>
</div>