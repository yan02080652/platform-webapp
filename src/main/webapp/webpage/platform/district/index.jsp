<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="districtMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>行政区域管理   &nbsp;&nbsp;
			</div>
			<div>
				<div class="col-md-3">
					<div class="panel panel-default">
		             	<div class="panel-body">
							<ul id="district_tree" style="padding-left: 10px;"></ul>
		            	</div>
		        	</div>
				</div>
				<div id="rightDetail" class="col-md-9 animated fadeInRight"> 
				     <jsp:include page="districtManager.jsp"></jsp:include>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 详细页面 -->
<div id="districtDetail" style="display:none;">
</div>
<script type="text/javascript" src="webpage/platform/district/district.js?version=${globalVersion}"></script>