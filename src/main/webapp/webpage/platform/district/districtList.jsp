<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 

<table class="table table-striped table-bordered table-hover" id="DistrictTable">
	<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectAll()" id="selectAll"/></th>
          <th><label>行政区级别 </label></th>
          <th><label>编码</label></th>
	      <th><label>中文名称 </label></th>
          <th><label>英文名称</label></th>
          <th><label>行政区类型 </label></th>
          <th><label>是否热门城市</label></th>
          <th><label>是否禁用</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="district" varStatus="vs">
         <tr class="odd gradeX">
        	 <td><input type="checkbox" name="ids" value="${district.id }"/></td>
             <td class="districtLevel">${district.level }</td>
             <td>${district.code }</td>
        	 <td>${district.nameCn }</td>
             <td>${district.nameEn }</td>
             <td class="districtType">${district.type }</td>
             <td>
             	<c:choose>
             		<c:when test="${district.isHot }">热门</c:when>
             		<c:otherwise>非热门</c:otherwise>
             	</c:choose>
             </td>
              <td>
             	<c:choose>
             		<c:when test="${district.isDisable }">禁用</c:when>
             		<c:otherwise>启用</c:otherwise>
             	</c:choose>
             </td>
             <td class="text-center">
             	<a onclick="getDistrict('${district.id }')" >修改</a>&nbsp;&nbsp;
             	<a onclick="deleted('${district.id }')">删除</a>
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadList"></jsp:include>
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>