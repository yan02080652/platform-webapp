/**
 * 保险产品库配置
 */
var pageVal = 
{
	
}

$(function(){
	pageVal.getCfgDicTreeTable(-1);
})

$("#tabs-333708 ul li").on('click',function(){
	var partnerId = $(this).attr("data-value");

	pageVal.getCfgDicTreeTable(partnerId);
})

/**
 * 加载字典页面
 * @param sysId
 * @param cfgnodeCode
 */
pageVal.getCfgDicTreeTable = function (partnerId){

    if(partnerId == -1){
        partnerId = $("#queryBpId").val();
    }

	$.ajax({
		type : 'GET',
		url : "product/config/list.json",
		data :{ partnerId1 : partnerId},
		success : function(data) {
			var treeData = TR.turnToZtreeData(data,{pid : 'parentId'});
			var html = "";
			
 			var ids = {};
			$.each(data,function(i,d){
				ids[d[id]] = true;
			});
			
			html = addChild(treeData,html);
			$("#treeTableDetail").html(html);
			
			function addChild(data) {
				
				$.each(data, function(i, d) {
					
					if(!d.pid || !ids[d.pid]){
						
						html += "<tr data-tt-id='"+d.id+"'>";
					}else{
						
						html += "<tr data-tt-id='"+d.id+"' data-tt-parent-id='"+d.pid+"'>";
					}					
					var productName = "";
					var productPrice = "";
					var available = "";
					var forcedBuy = "";
					var companyPay = "";
					var defaultChecked = "";
					
					$.each(d.data.productDto, function(i, d){
						if(d.name){
							productName += "<p class='pName'>" + d.name + "</p>";
						}
						if(d.salePrice){
							productPrice += "<p class='pName'>" + d.salePrice + "</p>";
						} else {
                            productPrice += "<p class='pName'>" + 0.0 + "</p>";
						}
						if(d.productConfig)
						{

							if(d.productConfig.available == 1)
							{
								available += "<p><input type='checkbox' onclick='available(this);' value='"+d.id+"' checked='checked'></p>";
							}
							else
							{
								available += "<p><input type='checkbox' onclick='available(this);' value='"+d.id+"' ></p>";
							}
							if(d.productConfig.companyPay == 1)
							{
								companyPay += "<p><input type='checkbox' onclick='companyPay(this);' value='"+d.id+"' checked='checked'></p>";
							}
							else
							{
								companyPay +=  "<p><input type='checkbox' onclick='companyPay(this);' value='"+d.id+"'></p>";
							}
							if(d.productConfig.forcedBuy == 1)
							{
								forcedBuy +=  "<p><input type='checkbox' onclick='forcedBuy(this);' value='"+d.id+"' checked='checked'></p>";
							}
							else
							{
								forcedBuy += "<p><input type='checkbox' onclick='forcedBuy(this);' value='"+d.id+"'></p>";
							}
                            if(d.productConfig.defaultChecked == 1)
                            {
                                defaultChecked +=  "<p><input type='checkbox' onclick='defaultChecked1(this);' value='"+d.id+"' checked='checked'></p>";
                            }
                            else
                            {
                                defaultChecked += "<p><input type='checkbox' onclick='defaultChecked1(this);' value='"+d.id+"'></p>";
                            }
						}
						else
						{
							available +=  "<p><input type='checkbox' value='"+d.id+"'  onclick='available(this);'></p>";
							companyPay +=  "<p><input type='checkbox' value='"+d.id+"' onclick='companyPay(this);' ></p>";
							forcedBuy +=  "<p><input type='checkbox' value='"+d.id+"' onclick='forcedBuy(this);' ></p>";
                            defaultChecked +=  "<p><input type='checkbox' value='"+d.id+"' onclick='defaultChecked1(this);' ></p>";
						}
		 
					})
					
					if(d.children){//存在子节点
						html += "<td><span class='folder'>"+d.data.name+"</span></td><td>"+productName+"</td>";
						
						html += "<td>"+productPrice+"</td><td>"+available+"</td><td>"+forcedBuy+"</td>";
						
						html += "<td>"+companyPay+"</td>";

                        html += "<td>"+defaultChecked+"</td>";
						
						html += "</tr>";
						addChild(d.children,html);
						
					}else{
						html += "<td><span class='file'>"+d.data.name+"</span></td><td>"+productName+"</td>";
						
						html += "<td>"+productPrice+"</td><td>"+available+"</td><td>"+forcedBuy+"</td>";
						
						html += "<td>"+companyPay+"</td>";

                        html += "<td>"+defaultChecked+"</td>";
						
						html += "</tr>";
					}
				});
				return html;
			}

			var option = {
				expandable: true,
			    expandLevel : 4,
			    indent : 30
			};

			$("#treeTableSyscfgDic").treetable(option);
			$('#treeTableSyscfgDic').treetable('expandAll');

			//Highlight selected row
			$("#treeTableSyscfgDic tbody tr").mousedown(function() {
				$("#treeTableSyscfgDic tr.selected").removeClass("selected");
				$(this).addClass("selected");
				
				currentNode = $(this).attr("data-tt-id");
				
				if($(this).hasClass("notSetCostCent")){
					$('#costCenter').prop('checked',true);
					$('.addCenter').addClass("addCostCent");
				}else {
					$('#costCenter').prop('checked',false);
					$('.addCenter').removeClass("addCostCent");
				}
			});

			$("#treeTableSyscfgDic.folder").each(function() {
				$(this).parents("tr").droppable({
					 accept: ".file, .folder",
					 drop: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   $("#treeTableSyscfgDic").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
					 },
					 hoverClass: "accept",
					 over: function(e, ui) {
					   var droppedEl = ui.draggable.parents("tr");
					   if(this != droppedEl[0] && !$(this).is(".expanded")) {
					     $("#treeTableSyscfgDic").treetable("expandNode", $(this).data("ttId"));
					   }
					 }
				});
			});
		}
	});
}

function available(id){	
	var productId = $(id).val();
	var ischecked = id.checked;
	var type = "available";

    var partnerId = $(".nav-tabs .active").attr("data-value");
    if(partnerId == -1){
        partnerId = $("#queryBpId").val();
    }
	
	$.post('product/config/check',{
		productId : productId,
		ischecked : ischecked,
		type : type,
		partnerId1 : partnerId
	},function(){
		layer.msg("保存成功");
	})
}

function companyPay(id){
	var productId = $(id).val();
	var ischecked = id.checked;
	var type = "companyPay";

    var partnerId = $(".nav-tabs .active").attr("data-value");
    if(partnerId == -1){
        partnerId = $("#queryBpId").val();
    }
	
	$.post('product/config/check',{
		productId : productId,
		ischecked : ischecked,
		type : type,
		partnerId1 : partnerId
	},function(){
		layer.msg("保存成功");
	})
	
}

function forcedBuy(id){
	var productId = $(id).val();
	var ischecked = id.checked;
	var type = "forcedBuy";

    var partnerId = $(".nav-tabs .active").attr("data-value");
    if(partnerId == -1){
        partnerId = $("#queryBpId").val();
    }
	
	$.post('product/config/check',{
		productId : productId,
		ischecked : ischecked,
		type : type,
		partnerId1 : partnerId
	},function(){
		layer.msg("保存成功");
	})
	
}

function defaultChecked1(id){

    var productId = $(id).val();
    var ischecked = id.checked;
    var type = "defaultChecked";

    var partnerId = $(".nav-tabs .active").attr("data-value");
    if(partnerId == -1){
        partnerId = $("#queryBpId").val();
    }

    $.post('product/config/check',{
        productId : productId,
        ischecked : ischecked,
        type : type,
        partnerId1 : partnerId
    },function(){
        layer.msg("保存成功");
    })

}

function queryList() {
    var partnerId = $(".nav-tabs .active").attr("data-value");
    if(partnerId == -1){
        partnerId = $("#queryBpId").val();
	}

    pageVal.getCfgDicTreeTable(partnerId);
}