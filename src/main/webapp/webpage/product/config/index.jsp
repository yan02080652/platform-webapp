<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<link href="resource/plugins/jqueryTreeTable/stylesheets/jquery.treetable.theme.default.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<script src="resource/plugins/jqueryTreeTable/javascripts/src/jquery.treetable.js?version=${globalVersion}" type="text/javascript"></script>
<link href="webpage/product/config/product_config.css?version=${globalVersion}" rel="stylesheet" type="text/css" />
<style>
	.pName {
		margin: 3px 0 12px;
	}
</style>


<!-- 商品列表页 -->
<div class="panel-body">
	<div class="row">
		<div class="col-md-12">
           <div class="form-inline">
           		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>保险配置   &nbsp;&nbsp;

			    <div class="selectHeader form-group">
				   <jsp:include page="../../common/businessSelecter.jsp"></jsp:include>
			    </div>

			    <div class="form-group">
				     <button type="button" class="btn btn-default" onclick="queryList()">查询</button>
			    </div>
			</div>
		</div>
		<hr/>

		  <div class="tabbable" id="tabs-333708">
				<ul class="nav nav-tabs">
					<li class="active" data-value = "-1">
						<a href="#panel-500917" data-toggle="tab">企业配置</a>
					</li>
					<c:if test="${superAdmin}">
						<li  data-value = "0">
							<a href="#panel-195047" data-toggle="tab">系统配置</a>
						</li>
					</c:if>
				</ul>
				<div class="tab-content">
					
					<table id="treeTableSyscfgDic" class="table table-bordered" style="width:100%">
						<thead>
				          <tr>
				            <th>保险品类</th>
				            <th>商品名称</th>
				            <th>价格</th>
				            <th>是否可用</th>
				            <th>是否强制购买</th>
				            <th>是否纳入公司费用</th>
							 <th>是否默认勾选</th>
				          </tr>
				        </thead>
				        <tbody id="treeTableDetail">
				        
				        </tbody>
					</table>					
					
				</div>
			</div>
		
	</div>
</div>

<script type="text/javascript" src="webpage/product/config/product_config.js?version=${globalVersion}"></script>