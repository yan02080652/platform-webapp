var curHubsCode = null;//当前选中的hubs

$(function(){
	
})

//添加
function addHubs() {
	showHubsModal("product/hubs/detail", function() {
		$('#mode_hubs').modal();
		initForm();
	});
};

//显示Model
function showHubsModal(url, data, _callback) {
	$('#modelHubsDiv').load(url, data, function(context, state) {
		if ('success' == state && _callback) {
			_callback();
		}
	});
}

function selectHubs(code){
	$("#hubsListTable tbody tr").removeClass("active");
	$("#hubsListTr"+code).addClass("active");
	curHubsCode = code;
	
	reloadRight(code);
	//加载右侧数据
}

//加载右侧面板
function reloadRight(code) {
	var path = 'product/hubs/right?hubsCode='+code;
	$("#rightDetail").load(path);
}

function submitHubs() {
	$("#detailForm").submit();
}

//修改
function editHubs() {
	if(curHubsCode == null){
		$.alert("请选择一行数据!","提示");
		return;
	}
	showHubsModal("product/hubs/detail?hubsCode="+curHubsCode, function() {
		$('#mode_hubs').modal();
		initForm();
	});
};

//删除
function deleteHubs() {
	if(curHubsCode == null){
		$.alert("请选择一行数据!","提示");
		return;
	}
	
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认删除当前连接器?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "product/hubs/delete",
				data : {
					hubsCode : curHubsCode
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						window.location.reload();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
			
		}
	});
};

function initForm() {
	$("#detailForm").validate(
	{
		rules : {
			hubsName : {
				required : true,
				minlength : 1,
				maxlength : 20,
			},
			hubsCode : {
				required : true,
				minlength : 1,
				maxlength : 20,
	           	remote:{
	              	url:"/product/hubs/checkCode",
	              	data:{
	              		hubsCode:function(){
	              			return $('#hubsCode').val();
	              		},
	              		flag:function(){
	              			return $('input[name=flag]').val();
	              		}
	          		}
	          	}  
			},
		},
		messages : {
			hubsCode: {
                remote: "请输入正确编码"
            }
		},
		submitHandler : function(form) {
			$.ajax({
				cache : true,
				type : "POST",
				url : "product/hubs/save",
				data : $('#detailForm').serialize(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
					$('#mode_hubs').modal('hide');
				},
				success : function(data) {
					$('#mode_hubs').modal('hide');
					if (data.type == 'success') {
						showSuccessMsg("操作成功");
						window.location.reload();
					} else {
						showErrorMsg(data.txt);
					}
				}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}