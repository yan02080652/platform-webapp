<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="mode_hubs" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <c:choose>
            	<c:when test="${ empty hubsDto.hubsCode}">
            		<h4 class="modal-title">新增连接器</h4>
            	</c:when>
            	<c:otherwise>
            		<h4 class="modal-title">修改连接器</h4>
            	</c:otherwise>
            </c:choose>
         </div>
         <div class="modal-body">
         <form modelAttribute="hubsDto" class="form-horizontal" id="detailForm" role="form">
               <c:if test="${empty hubsDto.hubsCode}">
         	   		<input type="hidden" name="flag" value="1"/>
         	   </c:if>
               <c:if test="${not empty hubsDto.hubsCode}">
         	   		<input type="hidden" name="flag" value="0"/>
         	   </c:if>         	   
			   <div class="form-group">
		     	  <label for="hubsCode" class="col-sm-4 control-label">连接器编码</label>
		     	  <div class="col-sm-8">
		         	 <input type="text" class="form-control" id="hubsCode" name="hubsCode" value="${hubsDto.hubsCode}" placeholder="编码"/>
		     	  </div>
		  	   </div>
			   <div class="form-group">
			      <label class="col-sm-4 control-label">连接器名称</label>
			      <div class="col-sm-8">
			         <input type="text" class="form-control" name="hubsName" value="${hubsDto.hubsName}" placeholder="名称"/>
			      </div>
			   </div>

				 <div class="form-group">
					 <label class="col-sm-4 control-label">出票渠道</label>
					 <div class="col-sm-8">
						 <select name="issueChannelId" class="form-control">
							 <option value="">-请选择-</option>
							 <c:forEach items="${issueChannels}" var="channel">
								 <option value="${channel.id}" <c:if test="${channel.id == hubsDto.issueChannelId}">selected="selected"</c:if> >${channel.name}</option>
							 </c:forEach>
						 </select>
					 </div>
				 </div>

			   
			   <div class="form-group">
			      <label  class="col-sm-4 control-label">保险商品</label>
			      <div class="col-sm-8">
					 <select class="selectpicker show-tick form-control" multiple data-live-search="false" id="select-type" name="idAndCodeAndPCode" aria-invalid="false">
				          <c:forEach items="${productList }" var="product">
						 		<option <c:if test="${product.flag}">selected</c:if>  value="${product.id },${product.providerProductCode },${product.providerCode }">${product.name }</option>
				          </c:forEach>
				     </select>	         
			      </div>
			   </div>   

			   <div class="form-group">
			      <label class="col-sm-4 control-label">是否支持投保确认接口</label>
				  <div class="col-sm-8">
				     <label class="radio-inline"><input  type="radio" value="true" name="isCanPolicyConfirm" <c:if test="${hubsDto.isCanPolicyConfirm}">checked="checked"</c:if> />是</label>
				     <lable class="radio-inline"><input  type="radio" value="false" name="isCanPolicyConfirm" <c:if test="${!hubsDto.isCanPolicyConfirm}">checked="checked"</c:if> />否</lable>
				  </div>
			   </div>
			   
			   <div class="form-group">
			      <label class="col-sm-4 control-label">是否支持保单撤销接口</label>
				  <div class="col-sm-8">
				     <label class="radio-inline"><input  type="radio" value="true" name="isCanPolicyCancel" <c:if test="${hubsDto.isCanPolicyCancel}">checked="checked"</c:if> />是</label>
				     <lable class="radio-inline"><input  type="radio" value="false" name="isCanPolicyCancel" <c:if test="${!hubsDto.isCanPolicyCancel}">checked="checked"</c:if> />否</lable>
				  </div>
			   </div>	
			   
			   <div class="form-group">
			      <label class="col-sm-4 control-label">是否支持保单打印接口</label>
				  <div class="col-sm-8">
				     <label class="radio-inline"><input  type="radio" value="true" name="isCanPolicyPrint" <c:if test="${hubsDto.isCanPolicyPrint}">checked="checked"</c:if> />是</label>
				     <lable class="radio-inline"><input  type="radio" value="false" name="isCanPolicyPrint" <c:if test="${!hubsDto.isCanPolicyPrint}">checked="checked"</c:if>/>否</lable>
				  </div>
			   </div>				   		   		  	 	
		  	 	
			   <div class="form-group">
			      <label class="col-sm-4 control-label">是否禁用</label>
				  <div class="col-sm-8">
				     <label class="radio-inline"><input  type="radio" value="true" name="isDisable" <c:if test="${hubsDto.isDisable}">checked="checked"</c:if> />是</label>
				     <lable class="radio-inline"><input  type="radio" value="false" name="isDisable" <c:if test="${!hubsDto.isDisable}">checked="checked"</c:if>/>否</lable>
				  </div>
			   </div>
			  
	
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            <button type="button" class="btn btn-primary" onclick="submitHubs()">保存</button>
         </div>
      </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var hubsCode = "${hubsDto.hubsCode}";
		if(hubsCode!=null && hubsCode!=''){//修改
			$('#hubsCode').attr('readonly',true);
		}
		
	});
	
</script>