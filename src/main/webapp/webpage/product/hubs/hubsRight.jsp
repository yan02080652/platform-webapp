<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
 <style>
 #trans-detail table{
 	margin-left: 15px;
 }
 #trans-detail table th{
 	font-size: 14px;
 	text-align: right;
 }
 
</style>
<c:if test="${not empty productHubs}">
	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>连接器详细信息&nbsp;&nbsp;
		</div>
		<div class="panel-body">
			<div id="trans-detail">
				<table class="table">
					<thead>
						<tr>
							<th>编码</th>
							<th>名称</th>
							<th>保险名称</th>
							<th>供应商名称</th>
							<th>供应商商品编码</th>
							<th>是否禁用</th>
							<th>是否支持投保确认接口</th>
							<th>是否支持保单撤销接口</th>
							<th>是否支持保单打印接口</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${productHubs }" var="hubsDto">
							<tr>
								<td width="10%">${hubsDto.hubsCode }</td>
								<td width="10%">${hubsDto.hubsName }</td>
								<td width="10%">${hubsDto.productName }</td>
								<td width="10%">${hubsDto.providerName }</td>
								<td width="10%">${hubsDto.productCode }</td>
								<td>
									<c:if test="${hubsDto.isDisable}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
									<c:if test="${!hubsDto.isDisable}"> <input type="checkbox" disabled="disabled"/></c:if>
								</td>
								<td>
									<c:if test="${hubsDto.isCanPolicyConfirm}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
									<c:if test="${!hubsDto.isCanPolicyConfirm}"> <input type="checkbox" disabled="disabled"/></c:if></td>
								</td>
								<td>
									<c:if test="${hubsDto.isCanPolicyCancel}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
									<c:if test="${!hubsDto.isCanPolicyCancel}"> <input type="checkbox" disabled="disabled"/></c:if></td>
								</td>
								<td>
									<c:if test="${hubsDto.isCanPolicyPrint}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
									<c:if test="${!hubsDto.isCanPolicyPrint}"> <input type="checkbox" disabled="disabled"/></c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
</c:if>

<div id="modelHubsConnectDiv"></div>
