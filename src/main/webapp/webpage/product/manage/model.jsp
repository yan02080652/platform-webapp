<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<div id="rightMenuDetail" class="col-md-9 animated fadeInRight">
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>计划信息<c:choose><c:when test="${product.id != '' && product.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
             	 &nbsp;&nbsp;<a data-pjax onclick="back_product();">返回</a>
             </div>
             <div class="panel-body">
                 <form id="product_model_form" action="product/save" method="post" class="form-horizontal bv-form">
					  <input name="id" type="hidden" value="${product.id}"/>
					  <input name="categoryId" type="hidden" value="${categoryId}"/>
					  <div class="form-group">
					    <label class="col-sm-2 control-label">计划编号:</label>
					    <div class="col-sm-3">
					      <input type="text" name="code" class="form-control" value="${product.code }">
					    </div>
					    <label class="col-sm-2 control-label">计划名称:</label>
					    <div class="col-sm-3">
					      <input type="text" name="name" class="form-control" value="${product.name }">
					    </div>					    
					  </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">计划说明:</label>
					    <div class="col-sm-8">
					      <textarea rows="3" name="description" class="form-control">${product.description }</textarea>
					    </div>
					  </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">有效期开始:</label>
					    <div class="col-sm-3">
					      <input type="datetime" name="validFrom" onClick="WdatePicker()" class="form-control" value="<fmt:formatDate value="${product.validFrom }"   pattern="yyyy-MM-dd"/>">
					    </div>
					    <label class="col-sm-2 control-label">有效期结束:</label>
					    <div class="col-sm-3">
					      <input type="datetime" name="validTo" onClick="WdatePicker()" class="form-control" value="<fmt:formatDate value="${product.validTo }"   pattern="yyyy-MM-dd"/>">
					    </div>					    
					  </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">单人限购数量:</label>
					    <div class="col-sm-3">
					      <input type="number" name="singleBuyLimitAmount" class="form-control" value="${product.singleBuyLimitAmount }">
					    </div>
					    <label class="col-sm-2 control-label">销售价格:</label>
					    <div class="col-sm-3">
					      <input type="number" name="salePrice" class="form-control" value="${product.salePrice }">
					    </div>
					    <div>
					   	 <input type="button"  class="form-control" style="width: 98px;padding: 0px;" value="维护价格政策" />
					    </div>
					  </div>
			  		 <div class="form-group">
					    <label class="col-sm-2 control-label">所属供应商:</label>
					    <div class="col-sm-3">
					   	  <input type="text" disabled="disabled" class="form-control" value="${providerDto.name }"  />
					   	  <input type="hidden" name="providerCode" value="${providerDto.code }">				    
					   	</div>
					    <label class="col-sm-2 control-label">供应商计划编码:</label>
					    <div class="col-sm-3">
					      <input type="text" name="providerProductCode" class="form-control" value="${product.providerProductCode }">
					    </div>					    
					  </div>
			  		 <div class="form-group">
					    <label class="col-sm-2 control-label">供应商结算参考价:</label>
					    <div class="col-sm-3">
					      <input type="number" name="settlePrice" class="form-control" value="${product.settlePrice }">
					    </div>
					    <label class="col-sm-2 control-label">保障金额:</label>
					    <div class="col-sm-3">
					      <input type="text" name="protectAmount" class="form-control" value="${product.protectAmount }">
					    </div>					    
					  </div>					  
					  <div class="tabbable  col-sm-8">
					      <a class="btn btn-default" onclick="submitProduct()">提交</a>
					  </div>					  					  					  					  	  					  					  					  					  					  					  
					</form>
             </div>
         </div> 
	</div>
</div>
</div>
<script>

function submitProduct(){
	 $('#product_model_form').submit();
}
$(document).ready(function() {
	    $("#product_model_form").validate({
	        rules : {
	            code : {
	                required : true,
	                minlength : 1
	            },
	            name:{
	                required:true,
                    minlength:1,
					maxlength:12
	            },
	            singleBuyLimitAmount:{
	                required:true
	            },
	            salePrice :{
	            	required:true
	            },
	            stockAmount:{
	                required:true
	            }
	        },
	        submitHandler : function(form) {
	            var option = {
	                success : function (data) {
                        if (data.type == 'success') {

                            $("#product_list").show();
                            $("#product_detail").hide();

                            reloadRight();

                        } else {
                            showErrorMsg(data.txt);
                        }
                    },
                    error : function(request) {
                        showErrorMsg("请求失败，请刷新重试");
                    }
				}
	            $(form).ajaxSubmit(option)
				
	        },
	        errorPlacement : setErrorPlacement,
	        success : validateSuccess,
	        highlight : setHighlight
	    });
});
</script> 