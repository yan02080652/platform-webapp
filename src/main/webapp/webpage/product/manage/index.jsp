<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<!-- 商品列表页 -->
<div class="panel-body">
	<div class="row">

		<div class="col-md-12">
           <div class="form-inline">
           		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>保险管理   &nbsp;&nbsp;
			</div>
		</div>

		<hr/>

		<div class="col-md-3">
			<div class="panel panel-default">
			<div class="panel-body" style="padding-left: 10px;padding-right: 10px;">
				<button class="btn btn-default" onclick="addTopNode();" id="addTopNode">添加顶级</button>
				<button class="btn btn-default" onclick="addNode();" id="addNode">添加下级</button>
				<button class="btn btn-default" onclick="editNode();" id="editNode">修改</button>
				<button class="btn btn-default" onclick="deleteNode();" id="deleteNode">删除</button>
			 </div>
				<div class="panel-body">
					<ul id="product_tree" style="padding-left: 10px;"></ul>
				</div>
			</div>
		</div>

		<div id="product_list" class="col-md-8 animated fadeInRight">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12">
									<form class="form-inline" method="post" action="product/list" id="product-form">
										<input type="hidden" name="categoryId"/>
										<input type="hidden" name="pageIndex"/>
										<div class=" form-group">
											<label>&nbsp;&nbsp;&nbsp;计划名称：</label>
											<input name="name" type="text" class="form-control"/>
										</div>
										<div class="form-group">
											<label>&nbsp;&nbsp;&nbsp;计划编号：</label>
											<input name="code" type="text" class="form-control" />
										</div>
										<div class="form-group">
											<input type="button" onclick="reloadRight();" class="form-control" value="查找" />
											<input type="reset" onclick="resetForm()" class="form-control" value="重置" />
										</div>
									</form>
								</div>
							</div>
							<div class="row row-bottom" style="margin-top: 10px">
								<div class="col-md-12 text-left">
									<a class="btn btn-default" onclick="addProduct();" role="button">添加计划</a>
								</div>
							</div>
							<div class="table-responsive" id="product_Table">

							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="product_detail">

	   </div>
	</div>
</div>

<div id="ModelDiv"></div>

<script type="text/javascript" src="webpage/product/manage/product.js?version=${globalVersion}"></script>