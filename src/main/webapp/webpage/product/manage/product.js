/**
 * 商品管理
 */
var currentNode;

require(['treeview'],function(){
    reloadTree();
    reloadRight();
});

//加载商品树
function reloadTree() {
	$.ajax({
		type : 'GET',
		url : "product/category/list",
		success : function(data) {
			$('#product_tree').treeview({
				data: TR.turnToTreeViewData(data,{pid:"parentId"}),
				showBorder:false,
				highlightSelected:true,
				onNodeSelected: function(event, data) {
					currentNode = data;

					$("input[name='categoryId']").val(data.id);
					reloadRight(1);
					
					$("#product_list").show();
					$("#product_detail").hide();
				}, 
				onNodeUnselected: function(event, data) {
					
					currentNode = null;
				}
			});
		}
	});
}

//加载产品列表
function reloadRight(pageIndex){
    pageIndex = pageIndex ? pageIndex : 1;

	$("input[name='pageIndex']").val(pageIndex);

	var option = {
		success :function (data) {
            $("#product_Table").html("").html(data);
        }
	}
	$("#product-form").ajaxSubmit(option);
}

//重置按钮
function resetForm() {
	$("#product-form")[0].reset();
    reloadRight(1);
}


//删除节点
function deleteProduct(id,name) {
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除当前商品："+name+"?"	,
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "product/delete/"+id+"",
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {

					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadRight(1);
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		}
	});
};

//添加修改商品
function addProduct(productId){
	if(!currentNode && !productId){
		layer.alert("请选择目录");
		return ;
	}

    var providerCode;
	if(currentNode){
        providerCode = currentNode.data.providerCode;
	}
    var categoryId;
	if(currentNode){
        categoryId = currentNode.id;
    }

	$.post("product/detail", {
		 productId : productId,
		 providerCode : providerCode,
         categoryId : categoryId

	 }, function(data){
		
		$("#product_detail").html(data);

	});
	$("#product_list").hide();
	$("#product_detail").show();
}

//返回商品列表 未对内容进行修改
function back_product(){

	$("#product_list").show();
	$("#product_detail").hide().html("");
}

//添加顶级节点
function addTopNode() {

	showCategoryModal("product/category/add", {
		'pid' : null
	}, function() {
		$('#mode_category').modal();
		initForm();
	});
};

//添加下级节点
function addNode() {

	if (currentNode == null || currentNode.data.nodeType == 0) {
		$.alert("请选择一个目录节点!", "提示");
		return;
	}
    showCategoryModal("product/category/add", {
		'pid' : currentNode.id
	}, function() {
		$('#mode_category').modal();
		initForm();
	});
};

//修改节点信息
function editNode() {

	if (currentNode == null) {
		$.alert("请选择一个节点!", "提示");
		return;
	}
    showCategoryModal("product/category/detail", {
		'Id' : currentNode.id
	}, function(a) {
		$('#mode_category').modal();
		initForm();
	});

};

//显示Model
function showCategoryModal(url, data, callback) {
	$('#ModelDiv').load(url, data, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

//删除节点
function deleteNode() {
	if (currentNode == null) {
		$.alert("请选择一个节点!", "提示");
		return;
	}

	if (currentNode.nodes) {
		$.alert("父节点不允许直接删除,请先删除该节点下的所有子节点后重新操作");
		return;
	}
	$.confirm({
		title : '提示',
		confirmButton : '确认',
		cancelButton : '取消',
		content : '确认删除当前节点吗?',
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "product/category/delete",
				data : {
					Id : currentNode.id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadTree();
						currentNode = null;
					} else {
						$.alert(data.txt, "提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

function initForm() {
	$("#detailForm").validate(
			{
				rules : {
					name : {
						required : true,
						minlength : 1,
						maxlength : 15,
					},
					code : {
						required : true,
						minlength : 1,
						maxlength : 20,
					},
				},
				messages : {
				},
				submitHandler : function(form) {
					$.ajax({
						cache : true,
						type : "POST",
						url : "product/category/save",
						data : $('#detailForm').serialize(),
						async : false,
						error : function(request) {
							showErrorMsg("请求失败，请刷新重试");
							$('#mode_category').modal('hide');
						},
						success : function(data) {
							$('#mode_category').modal('hide');
							if (data.type == 'success') {
								showSuccessMsg("操作成功");
								reloadTree();
								reloadRight(1);
							} else {
								showErrorMsg(data.txt);
							}
						}
					});
				},
				errorPlacement : setErrorPlacement,
				success : validateSuccess,
				highlight : setHighlight
			});
}

function submit() {
	$("#detailForm").submit();
}

function changeCateType(){
	
	var type = $("#cateType").val();
	if(type == 0){
		$("#myTabContent").hide();
	}
	else{
		$("#myTabContent").show();
	}
	
}
