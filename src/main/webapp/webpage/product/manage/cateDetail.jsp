<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="mode_category" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">分类</h4>
         </div>
         <div class="modal-body">
         <form modelAttribute="category" class="form-horizontal" id="detailForm" role="form">
            	<input type="hidden" name="parentId"  value="${category.parentId}"/>
            	<input type="hidden" name="id"  value="${category.id}"/>
				<input type="hidden" name="path" value="${category.path }"/>
			   <div class="form-group">
			      <label class="col-sm-2 control-label">名称</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" name="name" value="${category.name}"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label class="col-sm-2 control-label">类型</label>
			      <div class="col-sm-10">
			         <select class="form-control" name="type" id="cateType" onchange="changeCateType()"  value="${category.type}">
			         	<option value="0" <c:if test="${category.type eq 0}">selected</c:if>>目录</option>
			         	<option value="1" <c:if test="${category.type eq 1}">selected</c:if>>产品</option>
			         </select>
			      </div>
			   </div>			   
			   <div class="form-group">
			      <label class="col-sm-2 control-label">编码</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" name="code" value="${category.code}"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label class="col-sm-2 control-label">排序值</label>
			      <div class="col-sm-10">
			         <input type="number" class="form-control" name="seq" value="${category.seq}" placeholder="排序值"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label class="col-sm-2 control-label">供应商</label>
			      <div class="col-sm-10">
					      <select class="form-control" name="providerCode">
					      	<c:forEach items="${providerList }" var="provider">
					      		<option value="${provider.code }" <c:if test="${category.providerCode eq provider.code}">selected='selected'</c:if> >${provider.name }</option>
					      	</c:forEach>	
					      </select>
			      </div>
			   </div>			   
					<div id="myTabContent" class="tab-content" <c:if test="${category.type != 1}">style="display: none;"</c:if> > 
					  		 <div class="tab-pane fade in active" id="home">
					  		   <table class="table table-bordered">
					  		    	<thead>
					  		    		<tr>
					  		    			<th>场景</th>
					  		    			<th>编码</th>
					  		    			<th>是否可用</th>
					  		    		</tr>
					  		    	</thead>
					  		    	<tbody>
					  		    		<c:forEach items="${scenceList }" var="scence" varStatus="status">
					  		    			<tr>
					  		    				<input type="hidden" value="${scence.senceCode }" name="scence[${status.index }].senceCode"/>
					  		    				<td>${scence.senceName }</td>
					  		    				<td>${scence.senceCode }</td>
					  		    				<td><input type="checkbox" name="scence[${status.index }].isChecked" <c:if test = "${not empty scence.scenceId }">checked="checked"</c:if> ></td>
					  		    			</tr>
					  		    		</c:forEach>
					  		    	</tbody>
					  		    </table>
					  		 </div>
					</div>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="submit()">保存</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
         </div>
      </div>
</div>
</div>