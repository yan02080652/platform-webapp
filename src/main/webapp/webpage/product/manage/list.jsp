<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
	      <th><label>编号  </label></th>
          <th><label>名称</label></th>
          <th><label>供应商</label></th> 
          <th><label>有效期开始 </label></th>
          <th><label>有效期结束</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${pageList.list }" var="product">
         <tr class="odd gradeX">
             <td>${product.code }</td>
             <td>${product.name}</td>
             <td>${product.provider.name }</td>
             <td><fmt:formatDate value="${product.validFrom }"   pattern="yyyy-MM-dd"/></td>
             <td><fmt:formatDate value="${product.validTo }"   pattern="yyyy-MM-dd"/></td>
             <td class="text-center">
             	<a onclick="addProduct('${product.id }')">修改</a>&nbsp;&nbsp;
             	<a onclick="deleteProduct('${product.id }','${product.name }');">删除</a>
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadRight"></jsp:include>
</div>