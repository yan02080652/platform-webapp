<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>路由规则<c:choose><c:when
                    test="${routeRuleDto.id != '' && routeRuleDto.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
                &nbsp;&nbsp;<a href="train/routeRule">返回</a>
            </div>
            <div class="panel-body">
                <form:form modelAttribute="routeRuleDto" id="ruleForm" cssClass="form-horizontal bv-form" method="post"
                           action="train/routeRule/save">
                    <form:hidden path="id"/>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">规则名称</label>
                        <div class="col-sm-6">
                            <form:input path="ruleName" cssClass="form-control" required="true"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">规则级别</label>
                        <div class="col-sm-6 ">
                            <form:radiobuttons path="ruleGrade" cssClass="" itemLabel="msg"/>
                        </div>
                    </div>
                    <div class="form-group" id="scopeDiv">
                        <label class="col-sm-2 control-label">适用范围</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control partner" name="tmcName" value="${tmcPartner.name}"
                                   placeholder="请先选择规则级别" readonly/>
                            <form:hidden path="scope"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">优先级</label>
                        <div class="col-sm-6">
                            <form:input path="rank" cssClass="form-control" placeholder="从1开始，越小优先级越大"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">连接器</label>
                        <div class="col-sm-6">
                            <form:select path="hubsCode" cssClass="form-control" onchange="hubsChange(this.value)">
                                <form:options items="${hubs}" itemValue="code" itemLabel="name"/>
                            </form:select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">连接账号</label>
                        <div class="col-sm-6">
                            <form:select path="husConnectId" cssClass="form-control">

                            </form:select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">自动出票</label>
                        <div class="col-sm-6 checkbox-inline">
                            <form:checkbox path="isAutoIssue" label="是" cssClass=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">是否禁用</label>
                        <div class="col-sm-6 form-inline checkbox-inline">
                            <form:checkbox path="isDisable" label="是" cssClass="check"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-offset-6">
                            <button type="submit" class="btn btn-default">提交</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {

        var ruleGrade = $('input:radio[name="ruleGrade"]:checked').val();

        //如果进入的是修改页面就查询连接
        if (${routeRuleDto.id != '' && routeRuleDto.id != null}) {
            hubsChange($('#hubsCode').val(), ruleGrade);
        }

        if ("PARTNER" == ruleGrade) {
            $(".partner").click(function () {
                TR.select('partner', {
                    type: 0
                }, function (result) {
                    $("[name='tmcName']").val(result.name);
                    $("[name='scope']").val(result.id);
                    hubsChange($('#hubsCode').val(), ruleGrade);
                });
            });
        } else if ("TMC" == ruleGrade) {
            $(".partner").click(function () {
                TR.select('partner', {
                    type: 1
                }, function (result) {
                    $("[name='tmcName']").val(result.name);
                    $("[name='scope']").val(result.id);
                    hubsChange($('#hubsCode').val(), ruleGrade);
                });
            });
        } else {
            $(".partner").click(function () {
                ruleGrade = $('input:radio[name="ruleGrade"]:checked').val();
                if (ruleGrade == null) {
                    layer.msg("请先选择规则级别");
                }
            });
        }

        $('input:radio[name="ruleGrade"]').change(function () {
            ruleGrade = $('input:radio[name="ruleGrade"]:checked').val();
            var type = 1;
            var value = $(this).val();
            $('#scopeDiv').show();
            if (value == 'PARTNER') {
                type = 0;
            }
            $(".partner").click(function () {
                TR.select('partner', {
                    type: type
                }, function (result) {
                    $("[name='tmcName']").val(result.name);
                    $("[name='scope']").val(result.id);
                    hubsChange($('#hubsCode').val(), ruleGrade);
                });
            });

            //设置适用范围输入框提示
            if (type == 1) {
                $("[name='tmcName']").attr("placeholder", "请选择TMC");
            } else if (type == 0) {
                $("[name='tmcName']").attr("placeholder", "请选择企业");
            } else {
                $("[name='tmcName']").attr("placeholder", "请先选择规则级别");
            }

            //清空内容
            $("[name='tmcName']").val("");
            $("[name='scope']").text("");
        })
    })

    $("#ruleForm").validate(
        {
            rules: {
                ruleName: {
                    required: true,
                    maxlength: 30
                },
                ruleGrade: {
                    required: true
                },
                scope: {
                    required: true
                },
                rank: {
                    required: true,
                    digits: true,
                    min: 1
                },
                husConnectId: {
                    required: true
                }
            },
            submitHandler: function () {
                submitRule();
            },
            errorPlacement: setErrorPlacement,
            success: validateSuccess,
            highlight: setHighlight
        }
    );

    //提交表单
    function submitRule() {
        $.ajax({
            cache: true,
            type: "POST",
            url: "train/routeRule/save",
            data: $('#ruleForm').serialize(),
            async: false,
            error: function () {
                showErrorMsg("请求失败，请刷新重试");
            },
            success: function (data) {
                if (data.code == "0") {
                    showSuccessMsg(data.data);
                    setTimeout(function () {
                        window.history.go(-1);
                    }, 500);
                } else {
                    $.alert(data.data, "提示");
                }
            }
        });
    }

    function hubsChange(hubsCode, ruleGrade) {
        var scope = $('#scope').val();
        if (scope == "") {
            layer.msg("请先选择适用范围");
            return;
        }
        if (ruleGrade == "" || ruleGrade == null) {
            ruleGrade = $('input:radio[name="ruleGrade"]:checked').val();
        }
        var husConnectId = '${routeRuleDto.husConnectId}';
        $.ajax({
            url: '/train/routeRule/getConnects.json',
            type: 'post',
            data: {
                hubsCode: hubsCode,
                scope: scope,
                ruleGrade: ruleGrade
            },
            success: function (data) {
                $('#husConnectId').html('');
                for (var i = 0; i < data.length; i++) {
                    if (husConnectId == data[i].id) {
                        $('#husConnectId').append('<option value="' + data[i].id + '" selected>' + data[i].husConnectName + '</option>');
                    } else {
                        $('#husConnectId').append('<option value="' + data[i].id + '">' + data[i].husConnectName + '</option>');
                    }
                }
            },
            error: function () {
                layer.msg("系统异常", {icon: 2});
            }
        })
    }

</script>