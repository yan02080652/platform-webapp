<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    body {
        background-color: #FFFFFF;
    }

    .layui-form-label {
        box-sizing: content-box;
    }

    .box {
        padding-top: 20px;
    }

    .layui-form-switch {
        width: 52px;
    }

    .carrierBtn {
        margin-top: 3px;
    }

    .codes {
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        width: 500px
    }

</style>
<div class="box">
    <form class="layui-form" action="">
        <input type="hidden" name="id" value="${itemDto.id}"/>
        <input type="hidden" name="routeRuleId" value="${routeRuleId}"/>
        <input type="hidden" name="hubsCode" value="${itemDto.hubsCode}"/>
        <input type="hidden" name="connectId" value="${itemDto.connectId}"/>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">优先级</label>
                <div class="layui-input-inline">
                    <input type="text" name="rank" value="${itemDto.rank}" lay-verify="required|positiveNum"
                           class="layui-input">
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">连接账号</label>
                <div class="layui-input-inline">
                    <select name="connectAcc" lay-verify="select" lay-filter="connectAcc">
                        <option value="">请选择</option>
                        <c:forEach items="${connectMap}" var="map">
                            <optgroup label="${map.key}">
                                <c:forEach items="${map.value}" var="hubsConnect">
                                    <option value="${hubsConnect.hubsCode.concat("-").concat(hubsConnect.id)}"
                                            <c:if test="${itemDto.connectId eq hubsConnect.id}">selected</c:if> >${hubsConnect.husConnectName}</option>
                                </c:forEach>
                            </optgroup>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">查询</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="search" title="开启" value="1"
                           <c:if test="${itemDto.search}">checked</c:if>>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">验余/证件号</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="verify" title="开启" value="1"
                           <c:if test="${itemDto.verify}">checked</c:if>>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">占座</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="occupancySeat" title="开启" value="1"
                           <c:if test="${itemDto.occupancySeat}">checked</c:if>>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">抢票出票</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="grabTickets" title="开启" value="1"
                           <c:if test="${itemDto.grabTickets}">checked</c:if>>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">普通出票</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="normalIssue" title="开启" value="1"
                           <c:if test="${itemDto.normalIssue}">checked</c:if>>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <input type="checkbox" name="status" lay-skin="switch" lay-text="启用|禁用" value="1"
                       <c:if test="${itemDto.status}">checked</c:if>>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="itemFrom">保存</button>
                <button onclick="closeIframe()" class="layui-btn layui-btn-primary">取消</button>
            </div>
        </div>
    </form>
</div>
<script>
    //保存已选清单
    var form;
    $(function () {
        layui.use('form', function () {
            form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
            //验证规则
            form.verify({
                positiveNum: [/[1-9]\d*$/, '优先级必须正整数'],
                select: function (value) {
                    if (!value | value.length <= 0) {
                        return "请选择";
                    }
                },
                notAll: function (value) {
                }
            });

            //监听select
            form.on('select(connectAcc)', function (data) {
                var hubsCode = data.value.split("-")[0];
                render(hubsCode);

            });

            //监听提交
            form.on('submit(itemFrom)', function (data) {
                //保存配置项
                var formData = dataHandler(data.field);
                $.ajax({
                    url: "train/routeRule/saveRouteRuleItem",
                    data: formData,
                    type: "post",
                    success: function (result) {
                        if (result.code == '0') {
                            parent.location.reload();
                            closeIframe();
                        } else {
                            layer.msg(result.msg, {icon: 2});
                        }
                    }
                })
                return false;
            });
            let hubsCode = '${itemDto.hubsCode}';
            if (hubsCode) {
                render(hubsCode);
            }
        });
    })

    //根据hubsCode查询hub,控制页面效果
    function render(hubsCode) {
        $.get("train/hubs/getHubByCode?hubsCode=" + hubsCode, function (result) {
            if (result) {
                var search = result.search;
                var verify = result.verify;
                var occupancySeat = result.occupancySeat;
                var grabTickets = result.grabTickets;
                var normalIssue = result.normalIssue;
                if (!search) {
                    $("input[name='search']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='search']").prop('disabled', false);
                }
                if (!verify) {
                    $("input[name='verify']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='verify']").prop('disabled', false);
                }
                if (!occupancySeat) {
                    $("input[name='occupancySeat']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='occupancySeat']").prop('disabled', false);
                }
                if (!grabTickets) {
                    $("input[name='grabTickets']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='grabTickets']").prop('disabled', false);
                }
                if (!normalIssue) {
                    $("input[name='normalIssue']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='normalIssue']").prop('disabled', false);
                }
                //更新页面渲染
                form.render();
            }
        })
    }

    //关闭iframe
    function closeIframe() {
        //先得到当前iframe层的索引,再执行关闭
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }

    //表单数据处理
    function dataHandler(data) {
        if (!data.search) {
            data.search = "0";
        }
        if (!data.verify) {
            data.verify = "0";
        }
        if (!data.occupancySeat) {
            data.occupancySeat = "0";
        }
        if (!data.grabTickets) {
            data.grabTickets = "0";
        }
        if (!data.normalIssue) {
            data.normalIssue = "0";
        }
        if (!data.status) {
            data.status = "0";
        }
        var split = data.connectAcc.split("-");
        data.hubsCode = split[0];
        data.connectId = split[1];
        return data;
    }
</script>