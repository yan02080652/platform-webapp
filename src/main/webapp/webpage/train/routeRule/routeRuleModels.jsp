<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    td {
        text-align: center;
    }
    .word {
        width: 200px;
        display: block;
        text-overflow: ellipsis;
        -webkit-line-clamp: 1;
        white-space: nowrap;
        overflow: hidden;
        text-align: center;
    }
</style>
<link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        路由规则维护&nbsp;<a href="train/routeRule">返回</a>
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <!-- 路由规则定义 -->
            <div class="col-sm-12">
                <div class="page-header">
                    <h3>基础信息 |&nbsp;
                        <button class="layui-btn layui-btn-normal" onclick="submitForm()">
                            <i class="layui-icon">&#xe63c;保存</i>
                        </button>
                    </h3>
                </div>
                <form class="form-horizontal" id="routeRuleForm" role="form">
                    <input type="hidden" name="id" id="routeRuleDtoId" value="${routeRuleDto.id}"/>

                    <div class="form-group">
                        <label class="col-sm-2">规则级别</label>
                        <div class="col-sm-3">
                            <c:forEach items="${ruleGrade}"  var="grade">
                                <input type="radio" class="gradeClass" name="ruleGrade" value="${grade}" ${routeRuleDto.ruleGrade eq grade ? "checked" : ""}/>${grade.msg}
                            </c:forEach>
                        </div>

                        <!-- 当选择TMC或企业是出现选择器 -->
                        <div class="col-sm-4" hidden="hidden" id="chooseDepDiv">
                            <input type="hidden" name="scope" id="bpId" value="${routeRuleDto.scope}">
                            适用范围： <span id="bpName">${tmcPartner}</span>
                            <button type="button" class="btn btn-default btn-xs" onclick="chooseDep()">请选择...</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 ">状态</label>
                        <div class="col-sm-3">
                            <input type="radio" name="isDisable" value="0" ${!routeRuleDto.isDisable ? "checked" : ""}/>启用
                            <input type="radio" name="isDisable" value="1" ${routeRuleDto.isDisable ? "checked" : ""}/>禁用
                        </div>
                    </div>

                    <c:if test="${not empty routeRuleDto.id}">
                        <div class="form-group">
                            <label class="col-sm-2 ">创建人</label>
                            <div class="col-sm-4">
                                <label>${routeRuleDto.createrName}</label>
                            </div>

                            <label class="col-sm-2 ">创建时间</label>
                            <div class="col-sm-4">
                                <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                                       value="${routeRuleDto.createDate}"/></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 ">最后修改人</label>
                            <div class="col-sm-4">
                                <label>${routeRuleDto.lastModifierName}</label>
                            </div>

                            <label class="col-sm-2 ">最后修改时间</label>
                            <div class="col-sm-4">
                                <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                                       value="${routeRuleDto.lastUpdateDate}"/></label>
                            </div>
                        </div>
                    </c:if>
                </form>
            </div>

            <c:if test="${routeRuleDto.id != null}">
                <!-- 配置项-->
                <div class="col-sm-12">
                    <div class="page-header">
                        <h3>配置信息 |&nbsp;
                            <button class="layui-btn layui-btn-normal" onclick="getRouteRuleItem(${routeRuleDto.id})">
                                <i class="layui-icon">&#xe61f;连接</i>
                            </button>
                        </h3>
                    </div>
                    <div>
                        <table class="table table-striped table-bordered" lay-skin="row" id="itemTable">
                            <thead>
                            <tr>
                                <td><label>连接优先级</label></td>
                                <td><label>连接名称</label></td>
                                <td><label>查询</label></td>
                                <td><label>验余/证件号</label></td>
                                <td><label>占座</label></td>
                                <td><label>抢票出票</label></td>
                                <td><label>普通出票</label></td>
                                <td><label>状态</label></td>
                                <td><label>操作</label></td>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${routeRuleDto.ruleItemList}" var="item">
                                <tr>
                                    <td>${item.rank}</td>
                                    <td>${item.connectName}</td>
                                    <td><input type="checkbox" <c:if test="${item.search}">checked</c:if> disabled></td>
                                    <td><input type="checkbox" <c:if test="${item.verify}">checked</c:if> disabled></td>
                                    <td><input type="checkbox" <c:if test="${item.occupancySeat}">checked</c:if> disabled></td>
                                    <td><input type="checkbox" <c:if test="${item.grabTickets}">checked</c:if> disabled></td>
                                    <td><input type="checkbox" <c:if test="${item.normalIssue}">checked</c:if> disabled></td>
                                    <td>${item.status ? "启用" : "禁用"}</td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-sm"
                                                onclick="getRouteRuleItem(${routeRuleDto.id},${item.id})">编辑
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm"
                                                onclick="deleteItem(${item.id})">删除
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
<script type="text/javascript" src="resource/plugins/json-utils/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="webpage/train/routeRule/routeRule.js?version=${globalVersion}"></script>
<script type="text/javascript">
    $(function () {
        if ($('input:radio[class="gradeClass"]').is(':checked')) {
            //显示后面的div
            $("#chooseDepDiv").show();
        }
    })

    $(".gradeClass").change(function () {
        $("#bpId").val("");
        $("#bpName").text("");
        $("#chooseDepDiv").show();
    })
    //选择合作伙伴
    function chooseDep() {
        var type = $('input:radio[class="gradeClass"]:checked').val() == 'TMC' ? 1 : 0;
        // 0是企业,1是TMC
        TR.select('partner', {type: type}, function (data) {
            $("#bpId").val(data.id);
            $("#bpName").text(data.name);
        });
    }

</script>
