<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover" id="ruleTable">
    <thead>
    <tr>
        <th><label>规则级别</label></th>
        <th><label>适用范围</label></th>
        <th><label>是否禁用</label></th>
        <th><label>创建用户</label></th>
        <th><label>创建时间</label></th>
        <th><label>修改用户</label></th>
        <th><label>修改时间</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr class="odd gradeX">
            <td>${bean.ruleGrade.msg}</td>
            <td>${bean.tmcPartnerName}</td>
            <td>${bean.isDisable ? '是' : '否'}</td>
            <td>${bean.createrName}</td>
            <td><fmt:formatDate value="${bean.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
            <td>${bean.lastModifierName}</td>
            <td><fmt:formatDate value="${bean.lastUpdateDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
            <td class="text-center">
                <a href="train/routeRule/getRouteRule?id=${bean.id}">修改</a> |
                <a onclick="updateDisable('${bean.id}','${bean.isDisable ? '启用' : '禁用'}');">${bean.isDisable ? '启用' : '禁用'}</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadTable"></jsp:include>
</div>
<script type="text/javascript">
    //checkbox全选居中，固定宽度
    $("table tr").each(function () {
        $(this).find(":first").attr("style", "text-align: center;width:50px");
    })
</script>