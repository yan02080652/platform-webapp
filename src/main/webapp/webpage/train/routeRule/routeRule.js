//当前页
var currentPage = 1;

$(function () {
    reloadTable();
})

//加载列表
function reloadTable(pageIndex) {
    $.post('train/routeRule/list', {
        pageIndex: pageIndex,
        routeRuleGrade: $('#routeRuleGrade').val(),
        pId: $("#pid").val()
    }, function (data) {
        $("#routeRule_Table").html(data);
        currentPage = pageIndex;
    });
}
//重置
function reset_routeRule() {
    $("#routeRuleGrade").val("");
    $("#pid").val("");
    $("#pname").val("");
    reloadTable();
}
$("#pname").click(function () {
    TR.select('partner', {
        //type:0//固定参数
    }, function (data) {
        $("#pid").val(data.id);
        $("#pname").val(data.name);
        reloadTable();
    });
});

function updateDisable(id, isDisable) {
    $.ajax({
        cache: true,
        type: "POST",
        url: "train/routeRule/updateDisable",
        data: {
            id: id
        },
        async: false,
        error: function () {
            layer.alert("请求失败，请刷新重试", {icon: 2})
        },
        success: function (result) {
            if (result.code == '0') {
                reloadTable(currentPage);
                layer.msg('已' + isDisable + '！', {
                    icon: 1,
                    time: 900
                });
            } else {
                layer.alert(result.data, {icon: 2})
            }
        }
    });
}

//checkbox全选
function selectAll() {
    if ($("#selectAll").is(":checked")) {
        //所有选择框都选中
        $(":checkbox").prop("checked", true);
    } else {
        $(":checkbox").prop("checked", false);
    }
}

function submitForm() {
    var formData = $("#routeRuleForm").serializeJSON();
    console.log(formData);
    //当选择的级别是企业或TMC时，适用范围必选
    if (formData.ruleGrade != 'PLATFORM') {
        if (!formData.scope) {
            layer.msg("请选择适用范围企业!", {icon: 0, time: 1000})
            return;
        }
    }
    $.ajax({
        url: "train/routeRule/save",
        type: "post",
        data: formData,
        success: function (result) {
            if (result.code == '0') {
                window.location.href = '/train/routeRule/getRouteRule?id=' + result.data.id;
            } else {
                layer.alert(result.msg, {icon: 2});
            }
        }
    })
}

//配置项详情页
function getRouteRuleItem(routeRuleId, id) {
    var url = "train/routeRule/getRouteRuleItem?routeRuleId=" + routeRuleId;
    if (id) {
        url += "&id=" + id;
    }
    layer.open({
        type: 2,
        area: ['900px', '600px'],
        title: "连接配置",
        closeBtn: 1,
        content: url,
    });
}

function deleteItem(id) {
    layer.confirm('你确定要删除该配置项吗?', function (index) {
        layer.close(index);
        var loadIndex = layer.load();
        $.ajax({
            type: "get",
            url: "train/routeRule/deleteRouteRuleItem?id=" + id,
            success: function (result) {
                layer.close(loadIndex);
                if (!result) {
                    layer.msg("删除失败，请稍后重试!", {icon: 2, time: 2000})
                    return false;
                }
                window.location.reload();
            },
            error: function () {
                layer.msg("系统错误，请稍后刷新重试！", {icon: 2, time: 2000});
                layer.close(loadIndex);
            }
        });
    });
}
