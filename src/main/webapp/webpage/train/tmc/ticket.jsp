<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="ticket_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog" style="width:1200px;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h6 class="modal-title"> 手工补单</h6>
             <span class="span-margin">订单号：${trainOrderDto.id}</span>
			 <span class="span-margin">标签:${trainOrderDto.orderShowStatus.message}</span>
			 <span class="span-margin">订单状态:${trainOrderDto.orderStatus.message}</span>
			 <span class="span-margin">支付状态:${trainOrderDto.paymentStatus.message}</span>
             <span class="span-margin">企业:${trainOrderDto.cName}</span>
             <span class="span-margin">预订人:${trainOrderDto.userName}</span>
             <span class="span-margin">${trainOrderDto.trainCode} ${trainOrderDto.fromStation} - ${trainOrderDto.arriveStation} </span>
             <span class="span-margin">共<font color="red">${fn:length(trainOrderDto.passengerList)}</font>人</span>
             <span class="span-margin"><fmt:formatDate value="${trainOrderDto.fromTime}" pattern="yyyy:MM:dd HH:mm"/> - <fmt:formatDate value="${trainOrderDto.arriveTime}" pattern="yyyy:MM:dd HH:mm"/></span>
         </div>
        <div style="width:1200px;min-height: 200px;">
         <!-- 左 -->
         <div class="modal-body" style="width:600px;min-height: 200px;">
            <h5>补单产品信息</h5>
         <form  class="form-horizontal" id="trainTicketForm" role="form">
         	<input type="hidden" value="${taskId }" id="taskId"/>
         	<input type="hidden" value="${orderId }" id="orderId"/>
         	<input type="hidden" value="${flag}" id="flag"/>
		    <input type="hidden" value="${isRevise}" id="isRevise"/>

			   <div class="form-group">
		     	 <label  class="col-sm-6 control-label">供应商名称</label>
		     	 <div class="col-sm-6">
					 <c:set value="${not empty trainOrderDto.issueChannelType ? trainOrderDto.issueChannelType : trainOrderDto.hubsCode}" var="issueChannelCode"/>
		         	<select class="form-control" id="issueChannel" name="issueChannel">
						   <option value="">----</option>
				          <c:forEach items="${issueChannelMap['1']}" var="bean">
				          	<option value="${bean.key}" ${issueChannelCode eq bean.key ? 'selected' : ''}>${bean.value}</option>
				          </c:forEach>
				    </select>
		     	 </div>
		  	 </div>
		  	 <div class="form-group">
		  	     <label  class="col-sm-6 control-label">供应商订单ID</label>
		     	  <div class="col-sm-6">
			         <input type="text" class="form-control"  name="outOrderId" id="outOrderId" value="${trainOrderDto.outOrderId}" />
			      </div>
		  	 </div>
			   <div class="form-group">
			      <label  class="col-sm-6 control-label">票号</label>
		     	  <div class="col-sm-6">
			         <input type="text" class="form-control"   name="ticketNo" id="ticketNo" value="${trainOrderDto.ticketNo}" placeholder="选填"/>
			      </div>
			   </div>

			   <div class="form-group">
			      <label  class="col-sm-6 control-label">供应商结算总价</label>
		     	  <div class="col-sm-6">
			         <input type="number" class="form-control" name="totalPiace" id="totalPiace" value="${trainOrderDto.totalSettlePrice}"/>
			      </div>
			   </div>
			 <table class="table table-bordered" id="seatTable">
				 <thead>
				 <tr>
					 <th>乘客姓名</th>
					 <th>车厢号</th>
					 <th>座位号</th>
				 </tr>
				 </thead>
				 <tbody>
				 	<c:forEach items="${trainOrderDto.passengerList}" varStatus="st" var="p" >
						 <tr>
							 <input type="hidden"  name="passengerDtos[${st.index}].id" value="${p.id}" class="id"/>
							 <td title="姓名:${p.name},证件类型:${p.credentialTypeValue},证件号码:${p.credentialNo}" class="passengerName">
								 <c:if test="${fn:length(p.name) > 5}">
									 ${fn:substring(p.name, 0, 5)}...
								 </c:if>
								 <c:if test="${fn:length(p.name) <= 5}">
									 ${p.name}
								 </c:if>
							 </td>
							 <td><input type="text" name="passengerDtos[${st.index}].trainBox"  placeholder="例如:5车" value="${p.trainBox}" class="trainBox"/></td>
							 <td><input type="text" name="passengerDtos[${st.index}].seatNo" placeholder="例如:4A号" value="${p.seatNo}" class="seatNo"/></td>
							 <td>是否无座:<input type="checkbox"  name="passengerDtos[${st.index}].isNoSeat" ${p.isNoSeat ? "checked" : ""}  class="isNoSeat" /><td>
						 </tr>
				 	</c:forEach>
				 </tbody>
			 </table>
			</form>
         </div>
         <!-- 右 -->
         <div class="modal-body" style="width:600px;min-height: 200px;float: right;margin-top: -328px;padding-right: 200px;" id="oldOrder">
            <h5>原订单渠道</h5>
            <form  class="form-horizontal"  role="form">

			   <div class="form-group">
		     	 <label  class="col-sm-6 control-label">供应商名称</label>
		     	 <div class="col-sm-6">
					<%-- <c:set value="${not empty trainOrderDto.issueChannelType ? '1' : '0'}" var="ticketType"/>--%>
					 <%--<c:set value="${not empty trainOrderDto.issueChannelType ? trainOrderDto.issueChannelType : trainOrderDto.hubsCode}" var="issueChannelCode"/>--%>

		         	<select class="form-control" id="oldOriginSelect">
						  <option value="">----</option>
				          <c:forEach items="${issueChannelMap[fn:trim(trainOrderDto.lineType)]}" var="bean">
				          	<option value="${bean.key}" ${issueChannelCode eq bean.key ? 'selected' : ''}>${bean.value }</option>
				          </c:forEach>
				    </select>
		     	 </div>
		  	 </div>
		  	 <div class="form-group">
		  	     <label  class="col-sm-6 control-label">供应商订单ID</label>
		     	  <div class="col-sm-6">
			         <input type="text" class="form-control"  value="${trainOrderDto.outOrderId }" />
			      </div>
		  	 </div>
			   <div class="form-group">
			      <label  class="col-sm-6 control-label">票号</label>
		     	  <div class="col-sm-6">
			         <input type="text" class="form-control" value="${trainOrderDto.ticketNo}"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label  class="col-sm-6 control-label">供应商结算总价</label>
		     	  <div class="col-sm-6">
			         <input type="number" class="form-control" value="${trainOrderDto.totalSettlePrice }" id="totalPiace"/>
			      </div>
			   </div>

			  </form>
         </div>
        </div>

	    <table class="table table-bordered" >
			  <thead>
			  <tr>
				  <th>补单结算总价</th>
				  <th>原单结算总价</th>
				  <th>销售总价</th>
				  <th>原单利润</th>
				  <th>补单利润</th>
			  </tr>
			  </thead>
			  <tbody>
			  <tr>
				  <td id="newtotalSettlePrice"></td>
				  <td>￥${trainOrderDto.totalSettlePrice}</td>
				  <td>￥${trainOrderDto.totalSalePrice}</td>
				  <td>￥${trainOrderDto.trSpreads}</td>
				  <td id="newTrSpreads"><font color="red"></font></td>
			  </tr>
			  </tbody>
	     </table>

         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="submitTicketFrom()">补单完成</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">
				<c:if test="${!isRevise}">
					不补了
				</c:if>
				<c:if test="${isRevise}">
					放弃
				</c:if>
			</button>
         </div>
      </div>
</div>
</div>
<script type="text/javascript">

$(function(){

	$("#oldOrder input,#oldOrder textarea,#oldOrder select").attr("disabled", true);

	$("#totalPiace").on("input",function(){

		$("#newtotalSettlePrice").text($(this).val().trim());
		var newTrSpreads = parseFloat('${trainOrderDto.totalSalePrice}') - parseFloat($(this).val().trim());
		$("#newTrSpreads").text("￥"+newTrSpreads);
	});
})



</script>