<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="offLineChangeModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog" style="width:1200px;">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h6 class="modal-title"> 改签补单</h6>
             <span class="span-margin">订单号：${order.id}</span>
			 <span class="span-margin">标签:待改签</span>
			 <span class="span-margin">订单状态:${order.orderStatus.message}</span>
			 <span class="span-margin">支付状态:${order.paymentStatus.message}</span>
             <span class="span-margin">企业:${order.cName}</span>
             <span class="span-margin">预订人:${order.userName}</span>
             <span class="span-margin">${oldOrder.trainCode} ${oldOrder.fromStation} - ${oldOrder.arriveStation} </span>
             <span class="span-margin">共<font color="red">${fn:length(oldOrder.passengerList)}</font>人</span>
             <span class="span-margin"><fmt:formatDate value="${oldOrder.fromTime}" pattern="yyyy:MM:dd HH:mm"/> - <fmt:formatDate value="${oldOrder.arriveTime}" pattern="yyyy:MM:dd HH:mm"/></span>
         </div>
        <div style="width:1200px;min-height: 200px;position: relative">
         <!-- 左 -->
         <div class="modal-body" style="width:600px;min-height: 200px;margin-top:30px;">
         	<form  class="form-horizontal" id="offLineChangeForm" role="form">
			 <input type="hidden" value="${order.id}" name="orderId"/>
         	 <input type="hidden" value="${taskId}" name="taskId"/>
			 <input type="hidden" value="true" name="success"/>
		     <div>
				 <h5>补单产品信息</h5>
				 <div class="form-group">
					 <label  class="col-sm-6 control-label">供应商名称</label>
					 <div class="col-sm-6">
						 <select class="form-control" id="issueChannel" name="issueChannelCode">
							 <option value="">----</option>
							 <c:forEach items="${issueChannelMap['1']}" var="bean">
								 <option value="${bean.key}">${bean.value}</option>
							 </c:forEach>
						 </select>
					 </div>
				 </div>
				 <div class="form-group">
					 <label  class="col-sm-6 control-label">供应商订单ID</label>
					 <div class="col-sm-6">
						 <input type="text" class="form-control"  name="outOrderId" id="outOrderId"/>
					 </div>
				 </div>
			 </div>
			 <div style="position: absolute;left: 650px;width: 400px;top: 20px;background-color: #d7d4d4;">
				<h5>新订单信息</h5>
				<p style="margin-left:70px;">
					<span class="span-margin"><fmt:formatDate value="${order.fromTime}" pattern="MM-dd"/></span>
					<span class="span-margin">${order.fromStation} - ${order.arriveStation}</span>
					<span class="span-margin"><fmt:formatDate value="${order.fromTime}" pattern="E"/></span>
					<span class="span-margin">${order.seatTypeValue}</span>
				</p>
				<p style="margin-left:70px;margin-top:50px;">
					<span class="span-margin"><fmt:formatDate value="${order.fromTime}" pattern="HH-mm"/></span>
					<span class="span-margin">————</span>
					<span class="span-margin">${order.costTime}m</span>
					<span class="span-margin">————</span>
					<span class="span-margin"><fmt:formatDate value="${order.arriveTime}" pattern="HH-mm"/></span>
				</p>
				<p style="margin-left:70px;">
					<span class="span-margin">${order.fromStation}</span>
					<span style="margin:0 70px">${order.trainCode}</span>
					<span style="float:right;margin-right:20px;">${order.arriveStation}</span>
				</p>
			 </div>
			   <div class="form-group">
			      <label  class="col-sm-6 control-label">票号</label>
		     	  <div class="col-sm-6">
			         <input type="text" class="form-control"   name="ticketNo" id="ticketNo" placeholder="选填"/>
			      </div>
			   </div>

			   <div class="form-group">
			      <label  class="col-sm-6 control-label">改签订单差额</label>
		     	  <div class="col-sm-6">
			         <input type="number" class="form-control" value="${order.totalAmount le 0 ? "" : order.totalAmount}" name="balance" id="balance"/>
			      </div>
			   </div>
			 <table class="table table-bordered" id="seatTable">
				 <thead>
				 <tr>
					 <th>乘客姓名</th>
					 <th>车厢号</th>
					 <th>座位号</th>
				 </tr>
				 </thead>
				 <tbody>
				 	<c:forEach items="${order.passengerList}" varStatus="st" var="p" >
						 <tr>
							 <input type="hidden"  name="passengerList[${st.index}].id" value="${p.id}" class="id"/>
							 <td title="姓名:${p.name},证件类型:${p.credentialTypeValue},证件号码:${p.credentialNo}" class="passengerName">
								 <c:if test="${fn:length(p.name) > 5}">
									 ${fn:substring(p.name, 0, 5)}...
								 </c:if>
								 <c:if test="${fn:length(p.name) <= 5}">
									 ${p.name}
								 </c:if>
							 </td>
							 <td><input type="text" name="passengerList[${st.index}].trainBox"  placeholder="例如:5车" value="${p.trainBox}" class="trainBox"/></td>
							 <td><input type="text" name="passengerList[${st.index}].seatNo" placeholder="例如:4A号" value="${p.seatNo}" class="seatNo"/></td>
							 <td>无座:<input type="checkbox"  name="passengerList[${st.index}].noSeat"  class="isNoSeat" /><td>
						 </tr>
				 	</c:forEach>
				 </tbody>
			 </table>
			</form>
         </div>

	    <table class="table table-bordered" >
			  <thead>
			  <tr>
				  <th>补单结算总价</th>
				  <th>原单结算总价</th>
				  <th>销售总价</th>
			  </tr>
			  </thead>
			  <tbody>
			  <tr>
				  <td>￥<span id="ticketBalance">${order.totalAmount}</span></td>
				  <td>￥<span id="service">0.00</span></td>
				  <td>￥<span id="totalBalance">${order.totalAmount}</span></td>
			  </tr>
			  </tbody>
	     </table>
         
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="offLineChangeSubmit()">补单完成</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">
				不补了
			</button>
         </div>
      </div>
</div>
</div>
</div>

<script type="text/javascript">
	$('#balance').on('input', function () {
        $('#ticketBalance').text($(this).val());
        $('#totalBalance').text($(this).val());
    });
</script>