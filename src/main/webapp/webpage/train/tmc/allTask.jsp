<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
.tooltip {
  width: 24em;
}
</style>
<div>
	<!-- 状态栏 -->
	<input type="hidden" id="currentTaskType" />
	<div class="tab pull-left">
		<ul class="fix" id="allTaskStatusTab">
			<li class="tab1 active" data-value="WAIT_PROCESS" onclick="reloadAllTask(null ,'WAIT_PROCESS')">待处理(${waitTaskTypeMap.WAIT_PROCESS==null?0:waitTaskTypeMap.WAIT_PROCESS})</li>
			<li class="tab2" data-value="PROCESSING" onclick="reloadAllTask(null,'PROCESSING')">处理中(${processingMap.PROCESSING==null?0:processingMap.PROCESSING})</li>
			<li class="tab3" data-value="ALREADY_PROCESSED" onclick="reloadAllTask(null,'ALREADY_PROCESSED')">近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})</li>
		</ul>
		<div id="t_all_content">
		<div class="content1">
			<p>
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_TICKET')">待出票(${waitTaskTypeMap.WAIT_TICKET==null?0:waitTaskTypeMap.WAIT_TICKET })</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_URGE')">待催单(${waitTaskTypeMap.WAIT_URGE==null?0:waitTaskTypeMap.WAIT_URGE})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','REFUND_TICKET_AUDIT')">退审核(${waitTaskTypeMap.REFUND_TICKET_AUDIT==null?0:waitTaskTypeMap.REFUND_TICKET_AUDIT})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_REFUND')">待退款(${waitTaskTypeMap.WAIT_REFUND==null?0:waitTaskTypeMap.WAIT_REFUND})</a>  &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_CHANGE')">待改签(${waitTaskTypeMap.WAIT_CHANGE==null?0:waitTaskTypeMap.WAIT_CHANGE})</a>

			</p>
		</div>
		<div class="content2">
			<p>
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_TICKET')">待出票(${processingMap.WAIT_TICKET==null?0:processingMap.WAIT_TICKET })</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_URGE')">待催单(${processingMap.WAIT_URGE==null?0:processingMap.WAIT_URGE})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'PROCESSING','REFUND_TICKET_AUDIT')">退审核(${processingMap.REFUND_TICKET_AUDIT==null?0:processingMap.REFUND_TICKET_AUDIT})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_REFUND')">待退款(${processingMap.WAIT_REFUND==null?0:processingMap.WAIT_REFUND})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_CHANGE')">待改签(${processingMap.WAIT_CHANGE==null?0:processingMap.WAIT_CHANGE})</a>
			</p>
		</div>
		<div class="content3">
			<p>
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_TICKET')">出票(${alreadyMap.WAIT_TICKET==null?0:alreadyMap.WAIT_TICKET })</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_URGE')">催单(${alreadyMap.WAIT_URGE==null?0:alreadyMap.WAIT_URGE})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','REFUND_TICKET_AUDIT')">退审核(${alreadyMap.REFUND_TICKET_AUDIT==null?0:alreadyMap.REFUND_TICKET_AUDIT})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_REFUND')">退款(${alreadyMap.WAIT_REFUND==null?0:alreadyMap.WAIT_REFUND})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_CHANGE')">改签(${alreadyMap.WAIT_CHANGE==null?0:alreadyMap.WAIT_CHANGE})</a>
			</p>
		</div>
		</div>
	</div>
	<!-- 任务类型 -->
	<input type="hidden" id="task_type" value="WAIT_PROCESS"/>
	<input type="hidden" id="task_status" />
	
	<!-- 搜索条件 -->
	<div>
		<form class="form-inline" method="post" id="orderSearchForm">
			<nav>
				<div class=" form-group">
					<input  id="search_Field2" type="text" class="form-control" placeholder="订单号/处理人" style="width: 300px"/>				
					<button type="button" class="btn btn-default" onclick="reloadAllTask()">搜索</button>
					<button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
				</div>
			</nav>
		</form>
	</div>
	<div>
		<label><font color="red" size="4">刷新倒计时:<span id="countDown">9</span>秒</font></label>
	</div>
</div>


<!-- 任务列表 -->
<div style="margin-top: 10px">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th style="width: 20%">车次信息</th>
				<th style="width: 20%">乘客信息</th>
				<th style="width: 20%">收支信息</th>
				<th style="width: 20%">处理信息</th>
				<th style="width: 20%">供应信息</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="orderTask">
				<tr>
					<td colspan="5">
						<span>企业：${orderTask.orderDto.cName } </span>
						<span>预订人：${orderTask.orderDto.userName }</span>
						<span>订单号：<font color="blue"><a href="/order/trainDetail/${orderTask.orderDto.id }" target="_Blank">${orderTask.orderDto.id }</a></font></span>
						<span>来源：<font color="blue">${orderTask.orderDto.orderOriginType.message}</font></span>
						<span>标签：${orderTask.orderDto.orderShowStatus.message }</span>
						<span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${orderTask.orderDto.createDate }" /></span>
					    <span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span>
				    </td>
				</tr>
				<tr>
					<td>
						<span>
							<fmt:formatDate value="${orderTask.orderDto.fromTime }" pattern="yyyy-MM-dd HH:mm"/> - 
							<fmt:formatDate value="${orderTask.orderDto.arriveTime }" pattern="HH:mm"/></span><br/>
						<span>${orderTask.orderDto.fromStation } - ${orderTask.orderDto.arriveStation }</span><br/>
						<span>${orderTask.orderDto.trainCode } &nbsp;${orderTask.orderDto.seatTypeValue}</span>
					</td>
					<td>
						<span>
							${orderTask.orderDto.formatShowPassengers}
						</span>
					</td>
					<td>
						<span>应收￥${orderTask.orderDto.totalAmount }</span>
						<span>
							<c:choose>
								<c:when test="${not empty orderTask.orderDto.paymentPlanNo }">已收</c:when>
								<c:otherwise>未收</c:otherwise>
							</c:choose>
						</span><br/>
						<span>应付￥${orderTask.orderDto.totalSettlePrice }</span>	
						<span>
							<c:choose>
								<c:when test="${not empty orderTask.orderDto.outOrderId}">已付</c:when>
								<c:otherwise>未付</c:otherwise>
							</c:choose>
						</span><br/>
						<c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
							<span><a onclick="loadIncomeDetail('${orderTask.orderDto.id}')">明细</a></span>
						</c:if>
					</td>
					<td>
						<span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>
						<c:if test="${not empty orderTask.operatorId }">
							<span>处理人：${orderTask.operatorName }</span><br/>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'WAIT_PROCESS' }">
							<span><a onclick="transfer('${orderTask.id}','${orderTask.operatorId }',false)">分配</a></span>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' }">
							<span>处理结果：${orderTask.result }</span>
						</c:if>
					 </td>
					<td>
						<span>
							<a data-trigger="tooltip" data-content="原因：${orderTask.orderDto.orderOperationLog.description }">
								<font color="red">${orderTask.orderDto.orderOperationLog.type.msg }${orderTask.orderDto.orderOperationLog.status.msg }</font>
							</a><br/>

							<span>出票渠道:${issueChannelMap[fn:trim(orderTask.orderDto.lineType)][orderTask.orderDto.issueChannelType]}</span><br/>
							<!-- 有供应商订单信息 -->
							<c:if test="${not empty orderTask.orderDto.outOrderId }">
								<a data-trigger="tooltip" data-content="${orderTask.orderDto.outOrderId }">订单号</a><br/>
								<c:if test="${orderTask.orderDto.lineType eq '1' and orderTask.taskStatus eq 'ALREADY_PROCESSED' and orderTask.orderDto.supplierLiqStatus ne 'CLOSE' and orderTask.orderDto.orderStatus eq 'ISSUED'}">
									<a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id}','${orderTask.orderDto.userId}','${orderTask.operatorId}', true)">线下出票修订</a>
								</c:if>
							</c:if>

						</span>
					</td>
				</tr>
			</c:forEach>		
		</tbody>
	</table>
</div>

<div class="pagin" style="margin-bottom: 20px">
	<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="webpage/train/tmc/js/allTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(function(){
	var status = "${taskStatus}";
	$("#allTaskStatusTab li").each(function(){
		if($(this).attr("data-value")==status){
		
			$(this).addClass("active");
			$(this).siblings().removeClass("active");
			if(status == 'WAIT_PROCESS'){
				 $('#t_all_content .content1').show();
			     $('#t_all_content .content2').hide();
			     $('#t_all_content .content3').hide();
			}else if(status=='PROCESSING'){
				 $('#t_all_content .content1').hide();
				 $('#t_all_content .content2').show();
			     $('#t_all_content .content3').hide();
			}else if(status=='ALREADY_PROCESSED'){
				 $('#t_all_content .content1').hide();
			     $('#t_all_content .content2').hide();
			     $('#t_all_content .content3').show();
			}
			return false;
		}
	});
});
</script>