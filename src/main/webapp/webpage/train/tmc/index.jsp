<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.tem.platform.security.authorize.PermissionUtil"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">
<link rel="stylesheet" href="webpage/train/tmc/css/tmc.css?version=${globalVersion}">
<%-- 只有客服经理能看到全部任务列表 --%>
<% session.setAttribute("jurisdiction", PermissionUtil.checkTrans("COMMON_TMC_SERVICE", "TRAIN_M"));   %>

<ul id="myTab" class="nav nav-tabs" data-trigger="tab">
	<li><a onclick="getMyTask()">我的任务</a></li>
	<c:if test="${sessionScope.jurisdiction ==0}">
		<li><a onclick="getAllTask()">全部任务</a></li>
	</c:if>

	<li><a onclick="getOrderList()">订单查询</a></li>
</ul>
<div class="pane-wrapper">
	<!-- 我的任务 -->
	<div id="myTask">
		<%--<jsp:include page="myTask.jsp"></jsp:include>--%>
	</div>
	<c:if test="${sessionScope.jurisdiction ==0}">
		<!-- 全部任务 -->
		<div id="allTask">
		</div>
	</c:if>

	<!-- 订单查询 -->
	<div id="orderList">
	</div>

	<%--用来判断上次刷新前是选择的那个tab页，很繁琐，无奈之举 --%>
	<input type="hidden" id="myTask_pageIndex"/>
	<input type="hidden" id="myTask_task_status"/>
	<input type="hidden" id="myTask_task_type"/>
	<input type="hidden" id="allTask_pageIndex"/>
	<input type="hidden" id="allTask_task_status"/>
	<input type="hidden" id="allTask_task_type"/>
	<input type="hidden" id="is_flush" value="true"/>

</div>

<!-- 线下模态框 -->
<div id="model1"></div>

<!-- 线下改签模态框 -->
<div id="offLineChange"></div>

<!-- 拒单退款 -->
<div class="modal fade" id="rejectOrder_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
				<h4 class="modal-title">拒单退款</h4>
			</div>
			<div class="modal-body">
				<form  class="form-horizontal" id="rejectOrderForm" role="form">
					<input type="hidden" name="orderId"/>
					<input type="hidden" name="taskId"/>
					<input type="hidden" name="userId"/>
					<h4>请确认未支付给供应商，或支付后供应商同意退款</h4>
					<h4>请输入拒单退款原因:(企业客人可见)</h4>
					<input type="text" class="form-control" id="remark"  name="remark" placeholder="请输入拒单退款原因(至少4个字)"/>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" onclick="submitRejectOrderForm()">拒单退款</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">放弃</button>
			</div>
		</div>
	</div>
</div>

<!-- 线下改签,拒单退款 -->
<div class="modal fade" id="offLineChangeFailModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
				<h4 class="modal-title">拒单退款</h4>
			</div>
			<div class="modal-body">
				<form  class="form-horizontal" id="offLineChangeFailForm" role="form">
					<h4>请确认未支付给供应商，或支付后供应商同意退款</h4>
					<h4>请输入拒单退款原因:(企业客人可见)</h4>
					<input type="text" class="form-control" id="refundReason"  name="refundReason" placeholder="请输入拒单退款原因(至少4个字)"/>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" onclick="offLineChangeFailSubmit()">拒单退款</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">放弃</button>
			</div>
		</div>
	</div>
</div>


<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tab.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tooltip.js?version=${globalVersion}"></script>
<script src="resource/plugins/clipboard/clipboard.min.js?version=${globalVersion}"></script>
<script src="webpage/train/tmc/js/index.js?version=${globalVersion}"></script>
<script type="text/javascript">
	getMyTask();
    //清空定时器
    clearInterval(timerFlag);
</script>