<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="webpage/train/tmc/css/reset.css?version=${globalVersion}" type="text/css">
<link rel="stylesheet" href="webpage/train/tmc/css/orderDetail.css?version=${globalVersion}" type="text/css">
<div class="hoc-order-content l_float">
<input type="hidden" value="${refundOrderDto.orderId }" id="orderId"/>
<input type="hidden" value="${refundOrderDto.id }" id="refundOrderId"/>
<input type="hidden" value="${refundOrderDto.userId }" id="userId"/>
<input type="hidden" value="${taskId}" id="taskId"/>
<input type="hidden" value="${refundOrderDto.refundCount}" id="refundCount"/>
	<div class="hoc-cont-cont">
		<div class="cont-left">
			<p class="cont-left-1">
				<span>
					${refundOrderDto.orderDto.summary}
				</span>
			</p>
			<p class="cont-left-1">
				<span>
					供应商结算金额:${refundOrderDto.orderDto.totalSettlePrice},
				    用户支付总金额:${refundOrderDto.orderDto.totalAmount}
				</span>
			</p>
			
			<c:forEach items="${refundOrderDto.orderDto.passengerList }" var="passenger">
				<c:if test="${passenger.refundOrderId eq refundOrderDto.id}">
					<div class="cont-info">
						<div class="cont-info-label l_float">${passenger.name }</div>
						<div class="cont-info-in l_float">
							<span>${passenger.seatDetail }</span><span>&nbsp;已出票</span>
						</div>
					</div>
				</c:if>
			</c:forEach>
				
			<div class="hoc-cut-off"></div>
			<div class="cont-info">
				<div class="cont-info-label l_float">退票规则</div>
				<div class="cont-info-in l_float">
					<p> 1、开车前15天(不含)以上，不收取退票费；
						2、开车前48小时以上的，退票手续费5%；
						3、开车前25-48小时之间，退票手续费10%；
						4、开车前24小时之内，退票手续费20%；
						5、最终退款以铁路局实退为准，退票手续费最低收取2元；
					</p>
				</div>
			</div>
			<div class="hoc-cut-off"></div>
			<input type="hidden" value="${refundOrderDto.refundCount }" id="refundCount" />
			<form id="priceForm">
				<h5>可退票的乘客</h5>
				<div class="form-table">
					<table>
						<tbody>
							<tr>
								<th>姓名</th>
								<th>供应商退票金额</th>
								<th>服务费</th>
								<th>给客人退款金额</th>
							</tr>
							<c:forEach items="${refundOrderDto.orderDto.passengerList }" var="passenger">
								<c:if test="${passenger.refundOrderId eq refundOrderDto.id}">
									<tr>
										<input type="hidden" value="${passenger.id}" class="id"/>
										<td class="input-num" title="证件号:${passenger.credentialNo},证件类型:${passenger.credentialTypeValue}">${passenger.name}</td>
										<td class="input-num" ><input type="number"  onkeyup="calculation()" class="settlePrice inputNum" ></td>
										<td class="input-num"><input type="number"  onkeyup="calculation()"  class="servicePrice inputNum" ></td>
										<td class="input-num"><span class="refundToPassengerMoney" >0</span></td>
									</tr>
								</c:if>
							</c:forEach>
							<tr>
								<td>退票合计</td>
								<td id="td0">0</td>
								<td id="td1">0</td>
								<td id="td2">0</td>
							</tr>
						</tbody>
					</table>
					<div class="hoc-button-wards">
						<a class="hoc-bt hoc-pass-bt" onclick="aduitPass()">审核通过</a>
						<a class="hoc-bt hoc-refuse-bt" onclick="refuseRefund()">拒绝退票</a>
						<a class="hoc-bt hoc-refuse-bt" onclick="cancel()">暂不处理</a>
					</div>
				</div>
			</form>
		</div>
		<div class="cont-right imgBox">
			<div class="cont-info">
				<div class="cont-info-label l_float">原订单号</div>
				<div class="cont-info-in l_float">
					<p>${refundOrderDto.orderDto.id }</p>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">支付时间</div>
				<div class="cont-info-in l_float">
					<span class="data-1"><fmt:formatDate value="${refundOrderDto.orderDto.paymentDate }" pattern="yyyy.MM.dd HH:mm:ss"/></span> 
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">出票时间</div>
				<div class="cont-info-in l_float">
					<span class="data-1"><fmt:formatDate value="${refundOrderDto.orderDto.lastUpdateDate }" pattern="yyyy.MM.dd HH:mm:ss"/></span>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">预订人</div>
				<div class="cont-info-in l_float">
					<span class="data-1">${refundOrderDto.orderDto.userName }</span> 
					<span class="data-1">${refundOrderDto.orderDto.userMobile }</span>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">出票渠道</div>
				<div class="cont-info-in l_float">

					<p>${refundOrderDto.orderDto.issueChannelType}</p>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">车次</div>
				<div class="cont-info-in l_float">
					<p>${refundOrderDto.orderDto.trainCode }</p>
				</div>
			</div>
			<div class="hoc-cut-off"></div>
			<div class="cont-info">
				<div class="cont-info-label l_float">退单单号</div>
				<div class="cont-info-in l_float">
					<p>${refundOrderDto.id }</p>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">提交时间</div>
				<div class="cont-info-in l_float">
					<span class="data-1"><fmt:formatDate value="${refundOrderDto.applyTime }" pattern="yyyy.MM.dd HH:mm:ss"/></span>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">退票原因</div>
				<div class="cont-info-in l_float">
					<p>${refundOrderDto.reason }</p>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">联系人</div>
				<div class="cont-info-in l_float">
					<span class="data-1">${refundOrderDto.orderDto.contactor.name }</span>
					 <span class="data-1">${refundOrderDto.orderDto.contactor.mobile }</span>
					<span class="data-1">${refundOrderDto.orderDto.contactor.email }</span>
				</div>
			</div>
		</div>
	</div>
</div> 

<script type="text/javascript" src="webpage/train/tmc/js/orderDetail.js?version=${globalVersion}"></script>
