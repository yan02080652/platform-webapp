<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
.tooltip {
  width: 24em;
}
</style>

<!-- 订单状态选项 -->
	<div class="tab" style="margin-bottom: 5px">
		<ul class="fix" id="orderStatus">
			<c:forEach items="${orderStatuslist }" var="orderStatus">
				<c:choose>
					<c:when test="${orderStatus.orderShowStatus eq currentOrderStatus }">
					<li class="active"><span data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.message }(${orderStatus.count })</span></li>
					</c:when>
					<c:otherwise>
						<li ><span data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.message }(${orderStatus.count })</span></li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</div>

<!-- 订单列表 -->

	<table class="table table-bordered" id="orderTable">
		<thead>
			<tr>
				<th style="width: 20%">车次信息</th>
				<th style="width: 20%">乘客信息</th>
				<th style="width: 20%">收支信息</th>
				<th style="width: 20%">处理信息</th>
				<th style="width: 20%">供应信息</th>
			</tr>
		</thead>
		<tbody>
		
		<c:set var="nowDate" value="<%=System.currentTimeMillis()%>"></c:set>
		
			<c:forEach items="${pageList.list }" var="trainOrder">
					<tr>
						<td colspan="5">
							<span>企业：${trainOrder.cName }</span>
							<span>预订人：${trainOrder.userName }</span>
							<span>订单号：<font color="blue"><a href="/order/trainDetail/${trainOrder.id }" target="_Blank">${trainOrder.id }</a></font></span>
							<span>来源：<font color="blue">${trainOrder.orderOriginType.message}</font></span>
							<span>标签：${trainOrder.orderShowStatus.message }</span>
							<span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${trainOrder.createDate }" /></span>
						    <span><a onclick="logList('${trainOrder.id}')">日志</a></span>
						    <c:if test="${trainOrder.orderShowStatus eq 'IN_ISSUE' and (nowDate-trainOrder.lastUpdateDate.time)/(1000*60) > 5}">
						    	&nbsp;&nbsp;
						    	<span><font color="red">订单出票超时！</font></span>
						    </c:if>
					    </td>
					</tr>
					<tr>
					   <td>
							<span>
								<fmt:formatDate value="${trainOrder.fromTime}" pattern="yyyy-MM-dd HH:mm"/> - 
								<fmt:formatDate value="${trainOrder.arriveTime}" pattern="HH:mm"/></span><br/>
							<span>${trainOrder.fromStation } - ${trainOrder.arriveStation }</span><br/>
							<span>${trainOrder.trainCode}  ${trainOrder.seatTypeValue}
								<c:if test="${trainOrder.orderType eq 'grabTickets' and trainOrder.issuedDate eq null}">
                                    &nbsp;&nbsp;&nbsp;<span style="color:red">${trainOrder.orderType.msg}<span>
                                </c:if>
							</span>
						</td>
						<td>
							<span>
								${trainOrder.formatShowPassengers}
							</span>
						</td>
						<td>
							<span>应收￥${trainOrder.totalAmount}</span>
							<span>
								<c:choose>
									<c:when test="${not empty trainOrder.paymentPlanNo }">已收</c:when>
									<c:otherwise>未收</c:otherwise>
								</c:choose>
							</span><br/>
							<span>应付￥${trainOrder.totalAmount }</span>	
							<span>
								<c:choose>
									<c:when test="${not empty trainOrder.outOrderId}">已付</c:when>
									<c:otherwise>未付</c:otherwise>
								</c:choose>
						</span><br/>
							<c:if test="${not empty trainOrder.paymentPlanNo }">
								<span><a onclick="loadIncomeDetail('${trainOrder.id}')">明细</a></span>
							</c:if>
						</td>
						<td>
							<span>
								${trainOrder.taskInfo}<br/>
								<c:if test="${not empty trainOrder.processTagType }">
									${trainOrder.processTagType.message }:
								</c:if>
								${trainOrder.orderStatus.message }
							</span><br/>
							<span>
<%-- 								<c:if test="${trainOrder.processTagType.code eq 1 }"><a onclick="receive()">客服介入</a></c:if> --%>
								<c:if test="${trainOrder.processTagType.code eq 2 }">${trainOrder.taskOperatorName }</c:if>
							</span>
						 </td>
						<td>
							<span>
								<c:if test="${trainOrder.orderOperationLog.status eq 'FAIL' }">
									<a data-trigger="tooltip" data-content="失败原因：${trainOrder.orderOperationLog.description }">
									<font color="red">${trainOrder.orderOperationLog.type.msg }${trainOrder.orderOperationLog.status.msg }</font></a><br/>
								</c:if>

								<span>出票渠道:${issueChannelMap[fn:trim(trainOrder.lineType)][trainOrder.issueChannelType]}</span><br/>
								<c:if test="${not empty trainOrder.outOrderId }">
									<a data-trigger="tooltip" data-content="${trainOrder.outOrderId }">订单号</a><br/>
								</c:if>
							</span>
						</td>
					</tr>
			</c:forEach>
		</tbody>
	</table>
<div class="pagin" style="margin-bottom: 20px">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadOrderList"></jsp:include>
</div>
<script src="webpage/train/tmc/js/orderList.js?version=${globalVersion}"></script>