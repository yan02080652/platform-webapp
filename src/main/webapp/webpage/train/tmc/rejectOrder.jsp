<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="rejectOrder_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            		<h4 class="modal-title">拒单退款</h4>
         </div>
         <div class="modal-body">
         	<input type="hidden" value="${taskId }" id="taskId"/>
         	<input type="hidden" value="${orderId }" id="orderId"/>
         	<input type="hidden" value="${userId }" id="userId"/>
         
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="">拒单退款</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">放弃</button>
         </div>
      </div>
</div>
</div>