//我的任务定时器
var timerFlag ;
//全部任务定时器
var allTask_timer;

//我的任务Tab
function getMyTask(){
    $("#myTask_task_status").val("PROCESSING");
    $("#myTask_task_type").val("");
    $("#myTask_pageIndex").val(1);
    clearInterval(allTask_timer);
    var load_index = layer.load();
	$.ajax({
		type:"get",
		url:"train/tmc/getMyTaskTable",
		data:{},
		success:function(data){
            layer.close(load_index);
			$("#myTask").html(data);
		},
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }
	});
}
//全部任务Tab
function getAllTask(){
    $("#allTask_task_status").val("WAIT_PROCESS");
    $("#allTask_pageIndex").val(1);
    $("#allTask_task_type").val("");
    clearInterval(timerFlag);
    var load_index = layer.load();
	$.ajax({
		type:"get",
		url:"train/tmc/allTask",
		data:{},
		success:function(data){
            layer.close(load_index);
			$("#allTask").html(data);
		},
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }
	});
}
//订单列表Tab
function getOrderList(){
    clearInterval(timerFlag);
    clearInterval(allTask_timer);
    var load_index = layer.load();
	$.ajax({
		type:"get",
		url:"train/tmc/orderList",
		data:{},
		success:function(data){
            layer.close(load_index);
			$("#orderList").html(data);
		},
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }
	});
}

//订单操作日志
function logList(orderId){
	layer.open({
		  type: 2,
		  area: ['800px', '500px'],
		  title: "订单日志",
		  closeBtn: 1,
		  shadeClose: true,
		 // shade :0,
		  content: "train/tmc/getOrderLog?orderId="+orderId,
		});
}

//加载收入明细
function loadIncomeDetail(orderId){
	layer.open({
		  type: 2,
		  area: ['600px', '400px'],
		  title: "收入明细",
		  closeBtn: 1,
		  shadeClose: true,
		 // shade :0,
		  content: "train/tmc/getIncomeDetail?orderId="+orderId,
		});
}


//复制ss指令
var clipboard = new Clipboard('.ss');
clipboard.on('success', function(e) {
    layer.msg("复制成功")
});
clipboard.on('error', function(e) {
    layer.msg("复制失败")
});