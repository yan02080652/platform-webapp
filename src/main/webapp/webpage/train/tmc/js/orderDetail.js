
//审核通过
var passengersList = [];
function aduitPass(){
	if (!validateResult()) {
		return;
	}

    var supperRefundAmount = parseFloat($("#td0").text());
    var refundTotalAmount = parseFloat($("#td2").text());
	
	//毛利
	var profit =  supperRefundAmount - refundTotalAmount;

    if(profit < 0){
        layer.alert("价格信息有误，请更正！")
        return;
    }

    var orderId = $("#orderId").val();
    var refundOrderId = $("#refundOrderId").val();
    var userId = $("#userId").val();
    var taskId = $("#taskId").val();
    var refundCount = $("#refundCount").val();
    var servicePrice = parseFloat($("#td1").text());


	
	layer.confirm('是否通过退票？退单后毛利：'+profit+'元', { btn: ['确认','取消'],title:"提示"},
		function(index){//确认
            layer.close(index);
			var c_index = layer.load();
			var refundOrderDto = {
            	orderId:orderId,
                id:refundOrderId,
                supperRefundAmount:supperRefundAmount,
                servicePrice:servicePrice,
                refundTotalAmount:refundTotalAmount,
                userId:userId,
                refundCount:refundCount,
                passengersList : passengersList
            };
            var task = taskId;
			var from = {
                refundOrderDto:refundOrderDto,
                taskId:task
			};
			$.ajax({
				type:"post",
				url:"train/tmc/refundAduitPass",
				data:JSON.stringify(from),
                contentType: "application/json",
                dataType: "json",
				success:function(data){
					layer.close(c_index);
					if(data.code == '0'){
						parent.layer.msg("退票任务完成：方式(审核通过)",{icon:1,offset:'t',time:1000});
					}else{
						parent.layer.msg("系统退款失败，请稍后重试！",{icon:2,offset:'t',time:1000});
					}
					parent.submitAjax();
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
					
				},error:function () {
                    layer.close(c_index);
                    layer.msg('系统繁忙！');
                }
			});
		}, 
		function(){//取消
		});
}

//拒绝退票
function refuseRefund (){
	var orderId = $("#orderId").val();
	var refundOrderId = $("#refundOrderId").val();
	var taskId = $("#taskId").val();
	layer.prompt({
		  formType: 2,//输入框1为input，2为文本域
		  title: '请填写拒绝退票原因',
		}, function(value, index, elem){
			if($.trim(value).length>0){
                var c_index = layer.load();
				$.ajax({
					type:"post",
					url:"train/tmc/refundAduitNoPass",
					data:{
						orderId:orderId,
						id:refundOrderId,
						refuseReason:$.trim(value),
						taskId:taskId,
					},
					success:function(data){
                        layer.close(c_index);
						if(data.code == '0'){
							parent.layer.msg("退票任务完成：方式(拒绝退票)",{icon:1,offset:'t',time:1000});
						}else{
							parent.layer.msg("系统错误",{icon:2,offset:'t',time:1000});
						}
						parent.submitAjax();
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
					}
				});
			}
		});
}

//暂不处理
function cancel(){
	parent.layer.closeAll();
}


function calculation(){
	var settlePrice = 0;
   $(".settlePrice").each(function (i,item) {
       settlePrice += parseFloat($(this).val() || 0);
   });
    var servicePrice = 0;
    passengersList = [];//清空乘客数据
    $(".servicePrice").each(function (i,item) {
        var service = parseFloat($(this).val() || 0);
        servicePrice += service;
        //计算给客户退的钱
        var settle = parseFloat($(".settlePrice").eq(i).val() || 0);
        var passengerMoney = settle - service;
        $(".refundToPassengerMoney").eq(i).text(passengerMoney);
        //添加乘客
        var id = $(".id").eq(i).val();
        var passenger = {id:id,amount:passengerMoney};
        passengersList.push(passenger);

    });
    var refundToPassengerMoney = 0;
    $(".refundToPassengerMoney").each(function (i,item) {
        refundToPassengerMoney += parseFloat($(this).text() || 0);
    });

	$("#td0").text(settlePrice);
    $("#td1").text(servicePrice);
    $("#td2").text(refundToPassengerMoney);

}

function errorMsg(ele, msg) {
    layer.tips(msg, ele, {
        tips: [1, '#3595CC'],
        time: 4000
    });
};

function validateResult() {
	var result = true;
	$(".settlePrice").each(function () {
		if ($(this).val().trim() == '') {
            errorMsg($(this), "供应商退票金额不能为空");
            result = false;
			return false;
		}
    });
	if (!result) {
		return result;
	}
    var result = true;
    $(".servicePrice").each(function () {
        if ($(this).val().trim() == '') {
            errorMsg($(this), "服务费不能为空");
            result = false;
            return false;
        }
    });
    if (!result) {
        return result;
    }
	return result;
}


