$(function() {
	$("#orderStatus li").click(function() {

        $(this).siblings().removeClass("active");
		if ($(this).hasClass("active")) {
			//$(this).removeClass("active");
		} else {
			submitForm(null,$(this).find("span").attr("data-value"));
		}
	});
})

//重置查询条件
function resetSearchForm(){
	$('#searchForm')[0].reset();
	$("#orderStatus li").each(function(){
		$(this).removeClass("active");
	})
	// 再次请求
	submitForm();
}

//分页函数
function reloadOrderList(pageIndex){
	submitForm(pageIndex,$("#orderTable").find("li[class='active']").find("span").attr("data-value"));
}

function submitForm(pageIndex,orderStatus){

	var dateType = $("#searchField1").val();
	var startDate = $("#startDate3").val();
	var endDate = $("#endDate3").val();
	var field1 = $("#searchField2").val().trim();
	var field2 = $("#searchField3").val().trim();
	var field3 = $("#searchField4").val().trim();
	var field4 = $("#searchField5").val().trim();

	$.ajax({
		type:"post",
		url:"train/tmc/getOrderTable",
		data:{	dateType:dateType,
				startDate1:startDate==''?startDate:new Date(startDate).getTime(),
				endDate1:endDate==''?endDate:new Date(endDate).getTime(),
				passengerOrContent:field1,
				orderNumber:field2,
				partnerName:field3,
				stationName : field4,
				orderShowStatus:orderStatus,
				pageIndex:pageIndex
			},
		success:function(data){
			$("#orderTable").html(data);
			//添加选中阴影
            $("span[data-value='"+orderStatus+"']").parent().addClass("active");
		}
	});
}

//TODO 客服介入
function receive(){
	layer.alert("客服介入")
}

