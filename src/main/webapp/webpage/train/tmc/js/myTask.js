// 检查当前操作人是否为任务领取人
function checkOperator(operatorId){
	var lawful;
	$.ajax({
		url:"train/tmc/checkOperator",
		type:"GET",
		async:false,
		data:{operatorId:operatorId},
		success:function(data){
			lawful =  data;
		}
	});
	return lawful;
}


function reloadMyTaskTable(pageIndex){
    var taskStatus = $("#myTaskStatusTab .active").attr("data-value");
    var taskType = $("#currentTaskType").val();
    submitAjax(pageIndex,taskStatus,taskType);

}
function submitAjax(pageIndex,taskStatus,taskType){
    $("#currentTaskType").val(taskType);
    $("#myTask_pageIndex").val(pageIndex);
    $("#myTask_task_status").val(taskStatus);
    $("#myTask_task_type").val(taskType);

    $.ajax({
        type:"post",
        url:"train/tmc/getMyTaskTable",
        data:{
            taskStatus:taskStatus,
            pageIndex:pageIndex,
            taskType:taskType
        },
        success:function(data){
            $("#myTask").html(data);
        },
        error:function(){
            layer.msg("系统繁忙，请刷新重试",{icon:2,offset:'t',time:1000})
        }
    });
}

//显示Model
function showModal(url, data, callback) {
	$('#model1').load(url, data, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function getOffLineModel(orderId, taskId, operatorId) {
    if (!checkOperator(operatorId)) {
     	return;
    }
    $("#offLineChange").load(`train/tmc/getChangeData?orderId=${orderId}&taskId=${taskId}`,
        function (context, state) {
            if ('success' == state) {
                $('#offLineChangeModel').modal({
                    backdrop: 'static'
                });
            }
	});
}
function getTicketData(orderId,taskId,flag,operatorId,isRevise){
    showModal("train/tmc/getTicketData",{orderId:orderId,taskId:taskId,flag:flag,isRevise:isRevise},function(data){
        $('#ticket_model').modal({
            backdrop:"static"
        });
    });
}

function errorMsg(ele, msg) {
    layer.tips(msg, ele, {
        tips: [1, '#3595CC'],
        time: 4000
    });
};

function getOffLineChangeFail(orderId, taskId, operatorId) {
    var lawful = checkOperator(operatorId);
    if(!lawful){
        layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
        return;
    }
    $("#offLineChangeFailModel").modal({
        backdrop:"static"
    });
    initOffLineChangeForm(orderId, taskId, operatorId);
}
function offLineChangeFailSubmit() {
    $("#offLineChangeFailForm").submit();
}
var offLineChangeFail = {};
function initOffLineChangeForm(orderId,taskId,userId){
    offLineChangeFail.orderId = orderId;
    offLineChangeFail.taskId = taskId;

    $("#offLineChangeFailForm").validate(
        {
            rules : {
                refundReason : {
                    required : true,
                    minlength : 4,
                },
            },
            submitHandler : function(form) {
                $.ajax({
                    type : "POST",
                    url : "train/tmc/offLineChange",
                    data : {
                        orderId : offLineChangeFail.orderId,
                        taskId : offLineChangeFail.taskId,
                        success : false,
                        refundReason : $("#refundReason").val(),
                    },
                    async : false,
                    success : function(data) {
                        if(data.code == '0'){
                            $('#offLineChangeFailModel').modal('hide');
                            layer.msg("线下改签：拒单退款成功",{icon:1,offset:'t',time:1000})
                            setTimeout(function(){
                                submitAjax();
                            },200);
                        }else{
                            layer.msg(data.msg, {icon:2,offset:'t',time:1000})
                        }
                    },
					error : function(request) {
                        layer.msg("请求失败，请刷新重试",{icon:2});
                    }
                });
            },
            errorPlacement : setErrorPlacement,
            success : validateSuccess,
            highlight : setHighlight
        });
}

// 线下改签
function offLineChangeSubmit() {
	function validate() {
        if ($("#issueChannel").val().trim() == '') {
            errorMsg($("#issueChannel"), "供应商名称不能为空");
            return false;
        }
        if ($("#outOrderId").val().trim() == '') {
            errorMsg($("#outOrderId"), "供应商订单ID不能为空");
            return false;
        }
        if ($("#balance").val().trim() == '') {
            errorMsg($("#balance"), "改签订单差额不能为空");
            return false;
        }
        let result = true;
        $(".trainBox").each(function (index,item) {
            if ($(this).val().trim() == '') {
                errorMsg($(this), "车厢号不能为空");
                result = false;
                return false;
            }
        });
        $(".seatNo").each(function (index,item) {
            if ($(this).val().trim() != '' && $('.isNoSeat').eq(index).is(':checked')) {
                errorMsg($('.isNoSeat').eq(index), "座位号和无座不能同时存在");
                result = false;
                return false;
            }
        });
        return result;
    }
    if(!validate()) {
		return;
	}
    let loading = layer.load();
    $.ajax({
        type:"post",
        url:"train/tmc/offLineChange",
        data: $('#offLineChangeForm').serialize(),
        success:function(data){
            layer.close(loading);
            if (data.code == '0') {
                layer.msg('线下改签成功');
                $('#offLineChangeModel').modal('hide');
                submitAjax();
			} else {
                layer.msg(data.msg,{icon:2,offset:'t',time:1000})
			}
        },
		error:function () {
            layer.close(loading);
            layer.msg('系统繁忙');
        }
    });
}

//线下出票
function submitTicketFrom(){
     function vaidateResult() {
        if ($("#issueChannel").val().trim() == '') {
            errorMsg($("#issueChannel"), "供应商名称不能为空");
            return false;
        }
        if ($("#outOrderId").val().trim() == '') {
            errorMsg($("#outOrderId"), "供应商订单ID不能为空");
            return false;
        }
        if ($("#totalPiace").val().trim() == '') {
            errorMsg($("#totalPiace"), "供应商结算总价不能为空");
            return false;
        }
        let result = true;
        $(".trainBox").each(function (index,item) {
            if ($(this).val().trim() == '') {
                errorMsg($(this), "车厢号不能为空");
                result = false;
                return false;
            }
        });
        $(".seatNo").each(function (index,item) {
            if ($(this).val().trim() != '' && $('.isNoSeat').eq(index).is(':checked')) {
                errorMsg($('.isNoSeat').eq(index), "座位号和无座不能同时存在");
                result = false;
                return false;
            }
        });
        return result;
     }
     if (!vaidateResult()) {
     	return;
	 }
	// 提交
	var taskId = $("#taskId").val();
	var orderId = $("#orderId").val();
	var ticketNo = $("#ticketNo").val();
	var issueChannel = $("#issueChannel").val();
	var outOrderId = $("#outOrderId").val();
	var totalPiace = $("#totalPiace").val();
    var passengerDtos = new Array();
    var ids = $(".id");
    var trainBoxs = $(".trainBox");
    var seatNos = $(".seatNo");
    var isNoSeats = $(".isNoSeat");
    var passengerName = $(".passengerName");
    var isRevise = $("#isRevise").val();
    $("#seatTable tbody tr").each(function (i,item) {
		var p = {
			id: ids.eq(i).val(),
			trainBox: trainBoxs.eq(i).val(),
			seatNo: seatNos.eq(i).val(),
			isNoSeat: isNoSeats.eq(i).is(":checked"),
            name: passengerName.eq(i).text()
		};
        passengerDtos.push(p);
    })

    var formObj = {
		orderId:orderId,
        ticketNo:ticketNo,
		taskId:taskId,
		issueChannel:issueChannel,
		outOrderId:outOrderId,
		totalPiace:totalPiace,
        passengerDtos:passengerDtos,
        isRevise:isRevise
    };

	$.ajax({
		type:"post",
		url:"train/tmc/saveTicket",
		data:JSON.stringify(formObj),
		contentType: "application/json",
		datatype:"json",
		success:function(data){
			$('#ticket_model').modal('hide');
			if(data.code == '0'){
				var msg = isRevise ? '修订成功' : '待出票任务完成，处理方式：线下出票';
				layer.msg(msg, {icon:1,offset:'t',time:1000});
				setTimeout(function(){
					submitAjax();
				},200);
			}else{
				layer.msg(data.msg, {icon:2,offset:'t',time:1000})
			}
		}
	});

}

//向供应商19e重新支付
function payAgainForSupplier(orderId,taskId,operatorId){
	var lawful = checkOperator(operatorId);
	if(lawful){
		layer.confirm('请确保账户余额充足，确定重新支付?', function(index){
			layer.close(index);
			var loadIndex = layer.load();
			$.ajax({
				url : "train/tmc/payAgainForSupplier",
				data : {
					orderId : orderId,
					taskId : taskId
				},
				success : function(data) {
					layer.close(loadIndex);
					if (data.code == '0') {
						layer.msg("重新支付成功");
                        setTimeout(function(){
                            submitAjax();
                        }, 200);
					} else {
                        layer.msg(data.msg,{icon:2,offset:'t',time:1000});
                    }
				},
				error : function() {
					layer.close(loadIndex);
					layer.msg("系统错误，请稍后重试！",{icon:2,offset:'t',time:1000});
					window.reload();
				}
			});
		});
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
}

// 拒单退款
function rejectOrder(orderId,taskId,userId,operatorId){
    var lawful = checkOperator(operatorId);
    if(lawful){
        $('#rejectOrderForm')[0].reset();
        $("#rejectOrder_model").modal({
            backdrop:"static"
        });
        initRejectOrderForm(orderId,taskId,userId);

        $("#rejectOrderForm input[name='orderId']").val(orderId);
        $("#rejectOrderForm input[name='taskId']").val(taskId);
        $("#rejectOrderForm input[name='userId']").val(userId);
    }else{
        layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
    }
}
function initRejectOrderForm(orderId, taskId, userId){

    $("#rejectOrderForm").validate(
        {
            rules : {
                remark : {
                    required : true,
                    minlength : 4,
                },
            },
            submitHandler : function(form) {

                var orderId = $("#rejectOrderForm input[name='orderId']").val();
                var taskId = $("#rejectOrderForm input[name='taskId']").val();
                var userId = $("#rejectOrderForm input[name='userId']").val();
                var remark = $("#rejectOrderForm input[name='remark']").val();

                $.ajax({
                    type : "POST",
                    url : "train/tmc/rejectOrder",
                    data : {
                        orderId:orderId,
                        taskId:taskId,
                        userId:userId,
                        remark:remark
                    },
                    async : false,
                    error : function(request) {
                        layer.msg("请求失败，请刷新重试",{icon:2})
                        $('#rejectOrder_model').modal('hide');
                    },
                    success : function(data) {
                        $('#rejectOrder_model').modal('hide');
                        if(data.code == '0'){
                            layer.msg("待出票任务完成，处理方式：拒单退款",{icon:1,offset:'t',time:1000})
                            setTimeout(function(){
                                submitAjax();
                            },200);
                        }else{
                            layer.msg(data.msg,{icon:2,offset:'t',time:1000});
                        }
                    }
                });
            },
            errorPlacement : setErrorPlacement,
            success : validateSuccess,
            highlight : setHighlight
        });
}
function submitRejectOrderForm(){
	$("#rejectOrderForm").submit();
}

/**
 * 任务交接
 * 注：checkTrans boolean类型,普通客服任务交接需要判断是否为本人，传true，客服经理通过全部任务列表分配不需要判断,传false
 * @param taskId
 * @param operatorId
 * @param checkTrans
 */
function transfer(taskId,operatorId,checkTrans){
	var lawful = true;
	if(checkTrans){
		lawful = checkOperator(operatorId);
	}
	if(lawful){
		TR.select('user',{type:'me'},function(data){
			$.ajax({
				type:"post",
				url:"train/tmc/transfer",
				data:{
					taskId:taskId,
					operatorId:operatorId,
					userId:data.id,
					userName:data.fullname
				},
				success:function(rs){
					if(rs){
						layer.msg("任务已转交给"+data.fullname,{icon:1,offset:'t',time:1000})
						setTimeout(function(){
							submitAjax();
						},200);
					}else{
						layer.msg("系统错误，请刷新重试！",{icon:2,offset:'t',time:1000})
					}
				}
			});
		});
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
}

//退票审核
function ticketAduit(refundOrderId,taskId,operatorId){
	var lawful = checkOperator(operatorId);
	if(lawful){
		layer.open({
			type: 2,
			area: ['1000px', '600px'],
			title: "退票审核",
			closeBtn: 1,
			shadeClose: false,
			//shade :0,
			content: "train/tmc/getOrderDetail?refundOrderId="+refundOrderId+"&taskId="+taskId,
		});
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
}

function delay(taskId,delay,operatorId){
	var lawful = checkOperator(operatorId);
	if(lawful){
		if(delay != -1){
			$.ajax({
				url:"train/tmc/updateMaxProcessDate",
				post:"get",
				data:{taskId:taskId,delay:delay},
				success:function(){
					layer.msg("任务延时成功!",{icon:1,offset:'t',time:1000})
					submitAjax();
				}
			});
		}
	}else{
		layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
	}
	
}

function tmcPayAgain(orderId, taskId, operatorId) {
    if (!checkOperator(operatorId)) {
        layer.msg("当前操作人不是任务领取人!");
        return;
	}
    let loading = layer.load();
    $.ajax({
        url:"train/tmc/tmcPayAgain",
        post:"get",
        data:{orderId:orderId,taskId:taskId},
        success:function(data){
            layer.close(loading);
        	if (data.code == '0') {
                layer.msg("代扣成功!",{icon:1, offset:'t', time:1000});
                submitAjax();
			} else {
                layer.msg(data.msg, {icon:2, offset:'t', time:1000});
			}
        },
		error:function(){
            layer.close(loading);
            layer.msg("系统繁忙!",{icon:2, offset:'t', time:1000})
            submitAjax();
		}

    });
}
function finishTask(taskId, operatorId) {
    var lawful = checkOperator(operatorId);
    if(!lawful){
        layer.msg("非法操作！",{icon: 2,offset:'t',time:1000});
        return;
    }

    let loading = layer.load();
    $.ajax({
        url: "train/tmc/finishTask",
        post: "get",
        data: {taskId: taskId},
        success: function(data) {
            layer.close(loading);
            if (data.code == '0') {
                layer.msg("任务完成!",{icon:1, offset:'t', time:1000});
                submitAjax();
            } else {
                layer.msg(data.msg, {icon:2, offset:'t', time:1000});
            }
        },
        error:function(){
            layer.close(loading);
            layer.msg("系统繁忙!",{icon:2, offset:'t', time:1000})
            submitAjax();
        }
    });

}


