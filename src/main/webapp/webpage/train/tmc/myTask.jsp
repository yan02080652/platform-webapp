<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
.tooltip {
  width: 24em;
}
</style>
<!-- 状态栏 -->
<input type="hidden" id="currentTaskType" />
<div>
	<!-- 左边状态 -->
	<div class="tab pull-left">
            <ul class="fix" id="myTaskStatusTab">
                <li class="tab1 active" data-value="PROCESSING" onclick="submitAjax(null,'PROCESSING')">处理中(${processingMap.PROCESSING==null?0:processingMap.PROCESSING})</li>
                <li class="tab2" data-value="ALREADY_PROCESSED" onclick="submitAjax(null,'ALREADY_PROCESSED')">近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})</li>
            </ul>
		<div id="t_my_content">
            <div class="content1">
                <p>
               		<a onclick="submitAjax(null,'PROCESSING','WAIT_TICKET')">待出票(${processingMap.WAIT_TICKET==null?0:processingMap.WAIT_TICKET })</a> &nbsp;|&nbsp;
                	<a onclick="submitAjax(null,'PROCESSING','WAIT_URGE')">待催单(${processingMap.WAIT_URGE==null?0:processingMap.WAIT_URGE})</a> &nbsp;|&nbsp;
                	<a onclick="submitAjax(null,'PROCESSING','REFUND_TICKET_AUDIT')">退审核(${processingMap.REFUND_TICKET_AUDIT==null?0:processingMap.REFUND_TICKET_AUDIT})</a> &nbsp;|&nbsp;
                	<a onclick="submitAjax(null,'PROCESSING','WAIT_REFUND')">待退款(${processingMap.WAIT_REFUND==null?0:processingMap.WAIT_REFUND})</a> &nbsp;|&nbsp;
					<a onclick="submitAjax(null,'PROCESSING','WAIT_CHANGE')">待改签(${processingMap.WAIT_CHANGE==null?0:processingMap.WAIT_CHANGE})</a>
                </p>
            </div>
            <div class="content2">
                <p>
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_TICKET')">出票(${alreadyMap.WAIT_TICKET==null?0:alreadyMap.WAIT_TICKET })</a> &nbsp;|&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_URGE')">催单(${alreadyMap.WAIT_URGE==null?0:alreadyMap.WAIT_URGE})</a>&nbsp;|&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','REFUND_TICKET_AUDIT')">退审核(${alreadyMap.REFUND_TICKET_AUDIT==null?0:alreadyMap.REFUND_TICKET_AUDIT})</a>&nbsp;|&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_REFUND')">退款(${alreadyMap.WAIT_REFUND==null?0:alreadyMap.WAIT_REFUND})</a>&nbsp;|&nbsp;
					<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_CHANGE')">改签(${alreadyMap.WAIT_CHANGE==null?0:alreadyMap.WAIT_CHANGE})</a>
                </p>
            </div>
		</div>
     </div>

</div>


<!-- 任务列表 -->
<div>
	<table class="table table-bordered" >
		<thead>
			<tr>
				<th style="width: 20%">车次信息</th>
				<th style="width: 20%">乘客信息</th>
				<th style="width: 20%">收支信息</th>
				<th style="width: 20%">处理信息</th>
				<th style="width: 20%">供应信息</th>
			</tr>
		</thead>
		<tbody id="myTaskTbody">
			<c:forEach items="${pageList.list }" var="orderTask" varStatus="vs">
				<tr>
					<td colspan="5">
						<span>企业：${orderTask.orderDto.cName }</span>
						<span>预订人：${orderTask.orderDto.userName }</span>
						<span>订单号：<font color="blue"><a href="/order/trainDetail/${orderTask.orderDto.id }" target="_Blank">${orderTask.orderDto.id }</a></font></span>
						<span>来源：<font color="blue">${orderTask.orderDto.orderOriginType.message}</font></span>
						<span>标签：${orderTask.orderDto.orderShowStatus.message }</span>
						<span> 下单时间：<font color="blue"><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${orderTask.orderDto.createDate }" /></font></span>
					    <span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span>
					    <span>任务创建时间:<font color="blue"><fmt:formatDate value="${orderTask.createDate }" pattern="yyyy-MM-dd HH:mm:ss"/></font></span>
					    <span hidden><font color="red">任务超时</font></span>
					    <input class="proDate" type="hidden" value='<fmt:formatDate value="${orderTask.maxProcessDate }" pattern="yyyy-MM-dd HH:mm:ss"/>'>
					    <c:if test="${orderTask.taskStatus == 'PROCESSING' }">
						    <label>任务延时</label>
						    <select  onchange="delay('${orderTask.id }',$(this).val(),'${orderTask.operatorId }')">
						    	<option value="-1">--
						    	<option value="5">5分钟
						    	<option value="15">15分钟
						    	<option value="60">1小时
						    	<option value="1440">1天
						    </select>
					    </c:if>
				    </td>
				</tr>
				<tr>
					<td>
						<span>
							<fmt:formatDate value="${orderTask.orderDto.fromTime }" pattern="yyyy-MM-dd HH:mm"/> - 
							<fmt:formatDate value="${orderTask.orderDto.arriveTime }" pattern="HH:mm"/></span><br/>
						<span>${orderTask.orderDto.fromStation } - ${orderTask.orderDto.arriveStation }</span><br/>
						<span>${orderTask.orderDto.trainCode } &nbsp;${orderTask.orderDto.seatTypeValue}</span>
					</td>
					<td>
						<span>
							${orderTask.orderDto.formatShowPassengers}
						</span>
					</td>
					<td>
						<span>应收￥${orderTask.orderDto.totalAmount }</span>
						<span>
							<c:choose>
								<c:when test="${not empty orderTask.orderDto.paymentPlanNo }">已收</c:when>
								<c:otherwise>未收</c:otherwise>
							</c:choose>
						</span><br/>
						<span>应付￥${orderTask.orderDto.totalSettlePrice }</span>	
						<span>
							<c:choose>
								<c:when test="${not empty orderTask.orderDto.outOrderId}">已付</c:when>
								<c:otherwise>未付</c:otherwise>
							</c:choose>
						</span><br/>
						<c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
							<span><a onclick="loadIncomeDetail('${orderTask.orderDto.id}')">明细</a></span>
						</c:if>
					</td>
					<td>
						<span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>
						<span>处理人：${orderTask.operatorName }</span><br/>
						<c:if test="${orderTask.taskStatus eq 'PROCESSING' }">
							<span>
								<a onclick="transfer('${orderTask.id}','${orderTask.operatorId }',true)">交接</a> | 

								<!-- 待出票 -->
								<c:if test="${orderTask.taskType eq 'WAIT_TICKET'}">
									 <c:if test="${!orderTask.orderDto.manualOrder}">
										<a onclick="payAgainForSupplier('${orderTask.orderDto.id}','${orderTask.id}','${orderTask.operatorId}')" title="tem账户余额不足,客服充值确认后,方可操作">重新支付</a> |
									 </c:if>
								   	 <a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id}','${orderTask.orderDto.userId}','${orderTask.operatorId}',false)">线下出票</a>  |
									 <a onclick="rejectOrder('${orderTask.orderDto.id}','${orderTask.id}','${orderTask.orderDto.userId}','${orderTask.operatorId}')">拒单退款</a>
								</c:if>
								<!-- 待催单 -->
								<c:if test="${orderTask.taskType eq 'WAIT_URGE'}">
									 <%--<a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id}','${orderTask.orderDto.userId}','${orderTask.operatorId}',false)">线下出票</a>  |
									 <a onclick="rejectOrder('${orderTask.orderDto.id}','${orderTask.id}','${orderTask.orderDto.userId}','${orderTask.operatorId}')">拒单退款</a>  |--%>
									 <a onclick="finishTask('${orderTask.id}', '${orderTask.operatorId}')">已完成</a>
								</c:if>
								<!-- 待退款 -->
								<c:if test="${orderTask.taskType eq 'WAIT_REFUND'}">
									<a onclick="ticketAduit('${orderTask.refundOrderId}','${orderTask.id}','${orderTask.operatorId}')">审核</a> 
								</c:if>
								<!-- 待改签 -->
								<c:if test="${orderTask.taskType eq 'WAIT_CHANGE'}">
									<c:if test="${orderTask.ticketType eq 'ONLINE'}">
										<a onclick="tmcPayAgain('${orderTask.orderDto.id}','${orderTask.id}', '${orderTask.operatorId}')">再次代扣</a> |
									</c:if>
									<a onclick="getOffLineModel('${orderTask.orderDto.id}','${orderTask.id}', '${orderTask.operatorId}')">线下改签</a>  |
									<a onclick="getOffLineChangeFail('${orderTask.orderDto.id}', '${orderTask.id}', '${orderTask.operatorId}')">拒单退款</a>
								</c:if>
							 </span>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' }">
							<span>处理结果：${orderTask.result}</span>
						</c:if>
					 </td>
					<td>
					 <c:if test="${orderTask.taskType ne 'WAIT_CHANGE' }">
						<span>
							<a data-trigger="tooltip" data-content="原因：${orderTask.orderDto.orderOperationLog.description }">
								<font color="red">${orderTask.orderDto.orderOperationLog.type.message }${orderTask.orderDto.orderOperationLog.status.message }</font>
							</a><br/>

							<span>出票渠道:${issueChannelMap[fn:trim(orderTask.orderDto.lineType)][orderTask.orderDto.issueChannelType]}</span><br/>
							
							<!-- 有供应商订单信息 -->
							<c:if test="${not empty orderTask.orderDto.outOrderId }">
								<a data-trigger="tooltip" data-content="${orderTask.orderDto.outOrderId }">订单号</a><br/>
							</c:if>
						</span>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>


</div>


<div class="pagin" style="margin-bottom: 22px">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="webpage/train/tmc/js/myTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(function(){
    var status = "${taskStatus}";
    $("#myTaskStatusTab li").each(function(){
        if($(this).attr("data-value") == status){

            $(this).addClass("active");
            $(this).siblings().removeClass("active");
            if(status=='PROCESSING'){
                $('#t_my_content .content1').show();
                $('#t_my_content .content2').hide();
            }else if(status=='ALREADY_PROCESSED'){
                $('#t_my_content .content1').hide();
                $('#t_my_content .content2').show();
            }
            return false;
        }
    });
});

</script>