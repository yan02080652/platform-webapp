<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!-- 搜索条件 -->
<style>
#orderTable .tab li {
	height: 40px;
	line-height: 40px;
	width: 100px;
	color: #3e3a3d;
	float: left;
	text-align:center;
	cursor: pointer;
}
</style>
<div>
	<form class="form-inline" id="searchForm">
			<div class=" form-group">
				<select class="form-control" id="searchField1" >
					<option value="CREATE_ORDER_DATE">下单时间</option>
					<option value="TAKE_OFF_DATE">出发时间</option>
				</select>
				<input  id="startDate3" class="form-control  Wdate" type="text" onclick="WdatePicker({maxDate:'#F{$dp.$D(\'endDate3\')}'})" style="width: 120px"/> --
				<input  id="endDate3" class="form-control  Wdate" type="text" onclick="WdatePicker({minDate:'#F{$dp.$D(\'startDate3\')}'})" style="width: 120px"/> 
				<input  id="searchField2" type="text" class="form-control" style="width: 139px;" placeholder="姓名/手机号" />
				<input  id="searchField3" type="text" class="form-control" placeholder="订单号/供应商订单号" />
				<input  id="searchField4" type="text" class="form-control" placeholder="企业名称/TMC" />
				<input  id="searchField5" type="text" class="form-control" placeholder="出发/到达车站名称" />
				<button type="button" class="btn btn-default" onclick="submitForm()">搜索</button>
				<button type="button" class="btn btn-default" onclick="resetSearchForm()">重置</button>
			</div>
	</form>
</div>

<div style="margin-top: 10px" id="orderTable">
	<%@ include file="orderTable.jsp" %>	
</div>
