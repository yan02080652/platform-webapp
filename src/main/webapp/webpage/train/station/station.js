$(function(){
	reloadStation();
})

function synStationData() {
    layer.confirm('确认更新车站数据吗?',function (index) {
        layer.close(index);
        var loading = layer.load();
        var name = station_names || '';
        $.ajax({
            url: "train/station/batchInsert",
            type: "post",
            data: JSON.stringify(getStationList(name)),
            contentType: "application/json",
            datatype: "json",
            async: true,
            success: function (data) {
                layer.close(loading);
                if (data) {
                    location.href = "/train/station";
                } else {
                    $.alert('系统繁忙,请稍后重试');
                }

            },error:function () {
                layer.close(loading);
                $.alert('系统繁忙,请稍后重试');
            }
        });
    });

}

var cityList;
$.ajax({
    url: "train/station/getCitysList",
    type: "post",
    datatype: "json",
    async: true,
    success: function (data) {
        cityList = data;

    },error:function () {
        layer.close(loading);
        $.alert('系统繁忙,请稍后重试');
    }
});



function getStationList(str) {
	if (str == '') {
        $.alert('请求12306车站信息有误!');
        return;
	}
    function removeAllSpace(str) {
        return str.replace(/\s+/g, "");
    }
    var arrays = str.split("@");
    var stationList = [];
    for (var i = 0; i < arrays.length; i++) {
        if (!arrays[i] || arrays[i].trim() == '') {
            continue;
        }
        //line (bjb|北京北|VAP|beijingbei|bjb|0)
        //station(Long id, String nameCn, String nameCnJianpin, String nameCnQuanpin, String belongCityName, String stationCode)
        var line = arrays[i].split('|');
        var station = {
            nameCn : removeAllSpace(line[1]),
            nameCnSimple : removeAllSpace(line[4]),
            nameEn : removeAllSpace(line[3]),
            cityName : '',
            stationCode : removeAllSpace(line[2])
		};
        stationList.push(station);
	}

    return stationList;
}

function reloadStation(pageIndex){
	var loadingList = layer.load();
	//加载列表
	$.post('train/station/list',
		{
			pageIndex:pageIndex,
            name : $('#name').val(),
            belongCity : $('#belongCity').val(),
            cityLinkEnum : $("option:selected").attr('code')
		},function(data){
            layer.close(loadingList);
			$("#rule_Table").html(data);
	});
}


function search_station(){
    reloadStation();
}
//转到添加或修改页面
function getStation(id){
	$.post("train/station/getStation",{id:id},function(data){
		$("#ruleDetail").html(data);
		$("#ruleMain").hide();
		$("#ruleDetail").show();
	});
}


//重置
function reset_station(){
    $('#name').val('');
    $('#belongCity').val('');
    $("select").find("option:first").prop("selected",true);
	reloadStation();

}

//返回
function backStationMain(){
	reloadStation();
}





