<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="ruleMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>火车站点维护   &nbsp;&nbsp;
			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12">
										<div class="pull-left">
											<a class="btn btn-default" onclick="synStationData()" role="button">手动更新站点</a>
										</div>
										<!-- 查询条件 -->
										 <form class="form-inline" method="post" >
											<nav class="text-right">
												<div class=" form-group">
												 <label class="control-label">车站：</label>
													<input name="name" id="name" type="text"  class="form-control" placeholder="中文/全拼/简拼" style="width: 250px"/>
												</div>
												<div class="form-group">
													<label class="control-label">所属城市：</label>
													<input name="keyword" id="belongCity" type="text"  class="form-control" placeholder="中文" style="width: 250px"/>
												</div>
												<div class=" form-group">
													<select class="form-control" style="width: 100px">
														<c:forEach items="${cityLinkEnum}" var="link">
															<option  code = "${link}">
                                                              ${link.msg}
															</option>
														</c:forEach>
													</select>
												</div>
												<div class="form-group">
													<button type="button" class="btn btn-default" onclick="search_station()">查询</button>
													<button type="button" class="btn btn-default" onclick="reset_station()">重置</button>
												</div>
											</nav>
										 </form>
										</div>
									</div>
									<div  id="rule_Table" style="margin-top: 15px">
					
					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 详细页面 -->
<div id="ruleDetail" style="display:none;"></div>
<script src="https://kyfw.12306.cn/otn/resources/js/framework/station_name.js"></script>
<script type="text/javascript" src="webpage/train/station/station.js?version=${globalVersion}"></script>
