<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" id="ruleTable">
	<thead>
		<tr>
			<th><label>站点序号</label></th>
			<th><label>车站中文名</label></th>
			<th><label>车站中文全拼</label></th>
			<th><label>车站中文简拼</label></th>
			<th><label>车站代码</label></th>
			<th><label>所属城市</label></th>
			<th><label>最后更新时间</label></th>
			<th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="bean">
         <tr class="odd gradeX">
        	 <td>${bean.id}</td>
             <td>${bean.nameCn}</td>
			 <td>${bean.nameEn}</td>
			 <td>${bean.nameCnSimple}</td>
			 <td>${bean.stationCode}</td>
			 <td>${bean.cityName}</td>
			 <td><fmt:formatDate value="${bean.lastUpdateTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
             <td class="text-center" >
             	<a onclick="getStation('${bean.id }')" >修改</a>
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadStation"></jsp:include>
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>