<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                <h3 align="center">火车站点修改</h3>
             </div>
             <div class="panel-body">
                 <form:form id="stationForm" cssClass="form-horizontal bv-form" method="post" action="train/station/updateStation" modelAttribute="station">
                 	<form:hidden path="id"/>
					 <div class="form-group">
						 <label class="col-sm-2 control-label">车站中文名</label>
						 <div class="col-sm-6">
							 <form:input path="nameCn" cssClass="form-control"/>
						 </div>
					 </div>
					 <div class="form-group">
							 <label class="col-sm-2 control-label">车站中文全拼</label>
						 <div class="col-sm-6">
							 <form:input path="nameEn" cssClass="form-control"/>
						 </div>
					 </div>
					 <div class="form-group">
						 <label class="col-sm-2 control-label">车站中文简拼</label>
						 <div class="col-sm-6">
							 <form:input path="nameCnSimple" cssClass="form-control"/>
						 </div>
					 </div>
					 <div class="form-group">
					     <label class="col-sm-2 control-label">所属城市</label>
						 <div class="col-sm-6">
							 <form:input path="cityName" cssClass="form-control" readonly="true" onclick="chooseDistrictData()"/>
							 <form:hidden path="cityId"/>
						 </div>
					  </div>
			</form:form>
			<div class="form-group">
				<div class="col-sm-8">
					<center>
					    <a class="btn btn-primary"  onclick="submitStation()">确认修改</a>

						<a class="btn btn-default"  onclick="backUrl()">返回不修改</a>
					</center>
				</div>
			</div>
         </div> 
	     </div>
	</div>
</div>
<script type="text/javascript">

//提交表单
function submitStation(){
    if (!validateForm()) {
        return;
	}
	$.ajax({
		type : "POST",
		url : "train/station/updateStation",
		data : $('#stationForm').serialize(),
		async : false,
		error : function(request) {
			showErrorMsg("请求失败，请刷新重试");
		},
		success : function(data) {
            $.alert(data.txt,"提示");
            setTimeout(backUrl, 500);

		}
	});
}

function validateForm() {

	if ($('#nameCn').val().trim() == '') {
		$.alert('车站中文名不能为空');
		return false;
	}
    if ($('#nameEn').val().trim() == '') {
        $.alert('车站中文全拼不能为空');
        return false;
    }
    if ($('#nameCnSimple').val().trim() == '') {
        $.alert('车站中文简拼不能为空');
        return false;
    }
    return true;
}

function backUrl() {
    location.href = '/train/station';
}

var getDistrictDataChoosedData = JSON.parse('${city}' || '[]');
function chooseDistrictData() {

    new choose.districtData({
        id:'selMultiDistrictData',//给id可以防止一直新建对象
        empty:true,
        getChoosedData:function () {
            return getDistrictDataChoosedData;
        }
    },function (data) {
       
        getDistrictDataChoosedData = [];
        console.log(data)
        if (data) {
            var city = {id: data.id, nameCn: data.nameCn};
            getDistrictDataChoosedData.push(city);
            $("#cityId").val(data.id);
            $("#cityName").val(data.nameCn);
        }
    });
}
</script>