<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    #trans-detail table {
        margin-left: 15px;
    }

    #trans-detail table th {
        font-size: 14px;
        text-align: right;
    }


</style>
<c:if test="${empty hubsDtos}">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>连接器详细信息&nbsp;&nbsp;
        </div>
        <div class="panel-body">
            <div id="trans-detail">
                <table>
                    <tr>
                        <th width="10%">连接器编码:&nbsp;&nbsp;</th>
                        <td width="10%">${hubsDto.code}&nbsp;&nbsp;</td>
                        <th width="10%">名称:&nbsp;&nbsp;</th>
                        <td width="10%">${hubsDto.name}&nbsp;&nbsp;</td>
                        <th width="10%">是否禁用:&nbsp;&nbsp;</th>
                        <td width="10%">
                            <input type="checkbox" disabled="disabled" ${hubsDto.isDisable ? "checked" : ""}/>
                        </td>
                    </tr>
                    <tr>
                        <th>查询接口:&nbsp;&nbsp;</th>
                        <td>
                            <input type="checkbox" disabled="disabled" ${hubsDto.search ? "checked" : ""}/>
                        </td>
                        <th width="85px">验余票/验证件号接口:&nbsp;&nbsp;</th>
                        <td>
                            <input type="checkbox" disabled="disabled" ${hubsDto.verify ? "checked" : ""}/>
                        </td>
                        <th>占座:&nbsp;&nbsp;</th>
                        <td>
                            <input type="checkbox" disabled="disabled" ${hubsDto.occupancySeat ? "checked" : ""}/>
                        </td>
                    </tr>
                    <tr>
                        <th>抢票出票接口:&nbsp;&nbsp;</th>
                        <td>
                            <input type="checkbox" disabled="disabled" ${hubsDto.grabTickets ? "checked" : ""}/>
                        </td>
                        <th>普通出票接口:&nbsp;&nbsp;</th>
                        <td>
                            <input type="checkbox" disabled="disabled" ${hubsDto.normalIssue ? "checked" : ""}/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default" id="authtypeDiv">
        <div class="panel-heading">
            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>所支持的连接&nbsp;&nbsp;
            <button class="btn btn-default" onclick="addConnect('${hubsCode}')" role="button">添加</button>
        </div>
        <div class="panel-body">
            <div class="col-sm-12">
                <table class="table  table-bordered table-hover table-condensed dataTables-example dataTable">
                    <thead>
                    <tr>
                        <th class="sort-column">连接名称</th>
                        <th class="sort-column">商户名称</th>
                        <th class="sort-column">商户账号</th>
                        <th class="sort-column">接口URL</th>
                        <th class="sort-column">出票渠道</th>
                        <th class="sort-column">禁用</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${hubsConnectDtos }" var="hubsConnectDto">
                        <tr>
                            <td>${hubsConnectDto.husConnectName }</td>
                            <td>${hubsConnectDto.merchantName }</td>
                            <td>${hubsConnectDto.merchantId }</td>
                            <td>${hubsConnectDto.interfaceUrl }</td>
                            <td>${hubsConnectDto.channelCode }</td>
                            <td>
                                <c:if test="${hubsConnectDto.isDisable == true}">
                                    <input type="checkbox" disabled="disabled" checked="checked"/>
                                </c:if>
                                <c:if test="${hubsConnectDto.isDisable == false}">
                                    <input type="checkbox" disabled="disabled"/>
                                </c:if>
                            </td>
                            <td>
                                <c:if test="${not empty hubsConnectDto.tmcId}">
                                    <a onclick="editConnect('${hubsConnectDto.id}','${hubsConnectDto.tmcId}')">修改</a>
                                </c:if>
                                <c:if test="${empty hubsConnectDto.tmcId}">
                                    <a onclick="editConnect('${hubsConnectDto.id}')">修改</a>
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</c:if>

<div id="modelHubsConnectDiv"></div>