//当前选中的hubs
var curHubsCode = null;
var curHubsName = null;
var curHubsIsDisable = null;

function selectHubs(code, name, isDisable) {
    $("#hubsListTable tbody tr").removeClass("active");
    $("#hubsListTr" + code).addClass("active");
    curHubsCode = code;
    curHubsName = name;
    curHubsIsDisable = isDisable;
    //加载右侧数据
    reloadRight(code);
}

//加载右侧面板
function reloadRight(code) {
    var path = 'train/hubs/loadRight?code=' + code;
    $("#rightDetail").load(path);
}

//添加
function addHubs() {
    showHubsModal("train/hubs/hubsDetail", function () {
        $('#mode_hubs').modal();
        initForm();
    });
};

//修改
function editHubs() {
    if (curHubsCode == null) {
        $.alert("请选择一行数据!", "提示");
        return;
    }
    showHubsModal("train/hubs/hubsDetail?hubsCode="+curHubsCode, function () {
        $('#mode_hubs').modal();
        initForm();
    });
};

//删除
function deleteHubs() {
    if (curHubsCode == null) {
        $.alert("请选择一行数据!", "提示");
        return;
    }

    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: '确认删除当前连接器?',
        confirm: function () {
            $.ajax({
                cache: true,
                type: "POST",
                url: "train/hubs/deleteHubs",
                data: {
                    code: curHubsCode
                },
                async: false,
                error: function (request) {
                    showErrorMsg("请求失败，请刷新重试");
                },
                success: function (data) {
                    if (data.type == 'success') {
                        window.location.reload();
                    } else {
                        $.alert(data.txt, "提示");
                    }
                }
            });
        },
        cancel: function () {

        }
    });
};

//显示Model
function showHubsModal(url, data, callback) {
    $('#modelHubsDiv').load(url, data, function (context, state) {
        if ('success' == state && callback) {
            callback();
        }
    });
}

function initForm() {
    $("#detailForm").validate(
        {
            rules: {
                name: {
                    required: true,
                    maxlength: 16
                },
                code: {
                    required: true,
                    maxlength: 16
                },
            },
            submitHandler: function (form) {
                console.log($('#detailForm').serialize());
                $.ajax({
                    cache: true,
                    type: "POST",
                    url: "train/hubs/saveOrupdateHubs",
                    data: $('#detailForm').serialize(),
                    async: false,
                    error: function (e) {
                        showErrorMsg("请求失败，请刷新重试");
                        console.log(e);
                        $('#mode_hubs').modal('hide');
                    },
                    success: function (data) {
                        $('#mode_hubs').modal('hide');
                        if (data.type == 'success') {
                            showSuccessMsg("操作成功");
                            window.location.reload();
                        } else {
                            showErrorMsg(data.txt);
                        }
                    }
                });
            },
            errorPlacement: setErrorPlacement,
            success: validateSuccess,
            highlight: setHighlight
        });
}

function submitHubs() {
    $("#detailForm").submit();
}


//*****************************连接器 连接*************************************
function addConnect(hubsCode) {
    showHubsConnectModal("train/hubs/hubsConnectDetail?hubsCode=" + hubsCode, function () {
        $('#mode_hubsConnect').modal();
        initFormConnect();
    });
}

//显示Model
function showHubsConnectModal(url, data, callback) {
    $('#modelHubsConnectDiv').load(url, data, function (context, state) {
        if ('success' == state && callback) {
            callback();
        }
    });
}

function editConnect(id, tmcId) {
    if (tmcId == null) {
        alert("缺少tmc，暂不可修改！")
    } else {
        showHubsConnectModal("train/hubs/hubsConnectDetail?id=" + id + "&&tmcId=" + tmcId, function () {
            $('#mode_hubsConnect').modal();
            initFormConnect();
        });
    }
}

function deleteConnect(id) {
    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: '确认删除当前连接?',
        confirm: function () {
            $.ajax({
                cache: true,
                type: "POST",
                url: "train/hubs/deleteHubsConnect",
                data: {
                    id: id
                },
                async: false,
                error: function (request) {
                    showErrorMsg("请求失败，请刷新重试");
                },
                success: function (data) {
                    if (data.type == 'success') {
                        showSuccessMsg(data.txt);
                        reloadRight(curHubsCode);
                    } else {
                        $.alert(data.txt, "提示");
                    }
                }
            });
        },
        cancel: function () {

        }
    });
}

function initFormConnect() {
    $("#detailFormConnect").validate(
        {
            rules: {
                husConnectName: {
                    required: true,
                    maxlength: 16
                },
                merchantId: {
                    required: true,
                    maxlength: 32
                },
                merchantSigKey: {
                    required: true,
                    maxlength: 32
                },
                interfaceUrl: {
                    required: true,
                    url: true
                },
                tmcName: {
                    required: true
                },
                channelCode: {
                    required: true
                },
                contactName: {
                    required: true,
                    maxlength: 8
                },
                contactMobile: {
                    required: true,
                    mobile: true
                },
                contactPhone: {
                    required: true
                }
            },
            submitHandler: function (form) {
                $('#mode_hubsConnect').modal('hide');
                setTimeout(function () {
                    $.ajax({
                        cache: true,
                        type: "POST",
                        url: "train/hubs/saveOrupdateHubsConnect",
                        data: $('#detailFormConnect').serialize(),
                        async: false,
                        error: function (request) {
                            showErrorMsg("请求失败，请刷新重试");
                        },
                        success: function (data) {
                            if (data.type == 'success') {
                                showSuccessMsg("操作成功");
                                reloadRight(curHubsCode);
                            } else {
                                showErrorMsg(data.txt);
                            }
                        }
                    });
                }, 1000);
            },
            errorPlacement: setErrorPlacement,
            success: validateSuccess,
            highlight: setHighlight
        });
}

function submitHubsConnect() {
    $("#detailFormConnect").submit();
}


function backRight() {
    reloadRight(curHubsCode);
}


