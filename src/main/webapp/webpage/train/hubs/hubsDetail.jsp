<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="modal fade" id="mode_hubs" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <c:choose>
                    <c:when test="${empty trainHubsDto.code}">
                        <h4 class="modal-title">新增连接器</h4>
                    </c:when>
                    <c:otherwise>
                        <h4 class="modal-title">修改连接器</h4>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="modal-body">
                <form:form modelAttribute="trainHubsDto" class="form-horizontal" id="detailForm" role="form">
                    <input type="hidden" name="editMode" id="editMode" value="${not empty trainHubsDto.code}"/>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">连接器编码</label>
                        <div class="col-sm-9">
                            <form:input cssClass="form-control" path="code" id="hubsCode" placeholder="编码" readonly="${not empty code ? 'readonly' : ''}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">连接器名称</label>
                        <div class="col-sm-9">
                            <form:input cssClass="form-control" path="name"  placeholder="名称"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="pull-left col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">查询接口</label>
                                <div>
                                    <label class="radio-inline">
                                        <form:radiobutton path="search" checked="${trainHubsDto.search ? 'checked' : ''}" value="1"/>支持
                                    </label>
                                    <lable class="radio-inline">
                                        <form:radiobutton path="search" checked="${!trainHubsDto.search ? 'checked' : ''}" value="0"/>不支持
                                    </lable>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="pull-left col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">验余票/验证件号接口</label>
                                <div>
                                    <label class="radio-inline">
                                        <form:radiobutton path="verify" checked="${trainHubsDto.verify ? 'checked' : ''}" value="1"/>支持
                                    </label>
                                    <lable class="radio-inline">
                                        <form:radiobutton path="verify" checked="${!trainHubsDto.verify ? 'checked' : ''}" value="0"/>不支持
                                    </lable>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="pull-left col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">占座接口</label>
                                <div>
                                    <label class="radio-inline">
                                        <form:radiobutton path="occupancySeat" checked="${trainHubsDto.occupancySeat ? 'checked' : ''}" value="1"/>支持
                                    </label>
                                    <lable class="radio-inline">
                                        <form:radiobutton path="occupancySeat" checked="${!trainHubsDto.occupancySeat ? 'checked' : ''}" value="0"/>不支持
                                    </lable>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="pull-left col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">抢票出票接口</label>
                                <div>
                                    <label class="radio-inline">
                                        <form:radiobutton path="grabTickets" checked="${trainHubsDto.grabTickets ? 'checked' : ''}" value="1"/>支持
                                    </label>
                                    <lable class="radio-inline">
                                        <form:radiobutton path="grabTickets" checked="${!trainHubsDto.grabTickets ? 'checked' : ''}" value="0"/>不支持
                                    </lable>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="pull-left col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">普通出票接口</label>
                                <div>
                                    <label class="radio-inline">
                                        <form:radiobutton path="normalIssue" checked="${trainHubsDto.normalIssue ? 'checked' : ''}" value="1"/>支持
                                    </label>
                                    <lable class="radio-inline">
                                        <form:radiobutton path="normalIssue" checked="${!trainHubsDto.normalIssue ? 'checked' : ''}" value="0"/>不支持
                                    </lable>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="pull-left col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-6 control-label">禁用</label>
                                <div>
                                    <label class="radio-inline">
                                        <form:radiobutton path="isDisable" checked="${trainHubsDto.isDisable ? 'checked' : ''}" value="1"/>是
                                    </label>
                                    <lable class="radio-inline">
                                        <form:radiobutton path="isDisable" checked="${!trainHubsDto.isDisable ? 'checked' : ''}" value="0"/>否
                                    </lable>
                                </div>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitHubs()">保存</button>
            </div>
        </div>
    </div>
</div>