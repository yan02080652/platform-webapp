<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .reqMark {
        color: red;
        font-weight: bold;
        font-size: 10px;
    }
</style>
<div class="modal fade" id="mode_hubsConnect" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="overflow-y:auto">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <c:choose>
                    <c:when test="${ empty hubsConnectDto.id}">
                        <h4 class="modal-title">新增连接器连接</h4>
                    </c:when>
                    <c:otherwise>
                        <h4 class="modal-title">修改连接器连接</h4>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="modal-body">
                <form modelAttribute="hubsConnectDto" class="form-horizontal" id="detailFormConnect" role="form">
                    <input type="hidden" name="id" id="hubsConnectId" value="${hubsConnectDto.id}"/>
                    <input type="hidden" name="hubsCode" id="hubsCode" value="${hubsConnectDto.hubsCode}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>连接名称</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="husConnectName"
                                   value="${hubsConnectDto.husConnectName}"
                                   placeholder="连接名称"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">商户名称</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="merchantName"
                                   value="${hubsConnectDto.merchantName}"
                                   placeholder="商户名称"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>商户账号</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="merchantId"
                                   value="${hubsConnectDto.merchantId}"
                                   placeholder="商户账号"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>商户密钥</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="merchantSigKey"
                                   value="${hubsConnectDto.merchantSigKey}"
                                   placeholder="商户密钥"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">商户密码</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="merchantSigSecret"
                                   value="${hubsConnectDto.merchantSigSecret}"
                                   placeholder="商户密码"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">接口版本号</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="interfaceVersion"
                                   value="${hubsConnectDto.interfaceVersion}"
                                   placeholder="接口版本号"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>接口URL</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="interfaceUrl"
                                   value="${hubsConnectDto.interfaceUrl}"
                                   placeholder="接口URL"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">是否禁用</label>
                        <div class="col-sm-9">
                            <label class="radio-inline"><input type="radio" value="1"
                                                               name="isDisable"/>是</label>
                            <lable class="radio-inline"><input type="radio" value="0" checked="checked"
                                                               name="isDisable"/>否
                            </lable>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>所属tmc
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control partner" name="tmcName" value="${tmcPartner.name}"
                                   placeholder="选择tmc" readonly/>
                            <input type="hidden" id="tmcId" name="tmcId" value="${hubsConnectDto.tmcId}"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>出票渠道</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="channelCode" name="channelCode">
                                <option value="-1">请先选择所属TMC</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>联系人姓名</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="contactName"
                                   value="${hubsConnectDto.contactName}"
                                   placeholder="联系人姓名"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>联系人手机号</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="contactMobile"
                                   value="${hubsConnectDto.contactMobile}"
                                   placeholder="联系人手机号"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>联系人电话</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="contactPhone"
                                   value="${hubsConnectDto.contactPhone}"
                                   placeholder="联系人电话"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">联系人邮箱</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="contactEmail"
                                   value="${hubsConnectDto.contactEmail}"
                                   placeholder="联系人邮箱"/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitHubsConnect()">保存</button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            var tmcId = $('#tmcId').val();
            if ("" != tmcId) {
                tmcChange(tmcId);
            }
        });

        $(document).ready(function () {
            var id = "${hubsConnectDto.id}";
            if (id != null && id != '') {
                //修改
                if ('${hubsConnectDto.isDisable}' == 'false') {
                    $("input:radio[value=0][name=isDisable]").attr('checked', 'checked');
                } else {
                    $("input:radio[value=1][name=isDisable]").attr('checked', 'checked');
                }
                if ('${hubsConnectDto.channelCode}') {
                    $("#channelCode").val('${hubsConnectDto.channelCode}');
                }
            }

            $(".partner").click(function () {
                TR.select('partner', {
                    type: 1
                }, function (result) {
                    $("[name='tmcName']").val(result.name);
                    $("[name='tmcId']").val(result.id);
                    tmcChange();
                });
            });

        });

        function tmcChange() {
            $.ajax({
                url: '/train/hubs/getIssueChannels.json',
                type: 'post',
                data: {
                    tmcId: $('#tmcId').val()
                },
                success: function (data) {
                    $('#channelCode').html('');
                    for (var i = 0; i < data.length; i++) {
                        $('#channelCode').append('<option value="' + data[i].code + '">' + data[i].name + '</option>');
                    }
                },
                error: function () {
                    layer.msg("系统异常", {icon: 2})
                }
            })
        }
    </script>
</div>