<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
    table td{
        vertical-align:middle !important;
    }
</style>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>航班动态维护
    </div>
    <div class="panel-body">
        <%--top--%>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <a class="btn btn-default" onclick="addFligthChange()"role="button">新增航班动态</a>
                </div>
                <!-- 查询条件 -->
                <form class="form-inline" method="post" >
                    <nav class="text-right">
                        <div class="form-group">
                            <label class="control-label">航班号</label>
                            <input name="flightNo_con" id="flightNo_con" type="text" value="${flightNo_con }" class="form-control" style="width: 150px"/>
                        </div>
                        &nbsp;<label class="control-label">计划起飞时间</label>
                        <div class="input-group">
                            <input type="text" name="fromDate_con" id="fromDate_con" class="form-control"
                                   value="${fromDate_con }" readonly>
                            <span class="input-group-addon" onclick="chooseFromDate()"><i class="fa fa-calendar"></i></span>
                        </div>
                        <div class="form-group" style="margin-left: 10px">
                            <button type="button" class="btn btn-default" onclick="searchFlightChange()">查询</button>
                            <button type="button" class="btn btn-default" onclick="resetFc()">重置</button>
                        </div>
                    </nav>
                </form>
            </div>
        </div>
        <%--table body--%>
        <table class="table table-striped table-bordered table-hover" style="margin-top: 10px;">
            <thead>
            <tr>
                <th><label>航班号 </label></th>
                <th><label>航班状态</label></th>
                <th><label>出发机场</label></th>
                <th><label>到达机场</label></th>
                <th width="180"><label>计划起抵时间</label></th>
                <th width="180"><label>实际起抵时间</label></th>
                <th width="150"><label>最后更新时间</label></th>
                <th width="150"><label>最后发送时间</label></th>
                <th><label>操作</label></th>
            </tr>
            </thead>
            <tbody>
                <c:forEach items="${pageList.list }" var="fc">
                    <tr>
                        <td>${fc.flightNo}</td>
                        <td width="85" style="<c:if test="${fc.flightState eq 4 or fc.flightState eq 5 or fc.flightState eq 6 }">
                                    color:red;</c:if>">
                            <c:forEach items="${flightStates}" var="fs">
                                <c:if test="${fs.value eq fc.flightState}">
                                    ${fs.message}
                                </c:if>
                            </c:forEach>
                        </td>
                        <td>${fc.flightDepAirport}</td>
                        <td>${fc.flightArrAirport}</td>
                        <td style="text-align: left">
                            <c:if test="${not empty fc.flightDepTimePlanDate}">
                                <div>起飞：<fmt:formatDate value="${fc.flightDepTimePlanDate }" pattern="yyy-MM-dd HH:mm"/><div>
                            </c:if>
                            <c:if test="${not empty fc.flightArrTimePlanDate}">
                                <div>到达：<fmt:formatDate value="${fc.flightArrTimePlanDate }" pattern="yyy-MM-dd HH:mm"/></div>
                            </c:if>
                        </td>
                        <td style="text-align: left">
                            <c:if test="${not empty fc.flightDepTimeDate}">
                                <div>起飞：<fmt:formatDate value="${fc.flightDepTimeDate }" pattern="yyy-MM-dd HH:mm"/><div>
                            </c:if>
                            <c:if test="${not empty fc.flightArrTimeDate}">
                                <div>到达：<fmt:formatDate value="${fc.flightArrTimeDate }" pattern="yyy-MM-dd HH:mm"/><div>
                            </c:if>
                        </td>
                        <td><fmt:formatDate value="${fc.flightUpdateTime }" pattern="yyy-MM-dd HH:mm:ss"/></td>
                        <td><fmt:formatDate value="${fc.flightLastSendTime }" pattern="yyy-MM-dd HH:mm:ss"/></td>
                        <td><a href="flight/change/detail.html?id=${fc.id }">修改</a>&nbsp;&nbsp;
                            <a onclick="deleteFc(${fc.id})">删除</a>&nbsp;&nbsp;
                            <c:if test="${fc.flightState eq 4 or fc.flightState eq 5 or fc.flightState eq 6 }">
                                <a onclick="sendFcMsg(${fc.id })">发送</a>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="pagin">
            <jsp:include
                    page="/webpage/common/pagination_ajax.jsp?callback=searchFlightChange"></jsp:include>
        </div>
    </div>
</div>

<script>
    var pageIndex = <%= request.getParameter("pageIndex")%> || 1;
    var dateDom = $("#fromDate_con");
    $("#fromDate_con").jeDate({
        format:"YYYY-MM-DD"
    });
    if(!dateDom.val()){
        toToday();
    }else if('NULL' == dateDom.val()){
        dateDom.val(null);
    }

    //选择计划起飞时间
    function chooseFromDate() {
        $("#fromDate_con").trigger('click');
    }

    function addFligthChange(){
        window.location.href = "/flight/change/detail.html";
    }

    function searchFlightChange(pageIndex) {
        reloadFc({
            pageIndex:pageIndex
        });
    }

    function resetFc(){
        $('#flightNo_con').val(null);
        toToday();
        reloadFc({
            pageNo:1
        });
    }

    //设置计划起飞时间条件为今天
    function  toToday() {
        var now = new Date();
        var year = now.getFullYear();
        var month = now.getMonth()+1;
        var day = now.getDate();
        var fromDate = year + (month>9?'-':'-0')+month+(day>9?'-':'-0')+day;
        $("#fromDate_con").val(fromDate);
    }

    function reloadFc(data){
        var config = {
            pageIndex:pageIndex,
            flightNo_con:$('#flightNo_con').val(),
            fromDate_con:$('#fromDate_con').val() || 'NULL'
        };
        $.extend(config,data);
        var arr = [];
        $.each(config,function(k,v){
            arr.push(k+"="+v);
        });
        window.location.href ='/flight/change?'+arr.join('&');
    }

    function deleteFc(id){
        layer.confirm('确认删除航班动态信息？', {
            btn: ['确认','取消'] //按钮
        }, function(){
            window.location.href = "/flight/change/delete?id="+id+"&pageIndex="+pageIndex;
        });
    }

    function sendFcMsg(id) {
        var a = layer.confirm('确认发送航班动态信息？', {
            btn: ['确认','取消'] //按钮
        }, function(rs){
            layer.close(a);
            var sendLoading = layer.load(0, {shade: false}); ;
            $.post('flight/change/send?flightChangeId='+id,function(rs){
                layer.close(sendLoading);
                layer.msg('已发送了'+(rs.count || 0)+'个用户', {icon: 1});
            });
        });
    }
</script>
