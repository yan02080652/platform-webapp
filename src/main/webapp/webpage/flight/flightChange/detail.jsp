<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
    a{
        text-decoration: underline;
    }
    .noDisplay{
        display: none;
    }
    .popover.right>.arrow{
        top:50% !important;
    }
    .control-label .reqMark{
        color: red;
        font-weight: bold;
        font-size: 15px;
    }
    #formFlightChange .input-group{
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }
    #formFlightChange .input-group-addon {
        border-left: none;
        border-bottom-right-radius:4px;
        border-top-right-radius:4px;
        cursor: pointer;
    }
    #formFlightChange .btn-success{
        background-color: #46A2BB;
        border-color: #46A2BB;
    }
</style>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        <c:choose>
            <c:when test="${not empty fc.id}">
                修改
            </c:when>
            <c:otherwise>
                新增
            </c:otherwise>
        </c:choose>
        航班动态&nbsp;<a href="flight/change">返回</a>
    </div>
    <div class="panel-body">
        <form id="formFlightChange" class="form-horizontal" method="post" action="flight/change/save">
            <input type="hidden" name="id" class="form-control" value="${fc.id}">
            <div class="form-group">
                <label class="col-sm-2 control-label">航班号</label>
                <div class="col-sm-6">
                    <input type="text" name="flightNo" class="form-control" value="${fc.flightNo}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">航空公司</label>
                <div class="col-sm-2">
                    <input type="text" name="flightCompanyCode" class="form-control" value="${fn:substring(fc.flightNo,0 ,2 )}">
                </div>
                <div class="col-sm-4 input-group">
                    <input type="text" name="flightCompany" class="form-control" value="${fc.flightCompany}" placeholder="航司名称" readonly>
                    <span class="input-group-addon"><i class="fa fa-plane"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">出发地机场</label>
                <div class="col-sm-2">
                    <input type="text" name="flightDepCode" class="form-control" placeholder="示例：SHA" value="${fc.flightDepCode}">
                </div>
                <div class="col-sm-4 input-group">
                    <input type="text" name="flightDepAirport" class="form-control" value="${fc.flightDepAirport}" placeholder="请选择出发地机场" readonly>
                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">目的地机场</label>
                <div class="col-sm-2">
                    <input type="text" name="flightArrCode" class="form-control" placeholder="示例：SHA" value="${fc.flightArrCode}">
                </div>
                <div class="col-sm-4 input-group">
                    <input type="text" name="flightArrAirport" class="form-control" value="${fc.flightArrAirport}" placeholder="请选择目的地机场" readonly>
                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">计划起飞/到达时间</label>
                <div class="col-sm-3 input-group">
                    <input type="text" name="flightDepTimePlanDateStr" class="form-control time-picker"  placeholder="请输入计划起飞时间"
                           value="<fmt:formatDate value='${fc.flightDepTimePlanDate }' pattern="yyy-MM-dd HH:mm"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="col-sm-3 input-group">
                    <input type="text" name="flightArrTimePlanDateStr" class="form-control time-picker" placeholder="请输入计划到达时间"
                           value="<fmt:formatDate value='${fc.flightArrTimePlanDate }' pattern="yyy-MM-dd HH:mm"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">航班状态</label>
                <div class="col-sm-6">
                    <select name="flightState" class="form-control">
                        <c:forEach items="${flightStates}" var="fs">
                            <option value="${fs.value}" <c:if test="${fs.value eq fc.flightState}">selected</c:if>>${fs.message}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">取消/延误原因</label>
                <div class="col-sm-6">
                    <select name="cancelReson" class="form-control">
                        <option selected="selected" value="-1">请选择原因</option>
                        <c:forEach items="${cancelResons}" var="cr">
                            <option value="${cr.value}" <c:if test="${cr.value eq fc.cancelReson}">selected</c:if>>${cr.message}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">预计起飞/到达时间</label>
                <div class="col-sm-3 input-group">
                    <input type="text" name="delayFlightDepTimeStr" class="form-control time-picker" placeholder="请输入计划起飞时间"
                           value="<fmt:formatDate value='${fc.delayFlightDepTime }' pattern="yyy-MM-dd HH:mm"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="col-sm-3 input-group">
                    <input type="text" name="delayFlightArrTimeStr" class="form-control time-picker" placeholder="请输入计划到达时间"
                           value="<fmt:formatDate value='${fc.delayFlightArrTime }' pattern="yyy-MM-dd HH:mm"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">实际起飞/到达时间</label>
                <div class="col-sm-3 input-group">
                    <input type="text" name="flightDepTimeDateStr" class="form-control time-picker" placeholder="请输入实际起飞时间"
                           value="<fmt:formatDate value='${fc.flightDepTimeDate }' pattern="yyy-MM-dd HH:mm"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="col-sm-3 input-group">
                    <input type="text" name="flightArrTimeDateStr" class="form-control time-picker" placeholder="请输入实际到达时间"
                           value="<fmt:formatDate value='${fc.flightArrTimeDate }' pattern="yyy-MM-dd HH:mm"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">备降机场名</label>
                <div class="col-sm-2">
                    <input type="text" name="alternateDepAirportCode" class="form-control" placeholder="示例：SHA" value="${fc.alternateDepAirportCode}">
                </div>
                <div class="col-sm-4 input-group">
                    <input type="text" name="alternateDepAirport" class="form-control" value="${fc.alternateDepAirport}">
                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">出发地城市</label>
                <div class="col-sm-2">
                    <input type="text" name="flightDepCodeForCity" class="form-control" placeholder="示例：SHA">
                </div>
                <div class="col-sm-4">
                    <select class="form-control" name="flightDepCity">
                        <option value="">请选择城市</option>
                        <c:forEach items="${ cityList }" var="city">
                            <option value="${city.code }"<c:if test="${fc.flightDepCity eq city.code}">selected</c:if>>${city.nameCn }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">目的地城市</label>
                <div class="col-sm-2">
                    <input type="text" name="flightArrCodeForCity" class="form-control" placeholder="示例：SHA">
                </div>
                <div class="col-sm-4">
                    <select class="form-control" name="flightArrCity">
                        <option value="">请选择城市</option>
                        <c:forEach items="${ cityList }" var="city">
                            <option value="${city.code }"<c:if test="${fc.flightArrCity eq city.code}">selected</c:if>>${city.nameCn }</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">出发地机场航站楼</label>
                <div class="col-sm-2">
                    <input type="text" name="flightDepTerminal" class="form-control" placeholder="示例:T3" value="${fc.flightDepTerminal}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">目的地机场航站楼</label>
                <div class="col-sm-2">
                    <input type="text" name="flightArrTerminal" class="form-control" placeholder="示例:T3" value="${fc.flightArrTerminal}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">是否经停</label>
                <div class="col-sm-1">
                    <label class="radio-inline"><input type="radio" name="stopFlag" value="1" <c:if test="${fc.stopFlag eq 1}">checked</c:if>/>是</label>
                    <label class="radio-inline"><input type="radio" name="stopFlag" value="0" <c:if test="${fc.stopFlag eq 0}">checked</c:if>/>否</label>
                </div>
                <div class="col-sm-1 <c:if test="${fc.stopFlag ne 1}">noDisplay</c:if>">
                    <input type="text" name="stopAirportCode" class="form-control" value="${fc.stopAirportCode}" placeholder="SHA">
                </div>
                <div class="col-sm-2 input-group <c:if test="${fc.stopFlag ne 1}">noDisplay</c:if>">
                    <input type="text" name="stopAirport" class="form-control" value="${fc.stopAirport}" readonly>
                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                </div>
                <div class="col-sm-2 input-group <c:if test="${fc.stopFlag ne 1}">noDisplay</c:if>">
                    <input type="text" name="stopAirportDepTimeStr" class="form-control time-picker" placeholder="请输入经停起飞时间"
                           value="<fmt:formatDate value='${fc.stopAirportDepTime }' pattern="yyy-MM-dd HH:mm"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="col-sm-2 input-group <c:if test="${fc.stopFlag ne 1}">noDisplay</c:if>">
                    <input type="text" name="stopAirportArrTimeStr" class="form-control time-picker" placeholder="请输入经停到达时间"
                           value="<fmt:formatDate value='${fc.stopAirportArrTime }' pattern="yyy-MM-dd HH:mm"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">是否共享航班</label>
                <div class="col-sm-1">
                    <label class="radio-inline"><input type="radio" name="shareFlag" value="1" <c:if test="${fc.shareFlag eq 1}">checked</c:if>/>是</label>
                    <label class="radio-inline"><input type="radio" name="shareFlag" value="0" <c:if test="${fc.shareFlag eq 0}">checked</c:if>/>否</label>
                </div>
                <div class="col-sm-2 <c:if test="${fc.shareFlag ne 1}">noDisplay</c:if>">
                    <input type="text" name="shareFlightNo" class="form-control" placeholder="请输入实际承运航班号" value="${fc.shareFlightNo}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">出发登机口</label>
                <div class="col-sm-2">
                    <input type="text" name="flightDepBoardingPort" class="form-control" placeholder="示例:T3" value="${fc.flightDepBoardingPort}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">目的地行李转盘号</label>
                <div class="col-sm-2">
                    <input type="text" name="flightArrBaggageCarouselNo" class="form-control" placeholder="示例:T3" value="${fc.flightArrBaggageCarouselNo}">
                </div>
            </div>
            <div class="form-group">
                <input type="hidden" name="sengMsg" value="0"/>
                <div class="col-sm-4" style="text-align: right">
                    <button type="button" class="btn btn-success" style="width: 100px;" onclick="saveForm()">保存</button>
                </div>
                <div class="col-sm-2" style="text-align: right">
                    <button type="button" class="btn btn-success" style="width: 100px;" onclick="saveAndSendForm()" id="sendMsgBtn">保存并发送</button>
                </div>
            </div>
        </form>

    </div>
</div>
<script type="text/javascript" src="webpage/flight/flightChange/detail.js?version=${globalVersion}"></script>