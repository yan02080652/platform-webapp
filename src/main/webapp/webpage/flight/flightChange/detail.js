$(document).ready(function(){
    bingEvent();
});

//绑定页面时间
function bingEvent(){

    //时间选择器
    $(".time-picker").jeDate({
        format:"YYYY-MM-DD hh:mm"
    });
    $('.input-group-addon').click(function () {
        var preDom = $(this).prev();
        if(preDom.hasClass('time-picker')){
            preDom.trigger('click');
        }
    });

    //航司公司=航班号前2位
    var form = $('#formFlightChange');
    form.find('[name=flightNo]').keyup(function () {
        var companyCode = null;
        if(this.value){
            if(this.value.length<3){
                companyCode = this.value;
            }else{
                companyCode = this.value.substr(0,2);
            }
        }

        var elComCode = form.find('[name=flightCompanyCode]');
        if(elComCode.val() != companyCode){
            elComCode.val(companyCode);
            companyCodeChange(companyCode);
        }
    });

    function companyCodeChange(companyCode){
        var elCom = form.find('[name=flightCompany]');
        if(!companyCode || companyCode.length < 2){
            elCom.val(null);
        }else{
            var com = getName('C',companyCode);
            elCom.val(com?com.nameCn:null);
        }
    }

    //自动带出航司名称
    form.find('[name=flightCompanyCode]').change(function () {
        companyCodeChange(this.value);
    });

    //选择航司
    form.find('[name=flightCompany]').next().click(function () {
        TR.select('flightCompany',{
        },function(result){
            form.find('[name=flightCompany]').val(result?result.name:null);
            form.find('[name=flightCompanyCode]').val(result?result.code:null);
        });
    });

    //选择出发机场
    form.find('[name=flightDepAirport],[name=flightArrAirport],[name=alternateDepAirport],[name=stopAirport]').next().click(function () {
        var curAirport = $(this);
        TR.select('airPort',{
        },function(result){
            var code = result?result.code:null;
            var name = result?result.name:null;
            curAirport.prev().val(name);
            curAirport.parent().prev().find('input').val(code);

            if(code){
                var inputName = curAirport.prev().attr('name');
                if(inputName == 'flightDepAirport'){//自动带出出发城市名称
                    var codeCity = form.find('[name=flightDepCodeForCity]').val();
                    if(!codeCity){
                        var flightDepInfo = getName('T',code);
                        form.find('[name=flightDepCity]').val(flightDepInfo?flightDepInfo.cityCode:'');
                    }
                }else if(inputName == 'flightArrAirport'){//自动带出达到城市名称
                    var codeCity = form.find('[name=flightArrCodeForCity]').val();
                    if(!codeCity){
                        var flightDepInfo = getName('T',code);
                        form.find('[name=flightArrCity]').val(flightDepInfo?flightDepInfo.cityCode:'');
                    }
                }
            }

        });
    });


    //自动带出机场名称
    var depCodes = {
        flightDepCode:'flightDepAirport',
        flightArrCode:'flightArrAirport',
        stopAirportCode:'stopAirport',
        alternateDepAirportCode:'alternateDepAirport'
    };
    $.each(depCodes,function (code,value) {
        form.find("[name="+code+"]").change(function () {
            var flightDepInfo = getName('F',this.value);
            form.find("[name="+value+"]").val(flightDepInfo?flightDepInfo.nameCn:null);
        });
    });

    //输入出发机场自动带出出发城市名称
    form.find('[name=flightDepCode]').change(function(){
        var codeCity = form.find('[name=flightDepCodeForCity]').val();
        if(!codeCity){
            var flightDepInfo = getName('T',this.value);
            form.find('[name=flightDepCity]').val(flightDepInfo?flightDepInfo.cityCode:'');
        }
    });
    //输入出发机场自动带出到达城市名称
    form.find('[name=flightArrCode]').change(function(){
        var codeCity = form.find('[name=flightArrCodeForCity]').val();
        if(!codeCity){
            var flightDepInfo = getName('T',this.value);
            form.find('[name=flightArrCity]').val(flightDepInfo?flightDepInfo.cityCode:'');
        }
    });

    //自动带出出发城市名称
    form.find('[name=flightDepCodeForCity]').change(function () {
        var v = this.value || form.find('[name=flightDepCode]').val();
        var flightDepInfo = getName('T',v);
        form.find('[name=flightDepCity]').val(flightDepInfo?flightDepInfo.cityCode:'');
    });
    //自动带出到达城市名称
    form.find('[name=flightArrCodeForCity]').change(function () {
        var v = this.value || form.find('[name=flightArrCode]').val();
        var flightDepInfo = getName('T',v);
        form.find('[name=flightArrCity]').val(flightDepInfo?flightDepInfo.cityCode:'');
    });

    /*//是否经停
    form.find('[name=stopFlag],[name=shareFlag]').change(function () {
        if(this.value == 1){
            $(this).parent().parent().nextAll().removeClass('noDisplay');
        }else{
            $(this).parent().parent().nextAll().addClass('noDisplay');
        }
    });*/

    initForm();


    //航班状态校验
    var elementFlightState = form.find("[name=flightState]");
    elementFlightState.change(function () {
        flightStateChange(this.value,true);
    });
    flightStateChange(elementFlightState.val());

    //经停站校验
    form.find("[name=stopFlag]").change(stopFlagChange);
    stopFlagChange();

    //是否共享航班
    form.find("[name=shareFlag]").change(shareFlagChange);
    shareFlagChange();
}

/**
 * PLAN(1, "计划"),
 FLY(2, "起飞"),
 ARR(3, "到达"),
 DELAY(4, "延误"),
 CANCEL(5, "取消"),
 PRE_CANCEL(6, "提前取消"),
 ALTERNATE(7, "备降"),
 ALTERNATE_FLY(8, "备降起飞"),
 ALTERNATE_ARR(9, "备降到达"),
 ALTERNATE_CANCEL(10, "备降取消"),
 BACK(10, "返航"),
 BACK_FLY(10, "返航起飞"),
 BACK_ARR(10, "返航到达"),
 BACK_CANCEL(10, "返航取消");
 */
function flightStateChange(flightState,change){
    //航班状态选择为取消，提前取消以及延误的，则必须选择(取消、延误原因)
    var cancelReson = 'cancelReson';
    var cancelResonDom = getElement(cancelReson).parentsUntil('.form-group').parent();

    var sendDom = $('#sendMsgBtn');
    if(flightState == 5 || flightState == 6 || flightState == 4){
        sendDom.show();
        addReqMark(cancelReson,{
            type:true
        });
        cancelResonDom.show();
    }else{
        sendDom.hide();
        cancelResonDom.hide();
        removeReqMark(cancelReson,'type');
    }

    //当选择延误时，下面的 预计起飞/到达时间为必填项
    var delayFlightDepTime = 'delayFlightDepTimeStr';
    var delayFlightArrTime = 'delayFlightArrTimeStr';
    var delayFlightDepTimeDom = getElement(delayFlightDepTime);
    var delayFlightDom = delayFlightDepTimeDom.parentsUntil('.form-group').parent();
    if(flightState == 4){
        addReqMark(delayFlightDepTime,{
            required:true
        });
        addReqMark(delayFlightArrTime,{
            required:true
        });
        if(change){
            delayFlightDepTimeDom.val(getElement('flightDepTimePlanDateStr').val());
            getElement(delayFlightArrTime).val(getElement('flightArrTimePlanDateStr').val());
        }
        delayFlightDom.show();
    }else{
        //清空时间
        if(change){
            delayFlightDepTimeDom.val(null);
            getElement(delayFlightArrTime).val(null);
        }
        delayFlightDom.hide();
        removeReqMark(delayFlightDepTime,'required');
        removeReqMark(delayFlightArrTime,'required');
    }

    //当选择备降、备降起飞、备降到达、备降取消 时，备降机场名称为必填项
    var alternateDepAirport = 'alternateDepAirport';
    var alternateDepAirportDom = getElement(alternateDepAirport).parentsUntil('.form-group').parent();
    if(flightState == 7 || flightState == 8 || flightState == 9 || flightState == 10){
        addReqMark(alternateDepAirport,{
            required:true
        });
        alternateDepAirportDom.show();
    }else{
        removeReqMark(alternateDepAirport,'required');
        alternateDepAirportDom.hide();
    }
}

//经停站校验
function stopFlagChange(){
    var value = $('#formFlightChange [name=stopFlag]:checked').val();
    var stopAirportCode = 'stopAirportCode';
    var stopAirport = 'stopAirport';
    var stopAirportDepTimeStr = 'stopAirportDepTimeStr';
    var stopAirportArrTimeStr = 'stopAirportArrTimeStr';

    var stopAirportCodeEl = getElement(stopAirportCode);
    var stopAirportEl = getElement(stopAirport);
    var stopAirportDepTimeStrEl = getElement(stopAirportDepTimeStr);
    var stopAirportArrTimeStrEl = getElement(stopAirportArrTimeStr);
    if(1 == value){
        addReqMark(stopAirportCode,{
            required:true
        });
        addReqMark(stopAirport,{
            required:true
        });
        addReqMark(stopAirportDepTimeStr,{
            required:true
        });
        addReqMark(stopAirportArrTimeStr,{
            required:true
        });
        stopAirportCodeEl.parent().removeClass('noDisplay');
        stopAirportEl.parent().removeClass('noDisplay');
        stopAirportDepTimeStrEl.parent().removeClass('noDisplay');
        stopAirportArrTimeStrEl.parent().removeClass('noDisplay');
    }else{
        removeReqMark(stopAirportCode,'required');
        removeReqMark(stopAirport,'required');
        removeReqMark(stopAirportDepTimeStr,'required');
        removeReqMark(stopAirportArrTimeStr,'required');
        stopAirportCodeEl.parent().addClass('noDisplay');
        stopAirportEl.parent().addClass('noDisplay');
        stopAirportDepTimeStrEl.parent().addClass('noDisplay');
        stopAirportArrTimeStrEl.parent().addClass('noDisplay');
    }
}

//是否共享航班
function shareFlagChange() {
    var value = $('#formFlightChange [name=shareFlag]:checked').val();
    var shareFlightNo = 'shareFlightNo';
    var shareFlightNoEl = getElement(shareFlightNo);
    if(1 == value){
        addReqMark(shareFlightNo,{
            required:true
        });
        shareFlightNoEl.parent().removeClass('noDisplay');
    }else{
        removeReqMark(shareFlightNo,'required');
        shareFlightNoEl.val(null);
        shareFlightNoEl.parent().addClass('noDisplay');
    }
}

function getElement(name){
    return $("#formFlightChange [name="+name+"]");
}

/**
 * 获取名称
 * @param type
 * @param code
 */
function getName(type,code){
    if(!code){
        return null;
    }

    var rs = null;
    $.ajax({
        url:'flight/change/getName',
        data:{
            type:type,
            code:code
        },
        dataType:'json',
        async:false,
        success:function (data) {
            rs = data;
        }
    });
    return rs;
}

//初始化表格
function initForm(){
    var rules = {
        flightNo:{
            required:true
        },
        flightCompany:{
            required:true
        },
        flightDepAirport:{
            required:true
        },
        flightDepCode:{
            maxlength:10
        },
        flightArrAirport:{
            required:true
        },
        flightArrCode:{
            maxlength:10
        },
        flightDepTimePlanDateStr:{
            required:true,
            date:true,
            remote:{
                type:'post',
                url:'flight/change/checkSame',
                data:{
                    id:function(){
                        return $('#formFlightChange [name=id]').val();
                    },
                    flightNo:function () {
                        return $('#formFlightChange [name=flightNo]').val()
                    },
                    fromDate:function () {
                        return $('#formFlightChange [name=flightDepTimePlanDateStr]').val()
                    }
                },
                dataFilter : function(data) {
                    return 'true' != data;
                }
            }
        },
        flightArrTimePlanDateStr:{
            required:true,
            date:true
        },
        flightState:{
            type:true
        },
        flightDepTerminal:{
            maxlength:5
        },
        flightArrTerminal:{
            maxlength:5
        },
        flightDepBoardingPort:{
            maxlength:5
        },
        flightArrBaggageCarouselNo:{
            maxlength:5
        }

    };

    //添加必填标记
    $.each(rules,function (code,rule) {
        if(rule.required || rule.type){
            addReqMark(code);
        }
    });

    $("#formFlightChange").validate({
        rules:rules,
        messages:{
            flightDepTimePlanDateStr:{
                remote:'已存在相同航班动态'
            }
        },
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight,
        submitHandler: function(form) {
            form.submit();
        }
    });
}

/**
 * 增加校验规则
 * @param code
 * @param addRule object
 */
function addReqMark(code,addRule){
    var requriedMark = $("<span class='reqMark'>*</span>");
    var element = $("#formFlightChange").find("[name="+code+"]");
    var label = element.parentsUntil('.form-group').parent().children('.control-label');
    if($(".reqMark",label).length == 0){
        label.append(requriedMark);
    }

    if(addRule){
        element.rules('add',addRule);
    }
}

/**
 * 删除校验规则
 * @param code
 * @param removeRule string
 */
function removeReqMark(code,removeRule){
    var element = $("#formFlightChange").find("[name="+code+"]");
    var label = element.parentsUntil('.form-group').parent().children('.control-label');
    $('.reqMark',label).remove();
    if(removeRule){
        element.rules('remove',removeRule);
    }
}

function saveForm() {
    submitForm(0);
}

function  saveAndSendForm() {
    submitForm(1);
}

function submitForm(v){
    getElement('sengMsg').val(v);
    $("#formFlightChange").submit();
}