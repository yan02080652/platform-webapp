<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div id="segmentMain">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>航线管理 &nbsp;&nbsp;
            </div>
            <div id="rightDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- 查询条件 -->
                                        <form class="form-inline" method="post">
                                            <nav class="text-right">
                                                <div class=" form-group">
                                                    <label class="control-label">航司编码</label>
                                                    <input name="startKeyword" id="carrierCode" type="text"
                                                           class="form-control" value="${carrierCode}"
                                                           placeholder="航司编码" style="width: 100px"/>
                                                </div>
                                                <div class=" form-group">
                                                    <label class="control-label">出发城市</label>
                                                    <input name="startKeyword" id="fromKeyword" type="text"
                                                           class="form-control"
                                                           placeholder="出发城市" style="width: 100px"/>
                                                </div>
                                                <div class=" form-group">
                                                    <label class="control-label">到达城市</label>
                                                    <input name="startKeyword" id="toKeyword" type="text"
                                                           class="form-control"
                                                           placeholder="到达城市" style="width: 100px"/>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default"
                                                            onclick="search_segment()">查询
                                                    </button>
                                                    <button type="button" class="btn btn-default"
                                                            onclick="reset_segment()">重置
                                                    </button>
                                                </div>
                                            </nav>
                                        </form>
                                    </div>
                                </div>
                                <div id="segment_list" style="margin-top: 15px">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 详细页面 -->
<div id="airlineDetail" style="display:none;"></div>
<script type="text/javascript" src="webpage/flight/segment/segment.js?version=${globalVersion}"></script>