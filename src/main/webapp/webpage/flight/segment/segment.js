$(function () {
    loadData();
})

//加载列表
function loadData(pageIndex) {
    var carrierCode = $("#carrierCode").val();
    var fromKeyword = $("#fromKeyword").val();
    var toKeyword = $("#toKeyword").val();
    var layer_index = layer.load();
    $.post('flight/segment/list', {
        carrierCode: carrierCode,
        fromKeyword: fromKeyword,
        toKeyword: toKeyword,
        pageIndex: pageIndex,
    }, function (data) {
        layer.close(layer_index);
        $("#segment_list").html(data);
    });
}

//查找用户
function search_segment() {
    loadData();
}

//重置
function reset_segment() {
    $("#carrierCode").val("");
    $("#fromKeyword").val("");
    $("#toKeyword").val("");
    loadData();
}


