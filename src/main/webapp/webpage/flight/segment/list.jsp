<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="text-align: center"><label>航司</label></th>
        <th><label>航班号</label></th>
        <th><label>出发城市名称 </label></th>
        <th><label>出发城市三字码</label></th>
        <th><label>到达城市名称</label></th>
        <th><label>到达城市三字码</label></th>
        <th><label>出发时间</label></th>
        <th><label>到达时间</label></th>
        <th><label>飞行时长</label></th>
        <th><label>经停次数</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="segment">
        <tr>
            <td style="text-align: center">${carrierMap[fn:substring(segment.flightNo,0,2)]}</td>
            <td>${segment.flightNo }</td>
            <td>${segment.fromCityName }</td>
            <td>${segment.fromCityCode }</td>
            <td>${segment.toCityName }</td>
            <td>${segment.toCityCode }</td>
            <td><fmt:formatDate value="${segment.fromDate}" pattern="yyyy-MM-dd HH:mm" ></fmt:formatDate></td>
            <td><fmt:formatDate value="${segment.toData}" pattern="yyyy-MM-dd HH:mm" ></fmt:formatDate></td>
            <td>${segment.flightDuration }</td>
            <td>${segment.stopCount }</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=loadData"></jsp:include>
</div>