<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="aircraftMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
				<span class="glyphicon glyphicon-home" aria-hidden="true"></span>机型
				&nbsp;&nbsp;
			</div>
			<div id="rightDetail" class="col-md-12 animated fadeInRight">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div class="pull-left">
											<a class="btn btn-default" onclick="getAircraft('')" role="button">新增机型</a> 
											<a class="btn btn-default" onclick="batchDelete()" role="button">批量删除</a>
										</div>
										<!-- 查询条件 -->
										<form class="form-inline" method="post">
											<nav class="text-right">
												<div class="form-group">
													<label class="control-label">机型：</label> <select
														class="form-control" id="type" name="type"
														style="min-width: 100px">
														<option value="">全部</option>
														<c:if test="${not empty typeList }">
															<c:forEach items="${typeList }" var="type">
																<option value="${type.itemCode }">${type.itemTxt }</option>
															</c:forEach>
														</c:if>
													</select>
												</div>
												<div class=" form-group">
													<label class="control-label">其他：</label>
													 <input name="keyword" id="keyword" type="text"
														value="${startKeyword }" class="form-control"
														placeholder="编码/名称" style="width: 250px" />
												</div>
												<div class="form-group">
													<button type="button" class="btn btn-default"
														onclick="search_aircraft()">查询</button>
													<button type="button" class="btn btn-default"
														onclick="reset_search()">重置</button>
												</div>
											</nav>
										</form>
									</div>
								</div>
								<div id=aircraft_Table style="margin-top: 15px"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 详细页面 -->
<div id="aircraftDetail" style="display: none;"></div>
<script type="text/javascript" src="webpage/flight/aircraft/aircraft.js?version=${globalVersion}"></script>