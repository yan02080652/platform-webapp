<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 

<table class="table table-striped table-bordered table-hover" id="aircraftTable">
<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectAll()" id="selectAll"/></th>
          <th><label>编号</label></th>
	      <th><label>名称 </label></th>
          <th><label>类型</label></th>
          <th><label>最少座位</label></th>
          <th><label>最多座位</label></th>
          <th><label>机场建设费</label></th>
          <th><label>操作</label></th>
        </tr>
 </thead>

 <tbody>
        <c:forEach items="${pageList.list }" var="aircraft">
         <tr class="odd gradeX">
        	 <td><input type="checkbox" name="ids" value="${aircraft.id }"/></td>
             <td>${aircraft.code }</td>
        	 <td>${aircraft.name }</td>
        	 <td class="aircraftType">${aircraft.type }</td>
             <td>${aircraft.minSeat }</td>
             <td>${aircraft.maxSeat }</td>
             <td>
             	<c:choose>
             		<c:when test="${aircraft.isCollectAirportTax }">否</c:when>
             		<c:otherwise>是</c:otherwise>
             	</c:choose>
            </td>
             <td class="text-center">
             	<a onclick="getAircraft('${aircraft.id}')" >修改</a>&nbsp;&nbsp;
             	<a onclick="deleted('${aircraft.id}');">删除</a>
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=search_aircraft"></jsp:include> 
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>