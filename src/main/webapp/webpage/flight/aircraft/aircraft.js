var typeArray = {};//机型类型

$(function(){
	init(1);
})

function init(pageIndex){
	$.post("flight/aircraft/getAircraftType",function(data){
		$(data).each(function(){
			typeArray[this.itemCode] = this.itemTxt;
		})
		reloadRight(pageIndex);
	});
}

//加载列表
function reloadRight(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	var keyword = $("#keyword").val();
	var type = $("#type").val();
	$.post('flight/aircraft/list',{
		keyword:keyword,
		pageIndex:pageIndex,
		name:keyword,
		type:type
		},function(data){
		$("#aircraft_Table").html("");
		$("#aircraft_Table").html(data);
		
		$.each($('.aircraftType'),function(i,dom){
			var txt = $(dom).text();
			if(txt){
				$(dom).text(typeArray[txt]);
			}
		});		
	});
}

//查找用户
function search_aircraft(pageIndex){
	init(pageIndex);
}

//重置
function reset_search(){
	$("#keyword").val("");
	$("#type").val("");
	init();
}

//转到添加或修改页面
function getAircraft(id){
	$.post("flight/aircraft/getAircraft",{'id':id},function(data){
		$("#aircraftDetail").html(data);
		$("#aircraftMain").hide();
		$("#aircraftDetail").show();
	});
}

//批量删除
function batchDelete(){
    //判断至少选了一项
    var checkedNum = $("#aircraftTable input[name='ids']:checked").length;
    if(checkedNum==0){
        $.alert("请选择你要删除的记录!","提示");
        return false;
    }
    
    var ids = new Array();
    $("#aircraftTable input[name='ids']:checked").each(function(){
    	ids.push($(this).val());
    });
    
	  deleteAircraft(ids.toString());
}

//单个删除
function deleted(id) {
	deleteAircraft(id);
};

//确认删除
function deleteAircraft(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除所选记录?",
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "flight/aircraft/delete",
				data : {
					id : id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						init();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

//checkbox全选
function selectAll(){
	if ($("#selectAll").is(":checked")) {
        $(":checkbox").prop("checked", true);//所有选择框都选中
    } else {
        $(":checkbox").prop("checked", false);
    }
}
//返回
function backMain(){
	$("#aircraftMain").show();
	$("#aircraftDetail").hide().html("");
}



