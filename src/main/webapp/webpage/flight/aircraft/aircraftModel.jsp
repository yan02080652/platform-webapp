<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>机型信息<c:choose><c:when test="${aircraftDto.id != '' && aircraftDto.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
             	 &nbsp;&nbsp;<a data-pjax onclick="backMain()">返回</a>
             </div>
             <div class="panel-body">
                 <form id="aircraftForm" class="form-horizontal bv-form" method="post" action="flight/aircraft/save" enctype="multipart/form-data"> 
                 	<input type="hidden" name="id" id="id" value="${aircraftDto.id }"/>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">编号</label>
					    <div class="col-sm-6">
					      <input type="text" name="code" id="code" class="form-control" value="${aircraftDto.code }" placeholder="编号">
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label class="col-sm-2 control-label">名称</label>
					   <div class="col-sm-6">
					      <input type="text" name="name" class="form-control" value="${aircraftDto.name }" placeholder="名称">
					    </div>
					  </div>

					<div class="form-group">
					    <label class="col-sm-2 control-label">类型</label>
					    <div class="col-sm-6">
							<select class="form-control" id="type" name="type" style="min-width: 100px">
								<option value="">全部</option>
								<c:if test="${not empty typeList }">
									<c:forEach items="${typeList }" var="type">
										<option value="${type.itemCode }">${type.itemTxt }</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
					  </div>

					<div class="form-group">
					    <label class="col-sm-2 control-label">最小座位数</label>
					    <div class="col-sm-6">
					      <input type="number" name="minSeat" class="form-control" value="${aircraftDto.minSeat }" placeholder="最小座位数">
					    </div>
					  </div>
					  
					  <div class="form-group">
					    <label class="col-sm-2 control-label">最大座位数</label>
					    <div class="col-sm-6">
					      <input type="number" name="maxSeat" class="form-control" value="${aircraftDto.maxSeat }" placeholder="最大座位数">
					    </div>
					  </div>
					  
					  <div class="form-group">
						<label class="col-sm-2 control-label">是否收取机场建设费</label>
						<div class="col-sm-6">
							<label class="radio-inline"><input type="radio" name="isCollectAirportTax" value="0" checked="checked"/>是</label> 
							<label class="radio-inline"><input type="radio" name="isCollectAirportTax" value="1" />否</label>
						</div>
					</div>
					  
				</form>
				<div class="form-group">
					<div class="col-sm-offset-7" >
						<a class="btn btn-default"  onclick="submitAircraft()">提交</a>
					</div>
				</div>
			</div>
         </div> 
	</div>
</div>
<script type="text/javascript">

//提交表单
function submitAircraft(){
	
	 $('#aircraftForm').submit();
}

$(document).ready(function() {
	$("#aircraftForm").validate({
		rules : {
		name:{
			required : true,//名称必填
			}
        },
        messages:{
            code:{
               remote:"此编码已经存在"
            }
        },
        submitHandler : function(form) {
       	 $('#aircraftForm').ajaxSubmit({success:function(data){
    		 if (data.type == 'success') {
					$("#aircraftMain").show();
					$("#aircraftDetail").hide();
					reloadRight();
				} else {
					showErrorMsg(data.txt);
				}	 
    	 }});
    },
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight
    });
	//修改时code字段只读并删除code字段验证规则
	var code = $("#code").val();
	if(code){
		$("#code").attr("readonly",true);
		$("#code").rules("remove","remote");
	}	
	
	//设置checkbox的值
	if('${aircraftDto.isCollectAirportTax}' == 'true'){
		$("input[name='isCollectAirportTax'][value='1']").attr("checked",true);
	}else{
		$("input[name='isCollectAirportTax'][value='0']").attr("checked",true);
	}
	
	
});
</script>