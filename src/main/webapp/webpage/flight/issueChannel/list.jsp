<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table  table-bordered ">
    <thead>
    <tr>

            <th style="text-align:center;vertical-align:middle;"><label>tmc名称</label></th>

        <th style="text-align:center;vertical-align:middle;"><label>渠道类型</label></th>
        <th><label>渠道名称</label></th>
        <th><label>线上/线下</label></th>
        <th><label>默认收支渠道</label></th>
        <th><label>编码</label></th>
        <th><label>最后修改人</label></th>
        <th><label>最后修改时间</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>

    <tbody>

<c:choose>
    <c:when test="${admin}">
        <%--超级管理员--%>
        <c:forEach items="${dataMap}" var="map">
            <tr>
            <td style="text-align:center;vertical-align:middle;" rowspan="${countMap[map.key]}">${map.key}</td>
            <c:forEach items="${map.value}" var="channelMap" varStatus="vs">
                <td style="text-align:center;vertical-align:middle;" rowspan="${fn:length(channelMap.value)}">${channelMap.key}</td>
                <c:forEach items="${channelMap.value}" var="channel" varStatus="nel">
                    <td style="text-align:center;vertical-align:middle;">${channel.name}</td>
                    <td ><font <c:if test="${channel.lineType == '0'}">color="red"</c:if> >${lineTypes[channel.lineType]}</font></td>
                    <td style="text-align:center;vertical-align:middle;">${channel.settleChannelName}</td>
                    <td style="text-align:center;vertical-align:middle;">${channel.code}</td>
                    <td style="text-align:center;vertical-align:middle;">${channel.updatorName}</td>
                    <td style="text-align:center;vertical-align:middle;">
                        <c:if test="${not empty channel.updator}">
                            <fmt:formatDate value="${channel.updateTime}" pattern="yyy-MM-dd HH:mm" />
                        </c:if>
                    </td>

                    <td class="text-center">
                        <a onclick="issueChannel.detail.detailPage(${channel.id})">修改</a>&nbsp;&nbsp;
                        <a onclick="issueChannel.list.delete('${channel.id }');">删除</a>
                    </td>
                    </tr>
                </c:forEach>
            </c:forEach>
        </c:forEach>
    </c:when>
    <c:otherwise>
        <!-- 非超级管理员 -->
        <c:forEach items="${dataMap}" var="map">
            <tr>
            <td style="text-align:center;vertical-align:middle;" rowspan="${countMap[map.key]}">${map.key}</td>
            <c:forEach items="${map.value}" var="channelMap" varStatus="vs">
                <td style="text-align:center;vertical-align:middle;" rowspan="${fn:length(channelMap.value)}">${channelMap.key}</td>
                <c:forEach items="${channelMap.value}" var="channel" varStatus="nel">

                    <td style="text-align:center;vertical-align:middle;">${channel.name}</td>
                    <td><font <c:if test="${channel.lineType == '0'}">color="red"</c:if> >${lineTypes[channel.lineType]}</font></td>
                    <td style="text-align:center;vertical-align:middle;">${channel.settleChannelName}</td>
                    <td style="text-align:center;vertical-align:middle;">${channel.code}</td>
                    <td style="text-align:center;vertical-align:middle;">${channel.updatorName}</td>
                    <td style="text-align:center;vertical-align:middle;">
                        <c:if test="${not empty channel.updator}">
                            <fmt:formatDate value="${channel.updateTime}" pattern="yyy-MM-dd HH:mm" />
                        </c:if>
                    </td>

                    <td class="text-center">
                        <a onclick="issueChannel.detail.detailPage(${channel.id})">修改</a>&nbsp;&nbsp;
                        <a onclick="issueChannel.list.delete('${channel.id }');">删除</a>
                    </td>
                    </tr>
                </c:forEach>
            </c:forEach>
        </c:forEach>

    </c:otherwise>
</c:choose>

    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=issueChannel.list.reloadList"></jsp:include>
</div>