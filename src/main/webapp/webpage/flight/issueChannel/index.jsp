<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div >
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 出票渠道
            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <a class="btn btn-default" onclick="issueChannel.detail.detailPage()" role="button">新增出票渠道</a>
                                        </div>
                                        <!-- 查询条件 -->
                                        <form class="form-inline" method="post">
                                            <nav class="text-right">
                                                <c:if test="${admin}">
                                                    <label class="control-label">tmc：</label>
                                                    <input type="text" id="tmcName" class="form-control tmc"  placeholder="所属tmc" readonly>
                                                    <input type="hidden" id="tmcId">
                                                </c:if>
                                                <c:if test="${admin}">
                                                    <label class="control-label">线上/线下:</label>
                                                    <select  class="form-control" id="lineType">
                                                        <option value="">---</option>
                                                        <c:forEach items="${lineTypes}" var="type">
                                                            <option value="${type.key}">${type.value}</option>
                                                        </c:forEach>
                                                    </select>
                                                </c:if>
                                                <label class="control-label">渠道类型：</label>
                                                <select  class="form-control" id="channelType">
                                                        <option value="">---</option>
                                                    <c:forEach items="${typeMap}" var="type">
                                                        <option value="${type.key}">${type.value}</option>
                                                    </c:forEach>
                                                </select>


                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default" onclick="issueChannel.list.reloadList()">查询</button>
                                                    <button type="button" class="btn btn-default" onclick=" issueChannel.list.reset()">重置</button>
                                                </div>
                                            </nav>
                                        </form>
                                    </div>
                                </div>
                                <div id="listData" style="margin-top: 15px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="detailDiv"></div>

<script type="text/javascript" src="webpage/flight/issueChannel/issueChannel.js?version=${globalVersion}"></script>
<script>
    $(function(){
        issueChannel.list.init();
    })
</script>