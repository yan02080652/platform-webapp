var issueChannel = {
    url: {
        list: function () {
            return 'flight/issueChannel/list';
        },
        delete: function (id) {
            return 'flight/issueChannel/delete/' + id;
        },
        save: function () {
            return 'flight/issueChannel/save';
        },
        detail: function (id) {
            if (id) {
                return "flight/issueChannel/detail?id=" + id;
            } else {
                return "flight/issueChannel/detail";
            }
        }
    },

//=====================================列表页===============================================
    list: {
        init: function () {
            issueChannel.list.reloadList();
            $(".tmc").click(function () {
                TR.select('partner', {
                    type: 1
                }, function (data) {

                    $("#tmcId").val(data.id);
                    $("#tmcName").val(data.name);

                    issueChannel.list.reloadList();
                });
            });

        },
        reloadList: function (pageIndex) {
            var tmcId = $.trim($("#tmcId").val());
            var channelType = $("#channelType").val();
            var lineType = $("#lineType").val();
            $.post(issueChannel.url.list(), {
                pageIndex: pageIndex,
                tmcId: tmcId,
                channelType: channelType,
                lineType:lineType,
            }, function (data) {
                $("#listData").html(data);
            });
        },
        reset: function () {
            $("#tmcId").val("");
            $("#tmcName").val("");
            $("#channelType").find("option").eq(0).prop("selected",true);
            issueChannel.list.reloadList();
        },
        delete: function (id) {
            $.confirm({
                title: '提示',
                confirmButton: '确认',
                cancelButton: '取消',
                content: "确认删除所选记录?",
                confirm: function () {
                    $.ajax({
                        cache: true,
                        type: "POST",
                        url: issueChannel.url.delete(id),
                        async: false,
                        error: function (request) {
                            $.alert("删除失败,请刷新重试！", "提示");
                        },
                        success: function (result) {
                            if (result == 'success') {
                                issueChannel.list.reloadList();
                            } else if(result == 'exist'){
                                $.alert("该供应商与连接存在关联，无法删除", "提示");
                            } else {
                                $.alert("删除失败,请刷新重试！", "提示");
                            }
                        }
                    });
                },
                cancel: function () {
                }
            });
        },
    },
    //=======================================详情页=================================================//

    detail: {
        showModal: function (url, callback) {
            $('#detailDiv').load(url, function (context, state) {
                if ('success' == state && callback) {
                    callback();
                }
            });
        },

        detailPage: function (id) {
            issueChannel.detail.showModal(issueChannel.url.detail(id), function () {
                $('#issueChannelModel').modal();
                issueChannel.detail.initForm();
            });
        },

        init:function () {
            if($("#admin").val() == 'true') {
                $(".tmc").click(function () {
                    TR.select('partner', {
                        type: 1
                    }, function (data) {
                        $("[name='tmcName']").val(data.name)
                        $("[name='tmcId']").val(data.id)
                    });
                });
            } else {
                $("[name='tmcName']").val($("#name").val());
                $("[name='tmcId']").val($("#id").val());
                $("[name='tmcName']").prop("readonly",true);
            }

            $("input[name='name']").blur(function(){
                if($("input[name='code']").val() == '') {
                    $("input[name='code']").val(pinyin.getCamelChars($("input[name='name']").val()).toLowerCase());
                }
            });

        },

        initForm: function () {
            var rules = {
                name: {
                    required: true
                },
                code: {
                    required: true
                },
                tmcName: {
                    required: true
                },
                channelType : {
                    required: true
                },
                lineType : {
                    required: true
                },
                settelChannelId : {
                    required: true
                },
            };

            //添加必填标记
            $.each(rules, function (code, rule) {
                if (rule.required || rule.type) {
                    issueChannel.detail.addReqMark(code);
                }
            });

            $("#issueChannelForm").validate({
                rules: rules,
                errorPlacement: setErrorPlacement,
                success: validateSuccess,
                highlight: setHighlight,
                submitHandler: function (form) {
                    console.log($(form).serialize());
                    $.ajax({
                        type: "POST",
                        url: issueChannel.url.save(),
                        data: $(form).serialize(),
                        success: function (result) {
                            if (result == 'success') {
                                $('#issueChannelModel').modal('hide');
                                issueChannel.list.reloadList();
                            } else if (result == 'exist') {
                                
                                $.alert("保存失败:该业务类型已经存在相同的渠道编码");

                            } else {
                                $.alert("请求失败，请刷新重试！");
                            }
                        },
                        error: function (request) {
                            $.alert("请求失败，请刷新重试！");
                        },
                    });
                }
            });
        },
        addReqMark: function (code, addRule) {
            var requiredMark = $("<span class='reqMark'>*</span>");
            var element = $("#issueChannelForm").find("[name=" + code + "]");
            var label = element.parentsUntil('.form-group').parent().children('.control-label');
            if ($(".reqMark", label).length == 0) {
                label.append(requiredMark);
            }
            if (addRule) {
                element.rules('add', addRule);
            }
        },

        saveForm: function () {
            $("#issueChannelForm").submit();
        }
    }

};

