<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .popover.right > .arrow {
        top: 50% !important;
    }

    .control-label .reqMark {
        color: red;
        font-weight: bold;
        font-size: 15px;
    }

    #freightForm .btn-success {
        background-color: #46A2BB;
        border-color: #46A2BB;
    }
</style>

<div class="modal fade" id="issueChannelModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">
                    <c:choose>
                        <c:when test="${not empty issueChannelDto.id}">
                            修改
                        </c:when>
                        <c:otherwise>
                            新增
                        </c:otherwise>
                    </c:choose>
                </h4>
            </div>
            <div class="modal-body">
                <form id="issueChannelForm" class="form-horizontal">
                    <input type="hidden" name="id"  value="${issueChannelDto.id}">
                    <input type="hidden" name="tmcId"  value="${issueChannelDto.tmcId}">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">类型</label>
                        <div class="col-sm-6">
                            <select name="channelType" class="form-control">
                                <option value="">-请选择-</option>
                                <c:forEach items="${typeMap}" var="type">
                                    <option value="${type.key}" <c:if test="${type.key == issueChannelDto.channelType}">selected="selected"</c:if> >${type.value}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">名称</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="name" value="${issueChannelDto.name}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">线上/线下</label>
                        <div class="col-sm-6">
                            <select name="lineType" class="form-control">
                                <option value="">-请选择-</option>
                                <c:forEach items="${lineTypes}" var="type">
                                    <option value="${type.key}" <c:if test="${type.key == issueChannelDto.lineType}">selected="selected"</c:if> >${type.value}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label">默认收支渠道</label>
                        <div class="col-sm-6">
                            <select name="settelChannelId" class="form-control">
                                <option value="">-请选择-</option>
                                <c:forEach items="${settleChannels}" var="channel">
                                    <option value="${channel.id}" <c:if test="${channel.id == issueChannelDto.settelChannelId}">selected="selected"</c:if> >${channel.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">编码</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="code" value="${issueChannelDto.code}" <c:if test="${not empty issueChannelDto.id}">readonly</c:if>  >
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="issueChannel.detail.saveForm()">保存
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="webpage/flight/issueChannel/issueChannel.js?version=${globalVersion}"></script>
<script type="text/javascript" src="/resource/js/tr-widget/getPingyin.js?version=${globalVersion}"></script>

<input type="hidden" id="admin" value="${admin}">
<input type="hidden" id="id" value="${partner.id}">
<input type="hidden" id="name" value="${partner.name}">

<script>
    $(function () {
        issueChannel.detail.init();
    })
</script>