<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<style>
    .word{
        word-wrap:break-word;word-break:break-all;
    }
</style>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="width: 80px"><label>供应商</label></th>
        <th style="width: 80px"><label>方法名</label></th>
        <th style="width: 80px"><label>请求时间</label></th>
        <th style="width: 200px"><label>请求参数</label></th>
        <th style="width: 100px"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="data">
        <tr class="odd gradeX">
            <td style="width: 80px" >${data.hubsCode}</td>
            <td style="width: 80px" >${data.methodName.msg}</td>
            <td style="width: 80px" ><fmt:formatDate value="${data.requestTime}" pattern="yyyy-MM-dd HH:mm:ss"></fmt:formatDate></td>
            <td style="width: 200px" class="word">${data.requestParam}</td>
            <td style="width: 100px"><button btn btn-default onclick="list.detail(${data.id})">详情</button></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=list.reloadList"></jsp:include>
</div>


