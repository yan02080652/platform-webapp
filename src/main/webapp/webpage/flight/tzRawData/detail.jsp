<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <h3>请求数据</h3>
            <p>
                <textarea style="width: 760px;height: 150px;">${log.requestXml}</textarea>
            </p>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <h3>响应数据</h3>
            <c:choose>
                <c:when test="${showDownload}">
                    <span>数据过大,请<a target="_blank" href='/flight/tzRawData/responseXml/download/${log.id}'>下载</a>查看</span>
                </c:when>
                <c:otherwise>
                    <p>
                        <textarea style="width: 760px;height: 150px;">${log.responseXml}</textarea>
                    </p>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
