var url = {
    list: function () {
        return 'flight/tzRawData/list';
    },
    detail: function (id) {
        return "flight/tzRawData/detail/" + id;
    },
    paramsList: function () {
        return 'flight/tzRawData/paramsList';
    }
}
var list = {
    init: function () {
        $(".time-picker").jeDate({
            format: "YYYY-MM-DD hh:mm:ss"
        });
        $('.input-group-addon').click(function () {
            var preDom = $(this).prev();
            if (preDom.hasClass('time-picker')) {
                preDom.trigger('click');
            }
        });
        list.reloadList();
    },

    bizChange: function () {
        var bizType = $("#bizType").val();
        $.get(url.paramsList(), {bizType: bizType}, function (result) {
            var hubsHtml = '<option value="">---</option>';
            $.each(result.hubsList, function (i, n) {
                hubsHtml += ' <option value="' + n + '">' + n + '</option>';
            })
            $("#hubsCode").empty().append(hubsHtml);

            var methodHtml = '<option value="">---</option>';
            $.each(result.methodList, function (i, n) {
                methodHtml +=' <option value="' + n.name + '">' + n.msg + '</option>';
            })
            $("#methodName").empty().append(methodHtml);
        })
    },

    reset: function () {
        document.getElementById("searchForm").reset()
        list.reloadList();
    },

    reloadList: function (pageIndex) {
        var hubsCode = $("#hubsCode").val();
        var startTime = $("#startTime").val();
        var endTime = $("#endTime").val();
        var endTime = $("#endTime").val();
        var bizType = $("#bizType").val();
        var methodName = $("#methodName").val();

        $.get(url.list(), {
            pageIndex: pageIndex,
            hubsCode: hubsCode,
            startTime: startTime,
            endTime: endTime,
            bizType: bizType,
            methodName: methodName,
        }, function (data) {
            $("#listData").html(data);
        });
    },

    detail: function (id) {
        layer.open({
            type: 2,
            area: ['800px', '500px'],
            title: "详情",
            closeBtn: 1,
            shadeClose: true,
            content: url.detail(id)
        });
    },

}
