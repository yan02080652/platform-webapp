<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
    .popover.right>.arrow{
        top:50% !important;
    }
    .control-label .reqMark{
        color: red;
        font-weight: bold;
        font-size: 15px;
    }
    #freightForm .input-group{
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }
    #freightForm .input-group-addon {
        border-left: none;
        border-bottom-right-radius:4px;
        border-top-right-radius:4px;
        cursor: pointer;
    }
    #freightForm .btn-success{
        background-color: #46A2BB;
        border-color: #46A2BB;
    }
</style>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        <c:choose>
            <c:when test="${not empty freightDto.id}">
                修改
            </c:when>
            <c:otherwise>
                新增
            </c:otherwise>
        </c:choose>
        国内运价&nbsp;<a href="flight/freight">返回</a>
    </div>
    <div class="panel-body">
        <form id="freightForm" class="form-horizontal">
            <input type="hidden" name="id" class="form-control" value="${freightDto.id}">
            <div class="form-group">
                <label class="col-sm-2 control-label">适用航空公司</label>
                <div class="col-sm-2">
                    <input type="text"  class="form-control" id="carrierCode" name="carrierCode" value="${freightDto.carrierCode}" placeholder="示例:MU">
                </div>
                <div class="col-sm-4 input-group carrier" >
                    <input type="text" name="carrierName" id="carrierName" class="form-control" value="${freightDto.carrierName}" placeholder="航司名称" readonly>
                    <span class="input-group-addon"><i class="fa fa-plane"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">起飞机场</label>
                <div class="col-sm-2">
                    <input type="text" name="fromAirport" class="form-control" placeholder="示例：SHA" value="${freightDto.fromAirport}">
                </div>
                <div class="col-sm-4 input-group airport">
                    <input type="text" name="fromAirportName" class="form-control" value="${freightDto.fromAirportName}" placeholder="请选择起飞机场" readonly>
                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">到达机场</label>
                <div class="col-sm-2">
                    <input type="text" name="toAirport" class="form-control" placeholder="示例：PEK" value="${freightDto.toAirport}">
                </div>
                <div class="col-sm-4 input-group airport">
                    <input type="text" name="toAirportName" class="form-control" value="${freightDto.toAirportName}" placeholder="请选择到达机场" readonly>
                    <span class="input-group-addon"><i class="fa fa-building"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">舱位</label>
                <div class="col-sm-2">
                    <input type="text" name="cabinCode" class="form-control"  value="${freightDto.cabinCode}">
                </div>
                <div class="col-sm-4 input-group">
                    <select name="cabinGrade" class="form-control">
                        <c:forEach items="${cabinGrade}" var="grade">
                            <option value="${grade.itemCode}" <c:if test="${grade.itemCode eq freightDto.cabinGrade}">selected</c:if>>${grade.itemTxt}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">单程票面价</label>
                <div class="col-sm-2">
                    <input type="number" min="0" name="facePrice" class="form-control" value="${freightDto.facePrice}">
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>数值,正整数</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">往返票面价</label>
                <div class="col-sm-2">
                    <input type="number" min="0" name="doubleFacePrice" class="form-control" value="${freightDto.doubleFacePrice}">
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>默认单程票面价*2</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">单程全价票面价</label>
                <div class="col-sm-2">
                    <input type="number" min="0" name="fullPrice" class="form-control" value="${freightDto.fullPrice}">
                </div>
                <%--<div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">--%>
                    <%--<span>默认单程票面价*2</span>--%>
                <%--</div>--%>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">舱位折扣率</label>
                <div class="col-sm-2">
                    <input type="number" name="discount" class="form-control" value="${freightDto.discount}" readonly>
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>舱位折扣率=单程票面价/单程全价票面价</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">机场建设费</label>
                <div class="col-sm-2">
                    <input type="number" min="0" name="departureTax" class="form-control" value="${freightDto.departureTax}" >
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>默认:50</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">燃油费</label>
                <div class="col-sm-2">
                    <input type="number" min="0" name="fuelTax" class="form-control" value="${freightDto.fuelTax}" >
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>默认:0</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">出票开始/结束日期</label>
                <div class="col-sm-3 input-group">
                    <input type="text" name="ticketValidityStartDate" class="form-control time-picker"  placeholder="请输入开始日期"
                           value="<fmt:formatDate value='${freightDto.ticketValidityStartDate }' pattern="yyy-MM-dd"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="col-sm-3 input-group">
                    <input type="text" name="ticketValidityEndDate" class="form-control time-picker" placeholder="请输入截止日期"
                           value="<fmt:formatDate value='${freightDto.ticketValidityEndDate }' pattern="yyy-MM-dd"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">起飞开始/结束日期</label>
                <div class="col-sm-3 input-group">
                    <input type="text" name="takeOffValidityStartDate" class="form-control time-picker"  placeholder="请输入开始日期"
                           value="<fmt:formatDate value='${freightDto.takeOffValidityStartDate }' pattern="yyy-MM-dd"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="col-sm-3 input-group">
                    <input type="text" name="takeOffValidityEndDate" class="form-control time-picker" placeholder="请输入截止日期"
                           value="<fmt:formatDate value='${freightDto.takeOffValidityEndDate }' pattern="yyy-MM-dd"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">提前预订天数</label>
                <div class="col-sm-2">
                    <input type="number" name="minAdvanceDay" class="form-control"  value="${freightDto.minAdvanceDay}" placeholder="最小值" >
                </div>
                <div class="col-sm-2">
                    <input type="number" name="maxAdvanceDay" class="form-control"  value="${freightDto.maxAdvanceDay}" placeholder="最大值" >
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>0:代表无限制</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">去程适用航班</label>
                <div class="col-sm-2">
                    <input type="text" name="conformityFlights" class="form-control" value="${freightDto.conformityFlights}" >
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>实例：5173,多个航班以逗号","隔开</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">去程不适用航班</label>
                <div class="col-sm-2">
                    <input type="text" name="nonConformityFlights" class="form-control" value="${freightDto.nonConformityFlights}">
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>实例：5173,多个航班以逗号","隔开</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">去程适用班期</label>
                <div class="col-sm-10">
                    <c:forEach items="${weekMap}" var="week">
                        <label class='checkbox-inline'>
                            <input type=checkbox  name="week" value="${week.key}" <c:if test="${fn:contains(freightDto.conformityDate,week.key)}">checked</c:if>>${week.value}
                        </label>
                    </c:forEach>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">退改签规定</label>
                <div class="col-sm-3">
                    <input type="text" name="refundRule" class="form-control"  value="${freightDto.refundRule}" placeholder="退票" readonly>
                </div>
                <div class="col-sm-3">
                    <input type="text" name="changeRule" class="form-control"  value="${freightDto.changeRule}" placeholder="同舱改期" readonly>
                </div>
                <div class="col-sm-3 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>注:99表示无对应的退改签规则</span>
                </div>
            </div>
            <div class="form-group" >
                <label class="col-sm-2 control-label">是否可签转</label>
                <div class="col-sm-1" >
                    <label class="radio-inline"><input type="radio"  name="endorse" value="1" <c:if test="${freightDto.endorse}">checked</c:if>/>是</label>
                    <label class="radio-inline"><input type="radio"  name="endorse" value="0" <c:if test="${!freightDto.endorse}">checked</c:if>/>否</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">免费行李额</label>
                <div class="col-sm-2">
                    <input type="number" name="baggage" class="form-control"  value="${freightDto.baggage}">
                </div>
                <div class="col-sm-1 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>(单位:kg)</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">备注</label>
                <div class="col-sm-6">
                    <textarea class="form-control" rows="3" name="remark" >${freightDto.remark}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">是否有效</label>
                <div class="col-sm-2">
                    <label class="radio-inline"><input type="radio" name="status" value="1" <c:if test="${freightDto.status}">checked</c:if>/>有效</label>
                    <label class="radio-inline"><input type="radio" name="status" value="0" <c:if test="${!freightDto.status}">checked</c:if>/>无效</label>
                </div>
            </div>
            <div class="form-group" style="margin-left: 200px;  ">
                <div class="col-sm-2" style="text-align: right">
                    <button type="button" class="btn btn-success" style="width: 100px;" onclick="freight.detail.saveForm()">保存</button>
                </div>
                <div class="col-sm-2" style="text-align: right">
                    <button type="button" class="btn btn-success" style="width: 100px;" onclick="freight.detail.saveAndAdd()" >保存并新增</button>
                </div>
                <div class="col-sm-2" style="text-align: right">
                    <a type="button" class="btn btn-success" style="width: 100px;" href="flight/freight" >返回列表</a>
                </div>
            </div>
        </form>

    </div>
</div>
<script type="text/javascript" src="webpage/flight/freight/freight.js?version=${globalVersion}"></script>
<script>
    $(function(){
        freight.detail.init();
        if('${freightDto.id}' ==''){
            freight.detail.setDefaultValue();
        }else{
            freight.detail.formatNumber();
        }
    })
</script>