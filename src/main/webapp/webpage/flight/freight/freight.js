var freight = {
    onlyAdd: true,
    url: {
        list: function () {
            return 'flight/freight/list';
        },
        delete: function (id) {
            return 'flight/freight/delete/' + id;
        },
        carrierName: function () {
            return 'flight/change/getName';
        },
        save: function () {
            return 'flight/freight/save';
        },
        detail: function () {
            return 'flight/freight/detail';
        },
        index: function () {
            return 'flight/freight';
        },
        cabinGrade: function () {
            return 'flight/freight/getCabinGrade';
        },
        refundChangeRule: function () {
            return "flight/freight/refundChangeRule";
        }
    },

//=====================================列表页===============================================
    list: {
        init: function () {
            //航司选择器
            $(".carrier").click(function () {
                TR.select('flightCompany', {}, function (result) {
                    $("#carrierCode").val(result ? result.code : null);
                    $("#carrierName").val(result ? result.nameShort : null);
                    freight.list.reloadList();
                });
            });
            freight.list.reloadList();
        },
        reloadList: function (pageIndex) {
            var cabinCode = $.trim($("#cabinCode").val());
            var carrierCode = $.trim($("#carrierCode").val());
            var fromAirport = $.trim($("#fromAirport").val());
            var toAirport = $.trim($("#toAirport").val());
            $.post(freight.url.list(), {
                pageIndex: pageIndex,
                cabinCode: cabinCode,
                carrierCode: carrierCode,
                fromAirport: fromAirport,
                toAirport: toAirport,
            }, function (data) {
                $("#listData").html(data);
            });
        },
        reset: function () {
            $("#cabinCode").val("");
            $("#carrierCode").val("")
            $("#carrierName").val("")
            $("#fromAirport").val("")
            $("#toAirport").val("")
            freight.list.reloadList();
        },
        delete: function (id) {
            $.confirm({
                title: '提示',
                confirmButton: '确认',
                cancelButton: '取消',
                content: "确认删除所选记录?",
                confirm: function () {
                    $.ajax({
                        cache: true,
                        type: "POST",
                        url: freight.url.delete(id),
                        async: false,
                        error: function (request) {
                            $.alert("删除失败,请刷新重试！", "提示");
                        },
                        success: function (result) {
                            if (result == 'success') {
                                freight.list.reloadList();
                            } else {
                                $.alert("删除失败,请刷新重试！", "提示");
                            }
                        }
                    });
                },
                cancel: function () {
                }
            });
        },
    },
    //=======================================详情页=================================================//
    detail: {
        getNowDate: function () {
            var now = new Date();
            var year = now.getFullYear();
            var month = now.getMonth() + 1;
            var day = now.getDate();
            var nowDate = year + (month > 9 ? '-' : '-0') + month + (day > 9 ? '-' : '-0') + day;
            return nowDate;
        },
        getMaxDate: function () {
            var nowDate = freight.detail.getNowDate();
            var month_day = nowDate.substring(5, nowDate.length);
            return "2099-" + month_day;
        },
        setDefaultValue: function () {
            $("input[name='endorse'][value='0']").attr("checked", true);
            $("input[name='status'][value='1']").attr("checked", true);
            $("input[name='baggage']").val(20);
            $("input[name='minAdvanceDay']").val(0);
            $("input[name='maxAdvanceDay']").val(0);
            $("input[name='departureTax']").val(50);
            $("input[name='fuelTax']").val(0);
            $("input[name='ticketValidityStartDate']").val(freight.detail.getNowDate());
            $("input[name='ticketValidityEndDate']").val(freight.detail.getMaxDate());
            $("input[name='takeOffValidityStartDate']").val(freight.detail.getNowDate());
            $("input[name='takeOffValidityEndDate']").val(freight.detail.getMaxDate());
            $("input[type='checkbox']").attr("checked", true);
            $("[name='remark']").text("如有变动,以航空公司最新退改签规定为准");
        },
        companyCodeChange: function (companyCode) {
            var elCom = $("input[name='carrierName']");
            if (!companyCode || companyCode.length < 2) {
                elCom.val(null);
            } else {
                var com = freight.detail.getName('C', companyCode);
                elCom.val(com ? com.nameShort : null);
                $("[name='carrierCode']").val(companyCode.toLocaleUpperCase());
                freight.detail.cabinCodeChange($("[name='cabinCode']").val());
            }

        },
        cabinCodeChange: function (cabinCode) {
            $("[name='cabinCode']").val(cabinCode.toLocaleUpperCase());
            var carrierCode = $("input[name='carrierCode']").val();
            if (cabinCode && carrierCode && carrierCode.length == 2) {
                var grade = freight.detail.getGrade(carrierCode, cabinCode);
                if (grade) {
                    freight.detail.gradeChange(grade);
                    $("[name='cabinGrade']").val(grade);
                }
            }
        },
        //根据//根据航司编码+舱位编码查询退改签规则，计算对应字段值
        refundChangeRule: function () {
            var carrierCode = $("[name='carrierCode']").val();
            var cabinCode = $("[name='cabinCode']").val();
            var facePrice = $("[name='facePrice']").val();
            var fullPrice = $("[name='fullPrice']").val();
            if (!carrierCode || !cabinCode || !facePrice || !fullPrice) {
                return false;
            }

            $.ajax({
                type: "post",
                url: freight.url.refundChangeRule(),
                data: {
                    carrierCode: carrierCode,
                    cabinCode: cabinCode
                },
                success: function (result) {
                    if (result) {
                        var refundRule = result.refundRule;
                        var changeRule = result.changeRule;
                        var referenceBasis = result.referenceBasis;
                        var price;
                        if (referenceBasis == 1) {//票面价
                            price = facePrice;
                        } else {//标准价
                            price = fullPrice;
                        }

                        var obj = freight.detail.parseRule(refundRule, changeRule, price);

                        $("[name='refundRule']").val(obj.refundRule);
                        $("[name='changeRule']").val(obj.changeRule);
                        var endorse = result.refundRule ? 1 : 0;
                        $("input[name='endorse'][value=" + endorse + "]").attr("checked", true);
                        $("[name='remark']").val(result.remark);
                    } else {
                        $("[name='refundRule']").val(99);
                        $("[name='changeRule']").val(99);
                    }
                }
            })
        },
        calcPrice: function (Percentage, price) {
            return Math.ceil(parseFloat(Percentage) / 100 * price);
        },
        calcRule: function (array, price) {
            var rs = "";
            for (var i = 0; i < array.length; i += 2) {
                rs += freight.detail.calcPrice(array[i], price);
                if (i < array.length -1) {
                    rs += "-" + array[i + 1] +"-";
                }
            }
            return rs;
        },
        //根据退改签规则中的格式解析出运价中需要的格式数据
        parseRule: function (refundRule, changeRule, price) {
            var refundArray = refundRule.split("-");
            var changeArray = changeRule.split("-");
            var newRefundRule = freight.detail.calcRule(refundArray, price);
            var newChangeRule = freight.detail.calcRule(changeArray, price);
            var obj = {
                refundRule: newRefundRule,
                changeRule: newChangeRule
            }
            return obj;

        },
        //根据航司编码+舱位编码获取等级
        getGrade: function (carrierCode, cabinCode) {
            if (!carrierCode || !cabinCode) {
                return null;
            }
            var rs = null;
            $.ajax({
                type: "post",
                url: freight.url.cabinGrade(),
                data: {
                    carrierCode: carrierCode,
                    cabinCode: cabinCode
                },
                async: false,
                success: function (data) {
                    rs = data;
                }
            });
            return rs;
        },
        gradeChange: function (grade) {
            if (grade == 'Y') {
                $("[name='baggage']").val(20);
            } else {
                $("[name='baggage']").val(40);
            }
        },
        getName: function (type, code) {
            if (!code) {
                return null;
            }
            var rs = null;
            $.ajax({
                url: freight.url.carrierName(),
                data: {
                    type: type,
                    code: code
                },
                dataType: 'json',
                async: false,
                success: function (data) {
                    rs = data;
                }
            });
            return rs;
        },
        init: function () {
            var form = $('#freightForm');
            //时间选择器
            $(".time-picker").jeDate({
                format: "YYYY-MM-DD"
            });
            $('.input-group-addon').click(function () {
                var preDom = $(this).prev();
                if (preDom.hasClass('time-picker')) {
                    preDom.trigger('click');
                }
            });

            //航司选择器
            $(".carrier").click(function () {
                TR.select('flightCompany', {}, function (result) {
                    $("#carrierName").val(result ? result.nameShort : null);
                    $("#carrierCode").val(result ? result.code : null);
                    freight.detail.cabinCodeChange($("[name='cabinCode']").val());
                    freight.detail.refundChangeRule();
                });
            })

            form.find('[name=carrierCode]').change(function () {
                freight.detail.companyCodeChange(this.value);
            });
            form.find('[name=cabinCode]').change(function () {
                freight.detail.cabinCodeChange(this.value);
            });
            form.find('[name=cabinGrade]').change(function () {
                freight.detail.gradeChange(this.value);
            });

            freight.detail.calcDiscount();

            form.find('[name=cabinCode],[name=carrierCode],[name=facePrice],[name=fullPrice]').change(function () {
                freight.detail.refundChangeRule();
            });

            //机场选择器
            $(".airport").click(function () {
                var curDiv = $(this);
                TR.select('airPort', {}, function (result) {
                    var code = result ? result.code : null;
                    var name = result ? result.name : null;
                    curDiv.find(":first-child").val(name)
                    curDiv.prev().find(":first-child").val(code)
                });
            })

            //输入机场编码带出机场名称
            var airportCodes = {
                fromAirport: 'fromAirportName',
                toAirport: 'toAirportName',
            };
            $.each(airportCodes, function (code, value) {
                form.find("[name=" + code + "]").change(function () {
                    var airport = freight.detail.getName('F', this.value);
                    form.find("[name=" + value + "]").val(airport ? airport.nameCn : null);
                    $("[name=" + code + "]").val(this.value.toLocaleUpperCase());
                });
            });

            freight.detail.initForm();
        },
        initForm: function () {
            var rules = {
                carrierName: {
                    required: true
                },
                fromAirportName: {
                    required: true
                },
                toAirportName: {
                    required: true
                },
                cabinCode: {
                    required: true
                },
                facePrice: {
                    required: true
                },
                doubleFacePrice: {
                    required: true
                },
                fullPrice: {
                    required: true
                },
                departureTax: {
                    required: true
                },
                fuelTax: {
                    required: true
                },
            };

            //添加必填标记
            $.each(rules, function (code, rule) {
                if (rule.required || rule.type) {
                    freight.detail.addReqMark(code);
                }
            });

            $("#freightForm").validate({
                rules: rules,
                errorPlacement: setErrorPlacement,
                success: validateSuccess,
                highlight: setHighlight,
                submitHandler: function (form) {
                    $.ajax({
                        type: "POST",
                        url: freight.url.save(),
                        data: $(form).serialize(),
                        success: function (result) {
                            if (result == 'success') {
                                if (freight.onlyAdd) {
                                    location.href = freight.url.index();
                                } else {
                                    location.href = freight.url.detail();
                                }
                            } else if (result == 'exist') {
                                $.alert("保存失败:已存在相同运价！");
                            } else {
                                $.alert("请求失败，请刷新重试！");
                            }
                        },
                        error: function (request) {
                            $(form).find("[name='endorse']").attr("disabled", true);
                            $.alert("请求失败，请刷新重试！");
                        },
                    });
                }
            });
        },
        addReqMark: function (code, addRule) {
            var requiredMark = $("<span class='reqMark'>*</span>");
            var element = $("#freightForm").find("[name=" + code + "]");
            var label = element.parentsUntil('.form-group').parent().children('.control-label');
            if ($(".reqMark", label).length == 0) {
                label.append(requiredMark);
            }
            if (addRule) {
                element.rules('add', addRule);
            }
        },
       /* defaultPrice: function () {
            $("[name='facePrice']").change(function () {
                var facePrice = $(this).val();
                var fullPrice = $("[name='fullPrice']").val();
                if (facePrice.length > 0 && !isNaN(facePrice)) {
                    $("[name='doubleFacePrice']").val(facePrice * 2);
                    // fullPrice.val(facePrice * 2);
                    if (facePrice == 0 || fullPrice == 0) {
                        $("[name='discount']").val(0);
                    } else {
                        $("[name='discount']").val((facePrice / fullPrice).toFixed(2));
                    }
                }
            })
        },*/
        calcDiscount: function () {
            $("[name='fullPrice'],[name='facePrice']").change(function () {
                var facePrice = $("[name='facePrice']").val();
                var fullPrice = $("[name='fullPrice']").val();
                $("[name='doubleFacePrice']").val(facePrice * 2);
                if (facePrice == 0 || fullPrice == 0) {
                    $("[name='discount']").val(0);
                } else {
                    $("[name='discount']").val((facePrice / fullPrice).toFixed(2));
                }
            })
        },
        formatNumber: function () {
            var formatField = ["facePrice", "fullPrice", "doubleFacePrice", "departureTax", "fuelTax"];
            $.each(formatField, function (i, name) {
                var element = $("[name=" + name + "]");
                var price = element.val();
                element.val(parseInt(price));
            });
        },
        saveForm: function () {
            freight.onlyAdd = true;
            $("#freightForm").submit();
        },
        saveAndAdd: function () {
            freight.onlyAdd = false;
            $("#freightForm").submit();
        }
    }
};

