<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th><label>序号</label></th>
        <th><label>航司</label></th>
        <th><label>起飞机场</label></th>
        <th><label>到达机场</label></th>
        <th><label>舱位</label></th>
        <th><label>单程票面价</label></th>
        <th><label>全价票面价</label></th>
        <th><label>往返票面价</label></th>
        <th><label>退票规定</label></th>
        <th><label>同舱改期规定</label></th>
        <th><label>最后更新时间</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="freight" varStatus="vs">
        <tr class="odd gradeX">
            <td>${vs.count}</td>
            <td>${freight.carrierCode }</td>
            <td>${freight.fromAirport }</td>
            <td>${freight.toAirport }</td>
            <td>${freight.cabinCode }</td>
            <td><fmt:formatNumber value="${freight.facePrice}" /></td>
            <td><fmt:formatNumber value="${freight.fullPrice}" /></td>
            <td><fmt:formatNumber value="${freight.doubleFacePrice}" /></td>
            <td>${freight.refundRule}</td>
            <td>${freight.changeRule}</td>
            <td><fmt:formatDate value="${freight.lastUpdateDate }" pattern="yyy-MM-dd HH:mm:ss"/></td>
            <td class="text-center">
                <a href="flight/freight/detail?id=${freight.id}">修改</a>&nbsp;&nbsp;
                <a onclick="freight.list.delete('${freight.id }');">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=freight.list.reloadList"></jsp:include>
</div>