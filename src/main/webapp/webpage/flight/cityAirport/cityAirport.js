var cityArray={};//城市列表
var airportArray={};//机场列表
$(function(){
	init();
})


function loadData(callback){
	$.post("flight/cityAirport/getCitysAndAirport",function(data){
		$(data.cityList).each(function(){
			cityArray[this.code] = this.nameCn;
		})
		
		$(data.airportList).each(function(){
			airportArray[this.code] = this.nameCn;
		})
		
		callback();
	});
}

//初始化
function init(pageIndex){
	loadData(function(){
		reloadRight(pageIndex);
	})
}

//分页调用的方法
function reloadTable(pageIndex){
	init(pageIndex);
}

//加载列表
function reloadRight(pageIndex){
	var cityCode = $("#cityCode").val();
	var airportCode = $("#airportCode").val();
	$.post('flight/cityAirport/list',{
		cityCode:cityCode,
		airportCode:airportCode,
		pageIndex:pageIndex,
		},function(data){
		$("#cityAirport_Table").html(null);
		$("#cityAirport_Table").html(data);
		
		$.each($('.city'),function(i,dom){
			var txt = $(dom).text();
			if(txt){
				$(dom).text(cityArray[txt]);
			}
		});
		
		$.each($('.airport'),function(i,dom){
			var txt = $(dom).text();
			if(txt){
				$(dom).text(airportArray[txt]);
			}
		});
	});
}


//查找用户
function search_cityAirport(){
	init();
}

//重置
function reset_cityAirport(){
	$("#cityCode").val("");
	$("#airportCode").val("");
	init();
}

//转到添加或修改页面
function getCityAirport(id){
	$.post("flight/cityAirport/getCityAirport",{'id':id},function(data){
		$("#cityAirportDetail").html(data);
		$("#cityAirportMain").hide();
		$("#cityAirportDetail").show();
	});
}


//批量删除
function batchDelete(){
  //判断至少选了一项
  var checkedNum = $("#cityAirportTable input[name='ids']:checked").length;
  if(checkedNum==0){
      $.alert("请选择你要删除的记录!","提示");
      return false;
  }
  
  var ids = new Array();
  $("#cityAirportTable input[name='ids']:checked").each(function(){
  	ids.push($(this).val());
  });
  
	  deleteCityAirport(ids.toString());
}

//单个删除
function deleted(id) {
	deleteCityAirport(id);
};

//确认删除
function deleteCityAirport(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除所选记录?",
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "flight/cityAirport/delete",
				data : {
					id : id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						init();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

//checkbox全选
function selectCityAirport(){
	if ($("#selectCityAirport").is(":checked")) {
        $(":checkbox").prop("checked", true);//所有选择框都选中
    } else {
        $(":checkbox").prop("checked", false);
    }
}
//返回
function backMain(){
	$("#cityAirportMain").show();
	$("#cityAirportDetail").hide().html("");
}


