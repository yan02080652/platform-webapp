<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>城市-机场信息<c:choose><c:when test="${cityAirportDto.id != '' && cityAirportDto.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
             	 &nbsp;&nbsp;<a data-pjax onclick="backMain()">返回</a>
             </div>
             <div class="panel-body">
                 <form id="cityAirportForm" class="form-horizontal bv-form" >
					<input type="hidden" name="id"  value="${cityAirportDto.id }"/>
					<input type="hidden" name="cityId"  value="${cityAirportDto.cityId }"/>

					<div class="form-group">
					    <label class="col-sm-2 control-label">机场</label>
					    <div class="col-sm-6">
					    	<select class="form-control" name="airportCode" id="airportCode_model">
								<option value="-1">请选择</opton>
									<c:if test="${not empty dataMap.airportList }">
										<c:forEach items="${dataMap.airportList }" var="airport">
											<option value="${airport.code }">${airport.nameCn }</option>
										</c:forEach>
									</c:if>
							</select>
					    </div>
					  </div>

					<div class="form-group">
						<label class="col-sm-2 control-label">城市</label>
						<div class="col-sm-6">
							<select class="form-control" name="cityCode" id="cityCode_model">
								<option value="-1">请选择</option>
									<c:if test="${not empty dataMap.cityList }">
										<c:forEach items="${dataMap.cityList }" var="city">
											<option value="${city.code }">${city.nameCn }</option>
										</c:forEach>
									</c:if>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">是否在该城市</label>
						<div class="col-sm-6">
							<label class="radio-inline"><input type="radio" name="isWithin" value="1" />在</label> 
							<label class="radio-inline"><input type="radio" name="isWithin" value="0" checked="checked"/>不在</label>
						</div>
					</div>
				</form>
				<div class="form-group">
					<div class="col-sm-offset-7" >
						<a class="btn btn-default"  onclick="submitCityAirport()">提交</a>
					</div>
				</div>
			</div>
         </div> 
	</div>
</div>
<script type="text/javascript">

//提交表单
function submitCityAirport(){
	$('#cityAirportForm').submit(); 
}

$(document).ready(function() {
	$("#cityAirportForm").validate({
		rules : {
			airportCode:{
            	type:true
            },
            cityCode:{
            	type:true
            }
        },
        submitHandler : function(form) {
        	$.ajax({
				cache : true,
				type : "POST",
				url : "flight/cityAirport/save",
				data : $('#cityAirportForm').serialize(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						$("#cityAirportMain").show();
						$("#cityAirportDetail").hide();
						init();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			}); 
   
        },
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight
    });
	
	var cityCode = "${cityAirportDto.cityCode}";
	var airportCode = "${cityAirportDto.airportCode}";
	if(cityCode){
		$("#cityCode_model").val(cityCode);
	}
	if(airportCode){
		$("#airportCode_model").val(airportCode);
	}
	
	if('${cityAirportDto.isWithin}' == 'true'){
		$("input[name='isWithin'][value='1']").attr("checked",true);
	}else{
		$("input[name='isWithin'][value='0']").attr("checked",true);
	}
	
});
</script>