<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<table class="table table-striped table-bordered table-hover" id="cityAirportTable">
	<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectCityAirport()" id="selectCityAirport"/></th>
	      <th><label>机场</label></th>
          <th><label>城市</label></th>
          <th><label>是否在该城市</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="cityAirport">
         <tr class="odd gradeX">
        	 <td><input type="checkbox" name="ids" value="${cityAirport.id }"/></td>
             <td class="airport">${cityAirport.airportCode }</td>
             <td class="city">${cityAirport.cityCode }</td>
             <td>
             	<c:choose>
             		<c:when test="${cityAirport.isWithin }">在</c:when>
             		<c:otherwise>不在</c:otherwise>
             	</c:choose>
            </td>
             <td class="text-center">
             	<a onclick="getCityAirport('${cityAirport.id }')" >修改</a>&nbsp;&nbsp;|
             	<a onclick="deleted('${cityAirport.id }')">删除</a> 
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadTable"></jsp:include>
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>