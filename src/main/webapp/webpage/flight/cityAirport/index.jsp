<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="cityAirportMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>城市-机场关系    &nbsp;&nbsp;
			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12"> 
										<div class="pull-left">
											<a class="btn btn-default" onclick="getCityAirport()" role="button">新增记录</a>
											<a class="btn btn-default" onclick="batchDelete()" role="button">批量删除</a>
										</div>
										
											<!-- 查询条件 -->
											<form class="form-inline" method="post" >
												<nav class="text-right">
													<div class=" form-group">
													 <label class="control-label">城市：</label>
														<select class="form-control"  id="cityCode">
															<option value="">全部</opton>
																<c:if test="${not empty cityList }">
																	<c:forEach items="${cityList }" var="city">
																		<option value="${city.code }">${city.nameCn }</option>
																	</c:forEach>
																</c:if>
														</select> 
													</div>

												<div class=" form-group">
													 <label class="control-label">机场编码：</label>
														<input name="airportCode" id="airportCode" type="text"  class="form-control" placeholder="机场编码"/>
													</div>
													<div class="form-group">
														<button type="button" class="btn btn-default" onclick="search_cityAirport()">查询</button>
														<button type="button" class="btn btn-default" onclick="reset_cityAirport()">重置</button>
													</div>
												</nav>
											</form>
										</div>
									</div>
									<div  id="cityAirport_Table" style="margin-top: 15px">
					
					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 详细页面 -->
<div id="cityAirportDetail" style="display:none;"></div>

<script type="text/javascript" src="webpage/flight/cityAirport/cityAirport.js?version=${globalVersion}"></script>