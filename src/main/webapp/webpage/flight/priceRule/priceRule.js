$(function () {
    reloadTable();
})

//加载列表
function reloadTable(pageIndex) {
    var partnerId = $("#partnerId").val();
    $.post('flight/priceRule/list',
        {pageIndex: pageIndex, partnerId: partnerId},
        function (data) {
            $("#priceRule_Table").html(data);
        });
}

//企业选择器
function selectPartner() {
    TR.select('partner', {
        //type:0//固定参数
    }, function (data) {
        $("#partnerId").val(data.id);
        $("#partnerName").val(data.name);
    });
}

//查找
function search_priceRule() {
    reloadTable();
}

//重置
function reset_priceRule() {
    $("#partnerId").val("");
    $("#partnerName").val("");
    reloadTable();
}

//转到添加或修改页面
function getPriceRule(id) {
    $.post("flight/priceRule/getPriceRule", {'id': id}, function (data) {
        $("#priceRuleDetail").html(data);
        $("#priceRuleMain").hide();
        $("#priceRuleDetail").show();
    });
}

//批量删除
function batchDelete() {
    //判断至少选了一项
    var checkedNum = $("#priceRuleTable input[name='ids']:checked").length;
    if (checkedNum == 0) {
        $.alert("请选择你要删除的记录!", "提示");
        return false;
    }

    var ids = new Array();
    $("#priceRuleTable input[name='ids']:checked").each(function () {
        ids.push($(this).val());
    });

    deletePriceRule(ids.toString());
}

//单个删除
function deleted(id) {
    deletePriceRule(id);
};

//确认删除
function deletePriceRule(id) {
    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: "确认删除所选记录?",
        confirm: function () {
            $.ajax({
                cache: true,
                type: "POST",
                url: "flight/priceRule/delete",
                data: {
                    ids: id
                },
                error: function (request) {
                    showErrorMsg("请求失败，请刷新重试");
                },
                success: function (data) {
                    if (data.type == 'success') {
                        showSuccessMsg(data.txt);
                        reloadTable();
                    } else {
                        $.alert(data.txt, "提示");
                    }
                }
            });
        },
        cancel: function () {
        }
    });
}

//checkbox全选
function selectAll() {
    if ($("#selectAll").is(":checked")) {
        $(":checkbox").prop("checked", true);//所有选择框都选中
    } else {
        $(":checkbox").prop("checked", false);
    }
}

//提交
function submitPriceRule() {
    var grade = $("input[name='grade']:checked").val();
    if (grade == 2) {//企业级需要判断是否选择具体企业
        var pid = $("#pid").val();
        if (!pid) {//提示
            showTip('请选择企业', "#chooseDepDiv .showTips");
            return;
        }
    }
    //自定义规则判断是否输入了x,y
    var percentage = $("#percentage").val();
    if (isNaN(parseInt(percentage))) {
        showTip('请输入正确的数字', "#percentageDiv .showTips");
        return;
    }

    var fixedAmount = $("#fixedAmount").val();
    if (isNaN(parseInt(fixedAmount))) {
        showTip('请输入正确的数字', "#fixedAmountDiv .showTips");
        return;
    }
    $.ajax({
        url: "flight/priceRule/save",
        type: "post",
        data: $('#priceRuleForm').serialize(),
        error: function (request) {
            showErrorMsg("请求失败，请刷新重试");
        },
        success: function (data) {
            if (data.type == 'success') {
                showSuccessMsg(data.txt);
                backRuleTable();
            } else {
                $.alert(data.txt, "提示");
            }
        }
    })


}

//返回
function backRuleTable() {
    $("#priceRuleMain").show();
    $("#priceRuleDetail").hide().html("");
    reloadTable();
}


function showTip(content, selector) {
    layer.tips(content, $(selector), {
        tips: [1, '#3595CC'],
        time: 2000
    });
}

