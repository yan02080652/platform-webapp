<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="glyphicon glyphicon-home" aria-hidden="true"></span>加价规则
				<c:choose>
					<c:when test="${priceRuleDto.id != null}">修改</c:when>
					<c:otherwise>添加</c:otherwise>
				</c:choose>
				&nbsp;&nbsp;<a onclick="backRuleTable()">返回</a>
			</div>
			<div class="panel-body">
				<form id="priceRuleForm" class="form-horizontal bv-form">
					<input type="hidden" name="id" id="id" value="${priceRuleDto.id}" />
					<div class="form-group">
						<label class="col-sm-2">规则级别</label>
						<div class="col-sm-3">
							<c:if test="${not empty gradeEnum }">
								<c:forEach items="${gradeEnum }" var="grade" varStatus="vs">
									<c:if test="${vs.index eq 0 }">
										<input type="radio" class="gradeClass" name="grade"
											checked="checked" value="${grade.value }" />${grade.message }
											</c:if>
									<c:if test="${vs.index gt 0 }">
										<input type="radio" class="gradeClass" name="grade"
											value="${grade.value }" />${grade.message }
											</c:if>
								</c:forEach>
							</c:if>
						</div>
						<!-- 当选择企业出现选择器 -->
						<div class="col-sm-4" hidden="hidden" id="chooseDepDiv">
							<input type="hidden" name="partnerId" id="pid" value="${priceRuleDto.partnerId }"/> 
							适用范围： <span id="pname">${priceRuleDto.partnerName }</span>
							<button type="button" class="btn btn-default" onclick="chooseDep()">请选择...	<span class="showTips"></span></button>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 ">规则类型</label>
						<div class="col-sm-3">
							<c:if test="${not empty ruleTypeEnum }">
								<c:forEach items="${ruleTypeEnum }" var="type" varStatus="vs">
									<c:if test="${vs.index eq 0 }">
										<input type="radio" class="ruleTypeClass" name="ruleType" checked="checked"
											   value="${type.value }" />按${type.message }计算
									</c:if>
									<c:if test="${vs.index gt 0 }">
										<input type="radio" class="ruleTypeClass" name="ruleType" value="${type.value }" />按${type.message }计算
									</c:if>
								</c:forEach>
							</c:if>
						</div>
					</div>
					
					<div id="formulaDiv" class="form-group"  >
						<label class="col-sm-2 ">自定义价格规则<br/>(<span id="showType"></span>*(1+X%) + Y)</label>
						<div class="col-sm-2" id="percentageDiv">
							<span class="showTips"></span>
							<input type="number" name="percentage" id="percentage" class="form-control" value="${priceRuleDto.percentage }" placeholder="请输入浮动百分比 X" >
						</div>
						<div class="col-sm-2" id="fixedAmountDiv">
							<span class="showTips"></span>
							<input type="number" name="fixedAmount" id="fixedAmount" class="form-control" value="${priceRuleDto.fixedAmount }" placeholder="请输入固定金额Y" >
						</div>
						<div class="col-sm-2">
							<input type="checkbox"  name="exceedStandard" <c:if test="${priceRuleDto.exceedStandard}">checked</c:if>>销售价是否允许超票面</input>
						</div>
					</div>

					<c:if test="${ not empty priceRuleDto}">
						<div class="form-group">
							<label class="col-sm-2 ">创建人</label>
							<div class="col-sm-2">
								<label>${createor.fullname }</label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 ">创建时间</label>
							<div class="col-sm-2">
								<label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
										value="${priceRuleDto.createDate }" /></label>
							</div>
						</div>
					</c:if>
				</form>
				<div class="form-group">
					<div class="col-sm-offset-7">
						<a class="btn btn-default" onclick="submitPriceRule()">保存</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		if('${priceRuleDto.grade}'){
			$("input[name='grade'][value='${priceRuleDto.grade.value}']").attr("checked",true);
		}
		if('${priceRuleDto.ruleType}'){
			$("input[name='ruleType'][value='${priceRuleDto.ruleType.value}']").attr("checked",true);
		}
		
		var val = $('input:radio[class="gradeClass"]:checked').val();
		if (val != 1) {
			//显示后面的div
			$("#chooseDepDiv").attr("hidden", false);
		} else {
			//隐藏后面的div
			$("#chooseDepDiv").attr("hidden", true);
		}
		
		var val = $('input:radio[class="ruleTypeClass"]:checked').val();
		if (val == 1) {
			$("#showType").text("票面价");
		} else {
            $("#showType").text("结算价");
    }
	})
	
	$(".ruleTypeClass").change(function(){
		var value = $(this).val();
		if(value == 1){
            $("#showType").text("票面价");
		}else{
            $("#showType").text("结算价");
		}
	})

	$(".gradeClass").change(function() {
		//清空内容
		$("#pid").val("");
		$("#pname").text("");
		var value = $(this).val();
		if (value != 1) {
			//显示后面的div
			$("#chooseDepDiv").attr("hidden", false);
		} else {
			//隐藏后面的div
			$("#chooseDepDiv").attr("hidden", true);
		}
	})
	//选择合作伙伴
	function chooseDep() {
		TR.select('partner', {
		}, function(data) {
			$("#pid").val(data.id);
			$("#pname").text(data.name);
		});
	}
</script>
