<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="priceRuleMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>机票加价规则配置  &nbsp;&nbsp;
			</div>
				<div id="priceDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12"> 
											<div class="pull-left">
												<a class="btn btn-default" onclick="getPriceRule()" role="button">新增规则</a>
												<a class="btn btn-default" onclick="batchDelete()" role="button">批量删除</a>
											</div>
											<!-- 查询条件 -->
											<form class="form-inline" method="post" >
												<nav class="text-right">
													<div class=" form-group">
													 <label class="control-label">企业名称:</label>
													 <input type="hidden" id="partnerId"/>
													 <input id="partnerName" type="text" style="width: 250px"  onclick="selectPartner()" class="form-control" placeholder="企业名称" />
													</div>
													<div class="form-group" style="margin-left: 20px">
														<button type="button" class="btn btn-default" onclick="search_priceRule()">查询</button>
														<button type="button" class="btn btn-default" onclick="reset_priceRule()">重置</button>
													</div>
												</nav>
											</form>
										</div>
									</div>
									<div  id="priceRule_Table" style="margin-top: 15px">
					
					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 详细页面 -->
<div id="priceRuleDetail" style="display:none;"></div>
<script type="text/javascript" src="webpage/flight/priceRule/priceRule.js?version=${globalVersion}"></script>