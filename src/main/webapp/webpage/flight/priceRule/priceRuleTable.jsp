<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<table class="table table-striped table-bordered table-hover" id="priceRuleTable">
	<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectAll()" id="selectAll"/></th>
          <th><label>规则级别</label></th>
	      <th><label>适用企业</label></th>
          <th><label>规则类型</label></th>
          <th><label>涨幅百分比(%)</label></th>
          <th><label>固定加价价格(单位:元)</label></th>
          <th><label>创建时间</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="priceRuleDto">
         <tr class="odd gradeX">
        	 <td><input type="checkbox" name="ids" value="${priceRuleDto.id }"/></td>
             <td class="grade">${priceRuleDto.grade.message }</td>
             <td>${priceRuleDto.partnerName }</td>
             <td>${priceRuleDto.ruleType.message }</td>
             <td>${priceRuleDto.percentage }</td>
             <td>${priceRuleDto.fixedAmount }</td>
             <td><fmt:formatDate value="${priceRuleDto.createDate }" pattern="yyy-MM-dd HH:mm:ss"/></td>
             
             <td class="text-center">
             	<a onclick="getPriceRule('${priceRuleDto.id }')" >修改</a>&nbsp;&nbsp;
             	<a onclick="deleted('${priceRuleDto.id }');">删除</a>
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadTable"></jsp:include>
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>