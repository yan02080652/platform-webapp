var curHubsCode = null;//当前选中的hubs

function selectHubs(code) {
    $("#hubsListTable tbody tr").removeClass("active");
    $("#hubsListTr" + code).addClass("active");
    curHubsCode = code;

    reloadRight(code);
    //加载右侧数据
}

//加载右侧面板
function reloadRight(code) {
    var path = 'flight/hubs/loadRight?code=' + code;
    $("#rightDetail").load(path);
}

//添加
function addHubs() {
    showHubsModal("flight/hubs/hubsDetail", function () {
        $('#mode_hubs').modal();
        initForm();
    });
};

//修改
function editHubs() {
    if (curHubsCode == null) {
        $.alert("请选择一行数据!", "提示");
        return;
    }
    showHubsModal("flight/hubs/hubsDetail?code=" + curHubsCode, function () {
        $('#mode_hubs').modal();
        initForm();
    });
};

//删除
function deleteHubs() {
    if (curHubsCode == null) {
        $.alert("请选择一行数据!", "提示");
        return;
    }

    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: '确认删除当前连接器?',
        confirm: function () {
            $.ajax({
                cache: true,
                type: "POST",
                url: "flight/hubs/deleteHubs",
                data: {
                    code: curHubsCode
                },
                async: false,
                error: function (request) {
                    showErrorMsg("请求失败，请刷新重试");
                },
                success: function (data) {
                    if (data.type == 'success') {
                        window.location.reload();
                    } else {
                        $.alert(data.txt, "提示");
                    }
                }
            });
        },
        cancel: function () {

        }
    });
};

//显示Model
function showHubsModal(url, data, callback) {
    $('#modelHubsDiv').load(url, data, function (context, state) {
        if ('success' == state && callback) {
            callback();
        }
    });
}

function initForm() {
    $("#detailForm").validate(
        {
            rules: {
                type: {
                    type: true,
                },
                name: {
                    required: true,
                    minlength: 1,
                    maxlength: 20,
                },
                code: {
                    required: true,
                    minlength: 1,
                    maxlength: 20,
                    remote: {
                        url: "flight/hubs/checkHubsCode",
                        type: "post",
                        data: {
                            editMode: function () {
                                return $("#editMode").val();
                            }
                        },
                        dataFilter: function (data) {
                            var resultMes = eval("(" + data + ")");
                            if (resultMes.type == 'success') {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                },
            },
            messages: {
                code: {
                    remote: "编码已存在"
                }
            },
            submitHandler: function (form) {
                console.log($('#detailForm').serialize());
                $.ajax({
                    cache: true,
                    type: "POST",
                    url: "flight/hubs/saveOrupdateHubs",
                    data: $('#detailForm').serialize(),
                    async: false,
                    error: function (request) {
                        showErrorMsg("请求失败，请刷新重试");
                        $('#mode_hubs').modal('hide');
                    },
                    success: function (data) {
                        $('#mode_hubs').modal('hide');
                        if (data.type == 'success') {
                            showSuccessMsg("操作成功");
                            /*if($("#editMode").val()=="update"){
                             reloadRight(curHubsCode);
                             }else{
                             window.location.reload();
                             }*/
                            window.location.reload();
                        } else {
                            showErrorMsg(data.txt);
                        }
                    }
                });
            },
            errorPlacement: setErrorPlacement,
            success: validateSuccess,
            highlight: setHighlight
        });
}

function submitHubs() {
    $("#detailForm").submit();
}


//*****************************连接器 连接*************************************
function addConnect(hubsCode,regionType) {
    showHubsConnectModal("flight/hubs/hubsConnectDetail?hubsCode=" + hubsCode+"&regionType="+regionType, function () {
        $('#mode_hubsConnect').modal();
        initFormConnect();
    });
}

//显示Model
function showHubsConnectModal(url, data, callback) {
    $('#modelHubsConnectDiv').load(url, data, function (context, state) {
        if ('success' == state && callback) {
            callback();
        }
    });
}

function editConnect(id) {
    showHubsConnectModal("flight/hubs/hubsConnectDetail?id=" + id, function () {
        $('#mode_hubsConnect').modal();
        initFormConnect();
    });
}

function deleteConnect(id) {
    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: '确认删除当前连接?',
        confirm: function () {
            $.ajax({
                cache: true,
                type: "POST",
                url: "flight/hubs/deleteHubsConnect",
                data: {
                    id: id
                },
                async: false,
                error: function (request) {
                    showErrorMsg("请求失败，请刷新重试");
                },
                success: function (data) {
                    if (data.type == 'success') {
                        showSuccessMsg(data.txt);
                        reloadRight(curHubsCode);
                    } else {
                        $.alert(data.txt, "提示");
                    }
                }
            });
        },
        cancel: function () {

        }
    });
}

function initFormConnect() {
    $("#detailFormConnect").validate(
        {
            rules: {
                name: {
                    required: true,
                    minlength: 1,
                    maxlength: 20,
                },
                accountName: {
                    required: true,
                    minlength: 1,
                    maxlength: 30,
                },
                issueChannelCode: {
                    type: true,
                },
                // accountPwd: {
                //     required: true,
                //     minlength: 1,
                //     maxlength: 30,
                // },
                printId: {
                    maxlength: 1,
                },
                productType: {
                    required: true,
                },
                tmcName: {
                    required: true,
                },
                businessPhone: {
                    required: true,
                    mobile: true,
                },
            },
            submitHandler: function (form) {
                $('#mode_hubsConnect').modal('hide');
                setTimeout(function () {
                    $.ajax({
                        cache: true,
                        type: "POST",
                        url: "flight/hubs/saveOrupdateHubsConnect",
                        data: $('#detailFormConnect').serialize(),
                        async: false,
                        error: function (request) {
                            showErrorMsg("请求失败，请刷新重试");
                        },
                        success: function (data) {
                            if (data.type == 'success') {
                                showSuccessMsg("操作成功");
                                reloadRight(curHubsCode);
                            } else {
                                showErrorMsg(data.txt);
                            }
                        }
                    });
                }, 1000);
            },
            errorPlacement: setErrorPlacement,
            success: validateSuccess,
            highlight: setHighlight
        });
}

function submitHubsConnect() {
    $("#detailFormConnect").submit();
}


function backRight() {
    reloadRight(curHubsCode);
}


