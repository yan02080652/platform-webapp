<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="mode_hubs" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <c:choose>
            	<c:when test="${ empty hubsDto.code}">
            		<h4 class="modal-title">新增连接器</h4>
            	</c:when>
            	<c:otherwise>
            		<h4 class="modal-title">修改连接器</h4>
            	</c:otherwise>
            </c:choose>
         </div>
         <div class="modal-body">
         <form modelAttribute="hubsDto" class="form-horizontal" id="detailForm" role="form">
               <input type="hidden" name="editMode" id="editMode" value="${editMode}"/>
			   <div class="form-group">
		     	  <label  class="col-sm-3 control-label">连接器编码</label>
		     	  <div class="col-sm-9">
		         	 <input type="text" class="form-control" id="hubsCode" name="code" value="${hubsDto.code}" placeholder="编码"/>
		     	  </div>
		  	   </div>
			   <div class="form-group">
			      <label  class="col-sm-3 control-label">连接器名称</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" name="name" value="${hubsDto.name}" placeholder="名称"/>
			      </div>
			   </div>
			   <div class="form-group">
		     	  <label  class="col-sm-3 control-label">类型</label>
		     	  <div class="col-sm-9">
		         	<select class="form-control" id="hubsType" name="type">
				          <option value="-1">请选择类型</option>
				          <option value="1">机票代理</option>
				          <option value="2">机票代理交易平台</option>
				          <option value="3">航信IBE</option>
				          <option value="4">航信黑转白</option>
				          <option value="5">航司直连</option>
				    </select>
		     	  </div>
		  	 	</div>
		  	 	<div class="form-group">
			  	 	<div class="pull-left col-sm-6">
				  	 	<div class="form-group">
				      		<label  class="col-sm-6 control-label">查询接口</label>
						  	<div>
						     	<label class="radio-inline"><input  type="radio" value="1"  name="isCanQuery" />支持</label>
						     	<lable class="radio-inline"><input  type="radio" value="0" name="isCanQuery" checked="checked"/>不支持</lable>
						  	</div>
					
				   		</div>
				   
					   <div class="form-group">
					      <label  class="col-sm-6 control-label">验价接口</label>
						  <div>
						     <label class="radio-inline"><input  type="radio" value="1"  name="isCanCheckprice" />支持</label>
						     <lable class="radio-inline"><input  type="radio" value="0" name="isCanCheckprice" checked="checked"/>不支持</lable>
						  </div>
					   </div>
					   <div class="form-group">
					      <label  class="col-sm-6 control-label">占座接口</label>
						  <div>
						     <label class="radio-inline"><input  type="radio" value="1"  name="isCanSeat" />支持</label>
						     <lable class="radio-inline"><input  type="radio" value="0" name="isCanSeat" checked="checked"/>不支持</lable>
						  </div>
					   </div>
					   <div class="form-group">
					      <label  class="col-sm-6 control-label">出票接口</label>
						  <div>
						     <label class="radio-inline"><input  type="radio" value="1"  name="isCanIssue" />支持</label>
						     <lable class="radio-inline"><input  type="radio" value="0" name="isCanIssue" checked="checked"/>不支持</lable>
						  </div>
					   </div>
					   <div class="form-group">
					      <label  class="col-sm-6 control-label">退改签接口</label>
						  <div>
						     <label class="radio-inline"><input  type="radio" value="1"  name="isCanChange" />支持</label>
						     <lable class="radio-inline"><input  type="radio" value="0" name="isCanChange" checked="checked"/>不支持</lable>
						  </div>
					   </div>
					   <div class="form-group">
					      <label  class="col-sm-6 control-label">支付前验单接口</label>
						  <div>
						     <label class="radio-inline"><input  type="radio" value="1"  name="isCanPaybeforecheck" />支持</label>
						     <lable class="radio-inline"><input  type="radio" value="0" name="isCanPaybeforecheck" checked="checked"/>不支持</lable>
						  </div>
					   </div>
					   <div class="form-group">
					      <label  class="col-sm-6 control-label">是否禁用</label>
						  <div>
						     <label class="radio-inline"><input  type="radio" value="1" name="isDisable" />是</label>
						     <lable class="radio-inline"><input  type="radio" value="0" checked="checked" name="isDisable"/>否</lable>
						  </div>
					   </div>

						<div class="form-group">
							<label  class="col-sm-6 control-label">区域类型</label>
							<div>
								<label class="radio-inline"><input  type="radio" value="0" name="regionType" checked="checked" />国内</label>
								<lable class="radio-inline"><input  type="radio" value="1"  name="regionType"/>国际</lable>
							</div>
						</div>
			  	 	</div>
			  	 	<%--<div class="col-sm-6">
			  	 		<div style="border:1px solid #EEEEEE" id="showMethod" hidden>
				  			<input  type="radio" value="1"  name="showMethod" />只展示最低价<br/>
				  			<input  type="radio" value="2"  name="showMethod" checked="checked"/>展示所有舱位价格<br/>
				  			<input  type="radio" value="3"  name="showMethod" />展示最低价,全价经济舱,公务舱和头等舱
			  			</div>
			  	 	</div>--%>
			  	</div>
			   
				<div class="form-group">
			      <label  class="col-sm-3 control-label">描述</label>
			      <div class="col-sm-9">
			         <textarea class="form-control" name="description" rows="3" placeholder="定义">${hubsDto.description} </textarea>
			      </div>
			   </div>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            <button type="button" class="btn btn-primary" onclick="submitHubs()">保存</button>
         </div>
      </div>
</div>

<script type="text/javascript">
	
	$("input[name='isCanQuery']").change(function(){
	/*	if($(this).val() == '1') {
			$("#showMethod").show();
		}else{
			$("#showMethod").hide();
		}*/
	});

	$(document).ready(function(){
		var hubsCode = "${hubsDto.code}";
		if(hubsCode!=null && hubsCode!=''){//修改
			$("#hubsType").val("${hubsDto.type}");
			
			$('#hubsCode').attr('readonly',true);
			
			if('${hubsDto.isCanQuery}'=='false'){
				$("input:radio[value=0][name=isCanQuery]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isCanQuery]").attr('checked','checked');
				<%--$("input:radio[value=${hubsDto.showMethod}][name=showMethod]").attr('checked','checked');--%>
			}
			//支持查询接口的时候选择展示方式  
		/*	if($("input[name='isCanQuery']:checked").val() == '1') {
				$("#showMethod").show();
			}else{
				$("#showMethod").hide();
			}*/
			
			if('${hubsDto.isCanCheckprice}'=='false'){
				$("input:radio[value=0][name=isCanCheckprice]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isCanCheckprice]").attr('checked','checked');
			}
			
			if('${hubsDto.isCanSeat}'=='false'){
				$("input:radio[value=0][name=isCanSeat]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isCanSeat]").attr('checked','checked');
			}
			
			if('${hubsDto.isCanIssue}'=='false'){
				$("input:radio[value=0][name=isCanIssue]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isCanIssue]").attr('checked','checked');
			}
			
			if('${hubsDto.isCanChange}'=='false'){
				$("input:radio[value=0][name=isCanChange]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isCanChange]").attr('checked','checked');
			}
			
			if('${hubsDto.isDisable}'=='false'){
				$("input:radio[value=0][name=isDisable]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isDisable]").attr('checked','checked');
			}
			
			if('${hubsDto.isCanPaybeforecheck}'=='false'){
				$("input:radio[value=0][name=isCanPaybeforecheck]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isCanPaybeforecheck]").attr('checked','checked');
			}

            if('${hubsDto.regionType}'=='0'){
                $("input:radio[value=0][name=regionType]").attr('checked','checked');
            }else{
                $("input:radio[value=1][name=regionType]").attr('checked','checked');
            }
		}

	});
</script>