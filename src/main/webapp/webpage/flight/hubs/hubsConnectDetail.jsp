<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .reqMark {
        color: red;
        font-weight: bold;
        font-size: 10px;
    }
</style>
<div class="modal fade" id="mode_hubsConnect" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="overflow-y:auto">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <c:choose>
                    <c:when test="${ empty hubsConnectDto.id}">
                        <h4 class="modal-title">新增连接器连接</h4>
                    </c:when>
                    <c:otherwise>
                        <h4 class="modal-title">修改连接器连接</h4>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="modal-body">
                <form modelAttribute="hubsConnectDto" class="form-horizontal" id="detailFormConnect" role="form">
                    <input type="hidden" name="id" id="hubsConnectId" value="${hubsConnectDto.id}"/>
                    <input type="hidden" name="hubsCode" id="hubsCode" value="${hubsConnectDto.hubsCode}"/>
                    <input type="hidden" name="regionType" id="regionType" value="${hubsConnectDto.regionType}"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>连接名称</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="name" value="${hubsConnectDto.name}"
                                   placeholder="名称"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>账号名
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="accountName"
                                   value="${hubsConnectDto.accountName}" placeholder="账号名"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">账号密码</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="accountPwd"
                                   value="${hubsConnectDto.accountPwd}" placeholder="账号密码"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>产品类型
                        </label>
                        <div class="col-sm-9">
                            <select class="form-control" id="productType" name="productType">
                                <option value="">请选择</option>
                                <c:forEach items="${productTypeEnumList}" var="productTypeEnum">
                                    <option value="${productTypeEnum }"
                                            <c:if test="${productTypeEnum eq hubsConnectDto.productType}">selected</c:if>>${productTypeEnum.msg }</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>所属tmc
                        </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control partner" name="tmcName" value="${tmcPartner.name}"
                                   placeholder="选择tmc" readonly/>
                            <input type="hidden" name="tmcId" value="${hubsConnectDto.tmcId}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><span class="reqMark">*</span>机票渠道
                        </label>
                        <div class="col-sm-9">
                            <select class="form-control" id="issueChannelCode" name="issueChannelCode">
                                <option value="-1">请选择</option>
                                <c:forEach items="${issueChannelList}" var="issueChannel">
                                    <option value="${issueChannel.code }">${issueChannel.name }</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">office号</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="officeCode"
                                   value="${hubsConnectDto.officeCode}" placeholder="office号"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">打票机号</label>
                        <div class="col-sm-9">
                            <input type="number" class="form-control" name="printId" value="${hubsConnectDto.printId}"
                                   placeholder="打票机号"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><span class="reqMark">*</span>业务联系人手机</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="businessPhone" value="${hubsConnectDto.businessPhone}" placeholder="业务联系人手机"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label">德付通密码</label>
                        <div class="col-sm-9">
                            <input type="password"  class="form-control" name="dftPayPwd" value="${hubsConnectDto.dftPayPwd}" placeholder="德付通密码"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label">iata码</label>
                        <div class="col-sm-9">
                            <input type="text"  class="form-control" name="iataCode" value="${hubsConnectDto.iataCode}" placeholder="仅供国际机票查询使用"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">是否禁用</label>
                        <div class="col-sm-9">
                            <label class="radio-inline"><input type="radio" value="1" name="isDisable"/>是</label>
                            <lable class="radio-inline"><input type="radio" value="0" checked="checked"
                                                               name="isDisable"/>否
                            </lable>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="submitHubsConnect()">保存</button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var id = "${hubsConnectDto.id}";
            if (id != null && id != '') {//修改

                if ('${hubsConnectDto.isDisable}' == 'false') {
                    $("input:radio[value=0][name=isDisable]").attr('checked', 'checked');
                } else {
                    $("input:radio[value=1][name=isDisable]").attr('checked', 'checked');
                }

                if ('${hubsConnectDto.issueChannelCode}') {
                    $("#issueChannelCode").val('${hubsConnectDto.issueChannelCode}');
                }

            }

            $(".partner").click(function () {
                TR.select('partner', {
                    type: 1
                }, function (result) {
                    $("[name='tmcName']").val(result.name);
                    $("[name='tmcId']").val(result.id);

                    //显示对应tmc 的线上渠道？？？？
                    $.ajax({
                        cache: true,
                        type: "POST",
                        url: "flight/hubs/getOnlineChannel",
                        data: {"tmcId":$("[name='tmcId']").val(),"regionType":$("[name='regionType']").val()},
                        async: false,
                        error: function (request) {
                            showErrorMsg("请求失败，请刷新重试");
                        },
                        success: function (data) {
                            var html = "<option value='-1'>请选择</option>";
                            console.log(data);
                            $.each(data,function(){
                                html += "<option value='"+this.code+"'>"+this.name+"</option>";
                            });
                            console.log(html);
                            $("#issueChannelCode").html(html);

                        }
                    });


                });
            });

        });
    </script>


