<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>航司信息<c:choose><c:when test="${carrierDto.code != '' && carrierDto.code != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
             	 &nbsp;&nbsp;<a data-pjax onclick="backCarrierMain()">返回</a>
             </div>
             <div class="panel-body">
                 <form id="carrierForm" class="form-horizontal bv-form" method="post" action="flight/carrier/save" enctype="multipart/form-data"> 
                 
					 <div class="form-group">
					    <label class="col-sm-2 control-label">航司编码</label>
					    <div class="col-sm-6">
					      <input type="text" name="code" id="code" value="${carrierDto.code }" class="form-control"  placeholder="航司编码">
					    </div>
					  </div>
					  
					   <div class="form-group">
					    <label class="col-sm-2 control-label">中文名称</label>
					    <div class="col-sm-6">
					      <input type="text" name="nameCn" class="form-control" value="${carrierDto.nameCn }" placeholder="中文名称">
					    </div>
					  </div>
					  
					   <div class="form-group">
					    <label class="col-sm-2 control-label">英文名称</label>
					    <div class="col-sm-6">
					      <input type="text" name="nameEn" class="form-control" value="${carrierDto.nameEn }" placeholder="英文名称">
					    </div>
					  </div>
					  
					   <div class="form-group">
					    <label class="col-sm-2 control-label">中文简称</label>
					    <div class="col-sm-6">
					      <input type="text" name="nameShort" class="form-control" value="${carrierDto.nameShort }" placeholder="中文简称">
					    </div>
					  </div>
					  
					   <div class="form-group">
					    <label class="col-sm-2 control-label">航司类型</label>
					    <div class="col-sm-6">
					    	<select class="form-control" name="type" id="type">
								<option value="DOMESTIC">国内</option>
								<option value="INTERNATIONAL">国际</option>
							</select>
					    </div>
					  </div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">集团编码</label>
						 <div class="col-sm-6">
							 <input type="text" name="groupCode" class="form-control" value="${carrierDto.groupCode }" placeholder="集团编码">
						 </div>
					 </div>

					<div class="form-group">
						<label class="col-sm-2 control-label">图标</label>
						<div class="col-sm-6">
							<c:if test="${carrierDto.logo!=null }">
								<img alt="" src="http://${SYS_CONFIG['imgserver'] }/${carrierDto.logo }" width="100px" height="100px">
							</c:if>
							 <input type="file" name="carrier_logo" /></td>
						</div>
					</div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">航司官网产品人数上限</label>
						 <div class="col-sm-6">
							 <select class="form-control" name="numberLimit" id="numberLimit">
								 <option value="">-请选择-</option>
								 <option value="1">1</option>
								 <option value="2">2</option>
								 <option value="3">3</option>
								 <option value="4">4</option>
								 <option value="5">5</option>
								 <option value="6">6</option>
								 <option value="7">7</option>
								 <option value="8">8</option>
								 <option value="9">9</option>
							 </select>
						 </div>
					 </div>

					<div class="form-group">
					    <label class="col-sm-2 control-label">序号</label>
					    <div class="col-sm-6">
					      <input type="number" name="sort" class="form-control" value="${carrierDto.sort }" placeholder="序号">
					    </div>
					  </div>
					  
					<div class="form-group">
						<label class="col-sm-2 control-label">状态</label>
						<div class="col-sm-6">
							<label class="radio-inline"><input type="radio" name="isDisable" value="0" checked="checked"/>启用</label> 
							<label class="radio-inline"><input type="radio" name="isDisable" value="1" />禁用</label>
						</div>
					</div>
			</form>
				<div class="form-group">
					<div class="col-sm-offset-7" >
						<a class="btn btn-default"  onclick="submitCarrier()">提交</a>
					</div>
				</div>
         </div> 
	</div>
</div>
<script type="text/javascript">

//提交表单
function submitCarrier(){
	 $('#carrierForm').submit();
}

$(document).ready(function() {
	$("#carrierForm").validate({
		rules : {
        	code : {
                required : true,
                minlength : 1,
                maxlength:20,
                remote:{
                	type:'post',
                	url:'flight/carrier/checkCode',
                	data:{
                		code:function(){
                			return $('#code').val();
                		},
                	},
					dataFilter : function(data) {
						if (data == 'true') {
							return true;
						} else {
							return false;
						}
					}
                }
            },
            nameCn : {
                required : true,
                minlength : 1,
                maxlength:30
            },
        },
        messages:{
            code:{
               remote:"此编码已经存在"
            }
        },
        submitHandler : function(form) {
        	 $('#carrierForm').ajaxSubmit({success:function(data){
        		 if (data.type == 'success') {
						$("#carrierMain").show();
						$("#carrierDetail").hide();
						reloadRightCarrier();
					} else {
						showErrorMsg(data.txt);
					}
        		 
        	 }});
   
        },
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight
    });
	
	//修改时code字段只读并删除code字段验证规则
	var code = $("#code").val();
	if(code){
		$("#code").attr("readonly",true);
		$("#code").rules("remove","remote");
	}
	
	if("${carrierDto.code}"){
		$("#type").val("${carrierDto.type}");
		$("#numberLimit").val("${carrierDto.numberLimit}");
	}
	
	
	//设置checkbox的值
	if('${carrierDto.isDisable}' == 'true'){
		$("input[name='isDisable'][value='1']").attr("checked",true);
	}else{
		$("input[name='isDisable'][value='0']").attr("checked",true);
	}
	
});
</script>