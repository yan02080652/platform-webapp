
$(function(){
	reloadRightCarrier();
})


//分页调用的方法
function reloadCarrier(pageIndex){
	reloadRightCarrier(pageIndex);
}

//加载列表
function reloadRightCarrier(pageIndex){
	var keyword = $("#keyword").val();
	$.post('flight/carrier/list',{
		keyword:keyword,
		pageIndex:pageIndex
		},function(data){
		$("#carrier_Table").html(null);
		$("#carrier_Table").html(data);
	});
}

function showImport() {
    $("#carrierImportFile").val("");
    layer.open({
        title: '导入国际航司',
        type: 1,
        area: ['500px','200px'],
        skin: 'layui-layer-rim', //加上边框
        content: $('.import_window'),

    });
}

function importCarrier(){
    var fileObj = document.getElementById("carrierImportFile").files[0]; // js 获取文件对象
    if (typeof (fileObj) == "undefined" || fileObj.size <= 0) {
        alert("请选择文件");
        return;
    }
    var formFile = new FormData();
    formFile.append("file", fileObj);

    var data = formFile;

    $.ajax({
        url : 'flight/carrier/importCarrier',
        data: data,
        type: "Post",
        dataType: "json",
        cache: false,//上传文件无需缓存
        processData: false,//用于对data参数进行序列化处理 这里必须false
        contentType: false, //必须
        success : function(data) {
            $(".layui-layer-close").trigger("click");
            if (data.code == '0') {
                layer.alert(data.data);
            } else {
                layer.alert("上传失败，请检查文件是否符合格式要求。");
            }
        },
        error : function(data) {
            $(".layui-layer-close").trigger("click");
            layer.alert("上传失败，请检查文件是否符合格式要求。");
        }
    });
}

//查找用户
function search_carrier(){
	reloadRightCarrier();
}

//重置
function reset_carrier(){
	$("#keyword").val("");
	reloadRightCarrier();
}

//转到添加或修改页面
function getCarrier(code){
	$.post("flight/carrier/getCarrier",{'code':code},function(data){
		$("#carrierDetail").html(data);
		$("#carrierMain").hide();
		$("#carrierDetail").show();
	});
}

//批量删除
function batchDeleteCarrier(){
    //判断至少选了一项
    var checkedNum = $("#carrierTable input[name='codes']:checked").length;
    if(checkedNum==0){
        $.alert("请选择你要删除的记录!","提示");
        return false;
    }
    
    var codes = new Array();
    $("#carrierTable input[name='codes']:checked").each(function(){
    	codes.push($(this).val());
    });
    
    confirmDeleteCarrier(codes.toString());
}

//单个删除
function deleteCarrier(code) {
	confirmDeleteCarrier(code);
};

//确认删除
function confirmDeleteCarrier(code){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除所选记录?",
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "flight/carrier/delete",
				data : {
					codes : code
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadRightCarrier();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

//checkbox全选
function selectAllCarrier(){
	if ($("#selectAllCarrier").is(":checked")) {
		 $("#carrierTable input[type=checkbox]").prop("checked", true);//所有选择框都选中
    } else {
    	 $("#carrierTable input[type=checkbox]").prop("checked", false);
    }
}
//返回
function backCarrierMain(){
	$("#carrierMain").show();
	$("#carrierDetail").hide().html("");
}

//显示已有航线列表
function existingAirlineTable(carrierCode){
	$.post("flight/carrier/existingAirlineTable",{carrierCode:carrierCode,type:1},function(data){
		$("#existingAirline").html(data);
		$("#carrierMain").hide();
		$("#existingAirline").show();
	});
}

//显示舱位列表
function forwordCabin(carrierCode){
	$.post("flight/cabin",{carrierCode:carrierCode},function(data){
		$("#cabinListDiv").html(data);
		$("#carrierMain").hide();
		$("#cabinListDiv").show();
	});
}

