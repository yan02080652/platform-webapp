<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="carrierMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>航司管理   &nbsp;&nbsp;
			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12">
										<div class="pull-left">
											<a class="btn btn-default" onclick="getCarrier('')"role="button">新增航司</a> 
											<a class="btn btn-default" onclick="batchDeleteCarrier()" role="button">批量删除</a>
											<a class="btn btn-default" onclick="showImport()" role="button" style="display: none">批量导入国际航司</a>
										</div>
										<!-- 查询条件 -->
											<form class="form-inline" method="post" >
												<nav class="text-right">
													<div class=" form-group">
													 <label class="control-label">查询条件：</label>
														<input name="keyword" id="keyword" type="text" value="${keyword }" class="form-control" placeholder="编码/中文名称/英文名称/中文简称" style="width: 250px"/>
													</div>
													<div class="form-group">
														<button type="button" class="btn btn-default" onclick="search_carrier()">查询</button>
														<button type="button" class="btn btn-default" onclick="reset_carrier()">重置</button>
													</div>
												</nav>
											</form>
										</div>
									</div>
									<div  id="carrier_Table" style="margin-top: 15px">
					
					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 详细页面 -->
<div id="carrierDetail" style="display:none;"></div>
<!-- 已有航线列表 -->
<div id="existingAirline" style="display:none;"></div>
<!-- 舱位列表 -->
<div id="cabinListDiv" style="display:none;"></div>

<div class="import_window" hidden>
	<div class="form-group">
		<label class="col-sm-3 control-label">excel文件</label>
		<div class="col-sm-4 input-group">
			<input type="file" name="excelFile" id="carrierImportFile" class="file-loading">
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-6" style="text-align: right;padding-right:0px;">
			<button type="submit" class="btn btn-primary" style="width: 100px;" onclick="importCarrier()">导入</button>
		</div>
	</div>
</div>

<script type="text/javascript" src="webpage/flight/carrier/carrier.js?version=${globalVersion}"></script>