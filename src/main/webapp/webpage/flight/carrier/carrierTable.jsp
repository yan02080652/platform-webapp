<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" id="carrierTable">
	<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectAllCarrier()" id="selectAllCarrier"/ ></th>
          <th><label>编码</label></th>
	      <th><label>中文名称 </label></th>
          <th><label>英文名称</label></th>
          <th><label>中文简称</label></th>
          <th><label>Logo</label></th>
          <th><label>状态</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="carrier">
         <tr class="odd gradeX">
        	 <td><input type="checkbox" name="codes" value="${carrier.code }"/></td>
             <td>${carrier.code }</td>
        	 <td>${carrier.nameCn }</td>
             <td>${carrier.nameEn }</td>
             <td>${carrier.nameShort }</td>
             <td>
           		<c:if test="${carrier.logo!=null }">
				 <img alt="" src="http://${SYS_CONFIG['imgserver'] }/${carrier.logo }" width="100px" height="30px">
			   </c:if>
			 </td>
			  <td>
             	<c:choose>
             		<c:when test="${carrier.isDisable }">禁用</c:when>
             		<c:otherwise>启用</c:otherwise>
             	</c:choose>
            </td>
             <td class="text-center" >
             	<a href="flight/segment?carrierCode=${carrier.code}">航线</a> |
             	<a onclick="forwordCabin('${carrier.code}')">舱位</a> |
             	<a onclick="getCarrier('${carrier.code }')" >修改</a> |
             	<a onclick="deleteCarrier('${carrier.code }');">删除</a> 
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadCarrier"></jsp:include> 
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>