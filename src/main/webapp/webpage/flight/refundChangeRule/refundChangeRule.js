var refundChangeRule = {
    onlyAdd: true,
    url: {
        list: function () {
            return 'flight/refundChangeRule/list';
        },
        delete: function (id) {
            return 'flight/refundChangeRule/delete/' + id;
        },
        carrierName: function () {
            return 'flight/change/getName';
        },
        cabinList: function () {
            return 'flight/refundChangeRule/getCabinList';
        },
        save: function () {
            return 'flight/refundChangeRule/save';
        },
        detail: function () {
            return 'flight/refundChangeRule/detail';
        },
        index: function () {
            return 'flight/refundChangeRule';
        }
    },

//=====================================列表页===============================================
    list: {
        init: function () {
            //航司选择器
            $(".carrier").click(function () {
                TR.select('flightCompany', {}, function (result) {
                    $("#carrierCode").val(result ? result.code : null);
                    $("#carrierName").val(result ? result.nameShort : null);
                    refundChangeRule.list.reloadList();
                });
            });
            refundChangeRule.list.reloadList();
        },
        reloadList: function (pageIndex) {
            var cabinCode = $.trim($("#cabinCode").val());
            var carrierCode = $.trim($("#carrierCode").val());
            $.post(refundChangeRule.url.list(), {
                pageIndex: pageIndex,
                cabinCode: cabinCode,
                carrierCode: carrierCode,
            }, function (data) {
                $("#listData").html(data);
            });
        },
        reset: function () {
            $("#cabinCode").val("");
            $("#carrierCode").val("")
            $("#carrierName").val("")
            refundChangeRule.list.reloadList();
        },
        delete: function (id) {
            $.confirm({
                title: '提示',
                confirmButton: '确认',
                cancelButton: '取消',
                content: "确认删除所选记录?",
                confirm: function () {
                    $.ajax({
                        cache: true,
                        type: "POST",
                        url: refundChangeRule.url.delete(id),
                        async: false,
                        error: function (request) {
                            showErrorMsg("请求失败，请刷新重试");
                        },
                        success: function (result) {
                            if (result == 'success') {
                                refundChangeRule.list.reloadList();
                            } else {
                                $.alert("删除失败！", "提示");
                            }
                        }
                    });
                },
                cancel: function () {
                }
            });
        },
    },
    //=======================================详情页=================================================//
    detail: {
        getNowDate: function () {
            var now = new Date();
            var year = now.getFullYear();
            var month = now.getMonth() + 1;
            var day = now.getDate();
            var nowDate = year + (month > 9 ? '-' : '-0') + month + (day > 9 ? '-' : '-0') + day;
            return nowDate;
        },
        getMaxDate: function () {
            var nowDate = refundChangeRule.detail.getNowDate();
            var month_day = nowDate.substring(5, nowDate.length);
            return "2099-" + month_day;
        },
        setDefaultValue: function () {
            $("input[name='endorse'][value='0']").attr("checked", true);
            $("input[name='referenceBasis'][value='1']").attr("checked", true);
            $("input[name='baggage']").val(20);
            $("input[name='fromAirport']").val("全国");
            $("input[name='toAirport']").val("全国");
            $("input[name='ticketValidityStartDate']").val(refundChangeRule.detail.getNowDate());
            $("input[name='ticketValidityEndDate']").val(refundChangeRule.detail.getMaxDate());
            $("input[name='takeOffValidityStartDate']").val(refundChangeRule.detail.getNowDate());
            $("input[name='takeOffValidityEndDate']").val(refundChangeRule.detail.getMaxDate());
            $("[name='remark']").text("如有变动,以航空公司最新退改签规定为准");
        },
        companyCodeChange: function (companyCode) {
            var elCom = $("input[name='carrierName']");
            if (!companyCode || companyCode.length < 2) {
                elCom.val(null);
            } else {
                var com = refundChangeRule.detail.getName('C', companyCode);
                elCom.val(com ? com.nameShort : null);
                $("[name='carrierCode']").val(companyCode.toLocaleUpperCase());
                refundChangeRule.detail.gradeChange($("[name='cabinGrade']").val());
            }
        },
        gradeChange: function (grade) {
            var dom = $("#cabinCodeDiv");
            dom.empty();
            var carrierCode = $("input[name='carrierCode']").val();
            if (carrierCode && carrierCode.length == 2) {
                var cabinList = refundChangeRule.detail.getCabinList(carrierCode, grade);
                var cabinCodes = $("[name='cabinCodes']").val();
                var childDom = '';
                $.each(cabinList, function (i, n) {
                    var isChecked;
                    if (cabinCodes == '' || cabinCodes == undefined) {
                        isChecked = '';
                    } else {
                        // isChecked = cabinCodes.indexOf(n.code) > -1 ? 'checked' : '';
                        //处理子舱位的情况，indexOf处理不了
                        isChecked = '';
                        var cabinCodeArray = cabinCodes.split("/");
                        for(var i=0;i<cabinCodeArray.length;i++){
                            if (cabinCodeArray[i] == n.code){
                                isChecked = 'checked';
                                break;
                            }
                        }
                    }
                    childDom += "<label class='checkbox-inline'><input type=checkbox name='cabinCode' value=" + n.code + " " + isChecked + ">" + n.code + "</label>";
                })
                dom.append(childDom);
                dom.parent().show();
            }
        },
        getCabinList: function (carrierCode, grade) {
            var rs = null;
            $.ajax({
                url: refundChangeRule.url.cabinList(),
                data: {
                    carrierCode: carrierCode,
                    grade: grade
                },
                dataType: 'json',
                async: false,
                success: function (data) {
                    rs = data;
                }
            });
            return rs;
        },
        getName: function (type, code) {
            if (!code) {
                return null;
            }
            var rs = null;
            $.ajax({
                url: refundChangeRule.url.carrierName(),
                data: {
                    type: type,
                    code: code
                },
                dataType: 'json',
                async: false,
                success: function (data) {
                    rs = data;
                }
            });
            return rs;
        },
        init: function () {
            var form = $('#refundChangeRuleForm');
            //时间选择器
            $(".time-picker").jeDate({
                format: "YYYY-MM-DD"
            });
            $('.input-group-addon').click(function () {
                var preDom = $(this).prev();
                if (preDom.hasClass('time-picker')) {
                    preDom.trigger('click');
                }
            });

            $(".carrier").click(function () {
                TR.select('flightCompany', {}, function (result) {
                    $("#carrierName").val(result ? result.nameShort : null);
                    $("#carrierCode").val(result ? result.code : null);
                    refundChangeRule.detail.gradeChange($("[name='cabinGrade']").val());
                });
            })

            refundChangeRule.detail.gradeChange($("[name='cabinGrade']").val());

            form.find('[name=carrierCode]').change(function () {
                refundChangeRule.detail.companyCodeChange(this.value);
            });

            form.find('[name=cabinGrade]').change(function () {
                refundChangeRule.detail.gradeChange(this.value);
                if (this.value == 'Y') {
                    $("[name='baggage']").val(20);
                } else {
                    $("[name='baggage']").val(40);
                }

            });

            refundChangeRule.detail.initForm();

            $(document).on('mouseover','.fa-question-circle' ,function () {
                var that = this;
                var index = layer.tips($(".help").html(), that, {
                    tips: [2, '#1AA094'],
                    time: 0,
                    area: '500px'
                });
                $(that).on('mouseleave', function () {
                    layer.close(index);
                })
            });

        },
        initForm: function () {
            var rules = {
                refundRule: {
                    required: true,
                    numberAndUnderline: true
                },
                changeRule: {
                    required: true,
                    numberAndUnderline: true
                },
                endorse: {
                    required: true
                },
                carrierName: {
                    required: true
                },
                referenceBasis: {
                    required: true
                },
                cabinGrade: {
                    required: true
                },
            };

            //添加必填标记
            $.each(rules, function (code, rule) {
                if (rule.required || rule.type) {
                    refundChangeRule.detail.addReqMark(code);
                }
            });

            $("#refundChangeRuleForm").validate({
                rules: rules,
                errorPlacement: setErrorPlacement,
                success: validateSuccess,
                highlight: setHighlight,
                submitHandler: function (form) {
                    var size = $("input[type='checkbox']:checked").length;
                    if (size < 1) {
                        layer.tips("请选择舱位", $("[name='cabinCode']").first(), {
                            tips: [1, '#CF521C'],
                            tipsMore: true,
                        });
                        return false;
                    }
                    var refundRule = $("[name='refundRule']").val().split("-");
                    var changeRule = $("[name='changeRule']").val().split("-");

                    if(refundRule.length != changeRule.length){
                        $.alert("退票规则格式和改期格式必须一致！");
                        return false;
                    }

                    $.ajax({
                        type: "POST",
                        url: refundChangeRule.url.save(),
                        data: $(form).serialize(),
                        success: function (result) {
                            if (result == 'success') {
                                if (refundChangeRule.onlyAdd) {
                                    location.href = refundChangeRule.url.index();
                                } else {
                                    location.href = refundChangeRule.url.detail();
                                }
                            } else if (result == 'exist') {
                                $.alert("该舱位已存在规则，请核实！");
                            } else {
                                $.alert("请求失败，请刷新重试！");
                            }

                        },
                        error: function (request) {
                            $.alert("请求失败，请刷新重试！");
                        },
                    });
                }
            });
        },
        addReqMark: function (code, addRule) {
            var requiredMark = $("<span class='reqMark'>*</span>");
            var element = $("#refundChangeRuleForm").find("[name=" + code + "]");
            var label = element.parentsUntil('.form-group').parent().children('.control-label');
            if ($(".reqMark", label).length == 0) {
                label.append(requiredMark);
            }

            if (addRule) {
                element.rules('add', addRule);
            }
        },
        saveForm: function () {
            refundChangeRule.onlyAdd = true;
            $("#refundChangeRuleForm").submit();
        },
        saveAndAdd: function () {
            refundChangeRule.onlyAdd = false;
            $("#refundChangeRuleForm").submit();
        }
    }
};

