<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
    .popover.right>.arrow{
        top:50% !important;
    }
    .control-label .reqMark{
        color: red;
        font-weight: bold;
        font-size: 15px;
    }
    #refundChangeRuleForm .input-group{
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }
    #refundChangeRuleForm .input-group-addon {
        border-left: none;
        border-bottom-right-radius:4px;
        border-top-right-radius:4px;
        cursor: pointer;
    }
    #refundChangeRuleForm .btn-success{
        background-color: #46A2BB;
        border-color: #46A2BB;
    }
</style>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        <c:choose>
            <c:when test="${not empty refundChangeRuleDto.id}">
                修改
            </c:when>
            <c:otherwise>
                新增
            </c:otherwise>
        </c:choose>
        退改签维护&nbsp;<a href="flight/refundChangeRule">返回</a>
    </div>
    <div class="panel-body">
        <form id="refundChangeRuleForm" class="form-horizontal">
            <input type="hidden" name="id" class="form-control" value="${refundChangeRuleDto.id}">
            <input type="hidden" name="cabinCodes" class="form-control" value="${refundChangeRuleDto.cabinCodes}">
            <div class="form-group">
                <label class="col-sm-2 control-label">退票规定</label>
                <div class="col-sm-2">
                    <input type="text" name="refundRule" class="form-control" value="${refundChangeRuleDto.refundRule}" placeholder="示例:10-0-20">
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <i class="fa fa-question-circle"></i>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">同舱改期规定</label>
                <div class="col-sm-2">
                    <input type="text" name="changeRule" class="form-control" value="${refundChangeRuleDto.changeRule}" placeholder="示例:10-0-20">
                </div>
                <div class="col-sm-4 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <i class="fa fa-question-circle"></i>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">是否可签转</label>
                <div class="col-sm-1">
                    <label class="radio-inline"><input type="radio" name="endorse" value="1" <c:if test="${refundChangeRuleDto.endorse}">checked</c:if>/>是</label>
                    <label class="radio-inline"><input type="radio" name="endorse" value="0" <c:if test="${!refundChangeRuleDto.endorse}">checked</c:if>/>否</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">适用航空公司</label>
                <div class="col-sm-2">
                    <input type="text"  class="form-control" id="carrierCode" name="carrierCode" value="${refundChangeRuleDto.carrierCode}" placeholder="示例:MU">
                </div>
                <div class="col-sm-4 input-group carrier" >
                    <input type="text" name="carrierName" id="carrierName" class="form-control" value="${refundChangeRuleDto.carrierName}" placeholder="航司名称" readonly>
                    <span class="input-group-addon"><i class="fa fa-plane"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">适用舱位</label>
                <div class="col-sm-2">
                    <select name="cabinGrade" class="form-control">
                        <c:forEach items="${cabinGrade}" var="grade">
                            <option value="${grade.itemCode}" <c:if test="${grade.itemCode eq refundChangeRuleDto.cabinGrade}">selected</c:if>>${grade.itemTxt}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group" >
                <div class="col-sm-6 col-sm-offset-2" id="cabinCodeDiv" style="padding-left: 20px;">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">计算标准</label>
                <div class="col-sm-2">
                    <label class="radio-inline"><input type="radio" name="referenceBasis" value="1" <c:if test="${refundChangeRuleDto.referenceBasis eq 1}">checked</c:if>/>票面价</label>
                    <label class="radio-inline"><input type="radio" name="referenceBasis" value="0" <c:if test="${refundChangeRuleDto.referenceBasis eq 0}">checked</c:if>/>标准价</label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">免费行李额</label>
                <div class="col-sm-2">
                    <input type="number" name="baggage" class="form-control"  value="${refundChangeRuleDto.baggage}">
                </div>
                <div class="col-sm-1 text-warning" style="padding-left: 0px;line-height: 27px;">
                    <span>(单位:kg)</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">适用航程</label>
                <div class="col-sm-2">
                    <c:choose>
                        <c:when test="${refundChangeRuleDto.fromAirport eq 'all'}">
                            <input type="text" name="fromAirport" class="form-control"  value="全国" placeholder="示例:SHA" readonly>
                        </c:when>
                        <c:otherwise>
                            <input type="text" name="fromAirport" class="form-control"  value="${refundChangeRuleDto.fromAirport}" placeholder="示例:SHA" readonly>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-sm-1 input-group" style="padding-left: 0px;padding-right:0px;line-height: 33px;width: 18px;">
                    <span>至</span>
                </div>
                <div class="col-sm-2">
                    <c:choose>
                        <c:when test="${refundChangeRuleDto.toAirport eq 'all'}">
                            <input type="text" name="toAirport" class="form-control"  value="全国" placeholder="示例:SHA" readonly>
                        </c:when>
                        <c:otherwise>
                            <input type="text" name="toAirport" class="form-control"  value="${refundChangeRuleDto.toAirport}" placeholder="示例:PEK" readonly>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">出票有效期</label>
                <div class="col-sm-3 input-group">
                    <input type="text" name="ticketValidityStartDate" class="form-control time-picker"  placeholder="请输入开始日期"
                           value="<fmt:formatDate value='${refundChangeRuleDto.ticketValidityStartDate }' pattern="yyy-MM-dd"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="col-sm-3 input-group">
                    <input type="text" name="ticketValidityEndDate" class="form-control time-picker" placeholder="请输入截止日期"
                           value="<fmt:formatDate value='${refundChangeRuleDto.ticketValidityEndDate }' pattern="yyy-MM-dd"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">起飞有效期</label>
                <div class="col-sm-3 input-group">
                    <input type="text" name="takeOffValidityStartDate" class="form-control time-picker"  placeholder="请输入开始日期"
                           value="<fmt:formatDate value='${refundChangeRuleDto.takeOffValidityStartDate }' pattern="yyy-MM-dd"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
                <div class="col-sm-3 input-group">
                    <input type="text" name="takeOffValidityEndDate" class="form-control time-picker" placeholder="请输入截止日期"
                           value="<fmt:formatDate value='${refundChangeRuleDto.takeOffValidityEndDate }' pattern="yyy-MM-dd"/>" readonly>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">备注</label>
                <div class="col-sm-6">
                    <textarea class="form-control" rows="3" name="remark">${refundChangeRuleDto.remark}</textarea>
                </div>
            </div>

            <div class="form-group" style="margin-left: 200px;  ">
                <div class="col-sm-2" style="text-align: right">
                    <button type="button" class="btn btn-success" style="width: 100px;" onclick="refundChangeRule.detail.saveForm()">保存</button>
                </div>
                <div class="col-sm-2" style="text-align: right">
                    <button type="button" class="btn btn-success" style="width: 100px;" onclick="refundChangeRule.detail.saveAndAdd()" >保存并新增</button>
                </div>
                <div class="col-sm-2" style="text-align: right">
                    <a type="button" class="btn btn-success" style="width: 100px;" href="flight/refundChangeRule" >返回列表</a>
                </div>
            </div>
        </form>

    </div>
</div>
<div class="help" hidden>
    <p>格式说明:10-0-20表示起飞前10%手续费,起飞后20%手续费</p>
    <p>10-72-15-2-30表示起飞前72小时前10%手续费,72小时至起飞前2小时15%手续费,起飞前2小时及起飞后30%手续费</p>
    <p>以此类推，可以有更多</p>
    <p>仅支持英文输入法下的数字和横线'-'的组合</p>

</div>
<script type="text/javascript" src="webpage/flight/refundChangeRule/refundChangeRule.js?version=${globalVersion}"></script>
<script>
    $(function(){
        refundChangeRule.detail.init();
        if('${refundChangeRuleDto.id}' ==''){
            refundChangeRule.detail.setDefaultValue();
        }
    })
</script>