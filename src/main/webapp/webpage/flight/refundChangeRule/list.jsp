<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .word{
        word-wrap:break-word;word-break:break-all;
    }
</style>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th><label>序号</label></th>
        <th width="110"><label>退票规定</label></th>
        <th width="110"><label>同舱改期规定</label></th>
        <th><label>是否可以签转</label></th>
        <th><label>适用航司</label></th>
        <th width="240"><label>适用舱位</label></th>
        <th><label>计算标准</label></th>
        <th><label>出票有效期</label></th>
        <th><label>起飞有效期</label></th>
        <th><label>最后更新时间</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="rule" varStatus="vs">
        <tr class="odd gradeX">
            <td>${vs.count}</td>
            <td>${rule.refundRule }</td>
            <td>${rule.changeRule }</td>
            <td>${rule.endorse?"是":"否" }</td>
            <td>${rule.carrierCode }</td>
            <td class="word">${fn:substring(rule.cabinCodes ,0 ,fn:length(rule.cabinCodes )-1 )}</td>
            <td>${(rule.referenceBasis eq 1)?"票面价":"标准价" }</td>
            <td>
                <div>起飞：<fmt:formatDate value="${rule.ticketValidityStartDate }" pattern="yyy-MM-dd"/></div>
                <div>到达：<fmt:formatDate value="${rule.ticketValidityEndDate }" pattern="yyy-MM-dd"/></div>
            </td>
            <td>
                <div>起飞：<fmt:formatDate value="${rule.takeOffValidityStartDate }" pattern="yyy-MM-dd"/></div>
                <div>到达：<fmt:formatDate value="${rule.takeOffValidityEndDate }" pattern="yyy-MM-dd"/></div>
            </td>
            <td><fmt:formatDate value="${rule.lastUpdateDate }" pattern="yyy-MM-dd HH:mm:ss"/></td>
            <td class="text-center">
                <a href="flight/refundChangeRule/detail?id=${rule.id}">修改</a>&nbsp;&nbsp;
                <a onclick="refundChangeRule.list.delete('${rule.id }');">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=refundChangeRule.list. reloadList"></jsp:include>
</div>