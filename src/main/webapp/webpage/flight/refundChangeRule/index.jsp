<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div >
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 退改签列表
            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <a class="btn btn-default" href="flight/refundChangeRule/detail" role="button">新增退改签规则</a>
                                        </div>
                                        <!-- 查询条件 -->
                                        <form class="form-inline" method="post">
                                            <input type="hidden" id="carrierCode" >
                                            <nav class="text-right">
                                                <label class="control-label">航司：</label>
                                                <div class="input-group carrier">
                                                    <input type="text" id="carrierName" class="form-control"  placeholder="航司名称" readonly>
                                                    <span class="input-group-addon"><i class="fa fa-plane"></i></span>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">舱位：</label>
                                                    <input type="text" id="cabinCode" class="form-control" placeholder="舱位">
                                                </div>

                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default" onclick="refundChangeRule.list.reloadList()">查询</button>
                                                    <button type="button" class="btn btn-default" onclick=" refundChangeRule.list.reset()">重置</button>
                                                </div>
                                            </nav>
                                        </form>
                                    </div>
                                </div>
                                <div id="listData" style="margin-top: 15px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="webpage/flight/refundChangeRule/refundChangeRule.js?version=${globalVersion}"></script>
<script>
    $(function(){
       refundChangeRule.list.init();
    })
</script>