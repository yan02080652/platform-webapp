<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="cabinMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>/ ${carrierDto.nameCn} / 舱位管理   &nbsp;&nbsp;
	           <a data-pjax onclick="backCarrier()">返回</a>
			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12"> 
										<div class="pull-left">
											<a class="btn btn-default" onclick="getCabin('${carrierDto.code}')" role="button">新增舱位</a>
											<a class="btn btn-default" onclick="batchDeleteCabin()" role="button">批量删除</a>
										</div>
										
											<!-- 查询条件 -->
											<form class="form-inline" method="post" >
												<nav class="text-right">
												<input type="hidden" id="carrierCode_cabin" value="${carrierDto.code }"/>
													<div class=" form-group">
													 <label class="control-label">舱位类型：</label>
														<select class="form-control" name="cabinType" id="cabinType">
															<option value="">全部</opton>
																<c:if test="${not empty dicMap.cabinType }">
																	<c:forEach items="${dicMap.cabinType }" var="type">
																		<option value="${type.itemCode }">${type.itemTxt }</option>
																	</c:forEach>
																</c:if>
														</select> 
													</div>

												<div class=" form-group">
													<label class="control-label">舱位等级：</label> <select
														class="form-control" name="cabinGrade" id="cabinGrade">
														<option value="">全部</opton>
															<c:if test="${not empty dicMap.cabinGrade }">
																<c:forEach items="${dicMap.cabinGrade }" var="grade">
																	<option value="${grade.itemCode }">${grade.itemTxt }</option>
																</c:forEach>
															</c:if>
													</select>
												</div>
												
												<div class=" form-group">
													 <label class="control-label">其他：</label>
														<input name="keyword" id="cabinCode" type="text"  class="form-control" placeholder="舱位编码" style="width: 250px"/>
													</div>
													<div class="form-group">
														<button type="button" class="btn btn-default" onclick="search_cabin()">查询</button>
														<button type="button" class="btn btn-default" onclick="reset_cabin()">重置</button>
													</div>
												</nav>
											</form>
										</div>
									</div>
									<div  id="cabin_Table" style="margin-top: 15px">
					
					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 详细页面 -->
<div id="cabinDetail" style="display:none;"></div>

<script type="text/javascript" src="webpage/flight/cabin/cabin.js?version=${globalVersion}"></script>