var cabinTypeMap={};//舱位类型
var cabinGradeMap={};//舱位等级
$(function(){
	init();
})

//加载航司，舱位等信息
function loadData(callback){
	$.post("flight/cabin/getDicMap",function(data){
		$(data.cabinType).each(function(){
			cabinTypeMap[this.itemCode] = this.itemTxt;
			
		})
		$(data.cabinGrade).each(function(){
			cabinGradeMap[this.itemCode] = this.itemTxt;
		})
		
		callback();
	});
}

//初始化
function init(pageIndex){
	loadData(function(){
		reloadRightCabin(pageIndex);
	});
}

//分页调用的方法
function reloadCabin(pageIndex){
	init(pageIndex);
}

//加载列表
function reloadRightCabin(pageIndex){
	var cabinCode = $("#cabinCode").val();
	var cabinType = $("#cabinType").val();
	var cabinGrade = $("#cabinGrade").val();
	var carrierCode = $("#carrierCode_cabin").val();
	$.post('flight/cabin/list',{
		cabinCode:cabinCode,
		cabinType:cabinType,
		cabinGrade:cabinGrade,
		carrierCode:carrierCode,
		pageIndex:pageIndex,
		},function(data){
		$("#cabin_Table").html(null);
		$("#cabin_Table").html(data);
		
		$.each($('.carrier'),function(i,dom){
			var txt = $(dom).text();
			if(txt){
				$(dom).text(carrierMap[txt]);
			}
		});
		
		$.each($('.cabinType'),function(i,dom){
			var txt = $(dom).text();
			if(txt){
				$(dom).text(txt=='-1'?'--':cabinTypeMap[txt]);
			}
		});
		
		$.each($('.cabinGrade'),function(i,dom){
			var txt = $(dom).text();
			if(txt){
				$(dom).text(cabinGradeMap[txt]);
			}
		});
	});
}


//查找用户
function search_cabin(){
	init();
}

//重置
function reset_cabin(){
	$("#cabinCode").val("");
	$("#cabinGrade").val("");
	$("#cabinType").val("");
	init();
}

//转到添加或修改页面
function getCabin(carrierCode,id){
	$.post("flight/cabin/getCabin",{'carrierCode':carrierCode,'id':id},function(data){
		$("#cabinDetail").html(data);
		$("#cabinMain").hide();
		$("#cabinDetail").show();
	});
}


//批量删除
function batchDeleteCabin(){
  //判断至少选了一项
  var checkedNum = $("#cabinTable input[name='ids']:checked").length;
  if(checkedNum==0){
      $.alert("请选择你要删除的记录!","提示");
      return false;
  }
  
  var ids = new Array();
  $("#cabinTable input[name='ids']:checked").each(function(){
  	ids.push($(this).val());
  });
  
  confirmDeleteCabin(ids.toString());
}

//单个删除
function deleteCabin(id) {
	confirmDeleteCabin(id);
};

//确认删除
function confirmDeleteCabin(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除所选记录?",
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "flight/cabin/delete",
				data : {
					id : id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						init();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

//checkbox全选
function selectAllCabin(){
	if ($("#selectAllCabin").is(":checked")) {
        $("#cabinTable input[type=checkbox]").prop("checked", true);//所有选择框都选中
    } else {
        $("#cabinTable input[type=checkbox]").prop("checked", false);
    }
}
//返回
function backMain(){
	$("#cabinMain").show();
	$("#cabinDetail").hide().html("");
}

//返回航司列表
function backCarrier(){
	$("#carrierMain").show();
	$("#cabinListDiv").hide();
}

