<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>舱位信息<c:choose><c:when
                    test="${cabinDto.id != '' && cabinDto.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
                &nbsp;&nbsp;<a data-pjax onclick="backMain()">返回</a>
            </div>
            <div class="panel-body">
                <form id="cabinForm" class="form-horizontal bv-form">
                    <input type="hidden" name="id" id="id" value="${cabinDto.id }"/>
                    <input type="hidden" name="carrierCode" value="${carrierCode }"/>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">舱位编码</label>
                        <div class="col-sm-6">
                            <input type="text" name="code" id="code" value="${cabinDto.code }" class="form-control"
                                   placeholder="舱位编码">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">舱位类型</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="type" id="c_type">
                                <option value="-1">请选择</option>
                                <c:if test="${not empty dicMap.cabinType }">
                                    <c:forEach items="${dicMap.cabinType }" var="type">
                                        <option value="${type.itemCode }">${type.itemTxt }</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">舱位等级</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="grade" id="grade">
                                <option value="-1">请选择</option>
                                <c:if test="${not empty dicMap.cabinGrade }">
                                    <c:forEach items="${dicMap.cabinGrade }" var="grade">
                                        <option value="${grade.itemCode }">${grade.itemTxt }</option>
                                    </c:forEach>
                                </c:if>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">特殊舱位</label>
                        <div class="col-sm-6">
                            <select class="form-control" name="especial" id="especial">
                                <option value="-1">请选择</option>
                                <option value="0" <c:if test="${!cabinDto.especial}">selected</c:if>>否</option>
                                <option value="1" <c:if test="${cabinDto.especial}">selected</c:if>>是</option>
                            </select>
                        </div>
                    </div>
                </form>
                <div class="form-group">
                    <div class="col-sm-offset-7">
                        <a class="btn btn-default" onclick="submitCabin()">提交</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    //提交表单
    function submitCabin() {
        $.post("flight/cabin/checkCode", {
            code: $("#code").val(),
            carrierCode: $("#carrierCode").val(),
            id: $("#id").val()
        }, function (data) {
            if (data) {
                $('#cabinForm').submit();
            } else {
                $.alert("该编码已存在", "提示");
            }
        });

    }

    $(document).ready(function () {
        $("#cabinForm").validate({
            rules: {
                code: {
                    required: true,
                    minlength: 1,
                    maxlength: 20,
                    remote: {
                        type: 'post',
                        url: 'flight/cabin/checkCode',
                        data: {
                            code: function () {
                                return $('#code').val();
                            },
                            carrierCode: function () {
                                return $('#carrierCode').val();
                            },
                            id: function () {
                                return $('#id').val();
                            },
                        },
                        dataFilter: function (data) {
                            if (data == 'true') {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                },
                carrierCode: {
                    type: true
                },
                grade: {
                    type: true
                },
                especial: {
                    type: true
                }
            },
            messages: {
                code: {
                    remote: "此编码已经存在"
                }
            },
            submitHandler: function (form) {
                $.ajax({
                    cache: true,
                    type: "POST",
                    url: "flight/cabin/save",
                    data: $('#cabinForm').serialize(),
                    async: false,
                    error: function (request) {
                        showErrorMsg("请求失败，请刷新重试");
                    },
                    success: function (data) {
                        if (data.type == 'success') {
                            $("#cabinMain").show();
                            $("#cabinDetail").hide();
                            init();
                        } else {
                            $.alert(data.txt, "提示");
                        }
                    }
                });

            },
            errorPlacement: setErrorPlacement,
            success: validateSuccess,
            highlight: setHighlight
        });

        var type = "${cabinDto.type}";
        var grade = "${cabinDto.grade}";
        if (type) {
            $("#c_type").val(type);
        }
        if (grade) {
            $("#grade").val(grade);
        }

    });
</script>