<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<table class="table table-striped table-bordered table-hover" id="cabinTable">
	<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectAllCabin()" id="selectAllCabin"/></th>
	      <th><label>舱位编码</label></th>
          <th><label>舱位类型</label></th>
          <th><label>舱位等级</label></th>
          <th><label>特殊舱位</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="cabin">
         <tr class="odd gradeX">
        	 <td><input type="checkbox" name="ids" value="${cabin.id }"/></td>
             <td>${cabin.code }</td>
             <td class="cabinType">${cabin.type }</td>
             <td class="cabinGrade">${cabin.grade }</td>
             <td class="cabinGrade">
                 <c:if test="${cabin.especial}">是</c:if>
                 <c:if test="${!cabin.especial}">否</c:if>
             </td>
             <td class="text-center">
             	<a onclick="getCabin('${cabin.carrierCode }','${cabin.id }')" >修改</a>&nbsp;&nbsp;|
             	<a onclick="deleteCabin('${cabin.id }');">删除</a> 
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadCabin"></jsp:include> 
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>