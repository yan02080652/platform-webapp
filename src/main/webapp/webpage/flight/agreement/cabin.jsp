<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    body {
        background-color: #FFFFFF;
    }

    .layui-form-label {
        box-sizing: content-box;
        width: 120px;
    }

    .box {
        padding-top: 20px;
    }

    .long_text {
        width: 120px;
    }

</style>
<div class="box">
    <form class="layui-form" action="" id="agreementCabinForm">
        <input type="hidden" value="${agreementCabinDto.id}" name="id"/>
        <input type="hidden" value="${policyId}" name="agreementPolicyId"/>

        <div class="layui-form-item" >
            <label class="layui-form-label">除外旅行日期<i class="rule-tipDate fa fa-question-circle layui-word-aux"></i></label>
            <div class="layui-input-inline">
                <input  style="width: 700px" name="invalidTravelDate" value="${agreementCabinDto.invalidTravelDate}"
                        placeholder="500字以内"
                        class="layui-input">
            </div>
        </div>
        <div class="layui-form-item"  >
            <label class="layui-form-label">适用航线<i class="rule-tipFlight fa fa-question-circle layui-word-aux"></i></label>
            <div class="layui-input-inline">
                <input  style="width: 700px" name="effectiveAirline" value="${agreementCabinDto.effectiveAirline}"
                        placeholder="2000字以内"
                        class="layui-input">
            </div>
        </div>
        <div class="layui-form-item"  >
            <label class="layui-form-label">除外航线<i class="rule-tipFlight fa fa-question-circle layui-word-aux"></i></label>
            <div class="layui-input-inline">
                <input  style="width: 700px" name="invalidAirline" value="${agreementCabinDto.invalidAirline}"
                        placeholder="2000字以内"
                        class="layui-input">
            </div>
        </div>


        <div class="layui-form-item">
            <label class="layui-form-label">适用舱位</label>
            <div class="layui-input-block">
                <input type="checkbox" lay-filter="selectAll" title="全选" lay-skin="primary" lay-verify="checkedOne">
                <c:forEach items="${usableCabinList}" var="cabin">
                    <input type="checkbox" name="cabinCodes" value="${cabin.code}" title="${cabin.code}"
                           lay-skin="primary"
                           <c:if test="${fn:contains(agreementCabinDto.cabinCodes, cabin.code.concat('/'))}">checked</c:if>>
                </c:forEach>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">协议折扣</label>
            <div class="layui-input-inline">
                <input type="number" name="discount" min="0" max="100" value="${agreementCabinDto.discount}"
                       lay-verify="required|discount" class="layui-input">
            </div>

            <label class="layui-form-label">退改规则</label>
            <div class="layui-input-inline" style="width: 300px">
                <c:choose>
                    <c:when test="${ empty agreementCabinDto}">
                        <input lay-filter="refund" type="radio" name="standardRefund" value="1" title="标准退改" checked>
                        <input lay-filter="refund" type="radio" name="standardRefund" value="0" title="特殊退改">
                    </c:when>
                    <c:otherwise>
                        <input lay-filter="refund" type="radio" name="standardRefund" value="1" title="标准退改"
                               <c:if test="${agreementCabinDto.standardRefund}">checked</c:if>>
                        <input lay-filter="refund" type="radio" name="standardRefund" value="0" title="特殊退改"
                               <c:if test="${!agreementCabinDto.standardRefund}">checked</c:if>>
                    </c:otherwise>
                </c:choose>

            </div>

        </div>
        <div class="layui-form-item" id="refundRuleInput">
            <label class="layui-form-label">退票规定<i class="rule-tip fa fa-question-circle layui-word-aux"></i></label>
            <div class="layui-input-inline">
                <input type="text" name="refundRule" value="${agreementCabinDto.refundRule}"
                       lay-verify="requiredByRefund|numberAndUnderline|identical"
                       class="layui-input" placeholder="示例:10-0-20">
            </div>
            <label class="layui-form-label long_text">同舱改期规定</label>
            <div class="layui-input-inline">
                <input type="text" name="changeRule" value="${agreementCabinDto.changeRule}"
                       lay-verify="requiredByRefund|numberAndUnderline|identical"
                       class="layui-input" placeholder="示例:10-0-20">
            </div>
        </div>
        <div class="layui-form-item" id="endorseInput">
            <label class="layui-form-label">是否可签转</label>
            <div class="layui-input-inline">
                <c:choose>
                    <c:when test="${ empty agreementCabinDto}">
                        <input type="radio" name="endorse" value="1" title="是">
                        <input type="radio" name="endorse" value="0" title="否" checked>
                    </c:when>
                    <c:otherwise>
                        <input type="radio" name="endorse" value="1" title="是"
                               <c:if test="${agreementCabinDto.endorse}">checked</c:if>>
                        <input type="radio" name="endorse" value="0" title="否"
                               <c:if test="${!agreementCabinDto.endorse}">checked</c:if>>
                    </c:otherwise>
                </c:choose>
            </div>
            <label class="layui-form-label long_text">计算基准</label>
            <div class="layui-input-inline">
                <c:choose>
                    <c:when test="${ empty agreementCabinDto}">
                        <input type="radio" name="referenceBasis" value="1" title="票面价" checked>
                        <input type="radio" name="referenceBasis" value="2" title="全价">
                    </c:when>
                    <c:otherwise>
                        <input type="radio" name="referenceBasis" value="1" title="票面价"
                               <c:if test="${agreementCabinDto.referenceBasis eq 1}">checked</c:if>>
                        <input type="radio" name="referenceBasis" value="2" title="全价"
                               <c:if test="${agreementCabinDto.referenceBasis ne 1}">checked</c:if>>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="from">保存</button>
                <button onclick="closeIframe()" class="layui-btn layui-btn-primary">取消</button>
            </div>
        </div>
    </form>
</div>

<div class="CabineffectiveDate" hidden>
    <p>每个日期区间按照【起始日期】+【"-"】+【截止日期】构成，多个日期区间以英文","分隔</p>
    <p>示例:20180214-20180222,20181001-20181007</p>
</div>
<div class="CabineffectiveAirline" hidden>
    <p>每条航线由【多个出发机场三字码】+【"-"或"="】+【多个到达机场三字码】构成</p>
    <p>其中-表示单程，=表示往返程，目前由去程回程两个单程运价拼接而成的不能算作往返程运价</p>
    <p>多个机场以“/”分隔，多条航线由英文","分隔</p>
    <p>ALL代表所有机场，ALL-ALL表示适用所有单程航线</p>
    <p>示例:SHA/NKG/HFE/WNZ-ALL,ALL-ALL</p>
</div>

<div class="help" hidden>
    <p>格式说明:10-0-20表示起飞前10%手续费,起飞后20%手续费</p>
    <p>10-72-15-2-30表示起飞前72小时前10%手续费,72小时至起飞前2小时15%手续费,起飞前2小时及起飞后30%手续费</p>
    <p>以此类推，可以有更多</p>
    <p>退票规定与同舱改期规定格式应一致</p>
    <p>仅支持英文输入法下的数字和横线'-'的组合</p>

</div>
<script type="text/javascript" src="webpage/flight/agreement/agreement.js?version=${globalVersion}"></script>
<script>
    $(function () {
        agreement.cabin.init();
    })

    //关闭iframe
    function closeIframe() {
        //先得到当前iframe层的索引,再执行关闭
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }

</script>