<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    td {
        text-align: center;
    }

    .word {
        width: 200px;
        display: block;
        text-overflow: ellipsis;
        -webkit-line-clamp: 1;
        white-space: nowrap;
        overflow: hidden;
        text-align: center;
    }

    .reqMark {
        color: red;
        font-weight: bold;
        font-size: 15px;
    }
    .layui-form-switch {
        width: 52px;
    }
</style>
<link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        协议政策维护&nbsp;<a href="flight/agreement">返回</a>
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="page-header">
                    <h3>基础信息 |&nbsp;
                        <button class="layui-btn layui-btn-normal" onclick="agreement.detail.saveForm()">
                            <i class="layui-icon">&#xe63c;保存</i>
                        </button>
                    </h3>
                </div>
                <form class="form-horizontal layui-form" id="agreementForm" role="form">
                    <input type="hidden" name="id" value="${agreementPolicyDto.id}"/>
                    <input type="hidden" name="originPolicyId" value="${agreementPolicyDto.originPolicyId}">
                    <input type="hidden" name="operateType" value="${operateType}">

                    <div class="form-group">
                        <label class="col-sm-2"><span class="reqMark">*</span>协议类型</label>
                        <div class="col-sm-3" style="width: 250px">
                            <c:forEach items="${policyTypeMap}" var="entry" varStatus="vs">
                                <c:choose>
                                    <c:when test="${ empty agreementPolicyDto}">
                                        <input  lay-filter="type" type="radio" name="type" value="${entry.key}" title="${entry.value}"
                                               <c:if test="${vs.index eq 0}">checked</c:if>/>
                                    </c:when>
                                    <c:otherwise>
                                        <input lay-filter="type" type="radio" name="type" value="${entry.key}" title="${entry.value}"
                                               <c:if test="${agreementPolicyDto.type eq entry.key}">checked</c:if>/>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </div>
                        <c:if test="${agreementPolicyDto.type eq '1' and operateType eq '2' }">
                            <div class="col-sm-1">
                                <a type="button" class="btn btn-default" href="/flight/agreement/shareScope?id=${agreementPolicyDto.id}" target="_blank" >设置共享企业</a>
                            </div>
                            <div class="col-sm-1" style="padding-left: 20px">
                                <a type="button" class="btn btn-default" href="flight/agreement/detail?id=${agreementPolicyDto.originPolicyId}&operateType=2" target="_blank">查看原始协议</a>
                            </div>
                        </c:if>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 "><span class="reqMark">*</span>产品类型</label>
                        <div class="col-sm-5 ">
                            <c:forEach items="${agreementTypeEnumMap}" var="entry" varStatus="vs">
                                <c:choose>
                                    <c:when test="${ empty agreementPolicyDto}">
                                        <input type="radio" name="productType" value="${entry.key}" title="${entry.value}"
                                               <c:if test="${vs.index eq 0}">checked</c:if>/>
                                    </c:when>
                                    <c:otherwise>
                                        <input type="radio" name="productType" value="${entry.key}" title="${entry.value}"
                                               <c:if test="${agreementPolicyDto.productType eq entry.key}">checked</c:if>/>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2"><span class="reqMark">*</span>大客户编码</label>
                        <div class="col-sm-5">
                            <input type="text" name="customerAgreementCode" class="form-control"
                                   value="${agreementPolicyDto.customerAgreementCode}" placeholder="大客户编码" maxlength="30">
                        </div>
                    </div>

                    <c:if test="${agreementPolicyDto.type eq '1' }">
                        <div class="form-group">
                            <label class="col-sm-2">原始协议</label>
                            <div class="col-sm-5">
                                ${agreementPolicyDto.originPolicyName}
                            </div>
                        </div>
                    </c:if>

                    <div class="form-group">
                        <label class="col-sm-2"><span class="reqMark">*</span>适用航司</label>
                        <div class="col-sm-1">
                            <span id="chooseCarrier" class="carrierBtn layui-btn layui-btn-primary layui-btn-small"
                                  onclick="agreement.detail.chooseCarrier()">
                                 <i class="layui-icon">&#xe61f;航司</i>
                             </span>
                        </div>
                        <div class="codes" id="codesDiv">
                            <c:if test="${not empty agreementPolicyDto.carrierCodes}">已选:${agreementPolicyDto.carrierCodes}</c:if>
                        </div>
                        <input type="text" name="carrierCodes" hidden value="${agreementPolicyDto.carrierCodes}"/>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2"><span class="reqMark">*</span>白名单控制</label>
                        <div class="col-sm-1">
                            <input type="checkbox" name="whiteListControl" lay-skin="switch" lay-text="ON|OFF" <c:if test="${agreementPolicyDto.whiteListControl}">checked</c:if>>
                            <input type="hidden" value="${agreementPolicyDto.whiteListId}" id="whiteListId" name="whiteListId">
                        </div>
                        <c:if test="${operateType eq '2' and agreementPolicyDto.whiteListControl}">
                            <div class="col-sm-1">
                                <a type="button" class="btn btn-default whitelist" href="flight/whitelist/detail?id=${agreementPolicyDto.whiteListId}" target="_blank">管理白名单</a>
                            </div>
                        </c:if>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2"><span class="reqMark">*</span>协议名称</label>
                        <div class="col-sm-5">
                            <input type="text" name="name" class="form-control" value="${agreementPolicyDto.name}"
                                   placeholder="协议名称" maxlength="50">
                        </div>
                    </div>
                    <c:if test="${agreementPolicyDto.type ne '1' or empty agreementPolicyDto.id}">
                        <div class="form-group" id="partnerInput">
                            <label class="col-sm-2"><span class="reqMark">*</span>适用企业</label>
                            <div class="col-sm-5">
                                <input type="hidden" name="partnerScope" id="partnerScope"
                                       value="${agreementPolicyDto.partnerScope}"/>
                                <input type="text" class="form-control" id="partnerName"
                                       value="${agreementPolicyDto.partnerName}" readonly placeholder="企业名称">
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn btn-default partnerChoose">请选择...</button>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${agreementPolicyDto.type ne '1' or empty agreementPolicyDto.id}">
                        <div class="form-group" id="effectiveSaleDayInput">
                            <label class="col-sm-2"><span class="reqMark">*</span>适用销售日期<i
                                    class="effective-date fa fa-question-circle layui-word-aux"></i></label>
                            <div class="col-sm-5">
                                <input type="text" name="effectiveSaleDay" class="form-control"
                                       value="${agreementPolicyDto.effectiveSaleDay}"
                                       placeholder="适用销售日期(示例:20180214-20180222)" maxlength="100">
                            </div>
                        </div>
                    </c:if>

                    <c:if test="${agreementPolicyDto.type ne '1' or empty agreementPolicyDto.id}">
                        <div class="form-group" id="showNameInput">
                            <label class="col-sm-2"><span class="reqMark">*</span>展示标签</label>
                            <div class="col-sm-5">

                                <input type="text" class="form-control" name="showName" id="showName"
                                       value="${agreementPolicyDto.showName}" placeholder="展示标签">
                            </div>
                        </div>
                    </c:if>

                    <div class="form-group">
                        <label class="col-sm-2 ">状态</label>
                        <div class="col-sm-5">
                            <c:choose>
                                <c:when test="${ empty agreementPolicyDto}">
                                    <input type="radio" name="status" value="1" checked="checked" title="启用"/>
                                    <input type="radio" name="status" value="0" title="禁用"/>
                                </c:when>
                                <c:otherwise>
                                    <input type="radio" name="status" value="1"
                                           <c:if test="${agreementPolicyDto.status}">checked</c:if> title="启用"/>
                                    <input type="radio" name="status" value="0" title="禁用"
                                           <c:if test="${!agreementPolicyDto.status}" >checked</c:if>/>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>

                    <c:if test="${ not empty agreementPolicyDto}">
                        <div class="form-group">
                            <label class="col-sm-2 ">创建人</label>
                            <div class="col-sm-4">
                                <label>${creator.fullname }</label>
                            </div>
                            <label class="col-sm-2 ">创建时间</label>
                            <div class="col-sm-4">
                                <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                                       value="${agreementPolicyDto.createDate }"/></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 ">最后修改人</label>
                            <div class="col-sm-4">
                                <label>${lastModifier.fullname }</label>
                            </div>
                            <label class="col-sm-2 ">最后修改时间</label>
                            <div class="col-sm-4">
                                <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                                       value="${agreementPolicyDto.lastUpdateDate }"/></label>
                            </div>
                        </div>
                    </c:if>


                    <div class="page-header">
                        <h3>适用条件 |</h3>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2">适用旅行日期<i
                                class="effective-date fa fa-question-circle layui-word-aux"></i></label>
                        <div class="col-sm-5">
                            <textarea class="form-control" rows="4" name="effectiveTravelDay" class="form-control"
                                      placeholder="适用旅行日期(示例:20180214-20180222)" maxlength="500">${agreementPolicyDto.effectiveTravelDay}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2">适用航线<i
                                class="effective-airline fa fa-question-circle layui-word-aux"></i></label>
                        <div class="col-sm-5">
                            <textarea class="form-control" rows="4" name="effectiveAirline" class="form-control"
                                       maxlength="2000" placeholder="适用航线(示例:SHA/NKG/HFE/WNZ-ALL)">${agreementPolicyDto.effectiveAirline}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2">不适用航线<i
                                class="effective-airline fa fa-question-circle layui-word-aux"></i></label>
                        <div class="col-sm-5">
                            <textarea class="form-control" rows="4" name="invalidAirline" class="form-control"
                                       maxlength="2000" placeholder="不适用航线(示例:SHA/NKG/HFE/WNZ-ALL)">${agreementPolicyDto.invalidAirline}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2">航班号类型<i
                                class="effective-flightno fa fa-question-circle layui-word-aux"></i></label>
                        <div class="col-sm-2">
                            <select name="filterType" class="form-control col-sm-2">
                                <c:forEach items="${filterTypeEnums}" var="typeEnum">
                                    <option value="${typeEnum}"
                                            <c:if test="${agreementPolicyDto.filterType eq typeEnum}">selected</c:if> >${typeEnum.message}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="flightNo" class="form-control col-sm-2"
                                   value="${agreementPolicyDto.flightNo}" placeholder="航班号(示例:0-100,200-300)" maxlength="100">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2">航班销售限额</label>
                        <div class="col-sm-5">
                            <input type="number" name="saleQuota" min="0" class="form-control"
                                   value="${agreementPolicyDto.saleQuota}" placeholder="航班销售限额" max="500">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2">连接设置</label>
                        <div class="col-sm-5">
                            <table class="table table-striped table-bordered" lay-skin="row">
                                <tr>
                                    <td><label>绑定连接名称</label></td>
                                    <td><label>参与验价</label></td>
                                    <td><label>支付前占座</label></td>
                                    <td><label>自动出票</label></td>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="hubsConnectId" class="form-control col-sm-2">
                                            <option value="">请选择</option>
                                            <c:forEach items="${connectMap}" var="map">
                                                <optgroup label="${map.key}">
                                                    <c:forEach items="${map.value}" var="hubsConnect">
                                                        <option value="${hubsConnect.id}"
                                                                <c:if test="${agreementPolicyDto.hubsConnectId eq hubsConnect.id}">selected</c:if> >${hubsConnect.name}</option>
                                                    </c:forEach>
                                                </optgroup>
                                            </c:forEach>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="checkbox" name="checkPrice" lay-filter="selectAll"
                                               <c:if test="${agreementPolicyDto.checkPrice}">checked</c:if>>
                                    </td>
                                    <td>
                                        <input type="checkbox" name="payAfterSeat" lay-filter="selectSeat"
                                               <c:if test="${agreementPolicyDto.payAfterSeat}">checked</c:if>>
                                    </td>
                                    <td>
                                        <input type="checkbox" name="autoIssue" lay-filter="selectAll"
                                               <c:if test="${agreementPolicyDto.autoIssue}">checked</c:if>>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="form-group" id="pnrInput">
                        <label class="col-sm-2">pnr附加指令</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="pnr"
                                   value="${agreementPolicyDto.pnr}" placeholder="pnr附加指令">
                        </div>

                    </div>
                </form>
            </div>

            <c:if test="${agreementPolicyDto.id != null}">
                <!-- 适用舱位列表-->
                <div class="col-sm-12">
                    <div class="page-header">
                        <h3>协议折扣 |&nbsp;
                            <button class="layui-btn layui-btn-normal"
                                    onclick="agreement.cabin.detail(${agreementPolicyDto.id})">
                                <i class="layui-icon">&#xe61f;适用舱位</i>
                            </button>
                        </h3>
                    </div>
                    <div>
                        <table class="table table-striped table-bordered table-hover" lay-skin="row" id="itemTable">
                            <thead>
                            <tr>
                                <td><label>序号</label></td>
                                <td><label>除外旅行日期</label></td>
                                <td><label>适用航线</label></td>
                                <td><label>除外航线</label></td>
                                <td><label>适用舱位</label></td>
                                <td><label>协议折扣</label></td>
                                <td><label>退改规定</label></td>
                                <td><label>操作</label></td>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${agreementPolicyDto.agreementCabinDtos}" var="cabin" varStatus="vs">
                                <tr>
                                    <td style="text-align: center">${vs.count}</td>
                                    <td style="word-break:break-all" width="200px">${cabin.invalidTravelDate}</td>
                                    <td style="word-break:break-all" width="200px">${cabin.effectiveAirline}</td>
                                    <td style="word-break:break-all" width="200px">${cabin.invalidAirline}</td>
                                    <td style="word-break:break-all" width="200px">${fn:substring(cabin.cabinCodes,0 ,fn:length(cabin.cabinCodes)-1 )}</td>
                                    <td>${cabin.discount}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${cabin.standardRefund}">
                                                标准退改
                                            </c:when>
                                            <c:otherwise>
                                                退改规定：${cabin.refundRule}<br/>
                                                同期改签：${cabin.changeRule}<br/>
                                                可以签转：${cabin.endorse?'是':'否'}<br/>
                                                计算基准：${cabin.referenceBasis == '1'?'票面价':'全价'}
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-sm"
                                                onclick="agreement.cabin.detail(${agreementPolicyDto.id},${cabin.id})">
                                            编辑
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm"
                                                onclick="agreement.cabin.delete(${cabin.id})">删除
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>

<div class="effectiveDate" hidden>
    <p>每个日期区间按照【起始日期】+【"-"】+【截止日期】构成，多个日期区间以英文","分隔</p>
    <p>示例:20180214-20180222,20181001-20181007</p>
</div>
<div class="effectiveAirline" hidden>
    <p>每条航线由【多个出发机场三字码】+【"-"或"="】+【多个到达机场三字码】构成</p>
    <p>其中-表示单程，=表示往返程，目前由去程回程两个单程运价拼接而成的不能算作往返程运价</p>
    <p>多个机场以“/”分隔，多条航线由英文","分隔</p>
    <p>ALL代表所有机场，ALL-ALL表示适用所有单程航线</p>
    <p>示例:SHA/NKG/HFE/WNZ-ALL,ALL-ALL</p>
</div>
<div class="effectiveFlightno" hidden>
    <p>如果多个航班组以英文","分隔</p>
    <p>示例:0-100,200-300</p>
</div>

<div class="detailForm" hidden>
    <input type="hidden" id="productType" value="${agreementPolicyDto.productType}">
    <input type="hidden" id="customerAgreementCode" value="${agreementPolicyDto.customerAgreementCode}">
    <input type="hidden" id="carrierCodes" value="${agreementPolicyDto.carrierCodes}">
    <input type="hidden" id="whiteListControl" value="${agreementPolicyDto.whiteListControl}">
    <input type="hidden" id="whiteListIds" value="${agreementPolicyDto.whiteListId}">
    <input type="hidden" id="effectiveTravelDay" value="${agreementPolicyDto.effectiveTravelDay}">
    <input type="hidden" id="effectiveAirline" value="${agreementPolicyDto.effectiveAirline}">
    <input type="hidden" id="invalidAirline" value="${agreementPolicyDto.invalidAirline}">
    <input type="hidden" id="filterType" value="${agreementPolicyDto.filterType}">
    <input type="hidden" id="flightNo" value="${agreementPolicyDto.flightNo}">
    <input type="hidden" id="saleQuota" value="${agreementPolicyDto.saleQuota}">
    <input type="hidden" id="hubsConnectId" value="${agreementPolicyDto.hubsConnectId}">
    <input type="hidden" id="checkPrice" value="${agreementPolicyDto.checkPrice}">
    <input type="hidden" id="payAfterSeat" value="${agreementPolicyDto.payAfterSeat}">
    <input type="hidden" id="autoIssue" value="${agreementPolicyDto.autoIssue}">
    <input type="hidden" id="pnr" value="${agreementPolicyDto.pnr}">
</div>


<input type="hidden" id="operateType" value="${operateType}">
<script type="text/javascript" src="resource/plugins/json-utils/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="webpage/flight/agreement/agreement.js?version=${globalVersion}"></script>
<script type="text/javascript">

    agreement.detail.init();
</script>

