<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div >
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 协议列表


                &nbsp;&nbsp;|&nbsp;&nbsp;<a class="btn btn-default" href="flight/agreement/detail?operateType=1" role="button">添加原始协议</a>

            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <!-- 查询条件 -->
                                        <form class="form-inline" method="post">


                                            <div class="form-group">
                                                <label class="control-label">大客户编码:</label>
                                                <div class="input-group ">
                                                    <input style="width: 130px" type="text" id="agreementPolicyCode" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">适用航司:</label>
                                                <div class="input-group ">
                                                    <input style="width: 130px" type="text" id="carrierCode" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">类型:</label>
                                                <div class="input-group ">
                                                    <select id="type" class="form-control">
                                                        <option value="">全部</option>
                                                        <c:forEach items="${policyTypeMap}" var="policy">
                                                            <option value="${policy.key}">${policy.value}</option>
                                                        </c:forEach>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">适用企业:</label>
                                                <div class="input-group ">
                                                    <input type="hidden" name="pid" id="pid"/>
                                                    <input type="text" id="pname" class="form-control"
                                                           placeholder="请选择企业" readonly>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">名称:</label>
                                                <div class="input-group ">
                                                    <input type="text" id="name" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">状态:</label>
                                                <div class="input-group ">
                                                    <select  id="status" class="form-control">
                                                        <option value="">全部</option>
                                                        <option value="1">启用</option>
                                                        <option value="0">禁用</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <button type="button" class="btn btn-default" onclick="agreement.list.reloadList()">查询</button>
                                                <button type="button" class="btn btn-default" onclick=" agreement.list.reset()">重置</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                <div id="listData" style="margin-top: 15px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="webpage/flight/agreement/agreement.js?version=${globalVersion}"></script>
<script>
    $(function(){
        agreement.list.init();
    })
</script>