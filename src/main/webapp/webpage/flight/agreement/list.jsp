<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="text-align: center"><label>协议类型</label></th>
        <th><label>协议名称</label></th>
        <th><label>适用企业</label></th>
        <th><label>适用航司</label></th>
        <th><label>大客户编码</label></th>
        <th><label>状态</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="agreement">
        <tr class="odd gradeX">
            <td style="text-align: center">${policyTypeMap[agreement.type] }</td>
            <td >${agreement.name }</td>
            <td>
                <span style="float: left">${agreement.partnerName }</span>
                <c:if test="${agreement.type == '1'}">
                    <a style="float: right" href="/flight/agreement/shareScope?id=${agreement.id}" target="_blank">设置</a>
                </c:if>
            </td>
            <td style="word-break:break-all" width="200px">${agreement.carrierCodes}</td>
            <td>${agreement.customerAgreementCode}</td>
            <td>${agreement.status?"启用":"禁用"}</td>
            <td class="text-center">
                <a href="flight/agreement/detail?id=${agreement.id}&operateType=2">修改</a>&nbsp;&nbsp;
                <a href="flight/agreement/detail?id=${agreement.id}&operateType=3">复制</a>&nbsp;&nbsp;
                <a onclick="agreement.list.delete('${agreement.id }');">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=agreement.list.reloadList"></jsp:include>
</div>