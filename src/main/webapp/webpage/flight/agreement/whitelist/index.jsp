<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div >
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 大客户名单列表

            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <!-- 查询条件 -->
                                        <form class="form-inline" method="post">


                                            <div class="form-group">
                                                <label class="control-label">大客户编码/协议名称:</label>
                                                <div class="input-group ">
                                                    <input style="width: 130px" type="text" id="agreementPolicyCode" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label">适用航司:</label>
                                                <div class="input-group ">
                                                    <input style="width: 130px" type="text" id="carrierCode" class="form-control">
                                                </div>
                                            </div>


                                            <div class="form-group" style="float: right">
                                                <button type="button" class="btn btn-default" onclick="whitelist.list.readlist()">查询</button>
                                                <button type="button" class="btn btn-default" onclick=" whitelist.list.reset()">重置</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                <div id="listData" style="margin-top: 15px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="webpage/flight/agreement/whitelist/whitelist.js?version=${globalVersion}"></script>
<script>
    $(function(){
        whitelist.list.init();
    })
</script>