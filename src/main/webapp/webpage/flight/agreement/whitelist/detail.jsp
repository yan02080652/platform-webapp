<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .form-group {
        padding-top: 15px;
        padding-bottom: 15px;
    }

</style>
<div >
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 基本信息

            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">

                                            <div class="form-group">
                                                <label class="col-sm-3">原始协议大客户编码</label>
                                                <div class="col-sm-8">
                                                    <label>${whitelistDto.customerAgreementCode}</label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3">原始协议名称</label>
                                                <div class="col-sm-8">
                                                    <label>${whitelistDto.originPolicyName}</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3">原始协议使用航司</label>
                                                <div class="col-sm-8">
                                                    <label>${whitelistDto.originCarrierCodes}</label>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2">创建人</label>
                                                <div class="col-sm-4">
                                                    <label>${whitelistDto.createName }</label>
                                                </div>
                                                <label class="col-sm-2 ">创建时间</label>
                                                <div class="col-sm-4">
                                                    <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                                                           value="${whitelistDto.createTime }"/></label>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 ">最后修改人</label>
                                                <div class="col-sm-4">
                                                    <label>${whitelistDto.updateName }</label>
                                                </div>
                                                <label class="col-sm-2 ">最后修改时间</label>
                                                <div class="col-sm-4">
                                                    <c:if test="${not empty whitelistDto.updator}">
                                                        <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                                                           value="${whitelistDto.updateTime }"/></label>
                                                    </c:if>
                                                </div>
                                            </div>

                                    </div>
                                </div>
                                <%--<div id="listData" style="margin-top: 15px">--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div >
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 5px">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 白名单人员
                    <ul id="whiteUserTab" class="nav nav-tabs">
                        <li class="active"><a  data-toggle="tab" data-value="2">已提交</a></li>
                        <li><a data-toggle="tab" data-value="1">待提交</a></li>

                    </ul>
                </div>


                    <form class="form-inline" method="post">

                        <div class="form-group" style="padding-left: 30px">
                            <label class="control-label">姓名/证件号:</label>
                            <div class="input-group">
                                <input style="width: 130px" type="text" id="nameOrNo" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">所属企业:</label>
                            <div class="input-group ">
                                <input type="hidden" name="pid" id="pid"/>
                                <input type="text" id="pname" class="form-control"
                                       placeholder="请选择企业" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">类型:</label>
                            <div class="input-group ">
                                <select id="userType" class="form-control">
                                    <option value="" selected>全部</option>
                                    <option value="1">员工</option>
                                    <option value="2">非员工</option>
                                </select>

                            </div>
                        </div>

                        <div class="form-group" id="effictiveInput">
                            <label class="control-label">状态:</label>
                            <div class="input-group ">
                                <select  id="effictive" class="form-control">
                                    <option value="">全部</option>
                                    <option value="0" selected>未失效</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-default" onclick="whitelist.detail.initUserList()">查询</button>
                            <button type="button" class="btn btn-default" onclick=" whitelist.detail.reset()">重置</button>
                        </div>

                    </form>

                <div id="listData" style="margin-top: 15px">
            </div>
        </div>
    </div>

    </div>
    <jsp:include page="../../../widget/partnerAndUser.jsp"></jsp:include>
</div>

<div id="chooseDateDiv" hidden style="height: 100%;overflow: hidden;padding: 20px;">
    <div class="row" style="margin: 0;padding-top: 50px">
        <div class="input-group">
            <span class="input-group-addon">请选择生效日期</span>
            <input type="text" class="form-control date" placeholder="YYYY-MM-DD" readonly id="effectiveDate" onclick="$.jeDate('#effectiveDate', { insTrigger: false, isTime: false, format: 'YYYY-MM-DD',zIndex:999999999 });">
        </div>
    </div>
    <div class="modal-footer" style="text-align: center;padding-top: 50px">
        <button type="button" class="btn btn-primary" onclick="whitelist.detail.effectiveDate()">确认
        </button>
        <button type="button" class="btn btn-default closeDiv" data-dismiss="modal">取消
        </button>

    </div>

</div>

<div id="cancelDateDiv" hidden style="height: 100%;overflow: hidden;padding: 20px;">
    <form>
    <div class="row" style="margin: 0;padding-top: 50px">
        <div class="input-group">
            <span class="input-group-addon">请选择失效日期</span>
            <input type="text" class="form-control date" placeholder="YYYY-MM-DD" readonly id="invalidDate" onclick="$.jeDate('#invalidDate', { insTrigger: false, isTime: false, format: 'YYYY-MM-DD',zIndex:999999999 });">
        </div>
    </div>
    <div class="modal-footer" style="text-align: center;padding-top: 50px">
        <button type="button" class="btn btn-primary" onclick="whitelist.detail.invalidDate()">确认
        </button>
        <button type="button" class="btn btn-default closeDiv" data-dismiss="modal">取消
        </button>
    </div>
    </form>
</div>

<div class="import_window" hidden>
    <div class="form-group">
        <label class="col-sm-3 control-label">excel文件</label>
        <div class="col-sm-4 input-group">
            <input type="file" name="excelFile" id="userImportFile" class="file-loading">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6" style="text-align: right;padding-right:0px;">
            <button type="submit" class="btn btn-primary" style="width: 100px;" onclick="whitelist.detail.import()">导入</button>
        </div>
    </div>
</div>

<input type="hidden" id="id" value="${whitelistDto.id}">
<input type="hidden" id="parentIds" value="${parentIds}">

<script type="text/javascript" src="webpage/flight/agreement/whitelist/whitelist.js?version=${globalVersion}"></script>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>

<script>
    $(function(){
        whitelist.detail.init();
    })


</script>