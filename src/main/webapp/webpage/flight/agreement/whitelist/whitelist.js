var whitelist = {
    url : {
        list: function () {
            return "/flight/whitelist/list";
        },
        delete :function (id) {
            return '/flight/whitelist/delete?id='+id
        },
        userList : function () {
            return "/flight/whitelist/userList";
        },
        deleteUsers:function (ids) {
            return "/flight/whitelist/deleteUsers?ids="+ids;
        },
        batchUpdateUsers:function () {
            return "/flight/whitelist/batchUpdateUsers";
        }

    },

    list : {
        init:function () {
            whitelist.list.readlist();
        },
      readlist:function (pageIndex) {
          $.post(whitelist.url.list(), {
              pageIndex: pageIndex,
              agreementPolicyCode:$.trim($("#agreementPolicyCode").val()),
              carrierCode:$.trim($("#carrierCode").val()),
          }, function (data) {
              $("#listData").html(data);
          });

      },
        reset: function () {
            $("#agreementPolicyCode").val("");
            $("#carrierCode").val("");
            whitelist.list.readlist();
        },
        delete: function (id) {
            whitelist.confirm(whitelist.url.delete(id), whitelist.list.readlist);
        },

    },
    confirm: function (url, callback) {
        layer.confirm('确认删除所选记录?', function (index) {
            layer.close(index);
            var loadIndex = layer.load();
            $.ajax({
                type: "get",
                url: url,
                success: function (result) {
                    layer.close(loadIndex);
                    if (result.code == '0') {
                        callback();
                    } else {
                        layer.alert(result.data, {icon: 2})
                    }
                },
                error: function () {
                    layer.msg("系统错误，请稍后刷新重试！", {icon: 2, time: 2000});
                    layer.close(loadIndex);
                }
            });
        });
    },
    detail : {
        init:function () {
            $("#pname").click(function () {
                TR.select('partner', {type: 0}, function (data) {
                    $("#pid").val(data.id);
                    $("#pname").val(data.name);
                });
            });

            $('#whiteUserTab li a').click(function(){
                whitelist.detail.initUserList(1,$(this).attr("data-value"));
                if($(this).attr("data-value") == 1){
                    $("#effictiveInput").css("display","none");
                } else {
                    $("#effictiveInput").css("display","");
                }
            });

            $(".closeDiv").click(function () {
                $(".layui-layer-close").trigger("click");
            });

            whitelist.detail.initUserList();
        },
        initUserList : function (pageIndex,status) {

            $.post(whitelist.url.userList(), {
                status: status || $("#whiteUserTab li.active a").attr("data-value"),
                pageIndex: pageIndex,
                partnerId:$("#pid").val(),
                userType:$("#userType").val(),
                nameOrNo:$("#nameOrNo").val(),
                effective:$("#effictive").val(),
                whitelistId:$("#id").val(),
            }, function (data) {
                $("#listData").html(data);

                $("#checkAll").change(function () {
                    if($(this).is(":checked")) {
                        $("input[name='userId']").prop("checked",true);
                    } else {
                        $("input[name='userId']").prop("checked",false);
                    }
                });
                $("#chooseUser").click(function(){
                    TR.partnerAndUser.show({
                        partnerIds: $("#parentIds").val(),
                        selected: {
                            partner: [],
                            org: [],
                            user: []
                        },
                    },function (data) {
                        whitelist.detail.addChooseUser(data);

                    });
                });


            });
        },
        reset:function () {
            $("#pid").val(""),
            $("#pname").val(""),
            $("#userType").val(""),
            $("#nameOrNo").val(""),
            $("#effictive").val(""),
            whitelist.detail.initUserList();
        },
        deleteUsers:function () {

            var ids = [];
            $("input[name='userId']").each(function() {
                if ($(this).is(":checked")) {
                    ids.push($(this).val());
                }
            });
            if(ids.length == 0) {
                layer.msg("请至少选择一个！", {icon: 2, time: 2000});
                return false;
            }
            whitelist.confirm(whitelist.url.deleteUsers(ids.join(",")), whitelist.detail.initUserList);
        },
        chooseDate:function (type) {
            window.scrollTo(0,0);
            if(type=='1') {
                layer.open({
                    title: '',
                    type: 1,
                    skin: 'demo-class',
                    area: ['460px', "260px"],
                    content: $('#chooseDateDiv')
                })
            } else {
                layer.open({
                    title: '',
                    type: 1,
                    skin: 'demo-class',
                    area: ['460px', "260px"],
                    content: $('#cancelDateDiv')
                })
            }
        },
        effectiveDate:function () {

            var effectiveDate= $("#effectiveDate").val();
            console.log(invalidDate);
            if (!effectiveDate) {
                layer.msg("请选择失效日期", {icon: 5, time: 1000})
                return false;
            }
            layer.confirm('确认设置该生效日期吗?', function (index) {
                layer.close(index);
                var loadIndex = layer.load();
                $.ajax({
                    type: "get",
                    url: whitelist.url.batchUpdateUsers()+"?type=1&whitelistId="+$("#id").val()+"&date="+effectiveDate,
                    success: function (result) {
                        layer.close(loadIndex);
                        if (result.code == '0') {
                            $(".layui-layer-close").trigger("click");
                            whitelist.detail.initUserList();
                        } else {
                            layer.alert(result.data, {icon: 2})
                        }
                    },
                    error: function () {
                        layer.msg("系统错误，请稍后刷新重试！", {icon: 2, time: 2000});
                        layer.close(loadIndex);
                    }
                });
            });

        },
        invalidDate:function () {

            var invalidDate = $("#invalidDate").val();
            console.log(invalidDate);
            if (!invalidDate) {
                layer.msg("请选择失效日期", {icon: 5, time: 1000})
                return false;
            }
            layer.confirm('确认设置该失效日期吗?', function (index) {
                layer.close(index);
                var loadIndex = layer.load();
                $.ajax({
                    type: "get",
                    url: whitelist.url.batchUpdateUsers()+"?type=2&whitelistId="+$("#id").val()+"&date="+invalidDate,
                    success: function (result) {
                        layer.close(loadIndex);
                        if (result.code == '0') {
                            $(".layui-layer-close").trigger("click");
                            whitelist.detail.initUserList();
                        } else {
                            layer.alert(result.data, {icon: 2})
                        }
                    },
                    error: function () {
                        layer.msg("系统错误，请稍后刷新重试！", {icon: 2, time: 2000});
                        layer.close(loadIndex);
                    }
                });
            });
        },
        reloadExportUser :function () {

            var status = $("#whiteUserTab li.active a").attr("data-value");
            var partnerId = $("#pid").val();
            var userType=$("#userType").val();
            var nameOrNo=$("#nameOrNo").val();
            var effictive=$("#effictive").val();
            var whitelistId=$("#id").val();

            window.location.href='/flight/whitelist/exportUser?status='+status+'&partnerId='+partnerId+'&userType='+userType
            +'&nameOrNo='+nameOrNo+'&effictive='+effictive+'&whitelistId='+whitelistId;

        },
        importUser :function () {
            $("#userImportFile").val("");
            layer.open({
                title: '导入员工',
                type: 1,
                area: ['500px','200px'],
                skin: 'layui-layer-rim', //加上边框
                content: $('.import_window'),

            });

        },
        import:function () {
            var fileObj = document.getElementById("userImportFile").files[0]; // js 获取文件对象
            if (typeof (fileObj) == "undefined" || fileObj.size <= 0) {
                alert("请选择文件");
                return;
            }
            var formFile = new FormData();
            formFile.append("file", fileObj);
            formFile.append("whitelistId", $("#id").val());

            var data = formFile;

            $.ajax({
                url : '/flight/whitelist/importUser',
                data: data,
                type: "Post",
                dataType: "json",
                cache: false,//上传文件无需缓存
                processData: false,//用于对data参数进行序列化处理 这里必须false
                contentType: false, //必须
                success : function(data) {
                    $(".layui-layer-close").trigger("click");
                    if (data.code == '0') {
                        layer.alert(data.data);
                    } else {
                        layer.alert("上传失败，请检查文件是否符合格式要求。");
                    }

                    whitelist.detail.initUserList();
                },
                error : function(data) {
                    $(".layui-layer-close").trigger("click");
                    layer.alert("上传失败，请检查文件是否符合格式要求。");
                }
            });
        },
        addChooseUser:function (data) {
            var partnerIds = [];
            var orgIds = [];
            var userIds = [];
            var whitelistId = $("#id").val();
            for(var i = 0;i< data.partner.length;i++) {
                partnerIds.push(data.partner[i].id);
            }
            for(var i = 0;i< data.org.length;i++) {
                orgIds.push(data.org[i].id);
            }
            for(var i = 0;i< data.user.length;i++) {
                userIds.push(data.user[i].id);
            }

            layer.confirm('确认导入所选企业部门员工人员?', function (index) {
                layer.close(index);
                var loadIndex = layer.load();
                $.ajax({
                    type: "get",
                    url: "flight/whitelist/addChooseUser?whitelistId="+whitelistId+"&partnerIds="+partnerIds.join(",")+"&orgIds="+orgIds.join(",")+"&userIds="+userIds.join(","),
                    success: function (result) {
                        layer.close(loadIndex);
                        layer.alert(result.data, {icon: 1});
                        whitelist.detail.initUserList();
                    },
                    error: function () {
                        layer.close(loadIndex);
                        layer.msg("系统错误，请稍后刷新重试！", {icon: 2, time: 2000});
                    }
                });
            });
        }



    }
    




}