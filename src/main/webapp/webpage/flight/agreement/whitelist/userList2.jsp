<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<div class="col-md-12 animated fadeInRight">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-10">
                            <a type="button" class="btn btn-default" id="chooseUser">导入企业员工</a>
                            <a type="button" class="btn btn-default" onclick="whitelist.detail.importUser()">EXCEL导入非员工</a>
                            <a type="button" class="btn btn-default" onclick="whitelist.detail.deleteUsers()">删除所选人员</a> |

                            <a type="button" class="btn btn-default" onclick="whitelist.detail.reloadExportUser()">将当前筛选结果导出为EXCEL</a>
                            <a type="button" class="btn btn-default" onclick="whitelist.detail.chooseDate('1')" >确认已将所有人提交航司</a>
                        </div>
                        <div class="col-md-2" style="padding-top: 16px">
                            <a href="/flight/whitelist/downLoadTemplate">下载非员工导入模版</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th style="text-align: center"><label><input type="checkbox" id="checkAll"></label></th>
            <th><label>姓名</label></th>
            <th><label>英文姓/英文名</label></th>
            <th><label>身份证</label></th>
            <th><label>其他证件1</label></th>
            <th><label>其他证件2</label></th>
            <th><label>所属企业</label></th>
            <th><label>类型</label></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${pageList.list }" var="user">
            <tr class="odd gradeX">
                <td style="text-align: center"><input type="checkbox" name="userId" value="${user.id}"></td>
                <td>${user.fullName }</td>
                <td>${user.nameEn }</td>
                <td>${user.idNo }</td>
                <td>${user.certificateno1 }</td>
                <td>${user.certificateno2 }</td>
                <td>${user.partnerName }</td>
                <td>${user.userType eq '1'?'员工':'非员工'}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="pagin">
        <jsp:include page="../../../common/pagination_ajax.jsp?callback=whitelist.detail.initUserList"></jsp:include>
    </div>

</div>




