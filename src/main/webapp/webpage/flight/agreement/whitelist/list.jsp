<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="text-align: center"><label>原始协议大客户编码</label></th>
        <th><label>原始协议名称</label></th>
        <th><label>原始协议适用航司</label></th>
        <th><label>名单人数</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="whitelist">
        <tr class="odd gradeX">
            <td style="text-align: center">${whitelist.customerAgreementCode }</td>
            <td>${whitelist.originPolicyName }</td>
            <td style="word-break:break-all" width="200px">${whitelist.originCarrierCodes }</td>
            <td>${whitelist.userCount }</td>

            <td class="text-center">
                <a href="flight/whitelist/detail?id=${whitelist.id}">维护人数</a>&nbsp;&nbsp;
                <a onclick="whitelist.list.delete('${whitelist.id }');">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=whitelist.list.readlist"></jsp:include>
</div>