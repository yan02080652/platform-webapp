<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="text-align: center"><label>序号</label></th>
        <th><label>企业名称</label></th>
        <th><label>适用销售日期</label></th>
        <th><label>展示标签</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="scope" varStatus="vs">
        <tr class="odd gradeX">
            <td style="text-align: center">${vs.index + 1}</td>
            <td>${scope.partnerName}</td>
            <td>${scope.effectiveSaleDay}</td>
            <td>${scope.showName}</td>
            <td>
                <a onclick="agreementScope.scopeDetail.detail('${scope.id}')">修改</a>&nbsp;&nbsp;
                <a onclick="agreementScope.list.delete('${scope.id }');">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=agreementScope.list.reloadList"></jsp:include>
</div>