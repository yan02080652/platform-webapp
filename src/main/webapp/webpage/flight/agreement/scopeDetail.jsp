<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .popover.right > .arrow {
        top: 50% !important;
    }

    .control-label .reqMark {
        color: red;
        font-weight: bold;
        font-size: 15px;
    }

    #freightForm .btn-success {
        background-color: #46A2BB;
        border-color: #46A2BB;
    }
</style>

<div class="modal fade" id="partnerScopeModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">
                    <c:choose>
                        <c:when test="${not empty scope.id}">
                            修改共享企业
                        </c:when>
                        <c:otherwise>
                            新增共享企业
                        </c:otherwise>
                    </c:choose>

                </h4>
            </div>
            <div class="modal-body">
                <form id="scopeForm" class="form-horizontal">
                    <input type="hidden" name="agreementPolicyId" value="${policyId}">
                    <input type="hidden" name="id" id="id" value="${scope.id}">
                    <input type="hidden" name="partnerId" id="partnerId" value="${scope.partnerId}">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">企业</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control tmc"  style="width: 300px" id="partnerName" name="partnerName" value="${scope.partnerName}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">适用销售日期</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" style="width: 300px" name="effectiveSaleDay" value="${scope.effectiveSaleDay}" placeholder="适用销售日期(示例:20180214-20180222)" maxlength="500">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">展示标签</label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control"  style="width: 300px" name="showName" value="${scope.showName}" >
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="agreementScope.scopeDetail.saveForm()">保存
                </button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="webpage/flight/agreement/agreement.js?version=${globalVersion}"></script>

<script>
    $(function () {
        agreementScope.scopeDetail.init();
    })
</script>