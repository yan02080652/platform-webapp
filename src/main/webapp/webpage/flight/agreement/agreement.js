var agreement = {
    url: {
        list: function () {
            return 'flight/agreement/list';
        },
        delete: function (id) {
            return 'flight/agreement/delete?id=' + id;
        },
        save: function () {
            return 'flight/agreement/save';
        },
        detail: function (id,operateType) {
            if (id) {
                return 'flight/agreement/detail?id=' + id + '&operateType='+operateType;
            } else {
                return 'flight/agreement/detail?operateType='+operateType;
            }
        },
        agreementCabinDetail: function (policyId, id) {
            var url = "flight/agreement/agreementCabinDetail?policyId=" + policyId;
            if (id) {
                url += "&id=" + id;
            }
            return url;
        },
        deleteAgreementCabin: function (id) {
            return 'flight/agreement/deleteAgreementCabin?id=' + id;
        },
        saveAgreementCabin: function () {
            return 'flight/agreement/saveAgreementCabin';
        },

    },
    confirm: function (url, callback) {
        layer.confirm('确认删除所选记录?', function (index) {
            layer.close(index);
            var loadIndex = layer.load();
            $.ajax({
                type: "get",
                url: url,
                success: function (result) {
                    layer.close(loadIndex);
                    if (result.code == '0') {
                        callback();
                    } else {
                        layer.alert(result.data, {icon: 2})
                    }
                },
                error: function () {
                    layer.msg("系统错误，请稍后刷新重试！", {icon: 2, time: 2000});
                    layer.close(loadIndex);
                }
            });
        });
    },
    showHelpTip: function (className, dom) {
        $(document).on('mouseover', className, function () {
            var that = this;
            var index = layer.tips(dom, that, {
                tips: [2, '#1AA094'],
                time: 0,
                area: '500px'
            });
            $(that).on('mouseleave', function () {
                layer.close(index);
            })
        });
    },
    tipData: [
        {className: ".effective-date", dom: $(".effectiveDate").html()},
        {className: ".effective-airline", dom: $(".effectiveAirline").html()},
        {className: ".effective-flightno", dom: $(".effectiveFlightno").html()},
    ],

    //===============================列表页========================================
    list: {
        init: function () {
            //企业选择器
            $("#pname").click(function () {
                TR.select('partner', {type: 0}, function (data) {
                    $("#pid").val(data.id);
                    $("#pname").val(data.name);
                    agreement.list.reloadList();
                });
            });
            agreement.list.reloadList();
        },
        reloadList: function (pageIndex) {
            var partnerId = $.trim($("#pid").val());
            $.post(agreement.url.list(), {
                pageIndex: pageIndex,
                partnerId: partnerId,
                agreementPolicyCode:$.trim($("#agreementPolicyCode").val()),
                carrierCode:$.trim($("#carrierCode").val()),
                type:$("#type").val(),
                name:$.trim($("#name").val()),
                status:$("#status").val(),
            }, function (data) {
                $("#listData").html(data);
            });
        },
        reset: function () {
            $("#pid").val("");
            $("#pname").val("");
            $("#agreementPolicyCode").val("");
            $("#carrierCode").val("");
            $("#type").val("");
            $("#name").val("");
            $("#status").val("");
            agreement.list.reloadList();
        },
        delete: function (id) {
            agreement.confirm(agreement.url.delete(id), agreement.list.reloadList);
        },
    },
    //==============================详情页=========================================
    detail: {
        //保存已选清单
        carrierData: [],
        chooseCarrier: function () {
            new choose.carrierData({
                //给id可以防止一直新建对象
                id: 'multiCarrierData',
                //多选
                multi: true,
                title: "",
                height: 400,
                tableHeight: 277,
                getChoosedData: function () {
                    return agreement.detail.carrierData;
                }
            }, agreement.detail.chooseCallback);
        },
        chooseCallback: function (result) {
            agreement.detail.carrierData = [];
            if (result && result.length > 0) {
                var codes = "";
                $.each(result, function (i, data) {
                    if (data.code) {
                        var carrier = {code: data.code};
                        agreement.detail.carrierData.push(carrier);
                        codes += data.code + "/";
                    }
                })
                var showCodeStr = codes.substring(0, codes.length - 1);
                $("input[name='carrierCodes']").val(showCodeStr);
                $("#codesDiv").text("已选:" + showCodeStr).show();
            } else {
                $("input[name='carrierCodes']").val("");
                $("#codesDiv").text("").show();
            }
        },
        init: function () {
            layui.use(['form', 'layedit', 'laydate'], function(){
                form = layui.form;
                //监听指定开关
                form.on('switch(switchTest)', function(data){
                    layer.msg('开关checked：'+ (this.checked ? 'true' : 'false'), {
                        offset: '6px'
                    });
                    layer.tips('是否开启白名单', data.othis)
                });

                form.on('checkbox(selectSeat)', function (data) {

                    if (data.elem.checked) {
                        $("#pnrInput").css("display","block");
                    } else {
                        $("#pnrInput").css("display","none");
                    }
                    form.render('checkbox'); //刷新checkbox渲染
                });
                if($("input[name='payAfterSeat']").is(":checked")) {
                    $("#pnrInput").css("display","block");
                } else {
                    $("#pnrInput").css("display","none");
                }

                if($("#operateType").val() == '1') {
                    //新增协议规则时输入框默认为大客户协议价，用户可自行编辑
                    $("#showName").val("大客户协议价");
                    $("input[name='type']").prop("disabled",true);

                }
                if($("#operateType").val() == '2') {
                    //协议类型不可修改
                    $("input[name='type']").prop("disabled",true);
                    //共享协议
                    if($("input[name='type']:checked").val() == '1'){

                        $("#partnerName").prop("disabled",true);
                        $("#showName").prop("disabled",true);
                        $("input[name='effectiveSaleDay']").prop("disabled",true);

                        $("#partnerInput").css("display","none");
                        $("#effectiveSaleDayInput").css("display","none");
                        $("#showNameInput").css("display","none");
                        $("input[name='productType']").prop("disabled",true);
                        $("input[name='customerAgreementCode']").prop("readonly",true);
                        $("#chooseCarrier").removeAttr("onclick");
                        $("input[name='whiteListControl']").prop("disabled",true);
                        $("textarea[name='effectiveTravelDay']").prop("readonly",true);
                        $("textarea[name='effectiveAirline']").prop("readonly",true);
                        $("textarea[name='invalidAirline']").prop("readonly",true);
                        
                        $("select[name='filterType']").prop("disabled",true);

                        $("input[name='flightNo']").prop("readonly",true);
                        $("input[name='saleQuota']").prop("readonly",true);

                        $("select[name='hubsConnectId']").prop("disabled",true);

                        $("input[name='checkPrice']").prop("disabled",true);
                        $("input[name='payAfterSeat']").prop("disabled",true);
                        $("input[name='autoIssue']").prop("disabled",true);
                        $("input[name='pnr']").prop("readonly",true);

                        form.render('select');
                    }

                }
            });


            if($("#operateType").val() == '3') {
                //复制逻辑添加共享协议
                layui.use('form', function () {
                    var form = layui.form;
                    form.on('radio(type)', function(data){
                        if(data.value == '1') {
                            $("#partnerInput").css("display","none");
                            $("#effectiveSaleDayInput").css("display","none");
                            $("#showNameInput").css("display","none");

                            $("#partnerName").prop("disabled",true);
                            $("#showName").prop("disabled",true);
                            $("input[name='effectiveSaleDay']").prop("disabled",true);

                            $("input[name='productType']").prop("disabled",true);
                            $("input[name='customerAgreementCode']").prop("readonly",true);
                            $("#chooseCarrier").removeAttr("onclick");
                            $("input[name='whiteListControl']").prop("disabled",true);
                            $("textarea[name='effectiveTravelDay']").prop("readonly",true);
                            $("textarea[name='effectiveAirline']").prop("readonly",true);
                            $("textarea[name='invalidAirline']").prop("readonly",true);
                            $("select[name='filterType']").prop("disabled",true);
                            $("input[name='flightNo']").prop("readonly",true);
                            $("input[name='saleQuota']").prop("readonly",true);
                            $("select[name='hubsConnectId']").prop("disabled",true);
                            $("input[name='checkPrice']").prop("disabled",true);
                            $("input[name='payAfterSeat']").prop("disabled",true);
                            $("input[name='autoIssue']").prop("disabled",true);
                            $("input[name='pnr']").prop("readonly",true);

                            var pro = $("#productType").val();
                            $("input[value='"+pro+"']").prop("checked",true);

                            $("input[name='carrierCodes']").val($("#carrierCodes").val());
                            agreement.detail.carrierData = [];
                            var codeArr = $("input[name='carrierCodes']").val().split("/");
                            $.each(codeArr, function (i, code) {
                                if (code) {
                                    var carrier = {code: code};
                                    agreement.detail.carrierData.push(carrier);
                                }
                            })

                            if (agreement.detail.carrierData.length > 0) {
                                var codes = "";
                                $.each(agreement.detail.carrierData, function (i, data) {
                                    if (data) {
                                        var carrier = {code: data};
                                        codes += data.code + "/";
                                    }
                                })
                                var showCodeStr = codes.substring(0, codes.length - 1);
                                $("input[name='carrierCodes']").val(showCodeStr);
                                $("#codesDiv").text("已选:" + showCodeStr).show();
                            } else {
                                $("input[name='carrierCodes']").val("");
                                $("#codesDiv").text("").show();
                            }

                            if($("#whiteListControl").val() == 'true') {
                                $("input[name='whiteListControl']").prop("checked",true);
                            } else {
                                $("input[name='whiteListControl']").prop("checked",false);
                            }
                            $("#whiteListId").val($("#whiteListIds").val());

                            $("input[name='customerAgreementCode']").val($("#customerAgreementCode").val());
                            $("textarea[name='effectiveTravelDay']").val($("#effectiveTravelDay").val());
                            $("textarea[name='effectiveAirline']").val($("#effectiveAirline").val());
                            $("textarea[name='invalidAirline']").val($("#invalidAirline").val());
                            $("input[name='flightNo']").val($("#flightNo").val());
                            $("input[name='saleQuota']").val($("#saleQuota").val());
                            $("select[name='hubsConnectId']").val($("#hubsConnectId").val());
                            $("select[name='filterType']").val($("#filterType").val());
                            if($("#checkPrice").val() == 'true') {
                                $("input[name='checkPrice']").prop("checked",true);
                            } else {
                                $("input[name='checkPrice']").prop("checked",false);
                            }
                            if($("#payAfterSeat").val() == 'true') {
                                $("input[name='payAfterSeat']").prop("checked",true);
                            } else {
                                $("input[name='payAfterSeat']").prop("checked",false);
                            }
                            if($("#autoIssue").val() == 'true') {
                                $("input[name='autoIssue']").prop("checked",true);
                            } else {
                                $("input[name='autoIssue']").prop("checked",false);
                            }

                            $("input[name='pnr']").val($("#pnr").val());

                            if($("input[name='payAfterSeat']").is(":checked")) {
                                $("#pnrInput").css("display","block");
                            } else {
                                $("#pnrInput").css("display","none");
                            }

                            form.render('select');
                            form.render('radio');
                            form.render('checkbox');

                        } else if(data.value== '0'){
                            
                            $("#partnerInput").css("display","block");
                            $("#effectiveSaleDayInput").css("display","block");
                            $("#showNameInput").css("display","block");

                            $("#partnerName").prop("disabled",false);
                            $("#showName").prop("disabled",false);
                            $("input[name='effectiveSaleDay']").prop("disabled",false);

                            $("input[name='customerAgreementCode']").prop("readonly",false);
                            $("input[name='productType']").prop("disabled",false);
                            $("#chooseCarrier").attr("onclick","agreement.detail.chooseCarrier()");
                            $("input[name='whiteListControl']").prop("disabled",false);
                            $("textarea[name='effectiveTravelDay']").prop("readonly",false);
                            $("textarea[name='effectiveAirline']").prop("readonly",false);
                            $("textarea[name='invalidAirline']").prop("readonly",false);
                            $("select[name='filterType']").prop("disabled",false);
                            $("input[name='flightNo']").prop("readonly",false);
                            $("input[name='saleQuota']").prop("readonly",false);
                            $("select[name='hubsConnectId']").prop("disabled",false);
                            $("input[name='checkPrice']").prop("disabled",false);
                            $("input[name='payAfterSeat']").prop("disabled",false);
                            $("input[name='autoIssue']").prop("disabled",false);
                            $("input[name='pnr']").prop("readonly",false);

                            if($("input[name='payAfterSeat']").is(":checked")) {
                                $("#pnrInput").css("display","block");
                            } else {
                                $("#pnrInput").css("display","none");
                            }

                            $("#whiteListId").val("");
                            $("input[name='whiteListControl']").prop("checked",false);

                            form.render('select');
                            form.render('checkbox');
                            form.render('radio');
                        }
                    });
                });
                
                if($("input[name='type']:checked").val() == '1') {

                    $("#partnerName").prop("disabled",true);
                    $("#showName").prop("disabled",true);
                    $("input[name='effectiveSaleDay']").prop("disabled",true);

                    $("#partnerInput").css("display","none");
                    $("#effectiveSaleDayInput").css("display","none");
                    $("#showNameInput").css("display","none");
                    $("input[name='productType']").prop("disabled",true);
                    $("input[name='customerAgreementCode']").prop("readonly",true);
                    $("#chooseCarrier").removeAttr("onclick");
                    $("input[name='whiteListControl']").prop("disabled",true);
                    $("textarea[name='effectiveTravelDay']").prop("readonly",true);
                    $("textarea[name='effectiveAirline']").prop("readonly",true);
                    $("textarea[name='invalidAirline']").prop("readonly",true);
                    $("select[name='filterType']").prop("disabled",true);
                    $("input[name='flightNo']").prop("readonly",true);
                    $("input[name='saleQuota']").prop("readonly",true);
                    $("select[name='hubsConnectId']").prop("readonly",true);
                    $("input[name='checkPrice']").prop("disabled",true);
                    $("input[name='payAfterSeat']").prop("disabled",true);
                    $("input[name='autoIssue']").prop("disabled",true);
                    $("input[name='pnr']").prop("readonly",true);

                    if($("input[name='payAfterSeat']").is(":checked")) {
                        $("#pnrInput").css("display","block");
                    } else {
                        $("#pnrInput").css("display","none");
                    }

                    form.render('select');
                    form.render('checkbox');
                } else {

                    $("#whiteListId").val("");
                    $("input[name='whiteListControl']").prop("checked",false);

                }
            }


            var codeArr = $("input[name='carrierCodes']").val().split("/");
            $.each(codeArr, function (i, code) {
                if (code) {
                    var carrier = {code: code};
                    agreement.detail.carrierData.push(carrier);
                }
            })

            $(".partnerChoose").click(function () {
                TR.select('partner', {type: 0}, function (data) {
                    $("#partnerScope").val(data.id);
                    $("#partnerName").val(data.name);
                });
            });

            agreement.tipData.forEach(function (data) {
                agreement.showHelpTip(data.className, data.dom);
            })
        },

        validate: function (formData) {
            var type = $("input[name='type']:checked").val();
            if (!formData.name) {
                layer.msg("协议名称必填", {icon: 5, time: 1000})
                return false;
            }
            if (!formData.partnerScope && type == '0') {
                layer.msg("适用企业必填", {icon: 5, time: 1000})
                return false;
            }
            if (!formData.customerAgreementCode) {
                layer.msg("大客户编码必填", {icon: 5, time: 1000})
                return false;
            }
            if (!formData.carrierCodes) {
                layer.msg("适用航司必填", {icon: 5, time: 1000})
                return false;
            }
            var effectiveSaleDay = formData.effectiveSaleDay;
            if (!effectiveSaleDay  && type == '0') {
                layer.msg("适用销售日期必填", {icon: 5, time: 1000})
                return false;
            }
            var showName = formData.showName;
            if (!showName  && type == '0') {
                layer.msg("展示标签日期必填", {icon: 5, time: 1000})
                return false;
            }
            //格式：8位数字加'-'加8位数字 可以有多个，用','号分割(20180101-20180202,20180303-20180404)
            var reg = /^(\d{8}-\d{8})(\,\d{8}-\d{8})*$/;
            if (!reg.test(effectiveSaleDay)  && type == '0') {
                layer.msg("适用销售日期格式有误,请更正！", {icon: 5, time: 1000})
                return false;
            }
            if(formData.showName && type == '0') {
                var length = showName.replace(/[^\x00-\xff]/g, "01").length
                if (length > 14) {
                    layer.msg("展示标签不得超过14个字符！", {icon: 5, time: 1000})
                    return false;
                }
            }
            if (formData.effectiveTravelDay && !reg.test(formData.effectiveTravelDay)) {
                layer.msg("适用旅行日期格式有误,请更正！", {icon: 5, time: 1000})
                return false;
            }
            //格式：一个或多个3位字母,用/分割,加'-'加个或多个3位字母 可以有多个，用','号分割
            reg = /^[a-zA-Z]{3}(\/[a-zA-Z]{3})*-[a-zA-Z]{3}(\/[a-zA-Z]{3})*(\,[a-zA-Z]{3}(\/[a-zA-Z]{3})*-[a-zA-Z]{3}(\/[a-zA-Z]{3})*)*$/;
            if (formData.effectiveAirline && !reg.test(formData.effectiveAirline)) {
                layer.msg("适用航线格式有误,请更正！", {icon: 5, time: 1000})
                return false;
            }
            if (formData.invalidAirline && !reg.test(formData.invalidAirline)) {
                layer.msg("不适用航线格式有误,请更正！", {icon: 5, time: 1000})
                return false;
            }
            //格式：数字 加'-'加 数字 可以有多个，用','号分割
            reg = /^(\d+-\d+)(\,\d+-\d+)*$/;
            if (formData.flightNo && !reg.test(formData.flightNo)) {
                layer.msg("航班号格式有误,请更正！", {icon: 5, time: 1000})
                return false;
            }
            if (formData.saleQuota) {
                if( formData.saleQuota < 0){
                    layer.msg("航班销售额度必须为正整数", {icon: 5, time: 1000})
                    return false;
                }
                if( formData.saleQuota >500){
                    layer.msg("航班销售额度过大,请输入一个合理值", {icon: 5, time: 1000})
                    return false;
                }
            }
            if (formData.pnr && formData.pnr.length > 50) {
                layer.msg("pnr最长不能超过50！请确认后提交", {icon: 5, time: 1000})
                return false;
            }
            return true;
        },
        saveForm: function () {

            var formData = $("#agreementForm").serializeJSON();
            formData['type'] = $("input[name='type']:checked").val();
            formData['productType'] = $("input[name='productType']:checked").val();
            formData['whiteListControl'] = $("input[name='whiteListControl']").is(":checked");
            formData['filterType'] = $("select[name='filterType'] option:selected").val();
            formData['hubsConnectId'] = $("select[name='hubsConnectId'] option:selected").val();
            formData['checkPrice'] = $("input[name='checkPrice']").is(":checked");
            formData['payAfterSeat'] = $("input[name='payAfterSeat']").is(":checked");
            formData['autoIssue'] = $("input[name='autoIssue']").is(":checked");
            var validate = agreement.detail.validate(formData);
            if (validate) {

                if($("input[name='whiteListControl']").is(":checked") && $("#whiteListId").val() == '') {
                    var code = $("input[name='customerAgreementCode']").val();
                    $.ajax({
                        type: 'get',
                        url: 'flight/agreement/checkWhitelist?code='+code,
                        dataType: 'json',
                        async:false,
                        contentType: "application/json",
                        success: function (data) {

                            if(data.code != '0') {
                                layer.confirm(data.data, {
                                    'title': '',
                                    closeBtn: 0,
                                    btn: ['创建', '取消'],
                                }, function (index) {
                                    $.post("flight/agreement/createWhitelist",{'code':code},function(data){

                                        $("#whiteListId").val(data.data.id);

                                        formData = $("#agreementForm").serializeJSON();
                                        formData['type'] = $("input[name='type']:checked").val();
                                        formData['productType'] = $("input[name='productType']:checked").val();
                                        formData['whiteListControl'] = $("input[name='whiteListControl']").is(":checked");
                                        formData['filterType'] = $("select[name='filterType'] option:selected").val();
                                        formData['hubsConnectId'] = $("select[name='hubsConnectId'] option:selected").val();
                                        formData['checkPrice'] = $("input[name='checkPrice']").is(":checked");
                                        formData['payAfterSeat'] = $("input[name='payAfterSeat']").is(":checked");
                                        formData['autoIssue'] = $("input[name='autoIssue']").is(":checked");

                                        var layerIndex= layer.load();
                                        $.ajax({
                                            url: agreement.url.save(),
                                            type: "post",
                                            data: formData,
                                            success: function (result) {
                                                if (result.code == '0') {
                                                    window.location.href = agreement.url.detail(result.data.id,'2');
                                                } else {
                                                    layer.close(layerIndex);
                                                    layer.alert(result.data, {icon: 2})
                                                }
                                            }
                                        })
                                    });

                                }, function () {
                                    //刷新数据
                                    return;
                                });
                            } else {
                                var layerIndex= layer.load();
                                $.ajax({
                                    url: agreement.url.save(),
                                    type: "post",
                                    data: formData,
                                    success: function (result) {
                                        if (result.code == '0') {
                                            window.location.href = agreement.url.detail(result.data.id,'2');
                                        } else {
                                            layer.close(layerIndex);
                                            layer.alert(result.data, {icon: 2})
                                        }
                                    }
                                });
                            }


                        }

                    });
                } else {
                    var layerIndex= layer.load();
                    $.ajax({
                        url: agreement.url.save(),
                        type: "post",
                        data: formData,
                        success: function (result) {
                            if (result.code == '0') {
                                window.location.href = agreement.url.detail(result.data.id,'2');
                            } else {
                                layer.close(layerIndex);
                                layer.alert(result.data, {icon: 2})
                            }
                        }
                    });
                }

            }
        },

    },
    //=============================适用舱位========================================
    cabin: {
        detail: function (policyId, id) {
            layer.open({
                type: 2,
                area: ['900px', '600px'],
                title: "协议折扣维护",
                closeBtn: 1,
                content: agreement.url.agreementCabinDetail(policyId, id),
            });
        },
        delete: function (id) {
            agreement.confirm(agreement.url.deleteAgreementCabin(id), agreement.cabin.reload)
        },
        reload: function () {
            window.location.reload();
        },
        init: function () {

            if($("input[name='standardRefund']:checked").val() == "1") {
                $("input[name='refundRule']").attr("lay-verify","requiredByRefund");
                $("input[name='changeRule']").attr("lay-verify","requiredByRefund");
                $("#refundRuleInput").css("display","none");
                $("#endorseInput").css("display","none");
            } else {
                $("input[name='refundRule']").attr("lay-verify","requiredByRefund|numberAndUnderline|identical");
                $("input[name='changeRule']").attr("lay-verify","requiredByRefund|numberAndUnderline|identical");
                $("#refundRuleInput").css("display","block");
                $("#endorseInput").css("display","block");

            }

            layui.use('form', function () {
                form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功

                form.on('radio(refund)', function(data){

                    if(data.value == '1') {
                        $("input[name='refundRule']").attr("lay-verify","requiredByRefund");
                        $("input[name='changeRule']").attr("lay-verify","requiredByRefund");
                        $("#refundRuleInput").css("display","none");
                        $("#endorseInput").css("display","none");

                    } else {
                        $("input[name='refundRule']").attr("lay-verify","requiredByRefund|numberAndUnderline|identical");
                        $("input[name='changeRule']").attr("lay-verify","requiredByRefund|numberAndUnderline|identical");
                        $("#refundRuleInput").css("display","block");
                        $("#endorseInput").css("display","block");
                    }
                })

                //验证规则
                form.verify({
                    discount: [/^([1-9]\d{0,1}|100)$/, '折扣应为1-100间的整数'],
                    numberAndUnderline: [/^[0-9]+([-][0-9]+[-][0-9]+)+$/, '请填入正确格式:如 10-0-20'],
                    identical: function () {
                        var reg = /^[0-9]+([-][0-9]+[-][0-9]+)+$/;
                        var refundRule = $("input[name='refundRule']").val();
                        var changeRule = $("input[name='changeRule']").val();
                        if(reg.test(refundRule) && reg.test(changeRule)){
                            var rl = refundRule.split("-").length;
                            var cl = changeRule.split("-").length;
                            if (rl > 0 && cl > 0 && rl != cl) {
                                return "退票规则和改期规则格式必须一致";
                            }
                        }
                    },
                    checkedOne: function (value) {
                        if ($("input[name='cabinCodes']:checked").length == 0) {
                            return "请选择舱位";
                        }
                    }
                });

                form.on('checkbox(selectAll)', function (data) {
                    if (data.elem.checked) {
                        $("input[name='cabinCodes']").prop("checked", true);
                    } else {
                        $("input[name='cabinCodes']").prop("checked", false);
                    }
                    form.render('checkbox'); //刷新checkbox渲染
                });

                //监听提交
                form.on('submit(from)', function (data) {

                    var reg = /^(\d{8}-\d{8})(\,\d{8}-\d{8})*$/;
                    var invalidTravelDate = $("input[name='invalidTravelDate']").val().trim();
                    if (invalidTravelDate != '' && !reg.test(invalidTravelDate)) {
                        layer.msg("除外旅行日期格式有误,请更正！", {icon: 5, time: 1000})
                        return false;
                    }

                    var reg = /^[a-zA-Z]{3}(\/[a-zA-Z]{3})*-[a-zA-Z]{3}(\/[a-zA-Z]{3})*(\,[a-zA-Z]{3}(\/[a-zA-Z]{3})*-[a-zA-Z]{3}(\/[a-zA-Z]{3})*)*$/;
                    var effectiveAirline = $("input[name='effectiveAirline']").val().trim();
                    if (effectiveAirline != '' && !reg.test(effectiveAirline)) {
                        layer.msg("适用航线格式有误,请更正！", {icon: 5, time: 1000})
                        return false;
                    }
                    if (effectiveAirline.length > 2000) {
                        layer.msg("适用航线长度不允许超过2000,请更正！", {icon: 5, time: 1000})
                        return false;
                    }
                    var invalidAirline = $("input[name='invalidAirline']").val().trim();
                    if (invalidAirline != '' && !reg.test(invalidAirline)) {
                        layer.msg("除外航线格式有误,请更正！", {icon: 5, time: 1000})
                        return false;
                    }
                    if (invalidAirline.length > 2000) {
                        layer.msg("除外航线长度不允许超过2000,请更正！", {icon: 5, time: 1000})
                        return false;
                    }

                    $.ajax({
                        url: agreement.url.saveAgreementCabin(),
                        data: $("#agreementCabinForm").serialize(),
                        type: "post",
                        success: function (result) {
                            if (result.code == '0') {
                                parent.location.reload();
                                closeIframe();
                            } else {
                                layer.alert(result.data, {icon: 2})
                            }
                        }
                    })
                    return false;
                });
            });
            agreement.showHelpTip(".rule-tipFlight", $(".CabineffectiveAirline").html());
            agreement.showHelpTip(".rule-tipDate", $(".CabineffectiveDate").html());
            agreement.showHelpTip(".rule-tip", $(".help").html());
        },
    },

};

var agreementScope = {

    url:{
      list : function () {
          return 'flight/agreement/shareScopelist';
      },
      delete: function (id) {
        return 'flight/agreement/deleteScope?id=' + id;
      },
      detail: function (id,policyId) {
        return 'flight/agreement/scopeDetail?id='+id+'&policyId='+policyId;
      },
      save:function () {
          return 'flight/agreement/saveScope';
      }
    },

    list: {
        init: function () {
            //企业选择器
            $("#pName").click(function () {
                TR.select('partner', {type: 0}, function (data) {
                    $("#pId").val(data.id);
                    $("#pName").val(data.name);
                    //agreementScope.list.reloadList();
                });
            });
            agreementScope.list.reloadList();
        },
        reloadList: function (pageIndex) {
            $.post(agreementScope.url.list(), {
                pageIndex: pageIndex,
                partnerId: $("#pId").val(),
                policyId:$("#policyId").val(),
            }, function (data) {
                $("#shareScopelistData").html(data);
            });
        },
        reset: function () {
            $("#pId").val("");
            $("#pName").val("");
            agreementScope.list.reloadList();
        },
        delete: function (id) {
            agreement.confirm(agreementScope.url.delete(id), agreementScope.list.reloadList);
        },

    },
    scopeDetail : {
        init : function(){
            $("#partnerName").click(function () {
                TR.select('partner', {type: 0}, function (data) {
                    $("#partnerId").val(data.id);
                    $("#partnerName").val(data.name);
                });
            });

            if($("input[name='showName']").val() == '') {
                $("input[name='showName']").val("大客户协议价");
            }
        },
        showModal: function (url, callback) {
            $('#detailDiv').load(url, function (context, state) {
                if ('success' == state && callback) {
                    callback();
                }
            });
        },
        detail: function (id) {
            if(!id) {
                id = '';
            }
            agreementScope.scopeDetail.showModal(agreementScope.url.detail(id,$("#policyId").val()), function () {
                $('#partnerScopeModel').modal();
                agreementScope.scopeDetail.initForm();
            });
        },

        initForm: function () {
            var rules = {
                partnerName:{
                    required:true,
                },
                effectiveSaleDay:{
                    required:true,
                    maxlength:500,
                    effectiveSaleDay:true,
                },
                showName:{
                    required:true,
                    cnAnEnLengh:14,
                }

            };

            //添加必填标记
            $.each(rules, function (code, rule) {
                if (rule.required || rule.type) {
                    agreementScope.scopeDetail.addReqMark(code);
                }
            });

            $("#scopeForm").validate({
                rules: rules,
                errorPlacement: setErrorPlacement,
                success: validateSuccess,
                highlight: setHighlight,
                submitHandler: function (form) {

                    $.ajax({
                        type: "POST",
                        url: agreementScope.url.save(),
                        data: $(form).serialize(),
                        success: function (result) {

                            if (result.code == '0') {
                                $('#partnerScopeModel').modal('hide');
                                agreementScope.list.reloadList();
                            } else {
                                $.alert(result.msg);
                            }
                        },
                        error: function (request) {
                            $.alert("请求失败，请刷新重试！");
                        },
                    });
                }
            });
        },
        addReqMark: function (code, addRule) {
            var requiredMark = $("<span class='reqMark'>*</span>");
            var element = $("#scopeForm").find("[name=" + code + "]");
            var label = element.parentsUntil('.form-group').parent().children('.control-label');
            if ($(".reqMark", label).length == 0) {
                label.append(requiredMark);
            }
            if (addRule) {
                element.rules('add', addRule);
            }
        },

        saveForm:function () {
            $("#scopeForm").submit();
        }
    }
}

