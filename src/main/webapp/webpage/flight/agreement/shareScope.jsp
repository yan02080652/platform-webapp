<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div >
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span style="color: blue">协议列表 &nbsp;&nbsp;>&nbsp;&nbsp; ${policyDto.name}</span> &nbsp;&nbsp; > &nbsp;&nbsp;<span>管理共享企业</span>
            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <!-- 查询条件 -->
                                        <form class="form-inline" method="post">


                                            <div class="form-group">
                                                <label class="control-label">企业名称:</label>
                                                <div class="input-group ">
                                                    <input type="hidden" id="pId">
                                                    <input style="width: 130px" type="text" id="pName" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <button type="button" class="btn btn-default" onclick="agreementScope.list.reloadList()">查询</button>
                                                <button type="button" class="btn btn-default" onclick=" agreementScope.list.reset()">重置</button>
                                            </div>

                                            <div class="form-group" style="float: right">
                                                <button type="button" class="btn btn-default" onclick="agreementScope.scopeDetail.detail()">添加</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                <div id="shareScopelistData" style="margin-top: 15px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="policyId" value="${policyDto.id}">
<div id="detailDiv"></div>
<script type="text/javascript" src="webpage/flight/agreement/agreement.js?version=${globalVersion}"></script>

<script>
    $(function(){
        agreementScope.list.init();
    })
</script>