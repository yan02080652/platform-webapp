<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>
<div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>经停数据
            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div>
                                    <button type="button" class="btn btn-default pull-left" onclick="list.detail(null)">新增</button>
                                    <!-- 查询条件 -->
                                    <form class="form-inline" method="post" id="searchForm">
                                        <nav class="text-right">
                                            <div class=" form-group">
                                                <label class="control-label">航班号</label>
                                                <input name="flightNo" id="flightNo" type="text"
                                                       class="form-control"
                                                       placeholder="航班号" style="width: 100px"/>
                                            </div>
                                            <div class="form-group">
                                                <button type="button" class="btn btn-default" onclick="list.reloadList()">查询</button>
                                                <button type="button" class="btn btn-default" onclick="list.reset()">重置</button>
                                            </div>
                                        </nav>
                                    </form>
                                </div>
                                <div id="listData" style="margin-top: 15px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="webpage/flight/stopover/list.js?version=${globalVersion}"></script>
<script>
    $(function () {
        list.init();
    })
</script>