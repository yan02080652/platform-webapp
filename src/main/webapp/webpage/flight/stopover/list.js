var url = {
    list: function () {
        return 'flight/stopover/list';
    },
    detail: function (id) {
        var url = "flight/stopover/detail";
        if (id) {
            url += "?id=" + id;
        }
        return url;
    },
    remove: function (id) {
        return "flight/stopover/delete/" + id;
    },
    save: function () {
        return "flight/stopover/save";
    }
}
var list = {
    init: function () {
        list.reloadList();
    },

    reset: function () {
        document.getElementById("searchForm").reset()
        list.reloadList();
    },

    reloadList: function (pageIndex) {
        var flightNo = $("#flightNo").val();
        $.get(url.list(), {
            pageIndex: pageIndex,
            flightNo: flightNo,
        }, function (data) {
            $("#listData").html(data);
        });
    },

    remove: function (id) {
        layer.confirm("确定删除所选数据吗?", function (index) {
            layer.close(index);
            var loadIndex = layer.load();
            $.get(url.remove(id), function (result) {
                layer.close(loadIndex);
                if (result.code != '1') {
                    layer.msg("删除失败，请重试！")
                }
                list.reloadList();
            })
        });
    },

    detail: function (id) {
        layer.open({
            type: 2,
            area: ['500px', '450px'],
            title: "经停信息",
            closeBtn: 1,
            shadeClose: true,
            content: url.detail(id)
        });
    },

}
