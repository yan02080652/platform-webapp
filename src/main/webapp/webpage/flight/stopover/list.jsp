<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th><label>航班号</label></th>
        <th><label>经停机场</label></th>
        <th><label>经停开始时间</label></th>
        <th><label>经停结束时间</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="data">
        <tr class="odd gradeX">
            <td>${data.flightNo}</td>
            <td>${data.stopCity}</td>
            <td><fmt:formatDate value="${data.fromTime}" pattern="HH:mm"></fmt:formatDate></td>
            <td><fmt:formatDate value="${data.toTime}" pattern="HH:mm"></fmt:formatDate></td>
            <td>
                <a onclick="list.detail(${data.id})">修改</a>&nbsp;&nbsp;
                <a onclick="list.remove('${data.id }');">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=list.reloadList"></jsp:include>
</div>


