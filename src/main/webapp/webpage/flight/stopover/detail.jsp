<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    body {
        background-color: #FFFFFF;
    }

    .layui-form-label {
        box-sizing: content-box;
    }

    .box {
        padding-top: 20px;
    }
</style>
<div class="box">
    <form class="layui-form" action="">
        <input type="hidden" name="id" value="${stopOver.id}"/>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">航班号</label>
                <div class="layui-input-inline">
                    <input type="text" name="flightNo" value="${stopOver.flightNo}" lay-verify="required"
                           class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">经停机场</label>
                <div class="layui-input-inline">
                    <input type="text" name="stopCity" value="${stopOver.stopCity}" lay-verify="required"
                           class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">开始时间</label>
                <div class="layui-input-inline">
                    <input type="text" name="from"
                           value='<fmt:formatDate value="${stopOver.fromTime}" pattern="HH:mm"></fmt:formatDate>'
                           lay-verify="required|time"
                           class="layui-input">

                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">结束时间</label>
                <div class="layui-input-inline">
                    <input type="text" name="to"
                           value='<fmt:formatDate value="${stopOver.toTime}" pattern="HH:mm"></fmt:formatDate>'
                           lay-verify="required|time"
                           class="layui-input">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="stopoverForm">保存</button>
                <button onclick="closeIframe()" class="layui-btn layui-btn-primary">取消</button>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="webpage/flight/stopover/list.js?version=${globalVersion}"></script>
<script>
    var form;
    $(function () {
        layui.use('form', function () {
            form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
            //验证规则
            form.verify({
                time: [/[0-9]{2}\:[0-9]{2}$/, '格式必须HH:mm'],
            });

            //监听提交
            form.on('submit(stopoverForm)', function (data) {
                var formData = data.field;
                $.ajax({
                    url: "flight/stopover/save",
                    data: formData,
                    type: "post",
                    success: function (result) {
                        if (result) {
                            parent.list.reloadList();
                            closeIframe();
                        } else {
                            layer.msg("保存失败，请稍后重试！", {icon: 2});
                        }
                    }
                })
                return false;
            });
        });
    })


    //关闭iframe
    function closeIframe() {
        //先得到当前iframe层的索引,再执行关闭
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }

</script>