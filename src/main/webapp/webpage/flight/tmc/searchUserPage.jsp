<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<style>
.row{
	margin-top: 10px;
}
/* .row div{
	border: groove;
} */
</style>
<div class="container">
	<div>
		<form class="form-inline" method="post">
				<div class=" form-group">
					<label class="control-label">请输入查询条件：</label> 
					<input name="keyword" id="userKeyword" type="text"  class="form-control" placeholder="姓名/拼音/手机/邮箱" style="width: 250px" />
					<input id="hiddenText" type="text" style="display:none" /><!-- 阻止页面刷新 -->
				</div>
				<div class="form-group">
					<button type="button" class="btn btn-default" onclick="search_user()">查询</button>
				</div>
		</form>
	</div>
	<div class="row clearfix" id="userList">
	</div>
</div>

<script type="text/javascript">

$("body").keydown(function() {
    if (event.keyCode == "13") {
    	search_user();
    }
});
function search_user(){
	var keyword = $("#userKeyword").val().trim();
	if(keyword.length <= 0){
		return false;
	}
	$.ajax({
		type:"post",
		url:"flight/tmc/userList",
		data:{keyword:keyword},
		success:function(data){
			$("#userList").html(data);
		}
	});
}
</script>