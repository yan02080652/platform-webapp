<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
</style>
<c:choose>
	<c:when test="${not empty userList }">
		<c:forEach items="${userList }" var="user">
			<div class="col-md-4 column">
				<p>${user.deptPath }</p>
				<p>${user.userName }</p>
				<p>
					<span>${user.mobile }</span> 
					<span>${user.email }</span>
				</p>
				<p>
					<a class="btn" onclick="specialLogin('${user.userId}')">代客下单 »</a>
				</p>
			</div>
		</c:forEach>
	</c:when>
	<c:otherwise>
		没有相关数据!
	</c:otherwise>
</c:choose>
<form action="" method="get" id="loginForm">
	<input type="hidden" name="alp"/>
	<input type="hidden" name="service"/>
</form>
<script type="text/javascript">
function specialLogin(userId){
	
	var myDate=new Date()
	var date2 = myDate.getTime();
	$("#loginForm").attr("target", date2);
	var win = window.open(null,date2);
	win.focus();
	
	$.ajax({
		type:"get",
		url:"flight/tmc/specialLogin",
		data:{userId:userId},
		success:function(data){
			
			$("#loginForm").attr("action", data.loginUrl);
			$("input[name='alp']").val(data.alp);
			$("input[name='service']").val(data.service);
			
			logout(data.logoutUrl);
			
			$("#loginForm").submit();
			
		}
	});
}

function logout(logoutUrl){
	
	for(var i = 0 ; i < logoutUrl.length ; i++){
		new Image().src = logoutUrl[i];
		console.log(logoutUrl[i]);
	}
	
}
</script>