<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<style>
    p,ol,li{
        margin: 0;
        padding: 0;
    }
    .change{
        display: flex;
        font-size: 14px;
        color: #666;
    }
    .warn{
        color: #ff0000;
    }
    input[type="text"]{
        width: 70px;
        text-align:center;
    }
    input[type="number"]{
        width: 70px;
        text-align:center;
    }
    table{
        width: 450px;
        border-collapse:collapse;
        text-align: center;
    }
    table,th,td{
        border: 1px solid #cccccc;
        white-space: nowrap;
    }
    tr{
        height: 30px;
    }
    table input[type="text"]{
        width: 100%;
    }
    .cancel tr>td:first-child{
        width: 150px;
    }
    .btn_b{
        background: #0078c9;
        color: #fff;
        width: 140px;
    }
    .btn_f{
        background: #fff;
        color: #0099cc;
        width: 120px;
        border: 1px solid #0099cc;
        cursor: pointer;
    }
    [class*='btn']{
        /*display: inline-block;*/
        border-radius: 4px;
        /*text-align: center;*/
        line-height: 30px;
        margin-right: 10px;
        cursor: pointer;
    }
    .left{
        flex: 0 0 55%;
        margin-right: 20px;
        padding-left: 3%;
        padding-top: 1%;
    }
    .left .title{
        border-bottom: 1px solid #eee;
        padding-bottom: 8px;
    }
    .left .title p:first-child{
        font-size: 16px;
        color: #333;
        font-weight: 500;
        line-height: 30px;
    }
    .left ol{
        padding-left: 10px;
    }
    .left li{
        margin-top: 15px;
    }
    .left li>span{
        font-size: 15px;
        font-weight: 500;
    }
    .left li>div{
        padding: 5px 0;
        display: flex;
        text-align: center;
    }
    .left li>div>div{
        margin-right: 15px;
        text-align: center;
    }
    .left .accord .warn{
        margin-left: 10px;
        font-size: 12px;
    }
    .left li .fee-chose>div{
        margin-right: 25px;
    }
    .left .popovers>div{
        border: 1px solid #ccc;
        position: relative;
        padding: 10px;
    }
    .left .popovers>div:before{
        content: '';
        position: absolute;
        top: -5px;
        left: 100px;
        width: 10px;
        height: 10px;
        border: 1px solid #ccc;
        transform: rotate(45deg);
        border-right: transparent;
        border-bottom: transparent;
        background: #fff;
    }
    .right{
        flex: 0 0 45%;
        padding-right: 1%;
        padding-top: 1%;
    }
    .right .title{
        font-weight: 600;
        line-height: 25px;
        color: black;
        font-weight: 700;
    }
    .right .title span{
        font-weight: normal;
        margin-left: 10px;
    }
    .right .old-ticket{
        margin-top: 20px;
        margin-bottom: 20px;
    }
    .right .passen{
        display: flex;
    }
    table.info span{
        color: #fd6501;
    }
    .pay,.reason,.chang-ticket{
        display: none;
    }
    .alter .content{
        position: fixed;
        z-index: 500;
        width: 300px;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background: #fff;
        color: #333;
    }
    .alter .black{
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0,0,0,.5);
        z-index: 100
    }
    .alter .title{
        background: #f8f8f8;
        display: flex;
        justify-content: space-between;
        line-height: 40px;
        border-bottom: 1px solid #eee;
        padding: 0 10px;
    }
    .alter .menu{
        padding: 0 10px 10px;
    }
    .alter .tip{
        padding: 30px 0;
    }
    .alter .sure{
        text-align: right;
    }
    .alter .btn_sure{
        background: #0078c9;
        color: #fff;
        width: 55px;
    }
    .alter .btn_clear{
        background: #f0f0f1;
        width: 55px;
        border: 1px solid #eee;
    }
    .padingStyle {
        padding-right: 7px;
        padding-left: 7px;
    }
    .right div {
        margin-bottom: 5px;
        margin-top: 5px;
    }
    .right span {
        margin-left: 3px;
        margin-right: 3px;
    }
    .tip-td{
        position: relative;
    }
    .tip-td::before{
        content: '票号格式不正确(例:784-8775510286)';
        display: block;
        position: absolute;
        left: 105%;
        top: 50%;
        font-size: 12px;
        transform: translateY(-50%);
        color: red;
    }
    input:read-only {
        background: #BEBFC3;
    }
</style>

<div class="change">
    <div class="left">
        <div class="title">
            <p style="color: black;font-weight: 700;">改签单审核</p>
            <p>改签单号：${order.id}</p>
            <input id="orderId" value="${order.id}" hidden>
            <input id="taskId" value="${taskDto.id}" hidden>

        </div>
        <ol>
            <li>
                <span style="color: black;font-weight: 700;">1.核实改签原因是否非自愿</span>
                <div class="accord">
                    <label style="color: black;font-weight: 700;"><input type="checkbox" name="voluntary"  <c:if test="${!order.voluntary}">checked</c:if> >航班取消/延误1小时以上</label>
                    <span class="warn voluntaryNotice" <c:if test="${order.voluntary}">hidden</c:if>>非自愿改签</span>
                </div>
            </li>
            <li>
                <span style="color: black;font-weight: 700;">2.确认出票方式、是否有座以及供应商结算费用</span>
                <div>

                    <label style="padding-right: 10px;color: black;font-weight: 700;"><input type="radio" name="orderType" value="TICKET_CHANGES" <c:if test="${order.orderType eq 'TICKET_CHANGES'}">checked</c:if> >直接改签</label>

                    <label style="color: black;font-weight: 700;"><input type="radio" name="orderType" value="REFUND_TICKET_REPURCHASE" <c:if test="${order.orderType eq 'REFUND_TICKET_REPURCHASE'}">checked</c:if> >退票重出</label>
                </div>

                <div class="fee-chose changeDirect" >
                    <div>
                        <span>供应商</span>
                        <select name="issueChannel1">

                                <c:forEach items="${channelTypes}" var="channel">
                                    <c:choose>
                                        <c:when test="${empty order.issueChannel}">
                                            <option value="${channel.code}" <c:if test="${ originOrder.issueChannel eq channel.code}">selected</c:if> >${channel.name}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${channel.code}" <c:if test="${order.issueChannel eq channel.code}">selected</c:if> >${channel.name}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <%--<option value="${channel.code}" <c:if test="${(not empty order.issueChannel && order.issueChannel eq channel.code) || (empty order.issueChannel && originOrder.issueChannel eq channel.code)}">selected</c:if> >${channel.name}</option>--%>

                        </select>
                    </div>
                    <div>
                        <span>支付通道</span>
                        <select name="settleChannelId1">
                            <c:forEach items="${settleChannels}" var="settleChannel">
                                <c:choose>
                                    <c:when test="${empty order.settleChannelId}">
                                        <option value="${settleChannel.id}" <c:if test="${originOrder.settleChannelId eq settleChannel.id}">selected</c:if> >${settleChannel.name}</option>
                                    </c:when>
                                    <c:otherwise>
                                        <option value="${settleChannel.id}" <c:if test="${order.settleChannelId eq settleChannel.id}">selected</c:if> >${settleChannel.name}</option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>

                        </select>
                    </div>
                    <div>
                        <span>改期/升舱费</span>
                        <input type="number" name="settlePirce1"  value="${fn:length(order.passengers) * (order.orderFee.rescheduledPrice + order.orderFee.upgradePrice + order.orderFee.departureTax + order.orderFee.fuelTax + order.orderFee.servicePrice)}">
                    </div>
                </div>


                 <div class="out-ticket changeRefund">
                <div style="color: black;font-weight: 700;">新票出票</div>
                <div class="fee">
                    <div style="display: flex;margin-bottom: 10px">

                        <div class="padingStyle">

                            <div>供应商</div>
                            <div>
                                <select name="issueChannel2">

                                    <c:forEach items="${channelTypes}" var="channel">
                                        <c:choose>
                                            <c:when test="${empty order.issueChannel}">
                                                <option value="${channel.code}" <c:if test="${originOrder.issueChannel eq channel.code}">selected</c:if> >${channel.name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${channel.code}" <c:if test="${order.issueChannel eq channel.code}">selected</c:if> >${channel.name}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="padingStyle">
                            <div>支付通道</div>
                            <div>
                                <select name="settleChannelId2">

                                    <c:forEach items="${settleChannels}" var="settleChannel">

                                        <c:choose>
                                            <c:when test="${empty order.settleChannelId}">
                                                <option value="${settleChannel.id}" <c:if test="${originOrder.settleChannelId eq settleChannel.id}">selected</c:if> >${settleChannel.name}</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${settleChannel.id}" <c:if test="${order.settleChannelId eq settleChannel.id}">selected</c:if> >${settleChannel.name}</option>
                                            </c:otherwise>
                                        </c:choose>

                                    </c:forEach>

                                </select>
                            </div>
                        </div>
                        <div class="padingStyle">
                            <div>供应商结算价</div>
                            <div>
                                <input type="number" name="settlePrice2" class="price" value="${order.orderFee.totalSettlePrice}">
                            </div>
                        </div>
                    </div>
                    <div style="display: flex">
                        <div class="padingStyle" style="padding-top: 22px">
                            <div>每人</div>
                            <div>小计</div>
                        </div>
                        <div class="padingStyle">
                            <div>票价</div>
                            <div><input type="number" class="singlePrice1 price update" name="salePrice" value="${order.orderFee.salePrice}"></div>
                            <div><p>${fn:length(order.passengers) * order.orderFee.salePrice}</p></div>
                        </div>
                        <div class="padingStyle" >
                            <div>机建</div>
                            <div><input type="number" class="singlePrice1 price update" name="departureTax" value="${order.orderFee.departureTax}"></div>
                            <div><p>${fn:length(order.passengers) * order.orderFee.departureTax}</p></div>
                        </div>
                        <div class="padingStyle">
                            <div>燃油</div>
                            <div><input type="number" class="singlePrice1 price update" name="fuelTax" value="${order.orderFee.fuelTax}"></div>
                            <div><p>${fn:length(order.passengers) * order.orderFee.fuelTax}</p></div>
                        </div>
                        <div class="padingStyle">
                            <div>出票服务费</div>
                            <div><input type="number" class="singlePrice1 price update" name="servicePrice" value="${order.orderFee.servicePrice}"></div>
                            <div><p>${fn:length(order.passengers) * order.orderFee.servicePrice}</p></div>
                        </div>
                        <div class="padingStyle">
                            <div>新票总价</div>
                            <div><p id="newTicketPrice">${fn:length(order.passengers) * (order.orderFee.salePrice+order.orderFee.departureTax+order.orderFee.fuelTax+order.orderFee.servicePrice)}</p></div>
                        </div>
                    </div>
                </div>
            </div>
                 <div class="retund changeRefund">
                <div style="white-space: nowrap;color: black;font-weight: 700;">原票退票</div>
                     <input name="originRefundOrderId" value="${originRefundOrder.id}" hidden>
                <div>
                    <table>
                        <tr>
                            <td></td>
                            <td>票价</td>
                            <td>供应商退票费</td>
                            <td>额外调整</td>
                            <td>退票服务费</td>
                            <td>应退客户</td>
                            <td>支付通道</td>
                        </tr>
                        <tr>

                            <td>每人</td>
                            <td id="singleAmout">${originOrder.orderFee.salePrice + originOrder.orderFee.departureTax + originOrder.orderFee.fuelTax + originOrder.orderFee.upgradePrice + originOrder.orderFee.rescheduledPrice}</td>
                            <td><input type="number" class="price update" value="${originRefundOrder.refundFee}" name="refundFee"></td>
                            <td><input type="number" class="price update" value="${originRefundOrder.adjustFee}"  name="adjustFee"></td>
                            <td><input type="number" class="price update" name="refundServiceFee" value="${originRefundOrder.refundServiceFee}"  ></td>
                            <td><input type="number" class="price update" name="needReefundAmout" value="${originOrder.orderFee.salePrice + originOrder.orderFee.departureTax
                            + originOrder.orderFee.fuelTax + originOrder.orderFee.upgradePrice + originOrder.orderFee.rescheduledPrice
                            - originRefundOrder.refundFee - originRefundOrder.adjustFee - originRefundOrder.refundServiceFee}" readonly></td>
                            <td>

                                <select name="refundSettleChannelId">

                                    <c:forEach items="${settleChannels}" var="settleChannel">
                                        <option value="${settleChannel.id}" <c:if test="${order.settleChannelId eq settleChannel.id}"></c:if> >${settleChannel.name}</option>
                                    </c:forEach>

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>合计</td>

                            <td id="originTotalPrice">${fn:length(order.passengers) * (originOrder.orderFee.salePrice + originOrder.orderFee.departureTax + originOrder.orderFee.fuelTax + originOrder.orderFee.upgradePrice + originOrder.orderFee.rescheduledPrice)}</td>
                            <td id="refundTotalFee">${fn:length(order.passengers) * originRefundOrder.refundFee}</td>
                            <td id="adjustTotalFee">${fn:length(order.passengers) * originRefundOrder.adjustFee}</td>
                            <td id="refundTotalService1">${fn:length(order.passengers) * originRefundOrder.refundServiceFee}</td>
                            <td id="needRefundTotalAmout">${fn:length(order.passengers) * (originOrder.orderFee.salePrice + originOrder.orderFee.departureTax
                                    + originOrder.orderFee.fuelTax + originOrder.orderFee.upgradePrice + originOrder.orderFee.rescheduledPrice
                                    - originRefundOrder.refundFee - originRefundOrder.adjustFee - originRefundOrder.refundServiceFee)}</td>

                            <td></td>
                        </tr>
                    </table>
                </div>
            </div>


            </li>
            <li>
                <span style="color: black;font-weight: 700;">3.确认应收客户费用</span>
                <div class="fee-receive changeDirect">
                    <div>应收总额<span class="warn" id="changeTotalPrice">${(order.orderFee.rescheduledPrice + order.orderFee.upgradePrice
                    + order.orderFee.fuelTax + order.orderFee.departureTax + order.orderFee.servicePrice) * fn:length(order.passengers)}</span>
                    </div>
                    <div style="padding-top: 22px">
                        <div>每人</div>
                        <div>小计</div>
                    </div>
                    <div>
                        <div>同舱改期费</div>
                        <div><input type="number" name="rescheduledPrice" class="singlePrice price update" value="${order.orderFee.rescheduledPrice}"></div>
                        <div>${order.orderFee.rescheduledPrice * fn:length(order.passengers)}</div>
                    </div>

                    <div>
                        <div>票面差价</div>
                        <div><input type="number" name="upgradePrice" class="singlePrice price update" value="${order.orderFee.upgradePrice}"></div>
                        <div>${order.orderFee.upgradePrice * fn:length(order.passengers)}</div>
                    </div>
                    <div>
                        <div>税费差价</div>
                        <div><input type="number" name="fuelAndDepartureTax" class="singlePrice price update" value="${order.orderFee.departureTax + order.orderFee.fuelTax}"></div>
                        <div>${(order.orderFee.departureTax + order.orderFee.fuelTax) * fn:length(order.passengers)}</div>
                    </div>
                    <div>
                        <div>服务费</div>
                        <div><input type="number" name="changeService" class="singlePrice price update" value="${order.orderFee.servicePrice}"></div>
                        <div>${order.orderFee.servicePrice * fn:length(order.passengers)}</div>
                    </div>
                </div>

                <div class="fee-receive changeRefund">
                    <div>应收总额<span class="warn" id="refundChangeTotalPrice">${(fn:length(order.passengers) * (order.orderFee.salePrice + order.orderFee.departureTax+order.orderFee.fuelTax+order.orderFee.servicePrice))
                     - (fn:length(order.passengers) * (originOrder.orderFee.salePrice + originOrder.orderFee.departureTax
                            + originOrder.orderFee.fuelTax + originOrder.orderFee.upgradePrice + originOrder.orderFee.rescheduledPrice
                            - originRefundOrder.refundFee - originRefundOrder.adjustFee - originRefundOrder.refundServiceFee))}</span></div>
                    <div><span>新票总价</span><input type="number" id="newTicketTotalPrice" value="${fn:length(order.passengers) * (order.orderFee.salePrice+order.orderFee.departureTax+order.orderFee.fuelTax+order.orderFee.servicePrice)}" readonly></div>
                    <%--<div><span>出票服务费</span><input type="number" id="issueTicketTotalService" value="${fn:length(order.passengers) * order.orderFee.servicePrice}" readonly></div>--%>
                    <%--<div><span>退票服务费</span><input type="number" id="refundTotalService" value="0.00" readonly></div>--%>
                    <div><span>应退客户</span><input type="number" id="needRefundAmout" value="${fn:length(order.passengers) * (originOrder.orderFee.salePrice + originOrder.orderFee.departureTax
                                    + originOrder.orderFee.fuelTax + originOrder.orderFee.upgradePrice + originOrder.orderFee.rescheduledPrice
                                    - originRefundOrder.refundFee - originRefundOrder.adjustFee - originRefundOrder.refundServiceFee)}" readonly></div>
                </div>

                <div class="fee-receive">
                    <div>已收总额<span class="warn" id="alreadyAmout">${order.orderFee.receivedAmount}</span></div>
                    <table>
                        <tr>
                            <th class="text-center">时间</th>
                            <th class="text-center">备注</th>
                            <th class="text-center">收支明细</th>
                        </tr>

                            <c:forEach items="${logList}" var="log">
                                <c:if test="${log.type eq 'USER_PAYMENT'}">
                                    <c:if test="${fn:contains(log.description, '￥')}">
                                        <tr>
                                            <td><fmt:formatDate value="${log.operationDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                                            <td>${fn:substringBefore(log.description, "￥")}</td>
                                            <td>${fn:substringAfter(log.description, "￥")}</td>
                                        </tr>
                                    </c:if>

                                </c:if>
                            </c:forEach>


                    </table>
                </div>
            </li>
            <li>
                <span>新航班占座与核实是否需补收差价</span>
                <div>
                    <div>差价<span class="warn" id="difference">
                        <c:if test="${order.orderType eq 'REFUND_TICKET_REPURCHASE'}">
                            ${(fn:length(order.passengers) * (order.orderFee.salePrice + order.orderFee.departureTax+order.orderFee.fuelTax+order.orderFee.servicePrice))
                            - (fn:length(order.passengers) * (originOrder.orderFee.salePrice + originOrder.orderFee.departureTax
                            + originOrder.orderFee.fuelTax + originOrder.orderFee.upgradePrice + originOrder.orderFee.rescheduledPrice
                            - originRefundOrder.refundFee - originRefundOrder.adjustFee - originRefundOrder.refundServiceFee)) - order.orderFee.receivedAmount}
                        </c:if>

                        <c:if test="${order.orderType eq 'TICKET_CHANGES'}">
                            ${((order.orderFee.rescheduledPrice + order.orderFee.upgradePrice
                        + order.orderFee.fuelTax + order.orderFee.departureTax + order.orderFee.servicePrice) * fn:length(order.passengers)) - order.orderFee.receivedAmount}
                        </c:if>

                    </span></div>
                    <div><span>PNR</span><input type="text" name="pnrCode" value="${order.pnrCode}" maxlength="6"></div>
                    <div><span>供应商单号</span><input type="text" name="externalOrderId" value="${order.externalOrderId}" ></div>
                </div>
                <div class="needpay">
                    <span class="btn_b confirmPay">已沟通确认支付差价</span>
                    <span class="btn_f">拒绝改签</span>
                </div>
                <div class="popovers needpay">
                    <div>
                        <p style="text-align: left">操作后：</p>
                        <p style="text-align: left">1.需用户前往APP或官网支付差价</p>
                        <p>2.该任务变成“挂起中”，如需代付请在“挂起中”的任务详情中，点击“前往收银台”</p>
                    </div>
                </div>
                <div class="waitpay">
                    <span class="btn_b goPay">前往收银台</span>
                    <span class="btn_f">拒绝改签</span>
                </div>
            </li>
            <li class="needrefund">
                    <span>票务处理</span>
                    <div>
                        <span style="padding-right: 10px;">回填票号</span>
                        <label><input type="checkbox" name="checkTicketNo">未从供应商处获得票号</label>
                    </div>
                    <table class="cancel" id="passengerTable">
                        <c:forEach items="${order.passengers}" var="passenger">

                            <tr>
                                <td>${passenger.name}</td>
                                <td><input type="text" onblur="autoAdd($(this))" name="ticketNo" passengerId="${passenger.id}" value="${passenger.ticketNo}"></td>
                                <%--<span style="text-align: left;color: red" class="pull-left" >票号格式不正确(例:784-8775510286)</span>--%>
                            </tr>

                        </c:forEach>
                    </table>
                     <div class="warn">请确保完成改签时原订单已退票。</div>
                    <div>
                        <span class="btn_b completeChange" >完成改签</span>
                        <span class="btn_f">拒绝改签</span>
                    </div>

            </li>
        </ol>
    </div>
    <div class="right">
        <div class="new-ticket">
            <p class="title">新票</p>
            <div>
                <span>${order.orderFlights[0].carrierName}</span>
                <span>${order.orderFlights[0].flightNo}</span>
                <c:if test="${not empty order.orderFlights[0].realFlightNo}">
                    <span>(实际承运: ${order.orderFlights[0].shareCarrier.nameShort}  ${order.orderFlights[0].realFlightNo })</span>
                </c:if>
                <span><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${order.orderFlights[0].fromDate }" /></span>
                <span>${order.orderFlights[0].fromCityName}-${order.orderFlights[0].toCityName}</span>
                <span>经济舱 ${order.orderFlights[0].cabinCode}</span>
                <a href="/flight/tmc/getChangeOrderData?orderId=${order.id}&taskId=${taskDto.id}">修改</a>
            </div>
            <div><span>票面价：${order.orderFee.salePrice}</span>&nbsp;&nbsp;<span>税费：${order.orderFee.departureTax+order.orderFee.fuelTax}</span></div>
            <div><span>联系人：${order.contactor.name}</span>&nbsp;&nbsp;<span>${order.contactor.mobile}</span></div>
            <div>特殊改签说明: ${order.remark}</div>
        </div>
        <div class="old-ticket">
            <p class="title">原票:<span>${originOrder.id}</span></p>
            <div>
                <span>${originOrder.orderFlights[0].carrierName}</span>
                <span>${originOrder.orderFlights[0].flightNo}</span>
                <c:if test="${not empty originOrder.orderFlights[0].realFlightNo}">
                    <span>(实际承运: ${originOrder.orderFlights[0].shareCarrier.nameShort}  ${originOrder.orderFlights[0].realFlightNo })</span>
                </c:if>
                <span><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${originOrder.orderFlights[0].fromDate }" /></span>
                <span>${originOrder.orderFlights[0].fromCityName}-${originOrder.orderFlights[0].toCityName}</span>
                <span>经济舱 ${originOrder.orderFlights[0].cabinCode}</span>
            </div>
            <div><span>预定人：${originOrder.contactor.name}</span>&nbsp;&nbsp;<span>${originOrder.contactor.mobile}</span></div>
            <div class="passen">
                <div>乘机人：</div>
                <div>
                    <c:forEach items="${originOrder.passengers}" var="passenger">
                        <p>${passenger.name} ${passenger.mobile}<span>&nbsp;&nbsp;${passenger.status.message}</span></p>
                    </c:forEach>
                </div>
            </div>
            <div>出票时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${originOrder.issueTicketTime }" /></div>
            <div><span>供应商：${originOrder.issueChannelName}</span>&nbsp;&nbsp;<span>PNR：${originOrder.pnrCode }</span></div>
        </div>

        <div class="backOrChange">
            <c:if test="${originOrder.orderFlights[0].refundChangeDetail.type eq 1}">
                <table>
                    <tbody>
                    <tr>
                        <td></td>
                        <c:forEach items="${originOrder.orderFlights[0].refundChangeDetail.headers}" var="head">
                            <td>${head}</td>
                        </c:forEach>
                    </tr>
                    <tr>
                        <td>退票费</td>
                        <c:forEach items="${originOrder.orderFlights[0].refundChangeDetail.refundAmountList}" var="amonut">
                            <c:choose>
                                <c:when test="${amonut > 0}">
                                    <td>¥ <span class="bOrCMoney">${amonut}</span>/人</td>
                                </c:when>
                                <c:otherwise>
                                    <td>免费退票</td>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </tr>
                    <tr>
                        <td>同舱改期费</td>
                        <c:forEach items="${originOrder.orderFlights[0].refundChangeDetail.changeAmountList}" var="amonut">
                            <c:choose>
                                <c:when test="${amonut > 0}">
                                    <td>¥ <span class="bOrCMoney">${amonut}</span>/人</td>
                                </c:when>
                                <c:otherwise>
                                    <td>免费改期</td>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </tr>
                    <tr>
                        <td>签转条件</td>
                        <td colspan="${fn:length(originOrder.orderFlights[0].refundChangeDetail.headers)}">${originOrder.orderFlights[0].refundChangeDetail.endorseRule}</td>
                    </tr>
                    <tr>
                        <td>行李额</td>
                        <td colspan="${fn:length(originOrder.orderFlights[0].refundChangeDetail.headers)}">${originOrder.orderFlights[0].refundChangeDetail.baggage}</td>
                    </tr>
                    <tr>
                        <td>备注</td>
                        <td colspan="${fn:length(originOrder.orderFlights[0].refundChangeDetail.headers)}">${originOrder.orderFlights[0].refundChangeDetail.remark}</td>
                    </tr>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${originOrder.orderFlights[0].refundChangeDetail.type eq 2}">
                <table>
                    <tbody>
                    <tr>
                        <td>退票费</td>
                        <td>${originOrder.orderFlights[0].refundChangeDetail.headers[0]}</td>
                    </tr>
                    <tr>
                        <td>同舱改期费</td>
                        <td>${originOrder.orderFlights[0].refundChangeDetail.headers[1]}</td>
                    </tr>
                    <tr>
                        <td>签转条件</td>
                        <td>${originOrder.orderFlights[0].refundChangeDetail.endorseRule}</td>
                    </tr>
                    <tr>
                        <td>行李额</td>
                        <td>${originOrder.orderFlights[0].refundChangeDetail.baggage}</td>
                    </tr>
                    <tr>
                        <td>备注</td>
                        <td>${originOrder.orderFlights[0].refundChangeDetail.remark}</td>
                    </tr>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${originOrder.orderFlights[0].refundChangeDetail.type eq 3}">
                <table>
                    <tbody>
                    <tr>
                        <td>退票说明</td>
                        <td class="tdInfo">${originOrder.orderFlights[0].refundChangeDetail.headers[0]}</td>
                    </tr>
                    <tr>
                        <td>改期说明</td>
                        <td class="tdInfo">${originOrder.orderFlights[0].refundChangeDetail.headers[1]}</td>
                    </tr>
                    <tr>
                        <td>签转说明</td>
                        <td class="tdInfo">${originOrder.orderFlights[0].refundChangeDetail.headers[2]}</td>
                    </tr>
                    </tbody>
                </table>
            </c:if>
        </div>

    </div>

    <form action="" method="get" id="changePayForm">
        <input type="hidden" name="alp"/>
        <input type="hidden" name="service"/>
    </form>

    <input id="userId" value="${order.userId}" hidden>
    <input id="taskStatus" value="${taskDto.taskStatus}" hidden>
    <input id="passengerNum" value="${fn:length(order.passengers)}" hidden>

</div>


<script src="webpage/flight/tmc/js/changeOrder.js?version=${globalVersion}"></script>

