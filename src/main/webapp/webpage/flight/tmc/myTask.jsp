<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Locale" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<style>
    .tooltip {
        width: 24em;
    }

</style>
<!-- 状态栏 -->
<input type="hidden" id="currentTaskType"/>
<div>
    <!-- 左边状态 -->
    <div class="tab pull-left">
        <ul class="fix" id="myTaskStatusTab">
            <li class="tab1 active" data-value="PROCESSING" onclick="submitAjax(null,'PROCESSING')">
                处理中(${processingMap.PROCESSING==null?0:processingMap.PROCESSING})
            </li>
            <li class="tab2" data-value="HANG_UP" onclick="submitAjax(null,'HANG_UP')">
                挂起中(${hangUpMap.HANG_UP==null?0:hangUpMap.HANG_UP})
            </li>
            <li class="tab3" data-value="ALREADY_PROCESSED" onclick="submitAjax(null,'ALREADY_PROCESSED')">
                近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})
            </li>
        </ul>
        <div id="f_my_content">
            <div class="content1">
                <p>
                    <a onclick="submitAjax(null,'PROCESSING','WAIT_TICKET')">待出票(${processingMap.WAIT_TICKET==null?0:processingMap.WAIT_TICKET })</a>
                    <a onclick="submitAjax(null,'PROCESSING','WAIT_URGE')">待催单(${processingMap.WAIT_URGE==null?0:processingMap.WAIT_URGE})</a>
                    <a onclick="submitAjax(null,'PROCESSING','REFUND_TICKET_AUDIT')">退审核(${processingMap.REFUND_TICKET_AUDIT==null?0:processingMap.REFUND_TICKET_AUDIT})</a>
                    <a onclick="submitAjax(null,'PROCESSING','WAIT_CHANGE')">待改签(${processingMap.WAIT_CHANGE==null?0:processingMap.WAIT_CHANGE})</a>
                </p>
            </div>
            <div class="content2">
                <p style="margin-left: 427px;">
                    <a onclick="submitAjax(null,'HANG_UP','WAIT_CHANGE')">改签(${hangUpMap.WAIT_CHANGE==null?0:hangUpMap.WAIT_CHANGE })</a>
                </p>
            </div>
            <div class="content3">
                <p>&nbsp;&nbsp;&nbsp;
                    <a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_TICKET')">出票(${alreadyMap.WAIT_TICKET==null?0:alreadyMap.WAIT_TICKET })</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_URGE')">催单(${alreadyMap.WAIT_URGE==null?0:alreadyMap.WAIT_URGE})</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="submitAjax(null,'ALREADY_PROCESSED','REFUND_TICKET_AUDIT')">审核(${alreadyMap.REFUND_TICKET_AUDIT==null?0:alreadyMap.REFUND_TICKET_AUDIT})</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    <a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_CHANGE')">改签(${alreadyMap.WAIT_CHANGE==null?0:alreadyMap.WAIT_CHANGE})</a>
                </p>
            </div>

        </div>
    </div>

</div>


<!-- 任务列表 -->
<div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="width: 38%">航班信息</th>
            <%--<th style="width: 19%">乘客信息</th>--%>
            <th style="width: 19%">收支信息</th>
            <th style="width: 19%">处理信息</th>
            <th style="width: 19%">供应信息</th>
        </tr>
        </thead>
        <tbody id="myTaskTbody">
        <c:forEach items="${pageList.list }" var="orderTask" varStatus="vs">
            <tr>
                <td colspan="5">
                    <span>企业:${orderTask.orderDto.cName }</span>
                    <span>预订人:${orderTask.orderDto.userName }</span>
                    <span>订单号:<font color="blue"><a href="/order/flightDetail/${orderTask.orderDto.id }"
                                                    target="_Blank">${orderTask.orderDto.id }</a></font></span>
                    <span>来源:${orderTask.orderDto.orderOriginType.message}</span>
                    <span>标签:${orderTask.orderDto.orderShowStatus.message }</span>
                    <span>下单时间:<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                               value="${orderTask.orderDto.createDate }"/></span>
                    <span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span>
                    <span>任务创建时间:<fmt:formatDate value="${orderTask.createDate }" pattern="MM-dd HH:mm"/></span>
                    <span hidden><font color="red">任务超时</font></span>
                    <input class="proDate" type="hidden"
                           value='<fmt:formatDate value="${orderTask.maxProcessDate }" pattern="yyyy-MM-dd HH:mm:ss"/>'>
                    <input type="hidden" value="${orderTask.taskStatus }"/>
                    <c:if test="${orderTask.taskStatus == 'PROCESSING' }">
                        <label>任务延时</label>
                        <select style="width: 55px"
                                onchange="delay('${orderTask.id }',$(this).val(),'${orderTask.operatorId }')">
                            <option value="-1">--
                            <option value="5">5分钟
                            <option value="15">15分钟
                            <option value="60">1小时
                            <option value="1440">1天
                        </select>
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>
                    <c:forEach items="${orderTask.orderDto.orderFlights}" var="detail">

						<span style="display: inline-block;margin-bottom: 5px">
                             ${detail.detailMessages} <br>

						</span>


                    </c:forEach>
                    <br>
                    <span style="display: inline-block;margin-top: 15px">
						出行人: ${orderTask.userNames}
					</span>
                    <span style="float: right;display: inline-block;margin-top: 15px">
						<a data-trigger="tooltip" data-content="${orderTask.orderDto.ssInstructionsType1 }" class="ss"
                           data-clipboard-text="${orderTask.orderDto.ssInstructionsType2 }">点击复制SS</a>
					</span>
                </td>

                <td>
                    <span>应收￥${orderTask.orderDto.orderFee.totalOrderPrice }</span>
                    <span>
							<c:choose>
								<c:when test="${orderTask.orderDto.paymentStatus eq 'PAYMENT_SUCCESS' }">已收</c:when>
								<c:otherwise>未收</c:otherwise>
							</c:choose>
						</span><br/>
                    <c:if test="${not empty orderTask.orderDto.orderFee.totalSettlePrice}">
                        <span>应付￥${orderTask.orderDto.orderFee.totalSettlePrice }</span>
                        <span>
								<c:choose>
									<c:when test="${not empty orderTask.orderDto.externalOrderStatus and (orderTask.orderDto.externalOrderStatus eq 'WAIT_ISSUE' or orderTask.orderDto.externalOrderStatus eq 'ISSUED' or orderTask.orderDto.externalOrderStatus eq 'PAYING')}">
										已付
									</c:when>
									<c:otherwise>未付</c:otherwise>
								</c:choose>
							</span><br/>
                    </c:if>

                    <c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
                        <span><a
                                onclick="loadIncomeDetail('${orderTask.orderDto.id}','${orderTask.orderDto.paymentPlanNo}')">明细</a></span>
                    </c:if>
                </td>
                <td>
                    <span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>
                    <span>处理人：${orderTask.operatorName }</span><br/>
                    <c:if test="${not empty orderTask.orderDto.customerCode}">
                        <span style="color: red">大客户编码：${orderTask.orderDto.customerCode }</span><br/>
                    </c:if>
                    <c:if test="${(orderTask.taskStatus eq 'PROCESSING' || orderTask.taskStatus eq 'HANG_UP') && orderTask.taskType eq 'WAIT_CHANGE' && not empty orderTask.result}">
                        <font color="red">${orderTask.result }</font><br/>
                    </c:if>
                    <c:if test="${orderTask.taskStatus eq 'PROCESSING' }">
							<span>
								<a onclick="transfer('${orderTask.id}','${orderTask.operatorId }',true)">交接</a>

								<c:if test="${orderTask.taskType eq 'WAIT_URGE' or orderTask.taskType eq 'WAIT_TICKET' }">
                                    | <a
                                        onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.operatorId}')">线下出票</a>
                                    | <a
                                        onclick="rejectOrder('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.orderDto.userId }','${orderTask.operatorId}')">拒单退款</a>
                                </c:if>

                                <!-- 待催单 -->
								<c:if test="${orderTask.taskType eq 'WAIT_URGE' }">
                                    |<br/><a onclick="confirmDone('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.operatorId}')">已完成</a>
                                </c:if>

                                <!-- 退款审核 -->
								 <c:if test="${orderTask.taskType eq 'REFUND_TICKET_AUDIT' }">
                                     | <a
                                         onclick="ticketAduit('${orderTask.refundOrderId}','${orderTask.id }','${orderTask.operatorId }')">审核</a>
                                 </c:if>

                                <!-- 待改签 -->
								<c:if test="${orderTask.taskType eq 'WAIT_CHANGE' }">
                                    | <a
                                        href="/flight/tmc/getChangeOrderTask?orderId=${orderTask.orderDto.id}&taskId=${orderTask.id}"
                                        target="_blank">审核</a>
                                    <%--| <a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.operatorId }')">改签成功</a>
                                    | <a onclick="rejectOrder('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.orderDto.userId }','${orderTask.operatorId}')">改签失败</a>--%>
                                </c:if>
							 </span>
                    </c:if>
                    <c:if test="${orderTask.taskStatus eq 'HANG_UP' }">
                        <a onclick="transfer('${orderTask.id}','${orderTask.operatorId }',true)">交接</a>
                        | <a
                            href="/flight/tmc/getChangeOrderTask?orderId=${orderTask.orderDto.id}&taskId=${orderTask.id}"
                            target="_blank">详情</a>
                    </c:if>
                    <c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' }">
                        <span>处理结果：${orderTask.result}</span>
                    </c:if>
                </td>
                <td>
                    <c:if test="${orderTask.taskType ne 'WAIT_CHANGE' }">
						<span>
							<a data-trigger="tooltip"
                               data-content="原因：${orderTask.orderDto.orderOperationLog.description }">
								<font color="red">${orderTask.orderDto.orderOperationLog.type.message }${orderTask.orderDto.orderOperationLog.status.message }</font>
							</a><br/>

							<c:choose>
                                <c:when test="${not empty orderTask.orderDto.issueChannel }">
                                    ${issueChannelMap[orderTask.orderDto.issueChannel] }
                                </c:when>
                                <c:otherwise>
                                    ${issueChannelMap[orderTask.orderDto.hubsConnectDto.issueChannelCode] }
                                </c:otherwise>
                            </c:choose>

                            <!-- 有供应商订单信息 -->
							<c:if test="${not empty orderTask.orderDto.externalOrderId }">
                                <a data-trigger="tooltip"
                                   data-content="${orderTask.orderDto.externalOrderId }">订单号</a><br/>
                            </c:if>
								<c:if test="${not empty orderTask.orderDto.pnrCode }">
                                    PNR:${orderTask.orderDto.pnrCode }
                                </c:if>
								<c:if test="${not empty orderTask.orderDto.externalOrderStatusMessage }">
                                    状态:${orderTask.orderDto.externalOrderStatusMessage } <br/>
                                </c:if>

								<c:if test="${orderTask.taskStatus eq 'PROCESSING'}">
									<c:if test="${orderTask.taskType eq 'WAIT_TICKET'  }">
										<c:if test="${orderTask.orderDto.orderOriginType ne 'CC_MANUAL'}">
											<a onclick="payment('${orderTask.orderId}','${orderTask.id }','${orderTask.operatorId }')">转系统出票</a>&nbsp;|&nbsp;
										</c:if>
										<a onclick="cancelOrder('${orderTask.orderId}','${orderTask.operatorId }','${orderTask.id }')">取消</a>
									</c:if>
									<%--<c:if test="${orderTask.taskType eq 'WAIT_URGE'  }">--%>
									<%--<a onclick="cancelOrder('${orderTask.orderId}','${orderTask.operatorId }')">取消供应商订单</a>--%>
									<%--</c:if>--%>
								</c:if>
						</span>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<div class="pagin" style="margin-bottom: 22px">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="webpage/flight/tmc/js/myTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
    $(function () {

        var status = "${taskStatus}";
        $("#myTaskStatusTab li").each(function () {
            if ($(this).attr("data-value") == status) {
                $(this).addClass("active");
                $(this).siblings().removeClass("active");
                if (status == 'PROCESSING') {
                    $('#f_my_content .content1').show();
                    $('#f_my_content .content3').hide();
                    $('#f_my_content .content2').hide();
                } else if (status == 'ALREADY_PROCESSED') {
                    $('#f_my_content .content1').hide();
                    $('#f_my_content .content2').hide();
                    $('#f_my_content .content3').show();
                } else if (status == 'HANG_UP') {
                    $('#f_my_content .content1').hide();
                    $('#f_my_content .content3').hide();
                    $('#f_my_content .content2').show();
                }
                return false;
            }
        });

        var bizProcessingMapStr = '${bizProcessingMap}';
        var bizWaitProcessMapStr = '${bizWaitProcessMap}';
        if (bizProcessingMapStr != '') {
            var bizProcessingMap = eval('(' + bizProcessingMapStr.split('=').join(':') + ')');
            $("#bizMenu").find("span").each(function () {
                var bizName = $(this).attr("name");
                var count = bizProcessingMap[bizName];
                if (count != undefined) {
                    $(this).text("(" + count + ")");
                }
            })
        }
        if (bizWaitProcessMapStr != '') {
            var bizWaitProcessMap = eval('(' + bizWaitProcessMapStr.split('=').join(':') + ')');
            $(".tab_h_item").each(function () {
                var inputDom = $(this).find("input:eq(0)");
                var id = inputDom.attr("id");
                var count = bizWaitProcessMap["COUNT"][id];
                if (count != undefined) {
                    inputDom.parent().find("span").text("(" + count + ")");
                }

                var ulDom = inputDom.parent().next();
                ulDom.find("input").each(function () {
                    var child_input = $(this);
                    var taskType = child_input.attr("task_type");
                    var bizGroup = bizWaitProcessMap[id];
                    if (bizGroup != undefined) {
                        var child_count = bizGroup[taskType];
                        if (child_count != undefined) {
                            child_input.parent().find("span").text("(" + child_count + ")");
                        }
                    }
                })
            })

        }

    });


</script>
