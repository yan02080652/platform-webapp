<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="webpage/flight/tmc/css/reset.css?version=${globalVersion}" type="text/css">
<link rel="stylesheet" href="webpage/flight/tmc/css/orderDetail.css?version=${globalVersion}" type="text/css">
<style>
	.m-table-wrapper{
		width: 50%;
		padding: 0 20px;
	}
	.m-detail-table{
		width: 100%;
		text-align: center;
	}
	.m-detail-table thead tr th{
		border: 1px #aaa solid;
		text-align: center;
		padding: 8px;
		background: #fff;
	}
	.m-detail-table tbody tr td{
		border: 1px #aaa solid;
		border-bottom: 0;
		border-right: 0;
		padding: 8px;
		background: #fff;
	}
	.m-detail-table tbody tr td:last-child{
		border-right: 1px #aaa solid;
	}
	.m-detail-table tbody tr:last-child td{
		border-bottom: 1px #aaa solid;
	}
</style>
<div class="hoc-order-content l_float">
	<input type="hidden" value="${refundOrderDto.orderId }" id="orderId"/>
	<input type="hidden" value="${refundOrderDto.id }" id="refundOrderId"/>
	<input type="hidden" value="${refundOrderDto.userId }" id="userId"/>
	<input type="hidden" value="${taskId}" id="taskId"/>
	<div class="hoc-cont-cont">

		<div class="m-table-wrapper">
			<table class="m-detail-table">
				<thead>
				<th>姓名</th>
				<th>票号</th>
				<th>行程单</th>
				</thead>
				<c:forEach items="${refundOrderDto.orderDto.passengers }" var="passenger">
					<c:if test="${passenger.refundOrderId eq refundOrderDto.id and passenger.status eq 'REFUND_IN_REVIEW' }">
						<tr>
							<div >
								<td><div>${passenger.name }</div></td>
								<td><span>${passenger.ticketNo }</span></td>
								<td><span>行程单: 已打印,已回收</span></td>
							</div>
						</tr>
					</c:if>
				</c:forEach>
			</table>
		</div>

		<div class="cont-left">

			<c:forEach items="${refundOrderDto.orderDto.orderFlights}" var="detail">

				<p class="cont-left-1">
					<span class="l_float">${detail.carrierName }&nbsp;</span>
					<span>${detail.flightNo }</span>
					<c:if test="${not empty detail.realFlightNo}">
						<span>(实际承运: ${detail.shareCarrier.nameShort}  ${detail.realFlightNo })</span>
					</c:if>

					<span><fmt:formatDate value="${detail.fromDate }" pattern="yyyy.MM.dd HH:mm" /></span>
				</p>
				<p class="cont-left-1">
					<span>${detail.fromCityName }(${detail.fromAirport })</span>
					<span>-</span>
					<span>${detail.toCityName }(${detail.toAirport })</span>
					<span>${cabin[detail.cabinClass] }
					(全价：${detail.cabinFullPrice })
					${detail.cabinClass }
					(票面：${detail.facePrice})
				</span>
				</p>



				<div class="hoc-cut-off"></div>
				<div class="backOrChange">
					<c:if test="${detail.refundChangeDetail.type eq 1}">
						<table>
							<tbody>
							<tr>
								<td></td>
								<c:forEach items="${detail.refundChangeDetail.headers}" var="head">
									<td>${head}</td>
								</c:forEach>
							</tr>
							<tr>
								<td>退票费</td>
								<c:forEach items="${detail.refundChangeDetail.refundAmountList}" var="amonut">
									<c:choose>
										<c:when test="${amonut > 0}">
											<td>¥ <span class="bOrCMoney">${amonut}</span>/人</td>
										</c:when>
										<c:otherwise>
											<td>免费退票</td>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</tr>
							<tr>
								<td>同舱改期费</td>
								<c:forEach items="${detail.refundChangeDetail.changeAmountList}" var="amonut">
									<c:choose>
										<c:when test="${amonut > 0}">
											<td>¥ <span class="bOrCMoney">${amonut}</span>/人</td>
										</c:when>
										<c:otherwise>
											<td>免费改期</td>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</tr>
							<tr>
								<td>签转条件</td>
								<td colspan="${fn:length(detail.refundChangeDetail.headers)}">${detail.refundChangeDetail.endorseRule}</td>
							</tr>
							<tr>
								<td>行李额</td>
								<td colspan="${fn:length(detail.refundChangeDetail.headers)}">${detail.refundChangeDetail.baggage}</td>
							</tr>
							<tr>
								<td>备注</td>
								<td colspan="${fn:length(detail.refundChangeDetail.headers)}">${detail.refundChangeDetail.remark}</td>
							</tr>
							</tbody>
						</table>
					</c:if>
					<c:if test="${detail.refundChangeDetail.type eq 2}">
						<table>
							<tbody>
							<tr>
								<td>退票费</td>
								<td>${detail.refundChangeDetail.headers[0]}</td>
							</tr>
							<tr>
								<td>同舱改期费</td>
								<td>${detail.refundChangeDetail.headers[1]}</td>
							</tr>
							<tr>
								<td>签转条件</td>
								<td>${detail.refundChangeDetail.endorseRule}</td>
							</tr>
							<tr>
								<td>行李额</td>
								<td>${detail.refundChangeDetail.baggage}</td>
							</tr>
							<tr>
								<td>备注</td>
								<td>${detail.refundChangeDetail.remark}</td>
							</tr>
							</tbody>
						</table>
					</c:if>
					<c:if test="${detail.refundChangeDetail.type eq 3}">
						<table>
							<tbody>
							<tr>
								<td>退票说明</td>
								<td class="tdInfo">${detail.refundChangeDetail.headers[0]}</td>
							</tr>
							<tr>
								<td>改期说明</td>
								<td class="tdInfo">${detail.refundChangeDetail.headers[1]}</td>
							</tr>
							<tr>
								<td>签转说明</td>
								<td class="tdInfo">${detail.refundChangeDetail.headers[2]}</td>
							</tr>
							</tbody>
						</table>
					</c:if>
				</div>

			</c:forEach>



			<div class="hoc-cut-off"></div>
			<input type="hidden" value="${refundOrderDto.refundCount }" id="refundCount" />
			<form id="priceForm">
				<div class="form-table">
					<table>
						<tbody>
						<tr>
							<th class="first-th"></th>
							<th>销售票价</th>
							<th>供应商退票费</th>
							<th>额外调整</th>
							<th>服务费</th>
							<th>应退客户</th>
						</tr>
						<tr>
							<td>每人</td>
							<td id="salePrice">${refundOrderDto.orderDto.orderFee.salePrice + refundOrderDto.orderDto.orderFee.departureTax + refundOrderDto.orderDto.orderFee.fuelTax + refundOrderDto.orderDto.orderFee.upgradePrice + refundOrderDto.orderDto.orderFee.rescheduledPrice }</td>
							<td class="input-num"><input type="text"  onkeyup="calculation(this,td1)" id="num1" class="inputNum" value="0"></td>
							<td class="input-num"><input type="text"  onkeyup="calculation(this,td2)" id="num2" class="inputNum" value="0"></td>
							<td class="input-num"><input type="text"  onkeyup="calculation(this,td3)" id="num3" class="inputNum" value="${serverCharge}"></td>
							<td class="input-num"><input type="text" id="refundPrice" value="${refundOrderDto.orderDto.orderFee.salePrice + refundOrderDto.orderDto.orderFee.departureTax + refundOrderDto.orderDto.orderFee.fuelTax + refundOrderDto.orderDto.orderFee.upgradePrice + refundOrderDto.orderDto.orderFee.rescheduledPrice }"></td>
						</tr>
						<tr>
							<td>合计</td>
							<td id="td0">${(refundOrderDto.orderDto.orderFee.salePrice + refundOrderDto.orderDto.orderFee.departureTax + refundOrderDto.orderDto.orderFee.fuelTax + refundOrderDto.orderDto.orderFee.upgradePrice + refundOrderDto.orderDto.orderFee.rescheduledPrice )*refundOrderDto.refundCount }</td>
							<td id="td1">0</td>
							<td id="td2">0</td>
							<td id="td3">${serverCharge}</td>
							<td id="td4">${(refundOrderDto.orderDto.orderFee.salePrice + refundOrderDto.orderDto.orderFee.departureTax + refundOrderDto.orderDto.orderFee.fuelTax + refundOrderDto.orderDto.orderFee.upgradePrice + refundOrderDto.orderDto.orderFee.rescheduledPrice  )*refundOrderDto.refundCount}</td>
						</tr>
						</tbody>
					</table>
					<div class="hoc-button-wards">
						<a class="hoc-bt hoc-pass-bt" onclick="aduitPass()">审核通过</a>
						<a class="hoc-bt hoc-refuse-bt" onclick="refuseRefund()">拒绝退票</a>
						<a class="hoc-bt hoc-refuse-bt" onclick="cancel()">暂不处理</a>
					</div>
				</div>
			</form>
		</div>
		<div class="cont-right imgBox">
			<div class="cont-info">
				<div class="cont-info-label l_float">原订单号</div>
				<div class="cont-info-in l_float">
					<p>${refundOrderDto.orderDto.id }</p>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">支付时间</div>
				<div class="cont-info-in l_float">
					<span class="data-1"><fmt:formatDate value="${refundOrderDto.orderDto.paymentDate }" pattern="yyyy.MM.dd HH:mm:ss"/></span>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">出票时间</div>
				<div class="cont-info-in l_float">
					<span class="data-1"><fmt:formatDate value="${refundOrderDto.orderDto.lastUpdateDate }" pattern="yyyy.MM.dd HH:mm:ss"/></span>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">预订人</div>
				<div class="cont-info-in l_float">
					<span class="data-1">${refundOrderDto.orderDto.userName }</span>
					<span class="data-1">${refundOrderDto.orderDto.userMobile }</span>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">出票渠道</div>
				<div class="cont-info-in l_float">
					<p>${issueChannelMap[refundOrderDto.orderDto.issueChannel] }</p>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">PNR</div>
				<div class="cont-info-in l_float">
					<p>${refundOrderDto.orderDto.pnrCode }</p>
				</div>
			</div>
			<div class="hoc-cut-off"></div>
			<div class="cont-info">
				<div class="cont-info-label l_float">退单单号</div>
				<div class="cont-info-in l_float">
					<p>${refundOrderDto.id }</p>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">提交时间</div>
				<div class="cont-info-in l_float">
					<span class="data-1"><fmt:formatDate value="${refundOrderDto.applyTime }" pattern="yyyy.MM.dd HH:mm:ss"/></span>
				</div>
			</div>
			<div class="cont-info">
				<div class="cont-info-label l_float">退票原因</div>
				<div class="cont-info-in l_float">
					<p>${refundOrderDto.reason }</p>
				</div>
			</div>
			<c:if test="${not empty refundOrderDto.imageUrls }">
				<div class="cont-info">
					<div class="cont-info-label l_float">附件</div>
					<div class="cont-info-in l_float">
						<!--处理多张图片字符串 -->
						<c:set value="${ fn:split(refundOrderDto.imageUrls, ',') }" var="urls" />

						<div id="pic1" style="display: none; position: absolute; z-index: 100;"></div>
						<c:forEach items="${urls}" var="url">
							<p class="enclosure">
								<span class="img" onmouseout="hiddenPic();"  onmousemove="showPic(event,'http://${SYS_CONFIG['imgserver'] }/${url }');">图片</span>
								<span onclick="download('http://${SYS_CONFIG['imgserver'] }/${url }')">下载</span>
							</p>
						</c:forEach>
					</div>
				</div>
			</c:if>
			<!-- <div class="cont-info">
				<div class="cont-info-label l_float">情况说明</div>
				<div class="cont-info-in l_float">
					<p>心情不美丽</p>
				</div>
			</div> -->
			<div class="cont-info">
				<div class="cont-info-label l_float">联系人</div>
				<div class="cont-info-in l_float">
					<span class="data-1">${refundOrderDto.orderDto.contactor.name }</span>
					<span class="data-1">${refundOrderDto.orderDto.contactor.mobile }</span>
					<span class="data-1">${refundOrderDto.orderDto.contactor.email }</span>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="webpage/flight/tmc/js/orderDetail.js?version=${globalVersion}"></script>
