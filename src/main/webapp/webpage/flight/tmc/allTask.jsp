<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .tooltip {
        width: 24em;
    }

    .tab li {
        width: 248px;
    }

    .content3 p {
        margin-left: 600px;
        text-align: inherit;
    }

    .content4 p {
        text-align: right;
    }

</style>
<input type="hidden" id="currentTaskType"/>

<div>
    <!-- 状态栏 -->
    <div class="tab pull-left">
        <ul class="fix" id="allTaskStatusTab">
            <li class="tab1 active" data-value="WAIT_PROCESS" onclick="reloadAllTask(null ,'WAIT_PROCESS')">
                待处理(${waitTaskTypeMap.WAIT_PROCESS==null?0:waitTaskTypeMap.WAIT_PROCESS})
            </li>
            <li class="tab2" data-value="PROCESSING" onclick="reloadAllTask(null,'PROCESSING')">
                处理中(${processingMap.PROCESSING==null?0:processingMap.PROCESSING})
            </li>
            <li class="tab3" data-value="HANG_UP" onclick="reloadAllTask(null,'HANG_UP')">
                挂起中(${hangUpMap.HANG_UP==null?0:hangUpMap.HANG_UP})
            </li>
            <li class="tab4" data-value="ALREADY_PROCESSED" onclick="reloadAllTask(null,'ALREADY_PROCESSED')">
                近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})
            </li>
        </ul>
        <div id="f_all_content">
            <div class="content1">
                <p>
                    <a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_TICKET')">待出票(${waitTaskTypeMap.WAIT_TICKET==null?0:waitTaskTypeMap.WAIT_TICKET })</a>
                    <a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_URGE')">待催单(${waitTaskTypeMap.WAIT_URGE==null?0:waitTaskTypeMap.WAIT_URGE})</a>
                    <a onclick="reloadAllTask(null,'WAIT_PROCESS','REFUND_TICKET_AUDIT')">退审核(${waitTaskTypeMap.REFUND_TICKET_AUDIT==null?0:waitTaskTypeMap.REFUND_TICKET_AUDIT})</a>
                    <a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_CHANGE')">待改签(${waitTaskTypeMap.WAIT_CHANGE==null?0:waitTaskTypeMap.WAIT_CHANGE})</a>
                </p>
            </div>
            <div class="content2">
                <p style="margin-left: 243px;">
                    <a onclick="reloadAllTask(null,'PROCESSING','WAIT_TICKET')">待出票(${processingMap.WAIT_TICKET==null?0:processingMap.WAIT_TICKET })</a>
                    <a onclick="reloadAllTask(null,'PROCESSING','WAIT_URGE')">待催单(${processingMap.WAIT_URGE==null?0:processingMap.WAIT_URGE})</a>
                    <a onclick="reloadAllTask(null,'PROCESSING','REFUND_TICKET_AUDIT')">退审核(${processingMap.REFUND_TICKET_AUDIT==null?0:processingMap.REFUND_TICKET_AUDIT})</a>
                    <a onclick="reloadAllTask(null,'PROCESSING','WAIT_CHANGE')">待改签(${processingMap.WAIT_CHANGE==null?0:processingMap.WAIT_CHANGE})</a>
                </p>
            </div>
            <div class="content3">
                <p>
                    <a onclick="reloadAllTask(null,'HANG_UP','WAIT_CHANGE')">改签(${hangUpMap.WAIT_CHANGE==null?0:hangUpMap.WAIT_CHANGE })</a>
                </p>
            </div>
            <div class="content4">
                <p>&nbsp;&nbsp;&nbsp;
                    <a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_TICKET')">出票(${alreadyMap.WAIT_TICKET==null?0:alreadyMap.WAIT_TICKET })</a>
                    <a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_URGE')">催单(${alreadyMap.WAIT_URGE==null?0:alreadyMap.WAIT_URGE})</a>
                    <a onclick="reloadAllTask(null,'ALREADY_PROCESSED','REFUND_TICKET_AUDIT')">审核(${alreadyMap.REFUND_TICKET_AUDIT==null?0:alreadyMap.REFUND_TICKET_AUDIT})</a>
                    <a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_CHANGE')">改签(${alreadyMap.WAIT_CHANGE==null?0:alreadyMap.WAIT_CHANGE})</a>
                </p>
            </div>
        </div>
    </div>
    <!-- 任务类型 -->
    <input type="hidden" id="task_type" value="WAIT_PROCESS"/>
    <input type="hidden" id="task_status"/>

    <!-- 搜索条件 -->
    <div>
        <form class="form-inline" method="post" id="orderSearchForm">
            <nav>
                <div class=" form-group">
                    <input id="search_Field2" type="text" class="form-control" placeholder="订单号/处理人"
                           style="width: 300px"/>
                    <button type="button" class="btn btn-default" onclick="reloadMyTaskTable()">搜索</button>
                    <button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
                </div>
            </nav>
        </form>
    </div>
    <div>
        <label><font color="red" size="4">刷新倒计时:<span id="countDown">9</span>秒</font></label>
    </div>
</div>


<!-- 任务列表 -->
<div style="margin-top: 10px">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th style="width: 38%">航班信息</th>
            <%--<th style="width: 19%">乘客信息</th>--%>
            <th style="width: 19%">收支信息</th>
            <th style="width: 19%">处理信息</th>
            <th style="width: 19%">供应信息</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${pageList.list }" var="orderTask">
            <tr>
                <td colspan="5">
                    <span>企业：${orderTask.orderDto.cName } </span>
                    <span>预订人：${orderTask.orderDto.userName }</span>
                    <span>订单号：<font color="blue"><a href="/order/flightDetail/${orderTask.orderDto.id }"
                                                    target="_Blank">${orderTask.orderDto.id }</a></font></span>
                    <span>来源:${orderTask.orderDto.orderOriginType.message}</span>
                    <span>标签：${orderTask.orderDto.orderShowStatus.message }</span>
                    <span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                                value="${orderTask.orderDto.createDate }"/></span>
                    <span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span>
                </td>
            </tr>
            <tr>
                <td>
                    <c:forEach items="${orderTask.orderDto.orderFlights}" var="detail">

						<span style="display: inline-block;margin-bottom: 5px">
                             ${detail.detailMessages} <br>
						</span>


                    </c:forEach>
                    <br>
                    <span style="display: inline-block;margin-top: 15px">
						出行人: ${orderTask.userNames}
					</span>
                    <span style="float: right;display: inline-block;margin-top: 15px">
						<a data-trigger="tooltip" data-content="${orderTask.orderDto.ssInstructionsType1 }" class="ss"
                           data-clipboard-text="${orderTask.orderDto.ssInstructionsType2 }">点击复制SS</a>
					</span>
                        <%--<span>--%>
                        <%--<fmt:formatDate value="${orderTask.orderDto.orderFlights[0].fromDate }" pattern="yyyy-MM-dd HH:mm"/> ---%>
                        <%--<fmt:formatDate value="${orderTask.orderDto.orderFlights[0].toDate }" pattern="HH:mm"/></span><br/>--%>
                        <%--<span>${orderTask.orderDto.orderFlights[0].fromCityName }(${orderTask.orderDto.orderFlights[0].fromAirportName }${orderTask.orderDto.orderFlights[0].fromTerminal })--%>
                        <%--</span>---%>
                        <%--<span>${orderTask.orderDto.orderFlights[0].toCityName }--%>
                        <%--(${orderTask.orderDto.orderFlights[0].toAirportName }${orderTask.orderDto.orderFlights[0].toTerminal })</span><br/>--%>
                        <%--<span>${orderTask.orderDto.orderFlights[0].flightNo } ${cabinClassMap[orderTask.orderDto.orderFlights[0].cabinClass ]} &nbsp;${orderTask.orderDto.orderFlights[0].cabinCode }</span>--%>
                        <%--<span>--%>
                        <%--<a data-trigger="tooltip" data-content="${orderTask.orderDto.ssInstructionsType1 }" class="ss" data-clipboard-text="${orderTask.orderDto.ssInstructionsType2 }">SS</a>--%>
                        <%--</span>--%>
                </td>

                    <%--<td>--%>
                    <%--<span>${orderTask.userNames}</span>--%>
                    <%--</td>--%>

                <td>
                    <span>应收￥${orderTask.orderDto.orderFee.totalOrderPrice }</span>
                    <span>
							<c:choose>
								<c:when test="${orderTask.orderDto.paymentStatus eq 'PAYMENT_SUCCESS' }">已收</c:when>
								<c:otherwise>未收</c:otherwise>
							</c:choose>
						</span><br/>
                    <c:if test="${not empty orderTask.orderDto.orderFee.totalSettlePrice}">
                        <span>应付￥${orderTask.orderDto.orderFee.totalSettlePrice}</span>
                        <span>
								<c:choose>
									<c:when test="${not empty orderTask.orderDto.externalOrderStatus and orderTask.orderDto.externalOrderStatus eq 'WAIT_ISSUE' or orderTask.orderDto.externalOrderStatus eq 'ISSUED' or orderTask.orderDto.externalOrderStatus eq 'PAYING'}">
										已付
									</c:when>
									<c:otherwise>未付</c:otherwise>
								</c:choose>
							</span><br/>
                    </c:if>
                    <c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
                        <span><a
                                onclick="loadIncomeDetail('${orderTask.orderDto.id}','${orderTask.orderDto.paymentPlanNo}')">明细</a></span>
                    </c:if>
                </td>
                <td>
                    <span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>
                    <c:if test="${not empty orderTask.operatorId }">
                        <span>处理人：${orderTask.operatorName }</span><br/>
                    </c:if>
                    <c:if test="${orderTask.taskStatus eq 'WAIT_PROCESS' }">
                        <span><a
                                onclick="transfer('${orderTask.id}','${orderTask.operatorId }',false)">分配</a></span><br/>
                    </c:if>
                    <c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' }">
                        <span>处理结果：${orderTask.result }</span><br/>
                    </c:if>
                    <c:if test="${not empty orderTask.orderDto.customerCode}">
                        <span style="color: red">大客户编码：${orderTask.orderDto.customerCode }</span>
                    </c:if>
                </td>
                <td>
						<span>
							<a data-trigger="tooltip"
                               data-content="原因：${orderTask.orderDto.orderOperationLog.description }">
								<font color="red">${orderTask.orderDto.orderOperationLog.type.message }${orderTask.orderDto.orderOperationLog.status.message }</font>
							</a><br/>

							<c:choose>
                                <c:when test="${not empty orderTask.orderDto.issueChannel }">
                                    ${issueChannelMap[orderTask.orderDto.issueChannel] }
                                </c:when>
                                <c:otherwise>
                                    ${issueChannelMap[orderTask.orderDto.hubsConnectDto.issueChannelCode] }
                                </c:otherwise>
                            </c:choose>

                            <!-- 有供应商订单信息 -->
							<c:if test="${not empty orderTask.orderDto.externalOrderId }">
                                <a data-trigger="tooltip"
                                   data-content="${orderTask.orderDto.externalOrderId }">订单号</a><br/>
                            </c:if>
							<c:if test="${not empty orderTask.orderDto.pnrCode }">
                                PNR:${orderTask.orderDto.pnrCode }
                            </c:if>
							<c:if test="${not empty orderTask.orderDto.externalOrderStatusMessage }">
                                状态:${orderTask.orderDto.externalOrderStatusMessage } <br/>
                            </c:if>
								<c:if test="${orderTask.ticketType eq 'OFFLINE' and orderTask.orderDto.supplierLiqStatus ne 'CLOSE'}">
                                    <a onclick="ticketPopup(${orderTask.orderDto.id})">线下出票修订</a>
                                </c:if>
						</span>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div class="pagin" style="margin-bottom: 20px">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="webpage/flight/tmc/js/allTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
    $(function () {
        var status = "${taskStatus}";
        $("#allTaskStatusTab li").each(function () {
            if ($(this).attr("data-value") == status) {
                $(this).addClass("active");
                $(this).siblings().removeClass("active");
                if (status == 'WAIT_PROCESS') {
                    $('#f_all_content .content1').show();
                    $('#f_all_content .content2').hide();
                    $('#f_all_content .content3').hide();
                    $('#f_all_content .content4').hide();
                } else if (status == 'PROCESSING') {
                    $('#f_all_content .content1').hide();
                    $('#f_all_content .content2').show();
                    $('#f_all_content .content3').hide();
                    $('#f_all_content .content4').hide();
                } else if (status == 'HANG_UP') {
                    $('#f_all_content .content1').hide();
                    $('#f_all_content .content2').hide();
                    $('#f_all_content .content3').show();
                    $('#f_all_content .content4').hide();
                } else if (status == 'ALREADY_PROCESSED') {
                    $('#f_all_content .content1').hide();
                    $('#f_all_content .content2').hide();
                    $('#f_all_content .content3').hide();
                    $('#f_all_content .content4').show();
                }
                return false;
            }
        });
    });
</script>
