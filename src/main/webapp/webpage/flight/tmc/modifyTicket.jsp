<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" id="modifyTicket_model">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">
                    修订出票信息
                </h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="ticketForm" role="form">
                    <input type="hidden" value="${order.id }" id="orderId"/>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">出票渠道<span><font color="red">*</font></span></label>
                        <div class="col-sm-4">
                            <select class="form-control" id="issueTicketChannel">
                                <option value="">----</option>
                                <c:forEach items="${channelList }" var="channel">
                                    <option value="${channel.code }" <c:if test="${order.issueChannel eq channel.code}">selected</c:if>>${channel.name }</option>
                                </c:forEach>
                            </select>
                        </div>

                        <label class="col-sm-2 control-label">PNR</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="pnr" value="${order.pnrCode }"
                                   placeholder="请输入6位PNR编码" maxlength="6"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">供应单号</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" value="${order.externalOrderId }"
                                   id="externalOrderId" placeholder="请输入外部平台订单号"/>
                        </div>
                        <label class="col-sm-2 control-label">应付总价<span><font color="red">*</font></span></label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control" value="${order.orderFee.totalSettlePrice }"
                                   id="totalPrice" placeholder="请输入采购结算总价"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <table class="table table-bordered" id="passengerTable">
                                <thead>
                                <tr>
                                    <th>姓名</th>
                                    <th>票号</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${order.passengers }" var="passenger">
                                    <tr>
                                        <td>${passenger.name }</td>
                                        <td>
                                            <input type="text" class="form-control" data-value="${passenger.id }"
                                                   value="${passenger.ticketNo}"/>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="modify()">确认修改</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    function checkReg(value) {
        var reg1 = /^[0-9]{13}$/;
        var reg2 = /^[0-9]{3}-[0-9]{10}$/;
        return reg1.test(value) | reg2.test(value);
    }

    function modify() {
        var issueChannel = $("#issueTicketChannel").val();
        var totalPrice = $("#totalPrice").val();

        if (issueChannel == null || issueChannel == '') {
            layer.msg("出票渠道必填!", {time: 1000})
            return;
        }
        if (totalPrice == null || totalPrice == '') {
            layer.msg("结算总价必填！", {time: 1000})
            return;
        }

        var orderId = $("#orderId").val();
        var pnr = $("#pnr").val();
        var externalOrderId = $("#externalOrderId").val();

        var passengers = [];
        var hasErrTicket = false;
        var pname;
        $("#passengerTable tbody tr").each(function (i, dom) {
            var name = $(this).find("td:eq(0)").text();
            var no = $(this).find("td:eq(1) input").val();
            var id = $(this).find("td:eq(1) input").attr("data-value");
            if (!checkReg(no)) {
                hasErrTicket = true;
                pname = name;
                return false;
            }
            var p = {
                id: id,
                ticketNo: no
            }
            passengers.push(p);
        })

        if (hasErrTicket) {
            layer.msg(pname + "的票号格式不正确！", {time: 1000})
            return;
        }

        var formObj = {
            orderId: orderId,
            issueChannel: issueChannel,
            pnr: pnr,
            externalOrderId: externalOrderId,
            totalPrice: totalPrice,
            passengerDtos: passengers
        };

        var layer_index = layer.load(0);
        $.ajax({
            type: "post",
            url: "flight/tmc/modifyTicket",
            data: JSON.stringify(formObj),
            contentType: "application/json",
            datatype: "json",
            success: function (result) {
                layer.close(layer_index);
                if (result.code == '1') {
                    $('#modifyTicket_model').modal('hide');
                    layer.msg("修改成功！", {icon: 1, offset: 't', time: 1000});
                } else {
                    layer.msg(result.data, {icon: 2, offset: 't', time: 1000})
                }
            }
        })
    }
</script>