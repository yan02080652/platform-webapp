<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" href="resource/plugins/layer/skin/layui.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/bootstrap/fileinput.min.css?version=${globalVersion}">
<style>
    .otherExpense {
        height: 28px;
        padding-left: 8px;
        list-style: none;
        line-height: 28px;
        font-size: 14px;
        color: #46a1bb;
        cursor: pointer;
    }

    /*树弹窗*/
    #expenseDetail {
        margin-left: 30px;
    }

    .otherExpenseH {
        display: none;
        color: #ccc;
    }

    /*对树追加圆点的操作 小圆点*/
    .activePoint {
        color: #3e3a39;
        cursor: pointer;
    }

    .activeTree {
        color: #46a1bb;
    }

    .activePoint:hover {
        color: #46a1bb;
    }

    .layui-tree li a {
        /*cursor: not-allowed;*/
    }

    cite {
        position: relative;
        /*cursor: not-allowed;*/
    }

    .costUnit {
        position: absolute;
        z-index: 99;
        width: 8px;
        height: 8px;
        background-color: #1176CC;
        border-radius: 50%;
        right: -4px;
        top: 4px;
    }

    cite:hover .costUnit:after {
        border: 1px solid #cccccc;
        font-size: 12px;
        content: '成本核算单元';
        position: absolute;
        background-color: #FFFFFF;
        z-index: 2147483584;
        margin-top: -6px;
        margin-left: 10px;
        padding: 2px;
        height: 14px;
        line-height: 14px;
    }

    .item_pull {
        float: left;
        height: 30px;
        line-height: 28px;
        text-align: left;
        background-color: #ffffff;
        position: relative;
    }

    .item_div {
        width: 230px;
        height: 34px;
        padding-left: 8px;
        background: url('/resource/image/downGray.png') no-repeat 210px;
        border: 1px solid #aaaaaa;
        cursor: pointer;
        border-radius: 4px;
    }

    .ul_b {
        display: none;
        background: #ffffff;
        position: absolute;
        z-index: 999;
        border: 1px solid #bbbbbb;
        border-top: none;
        cursor: pointer;
    }

    .ul_b .otherExpense {
        width: 230px;
        padding-left: 8px;
        color: #1176CC;
    }

    .item_ul > li {

        height: 28px;
        padding-left: 8px;
        list-style: none;
    }

    .item_ul > li:hover {
        background: #46a1bb;
        color: #FFFFFF;
        border-color: #46a1bb;
    }
</style>

<div class="modal fade" id="ticket_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content" style="width: 800px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">
                    填写核价信息
                </h4>
            </div>
            <div style="border-bottom: 1px #ddd solid">
                <div class="row" style="padding-left: 45px;padding-top: 10px">
                    订单号：${orderDto.id}
                </div>
                <div class="row">
                    <div class="col-sm-10" style="margin:16px 30px 16px;">
                        行程信息：
                        <fmt:formatDate value="${orderDto.details[0].fromDate }"
                                        pattern="yyyy-MM-dd"/> ${fn:substring(orderDto.details[0].fromTime,0,2)}:${fn:substring(orderDto.details[0].fromTime,2,6)}
                        -
                        ${fn:substring(orderDto.details[0].toTime,0,2)}:${fn:substring(orderDto.details[0].toTime,2,6)}</span>
                        <br/>

                        ${orderDto.detailsMsg}
                        </br>
                        <c:forEach items="${orderDto.details}" var="detail" varStatus="index">
								<span>
									<c:if test="${index.last}">
										${detail.flightNo } &nbsp;${detail.cabinCode }
									</c:if>
									<c:if test="${!index.last}">
										${detail.flightNo } &nbsp;${detail.cabinCode } -
									</c:if>

								</span>
                        </c:forEach>

                    </div>
                    <div class="col-sm-12" style="padding-left: 45px;padding-bottom: 10px">
                        乘机人:
                        ${orderDto.passengerNameEns}
                    </div>
                </div>
            </div>
            <form class="form-horizontal" id="offerForm" method="post" action="internationalflight/tmc/offer">
                <div class="modal-body">

                    <input type="hidden" name="taskId" value="${taskId }"/>
                    <input type="hidden" name="orderId" value="${orderDto.id }">
                    <div class="row">
                        <div class="col-sm-6" id="internationalFlight_offer_from" style="border-right: 1px #ddd solid">

                            <div class="form-group">
                                <label class="col-sm-4 control-label">单人票价</label>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control price" name="singleTicketPirce"
                                           value="${orderDto.singleTicketPrice }"/>
                                </div>
                                <div class="col-sm-2" style="padding-top: 7px">元</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">单人税费</label>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control price" name="singleTaxFee"
                                           value="${orderDto.singleTaxFee }"/>
                                </div>
                                <div class="col-sm-2" style="padding-top: 7px">元</div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">单人服务费</label>
                                <div class="col-sm-6">
                                    <input type="number" class="form-control price" name="singleServiceFee"
                                           value="${orderDto.singleExtraFee }"/>
                                </div>
                                <div class="col-sm-2" style="padding-top: 7px">元</div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group" style="margin-top: 20px; padding-left: 100px;">
                                单人总价：￥<span id="singleTotalPrice">0</span>
                            </div>
                            <div class="form-group" style="padding-left: 100px;">
                                人数：<span id="pNum">${fn:length(orderDto.passengers)}</span>
                            </div>
                            <div class="form-group" style="padding-left: 100px;">
                                <font color="#F79134">订单总额：￥<span id="totalPrice">0</span></font>
                            </div>
                        </div>
                    </div>


                    <div class="modal-footer">

                        <div style="float: left">
                            <div>
                                <input type="checkbox" name="autoAppoint" checked> <font
                                    color="#F79134">此订单出票任务直接指派给我</font>
                            </div>
                            <div style="float: left">
                                <input type="checkbox" name="sendPaySms" checked> <font color="#F79134">确认发送催支付短信</font>
                            </div>
                        </div>

                        <button type="button" class="btn btn-default" onclick="submitOfferForm()">保存</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
            </form>
        </div>

    </div>
</div>
<div class="otherExpenseH">
    <ul id="expenseDetail"></ul>
</div>
<script src="resource/plugins/layer/layui.js?version=${globalVersion}"></script>
<script src="resource/plugins/layer/tree.js?version=${globalVersion}"></script>
<script src="resource/js/tr-widget/expenseAttribution.js?version=${globalVersion}"></script>
<script type="text/javascript">

    //取两位小数
    function toDecimal2(x) {
        var f = parseFloat(x);
        if (isNaN(f)) {
            return '0.00';
        }
        var f = Math.round(x * 100) / 100;
        var s = f.toString();
        var rs = s.indexOf('.');
        if (rs < 0) {
            rs = s.length;
            s += '.';
        }
        while (s.length <= rs + 2) {
            s += '0';
        }
        return s;
    }

    //计算页面价格 显示
    function calculationPrice() {

        if ($("input[name='singleTicketPirce']").val() == '') {
            return;
        }
        if ($("input[name='singleTaxFee']").val() == '') {
            return;
        }
        if ($("input[name='singleServiceFee']").val() == '') {
            return;
        }

        var singleTicketPrice = parseFloat($("input[name='singleTicketPirce']").val());
        var singleTaxFee = parseFloat($("input[name='singleTaxFee']").val());
        var singleExtraFee = parseFloat($("input[name='singleServiceFee']").val());

        var passengerNum = parseInt($("#pNum").text());


        var singelTotalPrice = singleTicketPrice + singleTaxFee + singleExtraFee;
        var totalPrice = passengerNum * (singleTicketPrice + singleTaxFee + singleExtraFee);

        $("#singleTotalPrice").text(toDecimal2(singelTotalPrice));
        $("#totalPrice").text(toDecimal2(totalPrice));

    }

    $(".price").blur(function () {
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });

    calculationPrice();
</script>