<%@ taglib prefix="fms" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<link rel="stylesheet" href="resource/css/order/interOrderDetail.css?version=${globalVersion}">

<style>

    table {
        width: 500px;
        border-collapse: collapse;
    }

    table, tr, td {
        border: 1px solid #0078c9;
    }

    tr {
        line-height: 36px;
    }

    td {
        padding-left: 10px;
    }

    .wrapper {
        width: 545px;
        font-size: 14px;
        display: none;
    }

    .wrapper > div {
        display: flex;
        width: 100%;
    }

    .wrapper .left {
        display: flex;
        flex-direction: column;
        justify-content: center;
        width: 55px;
        text-align: center;
        border: 1px solid #0078c9;
        border-right: none;
    }

    .bag .left {
        border-top: none;
    }

    .quit-rule:hover .wrapper {

        position: absolute;
        display: block;
        left: 0;
        top: 31px;
        padding: 15px;
        display: inline-block;
        z-index: 999;
        /*width: 660px;*/
        color: #666;
        border: 1px solid #46a1bb;
        background-color: #fff;

    }

</style>

<div class="inter-detail-wrapper">
    <div class="header-nav">
        <ul>
            <li class="active" data-id="order-info">订单信息</li>
            <li data-id="travel-info">行程信息</li>
            <li data-id="passenger-info">乘客信息</li>
            <li data-id="illega-info" hidden="${hidden}">违规信息</li>
            <li data-id="provider-info">供应商信息</li>
        </ul>
    </div>
    <div id="order-info" class="order-info">
        <ul>
            <li class="order-info-title">
                <p class="title-header">
                    <span class="bold">国际机票</span>
                    <span>|&nbsp;&nbsp;${orderDto.tripType.msg}&nbsp;&nbsp;|</span>
                    <span>共${orderDto.passengers.size()}人</span>
                </p>
                <ol>
                    <c:choose>
                        <c:when test="${orderDto.travelType ne 'PERSONAL'}">
                            <li class="flight_ell">出行性质：因公</li>
                            <li class="flight_ell">出行类别：${travelType}</li>
                            <li class="flight_ell">出差事由：${orderDto.travelReason}</li>
                            <li class="flight_ell">费用归属：${orgName}</li>
                        </c:when>
                        <c:otherwise>
                            <li class="flight_ell">出行性质：因私</li>
                        </c:otherwise>
                    </c:choose>

                    <li>预订人员：${orderDto.userName}</li>
                </ol>
            </li>
            <li class="order-info-money">
                <p class="title">
                    <span class="money-title">总金额：</span>
                    <span class="money-num">&nbsp;&nbsp;&nbsp;CNY&nbsp;${orderDto.totalSalePrice.intValue()}</span>
                </p>
                <ol>
                    <li>单人票价：&nbsp;&nbsp;&nbsp;CNY<span>&nbsp;${orderDto.singleTicketPrice.intValue()}</span></li>
                    <li>单人税费：&nbsp;&nbsp;&nbsp;CNY<span>&nbsp;${orderDto.singleTaxFee.intValue()}</span></li>
                    <li>单人服务费：CNY<span>&nbsp;${orderDto.singleExtraFee.intValue()}</span></li>
                </ol>
                <div class="quit-rule">
                    <span>退改行李额说明</span>
                    <c:if test="${not empty stringentRule}">

                    <div class="wrapper">
                        <div>
                            <div class="left">退<br/>改<br/>签</div>
                            <table>
                                <tr>
                                    <td colspan="3">全程</td>
                                </tr>

                                <c:choose>
                                    <c:when test="${not empty stringentRule.refundAmountList}">
                                        <c:forEach items="${stringentRule.refundHeaders}" var="head" varStatus="index">
                                            <c:if test="${index.first}">
                                                <tr>
                                                    <td rowspan="${fn:length(stringentRule.refundHeaders)}">退票费</td>
                                                    <td>${head}</td>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] eq -1}">
                                                        <td>以航司为准</td>
                                                    </c:if>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] eq -2}">
                                                        <td>不可退改</td>
                                                    </c:if>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] ne -1 && stringentRule.changeAmountList[index.index] ne -2}">
                                                        <td>￥${stringentRule.changeAmountList[index.index]}/人</td>
                                                    </c:if>
                                                </tr>
                                            </c:if>
                                            <c:if test="${!index.first}">
                                                <tr>
                                                    <td>${head}</td>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] eq -1}">
                                                        <td>以航司为准</td>
                                                    </c:if>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] eq -2}">
                                                        <td>不可退改</td>
                                                    </c:if>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] ne -1 && stringentRule.changeAmountList[index.index] ne -2}">
                                                        <td>￥${stringentRule.changeAmountList[index.index]}/人</td>
                                                    </c:if>
                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                            <td rowspan="1">退票费</td>
                                            <td colspan="2">${stringentRule.refundRule}</td>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>

                                <tr>
                                    <td colspan="3">备注：${stringentRule.specialRefundRule}</td>
                                </tr>

                                <c:choose>
                                    <c:when test="${not empty stringentRule.changeHeaders}">
                                        <c:forEach items="${stringentRule.changeHeaders}" var="head" varStatus="index">
                                            <c:if test="${index.first}">
                                                <tr>
                                                    <td rowspan="${fn:length(stringentRule.changeHeaders)}">同舱改期费</td>
                                                    <td>${head}</td>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] eq -1}">
                                                        <td>以航司为准</td>
                                                    </c:if>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] eq -2}">
                                                        <td>不可退改</td>
                                                    </c:if>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] ne -1 && stringentRule.changeAmountList[index.index] ne -2}">
                                                        <td>￥${stringentRule.changeAmountList[index.index]}/人</td>
                                                    </c:if>
                                                </tr>
                                            </c:if>
                                            <c:if test="${!index.first}">
                                                <tr>
                                                    <td>${head}</td>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] eq -1}">
                                                        <td>以航司为准</td>
                                                    </c:if>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] eq -2}">
                                                        <td>不可退改</td>
                                                    </c:if>
                                                    <c:if test="${stringentRule.changeAmountList[index.index] ne -1 && stringentRule.changeAmountList[index.index] ne -2}">
                                                        <td>￥${stringentRule.changeAmountList[index.index]}/人</td>
                                                    </c:if>
                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                        <tr>
                                            <td rowspan="1">同舱改期费</td>
                                            <td colspan="2">${stringentRule.changeRule}</td>
                                        </tr>
                                    </c:otherwise>
                                </c:choose>

                                <tr>
                                    <td colspan="3">备注：${stringentRule.specialChangeRule}</td>
                                </tr>

                            </table>
                        </div>
                        <div class="bag">
                            <div class="left">行李额</div>
                            <table>
                                <c:forEach items="${baggageMap}" var="map">
                                    <tr>
                                        <td>
                                            <c:forEach items="${map.value}" var="city">
                                                ${city.fromCityName} - ${city.toCityName}&nbsp;&nbsp;
                                            </c:forEach>
                                        </td>
                                        <c:if test="${map.key.baggageType eq 1}">
                                            <td>无免费托运行李额</td>
                                        </c:if>
                                        <c:if test="${map.key.baggageType eq 2}">
                                            <td>托运： ${map.key.baggagePieces}件 ,每件 ${map.key.baggageWeight}公斤</td>
                                        </c:if>
                                        <c:if test="${map.key.baggageType eq 3}">
                                            <td>托运： ${map.key.baggageWeight}公斤</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>

                            </table>
                        </div>
                    </div>
                    </c:if>

                    <c:if test="${empty stringentRule}">
                        <div class="rule-box">
                            <div class="rule-content">
                                <div class="refund-content refund2">
                                    <div class="left-line line"></div>
                                    <div class="right-line line"></div>
                                    <div class="refund-title">退改规定</div>
                                    <div class="box">
                                        <c:choose>
                                            <c:when test="${isShare}">
                                                <div>
                                                    <div class="city">
                                                        <c:forEach items="${orderDto.details}" var="detail">
                                                            <div>${detail.fromCityName}-${detail.toCityName}</div>
                                                        </c:forEach>
                                                    </div>
                                                    <div class="cont">
                                                        <span>${orderDto.details.get(0).refundRule}</span>
                                                        <span>${orderDto.details.get(0).specialRule}</span>
                                                    </div>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <c:forEach items="${orderDto.details}" var="detail">
                                                    <div>
                                                        <div class="city">
                                                            <div>${detail.fromCityName}-${detail.toCityName}</div>
                                                        </div>
                                                        <div class="cont">
                                                            <span>${detail.refundRule}</span>
                                                            <span>${deatil.specialRule}</span>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                                <div class="refund-content refund2">
                                    <div class="left-line line"></div>
                                    <div class="right-line line"></div>
                                    <div class="refund-title">行李规定</div>
                                    <div class="box">
                                        <c:forEach items="${orderDto.details}" var="detail">
                                            <div>
                                                <div class="city">
                                                    <div>${detail.fromCityName}-${detail.toCityName}</div>
                                                </div>
                                                <div class="cont">
                                                    <span>${detail.baggageRule}</span>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>

            </li>
            <li class="order-info-num">
                <p class="title">订单号： <span>${orderDto.id}</span></p>
                <ul>
                    <li>下单时间： <span><fmt:formatDate value="${orderDto.createTime}" pattern="yyyy-MM-dd HH:mm"/></span>
                    </li>
                    <li>支付时间：
                        <c:if test="${!(orderDto.orderShowStatus eq 'WAIT_PAYMENT') and !(orderDto.orderShowStatus eq 'WAIT_AUTHORIZE') and !(orderDto.orderShowStatus eq 'AUTHORIZE_RETURN')}">
                            <span><fmt:formatDate value="${orderDto.paymentTime}" pattern="yyyy-MM-dd HH:mm"/></span>
                        </c:if>
                    </li>
                    <li>支付方式：
                        <c:if test="${!(orderDto.orderShowStatus eq 'WAIT_PAYMENT') and !(orderDto.orderShowStatus eq 'WAIT_AUTHORIZE') and !(orderDto.orderShowStatus eq 'AUTHORIZE_RETURN')}">
                            <span>${orderDto.paymentMethod}</span>
                            <c:if test="${orderDto.paymentMethod eq BP_ACCOUNT}">
                                <!-- 审批历史 -->
                            </c:if>
                        </c:if>
                    </li>
                </ul>
            </li>
            <li class="order-info-btn">
                <p class="btn-title">${orderDto.orderShowStatus.getMessage()}</p>
                <c:if test="${modify}">
                    <div class="pay-btn" onclick="openFlightDetail('order/internaltionalFlightOrder/${orderDto.id}')">
                        修改订单
                    </div>
                </c:if>
            </li>
        </ul>
    </div>
    <div id="travel-info" class="travel-info">
         <c:forEach items="${orderDto.details}" var="detail" varStatus="statu">
            <div class="travel-wrapper">
                <div class="travel-step">
                    <div class="step-num">
                        <img src="resource/css/img/flight.png" alt="">
                        <span class="time">
							第
							<c:if test="${statu.index == 0}">一</c:if>
							<c:if test="${statu.index == 1}">二</c:if>
							<c:if test="${statu.index == 2}">三</c:if>
							<c:if test="${statu.index == 3}">四</c:if>
							<c:if test="${statu.index == 4}">五</c:if>
							<c:if test="${statu.index == 5}">六</c:if>
							<c:if test="${statu.index == 6}">七</c:if>
							<c:if test="${statu.index == 7}">八</c:if>
							<c:if test="${statu.index == 8}">九</c:if>
							<c:if test="${statu.index == 9}">十</c:if>
							程
						</span>
                    </div>
                    <div class="step-info">
                        <p><fmt:formatDate value="${detail.fromDate}" pattern="yyyy-MM-dd"/>
                            周${weekList.get(statu.index)}</p>
                        <p class="time"> ${detail.fromCityName}-${detail.toCityName}</p>
                    </div>
                </div>
                <div class="travel-detail">
                    <div class="detail-start">
                        <p class="time"><fmt:formatDate value="${detail.getFlyFromDate()}" pattern="HH:mm"/></p>
                        <p>${detail.fromAirportName} ${detail.fromTerminal}</p>
                    </div>
                    <div class="detail-icon">
                        <p class="duration-time"><span>${detail.getDuration()}</span></p>
                        <c:if test="${detail.stopOver eq true}">
                            <p class="stop">${detail.stopCityName} 经停 ${detail.stopTime}</p>
                        </c:if>
                    </div>
                    <div class="detail-end">
                        <p class="time">
                            <fmt:formatDate value="${detail.getFlyToDate()}" pattern="HH:mm"/>
                            <c:if test="${detail.differentDays() > 0}"><span>+${detail.differentDays()}天</span></c:if>
                        </p>
                        <c:if test="${detail.differentDays() > 0}">
                            <p class="contain-box">
                                <span>${detail.toCityName}时间：<fmt:formatDate value="${detail.getFlyToDate()}"
                                                                             pattern="yyyy-MM-dd HH:mm"/></span>
                            </p>
                        </c:if>
                        <p>${detail.toAirportName} ${detail.toTerminal}</p>
                    </div>
                </div>
                <div class="travel-flight-info">
                    <div>
                        <p class="flight-info">${detail.carrierName} ${detail.flightNo}</p>
                        <c:if test="${detail.share}">
                        <div class="flightIco" style="float: left;padding-right: 10px;">
                            <img alt="" src="resource/image/share_flight.png" width=15 height=15>
                        </div>
                        <p class="flight-info"><span style="color: #0078C9">实际乘坐</span>
                              ${detail.shareCarrierName} ${detail.realFlightNo}
                        </p>
                        </c:if>
                        <p>
                            <c:if test="${not empty detail.aircraftType}">
                                ${detail.aircraftType}
                            </c:if>
                            <c:choose>
                                <c:when test="${detail.cabinClass eq 'Y'}">
                                    经济舱
                                </c:when>
                                <c:when test="${detail.cabinClass eq 'C'}">
                                    公务舱
                                </c:when>
                                <c:when test="${detail.cabinClass eq 'F'}">
                                    头等舱
                                </c:when>
                            </c:choose>
                        </p>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
    <div id="passenger-info" class="passenger-info">
        <div class="passenger-wrapper">
            <div class="title">乘客信息</div>
            <div class="passenger-nav">
                <ul>
                    <li class="name">|&nbsp;姓名</li>
                    <li class="type">|&nbsp;类型</li>
                    <li class="identity">|&nbsp;证件类型</li>
                    <li class="identy-num">|&nbsp;证件号</li>
                    <li class="phone">|&nbsp;手机</li>
                    <li class="card">|&nbsp;票号</li>
                    <li class="staus">|&nbsp;状态</li>
                    <li class="contact">|&nbsp;联系人</li>
                </ul>
            </div>
            <div class="passenger-list">
                <div class="list-info">
                    <c:forEach items="${orderDto.passengers}" var="passenger">
                        <ul>
                            <li class="name flight_ell">
                                <span>${passenger.surName}/${passenger.givenName}<span
                                        class="box">${passenger.surName}/${passenger.givenName}</span></span>
                            </li>
                            <li class="type">${passenger.passengerType.message}</li>
                            <li class="identity">${passenger.credentialType.message}</li>
                            <li class="identy-num card-num">
                                <span>${passenger.credentialNo}</span>
                                <div class="card-info-box">
                                    <p>${passenger.credentialNo}</p>
                                    <p>
                                        <span>签发国家</span>
                                        <span>${passenger.nationality}</span>
                                    </p>
                                    <p>
                                        <span>性别</span>
                                        <span>${passenger.gender.getMessage()}</span>
                                    </p>
                                    <p>
                                        <span>出生日期</span>
                                        <span><fmt:formatDate value="${passenger.birthday}"
                                                              pattern="yyyy/MM/dd"/></span>
                                    </p>
                                    <p>
                                        <span>证件有效期</span>
                                        <span>${passenger.dateOfExpiry}</span>
                                    </p>
                                </div>
                            </li>
                            <li class="phone">${passenger.mobile}</li>
                            <li class="card">${passenger.ticketNo}</li>
                            <li class="staus">${passenger.status.message}</li>
                        </ul>
                    </c:forEach>
                </div>
                <div class="contact-info">
                    <div>
                        <p>
                            <span class="name">姓名：</span>
                            <span>${orderDto.contactorName}</span>
                        </p>
                        <p>
                            <span class="phone">手机：</span>
                            <span>${orderDto.contactorMobile}</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="illega-info" class="illega-info" hidden="${hidden}">
        <div class="illega-wrapper">
            <div class="title"><img src="resource/css/img/illega.png" width="32" height="32" alt="">该订单包含一下违规事项</div>
            <div class="nav">
                <ul>
                    <li>|&nbsp;违规项目</li>
                    <li>|&nbsp;违规原因</li>
                    <li>|&nbsp;情况说明</li>
                </ul>
            </div>
            <c:forEach items="${illegalInfoList}" var="illegalInfo">
                <div class="illega-detail">
                    <span>${illegalInfo.issueInfo}</span>
                    <span>${illegalInfo.illegalReason}</span>
                    <span class="flight_ell">${illegalInfo.extraInfo}</span>
                    <span>详情</span>
                </div>
            </c:forEach>
        </div>
    </div>
    <div id="provider-info" class="provider-info">
        <div class="provider-wrapper">
            <div class="title">供应商信息</div>
            <div class="nav">
                <ul>
                    <li class="nav_price">|&nbsp;供应商单人票面结算价</li>
                    <li class="nav_num">|&nbsp;供应商单人税费</li>
                    <li class="nav_total">|&nbsp;人数</li>
                    <li class="nav_num">|&nbsp;结算价总额</li>
                    <li class="nav_num">|&nbsp;毛利</li>
                    <li class="nav_num">|&nbsp;供应商</li>
                    <li class="nav_price">|&nbsp;订单号</li>
                    <li class="nav_price">|&nbsp;PNR</li>
                </ul>
            </div>
            <div class="provider-detail nav">
                <ul>
                    <li class="nav_price">CNY &nbsp; ${orderDto.singleSettelPrice.intValue()}</li>
                    <li class="nav_num">CNY &nbsp; ${orderDto.singleSettelTaxFee.intValue()}</li>
                    <li class="nav_total">${orderDto.passengers.size()}</li>
                    <li class="nav_num">CNY &nbsp; ${orderDto.totalSettlePrice.intValue()}</li>
                    <li class="nav_num">CNY &nbsp; <span
                            class="red">${orderDto.totalSalePrice.intValue()-orderDto.totalSettlePrice.intValue()}</span>
                    </li>
                    <li class="nav_num">${orderDto.supplierName}</li>
                    <li class="nav_price">${orderDto.id}</li>
                    <li class="nav_price">${orderDto.pnrCode}</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    $(".header-nav ul li").on("click", function () {
        $(this).addClass('active').siblings().removeClass('active');
        var id = $(this)[0].dataset.id
        var heights = document.getElementById(id).offsetTop
        $("html,body").scrollTop(heights - 100);
    })
    function openFlightDetail(url) {
        window.open(url);
    }
</script>
