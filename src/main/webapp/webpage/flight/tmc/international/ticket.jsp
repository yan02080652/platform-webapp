<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="ticket_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            		<h4 class="modal-title">
            			<c:choose>
            				<c:when test="${flag eq '1' }">线下出票</c:when>
            				<c:when test="${flag eq '2' }">回填票号</c:when>
            				<c:when test="${flag eq '3' }">改签成功</c:when>
            			</c:choose>
            		</h4>
         </div>
         <div class="modal-body">
         <form  class="form-horizontal" id="ticketForm" role="form">
         	<input type="hidden" value="${taskId }" id="taskId"/>
         	<input type="hidden" value="${orderId }" id="orderId"/>
         	<input type="hidden" value="${flag }" id="flag"/>

			   <div class="form-group">
				   <label  class="col-sm-2 control-label">出票渠道<span><font color="red">*</font></span></label>
		     	 <div class="col-sm-4">
		         	<select class="form-control" id="issueTicketChannel">
				          <option value="">----</option>
				          <c:forEach items="${channelList }" var="channel">
				          	<option value="${channel.code }">${channel.name }</option>
				          </c:forEach>
				    </select>
		     	 </div>

		     	 <label  class="col-sm-2 control-label">PNR</label>
		     	  <div class="col-sm-4">
			         <input type="text" class="form-control" id="pnr"  value="${flightOrderDto.pnrCode }" placeholder="请输入6位PNR编码" maxlength="6"/>
			      </div>
		  	 </div>
			   <div class="form-group">
			    <label  class="col-sm-2 control-label">供应单号</label>
		     	  <div class="col-sm-4">
			         <input type="text" class="form-control"  value="${flightOrderDto.externalOrderId }" id="externalOrderId" placeholder="请输入外部平台订单号"/>
			      </div>
			       <label  class="col-sm-2 control-label">应付总价<span><font color="red">*</font></span></label>
		     	  <div class="col-sm-4">
			         <input type="number" class="form-control" value="${flightOrderDto.totalSettlePrice }" id="totalPrice" placeholder="请输入采购结算总价" />
			      </div>
			   </div>

			   <div class="form-group">
			      <div class="col-sm-12">
							<table class="table table-bordered" id="passengerTable">
								<thead>
									<tr>
										<th>姓名</th>
										<th>票号</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${passengerList }" var="passenger" varStatus="vs">
										<tr>
											<td>${passenger.surName }/${passenger.givenName }</td>
											<td>
											<c:choose>
												<c:when test="${vs.first }">
													<input type="text" class="form-control" onblur="autoAdd($(this))" data-value="${passenger.id }"/>
													<span style="text-align: left;color: red" class="pull-left" hidden>票号格式不正确(例:784-8775510286-91)</span>
												</c:when>
												<c:otherwise>
													<input type="text" class="form-control" onblur="checkNo($(this))" data-value="${passenger.id }"/>
													<span style="text-align: left;color: red" class="pull-left" hidden>票号格式不正确(例:784-8775510286-91)</span>
												</c:otherwise>
											</c:choose>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
			   </div>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="submitTicketFrom()">保存并出票</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">放弃</button>
         </div>
      </div>
</div>
</div>
<script type="text/javascript">

$(function(){
	var issueChannel = "${flightOrderDto.issueChannel}";
	if(issueChannel){
		$("#issueChannel").val(issueChannel).attr("disabled",true);
//		$("#pnr").attr('readonly',true)
		$("#externalOrderId").attr('readonly',true)
		$("#totalPiace").attr('readonly',true)
	}
})
</script>