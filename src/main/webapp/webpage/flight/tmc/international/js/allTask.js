$(function () {
    $('.tab1').on('click', function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active')
        $('.content1').show();
        $('.content2').hide();
        $('.content3').hide();
    })
    $('.tab2').on('click', function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active');
        $('.content1').hide();
        $('.content2').show();
        $('.content3').hide();
    })
    $('.tab3').on('click', function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active');
        $('.content1').hide();
        $('.content2').hide();
        $('.content3').show();
    })

    //清空定时器
    clearInterval(allTask_timer);

    refreshAllTask();
})

function reloadMyTaskTable(pageIndex) {
    var taskStatus = $("#allTaskStatusTab .active").attr("data-value");
    var taskType = $("#currentTaskType").val();
    reloadAllTask(pageIndex, taskStatus, taskType);

}

function reloadAllTask(pageIndex, taskStatus, taskType) {
    $("#currentTaskType").val(taskType);
    pageIndex = pageIndex ? pageIndex : 1;
    $("#allTask_pageIndex").val(pageIndex);
    $("#allTask_task_status").val(taskStatus);
    $("#allTask_task_type").val(taskType);

    //查询条件
    var field1 = $.trim($("#search_Field2").val());

    if (!taskStatus) {
        taskStatus = $("#allTaskStatusTab .active").attr("data-value");
    }

    var layer_index = layer.load(0);
    $.ajax({
        type: "post",
        url: "internationalflight/tmc/allTask",
        data: {
            taskStatus: taskStatus,
            pageIndex: pageIndex,
            taskType: taskType,
            keyword: field1,
        },
        success: function (data) {
            layer.close(layer_index);
            $("#allTask").html(data);
            $("#search_Field2").val(field1);
        },
        error: function () {
            layer.close(layer_index);
            layer.msg("系统繁忙，请刷新重试", {icon: 2})
        }
    });
}

//重置查询条件
function resetForm() {
    $('#orderSearchForm')[0].reset();
    // 再次请求
    reloadAllTask();
}

//allTask:自动刷新
var allTask_count = 9;
function refreshAllTask() {
    $("#countDown").text(allTask_count);
    allTask_count--;
    if (allTask_count >= 0) {
        allTask_timer = setTimeout(refreshAllTask, 1000);
    } else {
        var pageIndex = $("#allTask_pageIndex").val();
        var taskStatus = $("#allTask_task_status").val();
        var taskType = $("#allTask_task_type").val();
        if (taskStatus == '') {
            taskStatus = 'WAIT_PROCESS';
        }
        reloadAllTask(pageIndex, taskStatus, taskType);
    }
}

//任务交接
//注：checkTrans boolean类型,普通客服任务交接需要判断是否为本人，传true，客服经理通过全部任务列表分配不需要判断,传false
function transfer(taskId, operatorId, checkTrans, callback) {
    var lawful = true;
    if (checkTrans) {
        lawful = checkOperator(operatorId);
    }
    if (lawful) {
        TR.select('user', {type: "me"}, function (data) {
            var layer_index = layer.load(0);
            $.ajax({
                type: "post",
                url: "internationalflight/tmc/transfer",
                data: {
                    taskId: taskId,
                    operatorId: operatorId,
                    userId: data.id,
                    userName: data.fullname
                },
                success: function (rs) {
                    layer.close(layer_index);
                    if (rs) {
                        layer.msg("任务已转交给" + data.fullname, {icon: 1, offset: 't', time: 1000})
                        setTimeout(function () {
                            submitAjax();
                        }, 200);
                    } else {
                        layer.msg("系统错误，请刷新重试！", {icon: 2, offset: 't', time: 1000})
                    }
                }
            });
        });
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}

// 检查当前操作人是否为任务领取人
function checkOperator(operatorId) {
    var lawful;
    $.ajax({
        url: "internationalflight/tmc/checkOperator",
        type: "GET",
        async: false,
        data: {operatorId: operatorId},
        success: function (data) {
            lawful = data;
        }
    });

    return lawful;
}

//修改出票信息弹窗
function ticketPopup(orderId) {
    $("#model1").load("internationalflight/tmc/orderDetail?orderId=" + orderId,
        function (context, state) {
            if ('success' == state) {
                $('#modifyTicket_model').modal({
                    backdrop: 'static'
                });
            }
        });
}
