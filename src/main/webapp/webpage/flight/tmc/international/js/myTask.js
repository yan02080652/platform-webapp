$(function () {
    $('.tab1').on('click', function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active')
        $('.content1').show();
        $('.content2').hide();
    })
    $('.tab2').on('click', function () {
        $('.tab').find('li').removeClass('active');
        $(this).addClass('active');
        $('.content1').hide();
        $('.content2').show();
    })

    clearInterval(timerFlag);
    timerFlag = setTimeout(taskTimer, 10 * 1000);
})

// 检查当前操作人是否为任务领取人
function checkOperator(operatorId) {
    var lawful;
    $.ajax({
        url: "internationalflight/tmc/checkOperator",
        type: "GET",
        async: false,
        data: {operatorId: operatorId},
        success: function (data) {
            lawful = data;
        }
    });

    return lawful;
}


function reloadMyTaskTable(pageIndex) {
    var taskStatus = $("#myTaskStatusTab .active").attr("data-value");
    var taskType = $("#currentTaskType").val();
    submitAjax(pageIndex, taskStatus, taskType);

}

function submitAjax(pageIndex, taskStatus, taskType) {
    $("#currentTaskType").val(taskType);
    $("#myTask_pageIndex").val(pageIndex);
    $("#myTask_task_status").val(taskStatus);
    $("#myTask_task_type").val(taskType);
    // var layer_index = layer.load(0);
    $.ajax({
        type: "post",
        url: "internationalflight/tmc/getMyTaskTable",
        data: {
            taskStatus: taskStatus,
            pageIndex: pageIndex,
            taskType: taskType
        },
        success: function (data) {
            // layer.close(layer_index);
            $("#myTask").html(data);
        },
        error: function () {
            // layer.close(layer_index);
            layer.msg("系统繁忙，请刷新重试", {icon: 2, offset: 't', time: 1000})
        }
    });
}

//显示Model
function showModal(url, data, callback) {
    $('#model1').load(url, data, function (context, state) {
        if ('success' == state && callback) {
            callback();
        }
    });
}

function getTicketData(orderId, taskId, flag, operatorId) {
    var lawful = checkOperator(operatorId);
    if (lawful) {
        showModal("internationalflight/tmc/getTicketData", {orderId: orderId, taskId: taskId, flag: flag}, function (data) {
            $('#ticket_model').modal({
                backdrop: 'static'
            });
        });
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}

function autoAdd(obj) {
    var firstNo = obj.val();
    //验证格式
    if (checkReg(firstNo)) {
        obj.next().hide();
        if (firstNo.indexOf("-") > 0) {

            if(firstNo.indexOf("-") == firstNo.lastIndexOf("-")) {

                var split = firstNo.split("-");
                $("#passengerTable tbody tr:gt(0)").each(function (i, dom) {
                    $(this).find("td:eq(1) input").val(split[0] + "-" + (parseInt(split[1]) + i + 1));
                })
            } else {

                var split = firstNo.split("-");
                $("#passengerTable tbody tr:gt(0)").each(function (i, dom) {
                    $(this).find("td:eq(1) input").val(split[0] + "-"+split[1] + "-" + (parseInt(split[2]) + i + 1));
                })
            }

        } else {
            //后面每个加1
            $("#passengerTable tbody tr:gt(0)").each(function (i, dom) {
                $(this).find("td:eq(1) input").val(parseInt(firstNo) + i + 1);
            })
        }

    } else {
        obj.next().show();
    }


}

function checkNo(obj) {
    if (checkReg(obj.val())) {
        obj.next().hide();
    } else {
        obj.next().show();
    }
}

function checkReg(value) {
    var reg3 = /^[\d]{1}[\d]{0,16}[-]{0,1}[\d]{1,16}[-]{0,1}[\d]{0,16}[\d]{1}$/;
    return reg3.test(value) && value.length < 21;
}


//线下出票
function submitTicketFrom() {

    var isTrue = true;

    $("#passengerTable tbody tr").each(function (i, dom) {
        var no = $(this).find("td:eq(1) input").val();
        if (checkReg(no)) {
            $(this).find("td:eq(1) input").next().hide();
        } else {
            $(this).find("td:eq(1) input").next().show();
            return;
        }
    })

    var issueChannel = $("#issueTicketChannel").val();
    var totalPrice = $("#totalPrice").val();

    if (issueChannel == null || issueChannel == '') {
        layer.msg("出票渠道必填！!")
        return;
    }
    if (totalPrice == null || totalPrice == '') {
        layer.msg("结算总价必填！")
        return;
    }


    // 提交
    var taskId = $("#taskId").val();
    var orderId = $("#orderId").val();
    var pnr = $("#pnr").val();
    var externalOrderId = $("#externalOrderId").val();
    var flag = $("#flag").val();

    var remark;
    if (flag == '1') {
        remark = "线下出票";
    } else if (flag == '2') {
        remark = "回填票号";
    } else {
        remark = "改签成功";
    }

    var passengers = [];
    var errNo = false;
    $("#passengerTable tbody tr").each(function (i, dom) {
        var no = $(this).find("td:eq(1) input").val();
        var id = $(this).find("td:eq(1) input").attr("data-value");
        if (no == '') {
            errNo = true;
        }
        if (!$(this).find("span").is(":hidden")) {
            errNo = true;
        }

        var p = {
            id: id,
            ticketNo: no
        }

        passengers.push(p);

    })

    if (errNo) {
        layer.msg("请填写正确的票号！")
        return;
    }

    var formObj = {
        orderId: orderId,
        taskId: taskId,
        flag: flag,
        issueChannel: issueChannel,
        pnr: pnr,
        externalOrderId: externalOrderId,
        totalPrice: totalPrice,
        passengerDtos: passengers,
    }

    console.log(formObj)

    var layer_index = layer.load(0);
    $.ajax({
        type: "post",
        url: "internationalflight/tmc/saveTicket",
        data: JSON.stringify(formObj),
        contentType: "application/json",
        datatype: "json",
        success: function (data) {
            layer.close(layer_index);
            $('#ticket_model').modal('hide');
            if (data) {
                layer.msg("待出票任务完成，处理方式：" + remark, {icon: 1, offset: 't', time: 1000});
                setTimeout(function () {
                    submitAjax();
                }, 200);
            } else {
                layer.msg("系统错误，请刷新重试！", {icon: 2, offset: 't', time: 1000})
            }
        }
    })

}

//拒单退款
function rejectOrder(orderId, taskId, userId, operatorId) {
    var lawful = checkOperator(operatorId);
    if (lawful) {
        $('#rejectOrderForm')[0].reset();
        $("#rejectOrder_model").modal({
            backdrop: 'static'
        });
        initForm();
        $("#rejectOrderForm input[name='orderId']").val(orderId);
        $("#rejectOrderForm input[name='taskId']").val(taskId);
        $("#rejectOrderForm input[name='userId']").val(userId);
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}

function initForm() {
    $("#rejectOrderForm").validate(
        {
            rules: {
                remark: {
                    required: true,
                    minlength: 4,
                },
            },
            submitHandler: function (form) {
                var orderId = $("#rejectOrderForm input[name='orderId']").val();
                var taskId = $("#rejectOrderForm input[name='taskId']").val();
                var userId = $("#rejectOrderForm input[name='userId']").val();
                var remark = $("#rejectOrderForm input[name='remark']").val();
                var layer_index = layer.load(0);
                $.ajax({
                    type: "POST",
                    url: "internationalflight/tmc/rejectOrder",
                    data: {
                        orderId: orderId,
                        taskId: taskId,
                        userId: userId,
                        remark: remark,
                    },
                    async: false,
                    error: function (request) {
                        layer.close(layer_index);
                        layer.msg("请求失败，请刷新重试", {icon: 2})
                        $('#rejectOrder_model').modal('hide');
                        setTimeout(function () {
                            submitAjax();
                        }, 200);
                    },
                    success: function (data) {
                        layer.close(layer_index);
                        $('#rejectOrder_model').modal('hide');
                        if (data) {
                            layer.msg("待出票任务完成，处理方式：拒单退款", {icon: 1, offset: 't', time: 1000})
                            setTimeout(function () {
                                submitAjax();
                            }, 200);
                        } else {
                            layer.msg("系统错误，请刷新重试！", {icon: 2, offset: 't', time: 1000})
                            setTimeout(function () {
                                submitAjax();
                            }, 200);
                        }
                    }
                });
            },
            errorPlacement: function (error, element) {
                layer.tips($(error).text(), $(element), {
                    tips: [1, '#bb0b07'],
                    tipsMore: true,
                });
            },
        });
}

function submitRejectOrderForm() {
    $("#rejectOrderForm").submit();
}

//填写报价
function fillOrder(orderId,taskId,operatorId){
    showModal("/internationalflight/tmc/fillOrder",{orderId:orderId,taskId:taskId, operatorId:operatorId},function(data){
        $('#ticket_model').modal();
        initTicketFrom();
    });
}

//提交报价
function submitOfferForm(){
    $("#offerForm").submit();
}

function initTicketFrom(){
    $("#offerForm").validate({
        rules : {
            singleTicketPirce : {
                required:true
            },
            singleTaxFee : {
                required:true
            },
            singleServiceFee : {
                required:true
            }
        },
        submitHandler : function(form) {

            $('#offerForm').ajaxSubmit({success:function(data){
                    if(data){
                        $('#ticket_model').modal('hide');
                        showPromptBox("操作成功!")
                        setTimeout(function(){
                            submitAjax(null,'PROCESSING',null);
                        },200);
                    }else{
                        layer.msg("系统异常，保存失败!",{icon:2})
                    }
                }});
        },
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight
    });
}

//任务交接
//注：checkTrans boolean类型,普通客服任务交接需要判断是否为本人，传true，客服经理通过全部任务列表分配不需要判断,传false
function transfer(taskId, operatorId, checkTrans, callback) {
    var lawful = true;
    if (checkTrans) {
        lawful = checkOperator(operatorId);
    }
    if (lawful) {
        TR.select('user', {type: "me"}, function (data) {
            var layer_index = layer.load(0);
            $.ajax({
                type: "post",
                url: "internationalflight/tmc/transfer",
                data: {
                    taskId: taskId,
                    operatorId: operatorId,
                    userId: data.id,
                    userName: data.fullname
                },
                success: function (rs) {
                    layer.close(layer_index);
                    if (rs) {
                        layer.msg("任务已转交给" + data.fullname, {icon: 1, offset: 't', time: 1000})
                        setTimeout(function () {
                            submitAjax();
                        }, 200);
                    } else {
                        layer.msg("系统错误，请刷新重试！", {icon: 2, offset: 't', time: 1000})
                    }
                }
            });
        });
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}


//无供应商信息时 点击'再次下单'
function againCreateOrder(orderId, operatorId) {
    var lawful = checkOperator(operatorId);
    if (lawful) {
        layer.confirm('你确定要再次下单吗?', function (index) {
            layer.close(index);
            var loadIndex = layer.load();
            $.ajax({
                type: "post",
                url: "internationalflight/tmc/againCreateOrder",
                data: {
                    orderId: orderId,
                },
                success: function (data) {
                    layer.close(loadIndex);
                    if (data) {
                        layer.msg("创单成功！", {icon: 4, offset: 't', time: 1000});
                    } else {
                        layer.msg("创单失败", {icon: 2, offset: 't', time: 1000})
                    }
                    submitAjax();
                },
                error: function () {
                    layer.msg("系统错误，请稍后刷新重试！", {icon: 2, offset: 't', time: 1000});
                    layer.close(loadIndex);
                }
            });
        });
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }

}

function payment(orderId, taskId, operatorId) {
    var lawful = checkOperator(operatorId);
    if (lawful) {
        layer.confirm('请确保账户余额充足,确定重新支付?', {title: "系统提示"}, function (index) {
            layer.close(index);
            var loadIndex = layer.load();
            $.ajax({
                type: "post",
                url: "internationalflight/tmc/payment",
                data: {
                    orderId: orderId,
                    taskId: taskId,
                },
                success: function (result) {
                    layer.close(loadIndex);
                    if (result.code == '1') {
                        layer.msg("待出票任务完成，处理方式：转系统出票", {icon: 1, offset: 't', time: 1000})
                    } else {
                        layer.msg("转系统出票失败:" + result.msg, {icon: 2, offset: 't', time: 10000})
                    }
                    submitAjax();
                },
                error: function () {
                    layer.msg("系统错误，请稍后刷新重试！", {icon: 2, offset: 't', time: 10000});
                    layer.close(loadIndex);
                }
            });
        });
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}

function cancelOrder(orderId,taskId,operatorId) {
    var lawful = checkOperator(operatorId);
    if (lawful) {
        layer.confirm('你确定要取消订单吗?', function (index) {
            layer.close(index);
            var load_index = layer.load();
            $.ajax({
                type: "post",
                url: "internationalflight/tmc/cancelOrder",
                data: {
                    orderId: orderId,
                    taskId:taskId
                },
                success: function (data) {
                    layer.close(load_index);
                    if (data) {
                        layer.msg("订单取消成功", {icon: 1, offset: 't', time: 1000})
                        submitAjax();
                    } else{
                        layer.msg("订单取消失败，请稍后重试", {icon: 1, offset: 't', time: 1000})
                    }
                }
            });
        });
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}

//供应拒单重新采票
function reTicket(orderId, taskId, operatorId) {
    var lawful = checkOperator(operatorId);
    if (lawful) {
        layer.confirm('你确定要拒单重新下单吗?', function (index) {
            layer.close(index);
            var load_index = layer.load();
            $.ajax({
                type: "post",
                url: "internationalflight/tmc/reTicket",
                data: {
                    orderId: orderId,
                    taskId: taskId,
                },
                success: function (data) {
                    layer.close(load_index);
                    if (data) {
                        layer.msg("待催单任务完成，处理方式：供应拒单重新采票", {icon: 1, offset: 't', time: 1000});
                        submitAjax();
                    }
                }
            });
        });
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}

//退票审核
function ticketAduit(refundOrderId, taskId, operatorId) {
    var lawful = checkOperator(operatorId);
    if (lawful) {
        layer.open({
            type: 2,
            area: ['1000px', '600px'],
            title: "退票审核",
            closeBtn: 1,
            shadeClose: false,
            //shade :0,
            content: "internationalflight/tmc/getOrderDetail?refundOrderId=" + refundOrderId + "&taskId=" + taskId,
        });
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}


// 定时任务
function taskTimer() {
    var pageIndex = $("#myTask_pageIndex").val();
    var taskStatus = $("#myTask_task_status").val();
    var taskType = $("#myTask_task_type").val();

    if (taskStatus == '') {
        taskStatus = 'PROCESSING';
    }
    submitAjax(pageIndex, taskStatus, taskType);
}

function delay(taskId, delay, operatorId) {
    var lawful = checkOperator(operatorId);
    if (lawful) {
        if (delay != -1) {
            $.ajax({
                url: "internationalflight/tmc/updateMaxProcessDate",
                post: "get",
                data: {taskId: taskId, delay: delay},
                success: function () {
                    layer.msg("任务延时成功!", {icon: 1, offset: 't', time: 1000})
                    submitAjax();
                }
            });
        }
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}

function confirmDone(orderId, taskId, operatorId) {
    var lawful = checkOperator(operatorId);
    if (lawful) {
        layer.confirm('请确认任务订单已完成,是否确认?', function (index) {
            layer.close(index);
            var load_index = layer.load();
            $.ajax({
                type: "get",
                url: "internationalflight/tmc/confirmDone",
                data: {
                    orderId: orderId,
                    taskId: taskId,
                },
                success: function (data) {
                    layer.close(load_index);
                    if (data.code == '1') {
                        layer.msg("催单任务完成", {icon: 1, offset: 't', time: 1000});
                        submitAjax();
                    } else {
                        layer.msg(data.msg, {icon: 2, offset: 't', time: 10000})
                    }
                }
            });
        });
    } else {
        layer.msg("非法操作！", {icon: 2, offset: 't', time: 1000});
    }
}

function dateAdd(time, strInterval, Number) {
    var dtTmp = time;
    switch (strInterval) {
        case 's' :
            return new Date(Date.parse(dtTmp) + (1000 * Number));
        case 'n' :
            return new Date(Date.parse(dtTmp) + (60000 * Number));
        case 'h' :
            return new Date(Date.parse(dtTmp) + (3600000 * Number));
        case 'd' :
            return new Date(Date.parse(dtTmp) + (86400000 * Number));
        case 'w' :
            return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));
        case 'q' :
            return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'm' :
            return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        case 'y' :
            return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
    }
};

Date.prototype.Format = function (fmt) {
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "H+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
}

