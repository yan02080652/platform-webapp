var pageVal = {
    titles: ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十'],
    userIndex: 0,
    getUserIndex() {
        return ++this.userIndex;
    },
    frameKey:null
};

var showIndex = 0;
var showIframeWin;

$(document).ready(function () {

    //绑定事件
    bindEvent();

    //以下默认增加逻辑，可以删除
    addProcess();

    // $("#partnerName").val($("input[name='pname']").val() + "方案" + showIndex);

    var flightRules = {};

    for (var i = 0; i <= pageVal.titles.length; i++) {

        flightRules['flightNo_' + i] = {required: true, interFlightNo: true};
        flightRules['realFlightNo_' + i] = {
            required: function (ele) {

                if ($(ele).parents(".fnDiv").find(".share").is(":checked")) {
                    return true;
                } else {
                    return false;
                }
            }, interFlightNo: true
        };
        flightRules['cabinCode_' + i] = {required: true, interCabinCode: true};
        flightRules['fromDate_' + i] = {required: true};
        flightRules['toDate_' + i] = {required: true, internationalToDate: true};
        flightRules['fromAirportCode_' + i] = {required: true, interAirportCode: true};
        flightRules['fromTerminal_' + i] = {interTerminal: true};
        flightRules['toAirportCode_' + i] = {required: true, interAirportCode: true};
        flightRules['toTerminal_' + i] = {interTerminal: true};
        flightRules['fromTime_' + i] = {required: true, interTime: true};
        flightRules['toTime_' + i] = {required: true, interTime: true};
        flightRules['aircraftType_' + i] = {interAircraftType: true};
        flightRules['stopCityName_' + i] = {
            required: function (ele) {
                if ($(ele).parents(".stopFlight").find(".stopOver").is(":checked")) {
                    return true;
                } else {
                    return false;
                }
            }, maxlength: 15, checkCHN: true
        };
        flightRules['stopTime_' + i] = {
            required: function (ele) {
                if ($(ele).parents(".stopFlight").find(".stopOver").is(":checked")) {
                    return true;
                } else {
                    return false;
                }
            }, stopTime: true
        };
        flightRules['baggageType_' + i] = {required: true};

    }

        flightRules['singleTicketPrice'] = {required: true},
        flightRules['singleTaxFee'] = {required: true},
        flightRules['singleExtraFee'] = {required: true},
        flightRules['singleSettelPrice'] = {required: true},
        flightRules['singleSettelTaxFee'] = {required: true},

        flightRules['refundRule'] = {required: true},
        flightRules['changeRule'] = {required: true},

        //联系人校验规则
        $("#interFlight").validate({
            rules: flightRules,

            submitHandler: function (form) {

                var subIndex = layer.load();
                //收集数据
                var details = [];

                var processes = $('.ftTravel .process').not('.defaultDiv');
                $.each(processes, function (i, process) {
                    var detail = {
                        digital: $(this).find("input[name='digital']").val(),
                        flightNo: $(this).find("input[name='flightNo_" + (i + 1) + "']").val(),
                        share: $(this).find("input[name='share_" + (i + 1) + "']").is(":checked"),
                        realFlightNo: $(this).find("input[name='realFlightNo_" + (i + 1) + "']").val(),
                        cabinCode: $(this).find("input[name='cabinCode_" + (i + 1) + "']").val(),
                        fromDate: $(this).find("input[name='fromDate_" + (i + 1) + "']").val(),
                        toDate: $(this).find("input[name='toDate_" + (i + 1) + "']").val(),
                        fromAirportCode: $(this).find("input[name='fromAirportCode_" + (i + 1) + "']").val(),
                        fromTerminal: $(this).find("input[name='fromTerminal_" + (i + 1) + "']").val(),
                        toAirportCode: $(this).find("input[name='toAirportCode_" + (i + 1) + "']").val(),
                        toTerminal: $(this).find("input[name='toTerminal_" + (i + 1) + "']").val(),
                        fromTime: $(this).find("input[name='fromTime_" + (i + 1) + "']").val(),
                        toTime: $(this).find("input[name='toTime_" + (i + 1) + "']").val(),
                        aircraftType: $(this).find("input[name='aircraftType_" + (i + 1) + "']").val(),
                        stopOver: $(this).find("input[name='stopOver_" + (i + 1) + "']").is(":checked"),
                        stopCityName: $(this).find("input[name='stopCityName_" + (i + 1) + "']").val(),
                        stopTime: $(this).find("input[name='stopTime_" + (i + 1) + "']").val(),

                        baggageType: $(this).find("input[name='baggageType_" + (i + 1) + "']:checked").val(),
                        pieceWeight: $(this).find("select[name='pieceWeight_" + (i + 1) + "'] option:selected").val(),
                        piece: $(this).find("select[name='piece_" + (i + 1) + "'] option:selected").val(),
                        weight: $(this).find("select[name='weight_" + (i + 1) + "'] option:selected").val(),
                        refundRule: $("input[name='refundRule']").val(),
                        changeRule: $("input[name='changeRule']").val(),
                        specialChangeRule: $("input[name='specialChangeRule']").val(),
                        specialRefundRule: $("input[name='specialRefundRule']").val(),
                        turningPoint: $(this).find("select[name='turningPoint_" + (i + 1) + "'] option:selected").val() == 1 ? true : false,
                        baggageDirect: $(this).find("select[name='baggageDirect_" + (i + 1) + "'] option:selected").val() == 1 ? true : false,
                        transitVisa: $(this).find("select[name='transitVisa_" + (i + 1) + "'] option:selected").val() == 1 ? true : false,
                    }
                    details.push(detail);
                });

                var lastTodate = 0;
                var errorDate = false;
                var detailIndex = [];
                for (var i = 0; i < details.length; i++) {
                    if (i == 0) {
                        lastTodate = details[i].toDate;
                        continue;
                    }
                    if (parseInt(details[i].fromDate) < lastTodate) {
                        detailIndex.push(i + 1);
                        errorDate = true;
                    }
                    lastTodate = details[i].toDate;
                }
                if (errorDate) {
                    layer.alert("行程中第" + detailIndex.join(",") + "程的出发日期小于上一程的到达日期，请确认正确后在录入", {
                        title: '',
                        closeBtn: 0,
                        btn: ['知道了']
                    });
                    return;
                }

                var orderForm = {
                    title : $("#partnerName").val(),
                    singleTicketPrice: $("input[name='singleTicketPrice']").val(),
                    singleTaxFee: $("input[name='singleTaxFee']").val(),
                    singleSettelPrice: $("input[name='singleSettelPrice']").val(),
                    singleSettelTaxFee: $("input[name='singleSettelTaxFee']").val(),
                    singleExtraFee: $("input[name='singleExtraFee']").val(),

                    tripType: $("input[name='tripType']:checked").val(),
                    detailDtos: details,

                };

                $.ajax({
                    url: "internationalflight/tmc/createTemplate.json",
                    type: "post",
                    data: JSON.stringify(orderForm),
                    contentType: "application/json",
                    success: function (data) {
                        layer.close(subIndex);

                        if (data.code == '0') {
                            //$("#keyWord").val(data.msg);
                            pageVal.frameKey = data.msg;

                            console.log(data.msg)
                            // $("#iframePic").attr("src","http://m.z-trip.cn/flight/template");
                            showIframeWin = layer.open({
                                title: '',
                                type: 1,
                                skin: 'demo-class',
                                closeBtn: false,
                                area: ['400px', "600px"],
                                content: $('#showIframe')
                            })
                            initPicKey();
                        } else {
                            layer.msg("很抱歉，生成模版发生未知错误，请确认数据录入数据是否正确。");
                        }
                    }
                })
            },
            errorClass: "promptError"
        });

    $(".formBtnGray").click(function () {
        $("#interFlight").submit();

    });


    $(".baggageType").click(function () {
        var value = $(this).val();
        var pDiv = $(this).parents(".newBaggage");
        if (value == 1) {
            pDiv.find(".piece").attr("disabled", "disabled");
            pDiv.find(".pieceWeight").attr("disabled", "disabled");
            pDiv.find(".weight").attr("disabled", "disabled");
        } else if (value == 2) {
            pDiv.find(".piece").removeAttr("disabled");
            pDiv.find(".pieceWeight").removeAttr("disabled");
            pDiv.find(".weight").attr("disabled", "disabled");
        } else if (value == 3) {
            pDiv.find(".piece").attr("disabled", "disabled");
            pDiv.find(".pieceWeight").attr("disabled", "disabled");
            pDiv.find(".weight").removeAttr("disabled");

        }
    });

    $(".turningPoint").change(function () {
        var turningPoint = $(this).val();
        var pDiv = $(this).parents(".stopFlight");
        if (turningPoint == 0) {
            pDiv.find(".baggageDirect").attr("disabled", "disabled");
            pDiv.find(".transitVisa").attr("disabled", "disabled");
        } else {
            pDiv.find(".baggageDirect").removeAttr("disabled");
            pDiv.find(".transitVisa").removeAttr("disabled");
        }
    });

    $(".btnclose").click(function () {
        layer.close(showIframeWin);
    });





    $(".btncopy").click(function(){
        //TODO  复制
        let iframe = document.getElementById('iframePic');
        let win = iframe.contentWindow;
        let data = {
            key: 'copy'
        }
        win.postMessage(JSON.stringify(data), "*");
    });

});

function initPicKey() {
    initFrame()
}

function frameLoaded(){
    pageVal.frameLoaded = true;
    initFrame();
}

function initFrame(){
    if(pageVal.frameLoaded && pageVal.frameKey){

        let iframe = document.getElementById('iframePic');
        let win = iframe.contentWindow;
        let data = {
            key: pageVal.frameKey
        }
        win.postMessage(JSON.stringify(data), "*");
    }
}

function bindEvent() {
    //添加行程按钮事件
    $('.ftTravel .addTrvbtn').click(function () {
        addProcess();
    });

    var str = '使用提示：\r\n' +
        '1. 请用您的黑屏系统，使用RT指令调取PNR内容\r\n' +
        '2. 使用PN指令进行翻页，直至PNR的底部\r\n' +
        '3. 将从RT开始到最末行的所有的文字复制到左侧窗口中';

    var pnrIndex = null;
    $(".pnrImport").click(function () {
        $(".bkDiv").val(str);
        $(".btnD").css("background-color", "#EBEBE4");
        $(".btnD").css("color", "black");
        $(".btnD").addClass("notallow");
        $(".btnD").css("cursor", "not-allowed");

        pnrIndex = layer.open({
            title: '',
            type: 1,
            skin: 'demo-class',
            closeBtn: false,
            area: ['585px', "520px"],
            content: $('.HFdiv')
        })
    });

    $(".bkDiv").val(str);

    $(".bkDiv").focus(function () {
        if ($(this).val().indexOf("使用提示：") != -1) {
            $(this).val("");
        }
        if ($(".bkDiv").val().indexOf("使用提示：") != -1 || $(".bkDiv").val().trim() == '') {
            $(".btnD").css("background-color", "#EBEBE4");
            $(".btnD").css("color", "black");
            $(".btnD").addClass("notallow");
            $(".btnD").css("cursor", "not-allowed");
        } else {
            $(".btnD").css("background-color", "#0b76ac");
            $(".btnD").css("color", "#ffffff");
            $(".btnD").removeClass("notallow");
            $(".btnD").css("cursor", "pointer");
        }
    });

    $(".bkDiv").blur(function () {
        if ($(this).val().length == 0) {
            $(".bkDiv").val(str);
        }
        if ($(".bkDiv").val().indexOf("使用提示：") != -1 || $(".bkDiv").val().trim() == '') {
            $(".btnD").css("background-color", "#EBEBE4");
            $(".btnD").css("color", "black");
            $(".btnD").addClass("notallow");
            $(".btnD").css("cursor", "not-allowed");
        } else {
            $(".btnD").css("background-color", "#0b76ac");
            $(".btnD").css("color", "#ffffff");
            $(".btnD").removeClass("notallow");
            $(".btnD").css("cursor", "pointer");
        }
    });

    $(".bkDiv").keyup(function () {
        if ($(".bkDiv").val().indexOf("使用提示：") != -1 || $(".bkDiv").val().trim() == '') {
            $(".btnD").css("background-color", "#EBEBE4");
            $(".btnD").css("color", "black");
            $(".btnD").addClass("notallow");
            $(".btnD").css("cursor", "not-allowed");
        } else {
            $(".btnD").css("background-color", "#0b76ac");
            $(".btnD").css("color", "#ffffff");
            $(".btnD").removeClass("notallow");
            $(".btnD").css("cursor", "pointer");
        }

    });

    if ($(".bkDiv").val().indexOf("使用提示：") != -1) {
        $(".btnD").css("background-color", "#EBEBE4");
        $(".btnD").css("color", "black");
        $(".btnD").addClass("notallow");
        $(".btnD").css("cursor", "not-allowed");
    } else {
        $(".btnD").css("background-color", "#0b76ac");
        $(".btnD").css("color", "#ffffff");
        $(".btnD").removeClass("notallow");
        $(".btnD").css("cursor", "pointer");
    }

    $(".btnC").click(function () {
        layer.close(pnrIndex);
    });


    $(".btnD").click(function () {
        if ($(this).hasClass("notallow")) {
            return false;
        }
        var loadIndex = layer.load();

        var pnr = $(".bkDiv").val();

        $.ajax({
            url: "internationalflight/tmc/parsePnr.json",
            type: "post",
            dataType: "json",
            data: {"pnr": pnr},
            success: function (data) {

                layer.close(loadIndex);

                if (data.result == -1) {
                    layer.closeAll();
                    layer.msg("pnr解析失败，请确认粘贴内容正确或联系管理员。");
                } else if (data.result == -2) {
                    layer.closeAll();
                    layer.msg(data.msg);
                } else {
                    layer.closeAll();

                    $("input[value='" + data.data.tripType + "']").prop("checked", true);

                    if (data.data.details.length > 1) {
                        $(".icon-close").trigger("click");
                        for (var i = 0; i < data.data.details.length - 1; i++) {
                            addProcess();
                        }
                    }

                    $("input[name='singleTicketPrice']").val(data.data.singleTicketPrice);
                    $("input[name='singleTaxFee']").val(data.data.singleTaxFee);
                    $("input[name='singleExtraFee']").val(data.data.singleExtraFee);
                    $("input[name='singleSettelPrice']").val(data.data.singleSettelPrice);
                    $("input[name='singleSettelTaxFee']").val(data.data.singleSettelTaxFee);
                    $("input[name='supplierName']").val(data.data.supplierName);
                    $("input[name='supplierNo']").val(data.data.supplierNo);

                    $("input[name='singleTicketPrice']").addClass("requiredColor");
                    $("input[name='singleTaxFee']").addClass("requiredColor");
                    $("input[name='singleExtraFee']").addClass("requiredColor");
                    $("input[name='singleSettelPrice']").addClass("requiredColor");
                    $("input[name='singleSettelTaxFee']").addClass("requiredColor");
                    $("input[name='supplierName']").addClass("requiredColor");
                    $("input[name='supplierNo']").addClass("requiredColor");

                    $("input[name='pnrCode']").val(data.data.pnrCode);


                    for (var i = 0; i < data.data.details.length; i++) {
                        var detail = data.data.details[i];

                        if (detail.flightNo) {
                            $("input[name='flightNo_" + (i + 1) + "']").val(detail.flightNo);
                        } else {
                            $("input[name='flightNo_" + (i + 1) + "']").addClass("requiredColor");
                        }
                        $("input[name='share_" + (i + 1) + "']").is(":checked");
                        $("input[name='realFlightNo_" + (i + 1) + "']").val(detail.realFlightNo);
                        $("input[name='cabinCode_" + (i + 1) + "']").val(detail.cabinCode);

                        if (detail.fromDate) {
                            $("input[name='fromDate_" + (i + 1) + "']").val(formatTime(detail.fromDate).replace(/-/g, ""));
                        } else {
                            $("input[name='fromDate_" + (i + 1) + "']").addClass("requiredColor");
                        }
                        if (detail.toDate) {
                            $("input[name='toDate_" + (i + 1) + "']").val(formatTime(detail.toDate).replace(/-/g, ""));
                        } else {
                            $("input[name='toDate_" + (i + 1) + "']").addClass("requiredColor");
                        }
                        if (detail.fromCityCode) {
                            $("input[name='fromAirportCode_" + (i + 1) + "']").val(detail.fromCityCode);
                        } else {
                            $("input[name='fromAirportCode_" + (i + 1) + "']").addClass("requiredColor");
                        }
                        if (detail.toCityCode) {
                            $("input[name='toAirportCode_" + (i + 1) + "']").val(detail.toCityCode);
                        } else {
                            $("input[name='toAirportCode_" + (i + 1) + "']").addClass("requiredColor");
                        }
                        $("input[name='fromTerminal_" + (i + 1) + "']").val(detail.fromTerminal);
                        $("input[name='toTerminal_" + (i + 1) + "']").val(detail.toTerminal);
                        if (detail.fromTime) {
                            $("input[name='fromTime_" + (i + 1) + "']").val(detail.fromTime.replace(":", ""));
                        } else {
                            $("input[name='fromTime_" + (i + 1) + "']").addClass("requiredColor");
                        }
                        if (detail.toTime) {
                            $("input[name='toTime_" + (i + 1) + "']").val(detail.toTime.replace(":", ""));
                        } else {
                            $("input[name='toTime_" + (i + 1) + "']").addClass("requiredColor");
                        }

                        $("input[name='aircraftType_" + (i + 1) + "']").val(detail.aircraftType);
                        $("input[name='stopOver_" + (i + 1) + "']").is(":checked");
                        $("input[name='stopCityName_" + (i + 1) + "']").val(detail.stopCityName);
                        $("input[name='stopTime_" + (i + 1) + "']").val(detail.stopTime);


                        if (detail.turningPoint) {
                            $("select[name='turningPoint_" + (i + 1) + "'] [value=" + 1 + "]").attr("selected", true);
                        } else {
                            $("select[name='turningPoint_" + (i + 1) + "'] [value=" + 0 + "]").attr("selected", true);
                        }
                        if (detail.baggageDirect) {
                            $("select[name='baggageDirect_" + (i + 1) + "'] [value=" + 1 + "]").attr("selected", true);
                        } else {
                            $("select[name='baggageDirect_" + (i + 1) + "'] [value=" + 0 + "]").attr("selected", true);
                        }
                        if (detail.transitVisa) {
                            $("select[name='transitVisa_" + (i + 1) + "'] [value=" + 1 + "]").attr("selected", true);
                        } else {
                            $("select[name='transitVisa_" + (i + 1) + "'] [value=" + 0 + "]").attr("selected", true);
                        }

                        $("select[name='turningPoint_" + (i + 1) + "']").trigger("change");

                        // $("input[name='baggageRule_"+(i + 1)+"']").addClass("requiredColor");
                        // $("input[name='refundRule_"+(i + 1)+"']").addClass("requiredColor");

                    }

                }

                $(".requiredColor").blur(function () {
                    if ($(this).val() != '') {
                        $(this).removeClass("requiredColor");
                    }
                });
                $(".requiredColor").mouseover(function () {
                    if ($(this).val() != '') {
                        $(this).removeClass("requiredColor");
                    }
                });
                showIndex = showIndex + 1;
                $("#partnerName").val($("input[name='pname']").val() + "方案" + showIndex);
            },
            error: function (e) {
                console.log(e);
                layer.closeAll();
                layer.msg("系统异常");
            }
        });

    });


}


//行程事件绑定
function bindProcessEvent(process) {
    //共享航班、经停航班控制
    $('.wd', process).change(function () {
        var v = $(this).prop('checked');
        var input = $('.wd_input', $(this).parent().parent());
        if (v) {
            input.removeAttr('disabled');
        } else {
            input.attr('disabled', 'disabled');
            input.val("");
            input.siblings(".promptError").remove();
        }
    });

    //清空
    $('.clearX', process).click(function () {
        $('input', $(this).parent().parent()).not(".baggageType").val(null);
        $("label.promptError", $(this).parent().parent()).remove();

    });

    //删除
    $('.icon-close', process).click(function () {
        if ($('.ftTravel .process').not('.defaultDiv').length == 1) {
            $('input', $(this).parent().parent()).not(".baggageType").val(null);
            $("label.promptError", $(this).parent().parent()).remove();
            return;
        }
        $(this).parent().parent().parent().remove();
        $("#detailsCheck .checkSpan").not(".defaultDiv").eq(0).remove();
        reOrderProcess();
    });

    //金额数
    $("input[name='singleTicketPrice']").blur(function () {
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });
    $("input[name='singleTaxFee']").blur(function () {
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });
    $("input[name='singleExtraFee']").blur(function () {
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });
    $("input[name='singleSettelPrice']").blur(function () {
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });
    $("input[name='singleSettelTaxFee']").blur(function () {
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });

    $(".fromDate").jeDate({
        format: "YYYYMMDD",
    })

    $(".toDate").jeDate({
        format: "YYYYMMDD",
    })

    $(".baggageType").click(function () {
        var value = $(this).val();
        var pDiv = $(this).parents(".newBaggage");
        if (value == 1) {
            pDiv.find(".piece").attr("disabled", "disabled");
            pDiv.find(".pieceWeight").attr("disabled", "disabled");
            pDiv.find(".weight").attr("disabled", "disabled");
        } else if (value == 2) {
            pDiv.find(".piece").removeAttr("disabled");
            pDiv.find(".pieceWeight").removeAttr("disabled");
            pDiv.find(".weight").attr("disabled", "disabled");
        } else if (value == 3) {
            pDiv.find(".piece").attr("disabled", "disabled");
            pDiv.find(".pieceWeight").attr("disabled", "disabled");
            pDiv.find(".weight").removeAttr("disabled");

        }
    });

    $(".turningPoint").change(function () {
        var turningPoint = $(this).val();
        var pDiv = $(this).parents(".stopFlight");
        if (turningPoint == 0) {
            pDiv.find(".baggageDirect").attr("disabled", "disabled");
            pDiv.find(".transitVisa").attr("disabled", "disabled");
        } else {
            pDiv.find(".baggageDirect").removeAttr("disabled");
            pDiv.find(".transitVisa").removeAttr("disabled");
        }
    });

}


//添加行程
function addProcess() {
    if ($('.ftTravel .process').not('.defaultDiv').length >= pageVal.titles.length) {
        layer.open({
            content: '最多添加' + pageVal.titles.length + '个行程'
        });
        return;
    }

    var newProcess = $('.ftTravel .defaultDiv').clone();
    newProcess.removeClass('defaultDiv');
    $('.ftTravel').append(newProcess);

    //退改行程增加一个
    var checkSpan = $("#detailsCheck").find(".defaultDiv").clone();
    checkSpan.removeClass("defaultDiv");
    $("#detailsCheck").append(checkSpan);

    bindProcessEvent(newProcess);
    reOrderProcess();

}

//重新排序行程
function reOrderProcess() {
    var processes = $('.ftTravel .process').not('.defaultDiv');
    var titles = pageVal.titles;

    $.each(processes, function (i, process) {
        var originIndex = $(this).find("input[name='digital']").val();
        $('.process_title', process).text('第' + titles[i] + '程');
        $(this).find("input[name='digital']").val(i + 1);
        $(this).find(".flightNo").attr("name", "flightNo_" + (i + 1));
        $(this).find(".realFlightNo").attr("name", "realFlightNo_" + (i + 1));
        $(this).find(".cabinCode").attr("name", "cabinCode_" + (i + 1));
        $(this).find(".fromDate").attr("name", "fromDate_" + (i + 1));
        $(this).find(".toDate").attr("name", "toDate_" + (i + 1));
        $(this).find(".fromAirportCode").attr("name", "fromAirportCode_" + (i + 1));
        $(this).find(".fromTerminal").attr("name", "fromTerminal_" + (i + 1));
        $(this).find(".toAirportCode").attr("name", "toAirportCode_" + (i + 1));
        $(this).find(".toTerminal").attr("name", "toTerminal_" + (i + 1));
        $(this).find(".fromTime").attr("name", "fromTime_" + (i + 1));
        $(this).find(".toTime").attr("name", "toTime_" + (i + 1));
        $(this).find(".aircraftType").attr("name", "aircraftType_" + (i + 1));
        $(this).find(".stopOver").attr("name", "stopOver_" + (i + 1));
        $(this).find(".stopCityName").attr("name", "stopCityName_" + (i + 1));
        $(this).find(".stopTime").attr("name", "stopTime_" + (i + 1));
        $(this).find(".baggageRule").attr("name", "baggageRule_" + (i + 1));
        $(this).find(".refundRule").attr("name", "refundRule_" + (i + 1));
        $(this).find(".specialRule").attr("name", "specialRule_" + (i + 1));
        $(this).find(".share").attr("name", "share_" + (i + 1));

        $(this).find(".baggageType").attr("name", "baggageType_" + (i + 1));
        $(this).find(".piece").attr("name", "piece_" + (i + 1));
        $(this).find(".pieceWeight").attr("name", "pieceWeight_" + (i + 1));
        $(this).find(".weight").attr("name", "weight_" + (i + 1));

        $(this).find(".turningPoint").attr("name", "turningPoint_" + (i + 1));
        $(this).find(".baggageDirect").attr("name", "baggageDirect_" + (i + 1));
        $(this).find(".transitVisa").attr("name", "transitVisa_" + (i + 1));

    });

    var detailChecks = $("#detailsCheck .checkSpan").not(".defaultDiv");
    $.each(detailChecks, function (i, process) {
        $(this).find(".detailNo").text('第' + titles[i] + '程');
    });
}


//取两位小数
function toDecimal2(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return false;
    }
    var f = Math.round(x * 100) / 100;
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
        rs = s.length;
        s += '.';
    }
    while (s.length <= rs + 2) {
        s += '0';
    }
    return s;
}

//计算页面价格 显示
function calculationPrice() {

    if ($("input[name='singleTicketPrice']").val() == '') {
        return;
    }
    if ($("input[name='singleTaxFee']").val() == '') {
        return;
    }
    if ($("input[name='singleExtraFee']").val() == '') {
        return;
    }
    if ($("input[name='singleSettelPrice']").val() == '') {
        return;
    }
    if ($("input[name='singleSettelTaxFee']").val() == '') {
        return;
    }
    var singleTicketPrice = parseFloat($("input[name='singleTicketPrice']").val());
    var singleTaxFee = parseFloat($("input[name='singleTaxFee']").val());
    var singleExtraFee = parseFloat($("input[name='singleExtraFee']").val());
    var singleSettelPrice = parseFloat($("input[name='singleSettelPrice']").val());
    var singleSettelTaxFee = parseFloat($("input[name='singleSettelTaxFee']").val());
    var passengerNum = parseInt($("#passengerNum").text());


    var totalSalePrice = passengerNum * (singleTicketPrice + singleTaxFee + singleExtraFee);
    var totalSettelPrice = passengerNum * (singleSettelPrice + singleSettelTaxFee);
    var profit = totalSalePrice - totalSettelPrice;

    $("#totalSalePrice").text(toDecimal2(totalSalePrice));
    $("#totalSettelPrice").text(toDecimal2(totalSettelPrice));
    $("#profit").text(toDecimal2(profit));

}

//转大写
function toUpper(obj) {
    $(obj).val($(obj).val().toUpperCase());
}

function formatTime(time) {
    if (time == null) {
        return null;
    }
    var date = new Date(time);
    var m = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    var d = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    return date.getFullYear() + "-" + m + "-" + d;
}
