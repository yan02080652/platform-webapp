//图片预览
function showPic(e,imgUrl){ 
    var x,y; 
    x = e.clientX; 
    y = e.clientY;
    y += window.scrollY;
    document.getElementById("pic1").style.left = x-200+'px'; 
    document.getElementById("pic1").style.top = y-400+'px'; 
    document.getElementById("pic1").innerHTML = "<img border='0' src=\"" + imgUrl + "\" width='400px;' height='400px;'>"; 
    document.getElementById("pic1").style.display = ""; 
} 
function hiddenPic(){ 
    document.getElementById("pic1").innerHTML = ""; 
    document.getElementById("pic1").style.display = "none"; 
} 
//图片下载
function download(src) {
    var $a = $("<a></a>").attr("href", src).attr("download", "img.png");
    $a[0].click();
}

//审核通过
function aduitPass(){
	var refundPrice = $("#refundPrice").val() || 0;
	if(parseFloat(refundPrice) < 0){
		layer.alert("价格信息有误，请更正！")
		return;
	}

	var orderId = $("#orderId").val();
	var refundOrderId = $("#refundOrderId").val();
	var userId = $("#userId").val();
	var taskId = $("#taskId").val();
	
	var refundFee = $("#num1").val();
	var adjustFee = $("#num2").val();
	var refundServiceFee = $("#num3").val();

	var refundCount = $("#refundCount").val();
	
	
	//毛利
	var profit = (parseFloat(adjustFee) + parseFloat(refundServiceFee)) * parseFloat(refundCount) ;
	
	
	
	layer.confirm('是否通过退票？退单后毛利：'+profit+'元', { btn: ['确认','取消'],title:"提示"},
		function(index){//确认
            layer.close(index);
			var c_index = layer.load();
			$.ajax({
				type:"post",
				url:"internationalflight/tmc/refundAduitPass",
				data:{
					orderId:orderId,
					id:refundOrderId,
					refundFee:refundFee,
					adjustFee:adjustFee,
					refundServiceFee:refundServiceFee,
					userId:userId,
					taskId:taskId,
					refundCount:refundCount,
				},
				success:function(data){
					layer.close(c_index);
					if(data){
						parent.layer.msg("退票任务完成：方式(审核通过)",{icon:1,offset:'t',time:1000});
					}else{
						parent.layer.msg("系统退款失败，请稍后重试！",{icon:2,offset:'t',time:1000});
					}
					parent.submitAjax();
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
					
				},
			});
		}, 
		function(){//取消
		});
}

//拒绝退票
function refuseRefund (){
	var orderId = $("#orderId").val();
	var refundOrderId = $("#refundOrderId").val();
	var taskId = $("#taskId").val();
	layer.prompt({
		  formType: 2,//输入框1为input，2为文本域
		  title: '请填写拒绝退票原因',
		}, function(value, index, elem){
			if($.trim(value).length>0){
                var c_index = layer.load();
				$.ajax({
					type:"post",
					url:"internationalflight/tmc/refundAduitNoPass",
					data:{
						orderId:orderId,
						id:refundOrderId,
						refuseReason:$.trim(value),
						taskId:taskId,
					},
					success:function(data){
                        layer.close(c_index);
						if(data){
							parent.layer.msg("退票任务完成：方式(拒绝退票)",{icon:1,offset:'t',time:1000});
						}else{
							parent.layer.msg("系统错误",{icon:2,offset:'t',time:1000});
						}
						parent.submitAjax();
						var index = parent.layer.getFrameIndex(window.name);
						parent.layer.close(index);
					}
				});
			}
		});
}

//暂不处理
function cancel(){
	parent.layer.closeAll();
}


function calculation(obj,id){
	var num =  parseFloat($(obj).val());
	var refundCount = $("#refundCount").val();
	if(!isNaN(num)){
		$(id).text(num*refundCount);
	}else{
		$(id).text(0);
	}
}

//更新价格
$(".inputNum").keyup(function(){
	var sum = 0;
	$(".inputNum").each(function(){
		var num = $(this).val().trim();
		if(num.length > 0 && !isNaN(num)){
			sum += parseFloat(num);
		}
	});
	var refundCount = $("#refundCount").val();
	$("#refundPrice").val(parseFloat($("#salePrice").text())-sum);
	$("#td4").text(parseFloat($("#refundPrice").val())*refundCount);
	
});

