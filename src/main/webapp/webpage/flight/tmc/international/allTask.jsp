<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
.tooltip {
  width: 24em;
}
</style>
<input type="hidden" id="currentTaskType" />

<div>
	<!-- 状态栏 -->
	<div class="tab pull-left">
		<ul class="fix" id="allTaskStatusTab">
			<li class="tab1 active" data-value="WAIT_PROCESS" onclick="reloadAllTask(null ,'WAIT_PROCESS')">待处理(${waitTaskTypeMap.WAIT_PROCESS==null?0:waitTaskTypeMap.WAIT_PROCESS})</li>
			<li class="tab2" data-value="PROCESSING" onclick="reloadAllTask(null,'PROCESSING')">处理中(${processingMap.PROCESSING==null?0:processingMap.PROCESSING})</li>
			<li class="tab3" data-value="ALREADY_PROCESSED" onclick="reloadAllTask(null,'ALREADY_PROCESSED')">近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})</li>
		</ul>
		<div id="f_all_content">
		<div class="content1">
			<p>
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_OFFER')">待核价(${waitTaskTypeMap.WAIT_OFFER==null?0:waitTaskTypeMap.WAIT_OFFER })</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_TICKET')">待出票(${waitTaskTypeMap.WAIT_TICKET==null?0:waitTaskTypeMap.WAIT_TICKET})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_REFUND')">待退款(${waitTaskTypeMap.WAIT_REFUND==null?0:waitTaskTypeMap.WAIT_REFUND})</a> &nbsp;|&nbsp;
			</p>
		</div>
		<div class="content2">
			<p>
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_OFFER')">待核价(${processingMap.WAIT_OFFER==null?0:processingMap.WAIT_OFFER })</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_TICKET')">待出票(${processingMap.WAIT_TICKET==null?0:processingMap.WAIT_TICKET})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_REFUND')">待退款(${processingMap.WAIT_REFUND==null?0:processingMap.WAIT_REFUND})</a> &nbsp;|&nbsp;
			</p>
		</div>
		<div class="content3">
			<p>
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_OFFER')">核价(${alreadyMap.WAIT_OFFER==null?0:alreadyMap.WAIT_OFFER })</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_TICKET')">出票(${alreadyMap.WAIT_TICKET==null?0:alreadyMap.WAIT_TICKET})</a> &nbsp;|&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_REFUND')">退款(${alreadyMap.WAIT_REFUND==null?0:alreadyMap.WAIT_REFUND})</a>
			</p>
		</div>
		</div>
	</div>
	<!-- 任务类型 -->
	<input type="hidden" id="task_type" value="WAIT_PROCESS"/>
	<input type="hidden" id="task_status" />
	
	<!-- 搜索条件 -->
	<div>
		<form class="form-inline" method="post" id="orderSearchForm">
			<nav>
				<div class=" form-group">
					<input  id="search_Field2" type="text" class="form-control" placeholder="订单号/处理人" style="width: 300px"/>				
					<button type="button" class="btn btn-default" onclick="reloadMyTaskTable()">搜索</button>
					<button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
				</div>
			</nav>
		</form>
	</div>
	<div>
		<label><font color="red" size="4">刷新倒计时:<span id="countDown">9</span>秒</font></label>
	</div>
</div>


<!-- 任务列表 -->
<div style="margin-top: 10px">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th style="width: 46%">航班信息</th>
				<%--<th style="width: 19%">乘客信息</th>--%>
				<th style="width: 19%">收支信息</th>
				<th style="width: 19%">处理信息</th>
				<th style="width: 19%">供应信息</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="orderTask">
				<tr>
				<td colspan="5">
					<span>企业:${orderTask.orderDto.cName }</span>
					<span>预订人:${orderTask.orderDto.userName }</span>
					<span>订单号:<font color="blue"><a href="/order/inter/orderDetail/${orderTask.orderDto.id }" target="_Blank">${orderTask.orderDto.id }</a></font></span>
					<span>来源:${orderTask.orderDto.orderOriginType.message}</span>
					<c:if test="${orderTask.taskType ne 'WAIT_REFUND'}">
						<span>标签:${orderTask.orderDto.orderShowStatus.message }</span>
					</c:if>

					<c:if test="${orderTask.taskType eq 'WAIT_REFUND'}">
						<span>标签:${orderTask.orderDto.refundStatusMsg }</span>
					</c:if>

					<span>下单时间:<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${orderTask.orderDto.createTime }" /></span>
					<span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span>
					<span>任务创建时间:<fmt:formatDate value="${orderTask.createDate }" pattern="MM-dd HH:mm"/></span>
					<span hidden><font color="red">任务超时</font></span>
					<input class="proDate" type="hidden" value='<fmt:formatDate value="${orderTask.maxProcessDate }" pattern="yyyy-MM-dd HH:mm:ss"/>'>
					<input type="hidden" value="${orderTask.taskStatus }"/>
					<c:if test="${orderTask.taskStatus == 'PROCESSING' }">
						<label>任务延时</label>
						<select style="width: 55px" onchange="delay('${orderTask.id }',$(this).val(),'${orderTask.operatorId }')">
							<option value="-1">--
							<option value="5">5分钟
							<option value="15">15分钟
							<option value="60">1小时
							<option value="1440">1天
						</select>
					</c:if>
				</td>
				</tr>
				<tr>
					<td>
						<c:forEach items="${orderTask.orderDto.details}" var="detail">

							<span style="display: inline-block;margin-bottom: 5px">
                                    ${detail.detailMessages} <br>

							</span>

						</c:forEach>
						<br>
						<span style="display: inline-block;margin-top: 15px">
								出行人: ${orderTask.userNames}
						</span>

					</td>

					<td>
						<span>应收￥${orderTask.orderDto.totalSalePrice }</span>
						<span>
							<c:choose>
								<c:when test="${orderTask.orderDto.paymentStatus eq 'PAYMENT_SUCCESS' }">已收</c:when>
								<c:otherwise>未收</c:otherwise>
							</c:choose>
						</span><br/>
						<c:if test="${not empty orderTask.orderDto.totalSettlePrice}">
							<span>应付￥${orderTask.orderDto.totalSettlePrice }</span>
							<span>
								<c:choose>
									<c:when test="${not empty orderTask.orderDto.externalOrderStatus and (orderTask.orderDto.externalOrderStatus eq 'WAIT_ISSUE' or orderTask.orderDto.externalOrderStatus eq 'ISSUED' or orderTask.orderDto.externalOrderStatus eq 'PAYING')}">
										已付
									</c:when>
									<c:otherwise>未付</c:otherwise>
								</c:choose>
							</span><br/>
						</c:if>

						<c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
							<span><a onclick="loadIncomeDetail('${orderTask.orderDto.id}','${orderTask.orderDto.paymentPlanNo}')">明细</a></span>
						</c:if>
					</td>
					<td>
						<span>订单状态：${orderTask.orderDto.orderShowStatus.message } (已下单)</span><br/>
						<span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>
						<c:if test="${not empty orderTask.operatorId }">
							<span>处理人：${orderTask.operatorName }</span><br/>
						</c:if>

						<c:if test="${orderTask.taskStatus eq 'WAIT_PROCESS' }">
							<span><a onclick="transfer('${orderTask.id}','${orderTask.operatorId }',false)">分配</a></span><br/>
						</c:if>

						<c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' }">
							<span>处理结果：${orderTask.result}</span>
						</c:if>
					</td>
					<td>

						<!-- 有供应商订单信息 -->
						<c:if test="${not empty orderTask.orderDto.externalOrderId }">
							<a data-trigger="tooltip" data-content="${orderTask.orderDto.externalOrderId }">订单号</a><br/>
						</c:if>
						<c:if test="${not empty orderTask.orderDto.pnrCode }">
							PNR:${orderTask.orderDto.pnrCode } <br/>
						</c:if>

						<c:if test="${orderTask.ticketType eq 'OFFLINE' and orderTask.orderDto.supplierLiqStatus ne 'CLOSE'}">
							<a onclick="ticketPopup(${orderTask.orderDto.id})">线下出票修订</a>
						</c:if>

					</td>
				</tr>
			</c:forEach>		
		</tbody>
	</table>
</div>

<div class="pagin" style="margin-bottom: 20px">
	<jsp:include page="../../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="webpage/flight/tmc/international/js/allTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(function(){
	var status = "${taskStatus}";
	$("#allTaskStatusTab li").each(function(){
		if($(this).attr("data-value")==status){
			$(this).addClass("active");
			$(this).siblings().removeClass("active");
			if(status == 'WAIT_PROCESS'){
				 $('#f_all_content .content1').show();
			     $('#f_all_content .content2').hide();
			     $('#f_all_content .content3').hide();
			}else if(status=='PROCESSING'){
				 $('#f_all_content .content1').hide();
				 $('#f_all_content .content2').show();
			     $('#f_all_content .content3').hide();
			}else if(status=='ALREADY_PROCESSED'){
				 $('#f_all_content .content1').hide();
			     $('#f_all_content .content2').hide();
			     $('#f_all_content .content3').show();
			}
			return false;
		}
	});
});
</script>