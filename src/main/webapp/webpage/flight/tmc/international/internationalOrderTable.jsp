<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .tooltip {
        width: 24em;
    }

</style>

<!-- 订单状态选项 -->
<div class="tab" style="margin-bottom: 5px">
    <ul class="fix" id="orderStatus">
        <c:forEach items="${OrderStatuslist }" var="orderStatus">
            <c:choose>
                <c:when test="${orderStatus.orderShowStatus eq currentOrderStatus }">
                    <li class="active"><span style="width: 130px"
                                             data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.message }(${orderStatus.count })</span>
                    </li>
                </c:when>
                <c:otherwise>
                    <li><span
                            data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.message }(${orderStatus.count })</span>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </ul>
</div>

<!-- 订单列表 -->

<table class="table table-bordered" id="orderTable">
    <thead>
    <tr>
        <th style="width: 46%">航班信息</th>
        <%--<th style="width: 19%">乘客信息</th>--%>
        <th style="width: 19%">收支信息</th>
        <th style="width: 19%">处理信息</th>
        <th style="width: 19%">供应信息</th>
    </tr>
    </thead>
    <tbody>

    <c:set var="nowDate" value="<%=System.currentTimeMillis()%>"></c:set>

    <c:forEach items="${pageList.list }" var="flightOrder">
        <tr>
            <td colspan="5">
                <span>企业：${flightOrder.cName }</span>
                <span>预订人：${flightOrder.userName }</span>
                <span>订单号：<font color="blue"><a href="/order/inter/orderDetail/${flightOrder.id }"
                                                target="_Blank">${flightOrder.id }</a></font></span>
                <span>来源:${flightOrder.orderOriginType.message}</span>
                <span>标签：${flightOrder.orderShowStatus.message }</span>
                <span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${flightOrder.createTime }"/></span>
                <span><a onclick="logList('${flightOrder.id}')">日志</a></span>
                <c:if test="${flightOrder.orderShowStatus eq 'IN_ISSUE' and (nowDate-flightOrder.updateTime.time)/(1000*60) > 5}">
                    &nbsp;&nbsp;
                    <span><font color="red">订单出票超时！</font></span>
                </c:if>
            </td>
        </tr>
        <tr>
            <td>

                <c:forEach items="${flightOrder.details}" var="detail">

							<span style="display: inline-block;margin-bottom: 5px">
                                    ${detail.detailMessages} <br>

							</span>

                </c:forEach>
                <br>
                <span style="display: inline-block;margin-top: 15px">
								出行人: <c:forEach items="${flightOrder.passengers }" var="passenger" varStatus="vs">
											<c:if test="${!vs.last }">${passenger.surName }/${passenger.givenName} 、 </c:if>
											<c:if test="${vs.last }">${passenger.surName }/${passenger.givenName}</c:if>
									   </c:forEach>
							</span>

            </td>

            <td style="vertical-align: middle">
                <span>应收￥${flightOrder.totalSalePrice }</span>
                <span>
								<c:choose>
									<c:when test="${flightOrder.paymentStatus eq 'PAYMENT_SUCCESS' }">已收</c:when>
									<c:otherwise>未收</c:otherwise>
								</c:choose>
							</span><br/>
                <c:if test="${not empty flightOrder.totalSettlePrice }">
                    <span>应付￥${flightOrder.totalSettlePrice }</span>
                    <span>
									<c:choose>
										<c:when test="${not empty flightOrder.externalOrderStatus and (flightOrder.externalOrderStatus eq 'WAIT_ISSUE' or flightOrder.externalOrderStatus eq 'ISSUED' or flightOrder.externalOrderStatus eq 'PAYING')}">
											已付
										</c:when>
										<c:otherwise>未付</c:otherwise>
									</c:choose>
								</span><br/>
                </c:if>
                <c:if test="${not empty flightOrder.paymentPlanNo }">
                    <span><a
                            onclick="loadIncomeDetail('${flightOrder.id}','${flightOrder.paymentPlanNo}')">明细</a></span>
                </c:if>
            </td>
            <td style="vertical-align: middle">
							<span>
								订单状态：${flightOrder.orderShowStatus.message }(${flightOrder.orderStatus.message })
							</span><br/>
                <c:if test="${not empty flightOrder.task}">
								<span>
									任务：${flightOrder.task.taskType.message }(${flightOrder.task.taskType.message })
								</span><br/>
                    <c:if test="${not empty flightOrder.task.operatorName and flightOrder.task.operatorName !=''}">
                        <span>处理人：${flightOrder.task.operatorName }</span><br/>
                    </c:if>
                </c:if>

            </td>
            <td style="vertical-align: middle">
							<span>

								<c:if test="${not empty flightOrder.externalOrderId }">
                                    <a data-trigger="tooltip"
                                       data-content="${flightOrder.externalOrderId }">订单号</a><br/>
                                </c:if>
								<c:if test="${not empty flightOrder.pnrCode }">
                                    PNR:${flightOrder.pnrCode }
                                </c:if>


							</span>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin" style="margin-bottom: 20px">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=reloadOrderList"></jsp:include>
</div>
<script src="webpage/flight/tmc/international/js/internationalOrderList.js?version=${globalVersion}"></script>