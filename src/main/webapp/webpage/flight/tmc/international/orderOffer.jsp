<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" autoFlush="false" buffer="600kb"%>
<link rel="stylesheet" href="/resource/css/order/ainteflight.css?version=${globalVersion}"/>

<style>
    /*页面验证信息提示*/
    label.promptError {
        line-height: 1;
        position: absolute;
        display: block;
        padding: 6px 12px;
        color: white;
        font-size: 12px;
        left: 8px;
        top: -24px;
        background: #e53935;
    }

    label.promptError:after {
        position: absolute;
        top: 18px;
        left: 4px;
    }
    .newBaggage input[type='radio'] {
        width: 15px;
        margin-top: 13px;
        margin-left: 5px;

    }
    .newBaggage select {
        margin-top: 11px;
    }
    .newBaggage label {
        font-weight: 500;
        margin-right: 3px;
    }

    .newBaggage span {
        color: red;
    }
    .m-table-container{
        width: 100%;
        border: 1px #ddd solid;
        border-bottom: 0;
    }
    .m-table-container thead tr th{
        background: #999;
        padding: 5px 12px;
        color: #fff;
        border-bottom: 1px #ddd solid;
        border-right: 1px #ddd solid;
    }
    .m-table-container thead tr th:last-child{
        border-right: 0;
    }
    .m-table-container tbody tr td{
        padding: 5px 12px;
        /*color: #fff;*/
        border-bottom: 1px #ddd solid;
        border-right: 1px #ddd solid;
    }
    .m-table-container tbody tr td input{
        width: 100%;
        border-radius: 2px;
        padding: 5px 12px;
        box-sizing: border-box;
        height: 28px;
    }
    .m-table-container tbody tr td:last-child{
        border-right: 0;
    }
    .m-table-container input[type='checkbox'] {
        width: 10px;
    }
    input:disabled {
        background: #bbbbbb;
    }
    select:disabled {
        background: #bbbbbb;
    }
    .layui-layer-page .layui-layer-content{
        overflow: visible;
    }
    .btnwrap{
        display: flex;
        justify-content: space-between;
        width: 100%;
    }
    .btnclose,.btncopy{
        display: inline-block;
        width:46%;
        height:35px;
        line-height: 35px;
        text-align: center;
        border-radius: 8px;
        background-color: #fff;
        cursor: pointer;
    }
    .btncopy:hover{
        color:dodgerblue;
    }
</style>


<div class="flightTravel">
    <form id="interFlight">
        <!--行程-->
        <div class="ftInfo ftTravel">
            <div class="travelTitle clearfix">
                <span class="title">行程</span>


                <div class="trvType">
                    <a style="padding-right: 30px" class="pnrImport">PNR导入</a>
                    行程类型:
                    <input type="radio" name="tripType" checked value="OW"/>单程
                    <input type="radio" name="tripType"  value="RT"/>往返

                    <a class="addTrvbtn">添加行程</a>
                </div>
            </div>
            <div class="process clearfix defaultDiv">
                <div class="process_title fl">第一程</div>
                <input type="hidden" name="digital">
                <div class="proInfo fl">
                    <div class="clearX"><a class="clearcon">清空</a> <i class="iconfont icon-close"></i></div>
                    <div class="flightNo">
                        <div class="fnDiv">
                            <label>航班号</label><span>*</span><br>
                            <input type="text" placeholder="编号" name="flightNo" class="flightNo" onblur="toUpper(this)"/>
                        </div>
                        <div class="fnDiv" style="width:12%;">
                            <input class="wd share" type="checkbox" name="share"/><label style="margin-left: 16px">共享航班</label><span>*</span><br/>
                            <input type="text" disabled="disabled" style="width: 93px" name="realFlightNo" placeholder="实际承运航班号" onblur="toUpper(this)" class="wd_input realFlightNo"/>
                        </div>
                        <div class="fnDiv">
                            <label>舱位编码</label><span>*</span><br/>
                            <input type="text" placeholder="编码" name="cabinCode" class="cabinCode" onblur="toUpper(this)"/>
                        </div>
                        <div class="fnDiv">
                            <label>起飞日期</label><span>*</span><br/>
                            <input type="text" placeholder="YYYYMMDD" name="fromDate" class="fromDate" readonly/>
                        </div>
                        <div class="fnDiv">
                            <label>到达日期</label><span>*</span><br/>
                            <input type="text" placeholder="YYYYMMDD" name="toDate" class="toDate" readonly/>
                        </div>
                        <div class="fnDiv">
                            <label>出发机场</label><span>*</span><br/>
                            <input type="text" placeholder="三字码" name="fromAirportCode" onblur="toUpper(this)" class="fromAirportCode"/>
                        </div>
                        <div class="fnDiv">
                            <label>出发航站楼</label><br/>
                            <input type="text" placeholder="编码" name="fromTerminal" onblur="toUpper(this)" class="fromTerminal"/>
                        </div>
                        <div class="fnDiv">
                            <label>到达机场</label><span>*</span><br/>
                            <input type="text" placeholder="三字码" name="toAirportCode" onblur="toUpper(this)" class="toAirportCode"/>
                        </div>
                        <div class="fnDiv">
                            <label>到达航站楼</label><br/>
                            <input type="text" placeholder="编码" name="toTerminal" onblur="toUpper(this)" class="toTerminal"/>
                        </div>
                        <div class="fnDiv">
                            <label>起飞时间</label><span>*</span><br/>
                            <input type="text" placeholder="HHMM" name="fromTime" class="fromTime"/>

                        </div>
                        <div class="fnDiv">
                            <label>到达时间</label><span>*</span><br/>
                            <input type="text" placeholder="HHMM" name="toTime" class="toTime"/>
                        </div>
                        <div class="fnDiv">
                            <label>机型</label><br/>
                            <input type="text" placeholder="录入机型" name="aircraftType"  class="aircraftType"/>
                        </div>
                    </div>
                    <div class="stopFlight">
                        <p class="fnDiv" style="text-indent: 16px; line-height: 20px;">
                            <input class="wd stopOver" type="checkbox" name="stopOver" />经停航班
                        </p>
                        <div class="fnDiv" style="width: 10.7%">
                            <label>经停城市</label><span>*</span><br/>
                            <input type="text" style="width: 100px" name="stopCityName" placeholder="城市名称" disabled="disabled" class="wd_input stopCityName"/>
                        </div>
                        <div class="fnDiv">
                            <label>经停时长</label><span>*</span><br/>
                            <input type="text" placeholder="HHMM" name="stopTime" disabled="disabled" class="wd_input stopTime"/>
                        </div>
                        <div class="newBaggage" style="position: relative">
                            <label>免费托运行李</label><span>*</span><br/>

                            <input type="radio" name="baggageType" class="baggageType" value="1"><label>无</label>

                            <input type="radio" name="baggageType" class="baggageType" value="2">
                            <select name="piece" class="piece">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select><label> 件,</label>
                            <label>每件</label>
                            <select name="pieceWeight" class="pieceWeight">
                                <option value="7">7</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="23">23</option>
                                <option value="25">25</option>
                                <option value="30">30</option>
                                <option value="32">32</option>
                                <option value="35">35</option>
                                <option value="40">40</option>
                                <option value="45">45</option>
                                <option value="50">50</option>
                                <option value="55">55</option>
                                <option value="60">60</option>
                                <option value="70">70</option>
                            </select>
                            <label>公斤</label>

                            <input type="radio" name="baggageType" class="baggageType" value="3"><label>公斤</label>
                            <select name="weight" class="weight">
                                <option value="7">7</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                                <option value="23">23</option>
                                <option value="25">25</option>
                                <option value="30">30</option>
                                <option value="32">32</option>
                                <option value="35">35</option>
                                <option value="40">40</option>
                                <option value="45">45</option>
                                <option value="50">50</option>
                                <option value="55">55</option>
                                <option value="60">60</option>
                                <option value="70">70</option>
                            </select>

                        </div>

                        <div class="newBaggage" style="margin-left: 260px">
                            <label>是否转机</label><span>*</span><br/>
                            <select name="turningPoint" class="turningPoint">
                                <option value="1">是</option>
                                <option value="0" selected>否</option>
                            </select>
                        </div>

                        <div class="newBaggage" style="margin-left: 10px">
                            <label>行李直达</label><span>*</span><br/>
                            <select name="baggageDirect" class="baggageDirect" disabled>
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </div>

                        <div class="newBaggage" style="margin-left: 10px">
                            <label>需过境签</label><span>*</span><br/>
                            <select name="transitVisa" class="transitVisa" disabled>
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </div>

                    </div>

                </div>
            </div>

        </div>

        <div class="ftInfo ftUser" style="margin-top: 0px">
            <div class="travelTitle clearfix">
                <span class="title">退改</span>
                <span style="margin-top: 13px;font-size: small;margin-left: 30px">请按"金额<font color="#3cb371">&</font>起飞前几小时<font color="#3cb371">&</font>金额"录入退改费，或 "-1" 表示"以航司为准"，"-2" 表示"不可退改"。如："800&2&-2"表示"起飞两小时前收取800元，起飞前2小时以内及起飞后不得退票"</span>
            </div>

            <table class="m-table-container">
                <thead>
                <th>适用航段</th>
                <th>退票</th>
                <th>改期</th>
                <th>操作</th>
                </thead>
                <tr>
                    <td style="width: 23%" id="detailsCheck">
                        <span class="checkSpan defaultDiv">
                            <span>
                                <input type="checkbox" checked disabled>
                            </span>
                            <span class="detailNo">第一程</span>
                        </span>

                    </td>
                    <td style="position: relative"><input type="text" name="refundRule"></td>
                    <td style="position: relative"><input type="text" name="changeRule"></td>
                    <td></td>
                </tr>
                <tr>
                    <td><span style="float: right;font-weight: 700">特殊说明</span></td>
                    <td><input type="text" name="specialRefundRule"></td>
                    <td><input type="text" name="specialChangeRule"></td>
                    <td></td>
                </tr>
            </table>


        </div>

        <!--金额-->
        <div class="ftInfo ftMoney">
            <span class="title">金额</span>
            <table class="moneyTab" cellpadding="1" cellspacing="1">
                <thead>
                <td>单人销售票价<span>*</span></td>
                <td>单人销售税费<span>*</span></td>
                <td>单人服务费<span>*</span></td>
                <td>单人结算票价<span>*</span></td>
                <td>单人结算税费<span>*</span></td>
                </thead>
                <tr>
                    <td style="position: relative"><input type="number" name="singleTicketPrice"/></td>
                    <td style="position: relative"><input type="number" name="singleTaxFee"/></td>
                    <td style="position: relative"><input type="number" name="singleExtraFee"/></td>
                    <td style="position: relative"><input type="number" name="singleSettelPrice"/></td>
                    <td style="position: relative"><input type="number" name="singleSettelTaxFee"/></td>
                </tr>
            </table>
            <div class="CNY">
                <label>人数：<span id="passengerNum">1</span>人</label>
                <label>客户支付总额：CNY<span class="rd" id="totalSalePrice">0.00</span></label>
                <label>供应商结算总额：CNY<span id="totalSettelPrice">0.00</span></label>
                <label>毛利：CNY<span class="rd" id="profit">0.00</span></label>
            </div>
        </div>

        <!--btn-->

        <div class="interBtn">

            <span>
            <input style="width: 200px;padding: 5px 12px;height: 40px;margin-left: 400px" id="partnerName">
                <a style="display: table-cell" class="formBtn formBtnGray">创建报价模版</a>
            </span>

        </div>

    </form>

    <input name="pname" value="${partner.alias}" hidden>

</div>


<script src="/resource/plugins/layer/tree.js?version=${globalVersion}"></script>
<script type="text/javascript" src="/resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>
<link rel="stylesheet" href="/resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>

<script src="/webpage/flight/tmc/international/js/orderOffer.js?version=${globalVersion}"></script>
<%--<script src="/resource/plugins/expenseAttribution.js?version=${globalVersion}"></script>--%>


<div class="HFdiv" hidden>
    <span class="HFtitle">PNR导入</span>
    <textarea class="bkDiv" contenteditable="true" >

    </textarea>
    <div class="btn">
        <a class="btnC">关闭</a>
        <a class="btnD">导入</a>

    </div>
</div>

<input type="hidden" id="keyWord">

<div id="showIframe" hidden>
    <iframe width="400px" height="600px"  id="iframePic" src="http://m.z-trip.cn/flight/template" onload="frameLoaded()">

    </iframe>
    <div class="btnwrap">
        <span class="btnclose">关闭</span>
        <span class="btncopy">生成图片</span>
    </div>
</div>
