<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
.tooltip {
  width: 24em;
}
</style>
<!-- 状态栏 -->
<input type="hidden" id="currentTaskType" />
<div>
	<!-- 左边状态 -->
	<div class="tab pull-left">
            <ul class="fix" id="myTaskStatusTab">
                <li class="tab1 active" data-value="PROCESSING" onclick="submitAjax(null,'PROCESSING')">处理中(${processingMap.PROCESSING==null?0:processingMap.PROCESSING})</li>
                <li class="tab2" data-value="ALREADY_PROCESSED" onclick="submitAjax(null,'ALREADY_PROCESSED')">近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})</li>
            </ul>
		<div id="f_my_content">
            <div class="content1">
                <p >
               		<a onclick="submitAjax(null,'PROCESSING','WAIT_OFFER')">待核价(${processingMap.WAIT_OFFER==null?0:processingMap.WAIT_OFFER })</a>
                	<a onclick="submitAjax(null,'PROCESSING','WAIT_TICKET')">待出票(${processingMap.WAIT_TICKET==null?0:processingMap.WAIT_TICKET})</a>
                	<a onclick="submitAjax(null,'PROCESSING','WAIT_REFUND')">待退款(${processingMap.WAIT_REFUND==null?0:processingMap.WAIT_REFUND})</a>
                </p>
            </div>
            <div class="content2">
                <p >&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_OFFER')">核价(${alreadyMap.WAIT_OFFER==null?0:alreadyMap.WAIT_OFFER })</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_TICKET')">出票(${alreadyMap.WAIT_TICKET==null?0:alreadyMap.WAIT_TICKET})</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_REFUND')">退款(${alreadyMap.WAIT_REFUND==null?0:alreadyMap.WAIT_REFUND})</a>&nbsp;&nbsp;&nbsp;&nbsp;
                </p>
            </div>
		</div>
     </div>

</div>


<!-- 任务列表 -->
<div>
	<table class="table table-bordered" >
		<thead>
			<tr>
				<th style="width: 46%">航班信息</th>
				<%--<th style="width: 19%">乘客信息</th>--%>
				<th style="width: 19%">收支信息</th>
				<th style="width: 19%">处理信息</th>
				<th style="width: 19%">供应信息</th>
			</tr>
		</thead>
		<tbody id="myTaskTbody">
			<c:forEach items="${pageList.list }" var="orderTask" varStatus="vs">
				<tr>
					<td colspan="5">
						<span>企业:${orderTask.orderDto.cName }</span>
						<span>预订人:${orderTask.orderDto.userName }</span>
						<span>订单号:<font color="blue"><a href="/order/inter/orderDetail/${orderTask.orderDto.id }" target="_Blank">${orderTask.orderDto.id }</a></font></span>
						<span>来源:${orderTask.orderDto.orderOriginType.message}</span>

						<c:if test="${orderTask.taskType ne 'WAIT_REFUND'}">
							<span>标签:${orderTask.orderDto.orderShowStatus.message }</span>
						</c:if>

						<c:if test="${orderTask.taskType eq 'WAIT_REFUND'}">
							<span>标签:${orderTask.orderDto.refundStatusMsg }</span>
						</c:if>

						<span>下单时间:<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${orderTask.orderDto.createTime }" /></span>
					    <span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span>
					    <span>任务创建时间:<fmt:formatDate value="${orderTask.createDate }" pattern="MM-dd HH:mm"/></span>
					    <span hidden><font color="red">任务超时</font></span>
					    <input class="proDate" type="hidden" value='<fmt:formatDate value="${orderTask.maxProcessDate }" pattern="yyyy-MM-dd HH:mm:ss"/>'>
					    <input type="hidden" value="${orderTask.taskStatus }"/>
					    <c:if test="${orderTask.taskStatus == 'PROCESSING' }">
						    <label>任务延时</label>
						    <select style="width: 55px" onchange="delay('${orderTask.id }',$(this).val(),'${orderTask.operatorId }')">
						    	<option value="-1">--
						    	<option value="5">5分钟
						    	<option value="15">15分钟
						    	<option value="60">1小时
						    	<option value="1440">1天
						    </select>
					    </c:if>
				    </td>
				</tr>
				<tr>
					<td>
						<c:forEach items="${orderTask.orderDto.details}" var="detail">

							<span style="display: inline-block;margin-bottom: 5px">
                                    ${detail.detailMessages} <br>
							</span>

						</c:forEach>
						<br>
						<span style="display: inline-block;margin-top: 15px">
								出行人: ${orderTask.userNames}
						</span>
					</td>

					<td>
						<span>应收￥${orderTask.orderDto.totalSalePrice }</span>
						<span>
							<c:choose>
								<c:when test="${orderTask.orderDto.paymentStatus eq 'PAYMENT_SUCCESS' }">已收</c:when>
								<c:otherwise>未收</c:otherwise>
							</c:choose>
						</span><br/>
						<c:if test="${not empty orderTask.orderDto.totalSettlePrice}">
							<span>应付￥${orderTask.orderDto.totalSettlePrice }</span>
							<span>
								<c:choose>
									<c:when test="${not empty orderTask.orderDto.externalOrderStatus and (orderTask.orderDto.externalOrderStatus eq 'WAIT_ISSUE' or orderTask.orderDto.externalOrderStatus eq 'ISSUED' or orderTask.orderDto.externalOrderStatus eq 'PAYING')}">
										已付
									</c:when>
									<c:otherwise>未付</c:otherwise>
								</c:choose>
							</span><br/>
						</c:if>

						<c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
								<span><a onclick="loadIncomeDetail('${orderTask.orderDto.id}','${orderTask.orderDto.paymentPlanNo}')">明细</a></span>
						</c:if>
					</td>
					<td>
						<span>订单状态：${orderTask.orderDto.orderShowStatus.message } (已下单)</span><br/>
						<span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>

						<span>处理人：${orderTask.operatorName }</span><br/>

						<c:if test="${orderTask.taskStatus eq 'PROCESSING' }">
							<span>
								<a onclick="transfer('${orderTask.id}','${orderTask.operatorId }',true)">交接</a>

								<c:if test="${orderTask.taskType eq 'WAIT_OFFER'}">
									| <a onclick="fillOrder('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.operatorId}')">填写报价</a>
									| <a onclick="cancelOrder('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.operatorId}')">取消订单</a>
								</c:if>

								<!-- 待催单 -->
								<c:if test="${orderTask.taskType eq 'WAIT_TICKET' }">
									 |<a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id }','1','${orderTask.operatorId}')">线下出票</a>
									 |<a onclick="rejectOrder('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.orderDto.userId }','${orderTask.operatorId}')">拒单退款</a>
								</c:if>
								
								 <!-- 退款审核 -->
								 <c:if test="${orderTask.taskType eq 'WAIT_REFUND' }">
									 | <a onclick="ticketAduit('${orderTask.refundOrderId}','${orderTask.id }','${orderTask.operatorId }')">审核</a>
								 </c:if>

							 </span>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' }">
							<span>处理结果：${orderTask.result}</span>
						</c:if>
					 </td>
					<td>

						<!-- 有供应商订单信息 -->
						<c:if test="${not empty orderTask.orderDto.externalOrderId }">
							<a data-trigger="tooltip" data-content="${orderTask.orderDto.externalOrderId }">订单号</a><br/>
						</c:if>
						<c:if test="${not empty orderTask.orderDto.pnrCode }">
							PNR:${orderTask.orderDto.pnrCode }
						</c:if>

					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<div class="pagin" style="margin-bottom: 22px">
<jsp:include page="../../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="webpage/flight/tmc/international/js/myTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(function(){

	var status = "${taskStatus}";
	$("#myTaskStatusTab li").each(function(){
		if($(this).attr("data-value")==status){
			$(this).addClass("active");
			$(this).siblings().removeClass("active");
			if(status=='PROCESSING'){
				 $('#f_my_content .content1').show();
			     $('#f_my_content .content2').hide();
			}else if(status=='ALREADY_PROCESSED'){
				 $('#f_my_content .content1').hide();
			     $('#f_my_content .content2').show();
			}
			return false;
		}
	});

	var bizProcessingMapStr = '${bizProcessingMap}';
	var bizWaitProcessMapStr = '${bizWaitProcessMap}';
	if(bizProcessingMapStr != '' ){
        var bizProcessingMap = eval('(' + bizProcessingMapStr.split('=').join(':') + ')');
        $("#bizMenu").find("span").each(function () {
            var bizName = $(this).attr("name");
            var count = bizProcessingMap[bizName];
            if (count != undefined){
                $(this).text("("+count+")");
            }
        })
    }
    if(bizWaitProcessMapStr != ''){
        var bizWaitProcessMap = eval('(' + bizWaitProcessMapStr.split('=').join(':') + ')');
		$(".tab_h_item").each(function () {
		    var inputDom = $(this).find("input:eq(0)");
		    var id = inputDom.attr("id");
		    var count  = bizWaitProcessMap["COUNT"][id];
		    if(count != undefined){
                inputDom.parent().find("span").text("("+count+")");
			}

			var ulDom = inputDom.parent().next();
			ulDom.find("input").each(function () {
			    var child_input = $(this);
				var taskType = child_input.attr("task_type");
				var bizGroup = bizWaitProcessMap[id];
				if(bizGroup != undefined){
                    var child_count = bizGroup[taskType];
                    if(child_count != undefined){
                        child_input.parent().find("span").text("("+child_count+")");
                    }
				}
            })
        })

    }

});


</script>