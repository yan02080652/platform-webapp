//我的任务定时器
var timerFlag ;
//全部任务定时器
var allTask_timer;

//我的任务Tab
function getMyTask(){
    $("#myTask_task_status").val("PROCESSING");
    $("#myTask_task_type").val("");
    $("#myTask_pageIndex").val(1);
    clearInterval(allTask_timer);
    var load_index = layer.load();
	$.ajax({
		type:"get",
		url:"flight/tmc/getMyTaskTable",
		data:{},
		success:function(data){
			layer.close(load_index);
			$("#myTask").html(data);
		},
		error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
		}
	});
}
//全部任务Tab
function getAllTask(){
    $("#allTask_task_status").val("WAIT_PROCESS");
    $("#allTask_pageIndex").val(1);
    $("#allTask_task_type").val("");
    clearInterval(timerFlag);
    var load_index = layer.load();
	$.ajax({
		type:"get",
		url:"flight/tmc/allTask",
		data:{},
		success:function(data){
            layer.close(load_index);
			$("#allTask").html(data);
		},
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }
	});
}
//订单列表Tab
function getOrderList(){
    clearInterval(timerFlag);
    clearInterval(allTask_timer);
    var load_index = layer.load();
	$.ajax({
		type:"get",
		url:"flight/tmc/orderList",
		data:{},
		success:function(data){
            layer.close(load_index);
			$("#orderList").html(data);
		},
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }

	});
}

function getInternationalOrderList(){
    clearInterval(timerFlag);
    clearInterval(allTask_timer);
    var load_index = layer.load();
    $.ajax({
        type:"get",
        url:"internationalflight/tmc/orderList",
        data:{},
        success:function(data){
            layer.close(load_index);
            $("#internationalOrderList").html(data);
        },
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }

    });
}

//代客下单入口
function getSearchUserList(){
    clearInterval(timerFlag);
    clearInterval(allTask_timer);
    var load_index = layer.load();
	$.ajax({
		type:"get",
		url:"flight/tmc/searchUserPage",
		data:{},
		success:function(data){
            layer.close(load_index);
			$("#searchUserPage").html(data);
		},
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }
	});
}

//订单操作日志
function logList(orderId){
	layer.open({
		  type: 2,
		  area: ['900px', '500px'],
		  title: "订单日志",
		  closeBtn: 1,
		  shadeClose: true,
		 // shade :0,
		  content: "flight/tmc/getOrderLog?orderId="+orderId,
		});
}

//加载收入明细
function loadIncomeDetail(orderId,PaymentPlanNo){
	layer.open({
		  type: 2,
		  area: ['600px', '400px'],
		  title: "交易记录",
		  closeBtn: 1,
		  shadeClose: true,
		 // shade :0,
		  content: "flight/tmc/getIncomeDetail?orderId="+orderId+"&PaymentPlanNo="+PaymentPlanNo,
		});
}


//复制ss指令
var clipboard = new Clipboard('.ss');
clipboard.on('success', function(e) {
    layer.msg("复制成功")
});
clipboard.on('error', function(e) {
    layer.msg("复制失败")
});
