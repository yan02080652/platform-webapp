$(function() {
	$("#orderStatus li").click(function() {
		if (!$(this).hasClass("active")) {
            $(this).addClass("active");
            $(this).siblings().removeClass("active");
            submitForm(null,$(this).find("span").attr("data-value"));
		}
	});
})

//重置查询条件
function resetSearchForm(){
	$('#searchForm')[0].reset();
	$("#orderStatus li").each(function(){
		$(this).removeClass("active");
	})
	// 再次请求
	submitForm();
}

//分页函数
function reloadOrderList(pageIndex){
	submitForm(pageIndex);
}

function submitForm(pageIndex){
	var dateType = $("#searchField1").val();
	var startDate = $("#startDate3").val();
	var endDate = $("#endDate3").val();
	var field1 = $.trim($("#searchField2").val());
	var field2 = $.trim($("#searchField3").val());
	var field3 = $.trim($("#searchField4").val());
    var orderStatus = $("#orderStatus .active span").data("value");
    var issueChannel = $("#issueChannel").val();

    var load_Index = layer.load(0);
	$.ajax({
		type:"post",
		url:"flight/tmc/getOrderTable",
		data:{	dateType:dateType,
				startDate1:startDate==''?startDate:new Date(startDate).getTime(),
				endDate1:endDate==''?endDate:new Date(endDate).getTime(),
				passengerOrContent:field1,
				orderNumber:field2,
				partnerName:field3,
				orderStatus:orderStatus,
			    issueChannel:issueChannel,
				pageIndex:pageIndex
			},
		success:function(data){
			layer.close(load_Index);
			$("#orderTable").html(data);
		},
		error:function(){
            layer.close(load_Index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
		}
	});
}

//TODO 客服介入
function receive(){
	layer.alert("客服介入")
}

