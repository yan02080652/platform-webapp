

fillForm($("#order").val(),$("#carrier").val(),$("#aircraft").val());

//获取当前时间，格式YYYY-MM-DD
function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate;
}

function initForm() {
    var rules = {
        "flightDetail.flightNo": {
            required: true,
        },
        "flightDetail.cabinList[0].code": {
            required: true
        },
        "flightDetail.fromAirport": {
            required: true
        },
        "flightDetail.toAirport": {
            required: true
        },
        "flightDetail.aircraftDto.code": {
            required: true
        },
        "flightDetail.fullPriceCabin.yprice": {
            required: true
        },

        "flightDetail.flightPrice.facePrice": {
            required: true
        },
        "flightDetail.flightPrice.salePrice": {
            required: true
        },
        "flightDetail.flightPrice.departureTax": {
            required: true
        },
        "date_a": {
            required: true
        },
        "date_b": {
            required: true
        },
        "date_c": {
            required: true
        },
        "upgradePrice": {
            required: true
        },
        "rescheduledPrice": {
            required: true
        },
        "servicePrice": {
            required: true
        },
        "discountAmount": {
            required: true
        },


    };

    //添加必填标记
    $.each(rules, function (code, rule) {
        if (rule.required || rule.type) {
            addReqMark(code);
        }
    });

    $("#flightForm").validate({
        rules: rules,
        ignore: ".ignore",
        errorPlacement: function (error, element) {
            showErrorTip($(element), $(error).text());
        },
        submitHandler: function (form) {
            var flag = submitForm();
            console.log(flag);
            if(flag) {
                layer.confirm('是否确定选择此航班？', { btn: ['确认','取消'],title:"提示"},
                    function(index){//确认
                        $.ajax({
                            url: "/flight/tmc/updateChangeOrder.json",
                            type: "post",
                            async:false,
                            data: $("#flightForm").serialize(),
                            success: function (data) {
                                layer.close(index);
                                if (data) {
                                    location.href = "flight/tmc/getChangeOrderTask?orderId="+$("input[name='id']").val()+"&taskId="+$("input[name='taskId']").val();
                                } else {
                                    layer.alert("系统繁忙，请稍后再试！")
                                }
                            },
                            error: function (e) {
                                console.log(e);
                                layer.alert("系统繁忙，请稍后再试！")
                            }
                        })
                    },
                    function(){//取消
                    });

            }
        }
    });


}

function addReqMark(code, addRule) {
    var requriedMark = $("<span class='reqMark'>*</span>");
    var element = $("#flightForm").find("[name='" + code + "']");
    var label = element.parent(".itemSub").children('.l');
    if ($(".reqMark", label).length == 0) {
        label.append(requriedMark);
    }

    if (addRule) {
        element.rules('add', addRule);
    }
}

//提交表单
function submitForm() {
    //根据页面的几个时间计算相关信息
    var rs = calculationTime();
    if (rs) {
        //改期时不需要createDate
        /*if ($('input[type="checkbox"]').is(":checked")) {
            $("input[name='createDate']").val("");
        }
        $("#flightSearchForm input[name='cabinCode']").val($("input[name='flightDetail.cabinList[0].code']").val());
        $("#flightSearchForm input[name='fromDate']").val($("input[name='fromDateString']").val());
        $("#flightSearchForm input[name='flightNo']").val($("input[name='flightDetail.flightNo']").val());
        $("#flightSearchForm input[name='fromCityCode']").val($("input[name='flightDetail.fromCity']").val());
        $("#flightSearchForm input[name='toCityCode']").val($("input[name='flightDetail.toCity']").val());*/

        if (!$("input[name='flightDetail.carrier.code']").val()) {
            showCarrierNoFoundTip($("input[name='flightDetail.flightNo']"));
            return false;
        }
        if (!$("#airport1").val()) {
            showAirportNoFoundTip($("input[name='flightDetail.fromAirport']"));
            return false;
        }
        if (!$("#airport2").val()) {
            showAirportNoFoundTip($("input[name='flightDetail.toAirport']"))
            return false;
        }
        if (!$("#airport3").val() && $("input[name='stopAirport']").val()) {
            showAirportNoFoundTip($("input[name='stopAirport']"))
            return false;
        }

        if (!checkAircraft()){
            showErrorTip($("input[name='flightDetail.aircraftDto.code']"), "编码仅支持3位数字、字母组合");
            return false;
        }

        //$("input[name='userJsonString']").val(getUserMsg());

        // 提交表单
        return true;
    }
}

//回填订单信息
function fillForm(a,b,c) {
    var obj = JSON.parse(a);
    var carrier = JSON.parse(b);
    var aircraft = JSON.parse(c);

    $("input[name='originalOrderId']").val(obj.id);
    $("input[name='servicePrice']").val(obj.serverCharge);

    $("#flightDetailDiv span:eq(0)").text(carrier.nameShort + " " + obj.orderFlights[0].flightNo);
    $("#flightDetailDiv span:eq(1)").text(date.format(new Date(obj.orderFlights[0].fromDate), "yyyy.MM.dd HHmm"));
    $("#flightDetailDiv span:eq(2)").text(obj.orderFlights[0].fromCityName + " " + obj.orderFlights[0].fromAirportName + "(" + obj.orderFlights[0].fromAirport + ")");
    $("#flightDetailDiv span:eq(3)").text("--");
    $("#flightDetailDiv span:eq(4)").text(obj.orderFlights[0].toCityName + " " + obj.orderFlights[0].toAirportName + "(" + obj.orderFlights[0].toAirport + ")");
    $("#flightDetailDiv span:eq(5)").text("经济舱 (全价:" + obj.orderFlights[0].cabinFullPrice + ")");
    $("#flightDetailDiv span:eq(6)").text(obj.orderFlights[0].cabinClass + " (票面:" + obj.orderFee.facePrice + ")");

    $("#changeRule").text(obj.endorseRefundRules[0].changeRule);
    $("#endorseRule").text(obj.endorseRefundRules[0].endorseRule);
    $("#remarkOut").text(obj.remarkOut || "无政策备注");
    $("#refundServiceTime").text(obj.refundServiceTime);


    $("input[name='flightDetail.flightNo']").val(obj.orderFlights[0].flightNo);
    $("input[name='flightDetail.flightNo']").attr("data-value",obj.orderFlights[0].flightNo);
    $("input[name='flightDetail.cabinList[0].code']").val(obj.orderFlights[0].cabinCode);
    $("input[name='flightDetail.cabinList[0].code']").attr("data-value",obj.orderFlights[0].cabinCode);


    $("input[name='flightDetail.fromAirport']").val(obj.orderFlights[0].fromAirport);
    $("input[name='flightDetail.toAirport']").val(obj.orderFlights[0].toAirport);
    $("input[name='stopAirport']").val(obj.orderFlights[0].stopCity);
    $("input[name='flightDetail.carrier.nameShort']").val(carrier.nameShort);
    $("input[name='flightDetail.carrier.code']").val(carrier.code);
    $("input[name='flightDetail.carrier.nameEn']").val(carrier.nameEn);
    $("input[name='flightDetail.carrier.nameCn']").val(carrier.nameCn);
    $("input[name='flightDetail.carrier.logo']").val(carrier.logo);
    $("select[name='flightDetail.cabinList[0].grade']").val(obj.orderFlights[0].cabinClass);
    $("input[name='flightDetail.fromCityCN']").val(obj.orderFlights[0].fromCityName);
    $("input[name='flightDetail.fromCity']").val(obj.orderFlights[0].fromCity);
    $("input[name='flightDetail.toCityCN']").val(obj.orderFlights[0].toCityName);
    $("input[name='flightDetail.toCity']").val(obj.orderFlights[0].toCity);
    $("input[name='flightDetail.flightStopOver.cityName']").val(obj.orderFlights[0].stopCityName);
    $("input[name='flightDetail.flightStopOver.stopCity']").val(obj.orderFlights[0].stopCity);
    $("input[name='flightDetail.realFlightNo']").val(obj.orderFlights[0].realFlightNo);
    if (obj.orderFlights[0].cabinClass == 'Y') {
        $("input[name='flightDetail.fullPriceCabin.yprice']").val(obj.orderFlights[0].cabinFullPrice);
    } else if (obj.orderFlights[0].cabinCode == 'C') {
        $("input[name='flightDetail.fullPriceCabin.cprice']").val(obj.orderFlights[0].cabinFullPrice);
    } else {
        $("input[name='flightDetail.fullPriceCabin.fprice']").val(obj.orderFlights[0].cabinFullPrice);
    }
    $("input[name='flightDetail.fullPriceCabin.yprice']").val(obj.orderFlights[0].yprice);
    $("input[name='flightDetail.fromAirportName']").val(obj.orderFlights[0].fromAirportName);
    $("input[name='flightDetail.toAirportName']").val(obj.orderFlights[0].toAirportName);
    $("#fromDate").val(date.format(new Date(obj.orderFlights[0].fromDate), "yyyy-MM-dd"));
    $("input[name='flightDetail.fromTerminal']").val(obj.orderFlights[0].fromTerminal);
    $("input[name='flightDetail.toTerminal']").val(obj.orderFlights[0].toTerminal);
    $("input[name='flightDetail.aircraftDto.code']").val(aircraft.code);
    $("input[name='flightDetail.aircraftDto.type']").val(aircraft.type);
    $("input[name='flightDetail.aircraftDto.name']").val(aircraft.name);
    if($("#orderType").val() != 'TICKET_CHANGES'){
        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketBounceRemark']").val(obj.endorseRefundRules[0].refundRule);
        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketCancelRemark']").val(obj.endorseRefundRules[0].cancelRemark);
        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketChangeRemark']").val(obj.endorseRefundRules[0].changeRule);
        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketSignChangeRemark']").val(obj.endorseRefundRules[0].endorseRule);
        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].outRemark']").val(obj.endorseRefundRules[0].outRemark);
    }
    $("input[name='flightDetail.flightPrice.facePrice']").val(obj.orderFee.facePrice);
    $("input[name='flightDetail.flightPrice.departureTax']").val(obj.orderFee.departureTax);
    $("input[name='flightDetail.flightPrice.fuelTax']").val(obj.orderFee.fuelTax);
    $("input[name='discountAmount']").val(obj.orderFee.discountAmount || 0);
    $("input[name='flightDetail.flightPrice.salePrice']").val(obj.orderFee.salePrice);
    $("input[name='flightDetail.flightPrice.extraFee']").val(obj.orderFee.extraFee);
    $("input[name='servicePrice']").val(obj.orderFee.servicePrice);

    $("#totalPrice").text(obj.orderFee.totalOrderPrice);
    $("input[name='flightDetail.airRangeType']").val(obj.airRangeType);
    $("input[name='flightDetail.flightDuration']").val(obj.orderFlights[0].flightDuration);

    $("#fromTime").val(date.format(new Date(obj.orderFlights[0].fromDate), "HH:mm"));
    var day = date.diffDay(date.format(new Date(obj.orderFlights[0].fromDate), "yyyy-MM-dd"), date.format(new Date(obj.orderFlights[0].toDate), "yyyy-MM-dd"));
    if (day > 0) {
        $("#toTime").val(date.format(new Date(obj.orderFlights[0].toDate), "HH:mm") + "+" + day);
    } else {
        $("#toTime").val(date.format(new Date(obj.orderFlights[0].toDate), "HH:mm"));
    }

    if (obj.orderFlights[0].fromTime) {
        day = date.diffDay(date.format(new Date(obj.orderFlights[0].fromDate), "yyyy-MM-dd"), date.format(new Date(obj.orderFlights[0].fromTime), "yyyy-MM-dd"));
        if (day > 0) {
            $("#stopFromTime").val(date.format(new Date(obj.orderFlights[0].fromTime), "HH:mm") + "+" + day);
        } else {
            $("#stopFromTime").val(date.format(new Date(obj.orderFlights[0].fromTime), "HH:mm"));
        }
    }

    if (obj.orderFlights[0].toTime) {
        day = date.diffDay(date.format(new Date(obj.orderFlights[0].fromDate), "yyyy-MM-dd"), date.format(new Date(obj.orderFlights[0].toTime), "yyyy-MM-dd"));
        if (day > 0) {
            $("#stopToTime").val(date.format(new Date(obj.orderFlights[0].toTime), "HH:mm") + "+" + day);
        } else {
            $("#stopToTime").val(date.format(new Date(obj.orderFlights[0].toTime), "HH:mm"));
        }
    }

    // changeInputStatus();

}



//航班号失去焦点时根据航班号查询出航司相关信息
$("input[name='flightDetail.flightNo']").blur(function () {
    if($(this).val().substring(0,2) != $(this).attr("data-value").substring(0,2)) {
        checkSameCarrierGroup($("#originOrderId").val(),$(this).val());
        getCarrier();
    }
    $(this).attr("data-value",$(this).val());

});
$("input[name='flightDetail.cabinList[0].code']").blur(function () {
    $(this).val($.trim($(this).val().toUpperCase()));
    if($(this).val() != $(this).attr("data-value")) {
        changeInputStatus();
    }
    $(this).attr("data-value",$(this).val());
});

function getFloatNum(num) {
    num = $.trim(num);
    if (num != '' && !isNaN(num)) {
        return parseFloat(num);
    } else {
        return 0;
    }
}

$(".price").keyup(function () {
    calcPrice();
});

function calcPrice(){
    //用来判断是否改签
    var orderType = $("#orderType").val();

    var salePrice = getFloatNum($("input[name='flightDetail.flightPrice.salePrice']").val());
    var departureTax = getFloatNum($("input[name='flightDetail.flightPrice.departureTax']").val());
    var fuelTax = getFloatNum($("input[name='flightDetail.flightPrice.fuelTax']").val());
    var servicePrice = getFloatNum($("input[name='servicePrice']").val());
    var discountAmount = getFloatNum($("input[name='discountAmount']").val());

    var upgradePrice = getFloatNum($("input[name='upgradePrice']").val());
    var rescheduledPrice = getFloatNum($("input[name='rescheduledPrice']").val());
    if (orderType == 'TICKET_CHANGES') {//直接改签
        $("#totalPrice").text(upgradePrice + rescheduledPrice + departureTax + fuelTax + servicePrice - discountAmount);
    } else {//退票重出
        $("#totalPrice").text(salePrice + departureTax + fuelTax + servicePrice - discountAmount);
    }
}

//点击改期CheckBox时隐藏时间选择插件，显示导入原订单的输入框
$('input[type="checkbox"]').change(function () {
    if ($(this).is(":checked")) {
        //$("#orderInputDiv").show();
        $("#createDateDiv").hide();
        $(".ticket_changes").show();
        $(".routine").hide();
        $("#oldOrder").show();
        $("input[name='changeOrder']").val(true);
    } else {
        //$("#orderInputDiv").hide();
        $("#createDateDiv").show();
        $(".routine").show();
        $(".ticket_changes").hide();
        $("#oldOrder").hide();
        $("input[name='changeOrder']").val(false);
    }
});

function checkSameCarrierGroup(originOrderId,flightNo) {
    //TODO  check is same carrier?
    console.log(originOrderId,flightNo);

    $.ajax({
        type: "get",
        url: "flight/tmc/checkSameCarrierGroup.json",
        data: {
            originOrderId: originOrderId,
            flightNo: flightNo
        },
        success: function (result) {
            console.log(result)
            if(result) {
                //相同企业的航司
                $("#orderType").val("TICKET_CHANGES");
                changeInputStatus();

            } else {
                //不同企业的航司
                $("#orderType").val("REFUND_TICKET_REPURCHASE");

                changeInputStatus();

            }
        },
    })


}

function changeInputStatus() {
    var orderType = $("#orderType").val();
    if(orderType == 'TICKET_CHANGES') {
        $(".routine").addClass("hidDiv");
        $("input[name='upgradePrice']").prop("readonly",false);
        $("input[name='rescheduledPrice']").prop("readonly",false);
    } else if(orderType == 'REFUND_TICKET_REPURCHASE'){
        $(".routine").removeClass("hidDiv");
        $("input[name='upgradePrice']").prop("readonly",true);
        $("input[name='rescheduledPrice']").prop("readonly",true);
    }
    getRefundChangeRule();
    calcPrice();
}

function getCarrier() {
    var flightNo = $.trim($("input[name='flightDetail.flightNo']").val().toUpperCase());
    if (flightNo.length > 0) {
        $("input[name='flightDetail.flightNo']").val(flightNo);
        var carrierCode = flightNo.substring(0, 2);
        $.get("flight/tmc/getCarrierByCode.json", {'carrierCode': carrierCode}, function (data) {
            if (!$.isEmptyObject(data)) {
                $("input[name='flightDetail.carrier.nameShort']").val(data.nameShort);
                $("input[name='flightDetail.carrier.code']").val(data.code);
                $("input[name='flightDetail.carrier.nameEn']").val(data.nameEn);
                $("input[name='flightDetail.carrier.nameCn']").val(data.nameCn);
                $("input[name='flightDetail.carrier.logo']").val(data.logo);
            } else {
                $("input[name='flightDetail.carrier.nameShort']").val("");
                $("input[name='flightDetail.carrier.code']").val("");
                $("input[name='flightDetail.carrier.nameEn']").val("");
                $("input[name='flightDetail.carrier.nameCn']").val("");
                $("input[name='flightDetail.carrier.logo']").val("");
                showCarrierNoFoundTip($("input[name='flightDetail.flightNo']"));
            }
        });
    }
}

function checkAircraft(){
    var reg = /^[0-9a-zA-Z]{3}$/;
    var code = $.trim($("input[name='flightDetail.aircraftDto.code']").val());
    return reg.test(code);
}


//机型失去焦点时根据航班号查询出机型相关信息
$("input[name='flightDetail.aircraftDto.code']").blur(function () {
    var code = $.trim($(this).val());

    if (!checkAircraft()){
        showErrorTip($(this), "编码仅支持3位数字、字母组合");
        return false;
    }

    if (code.length > 0) {
        $.get("flight/tmc/getAircraftByCode.json", {'code': code}, function (data) {
            if (!$.isEmptyObject(data)) {
                $("input[name='flightDetail.aircraftDto.type']").val(data.type);
                $("input[name='flightDetail.aircraftDto.name']").val(data.name);
            }
        });
    }
});

function getRefundChangeRule() {
    var flightNo = $("input[name='flightDetail.flightNo']").val();
    var cabinCode = $("input[name='flightDetail.cabinList[0].code']").val();
    var fromAirport = $("input[name='flightDetail.fromAirport']").val();
    var toAirport = $("input[name='flightDetail.toAirport']").val();
    var orderType = $("#orderType").val();
    if (flightNo && cabinCode && fromAirport && toAirport) {
        $.ajax({
            type: "get",
            url: "flight/tmc/refundChangeDetail.json",
            data: {
                flightNo: flightNo,
                cabinCode: cabinCode,
                fromAirport: fromAirport,
                toAirport: toAirport
            },
            success: function (result) {
                var defaultRule = "以航司退改签规定为准";
                if(orderType != 'TICKET_CHANGES') {
                    if ($.isEmptyObject(result) || result.refundRule == '99' || result.changeRule == '99') {
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketBounceRemark']").val(defaultRule);
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketChangeRemark']").val(defaultRule);
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketSignChangeRemark']").val(defaultRule);
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketCancelRemark']").val(defaultRule);
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].outRemark']").val(defaultRule);
                    } else {
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketBounceRemark']").val(result.refundRule);
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketChangeRemark']").val(result.changeRule);
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketSignChangeRemark']").val(result.endorseRule);
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketCancelRemark']").val(result.baggage);
                        $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].outRemark']").val(result.remark);
                    }
                } else {
                    $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketBounceRemark']").val("");
                    $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketChangeRemark']").val("");
                    $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketSignChangeRemark']").val("");
                    $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].ticketCancelRemark']").val("");
                    $("input[name='flightDetail.flightPrice.endorseAndRefundRule[0].outRemark']").val("");
                }
            },
        })
    }
}

function showAirportNoFoundTip(element) {
    showErrorTip(element, "未找到对应机场信息,请联系运维添加")
}

function showCarrierNoFoundTip(element) {
    showErrorTip(element, "未找到对应航司信息,请联系运维添加")
}

function showErrorTip(element, msg) {
    layer.tips(msg, $(element), {
        tips: [1, '#bb0b07'],
        tipsMore: true,
    });
}

function calculationTime() {
    var fromDate = $.trim($("#fromDate").val());
    var fromTime = $.trim($("#fromTime").val());
    var toTime = $.trim($("#toTime").val());
    var stopFromTime = $.trim($("#stopFromTime").val());
    var stopToTime = $.trim($("#stopToTime").val());

    //时间格式效验
    var reg = /^\d{4}\-\d{2}\-\d{2}$/;
    if (!reg.test(fromDate)) {
        layer.alert("起飞日期格式不正确：正确格式（yyyy-MM-dd）例：2017-01-01")
        return false;
    }

    reg = /^\d{2}\:\d{2}$/;
    if (!reg.test(fromTime)) {
        layer.alert("起飞时间格式不正确：正确格式（HH:mm）  <br/>例：12:15")
        return false;
    }

    reg = /^\d{2}\:\d{2}(\+\d)?$/;
    if (!reg.test(toTime)) {
        layer.alert("到达时间格式不正确：正确格式（HH:mm+N）  <br/>例：13:30+1")
        return false;
    }

    if (stopFromTime.length > 0) {
        if (!reg.test(stopFromTime)) {
            layer.alert("经停起始格式不正确：正确格式（HH:mm+N）  <br/>例：13:30+1")
            return false;
        }
    }
    if (stopToTime.length > 0) {
        if (!reg.test(stopToTime)) {
            layer.alert("经停结束格式不正确：正确格式（HH:mm+N）  <br/>例：13:30+1")
            return false;
        }
    }

    if (fromDate.length > 0 && fromTime.length > 0) {
        $("input[name='fromDateString']").val(fromDate + " " + fromTime + ":00");
    }

    if (fromDate.length > 0 && toTime.length > 0) {
        if (toTime.length > 5) {
            var day = toTime.substring(6, 7);
            $("input[name='toDateString']").val(date.format(date.add(fromDate + " " + toTime.substring(0, 5), 'd', day), "yyyy-MM-dd HH:mm:ss"));
            $("input[name='flightDetail.crossDay']").val(true);
        } else {
            $("input[name='toDateString']").val(fromDate + " " + toTime + ":00");
            $("input[name='flightDetail.crossDay']").val(false);
        }
    }

    //时长
    var duration = new Date($("input[name='toDateString']").val()).getTime() - new Date($("input[name='fromDateString']").val()).getTime();
    //计算出相差天数
    var days = Math.floor(duration / (24 * 3600 * 1000));
    // 计算出小时数
    var leave1 = duration % (24 * 3600 * 1000); // 计算天数后剩余的毫秒数
    var hours = Math.floor(leave1 / (3600 * 1000));
    if (hours < 10) {
        hours = "0" + hours;
    }
    // 计算相差分钟数
    var leave2 = leave1 % (3600 * 1000) // 计算小时数后剩余的毫秒数
    var minutes = Math.floor(leave2 / (60 * 1000))
    if (minutes < 10) {
        minutes = "0" + minutes;
    }

    if (days > 0) {//国内机票不存在超过一天的
        $("input[name='flightDetail.flightDuration']").val(days + "d" + hours + ":" + minutes);
    } else {
        $("input[name='flightDetail.flightDuration']").val(hours + ":" + minutes);
    }

    //当实际承运不为空时说明是共享航班
    if ($.trim($("input[name='flightDetail.realFlightNo']").val()).length > 0) {
        $("input[name='flightDetail.shareFlightNo']").val(true);
    } else {
        $("input[name='flightDetail.shareFlightNo']").val(false);
    }

    if (fromDate.length > 0 && stopFromTime.length > 0) {
        if (stopFromTime.length > 5) {
            var day = stopFromTime.substring(6, 7);
            $("input[name='stopOverFromTime']").val(date.format(date.add(fromDate + " " + stopFromTime.substring(0, 5), 'd', day), "yyyy-MM-dd HH:mm:ss"));
        } else {
            $("input[name='stopOverFromTime']").val(fromDate + " " + stopFromTime + ":00");
        }
    }

    if (fromDate.length > 0 && stopToTime.length > 0) {
        if (stopToTime.length > 5) {
            var day = stopToTime.substring(6, 7);
            $("input[name='stopOverToTime']").val(date.format(date.add(fromDate + " " + stopToTime.substring(0, 5), 'd', day), "yyyy-MM-dd HH:mm:ss"));
        } else {
            $("input[name='stopOverToTime']").val(fromDate + " " + stopFromTime + ":00");
        }
    }

    return true;
}

$("input[name='flightDetail.fromAirport']").blur(function () {
    checkSameCarrierGroup($("#originOrderId").val(),$("input[name='flightDetail.flightNo']").val());
    searchAiportAndCity($(this), '#city1', '#airport1');

});
$("input[name='flightDetail.toAirport']").blur(function () {
    checkSameCarrierGroup($("#originOrderId").val(),$("input[name='flightDetail.flightNo']").val());
    searchAiportAndCity($(this), '#city2', '#airport2');
});
$("input[name='stopAirport']").blur(function () {
    checkSameCarrierGroup($("#originOrderId").val(),$("input[name='flightDetail.flightNo']").val());
    searchAiportAndCity($(this), '#city3', '#airport3');
});

//根据三字码查询机场，城市等信息
function searchAiportAndCity(element, city, airport) {
    var code = $.trim($(element).val().toUpperCase());
    if (code.length > 0) {
        $(element).val(code);
        $.get("flight/tmc/getAirportAndCity.json", {'code': code}, function (data) {
            if (!$.isEmptyObject(data)) {
                if (data.airport != undefined) {
                    $(airport).val(data.airport.nameCn);
                    $(city).val(data.airport.cityName);
                    $(city).next().val(data.airport.code);

                }
            } else {
                $(airport).val("");
                $(city).val("");
                $(city).next().val("");
                showAirportNoFoundTip(element)
            }
        });
    }else{
        $(airport).val("");
        $(city).val("");
        $(city).next().val("");
    }
}

function checkData() {
    $("#flightForm").submit();
}

$(function () {
    initForm();
});