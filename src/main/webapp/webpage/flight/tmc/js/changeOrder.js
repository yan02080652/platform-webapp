

$(function () {

    $("input[name='voluntary']").change(function () {
        if($(this).is(":checked")) {
            $(".voluntaryNotice").show();
        } else {
            $(".voluntaryNotice").hide();
        }
    });

    var taskStatus = $("#taskStatus").val();

    if(taskStatus == 'PROCESSING') {
        $(".waitpay").css("display","none");
        var diff = parseInt($("#difference").text());
        if(diff > 0) {
            $(".needrefund").css("display","none");
        }else {
            $(".needpay").css("display","none");
        }

    } else if(taskStatus == 'HANG_UP'){
        $(".needrefund").css("display","none");
        $(".needpay").css("display","none");

    }

    if($("input[name='orderType']").eq(0).is(":checked")){
        // "fee-chose -- changeDirect" "out-ticket retund -- changeRefund"
        $(".changeDirect").css("display","");
        $(".changeRefund").css("display","none");


    } else if($("input[name='orderType']").eq(1).is(":checked")){
        $(".changeDirect").css("display","none");
        $(".changeRefund").css("display","");

    }

    $("input[name='orderType']").eq(0).change(function () {
        if($(this).is(":checked")) {
            $(".changeDirect").css("display","");
            $(".changeRefund").css("display","none");

        }
        var diff = parseFloat(toDecimal2($("#changeTotalPrice").text())) - parseFloat(toDecimal2($("#alreadyAmout").text()));

        $("#difference").text(toDecimal2(diff));
        showBtn();
    });

    $("input[name='orderType']").eq(1).change(function () {
        if($(this).is(":checked")) {
            $(".changeDirect").css("display","none");
            $(".changeRefund").css("display","");

        }
        var diff = parseFloat(toDecimal2($("#refundChangeTotalPrice").text())) - parseFloat(toDecimal2($("#alreadyAmout").text()));

        $("#difference").text(toDecimal2(diff));

        showBtn();
    });


    $(".singlePrice").blur(function () {

        $(this).parent("div").next("div").text(toDecimal2($(this).val() * $("#passengerNum").val()));

        $("#changeTotalPrice").text(totalChangePrice("singlePrice"));
        var diff = parseFloat(toDecimal2($("#changeTotalPrice").text())) - parseFloat(toDecimal2($("#alreadyAmout").text()));

        $("#difference").text(toDecimal2(diff));


    });

    $(".price").blur(function () {
        $(this).val(toDecimal2($(this).val()));
    });


    $(".singlePrice").blur(function () {

        var taskStatus = $("#taskStatus").val();

        if(taskStatus == 'PROCESSING') {
            $(".waitpay").css("display","none");
            var diff = parseInt($("#difference").text());
            if(diff > 0) {
                $(".needrefund").css("display","none");
                $(".needpay").css("display","");
            }else {
                $(".needrefund").css("display","");
                $(".needpay").css("display","none");
            }

        } else if(taskStatus == 'HANG_UP'){
            $(".needrefund").css("display","none");
            $(".needpay").css("display","none");

        }
    });


    //---------------------------退票重出---------------------------
    $(".singlePrice1").blur(function () {
        $(this).parent("div").next("div").find("p").text(toDecimal2($(this).val() * $("#passengerNum").val()));

        $("#newTicketPrice").text(totalChangePrice("singlePrice1"));
        //新票总价
        $("#newTicketTotalPrice").val($("#newTicketPrice").text());
        // //出票服务费
        // $("#issueTicketTotalService").val( toDecimal2(parseFloat(toDecimal2($("input[name='servicePrice']").val()) * $("#passengerNum").val())) );

        needReefundSingleAmout();

        var diff = parseFloat(toDecimal2($("#refundChangeTotalPrice").text())) - parseFloat(toDecimal2($("#alreadyAmout").text()));

        $("#difference").text(toDecimal2(diff));
    });

    //
    $("input[name='refundFee']").blur(function () {
        $("#refundTotalFee").text(toDecimal2(parseFloat(toDecimal2($(this).val()) * $("#passengerNum").val())));
        needReefundSingleAmout();
        var diff = parseFloat(toDecimal2($("#refundChangeTotalPrice").text())) - parseFloat(toDecimal2($("#alreadyAmout").text()));

        $("#difference").text(toDecimal2(diff));
    });
    $("input[name='adjustFee']").blur(function () {
        $("#adjustTotalFee").text(toDecimal2(parseFloat(toDecimal2($(this).val()) * $("#passengerNum").val())));
        needReefundSingleAmout();
        var diff = parseFloat(toDecimal2($("#refundChangeTotalPrice").text())) - parseFloat(toDecimal2($("#alreadyAmout").text()));

        $("#difference").text(toDecimal2(diff));
    });
    $("input[name='refundServiceFee']").blur(function () {
        $("#refundTotalService1").text(toDecimal2(parseFloat(toDecimal2($(this).val()) * $("#passengerNum").val())));
        // $("#refundTotalService").val(toDecimal2(parseFloat(toDecimal2($(this).val()) * $("#passengerNum").val())));
        needReefundSingleAmout();
        var diff = parseFloat(toDecimal2($("#refundChangeTotalPrice").text())) - parseFloat(toDecimal2($("#alreadyAmout").text()));

        $("#difference").text(toDecimal2(diff));
    });

    $("input[name='refundFee']").blur(function () {
        $(this).text(toDecimal2($(this).val()));
    });
    $("input[name='adjustFee']").blur(function () {
        $(this).text(toDecimal2($(this).val()));
    });
    $("input[name='refundServiceFee']").blur(function () {
        $(this).text(toDecimal2($(this).val()));
    });


    $(".update").blur(function () {
        showBtn();
    });


    $(".btn_f").click(function () {
        refuseRefund();
    });


    $(".completeChange").click(function () {
        var flag = validateForm();
        if(!flag) {
            return;
        }
        var form = getForm();
        layer.confirm('确定完成改签？', { btn: ['确认','取消'],title:"提示"},
            function(index){//确认
                var loadIndex = layer.load();
                console.log(index);
                $.ajax({
                    url: "/flight/tmc/completeChange.json",
                    type: "post",
                    async:true,
                    contentType: "application/json",
                    data: JSON.stringify(form),
                    success: function (data) {
                        layer.close(loadIndex);
                        if (data) {
                            layer.alert('改签完成！！', {
                                closeBtn: 0
                            }, function(){
                                window.opener=null;
                                window.open('','_self');
                                window.close();
                            });
                        } else {
                            layer.alert("系统繁忙，请稍后再试！")
                        }
                    },
                    error: function (e) {
                        layer.closeAll();
                        console.log(e);
                        layer.alert("系统繁忙，请稍后再试！")
                    }
                })
            },
            function(){//取消
            });

    });

    $(".confirmPay").click(function () {
        var flag = validateForm();
        if(!flag) {
            return;
        }
        var form = getForm();
        layer.confirm('确认已沟通支付的差价？', { btn: ['确认','取消'],title:"提示"},
            function(index){//确认
                var load_index = layer.load();
                $.ajax({
                    url: "/flight/tmc/confirmPay.json",
                    type: "post",
                    async:true,
                    contentType: "application/json",
                    data: JSON.stringify(form),
                    success: function (data) {
                        layer.close(load_index);
                        if (data) {
                            layer.msg("确认支付完成，任务挂起，等待用户支付！");
                            window.location.reload();
                        } else {
                            layer.alert("系统繁忙，请稍后再试！")
                        }
                    },
                    error: function (e) {
                        layer.closeAll();
                        console.log(e);
                        layer.alert("系统繁忙，请稍后再试！")
                    }
                })
            },
            function(){//取消
            });

    });

    $(".goPay").click(function () {
        changePay($("#userId").val(),$("#orderId").val());
    });


    $("input[name='checkTicketNo']").change(function () {
        if($(this).is(":checked")) {
            $("input[name='ticketNo']").each(function () {
                $(this).val("");
                $(this).prop("readonly",true);
            });
            $("input[name='ticketNo']").each(function () {
                $(this).parent("td").removeClass("tip-td");
            });

        } else {
            $("input[name='ticketNo']").each(function () {
                $(this).prop("readonly",false);
            });
        }

    });

})

function changePay(userId,orderId){

    var myDate=new Date()
    var date2 = myDate.getTime();
    $("#changePayForm").attr("target", date2);
    var win = window.open(null,date2);
    win.focus();

    $.ajax({
        type:"get",
        url:"flight/tmc/changePay",
        data:{userId:userId,orderId:orderId},
        success:function(data){

            $("#changePayForm").attr("action", data.loginUrl);
            $("input[name='alp']").val(data.alp);
            $("input[name='service']").val(data.service);

            logout(data.logoutUrl);

            $("#changePayForm").submit();

        }
    });
}

function logout(logoutUrl){

    for(var i = 0 ; i < logoutUrl.length ; i++){
        new Image().src = logoutUrl[i];
        console.log(logoutUrl[i]);
    }

}


function getForm() {
    var orderType = $("input[name='orderType']:checked").val();

    var passengers = [];
    if($(".needrefund").is(":visible")) {

        $("input[name='ticketNo']").each(function () {

            var p = {
                id : $(this).attr("passengerId"),
                ticketNo : $(this).val(),
            }
            passengers.push(p);
        });

    }

    if(orderType == "TICKET_CHANGES") {
        var form = {
            orderId : $("#orderId").val(),
            taskId : $("#taskId").val(),
            voluntary : $("input[name='voluntary']").is(":checked") ? false : true,
            orderType : orderType,
            issueChannel : $("select[name='issueChannel1']").val(),
            settleChannelId : $("select[name='settleChannelId1']").val(),
            settlePirce : $("input[name='settlePirce1']").val(),
            rescheduledPrice : $("input[name='rescheduledPrice']").val(),
            upgradePrice : $("input[name='upgradePrice']").val(),
            departureTax : $("input[name='fuelAndDepartureTax']").val(),
            //TODO
            fuelTax : 0,
            issueService : $("input[name='changeService']").val(),
            pnrCode : $("input[name='pnrCode']").val(),
            externalOrderId : $("input[name='externalOrderId']").val(),
            passengerDtos : passengers,
            originRefundOrderId : $("input[name='originRefundOrderId']").val(),
        }

        return form;
    } else {

        var form = {
            orderId : $("#orderId").val(),
            taskId : $("#taskId").val(),
            voluntary : $("input[name='voluntary']").is(":checked") ? false : true,
            orderType : orderType,
            issueChannel : $("select[name='issueChannel2']").val(),
            settleChannelId : $("select[name='settleChannelId2']").val(),
            settlePirce : $("input[name='settlePrice2']").val(),
            salePrice : $("input[name='salePrice']").val(),
            departureTax :  $("input[name='departureTax']").val(),
            fuelTax : $("input[name='fuelTax']").val(),
            issueService : $("input[name='servicePrice']").val(),
            refundFee : $("input[name='refundFee']").val(),
            adjustFee : $("input[name='adjustFee']").val(),
            refundService : $("input[name='refundServiceFee']").val(),
            refundSettleChannelId : $("select[name='refundSettleChannelId']").val(),
            originRefundOrderId : $("input[name='originRefundOrderId']").val(),
            pnrCode : $("input[name='pnrCode']").val(),
            externalOrderId : $("input[name='externalOrderId']").val(),
            passengerDtos : passengers,
        }

        return form;
    }

}

function validateForm() {

    var orderType = $("input[name='orderType']:checked").val();

    if(orderType == "TICKET_CHANGES") {


    } else {
        var settle = $("input[name='settlePrice2']").val();
        if(isNaN(settle) || settle == '') {
            layer.msg("请填写供应商结算价！");
            return false;
        }
    }

    var pnr = $("input[name='pnrCode']").val();

    if($(".needrefund").is(":visible")) {

        if(!$("input[name='checkTicketNo']").is(":checked")){
            var flag = false;
            $("input[name='ticketNo']").each(function () {
               if($(this).val() == ''  || !checkReg($(this).val())){
                   flag = true;
                   layer.msg("请提供正确的票号！");
               }
            });
            if(flag) {
                return false;
            }

        } else {
            var pnrCode = $("input[name='pnrCode']").val().trim();
            var externalOrderId = $("input[name='externalOrderId']").val().trim();
            if( pnrCode == '' && externalOrderId == '') {
                layer.msg("请填写PNR或供应商单号！");
                return false;
            }
        }

    }


    return true;
}



function autoAdd(obj) {
    var firstNo = obj.val();
    if(firstNo == '' || $("input[name='checkTicketNo']").is(":checked")){
        return;
    }
    //验证格式
    if (checkReg(firstNo)) {
        obj.parent("td").removeClass("tip-td");
        // if (firstNo.indexOf("-") > 0) {
        //     //包含有横杆
        //     var split = firstNo.split("-");
        //     $("#passengerTable tbody tr:gt(0)").each(function (i, dom) {
        //         // $(this).find("td:eq(1) input").val(split[0] + "-" + (parseInt(split[1]) + i + 1));
        //         $(this).find("td:eq(1) input").parent("td").removeClass("tip-td");
        //     })
        //
        // } else {
        //     //后面每个加1
        //     $("#passengerTable tbody tr:gt(0)").each(function (i, dom) {
        //         // $(this).find("td:eq(1) input").val(parseInt(firstNo) + i + 1);
        //         $(this).find("td:eq(1) input").parent("td").removeClass("tip-td");
        //     })
        // }

    } else {
        obj.parent("td").addClass("tip-td");
    }


}

function checkReg(value) {
    var reg1 = /^[0-9]{13}$/;
    var reg2 = /^[0-9]{3}-[0-9]{10}$/;
    return reg1.test(value) | reg2.test(value);
}

//应退
function needReefundSingleAmout() {
    var single = $("#singleAmout").text();
    var refundFee = $("input[name='refundFee']").val();
    var adjustFee = $("input[name='adjustFee']").val();
    var refundServiceFee = $("input[name='refundServiceFee']").val();

    var amout = parseFloat(toDecimal2(single)) - parseFloat(toDecimal2(refundFee)) - parseFloat(toDecimal2(adjustFee)) - parseFloat(toDecimal2(refundServiceFee));

    $("input[name='needReefundAmout']").val(toDecimal2(amout));

    $("#needRefundTotalAmout").text(toDecimal2(amout * $("#passengerNum").val()));

    $("#needRefundAmout").val(toDecimal2(amout * $("#passengerNum").val()));

    var newTicketTotalPrice = $("#newTicketTotalPrice").val();
    // var issueTicketTotalService = $("#issueTicketTotalService").val();
    // var refundTotalService = $("#refundTotalService").val();
    var needRefundAmout = $("#needRefundAmout").val();
    var total = parseFloat(toDecimal2(newTicketTotalPrice))  - parseFloat(toDecimal2(needRefundAmout));

    $("#refundChangeTotalPrice").text(toDecimal2(total));
}

//根据差额显示按钮情况
function showBtn() {
    var taskStatus = $("#taskStatus").val();

    if(taskStatus == 'PROCESSING') {
        $(".waitpay").css("display","none");
        var diff = parseInt($("#difference").text());
        if(diff > 0) {
            $(".needrefund").css("display","none");
            $(".needpay").css("display","");
        }else {
            $(".needrefund").css("display","");
            $(".needpay").css("display","none");
        }

    } else if(taskStatus == 'HANG_UP'){
        $(".needrefund").css("display","none");
        $(".needpay").css("display","none");

    }
}

//重新计算改签总额
function totalChangePrice(css){
    var singleTotal = 0;
    $("."+css).each(function () {
        singleTotal = singleTotal + parseFloat(toDecimal2($(this).val()));
    });
    return toDecimal2(singleTotal * $("#passengerNum").val());
}


//取两位小数
function toDecimal2(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return 0.00;
    }
    var f = Math.round(x*100)/100;
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
        rs = s.length;
        s += '.';
    }
    while (s.length <= rs + 2) {
        s += '0';
    }
    return s;
}

//拒绝退票
function refuseRefund (){
    var orderId = $("#orderId").val();
    var taskId = $("#taskId").val();
    var originRefundOrderId = $("input[name='originRefundOrderId']").val();
    layer.prompt({
        formType: 2,//输入框1为input，2为文本域
        title: '请填写拒绝改签原因',
    }, function(value, index, elem){
        if($.trim(value).length>0){
            var c_index = layer.load();
            $.ajax({
                type:"post",
                url:"flight/tmc/refuseChange",
                async:true,
                data:{
                    orderId:orderId,
                    remark:$.trim(value),
                    taskId:taskId,
                    originRefundOrderId:originRefundOrderId,
                },
                success:function(data){
                    layer.close(c_index);
                    if(data){
                        layer.alert('拒绝改签完成！', {
                            closeBtn: 0
                        }, function(){
                            window.opener=null;
                            window.open('','_self');
                            window.close();
                        });

                    }else{
                        parent.layer.msg("系统错误",{icon:2,offset:'t',time:1000});
                    }

                }
            });
        }
    });
}