<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<style>
    span.mr{margin-right: 20px;}
    .hui{color: #C0C0C0}
    .reqMark{
        color: red;
        font-weight: bold;
        font-size: 15px;
    }
    input[readonly] {
        border:1px solid #DDD;
        background-color:#F5F5F5;
        color:#000000;
    }
    .main {
        box-sizing: border-box;
    }
    .hidDiv {
        display: none;
    }

</style>
<link rel="stylesheet" href="/webpage/flight/tmc/css/modifyChangeOrder.css?version=${globalVersion}">
<link rel="stylesheet" href="/resource/plugins/My97DatePicker/skin/default/datepicker.css?version=${globalVersion}">
<link rel="stylesheet" href="/resource/css/supplier/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="/resource/css/supplier/template.css?version=${globalVersion}">
<div class="main">
    <form  method="post" id="flightForm">
            <input  name="id" value="${orderDto.id}" hidden>
            <input  name="taskId" value="${taskId}" hidden>

            <input type="hidden" name="flightDetail.crossDay"/>
            <input type="hidden" name="flightDetail.flightDuration"/>
            <input type="hidden" name="fromDateString"/>
            <input type="hidden" name="toDateString"/>
            <input type="hidden" name="flightDetail.shareFlightNo"/>
            <input type="hidden" name="stopOverFromTime"/>
            <input type="hidden" name="stopOverToTime" />
            <div class="item fix">
                <div class="itemSub fix"><span class="l">航班号</span><input type="text" class="r" name="flightDetail.flightNo" data-value=""/></div>
                <div class="itemSub fix"><span class="l">舱位编码</span><input type="text" class="r" name="flightDetail.cabinList[0].code" data-value=""/></div>
                <div class="itemSub fix"><span class="l">出发三字码</span><input type="text" class="r" name="flightDetail.fromAirport"  /></div>
                <div class="itemSub fix"><span class="l">达到三字码</span><input type="text" class="r" name="flightDetail.toAirport"  /></div>
                <div class="itemSub fix"><span class="l">经停三字码</span><input type="text" class="r" name="stopAirport" /></div>
            </div>
            <div class="item fix">
                <div class="itemSub fix">
                    <span class="l">航空公司</span>
                    <input type="text" class="r ignore" name="flightDetail.carrier.nameShort" readonly/>
                    <input type="hidden" name="flightDetail.carrier.code"/>
                    <input type="hidden" name="flightDetail.carrier.nameEn"/>
                    <input type="hidden" name="flightDetail.carrier.nameCn"/>
                    <input type="hidden" name="flightDetail.carrier.logo"/>
                </div>
                <div class="itemSub fix"><span class="l">物理等级<span class='reqMark'>*</span></span>
                    <select name="flightDetail.cabinList[0].grade" >
                        <c:forEach items="${cabinGrade }" var="grade">
                            <option value="${grade.itemCode }">${grade.itemTxt}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="itemSub fix">
                    <span class="l">出发城市</span>
                    <input type="text" class="r ignore" name="flightDetail.fromCityCN" id="city1" readonly/>
                    <input type="hidden" name="flightDetail.fromCity" />
                </div>
                <div class="itemSub fix">
                    <span class="l">到达城市</span>
                    <input type="text" class="r ignore" name="flightDetail.toCityCN" id="city2" readonly/>
                    <input type="hidden" name="flightDetail.toCity" />
                </div>
                <div class="itemSub fix">
                    <span class="l">经停城市</span>
                    <input type="text" class="r ignore" name="flightDetail.flightStopOver.cityName" id="city3" readonly/>
                    <input type="hidden" name="flightDetail.flightStopOver.stopCity" />
                </div>
            </div>
            <div class="item fix">
                <div class="itemSub fix"><span class="l">实际承运</span><input type="text" class="r" name="flightDetail.realFlightNo"/></div>
                <div class="itemSub fix"><span class="l">头等舱全价</span><input type="text" class="r" name="flightDetail.fullPriceCabin.fprice"/></div>
                <div class="itemSub fix"><span class="l">出发机场</span><input type="text" class="r" name="flightDetail.fromAirportName" id="airport1" readonly/></div>
                <div class="itemSub fix"><span class="l">到达机场</span><input type="text" class="r" name="flightDetail.toAirportName" id="airport2" readonly/></div>
                <div class="itemSub fix"><span class="l">经停机场</span><input type="text" class="r" name="stopAirportName" id="airport3" readonly/></div>
            </div>
            <div class="item fix">
                <div class="itemSub fix"><span class="l">起飞日期</span><input type="text" class="r" placeholder="yyyy-MM-dd" id="fromDate" name="date_a"/></div>
                <div class="itemSub fix"><span class="l">公务舱全价</span><input type="text" class="r" name="flightDetail.fullPriceCabin.cprice"/></div>
                <div class="itemSub fix"><span class="l">出发航站楼</span><input type="text" class="r" name="flightDetail.fromTerminal"/></div>
                <div class="itemSub fix"><span class="l">到达航站楼</span><input type="text" class="r" name="flightDetail.toTerminal"/></div>
                <div class="itemSub fix"><span class="l">经停起始</span><input type="text" class="r ignore" placeholder="HH:mm+N" id="stopFromTime"/></div>
            </div>
            <div class="item fix">
                <div class="itemSub fix">
                    <span class="l">机型编码</span>
                    <input type="text" class="r" name="flightDetail.aircraftDto.code"/>
                    <input type="hidden"  name="flightDetail.aircraftDto.type"/>
                    <input type="hidden"  name="flightDetail.aircraftDto.name"/>
                </div>
                <div class="itemSub fix"><span class="l">经济舱全价</span><input type="text" class="r" name="flightDetail.fullPriceCabin.yprice"/></div>
                <div class="itemSub fix"><span class="l">起飞时间</span><input type="text" class="r" placeholder="HH:mm" id="fromTime" name="date_b"/></div>
                <div class="itemSub fix"><span class="l">到达时间</span><input type="text" class="r" placeholder="HH:mm+N" id="toTime" name="date_c"/></div>
                <div class="itemSub fix"><span class="l">经停结束</span><input type="text" class="r ignore" placeholder="HH:mm+N" id="stopToTime"/></div>
            </div>
            <div class="item fix">
                <span class="l rule_text">退票规则</span><input type="text" class="longInput" name="flightDetail.flightPrice.endorseAndRefundRule[0].ticketBounceRemark"/>
            </div>
            <div class="item fix">
                <span class="l rule_text">改期规则</span><input type="text" class="longInput" name="flightDetail.flightPrice.endorseAndRefundRule[0].ticketChangeRemark"/>
            </div>
            <div class="item fix">
                <span class="l rule_text">签转规则</span><input type="text" class="longInput" name="flightDetail.flightPrice.endorseAndRefundRule[0].ticketSignChangeRemark"/>
            </div>
            <div class="item fix">
                <span class="l rule_text">行李规则</span><input type="text" class="longInput" name="flightDetail.flightPrice.endorseAndRefundRule[0].ticketCancelRemark"/>
            </div>
            <div class="item fix">
                <span class="l rule_text">备注</span><input type="text" class="longInput" name="flightDetail.flightPrice.endorseAndRefundRule[0].outRemark"/>
            </div>

        <div class="manual-pnr" id="userTable">
        </div>
        <div class=" fix">

            <%--<div class="footerItem routine ">--%>
                <%--<span class="l">票面价</span><input type="number" class="price" value="0" name="flightDetail.flightPrice.facePrice"/>--%>
            <%--</div>--%>
            <%--<div class="footerItem routine">--%>
                <%--<span class="l">销售价</span><input type="number" class="price"  value="0" name="flightDetail.flightPrice.salePrice"/>--%>
            <%--</div>--%>


            <%--<div class="footerItem ticket_changes">--%>
                <%--<span class="l">升舱费</span><input type="number" class="price"  value="0" name="upgradePrice" <c:if test="${orderDto.orderType eq 'REFUND_TICKET_REPURCHASE'}">readonly</c:if> />--%>
            <%--</div>--%>
            <%--<div class="footerItem ticket_changes">--%>
                <%--<span class="l">改签费</span><input type="number" class="price"  value="0" name="rescheduledPrice" <c:if test="${orderDto.orderType eq 'REFUND_TICKET_REPURCHASE'}">readonly</c:if> />--%>

            <%--</div>--%>
            <%--<div class="footerItem">--%>
                <%--<span class="l">机建</span><input type="number" class="price"  value="0" name="flightDetail.flightPrice.departureTax"/>--%>
            <%--</div>--%>
            <%--<div class="footerItem">--%>
                <%--<span class="l">燃油</span><input type="number"class="price"  value="0" name="flightDetail.flightPrice.fuelTax"/>--%>
            <%--</div>--%>
            <%--<div class="footerItem">--%>
                <%--<span class="l">服务费</span><input type="number" class="price"  value="0" name="servicePrice"/>--%>
            <%--</div>--%>
            <%--<div class="footerItem">--%>
                <%--<span class="l">直减</span><input type="number" class="price" value="0" name="discountAmount"/>--%>
            <%--</div>--%>

            <div class="moneyBox fix">
                <%--<div class="footerItem moneyT">--%>
                    <%--<div class="l">单张总计:¥</div><div class="money" id="totalPrice">0</div>--%>
                <%--</div>--%>
                <div class="footerItem">
                    <div class="btn_success nextBtn" onclick="checkData()">选择此航班</div>
                </div>
            </div>
        </div>
    </form>
</div>

<input id="order" value='${order}' hidden>
<input id="carrier" value='${carrierDto}' hidden>
<input id="aircraft" value='${aircraftDto}' hidden>
<input id="originOrderId" value="${originOrder.id}" hidden>
<input id="orderType" value="${orderDto.orderType}" hidden>

<script src="resource/js/datepicker.js?version=${globalVersion}"></script>
<script src="resource/js/dateUtil.js?version=${globalVersion}"></script>
<script src="webpage/flight/tmc/js/modifyChangeOrder.js?version=${globalVersion}"></script>
