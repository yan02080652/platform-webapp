<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    body {
        background-color: #FFFFFF;
    }

    .layui-form-label {
        box-sizing: content-box;
    }

    .box {
        padding-top: 20px;
    }

    .layui-form-switch {
        width: 63px;
    }

    .carrierBtn {
        margin-top: 3px;
    }

    .codes {
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        width: 500px
    }

</style>
<div class="box">
    <form class="layui-form" action="">
        <input type="hidden" name="id" value="${itemDto.id}"/>
        <input type="hidden" name="routeRuleId" value="${routeRuleId}"/>
        <input type="hidden" name="hubsCode" value="${itemDto.hubsCode}"/>
        <input type="hidden" name="hubsConnectId" value="${itemDto.hubsConnectId}"/>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">优先级</label>
                <div class="layui-input-inline">
                    <input type="text" name="priority" value="${itemDto.priority}" lay-verify="required|positiveNum"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">连接账号</label>
                <div class="layui-input-inline">
                    <select name="connectAcc" lay-verify="select" lay-filter="connectAcc">
                        <option value="">请选择</option>
                        <c:forEach items="${connectMap}" var="map">
                            <optgroup label="${map.key}">
                                <c:forEach items="${map.value}" var="hubsConnect">
                                    <option value="${hubsConnect.hubsCode.concat("-").concat(hubsConnect.id).concat("-").concat(hubsConnect.productType)}"
                                            <c:if test="${itemDto.hubsConnectId eq hubsConnect.id}">selected</c:if> >${hubsConnect.name}</option>
                                </c:forEach>
                            </optgroup>
                        </c:forEach>
                    </select>
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">连接类型</label>
                <div class="layui-input-inline">
                    <select name="productType" lay-verify="select" lay-filter="connectAcc">
                        <option value="">请选择</option>
                        <option value="STANDARD_REFUND" <c:if test="${itemDto.productType eq 'STANDARD_REFUND'}">selected</c:if> >标准退改</option>
                            <option value="DIRECT_SALES" <c:if test="${itemDto.productType eq 'DIRECT_SALES'}">selected</c:if>  >航司直连</option>

                    </select>
                </div>
            </div>


        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">适用航司</label>
                <div class="layui-input-inline">
                    <input type="checkbox" name="allCarrier" title="全部" lay-filter="allCarrierCheckbox" value="1"
                           <c:if test="${itemDto.allCarrier}">checked</c:if>>
                    <span id="chooseCarrier" class="carrierBtn layui-btn layui-btn-primary layui-btn-small"
                          onclick="chooseCarrier()">
                        <i class="layui-icon">&#xe61f;航司</i>
                    </span>
                </div>
            </div>
            <div class="layui-inline">
                <div class="layui-form-mid layui-word-aux codes" id="codesDiv" title=""></div>
                <input type="text" hidden name="carrierCodes" value="${itemDto.carrierCodes}" lay-verify="notAll"/>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">参与查询</label>
                <div class="layui-input-block">


                    <input type="checkbox" name="query" title="开启" value="1"
                           <c:if test="${itemDto.query}">checked</c:if>>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">参与验价</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="checkPrice" title="开启" value="1"
                           <c:if test="${itemDto.checkPrice}">checked</c:if>>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">支付前占座</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="payAfterSeat" title="开启" value="1"
                           <c:if test="${itemDto.payAfterSeat}">checked</c:if>>
                </div>
            </div>
            <div class="layui-inline">
                <label class="layui-form-label">自动出票</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="autoIssue" title="开启" value="1"
                           <c:if test="${itemDto.autoIssue}">checked</c:if>>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <input type="checkbox" name="isDisable" lay-skin="switch" lay-text="启用|禁用" value="0"
                       <c:if test="${!itemDto.isDisable}">checked</c:if>>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="itemFrom">保存</button>
                <button onclick="closeIframe()" class="layui-btn layui-btn-primary">取消</button>
            </div>
        </div>
    </form>
</div>
<script>
    //保存已选清单
    var carrierData = [];
    var form;
    $(function () {
        layui.use('form', function () {
            form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
            //验证规则
            form.verify({
                positiveNum: [/[1-9]\d*$/, '优先级必须正整数'],
                select: function (value) {
                    if (!value | value.length <= 0) {
                        return "请选择";
                    }
                },
                notAll: function (value) {
                    if (!$("input[name='allCarrier']").is(":checked") && !value | value.length <= 0) {
                        return "请选择航司";
                    }
                }
            });

            //监听指定开关
            form.on('checkbox(allCarrierCheckbox)', function (data) {
                var checked = this.checked ? true : false
                if (checked) {
                    $("#chooseCarrier").hide();
                    $("#codesDiv").hide();
                } else {
                    $("#chooseCarrier").show();
                    $("#codesDiv").show();
                }
            });

            //监听select
            form.on('select(connectAcc)', function (data) {

                var hubsCode = data.value.split("-")[0];
                var productType = data.value.split("-")[2];
                $("select[name='productType'] [value='"+productType+"']").prop("selected",true);
                render(hubsCode);

            });

            //监听提交
            form.on('submit(itemFrom)', function (data) {
                //保存配置项
                var formData = dataHandler(data.field);
                $.ajax({
                    url: "flight/routeRule/saveRouteRuleItem",
                    data: formData,
                    type: "post",
                    success: function (result) {
                        if (result) {
                            parent.location.reload();
                            closeIframe();
                        } else {
                            layer.msg("保存失败，请稍后重试！", {icon: 2});
                        }
                    }
                })
                return false;
            });

            render('${itemDto.hubsCode}');
        });

        if ('${itemDto.allCarrier}' == 'false') {
            var codes = '${itemDto.carrierCodes}';
            $("#codesDiv").attr("title", "已选:" + codes).text("已选:" + codes).show();
            var codeArr = codes.split("/");
            $.each(codeArr, function (i, code) {
                var carrier = {code: code};
                carrierData.push(carrier);
            })
        } else if ('${itemDto.allCarrier}' == 'true') {
            $("#chooseCarrier").hide();
        }
    })

    //根据hubsCode查询hub,控制页面效果
    function render(hubsCode) {
        $.get("flight/hubs/getHubByCode?code=" + hubsCode, function (result) {
            if (result) {
                var query = result.isCanQuery;
                var checkPrice = result.isCanCheckprice;
                var payAfterSeat = result.isCanSeat;
                var autoIssue = result.isCanIssue;
                if (!query) {
                    $("input[name='query']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='query']").prop('disabled', false);
                }
                if (!checkPrice) {
                    $("input[name='checkPrice']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='checkPrice']").prop('disabled', false);
                }
                if (!payAfterSeat) {
                    $("input[name='payAfterSeat']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='payAfterSeat']").prop('disabled', false);
                }
                if (!autoIssue) {
                    $("input[name='autoIssue']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='autoIssue']").prop('disabled', false);
                }
                //更新页面渲染
                form.render();
            }
        })
    }

    //关闭iframe
    function closeIframe() {
        //先得到当前iframe层的索引,再执行关闭
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }

    //表单数据处理
    function dataHandler(data) {
        if (!data.allCarrier) {
            data.allCarrier = "0";
        }else{
            data.carrierCodes="";
        }
        if (!data.query) {
            data.query = "0";
        }
        if (!data.checkPrice) {
            data.checkPrice = "0";
        }
        if (!data.payAfterSeat) {
            data.payAfterSeat = "0";
        }
        if (!data.autoIssue) {
            data.autoIssue = "0";
        }
        if (!data.isDisable) {
            data.isDisable = "1";
        }

        var split = data.connectAcc.split("-");
        data.hubsCode = split[0];
        data.hubsConnectId = split[1];


        return data;
    }

    //航司选择器
    function chooseCarrier() {
        new choose.carrierData({
            //给id可以防止一直新建对象
            id: 'multiCarrierData',
            //多选
            multi: true,
            title: "",
            height: 400,
            tableHeight: 277,
            getChoosedData: function () {
                return carrierData;
            }
        }, chooseCallback);
    }

    //选择后的处理函数
    function chooseCallback(result) {
        carrierData = [];
        if (result && result.length > 0) {
            var codes = "";
            $.each(result, function (i, data) {
                var carrier = {code: data.code};
                carrierData.push(carrier);
                codes += data.code + "/";
            })
            var showCodeStr = codes.substring(0, codes.length - 1);
            $("input[name='carrierCodes']").val(showCodeStr);
            $("#codesDiv").attr("title", "已选:" + showCodeStr).text("已选:" + showCodeStr).show();
        } else {
            $("input[name='carrierCodes']").val("");
            $("#codesDiv").text("").show();
        }
    }
</script>