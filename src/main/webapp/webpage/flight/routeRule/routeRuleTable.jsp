<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<table class="table table-striped table-bordered table-hover" id="routeRuleTable">
	<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectAll()" id="selectAll"/></th>
          <th><label>规则级别</label></th>
	      <th><label>适用范围</label></th>
	      <th><label>规则类型</label></th>
          <th><label>是否禁用</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="routeRuleDto">
         <tr class="odd">
        	 <td><input type="checkbox" name="ids" value="${routeRuleDto.id }"/></td>
             <td>${gradeMap[routeRuleDto.grade]}</td>
             <td>
             	<c:if test="${not empty routeRuleDto.scopeName }">
             		${routeRuleDto.scopeName }
             	</c:if>
             	<c:if test="${empty routeRuleDto.scopeName }">
             		--
             	</c:if>
             </td>
             <td>
                 <c:if test="${routeRuleDto.regionType eq '0'}">国内</c:if>
                 <c:if test="${routeRuleDto.regionType eq '1'}">国际</c:if>
             </td>
             <td>
             	<c:if test="${routeRuleDto.isDisable eq true}">是</c:if>
             	<c:if test="${routeRuleDto.isDisable eq false}">否</c:if>
             </td>
             
             <td class="text-center">
             	<a  href="flight/routeRule/getRouteRule?id=${routeRuleDto.id }">修改</a>&nbsp;&nbsp;
             	<a onclick="deleted('${routeRuleDto.id }');">删除</a>
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadTable"></jsp:include>
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>