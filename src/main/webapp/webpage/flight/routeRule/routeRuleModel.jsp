<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    td {
        text-align: center;
    }
    .word {
        width: 200px;
        display: block;
        text-overflow: ellipsis;
        -webkit-line-clamp: 1;
        white-space: nowrap;
        overflow: hidden;
        text-align: center;
    }
</style>
<link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        路由规则维护&nbsp;<a href="flight/routeRule">返回</a>
    </div>
    <div class="panel-body">
        <div class="col-sm-12">
            <!-- 路由规则定义 -->
            <div class="col-sm-12">
                <div class="page-header">
                    <h3>基础信息 |&nbsp;
                        <button class="layui-btn layui-btn-normal" onclick="submitForm()">
                            <i class="layui-icon">&#xe63c;保存</i>
                        </button>
                    </h3>
                </div>
                <form class="form-horizontal" id="routeRuleForm" role="form">
                    <input type="hidden" name="id" id="routeRuleDtoId" value="${routeRuleDto.id}"/>

                    <div class="form-group">
                        <label class="col-sm-2">规则级别</label>
                        <div class="col-sm-3">
                            <c:if test="${not empty gradeMap }">
                                <c:forEach items="${gradeMap }" var="grade" varStatus="vs">
                                    <c:if test="${vs.index eq 0 }">
                                        <input type="radio" class="gradeClass" name="grade" checked="checked"
                                               value="${grade.key }"/>${grade.value }
                                    </c:if>
                                    <c:if test="${vs.index gt 0 }">
                                        <input type="radio" class="gradeClass" name="grade"
                                               value="${grade.key }"/>${grade.value }
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </div>

                        <!-- 当选择TMC或企业是出现选择器 -->
                        <div class="col-sm-4" hidden="hidden" id="chooseDepDiv">
                            <input type="hidden" name="scope" id="bpId" value="${routeRuleDto.scope }">
                            适用范围： <span id="bpName">${routeRuleDto.scopeName }</span>
                            <button type="button" class="btn btn-default btn-xs" onclick="chooseDep()">请选择...</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 ">状态</label>
                        <div class="col-sm-3">
                            <input type="radio" name="isDisable" value="0" checked="checked"/>启用
                            <input type="radio" name="isDisable" value="1"/>禁用
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 ">国内/国际类型</label>
                        <div class="col-sm-3">
                            <input type="radio" name="regionType" value="0" <c:if test="${routeRuleDto.regionType eq '0'}">checked</c:if> <c:if test="${not empty routeRuleDto.id}">disabled</c:if> checked="checked"/>国内
                            <input type="radio" name="regionType" <c:if test="${routeRuleDto.regionType eq '1'}">checked</c:if> <c:if test="${not empty routeRuleDto.id}">disabled</c:if> value="1"/>国际
                        </div>
                    </div>

                    <c:if test="${ not empty routeRuleDto}">
                        <div class="form-group">
                            <label class="col-sm-2 ">创建人</label>
                            <div class="col-sm-4">
                                <label>${creator.fullname }</label>
                            </div>

                            <label class="col-sm-2 ">创建时间</label>
                            <div class="col-sm-4">
                                <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                                       value="${routeRuleDto.createDate }"/></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 ">最后修改人</label>
                            <div class="col-sm-4">
                                <label>${lastModifier.fullname }</label>
                            </div>

                            <label class="col-sm-2 ">最后修改时间</label>
                            <div class="col-sm-4">
                                <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
                                                       value="${routeRuleDto.lastUpdateDate }"/></label>
                            </div>
                        </div>
                    </c:if>
                </form>
            </div>

            <c:if test="${routeRuleDto.id != null}">
                <!-- 配置项-->
                <div class="col-sm-12">
                    <div class="page-header">
                        <h3>配置信息 |&nbsp;
                            <button class="layui-btn layui-btn-normal" onclick="getRouteRuleItem(${routeRuleDto.id},null,${routeRuleDto.regionType})">
                                <i class="layui-icon">&#xe61f;连接</i>
                            </button>
                        </h3>
                    </div>
                    <div>
                        <table class="table table-striped table-bordered" lay-skin="row" id="itemTable">
                            <thead>
                            <tr>
                                <td><label>适用航司</label></td>
                                <td><label>连接名称</label></td>
                                <td><label>性质</label></td>
                                <td><label>优先级</label></td>
                                <td><label>参与查询</label></td>
                                <td><label>参与验价</label></td>
                                <td><label>支付前占座</label></td>
                                <td><label>自动出票</label></td>
                                <td><label>状态</label></td>
                                <td><label>操作</label></td>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${routeRuleDto.routeRuleItems}" var="item">
                                <tr <c:if test="${item.hubsConnect.productType=='UN_STANDARD_REFUND'}">style="background-color: #E6F2FA" </c:if>>
                                    <c:choose>
                                        <c:when test="${item.allCarrier}">
                                            <c:set var="codes" value="ALL"></c:set></c:when>
                                        <c:otherwise>
                                            <c:set var="codes" value="${item.carrierCodes}"></c:set>
                                        </c:otherwise>
                                    </c:choose>
                                    <td title="${codes}">
                                        <div class="word">${codes}</div>
                                    </td>
                                    <td>${item.hubsCode.concat("-").concat(item.hubsConnect.name)}</td>
                                    <td>${item.productType.msg}</td>
                                    <td>${item.priority}</td>
                                    <td><input type="checkbox" <c:if test="${item.query}">checked</c:if> disabled></td>
                                    <td><input type="checkbox" <c:if test="${item.checkPrice}">checked</c:if> disabled></td>
                                    <td><input type="checkbox" <c:if test="${item.payAfterSeat}">checked</c:if> disabled></td>
                                    <td><input type="checkbox" <c:if test="${item.autoIssue}">checked</c:if> disabled></td>
                                    <td>${item.isDisable?"禁用":"启用"}</td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-sm"
                                                onclick="getRouteRuleItem(${routeRuleDto.id},${item.id},${routeRuleDto.regionType})">编辑
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm"
                                                onclick="deleteItem(${item.id})">删除
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </c:if>
        </div>
    </div>
</div>
<script type="text/javascript" src="resource/plugins/json-utils/jquery.serializejson.min.js"></script>
<script type="text/javascript" src="webpage/flight/routeRule/routeRule.js?version=${globalVersion}"></script>
<script type="text/javascript">
    $(function () {
        if ('${routeRuleDto.isDisable}' == 'true') {
            $("input[name='isDisable'][value='1']").attr("checked", true);
        } else {
            $("input[name='isDisable'][value='0']").attr("checked", true);
        }

        if ('${routeRuleDto.grade}') {
            $("input[name='grade'][value='${routeRuleDto.grade}']").attr("checked", true);
        }

        var val = $('input:radio[class="gradeClass"]:checked').val();
        if (val != 1) {
            //显示后面的div
            $("#chooseDepDiv").attr("hidden", false);
        } else {
            //隐藏后面的div
            $("#chooseDepDiv").attr("hidden", true);
        }

    })

    $(".gradeClass").change(function () {
        //清空内容
        $("#bpId").val("");
        $("#bpName").text("");
        var value = $(this).val();
        if (value != 1) {
            //显示后面的div
            $("#chooseDepDiv").attr("hidden", false);
        } else {
            //隐藏后面的div
            $("#chooseDepDiv").attr("hidden", true);
        }
    })
    //选择合作伙伴
    function chooseDep() {
        var val = $('input:radio[class="gradeClass"]:checked').val();
        //默认为0
        var type = 0;
        if (val == 2) {
            //选中TMC时为1
            type = 1;
        }

        TR.select('partner', {type: type}, function (data) {
            $("#bpId").val(data.id);
            $("#bpName").text(data.name);
        });
    }

</script>
