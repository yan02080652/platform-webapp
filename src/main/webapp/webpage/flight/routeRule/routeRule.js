//====================================列表页=============================================
$(function () {
    reloadTable();
})

//加载列表
function reloadTable(pageIndex) {
    var routeRuleGrade = $("#routeRuleGrade").val();
    var pid = $("#pid").val();
    var regionType = $("#regionType").val();
    $.post('flight/routeRule/list', {
        ruleGrade: routeRuleGrade,
        pid: pid,
        regionType:regionType,
        pageIndex: pageIndex,
    }, function (data) {
        $("#routeRule_Table").html(data);
    });
}

$("#pname").click(function () {
    TR.select('partner', {
        //type:0//固定参数
    }, function (data) {
        $("#pid").val(data.id);
        $("#pname").val(data.name);
        reloadTable();
    });
});


//重置
function reset_routeRule() {
    $("#routeRuleGrade").val("");
    $("#pid").val("");
    $("#pname").val("");
    reloadTable();
}

//批量删除
function batchDelete() {
    //判断至少选了一项
    var checkedNum = $("#routeRuleTable input[name='ids']:checked").length;
    if (checkedNum == 0) {
        $.alert("请选择你要删除的记录!", "提示");
        return false;
    }

    var ids = new Array();
    $("#routeRuleTable input[name='ids']:checked").each(function () {
        ids.push($(this).val());
    });

    deleteRouteRule(ids.toString());
}

//单个删除
function deleted(id) {
    deleteRouteRule(id);
};

//确认删除
function deleteRouteRule(id) {
    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: "删除这条记录时会同时删除所配置的规则项，确认删除所选记录?",
        confirm: function () {
            $.ajax({
                cache: true,
                type: "POST",
                url: "flight/routeRule/delete",
                data: {
                    id: id
                },
                async: false,
                error: function (request) {
                    layer.alert("请求失败，请刷新重试", {icon: 2})
                },
                success: function (result) {
                    if (result.code == '0') {
                        reloadTable();
                    } else {
                        layer.alert(result.data, {icon: 2})
                    }
                }
            });
        },
        cancel: function () {
        }
    });
}

//checkbox全选
function selectAll() {
    if ($("#selectAll").is(":checked")) {
        $(":checkbox").prop("checked", true);//所有选择框都选中
    } else {
        $(":checkbox").prop("checked", false);
    }
}


//=================================详情页================================================

//表单提交
function submitForm() {
    var formData = $("#routeRuleForm").serializeJSON();
    formData['regionType'] = $("input[name='regionType']:checked").val();
    console.log(formData)

    //当选择的级别是企业或TMC时，适用范围必选
    if (formData.grade != '1') {
        if (!formData.scope) {
            layer.msg("请选择适用范围企业!", {icon: 0, time: 1000})
            return;
        }
    }

    $.ajax({
        url: "flight/routeRule/save",
        type: "post",
        data: formData,
        success: function (result) {
            if (result.code == '0') {
                window.location.href = '/flight/routeRule/getRouteRule?id=' + result.data.id;
            } else {
                layer.alert(result.data, {icon: 2})
            }
        }
    })
}

//配置项详情页
function getRouteRuleItem(routeRuleId, id,regionType) {
    var url = "flight/routeRule/getRouteRuleItem?routeRuleId=" + routeRuleId+"&regionType="+regionType;
    if (id) {
        url += "&id=" + id;
    }
    layer.open({
        type: 2,
        area: ['900px', '600px'],
        title: "连接配置",
        closeBtn: 1,
        content: url,
    });
}

function deleteItem(id) {
    layer.confirm('你确定要删除该配置项吗?', function (index) {
        layer.close(index);
        var loadIndex = layer.load();
        $.ajax({
            type: "get",
            url: "flight/routeRule/deleteRouteRuleItem?id=" + id,
            success: function (result) {
                layer.close(loadIndex);
                if (!result) {
                    layer.msg("删除失败，请稍后重试!", {icon: 2, time: 2000})
                    return false;
                }
                window.location.reload();
            },
            error: function () {
                layer.msg("系统错误，请稍后刷新重试！", {icon: 2, time: 2000});
                layer.close(loadIndex);
            }
        });
    });
}