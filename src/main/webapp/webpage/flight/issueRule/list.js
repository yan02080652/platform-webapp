
var url={
    list:function () {
        return 'flight/issueRule/list';
    } ,
    delete:function (id) {
        return 'flight/issueRule/delete/'+id;
    }
}
var list = {
    init: function () {
        //企业选择器
        $(".issueRule").click(function () {

            if($("#admin").val() == 'false') {
                TR.select('partner',{type:0},function(data){
                    $("#partnerId").val(data.id);
                    $("#partnerName").val(data.name);
                    list.reloadList();
                });
            } else {
                TR.select('partner',function(data){
                    $("#partnerId").val(data.id);
                    $("#partnerName").val(data.name);
                    list.reloadList();
                });
            }

        });
        list.reloadList();
    },

    reloadList: function (pageIndex) {
        var partnerId = $.trim($("#partnerId").val());
        $.post(url.list(), {
            pageIndex: pageIndex,
            partnerId: partnerId,
        }, function (data) {
            $("#listData").html(data);
        });
    },

    reset: function () {
        $("#partnerId").val("");
        $("#partnerName").val("")
        list.reloadList();
    },

    delete: function (id) {
        $.confirm({
            title: '提示',
            confirmButton: '确认',
            cancelButton: '取消',
            content: "确认删除所选记录?",
            confirm: function () {
                $.ajax({
                    cache: true,
                    type: "POST",
                    url: url.delete(id),
                    async: false,
                    error: function (request) {
                        $.alert("删除失败,请刷新重试！", "提示");
                    },
                    success: function (result) {
                        if (result == 'success') {
                            list.reloadList();
                        } else {
                            $.alert("删除失败,请刷新重试！", "提示");
                        }
                    }
                });
            },
            cancel: function () {
            }
        });
    },
}