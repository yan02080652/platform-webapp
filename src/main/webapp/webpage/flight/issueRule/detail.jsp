<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
	.form-group {
		padding-top: 8px;
		padding-bottom: 8px;
	}
    .popover.right>.arrow{
        top:50% !important;
    }
    .control-label .reqMark{
        color: red;
        font-weight: bold;
        font-size: 15px;
    }
    #issueRuleForm .input-group{
        float: left;
        padding-left: 15px;
        padding-right: 15px;
    }
    #issueRuleForm .input-group-addon {
        border-left: none;
        border-bottom-right-radius:4px;
        border-top-right-radius:4px;
        cursor: pointer;
    }
    #issueRuleForm .btn-success{
        background-color: #46A2BB;
        border-color: #46A2BB;
    }
	 .spinner {  
	  width: 100px;  
	}  
	.spinner input {  
	  text-align: right;  
	}  
	.input-group-btn-vertical {  
	  position: relative;  
	  white-space: nowrap;  
	  width: 1%;  
	  vertical-align: middle;  
	  display: table-cell;  
	}  
	.input-group-btn-vertical > .btn {  
	  display: block;  
	  float: none;  
	  width: 100%;  
	  max-width: 100%;  
	  padding: 8px;  
	  margin-left: -1px;  
	  position: relative;  
	  border-radius: 0;  
	}  
	.input-group-btn-vertical > .btn:first-child {  
	  border-top-right-radius: 4px;  
	}  
	.input-group-btn-vertical > .btn:last-child {  
	  margin-top: -2px;  
	  border-bottom-right-radius: 4px;  
	}  
	.input-group-btn-vertical i{  
	  position: absolute;  
	  top: 0;  
	  left: 4px;  
	}  
</style>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>

<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        <c:choose>
            <c:when test="${not empty refundChangeRuleDto.id}">
                修改
            </c:when>
            <c:otherwise>
                新增
            </c:otherwise>
        </c:choose>
       TMC机票出票拦截规则项&nbsp;<a href="flight/issueRule">返回</a>
    </div>
    <div class="panel-body">
        <form id="issueRuleForm" class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">名称：</label>
                <div class="col-sm-4">
                	<input type="hidden" name="id" id="id" value="${issueRuleDto.id }">
                    <input type="text" name="name" class="form-control" style="width: 300px" value="${issueRuleDto.name }">
                </div>
                <div class="col-sm-2" style="width: 100px;padding-top: 6px">
                	规则类型：
                </div>
                
                <div class="col-sm-2" style="width: 140px;padding-right: 0px;padding-top: 6px">
               		<input name="type" type="radio" value="tmc"/>TMC级
               		<input name="type" type="radio" value="company"/>企业级
                </div>
                <label  class="col-sm-2 control-label scope" style="width:100px">适用范围:</label>
                <div class="col-sm-2 text-warning scope" style="padding-left: 0px;line-height: 27px;">
                	<input type="hidden" name="tmcId" value="${issueRuleDto.tmcId }" id="tmcId">
                	<input type="hidden" name="partnerId" id="partnerId" value="${issueRuleDto.partnerId }">
                	<input type="text" name="partnerName" class="form-control"  placeholder="TMC/企业" value="${issueRuleDto.partnerName }" id="partnerName" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">航线：</label>
                <div>
                   <div class="col-sm-2" style="width: 90px;padding-top: 6px">
	               		<input type="checkbox" name="allAirline" value="${issueRuleDto.allAirline }"> 全航线
	               		<input type="hidden" value="${issueRuleDto.allCarrier }">
	               </div>
	               <div class="col-sm-2">
	               		<input type="text" placeholder="选择出发城市" class="form-control" name="fromCityName" id="fromCityName" value="${issueRuleDto.fromCityName }"/>
	               		<input type="hidden" name="fromCityCode" value="${issueRuleDto.fromCityCode }">
	               </div>
	               	<div class="col-sm-1 input-group" style="padding-left: 0px;padding-right:0px;line-height: 33px;width: 18px;">
                    	<span>至</span>
                	</div>	
	               <div class="col-sm-2">
	               		<input type="text" placeholder="选择到达城市" class="form-control" name="toCityName" id="toCityName" value="${issueRuleDto.toCityName }"/>
	               		<input type="hidden" name="toCityCode" value="${issueRuleDto.toCityCode }">
	               </div>
	               <div class="col-sm-2" style="width: 100px;padding-top: 6px">
	               		<input type="checkbox" name="roundTrip" value="${issueRuleDto.roundTrip }">含双程
	               	</div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" style="padding-top: 13px">航司：</label>
                <div class="col-sm-2" style="padding-top: 13px;width: 90px">
                	<input type="checkbox" name="allCarrier" value="${issueRuleDto.allCarrier }"> 全航司
                </div>
                <div class="col-sm-2 carrier" style="padding-top: 8px">
                	<input type="text" placeholder="选择航空公司" class="form-control" name="carrierName" value="${issueRuleDto.carrierName }" id="carrierName" readonly>
                	<input type="hidden" name="carrierCode" id="carrierCode" value="${issueRuleDto.carrierCode }">
                </div>
                <div class="col-sm-6" style="padding-top: 6px">
                	<div class="col-sm-2" style="padding-top: 6px">
    	            	<input type="checkbox" name="allCabin" value="${issueRuleDto.allCabin }"> 所有舱位
                	</div>
                	<div class="col-sm-2" style="padding-left: 0px;padding-top: 6px;width: 80px">
	                <input type="checkbox" name="cabinCode" value="Y" id="cabinCodeY"> 经济舱
                	</div>
                	
                	 <div class="input-group spinner col-sm-3" style="padding-left: 0px;width: 80px;" id="discountDiv">
                	 <div>
		               	<input type="text" style="height: 35px;width: 25px;text-align:center; vertical-align:middel;" name="discount" value="${issueRuleDto.discount }">
                	 </div>
		          		<div class="input-group-btn-vertical" style="padding-left: 0px;padding-top: 0px;padding-bottom: 20px">
					     <button class="btn btn-default up" type="button" style="height: 18.5px;width: 18px"><i class="fa fa-caret-up"></i></button>
					     <button class="btn btn-default down" type="button" style="height: 18.5px"><i class="fa fa-caret-down"></i></button>
					     </div>
					    <div style="width: 70px" style="padding-top: 1px">
							     折以上
					    </div>
				     </div>

                	<div class="col-sm-2" style="padding-top: 6px;padding-left: 0px">
    	            	<input type="checkbox" name="cabinCode" value="C"> 公务舱
                	</div>

                	<div class="col-sm-2" style="padding-top: 6px;padding-left: 0px">
	                	<input type="checkbox" name="cabinCode" value="F"> 头等舱
                	</div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">时间段：</label>
                
                <div class="col-sm-2" style="padding-top: 6px;width: 90px">
                	<input type="checkbox" name="allTime" value="${issueRuleDto.allTime }"> 全时段
                </div>
                <div class="col-sm-2" style="width: 140px">
                	<input placeholder="选择起始时间" class="form-control time-picker" name="startTime" value="${issueRuleDto.startTime }" readonly>
                </div>
                <div class="col-sm-1 input-group" style="padding-left: 0px;padding-right:0px;line-height: 33px;width: 18px;">
                   	<span>至</span>
                </div>	
                <div class="col-sm-2" style="width: 140px">
                	<input placeholder="选择终止时间" class="form-control time-picker" name="endTime" value="${issueRuleDto.endTime }" readonly>
                </div>
                <div class="col-sm-2" style="padding-top: 6px;width: 150px">
                	<input type="checkbox" name="weekend" value="${issueRuleDto.weekend }"> 含周六周日
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" style="padding-top: 13px">销售价格：</label>
                
                <div class="col-sm-2" style="padding-top: 13px">
                	<input type="text" style="width: 70px" name="priceThreshold" value="${issueRuleDto.priceThreshold }"> 元以上
                </div>
                <div class="col-sm-6" style="padding-left: 10px;padding-top: 6px">
                	<div class="col-sm-3" style="padding-top: 10px ">
	                                                             起飞时间距离当前:
                	</div>
                    <div class="input-group spinner col-sm-3" style="padding-left: 0px;width: 80px;" id="hourDiv"> 
                	 <div>
		               	<input type="text" style="height: 35px;width: 30px;text-align:center; vertical-align:middel;" name="timeDifference" value="${issueRuleDto.timeDifference }" >
                	 </div>
		          		<div class="input-group-btn-vertical" style="padding-left: 0px;padding-top: 0px;padding-bottom: 20px">  
					     <button class="btn btn-default " type="button" style="height: 18.5px;width: 18px"><i class="fa fa-caret-up"></i></button>
					     <button class="btn btn-default " type="button" style="height: 18.5px" ><i class="fa fa-caret-down"></i></button>
					     </div>
					    <div style="width: 70px;padding-top: 1px;" >
							    小时以上
					    </div>
				     </div>
                    
                    <!-- <input type="text" style="width: 30px" name="timeDifference">小时以上 -->
                </div>
            </div>
            <div class="form-group" >
				 <label class="col-sm-2 control-label">状态：</label>
				 <div class="col-sm-1" style="padding-top: 6px">            
				 	<input type="radio" name="status" value="1" checked="checked"> 启用
				 </div>
				 <div class="col-sm-1" style="padding-top: 6px">
				 	<input type="radio" name="status" value="0"> 禁用
				 </div>
            </div>
            
            
            <div class="form-group">
                <label class="col-sm-2 control-label" id="creatorDiv">创建人：</label>
                <div class="col-sm-2" style="padding-top: 8px">
    	            ${issueRuleDto.createName }&nbsp;&nbsp;
	                <fmt:formatDate value='${issueRuleDto.createDate }' pattern="yyy-MM-dd HH:mm:ss"/>
                </div>
                <label class="col-sm-2 control-label" id="updaterDiv">更新人：</label>
                <div class="col-sm-2" style="padding-top: 8px">
    	            ${issueRuleDto.updateName }&nbsp;&nbsp;
	                <fmt:formatDate value='${issueRuleDto.updateDate }' pattern="yyy-MM-dd HH:mm:ss"/>
                </div>
                
            </div>

            <div class="form-group" style="margin-left: 200px;  ">
                <div class="col-sm-2" style="text-align: right">
                    <button type="button" class="btn btn-success" style="width: 120px;" onclick="save()">保存</button>
                </div>
                <div class="col-sm-2" style="text-align: right">
                    <button type="button" class="btn btn-success" style="width: 120px;" onclick="resetForm()" >重置</button>
                </div>
                <div class="col-sm-2" style="text-align: right">
                    <a type="button" class="btn btn-success" style="width: 120px;" href="flight/issueRule" >返回规则列表</a>
                </div>
            </div>
        </form>

    </div>
</div>
<input type="hidden" value="${issueRuleDto.status }" id="ruleStatus">
<input type="hidden" value="${type }" id="UpdateOrClone">
<input type="hidden" value="${issueRuleDto.cabinCode }" id="cabinCodes">
<input type="hidden" value="${issueRuleDto.creator }" id="creator">
<input type="hidden" value="${issueRuleDto.updater }" id="updater">
<input type="hidden" value="${admin}" id="admin">

<script type="text/javascript" src="webpage/flight/issueRule/issueRule.js?version=${globalVersion}"></script>
<script type="text/javascript" src="webpage/flight/issueRule/cityPicker-min.js?version=${globalVersion}"></script>
<script type="text/javascript" src="webpage/flight/issueRule/cityData.js?version=${globalVersion}"></script>
