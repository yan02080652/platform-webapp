var addForm;

var admin;

$(function(){

	admin = $("#admin").val();

	initForm();
	
	var flightFormCity;
	var flightArriveCity;
	
	$.ajax({
	    url: 'flight/issueRule/cityData.json',
	    type: 'post',
	    async: false,
	    success: function(data){
	        allCity = data;
	        flightFormCity = new cityPicker('#fromCityName',allCity);
	        flightArriveCity = new cityPicker('#toCityName',allCity);
	    }
	});
	 //tmc 企业选择
	 tmcAndCompanySelect();


	 $(".time-picker").jeDate({
         format: "hh:mm"
     });
	 
	 $("#fromCityName").click(function(){
	 	if (flightFormCity.getResult()) {
	        $("input[name='fromCityCode']").val(flightFormCity.getResult().code);
	    }
	 });
	 
	 $("#toCityName").click(function(){
		 if (flightArriveCity.getResult()) {
	        $("input[name='toCityCode']").val(flightArriveCity.getResult().code);
	     }
	 });
	 
	 $($("input[name='type']").eq(0)).click(function(){
		if(admin == 'false') {
            $(".scope").hide();
            $("#partnerId").prop("disabled",true);
            $("#partnerName").prop("disabled",true);
		}

	 });
	 $($("input[name='type']").eq(1)).click(function(){
	 	if(admin == 'false') {
            $(".scope").show();
            $("#partnerId").prop("disabled",false);
            $("#partnerName").prop("disabled",false);
		}

	 });
	 
	 if($("#id").val() == '') {
		 $("input[name='type']").eq(0).attr("checked","checked");
	 }
	
	 if($("input[name='type']:checked").val() == 'tmc') {
	 	if(admin == 'false') {
            $(".scope").hide();
            $("#partnerId").prop("disabled",true);
            $("#partnerName").prop("disabled",true);
		}

	 }
	 
	 $('.spinner .btn:first-of-type').on('click', function() {
		 
		 if($(this).parents(".spinner").find("input").attr("name") == 'discount') {
			 if($(this).parents(".spinner").find("input").val() <10) {
				 var num = $(this).parents(".spinner").find("input").val() == '' ? 0 : $(this).parents(".spinner").find("input").val();
				 $($(this).parents(".spinner").find("input")).val(parseInt(num, 10) + 1);  
			 }
		 } else {
			 if($(this).parents(".spinner").find("input").val() <24) {
				 var num = $(this).parents(".spinner").find("input").val() == '' ? 0 : $(this).parents(".spinner").find("input").val();
				 $($(this).parents(".spinner").find("input")).val(parseInt(num, 10) + 1);  
			 }
		 }
		 
	  });  
	  $('.spinner .btn:last-of-type').on('click', function() {
		 if($(this).parents(".spinner").find("input").val() > 0) {
			 $($(this).parents(".spinner").find("input")).val(parseInt($(this).parents(".spinner").find("input").val(), 10) - 1);  
		 }
	  }); 
	 
	  
	  $("input[name='allCabin']").click(function(){
		  if($(this).is(":checked")) {
			  $("#cabinCodeY").prop("checked",true);
			  $("input[name='cabinCode']").each(function(){
				  $(this).prop("checked",true);
			  });
			  $(".up").prop("disabled",false);
			  $(".down").prop("disabled",false);
			  $("input[name='discount']").prop("disabled",false);
		  } else {
			  $("input[name='cabinCode']").each(function(){
				  $(this).prop("checked",false);
			  });
			  $("#cabinCodeY").prop("checked",false);
			  $("input[name='discount']").prop("disabled",true);
			  $(".up").prop("disabled",true);
			  $(".down").prop("disabled",true);
		  }
	  });
	  
	  $("#cabinCodeY").click(function(){
		 if(!$(this).is(':checked')) {
			 $("input[name='discount']").prop("disabled",true);
			 $(".up").prop("disabled",true);
			 $(".down").prop("disabled",true);
		 } else {
			 $("input[name='discount']").prop("disabled",false);
			 $(".up").prop("disabled",false);
			 $(".down").prop("disabled",false);
		 }
	  });
	  
	  
	  if(!$("#cabinCodeY").is(':checked')){
		  $("input[name='discount']").prop("disabled",true);
		  $(".up").prop("disabled",true);
		  $(".down").prop("disabled",true);
	  }
	  
	  $("input[name='allAirline']").click(function(){
		  if($(this).is(':checked')) {
              $("#fromCityName").val("");
              $("#toCityName").val("");
			  $("#fromCityName").prop("disabled",true);
			  $("#toCityName").prop("disabled",true);

		  } else {
			  $("#fromCityName").prop("disabled",false);
			  $("#toCityName").prop("disabled",false);
		  }
	  });
	  $("input[name='allCarrier']").click(function(){
		  if($(this).is(':checked')) {
			  $("input[name='carrierName']").val("");
			  $("input[name='carrierName']").prop("disabled",true);
		  } else {
			  $("input[name='carrierName']").prop("disabled",false);
		  }
	  });
	  $("input[name='allTime']").click(function(){
		  if($(this).is(':checked')) {
			  $("input[name='startTime']").val("");
			  $("input[name='endTime']").val("");
			  $("input[name='startTime']").prop("disabled",true);
			  $("input[name='endTime']").prop("disabled",true);

		  } else {
			  $("input[name='startTime']").prop("disabled",false);
			  $("input[name='endTime']").prop("disabled",false);
		  }
	  });
	  
	//航司选择
	 $(".carrier").click(function () {
		 if(!$("input[name='allCarrier']").is(":checked")) {
			 TR.select('flightCompany', {}, function (result) {
				 $("#carrierName").val(result ? result.nameShort : null);
				 $("#carrierCode").val(result ? result.code : null);
			 });
		 }
     });
	 
	 $("input[name='cabinCode']").each(function(){
		 if(!$(this).is(":checked")) {
			 $("input[name='allCabin']").prop("checked",false);
		 }
		 $(this).click(function(){
			 if(!$(this).is(":checked")) {
				 $("input[name='allCabin']").prop("checked",false);
			 }
		 });
	 });
	  
	  var rules = {
		  name: {
              required: true
          },
          fromCityName: {
              required: true
          },
          toCityName: {
              required: true
          },
          carrierName: {
              required: true
          },
          startTime: {
              required: true
          },
          endTime: {
              required: true
          },
          status: {
        	  required: true
          },
          partnerName : {
        	  required: true
          },
          cabinCode : {
        	  required : true
          },
          priceThreshold : {
        	  number : true
          },
          timeDifference : {
        	  number : true
          },
          discount : {
        	  number : true
          },

      };

      //添加必填标记
      $.each(rules, function (code, rule) {
          if (rule.required || rule.type) {
        	  var requiredMark = $("<span class='reqMark'>*</span>");
              var element = $("#issueRuleForm").find("[name=" + code + "]");
              var label = element.parentsUntil('.form-group').parent().children('.control-label');
              if ($(".reqMark", label).length == 0) {
                  label.append(requiredMark);
              }
        	  
          }
      });
	  
	  $("#issueRuleForm").validate({
          rules: rules,
          errorPlacement: setErrorPlacement,
          success: validateSuccess,
          highlight: setHighlight,
          submitHandler: function (form) {
        	  $("#fromCityName").trigger("click");
        	  $("#toCityName").trigger("click");
        	  var arr = $(form).serializeArray();
        	  var data = {};
        	  $.each(arr,function(i,ar){
        		  if(ar.name == 'cabinCode') {
        			  var cabinCodes = new Array();
        			  $("input[name='cabinCode']").each(function(){
        				  if($(this).is(":checked")) {
        					  cabinCodes.push($(this).val());
        				  }
        			  });
        			  ar.value = cabinCodes.join("/");
        		  }
        		  if(ar.name == 'discount') {
        			  if($("#cabinCodeY").is(":checked")) {
        				  ar.value = $("input[name='discount']").val();
        			  }
        		  }
        		  if(!$("input[name='allAirline']").is(":checked") && addForm) {
        			  if(ar.name == 'fromCityCode') {
        				  if (flightFormCity.getResult()) {
        					  ar.value = flightFormCity.getResult().code;
        				  }
        			  }
        			  if (ar.name == 'toCityCode') {
        				  ar.value = flightArriveCity.getResult().code;
        			  }
        		  }
        		  data[ar.name] = ar.value;
        	  });
        	 
        	  delete data.type;
              $.ajax({
                  type: "POST",
                  url: "flight/issueRule/save",
                  data: data,
                  success: function (result) {
                	  if(result) {
            			  location.href = "/flight/issueRule";
                	  } else {
                		  $.alert("请求失败，请刷新重试！");
                	  }
                  },
                  error: function (request) {
                      $.alert("请求失败，请刷新重试！");
                  },
              });
          }
      });
	  
	  
	  
});



function save(){
	$("#issueRuleForm").submit();
}


function resetForm(){
	
	var UpdateOrClone = $("#UpdateOrClone").val();
	
	if($("#id").val() == '' && UpdateOrClone == '1') {
		$("#issueRuleForm").reset();
		$("input[name='discount']").val("7");
		$("input[name='timeDifference']").val("2");
	} else {
		location.href = "/flight/issueRule/detail?type=1&id="+$("#id").val();
	}
	
}

function initForm() {
	
	var UpdateOrClone = $("#UpdateOrClone").val();
	
	if($("#id").val() == '' && UpdateOrClone == '1') {
		addForm = true;
		$("input[name='discount']").val("7");
		$("input[name='timeDifference']").val("2");
		
		$("input[name='allAirline']").removeAttr("value");
		$("input[name='roundTrip']").removeAttr("value");
		$("input[name='allCarrier']").removeAttr("value");
		$("input[name='allCabin']").removeAttr("value");
		$("input[name='allTime']").removeAttr("value");
		$("input[name='weekend']").removeAttr("value");
		
	} else {
		addForm = false;
		if($("#tmcId").val() == $("#partnerId").val()) {
			$("input[name='type']").eq(0).prop("checked",true);
		} else {
			$("input[name='type']").eq(1).prop("checked",true);
		}
		if($("input[name='allAirline']").val() == 'true') {
			$("input[name='allAirline']").prop("checked",true);
			$("#fromCityName").prop("disabled",true);
			$("#toCityName").prop("disabled",true);
		}
		if($("input[name='roundTrip']").val() == 'true') {
			$("input[name='roundTrip']").prop("checked",true);
		}
		if($("input[name='allCarrier']").val() == 'true') {
			$("input[name='allCarrier']").prop("checked",true);
			$("input[name='carrierName']").prop("disabled",true);
		}
		if($("input[name='allCabin']").val() == 'true') {
			$("input[name='allCabin']").prop("checked",true);
			$("input[name='cabinCode']").each(function(){
				  $(this).prop("checked",true);
			 });
			$("input[name='discount']").prop("disabled",false);
		}
		if($("input[name='allTime']").val() == 'true') {
			$("input[name='allTime']").prop("checked",true);
			$("input[name='startTime']").prop("disabled",true);
			$("input[name='endTime']").prop("disabled",true);
		}
		if($("input[name='weekend']").val() == 'true') {
			$("input[name='weekend']").prop("checked",true);
		}
		$("input[name='allAirline']").removeAttr("value");
		$("input[name='roundTrip']").removeAttr("value");
		$("input[name='allCarrier']").removeAttr("value");
		$("input[name='allCabin']").removeAttr("value");
		$("input[name='allTime']").removeAttr("value");
		$("input[name='weekend']").removeAttr("value");
		
		var st = $("#ruleStatus").val();
		$("input[name='status'][value='"+st+"']").prop("checked",true);
		
		var cabinCodes = $("#cabinCodes").val().split("/");
		for(var i = 0;i<cabinCodes.length;i++){
			$("input[name='cabinCode'][value='"+cabinCodes[i]+"']").prop("checked",true);
		}
	}
	
	if($("#creator").val() == '') {
		$("#creatorDiv").hide();
	} else {
		$("#creatorDiv").show();
	}
	
	if($("#updater").val() == '') {
		$("#updaterDiv").hide();
	} else {
		$("#updaterDiv").show();
	}
}

function tmcAndCompanySelect() {

	if(admin == 'true') {
		//超级管理员可选择tmc
        $("#partnerName").click(function(){
        	if($("input[name='type']:checked").val() == 'tmc') {
                TR.select('partner',{type:1}, function (data) {
                    $("#partnerId").val(data.id);
                    $("#partnerName").val(data.name);
                    $("#tmcId").val(data.id);
                });
			} else {
                TR.select('partner',{type:0},function (data) {
                    $("#partnerId").val(data.id);
                    $("#partnerName").val(data.name);
                });
			}

        });
	} else {
		//非超级管理员选择企业
        $("#partnerName").click(function(){
            TR.select('partner',{type:0}, function (data) {
                $("#partnerId").val(data.id);
                $("#partnerName").val(data.name);
                $("#tmcId").val("");
            });
        });
	}

}

