<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div >
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span> 出票拦截规则
            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <a class="btn btn-default" href="flight/issueRule/detail" role="button">新增规则</a>
                                        </div>
                                        <!-- 查询条件 -->
                                        <form class="form-inline" method="post">
                                            <input type="hidden" id="partnerId" >
                                            <nav class="text-right">
                                                <label class="control-label">适用范围：</label>
                                                <div class="input-group issueRule">
                                                    <input type="text" id="partnerName" class="form-control"  placeholder="企业名称" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default" onclick="list.reloadList()">查询</button>
                                                    <button type="button" class="btn btn-default" onclick="list.reset()">重置</button>
                                                </div>
                                            </nav>
                                        </form>
                                    </div>
                                </div>
                                <div id="listData" style="margin-top: 15px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="${admin}" id="admin">
<script type="text/javascript" src="webpage/flight/issueRule/list.js?version=${globalVersion}"></script>
<script>
$(function(){
    list.init();
})
</script>