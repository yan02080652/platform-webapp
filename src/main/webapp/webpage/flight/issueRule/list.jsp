<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th><label>名称</label></th>
        <th><label>适用范围</label></th>
        <th><label>航线</label></th>
        <th><label>含双程</label></th>
        <th><label>航司</label></th>
        <th><label>舱位</label></th>
        <th><label>时间段</label></th>
        <th><label>含周末</label></th>
        <th><label>状态</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="issueRule" >
        <tr class="odd gradeX">
            <td>${issueRule.name}</td>
            <td>${issueRule.partnerName}</td>
            <td>
                <c:choose>
                    <c:when test="${issueRule.allAirline}">全航线</c:when>
                    <c:otherwise>${issueRule.fromCityName}-${issueRule.toCityName}</c:otherwise>
                </c:choose>
            </td>
            <td>
                <c:choose>
                    <c:when test="${issueRule.roundTrip}">是</c:when>
                    <c:otherwise>否</c:otherwise>
                </c:choose>
            </td>
            <td>
            <c:choose>
    	        <c:when test="${issueRule.allCarrier}">全航司</c:when>
	            <c:otherwise>${issueRule.carrierName}</c:otherwise>
            </c:choose>
            
            </td>
            <td>
                <c:choose>
                    <c:when test="${issueRule.allCabin}">全舱位</c:when>
                    <c:otherwise>
                        <c:forEach items="${fn:split(issueRule.cabinCode,'/')}" var="cabin" varStatus="vs">
                            ${cabinClassMap[cabin]}
                            <c:if test="${!vs.last}">/</c:if>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
            </td>
            <td>
                <c:choose>
                    <c:when test="${issueRule.allTime}">全时段</c:when>
                    <c:otherwise>${issueRule.startTime}-${issueRule.endTime}</c:otherwise>
                </c:choose>
            </td>
            <td>
                <c:choose>
                    <c:when test="${issueRule.weekend}">是</c:when>
                    <c:otherwise>否</c:otherwise>
                </c:choose>
            </td>
            <td>
                <c:choose>
                    <c:when test="${issueRule.status eq 1}">开启</c:when>
                    <c:otherwise>禁用</c:otherwise>
                </c:choose>
            </td>
            <td class="text-center">
                <a href="flight/issueRule/detail?type=1&id=${issueRule.id}">修改</a>&nbsp;&nbsp;
                <a href="flight/issueRule/detail?type=0&id=${issueRule.id}">克隆</a>&nbsp;&nbsp;
                <a onclick="list.delete('${issueRule.id }');">删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=list.reloadList"></jsp:include>
</div>