<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" href="resource/css/bootstrap/fileinput.min.css?version=${globalVersion}">
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        <span>机票数据导入</span>
    </div>
    <div class="panel-body">
        <form id="importForm" role="form" class="form-horizontal" method="post" action="flight/data/import" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-2 control-label">企业名称</label>
                <div class="col-sm-4 input-group partner">
                    <input type="hidden" name="pid" id="pid"/>
                    <input type="text" id="pname" class="form-control ignore" placeholder="请选择企业名称" readonly>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">默认预订人</label>
                <div class="col-sm-4 input-group user">
                    <input type="hidden" name="userId" id="userId"/>
                    <input type="text" id="userName" class="form-control ignore"  placeholder="请选择默认预订人" readonly>
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">出票渠道</label>
                <div class="col-sm-4 input-group">
                    <select class="form-control" name="issueChannel">
                        <c:if test="${ not empty issueChannelList}">
                            <c:forEach items="${issueChannelList}" var="issueChannel">
                                <option value="${issueChannel.code}">${issueChannel.name}</option>
                            </c:forEach>
                        </c:if>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">excel文件</label>
                <div class="col-sm-4 input-group">
                    <input type="file" name="excelFile" class="file-loading" id="excelFile">
                    <p class="help-block">支持xls, xlsx格式 <a href="/flight/data/excelTemplateExport"  style="width: 100px;">机票导入模板下载</a></p>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6" style="text-align: right;padding-right:0px;">
                    <button type="submit" class="btn btn-primary" style="width: 100px;">导入</button>
                </div>
            </div>
        </form>
        <div id="filedDiv" class="col-sm-6" hidden>
            <div class="alert alert-danger" role="alert">
                <label>失败记录数据</label>
                <table class="table table-striped  table-hover " >
                    <thead>
                    <tr>
                        <th><label>航班号</label></th>
                        <th><label>乘机人</label></th>
                    </tr>
                    </thead>
                    <tbody id="tbodyDiv">
                    <tr class="active">
                        <td>dd</td>
                        <td>dd</td>
                    </tr>
                    <tr class="active">
                        <td>dd</td>
                        <td>dd</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script src="/webpage/flight/import/import.js?version=${globalVersion}"></script>