<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>机场信息<c:choose><c:when test="${airportDto.id != '' && airportDto.id != null}">修改</c:when><c:otherwise>添加</c:otherwise></c:choose>
             	 &nbsp;&nbsp;<a data-pjax onclick="backMain()">返回</a>
             </div>
             <div class="panel-body">
                 <form id="airportForm" class="form-horizontal bv-form" > 
                 		<input type="hidden" name="id" value="${airportDto.id}">
					 <div class="form-group">
					    <label class="col-sm-2 control-label">编码</label>
					    <div class="col-sm-6">
					      <input type="text" name="code" id="code" value="${airportDto.code }" class="form-control"  placeholder="机场-城市编码">
					    </div>
					  </div>
					  
					   <div class="form-group">
					    <label class="col-sm-2 control-label">中文名称</label>
					    <div class="col-sm-6">
					      <input type="text" name="nameCn" class="form-control" value="${airportDto.nameCn }" placeholder="中文名称">
					    </div>
					  </div>
					  
					   <div class="form-group">
					    <label class="col-sm-2 control-label">英文名称</label>
					    <div class="col-sm-6">
					      <input type="text" name="nameEn" class="form-control" value="${airportDto.nameEn }" placeholder="英文名称">
					    </div>
					  </div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">拼音</label>
						 <div class="col-sm-6">
							 <input type="text" name="namePy" class="form-control" value="${airportDto.namePy }" placeholder="机场-城市拼音">
						 </div>
					 </div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">首字母</label>
						 <div class="col-sm-6">
							 <input type="text" name="nameSimply" class="form-control" value="${airportDto.nameSimply }" placeholder="首字母">
						 </div>
					 </div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">所在城市</label>
						 <div class="col-sm-6">
							 <input type="text" name="cityName" class="form-control" value="${airportDto.cityName }" readonly="readonly" placeholder="请选择城市" onclick="chooseCity()" id="cityName">
							 <input type="hidden" name="cityId" value="${airportDto.cityId}" id="cityId">
							 <input type="hidden" name="cityEn" value="${airportDto.cityEn}" id="cityEn">
							 <input type="hidden" name="cityEnSimply" value="${airportDto.cityEnSimply}" id="cityEnSimply">
						 </div>
					 </div>
					  
					<div class="form-group">
					    <label class="col-sm-2 control-label">序号</label>
					    <div class="col-sm-6">
					      <input type="number" name="sort" class="form-control" value="${airportDto.sort }" placeholder="序号">
					    </div>
					  </div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">机场/城市类型</label>
						 <div class="col-sm-6">
							 <label class="radio-inline"><input type="radio" name="type" value="0" <c:if test="${airportDto.type == 0 or airportDto.id == '' or airportDto.id == null}">checked="checked"</c:if> />城市</label>
							 <label class="radio-inline"><input type="radio" name="type" value="1" <c:if test="${airportDto.type == 1}">checked="checked"</c:if> />机场</label>
						 </div>
					 </div>

					<div class="form-group">
						<label class="col-sm-2 control-label">状态</label>
						<div class="col-sm-6">
							<label class="radio-inline"><input type="radio" name="isDisable" value="false" <c:if test="${!airportDto.isDisable}">checked="checked"</c:if>/>启用</label>
							<label class="radio-inline"><input type="radio" name="isDisable" value="true" <c:if test="${airportDto.isDisable}">checked="checked"</c:if>/>禁用</label>
						</div>
					</div>

					 <div class="form-group">
						 <label class="col-sm-2 control-label">国内/国际类型</label>
						 <div class="col-sm-6">
							 <label class="radio-inline"><input type="radio" name="airportType" value="0" <c:if test="${airportDto.airportType == 0 or airportDto.id == '' or airportDto.id == null}">checked="checked"</c:if>  />国内</label>
							 <label class="radio-inline"><input type="radio" name="airportType" value="1" <c:if test="${airportDto.airportType == 1}">checked="checked"</c:if> />国际</label>
						 </div>
					 </div>


			</form>
				<div class="form-group">
					<div class="col-sm-offset-7" >
						<a class="btn btn-default"  onclick="submitAirport()">提交</a>
					</div>
				</div>
			</div>
         </div> 
	</div>
</div>
<script type="text/javascript">

//提交表单
function submitAirport(){
	 $('#airportForm').submit();
}

var getDistrictDataChoosedData = JSON.parse('${city}' || '[]');
function chooseCity() {

    new choose.flightDistrictData({
        id:'selMultiDistrictData',//给id可以防止一直新建对象
        empty:true,
        getChoosedData:function () {
            return getDistrictDataChoosedData;
        }
    },function (data) {
        getDistrictDataChoosedData = [];
        if (data) {
            var city = {id: data.id, nameCn: data.nameCn};
            getDistrictDataChoosedData.push(city);
            $("#cityId").val(data.id);
            $("#cityName").val(data.nameCn);
            $("#cityEn").val(data.nameEn);
            $("#cityEnSimply").val(data.nameSimply);
        }
    });
}

$(document).ready(function() {
	$("#airportForm").validate({
		rules : {
        	code : {
                required : true,
                minlength : 1,
                maxlength:20,
                remote:{
                	type:'post',
                	url:'flight/airport/checkCode',
                	data:{
                		code:function(){
                			return $('#code').val();
                		},
						type:function(){
                		    return $("input[name='type']:checked").val();
						},
                	},
					dataFilter : function(data) {
						if (data == 'true') {
							return true;
						} else {
							return false;
						}
					}
                }
            },
            nameCn : {
                required : true,
                minlength : 1,
                maxlength:30
            },
            nameEn:{
                required : true,
                minlength : 1,
                maxlength:50
            },
            namePy : {
                required : true,
                minlength : 1,
                maxlength:30
			},
            nameSimply:{
                required : true,
                minlength : 1,
                maxlength:10
			},
			cityName:{
                required : true,
                minlength : 1,
                maxlength:20
			},
            cityIdAndCode:{
            	type:true
            }
        },
        messages:{
            code:{
               remote:"此编码已经存在"
            }
        },
        submitHandler : function(form) {
        	$.ajax({
				cache : true,
				type : "POST",
				url : "flight/airport/save",
				data : $('#airportForm').serialize(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						$("#airportMain").show();
						$("#airportDetail").hide();
						init();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			}); 
   
        },
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight
    });
	
	//修改时code字段只读并删除code字段验证规则
	var code = $("#code").val();
	if(code){
		//$("#code").attr("readonly",true);
		$("#code").rules("remove","remote");
	}
	
	//状态
	if('${airportDto.isDisable}' == 'true'){
		$("input[name='isDisable'][value='1']").attr("checked",true);
	}else{
		$("input[name='isDisable'][value='0']").attr("checked",true);
	}

	
	
});
</script>