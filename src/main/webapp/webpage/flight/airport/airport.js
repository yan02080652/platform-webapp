$(function(){
	init();
})

//初始化
function init(pageIndex){
	reloadRight(pageIndex);
}

//分页调用的方法
function reloadTable(pageIndex){
	init(pageIndex);
}

//加载列表
function reloadRight(pageIndex){
	var keyword = $("#keyword").val();
	var type = $("#type").val();
	var airportType = $("#airportType").val();
	$.post('flight/airport/list',{
		keyword:keyword,
        type:type,
        airportType : airportType,
		pageIndex:pageIndex,
		//pageSize:size
		},function(data){
		$("#airport_Table").html(null);
		$("#airport_Table").html(data);
	});
}


//查找用户
function search_airport(){
	init();
}

//重置
function reset_search(){
	$("#keyword").val("");
    $("#type").val("");
    $("#airportType").val("");
	init();
}

//转到添加或修改页面
function getAirport(id){
	$.post("flight/airport/getAirport",{'id':id},function(data){
		$("#airportDetail").html(data);
		$("#airportMain").hide();
		$("#airportDetail").show();
	});
}


//批量删除
function batchDelete(){
  //判断至少选了一项
  var checkedNum = $("#airportTable input[name='id']:checked").length;
  if(checkedNum==0){
      $.alert("请选择你要删除的记录!","提示");
      return false;
  }
  
  var ids = new Array();
  $("#airportTable input[name='id']:checked").each(function(){
  	ids.push($(this).val());
  });
  		console.log(ids);
	  deleteAirport(ids.toString());
}

//单个删除
function deleted(id) {
	deleteAirport(id);
};

//确认删除
function deleteAirport(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除所选记录?",
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "flight/airport/delete",
				data : {
					ids : id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						init();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}

//checkbox全选
function selectAll(){
	if ($("#selectAll").is(":checked")) {
        $(":checkbox").prop("checked", true);//所有选择框都选中
    } else {
        $(":checkbox").prop("checked", false);
    }
}
//返回
function backMain(){
	$("#airportMain").show();
	$("#airportDetail").hide().html("");
	$("#nearCityDiv").hide();
}

//获取机场附近城市列表
function nearCity(airportCode){
	$.post("flight/airport/getNearCityList",{'airportCode':airportCode},function(data){
		$("#airportMain").hide();
		$("#nearCityDiv").html(data).show();
	});
}


function synStationData() {
    layer.confirm('确认同步更新城市数据吗?更新时可能要花费一定时间，请勿频繁点击',function (index) {
        layer.close(index);
        var start = layer.load();

        $.ajax({
            url: "flight/airport/batchUpdateAirportCityData",
            type: "post",
            contentType: "application/json",
            datatype: "json",
            async: true,
            success: function (data) {
                layer.close(start);
                if (data) {
                    location.href = "/flight/airport";
                } else {
                    $.alert('系统繁忙,请稍后重试');
                }

            },error:function () {
                layer.close(start);
                $.alert('系统繁忙,请稍后重试');
            }
        });
    });

}

