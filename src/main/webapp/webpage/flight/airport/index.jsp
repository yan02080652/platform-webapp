<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="airportMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>机场-城市后台维护   &nbsp;&nbsp;
			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12"> 
										<div class="pull-left">
											<a class="btn btn-default" onclick="getAirport('')" role="button">新增</a>
											<a class="btn btn-default" onclick="batchDelete()" role="button">删除</a>
										</div>
											<c:if test="${admin}">
												<div class="pull-left" style="padding-left: 10px">
													<a class="btn btn-default" onclick="synStationData()" role="button">同步更新城市数据</a>
												</div>
											</c:if>
											<!-- 查询条件 -->
											<form class="form-inline" method="post" >
												<nav class="text-right">
													<div class=" form-group">
													 <label class="control-label">查询条件：</label>
														<input name="keyword" id="keyword" type="text" value="${keyword }" class="form-control" placeholder="编码/中文名称/英文名称/所在城市" style="width: 250px"/>
													<label class="control-label">机场/城市类型:</label>
													<select  class="form-control" id="type">
														<option value="">---</option>
														<option value="0">城市</option>
														<option value="1">机场</option>
													</select>
													<label class="control-label">国内/国际类型:</label>
													<select  class="form-control" id="airportType">
														<option value="">---</option>
														<option value="0">国内</option>
														<option value="1">国际</option>
													</select>
													</div>
													<div class="form-group">
														<button type="button" class="btn btn-default" onclick="search_airport()">查询</button>
														<button type="button" class="btn btn-default" onclick="reset_search()">重置</button>
													</div>
												</nav>
											</form>
										</div>
									</div>
									<div  id="airport_Table" style="margin-top: 15px">
					
					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 详细页面 -->
<div id="airportDetail" style="display:none;"></div>
<!-- 附近城市页面 -->
<div id="nearCityDiv" style="display:none;"></div>

<script type="text/javascript" src="webpage/flight/airport/airport.js?version=${globalVersion}"></script>