<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
				<span class="glyphicon glyphicon-home" aria-hidden="true"></span>附近城市列表
				&nbsp;&nbsp;
				<a data-pjax onclick="backMain()">返回</a>
			</div>
			<div id="rightDetail" class="col-md-12 animated fadeInRight">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<table class="table table-striped table-bordered table-hover" id="airportTable">
									<thead>
										<tr>
											<th><label>编码</label></th>
											<th><label>中文名称 </label></th>
											<th><label>英文名称</label></th>
											<th><label>是否热门城市</label></th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${not empty cityList }">
												<c:forEach items="${cityList }" var="district">
													<tr class="odd gradeX">
														<td>${district.code }</td>
														<td>${district.nameCn }</td>
														<td>${district.nameEn }</td>
														<td><c:choose>
																<c:when test="${district.isHot }">热门</c:when>
																<c:otherwise>非热门</c:otherwise>
															</c:choose></td>
													</tr>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<tr>
													<td colspan="4" style="text-align: center;"><span style="color: #bbb;"> 没有相关的数据</span></td>
												</tr>
											</c:otherwise>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

