<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<table class="table table-striped table-bordered table-hover" id="airportTable">
	<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectAll()" id="selectAll"/></th>
          <th><label>编码</label></th>
	      <th><label>中文名称 </label></th>
          <th><label>英文名称</label></th>
          <th><label>拼音</label></th>
          <th><label>首字母</label></th>
          <th><label>所在城市</label></th>
          <th><label>状态</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="airport">
         <tr class="odd gradeX">
        	 <td><input type="checkbox" name="id" value="${airport.id }"/></td>
             <td>${airport.code }</td>
        	 <td>${airport.nameCn }</td>
             <td>${airport.nameEn }</td>
             <td>${airport.namePy }</td>
             <td>${airport.nameSimply }</td>
             <td>${airport.cityName }</td>
             <td>
             	<c:choose>
             		<c:when test="${airport.isDisable }">禁用</c:when>
             		<c:otherwise>启用</c:otherwise>
             	</c:choose>
            </td>
             <td class="text-center">
             	<a onclick="getAirport('${airport.id }')" >修改</a>&nbsp;&nbsp;|
             	<a onclick="deleted('${airport.id }');">删除</a>&nbsp;&nbsp;
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadTable"></jsp:include> 
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>