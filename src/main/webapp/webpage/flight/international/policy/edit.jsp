<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .layui-form-label {
        box-sizing: content-box;
        width: 120px;
    }
    .text-input {
        width: 620px !important;
    }
    .reqMark {
        color: red;
        font-weight: bold;
        font-size: 10px;
    }
</style>
<div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>政策详情维护
                <a href="/flight/international/policy">返回</a>
            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form class="layui-form" lay-filter="policy">
                                    <input type="hidden" value="${id}" name="id" id="policyId">
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">外部编号</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="externalNum"  maxlength="50">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">供应商自用编号,格式不限</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">文件编号</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="fileNum" id="fileNum"  maxlength="50">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">自由录入格式,如SHA5700FF500</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">可组文件编号</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="combinationFileNum" lay-verify="combination"  maxlength="50">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">同上,当文件编号有值时,可组文件编号不可为空</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">行程类型</label>
                                        <div class="layui-input-inline">
                                            <select name="airRangeType">
                                                <option value="OW" selected>单程</option>
                                                <option value="RT">往返</option>
                                                <option value="OWRT">单程/往返</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>开票航司</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="billingCarrier" lay-verify="required|carrier"  maxlength="10">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">航司二字代码，只允许录入一家</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">出发地</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="departure" lay-verify="region"   maxlength="500">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">城市三代/国家二代/大区四代/TC区代码,多个用"/"隔开,最多100个,空表示不限制</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">目的地</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="destination" lay-verify="region"   maxlength="500">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">同上</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">出发地除外</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="excludeDeparture" lay-verify="region"   maxlength="500">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">同上</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">目的地除外</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="excludeDestination" lay-verify="region"   maxlength="500">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">同上</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">适用舱位</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="cabinCodes" lay-verify="cabin"  maxlength="100">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">舱位代码,多个有"/"隔开,为空表示不限制</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>是否适用于联运</label>
                                        <div class="layui-input-inline">
                                            <select name="intermodal">
                                                <option value="1">是</option>
                                                <option value="0" selected>否</option>
                                            </select>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">是:允许联运与非联运,否:允许非联运</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">适用联运航司</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="intermodalCarrierCodes" lay-verify="multi_carrier"  maxlength="59">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">航司代码,多个有"/"隔开,为空表示不限制</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">除外联运航司</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="excludeIntermodalCarrierCodes" lay-verify="multi_carrier"  maxlength="59">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">同上</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>是否适用于中转</label>
                                        <div class="layui-input-inline">
                                            <select name="transfer">
                                                <option value="1">是</option>
                                                <option value="0" selected>否</option>
                                                <option value="-1">不限</option>
                                            </select>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">是:只适用于中转,否:只适用于直飞,不限:中转直飞都适用</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">指定转机点</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="transitAirport" lay-verify="multi_city"  maxlength="500">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">城市代码,多个有"/"隔开,为空表示不限制</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>是否适用于代码共享航班</label>
                                        <div class="layui-input-inline">
                                            <select name="shareAirline">
                                                <option value="1">是</option>
                                                <option value="0" selected>否</option>
                                                <option value="-1">不限</option>
                                            </select>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">是:只适用于共享航班,否:只适用于非共享航班,不限:共享于非共享都适用</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>是否允许缺口</label>
                                        <div class="layui-input-inline">
                                            <select name="gap">
                                                <option value="1">是</option>
                                                <option value="0" selected>否</option>
                                                <option value="-1">不限</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>运价类型</label>
                                        <div class="layui-input-inline">
                                            <select name="freightType">
                                                <option value="BSP" selected>BSP</option>
                                                <option value="Amadeus">Amadeus</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">报销凭证</label>
                                        <div class="layui-input-inline">
                                            <select name="reimbursementVoucher">
                                                <option value="ITINERARY" selected>行程单</option>
                                                <option value="TOURISTINVOICE">旅游发票</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">去程旅行时间</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input"  name="departureDate" lay-verify="multi_data_range"  maxlength="500">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">格式:YYYY-MM-DD>YYYY-MM-DD,多个有"/"隔开,为空表示不限制</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">回程旅行时间</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="destinationDate" lay-verify="multi_data_range"  maxlength="500">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">同上</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>销售日期</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="saleDate" lay-verify="required|data_range"  maxlength="50">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">只可填一个</div>
                                    </div>
                                    <div class="layui-form-item" >
                                        <label class="layui-form-label">乘客类型</label>
                                        <div class="layui-input-block" id="passengerTypeDiv">
                                            <input type="checkbox"  value="ADU" lay-skin="primary" title="成人" checked>
                                            <input type="checkbox"  value="CHD" lay-skin="primary" title="成人陪伴儿童" >
                                            <input type="checkbox"  value="INF" lay-skin="primary" title="无座婴儿" >
                                        </div>
                                    </div>
                                    <div class="layui-form-item" >
                                        <label class="layui-form-label">旅客资质</label>
                                        <div class="layui-input-block" id="passengerQualification">
                                            <input type="checkbox"  value="ADULT" lay-skin="primary" title="普通成人" checked>
                                            <input type="checkbox"  value="OVERSEAS_STUDENT" lay-skin="primary" title="留学生" >
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>返点</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="number" class="layui-input" name="rebate" lay-verify="required|number|decimal" id="rebate" min="0" max="99">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">0-99,最多2位小数</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>留钱</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="number" class="layui-input" name="keepMoney" lay-verify="required|number|integer" id="keepMoney">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">正负整数</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">出票备注</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="remark"  maxlength="200">
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">工作时间</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="operatingHours"   maxlength="100">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">最多录入3个时间段</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>1/2R佣金计算方式</label>
                                        <div class="layui-input-inline">
                                            <select name="commissionCalculationMethod">
                                                <option value="STRICT" selected>取严</option>
                                                <option value="EACH">各取各</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">出票时限</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="timeLimitTicketing" lay-verify="time_limit_ticketing"  maxlength="30">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">举例:0-356,7,3</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">出票城市</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="ticketingCity" lay-verify="city"  maxlength="3">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">录入一个城市三代</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">适用乘客国籍</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="passengerNationality" lay-verify="nationality"  maxlength="100">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">国家代码,多个用"/"隔开,为空表示不限制</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">除外乘客国籍</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="text" class="layui-input" name="excludePassengerNationality" lay-verify="nationality"  maxlength="100">
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">同上</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>是否转私有</label>
                                        <div class="layui-input-inline">
                                            <select name="transferPrivate" id="transferPrivate">
                                                <option value="1">是</option>
                                                <option value="0" selected>否</option>
                                            </select>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">是:允许转为私有运价,否:不允许转为私有运价</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">退改费用(百分比)</label>
                                        <div class="layui-input-inline text-input">
                                            <input type="number" class="layui-input" name="refundChangePercentage" lay-verify="refund_change" min="0" max="60" >
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">当"是否转私有"为是时,"退改费用必填",0-60的正整数,不需录%符号</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>是否适用于小团价</label>
                                        <div class="layui-input-inline">
                                            <select name="smallGroup">
                                                <option value="1">是</option>
                                                <option value="0" selected>否</option>
                                                <option value="-1">不限</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>是否适用文件公布运价</label>
                                        <div class="layui-input-inline">
                                            <select name="publicFreight">
                                                <option value="1">是</option>
                                                <option value="0" selected>否</option>
                                                <option value="-1">不限</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label"><span class="reqMark">*</span>可适用运价来源</label>
                                        <div class="layui-input-inline">
                                            <select name="freightOrigin">
                                                <option value="0" selected>公布运价</option>
                                                <option value="1">私有运价</option>
                                                <option value="-1">不限</option>
                                            </select>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">公布运价:仅适用于公布运价,私有运价:仅适用于私有运价,不限:公布/私有运价均适用</div>
                                    </div>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">供应商</label>
                                        <div class="layui-input-inline">
                                            <div class="layui-input-inline">
                                                <select name="supplierId" lay-verify="supplier" >
                                                    <option value="">请选择</option>
                                                    <c:forEach items="${channelList}" var="item">
                                                        <option value="${item.id}">${item.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="layui-form-mid layui-word-aux">用于区分该政策属于哪一个供应商</div>
                                    </div>
                                    <c:if test="${not empty id }">
                                        <div class="layui-form-item" >
                                            <label class="layui-form-label">创建人</label>
                                            <div class="layui-form-mid layui-word-aux"><span id="createUser"></span></div>
                                            <label class="layui-form-label">最后修改人</label>
                                            <div class="layui-form-mid layui-word-aux"><span id="lastUpdateUser"></span></div>
                                            <label class="layui-form-label">最后修改时间</label>
                                            <div class="layui-form-mid layui-word-aux"><span id="lastUpdateTime"></span></div>
                                        </div>
                                    </c:if>
                                    <div class="layui-form-item">
                                        <div class="layui-input-block">
                                            <button class="layui-btn" lay-submit="" lay-filter="policyForm">提交</button>
                                            <button type="reset" class="layui-btn layui-btn-primary">清空</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="resource/plugins/json-utils/jquery.serializejson.js"></script>
<script src="webpage/flight/international/policy/policy.js"></script>
<script>
  $(function () {
      policy.edit.init();
  })
</script>