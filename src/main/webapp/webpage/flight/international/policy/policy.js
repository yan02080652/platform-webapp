var tableObj;
var policy = {
    url: {
        index: function () {
            return 'flight/international/policy';
        },
        list: function () {
            return 'flight/international/policy/list';
        },
        delete: function () {
            return 'flight/international/policy/delete';
        },
        batchUpdate: function () {
            return 'flight/international/policy/batchUpdate';
        },
        save: function () {
            return 'flight/international/policy/save';
        },
        detail: function (id) {
            return 'flight/international/policy/detail?id=' + id;
        },
        edit: function (id) {
            if (id) {
                return 'flight/international/policy/edit?id=' + id;
            } else {
                return 'flight/international/policy/edit';
            }
        },
        export: function (params) {
            return "flight/international/policy/export?" + params;
        },
        uploadPage: function () {
            return "flight/international/policy/uploadPage";
        },
        upload: function () {
            return "flight/international/policy/upload";
        },
    },
    list: {
        init: function () {
            layui.use(['table', 'form'], function () {
                var table = layui.table;
                tableObj = table.render({
                    elem: '#policyTable',
                    url: policy.url.list(),
                    page: true,//开启分页
                    toolbar: '#headTool', //开启工具栏
                    defaultToolbar: ['filter'],//工具类右侧按钮自定义
                    limits: [10, 30, 50, 100, 500, 1000],
                    limit: 10,
                    //其他的查询参数
                    where: $('#searchForm').serializeJSON(),
                    request: {
                        pageName: 'pageIndex',
                        limitName: 'pageSize'
                    },
                    cols: [[ //表头
                        {type: 'checkbox', fixed: 'left'}
                        // ,{type: 'numbers', fixed: 'left',title:'序号'}
                        , {
                            field: 'airRangeType', title: '行程类型', align: 'center', width: 90, templet: function (d) {
                                return policy.list.airRangeType_templet(d.airRangeType);
                            }
                        }
                        , {field: 'billingCarrier', title: '开票航司', align: 'center', width: 90}
                        , {field: 'departure', title: '出发地', align: 'center', width: 90}
                        , {field: 'destination', title: '目的地', align: 'center', width: 90}
                        , {field: 'rebate', title: '返点', align: 'center', width: 90}
                        , {field: 'keepMoney', title: '留钱', align: 'center', width: 90}
                        , {
                            field: 'transfer', title: '是否适用中转', align: 'center', width: 120, templet: function (d) {
                                return policy.list.three_scene_templet(d.transfer);
                            }
                        }
                        , {field: 'transitAirport', title: '指定中转点', align: 'center', width: 110}
                        , {
                            field: 'intermodal', title: '是否适用联运', align: 'center', width: 120, templet: function (d) {
                                return policy.list.boolean_templet(d.intermodal);
                            }
                        }
                        , {
                            field: 'gap', title: '是否允许缺口', align: 'center', width: 120, templet: function (d) {
                                return policy.list.three_scene_templet(d.gap);
                            }
                        }
                        , {
                            field: 'transferPrivate',
                            title: '是否转私有',
                            align: 'center',
                            width: 120,
                            templet: function (d) {
                                return policy.list.boolean_templet(d.transferPrivate);
                            }
                        }
                        , {field: 'refundChangePercentage', title: '退改费', align: 'center', width: 90}
                        , {field: 'externalNum', title: '外部编码', align: 'center', width: 90}
                        , {fixed: 'right', width: 115, align: 'center', toolbar: '#rowTool', title: "操作"}
                    ]],
                    response: {
                        statusName: 'statusCode',
                        statusCode: 0,
                        msgName: 'msg',
                        countName: 'count',
                        dataName: 'data'
                    },
                });

                //监听头工具栏事件
                table.on('toolbar(policy)', function (obj) {
                    var checkStatus = table.checkStatus(obj.config.id)
                        , data = checkStatus.data; //获取选中的数据
                    switch (obj.event) {
                        case 'add':
                            window.location.href = policy.url.edit();
                            break;
                        case 'hangUp':
                            policy.list.handler(data, 'batchUpdate', 'HANG')
                            break;
                        case 'hangOut':
                            policy.list.handler(data, 'batchUpdate', 'EFFECTIVE')
                            break;
                        case 'batchDel':
                            policy.list.handler(data, 'batchDelete')
                            break;
                        case 'export':
                            window.location.href = policy.url.export($('#searchForm').serialize());
                            break;
                        case 'upload':
                            window.location.href = policy.url.uploadPage();
                            break;
                    }
                    ;
                });
                //监听行工具事件
                table.on('tool(policy)', function (obj) { //注：tool 是工具条事件名，operation 是 table 原始容器的属性 lay-filter="对应的值"
                    var data = obj.data //获得当前行数据
                        , layEvent = obj.event; //获得 lay-event 对应的值
                    if (layEvent === 'detail') {
                        window.location.href = policy.url.edit(data.id);
                    } else if (layEvent === 'del') {
                        policy.list.batchDelete(data.id);
                    }
                });
            });
        },
        boolean_templet: function (sign) {
            if (sign) {
                return '<div><i class="iconfont icon-gou" style="color: #009688"></i></div>';
            } else {
                return '<div><i class="iconfont icon-guanbi" style="color:#FF5722"></i></div>';
            }
        },
        airRangeType_templet: function (type) {
            if (type == 'OW') {
                return "单程";
            } else if (type == 'RT') {
                return "往返";
            } else if(type=='OWRT'){
                return "单程/往返";
            }else{
                return "";
            }
        },
        three_scene_templet: function (sign) {
            switch (sign) {
                case "1":
                    return '<div><i class="iconfont icon-gou" style="color: #009688"></i></div>';
                case "0":
                    return '<div><i class="iconfont icon-guanbi" style="color:#FF5722"></i></div>';
                case "-1":
                    return '<div><span>不限</span></div>';
                default:return "";
            }
        },
        batchDelete: function (id) {
            layer.confirm('确认删除所选数据吗?', function (index) {
                layer.close(index);
                var loadingIndex = layer.close();
                $.ajax({
                    type: "POST",
                    url: policy.url.delete(),
                    data: {id: id},
                    success: function (data) {
                        layer.close(loadingIndex);
                        if (data.code == '1') {
                            policy.list.reload();
                        } else {
                            layer.msg("删除失败!")
                        }
                    }
                });
            });
        },
        reload: function () {
            tableObj.reload({
                where: $('#searchForm').serializeJSON()
            });
        },
        resetForm: function () {
            $("#searchForm")[0].reset();
            policy.list.reload();
        },
        batchUpdate: function (id, status) {
            layer.confirm('确认修改所选数据吗?', function (index) {
                layer.close(index);
                var loadingIndex = layer.close();
                $.ajax({
                    type: "POST",
                    url: policy.url.batchUpdate(),
                    data: {'id': id, 'status': status},
                    success: function (data) {
                        layer.close(loadingIndex);
                        if (data.code == '1') {
                            policy.list.reload();
                        } else {
                            layer.msg("修改失败!")
                        }
                    }
                });
            });
        },
        handler: function (obj, event, status) {
            var id = [];
            if (obj.length === 0) {
                layer.msg('请选择数据');
                return;
            } else {
                $.each(obj, function (index, item) {
                    if (event == 'batchUpdate' && item.status == 'ABNORMAL') {
                        return;
                    }
                    id.push(item.id);
                })
            }
            switch (event) {
                case "batchUpdate":
                    policy.list.batchUpdate(id.toString(), status);
                    break;
                case "batchDelete":
                    policy.list.batchDelete(id.toString());
                    break;
            }
        }
    },
    edit: {
        init: function () {
            layui.use(['form'], function () {
                form = layui.form;
                //表单提交
                form.on('submit(policyForm)', function (data) {
                    var formData = policy.edit.formDataHandler(data.field);
                    var index = layer.load(0)
                    $.ajax({
                        url: policy.url.save(),
                        data: formData,
                        type: "post",
                        success: function (result) {
                            layer.close(index);
                            if (result.code == '1') {
                                window.location.href = policy.url.index();
                            } else {
                                $("input[name=" + result.data.field + "]").focus();
                                layer.msg(result.data.msg, {icon: 5, anim: 6});
                            }
                        }
                    })
                    return false;
                });

                var policyId = $("#policyId").val();
                if (policyId) {
                    $.get(policy.url.detail(policyId), function (result) {
                        if (result.code == '1') {
                            var data = result.data.policy;
                            //数据回填
                            form.val('policy', {
                                "externalNum": data.externalNum,
                                "fileNum": data.fileNum,
                                "combinationFileNum": data.combinationFileNum,
                                "airRangeType": data.airRangeType,
                                "billingCarrier": data.billingCarrier,
                                "departure": data.departure,
                                "destination": data.destination,
                                "excludeDeparture": data.excludeDeparture,
                                "excludeDestination": data.excludeDestination,
                                "cabinCodes": data.cabinCodes,
                                "intermodal": data.intermodal ? "1" : "0",
                                "intermodalCarrierCodes": data.intermodalCarrierCodes,
                                "excludeIntermodalCarrierCodes": data.excludeIntermodalCarrierCodes,
                                "transfer": data.transfer,
                                "transitAirport": data.transitAirport,
                                "shareAirline": data.shareAirline,
                                "gap": data.gap,
                                "freightType": data.freightType,
                                "reimbursementVoucher": data.reimbursementVoucher,
                                "departureDate": data.departureDate,
                                "destinationDate": data.destinationDate,
                                "saleDate": data.saleDate,
                                "rebate": data.rebate,
                                "keepMoney": data.keepMoney,
                                "remark": data.remark,
                                "operatingHours": data.operatingHours,
                                "passengerNationality": data.passengerNationality,
                                "excludePassengerNationality": data.excludePassengerNationality,
                                "commissionCalculationMethod": data.commissionCalculationMethod,
                                "timeLimitTicketing": data.timeLimitTicketing,
                                "ticketingCity": data.ticketingCity,
                                "transferPrivate": data.transferPrivate ? "1" : "0",
                                "refundChangePercentage": data.refundChangePercentage,
                                "smallGroup": data.smallGroup,
                                "publicFreight": data.publicFreight,
                                "freightOrigin": data.freightOrigin,
                                "supplierId": data.supplierId,
                            })
                            if (result.data.createUser){
                                $("#createUser").text(result.data.createUser.fullname);
                            }
                            if (result.data.lastUpdateUser){
                                $("#lastUpdateUser").text(result.data.lastUpdateUser.fullname);
                            }
                            $("#lastUpdateTime").text(policy.edit.dateFtt("yyyy-MM-dd hh:mm:ss", new Date(data.lastUpdateTime)));

                            var passengerType = data.passengerType;
                            var passengerQualification = data.passengerQualification;

                            $("#passengerTypeDiv input").each(function (i, dom) {
                                if (passengerType.indexOf($(dom).val()) == -1){
                                    $(dom).prop("checked", false);
                                }else{
                                    $(dom).prop("checked", true);
                                }
                            })
                            $("#passengerQualification input").each(function (i, dom) {
                                if (passengerQualification.indexOf($(dom).val()) == -1){
                                    $(dom).prop("checked", false);
                                }else{
                                    $(dom).prop("checked", true);
                                }
                            })
                            form.render();
                        }
                    })
                }

                //自定义验证规则
                form.verify({
                    combination: function (value) {
                        var fileName = $("#fileNum").val();
                        if (fileName && !value) {
                            return "文件编号存在时，可组文件编号不能为空"
                        }
                    },
                    decimal: function (value) {
                        var arr = value.split(".");
                        if (parseInt(arr[0])<0 || parseInt(arr[0])>99){
                            return "超出有效值区间";
                        }
                        if (arr.length == 2) {
                            if (arr[1].length > 2) {
                                return "小数点后最多2位";
                            }
                        }
                    },
                    refund_change: function (value) {
                        var transferPrivate = $("#transferPrivate").val();
                        if (transferPrivate == '1') {
                            if (!value) {
                                return '当"是否转私有"为是时,"退改费用"必填'
                            }
                        }
                        if (value < 0 || value > 60) {
                            return "必须输入0-60间的整数";
                        }
                    },
                    integer:function(value){
                        return policy.edit.validationHandler(/^-?\d+$/, value, "必须为整数");
                    },
                    carrier: function (value) {
                        return policy.edit.validationHandler(/^[0-9a-zA-Z]{2}$/, value, "航司格式不正确");
                    },
                    multi_carrier: function (value) {
                        return policy.edit.validationHandler(/^([0-9a-zA-Z]{2})(\/[0-9a-zA-Z]{2}){0,19}$/, value, "航司格式不正确");
                    },
                    region: function (value) {
                        return policy.edit.validationHandler(/^([a-zA-Z0-9]{2,4})(\/[a-zA-Z0-9]{2,4})*$/, value, "地点格式不正确");
                    },
                    cabin: function (value) {
                        return policy.edit.validationHandler(/^([a-zA-Z0-9]{1,2})(\/[a-zA-Z0-9]{1,2})*$/, value, '适用舱位格式不正确')
                    },
                    city: function (value) {
                        return policy.edit.validationHandler(/^([a-zA-Z]{3})$/, value, '城市代码格式不正确')
                    },
                    multi_city: function (value) {
                        return policy.edit.validationHandler(/^([a-zA-Z]{3})(\/[a-zA-Z0-9]{3})*$/, value, '城市代码格式不正确')
                    },
                    data_range: function (value) {
                        return policy.edit.validationHandler(/^[0-9]{4}-[0-9]{2}-[0-9]{2}>[0-9]{4}-[0-9]{2}-[0-9]{2}$/, value, '时间格式不正确')
                    },
                    multi_data_range: function (value) {
                        return policy.edit.validationHandler(/^[0-9]{4}-[0-9]{2}-[0-9]{2}>[0-9]{4}-[0-9]{2}-[0-9]{2}(\/[0-9]{4}-[0-9]{2}-[0-9]{2}>[0-9]{4}-[0-9]{2}-[0-9]{2})*$/, value, '时间格式不正确')
                    },
                    nationality: function (value) {
                        return policy.edit.validationHandler(/^([a-zA-Z]{2})(\/[a-zA-Z]{2})*$/, value, '国籍格式不正确')
                    },
                    time_limit_ticketing: function (value) {
                        return policy.edit.validationHandler(/^([0-9]{1,3}-[0-9]{1,3}(,[0-9]{1,3}){2})(\/[0-9]{1,3}-[0-9]{1,3}(,[0-9]{1,3}){2})*$/, value, '出票时限格式不正确')
                    },
                });

                $("#rebate").blur(function () {
                    var value = $(this).val();
                    if (value < 0 || value > 99){
                        $(this).focus();
                        layer.msg("超出有效值区间", {icon: 5, anim: 6});
                        return false;
                    }

                    if (value > 30) {
                        layer.confirm("返点大于30，是否继续保存", {title: "提示"}, function (index) {
                            layer.close(index);
                        }, function (index) {
                            $("#rebate").val("");
                            layer.close(index)
                        })
                    }
                })
                $("#keepMoney").blur(function () {
                    var value = $(this).val();
                    if (value < -1000) {
                        layer.confirm("留钱小于-1000，是否继续保存", {title: "提示"}, function (index) {
                            layer.close(index);
                        }, function (index) {
                            $("#keepMoney").val("");
                            layer.close(index)
                        })
                    }
                })
            });
        },

        formDataHandler: function (obj) {
            var pt = "";
            var pq = "";
            //处理多选框
            $("#passengerTypeDiv input").each(function (i, dom) {
                if ($(dom).is(":checked")) {
                    pt += $(dom).val() + "/";
                }
            })
            obj.passengerType = pt;
            //处理多选框
            $("#passengerQualification input").each(function (i, dom) {
                if ($(dom).is(":checked")) {
                    pq += $(dom).val() + "/";
                }
            })
            obj.passengerQualification = pq;


            //删除空字符的字段，避免后台验证提示必填
            for (var i in obj) {
                var value = obj[i];
                if (value === '' || value === null || value === undefined) {
                    delete obj[i];
                }
            }

            return obj;
        },

        validationHandler: function (reg, value, msg) {
            if ($.trim(value)) {
                if (!reg.test(value)) {
                    return msg;
                }
            }
        },
        dateFtt: function (fmt, date) {
            var o = {
                "M+": date.getMonth() + 1,                 //月份
                "d+": date.getDate(),                    //日
                "h+": date.getHours(),                   //小时
                "m+": date.getMinutes(),                 //分
                "s+": date.getSeconds(),                 //秒
                "q+": Math.floor((date.getMonth() + 3) / 3), //季度
                "S": date.getMilliseconds()             //毫秒
            };
            if (/(y+)/.test(fmt))
                fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
            for (var k in o)
                if (new RegExp("(" + k + ")").test(fmt))
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            return fmt;
        }
    },
    upload: {
        init: function () {
            layui.use(['upload', 'table'], function () {
                var upload = layui.upload;
                var table = layui.table;
                upload.render({
                    elem: '#selectFile',
                    url: policy.url.upload(),
                    auto: false,
                    bindAction: '#upload',
                    accept: 'file',
                    exts: 'xlsx|xls',
                    field: 'excelFile',
                    before: function (obj) {
                        layer.load();
                    },
                    done: function (res) {
                        layer.closeAll('loading');
                        if (res.code == '0') {
                            layer.msg("导入失败", {icon: 5})
                        } else {
                            var data = res.data;
                            if (data != null && data.length > 0) {
                                table.render({
                                    elem: '#errorTable',
                                    page: true,//开启分页
                                    limits: [10, 30, 50, 100, 500, 1000],
                                    limit: 10,
                                    cols: [[
                                        {type: 'numbers', title: '序号'},
                                        {
                                            field: 'msg', title: '详情', templet: function (d) {
                                                return policy.upload.msg_template(d);
                                            }
                                        }
                                    ]],
                                    data: data,
                                });
                            } else {
                                $("#errorTable").next().hide();
                                layer.msg("导入成功", {icon: 1})
                            }
                        }
                    },
                    error: function () {
                        layer.closeAll('loading');
                        layer.msg("导入失败", {icon: 5})
                    }
                });
            });
        },
        msg_template: function (data) {
            return "Excel第" + data.seq + "行,错误信息" + data.msg;
        }
    },
}