<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .layui-form-label {
        box-sizing: content-box;
    }
</style>
<div >
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>国际机票政策
            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div>
                                    <form class="layui-form" id="searchForm">
                                        <div class="layui-form-item">
                                            <label class="layui-form-label">TMC</label>
                                            <div class="layui-input-inline">
                                                <input type="text" class="layui-input" value="${partner.name}" readonly>
                                            </div>
                                            <label class="layui-form-label">行程类型</label>
                                            <div class="layui-input-inline">
                                                <select name="tripType">
                                                    <option value="" selected>全部</option>
                                                    <option value="OW" >单程</option>
                                                    <option value="RT">往返</option>
                                                    <option value="OW/RT">单程/往返</option>
                                                </select>
                                            </div>
                                            <label class="layui-form-label">有效性</label>
                                            <div class="layui-input-inline">
                                                <select name="status">
                                                    <option value="" selected>全部</option>
                                                    <option value="EFFECTIVE">有效</option>
                                                    <option value="HANG">挂起</option>
                                                    <option value="EXPIRED">已过期</option>
                                                    <option value="ABNORMAL">异常</option>
                                                </select>
                                            </div>
                                            <label class="layui-form-label">公布转私有</label>
                                            <div class="layui-input-inline">
                                                <select name="transferPrivate">
                                                    <option value="" selected="">全部</option>
                                                    <option value="1">是</option>
                                                    <option value="0">否</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                                <label class="layui-form-label">出发地</label>
                                                <div class="layui-input-inline">
                                                    <input type="text" name="departure"  class="layui-input">
                                            </div>
                                                <label class="layui-form-label">目的地</label>
                                                <div class="layui-input-inline">
                                                    <input type="text" name="destination" class="layui-input">
                                                </div>
                                                <label class="layui-form-label">文件编号</label>
                                                <div class="layui-input-inline">
                                                    <input type="text" name="fileNum" class="layui-input">
                                                </div>
                                                <label class="layui-form-label">外部编号</label>
                                                <div class="layui-input-inline">
                                                    <input type="text" name="externalNum" class="layui-input">
                                            </div>
                                        </div>
                                        <div class="layui-form-item">
                                                <label class="layui-form-label">开票航司</label>
                                                <div class="layui-input-inline">
                                                    <input type="text" name="carrier" class="layui-input">
                                                </div>
                                                <label class="layui-form-label">适用舱位</label>
                                                <div class="layui-input-inline">
                                                    <input type="text" name="cabin" class="layui-input">
                                                </div>
                                                <label class="layui-form-label"></label>
                                                <div class="layui-input-inline">
                                                    <button type="button" class="layui-btn"  onclick="policy.list.reload()">查询</button>
                                                    <button type="button" class="layui-btn"  onclick="policy.list.resetForm()">重置</button>
                                                </div>
                                        </div>
                                    </form>
                                </div>
                                <div>
                                    <table id="policyTable" lay-filter="policy"></table>
                                    <script type="text/html" id="rowTool">
                                        <a class="layui-btn layui-btn-xs" lay-event="detail">编辑</a>
                                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/html" id="headTool">
    <div style="float: left">
        <a class="layui-btn layui-btn-xs" lay-event="hangUp" >挂起选中</a>
        <a class="layui-btn layui-btn-xs" lay-event="hangOut">解挂选中</a>
        <a class="layui-btn layui-btn-xs" lay-event="batchDel" >删除选中</a>
    </div>
    <div style="float: right">
        <a class="layui-btn layui-btn-xs" lay-event="add">新增</a>
        <a class="layui-btn layui-btn-xs" lay-event="upload">导入</a>
        <a class="layui-btn layui-btn-xs" lay-event="export">导出查询结果</a>
    </div>
</script>
<script src="resource/plugins/json-utils/jquery.serializejson.js"></script>
<script src="webpage/flight/international/policy/policy.js"></script>
<script>
   $(function () {
       policy.list.init();
   })
</script>