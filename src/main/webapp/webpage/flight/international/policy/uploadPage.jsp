<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .layui-form-label {
        box-sizing: content-box;
    }
</style>
<div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>国际机票政策导入
                <a href="/flight/international/policy">返回</a>
            </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="layui-upload">
                                    <label class="layui-form-label">选择文件:</label>
                                    <button type="button" class="layui-btn" id="selectFile">选择文件
                                    </button>
                                    <button type="button" class="layui-btn" id="upload">导入</button>
                                </div>
                                <div class="layui-form-mid layui-word-aux"><a href="/flight/international/policy/excelTemplateExport">模板下载</a></div>
                                <div style="min-height:450px; margin-top: 50px">
                                    <table id="errorTable" lay-filter="errorTable"></table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="resource/plugins/json-utils/jquery.serializejson.js"></script>
<script src="webpage/flight/international/policy/policy.js"></script>
<script>
    $(function () {
        policy.upload.init();
    })
</script>