<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" language="java"%> 
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/carOrderDetail.css?version=${globalVersion}">

<div class="main">
	<div class="oContent fix">
		<div class="oContentL">
			<h2 class="oTitle">订单详情</h2>
			<div class="fix oNumber">
				<div class="l">
					<p>
						订单号：<span>${order.order.id }</span>
					</p>
				</div>
			</div>
			<div class="oState">
				<h3 class="oStateTitle">${order.order.orderShowStatus.message }</h3>
				<div class="fix mb20">
					<div class="l"></div>
				</div>
				<div class="oProgress fix">
					<div class="oProgressItem oProgressItemActive">
						<p>提交订单</p>
						<span><fmt:formatDate value="${order.order.createDate }" pattern="yyyy年MM月dd日 HH:mm:ss"/></span>
					</div>
					<div class="oProgressItem <c:if test="${order.order.orderShowStatus eq 'CONFIRM_ORDER' or order.order.orderShowStatus eq 'DRIVER_ARRIVCE' or order.order.orderShowStatus eq 'DRIVING' or order.order.orderShowStatus eq 'DRIVE_OVER' or order.order.orderShowStatus eq 'WAIT_PAY'}">oProgressItemActive</c:if>">
						<p>出票成功</p>
						<span><fmt:formatDate value="${order.order.receiveTime}" pattern="yyyy年MM月dd日 HH:mm:ss"/></span>
					</div>
					<div class="oProgressItem <c:if test="${order.order.paymentStatus eq 'PAYMENT_SUCCESS' }">oProgressItemActive</c:if> ">
						<p>支付订单</p>
						<c:forEach items="${operations }" var="bean" varStatus="status">
							<c:if test="${bean.type eq 'USR_PAY' }">
								<span><fmt:formatDate value="${bean.operationDate }" pattern="yyyy年MM月dd日 HH:mm:ss"/></span>
							</c:if>
						</c:forEach>
					</div>
					<%--<div class="oProgressItem <c:if test="${order.orderStatus eq 'CHECKOUT' or order.orderStatus eq 'CHECKIN' }">oProgressItemActive</c:if>" >--%>
						<%--<p>预订完成</p>--%>
					<%--</div>--%>
				</div>
			</div>
			<div class="oItem">
                <h3 class="oItemTitle">差旅信息</h3>
                <div class="oItemLi">
                    <p><span class="oItemLiLabel">差旅计划</span><span>{order.order.travelPlanNo}</span></p>
                    <p><span class="oItemLiLabel">出行类别</span><span>${order.order.travelName }</span></p>
                    <p><span class="oItemLiLabel">费用类型</span><span>${order.order.costUnitName }</span></p>
					<p><span class="oItemLiLabel">出差事由</span><span></span></p>
                </div>
            </div>
			<div class="oItem">
			<h3 class="oItemTitle">付款信息</h3>
				<c:if test="${not empty payItem}">
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">付款方式</span><span>${payItem.payMethod }</span>
					</p>
					<p>
						<span class="oItemLiLabel">交易编号</span><span>${payItem.tradeId }</span>
					</p>
				</div>
				</c:if>
			</div>
			<div class="oItem">
				<h3 class="oItemTitle">联系人</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">姓名</span><span>${order.order.contactorName }</span>
					</p>
					<p>
						<span class="oItemLiLabel">手机</span><span>${order.order.contactorMobile }</span>
					</p>
					<p>
						<span class="oItemLiLabel">邮箱</span><span>${order.order.contactorEmail }</span>
					</p>
				</div>
			</div>
		</div>
		<!-- 右侧内容 -->
		<div class="oContentR">
			<div class="rightTitle">
				<p>
					<span><fmt:formatDate value="${order.order.createDate}" pattern="MM-dd E HH:mm" /></span>
					<span>预计时长${order.trip.actualTimeLength}分钟 , 行驶${order.trip.drivingKilometres}公里</span>
				</p>
			</div>
			<div class="rightTrip">
				<ul>
					<li>
						<p><span style="color: #46a1bb;font-size: 9px;">[出发地]</span>&nbsp;&nbsp;${order.trip.startName}</p>
						<p><fmt:formatDate value="${order.trip.carUseTime}" pattern="HH:mm" /> ${order.trip.startAddress}</p>
					</li>
					<li>
						<p><span style="color: #46a1bb;font-size: 9px;">[目的地]</span>&nbsp;&nbsp;${order.trip.endName}</p>
						<p><fmt:formatDate value="${order.trip.carEndTime}" pattern="HH:mm" /> ${order.trip.endAddress}</p>
					</li>
				</ul>
			</div>

			<c:if test="${not empty order.order.receiveTime }">
				<div class="rightCarInfo">
					<p>用车信息</p>
					<p style="padding: 10px;">
					<div style="display: flex;align-items: center;">
						<div style="margin-right: 10px;padding: 0 10px;">
							<img src="${empty order.trip.driverPhoto ? 'resource/image/car.jpg' : order.trip.driverPhoto}" style="width: 75px;height: 75px; border-radius: 45px;">
						</div>
						<div class="info-list" style="line-height: 25px">
							<span style="padding-left:10px">${order.trip.driverName}  ${order.trip.driverTel}</span>
							<br/>
							<span style="padding-left:25px">专车   ${order.trip.vehicleNo}</span>
							<br/>
							<span style="padding-left:25px">${order.trip.vehicleColor}   ${order.trip.vehicleBrand}</span>
							<br/>
							<span style="padding-left:10px">预估价格  ¥${ order.order.estimatePrice}</span>
						</div>
					</div>

					</p>
				</div>
			</c:if>
			<c:if test="${not empty order.feeDetail }">
				<div class="rightFee">
					<p>费用明细</p>
					<ul>
						<c:forEach items="${order.feeDetail}" var="bean" varStatus="status">
							<li>
								<span>${bean.type}</span>
								<span>--------------------------</span>
								<span class="price">¥${bean.amount}</span>
							</li>
						</c:forEach>
					</ul>

				</div>
			</c:if>
			<div class="totalAmount ">
				<p style="display: flex; justify-content: space-between">
					<span>总价</span>
					<span class="price"><c:if test="${empty order.order.totalAmount }">约</c:if> ¥${empty order.order.totalAmount ? order.order.estimatePrice : order.order.totalAmount}</span>
				</p>
			</div>
		</div>
	</div>
</div>
<script src="${baseRes }/js/order/carOrderDetail.js?version=${globalVersion}"></script>
