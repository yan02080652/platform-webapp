<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<style>
	.dash_tr td{
		border-top:none !important;
	}
</style>

<div class="result">
	<table class="table orderTable">
		<thead>
			<tr>
				<th class="tipL">订单号</th>
				<th align="center">订单情况</th>
				<th>预订人</th>
				<th>下单时间</th>
				<th>所属客户</th>
				<th>费用归属</th>
				<th>大客户协议编码</th>
				<th>客户结算价</th>
				<th>支付方式</th>
				<th>所属供应商</th>
				<th>供应商结算价</th>
				<th>状态</th>
				<th>结算状态</th>
				<th class="tipR">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="flightOrder">
				<tr <c:if test="${!flightOrder.topLine}">class="dash_tr"</c:if>>
					<td class=" tipL">
						<%--<a href='javascript:;' onclick="openFlightDetail('order/flightDetail/${flightOrder.orderId }')">--%>
							${flightOrder.orderId}
						<%--</a>--%>
					</td>
					<td style="text-align:center;max-width:200px;">
						<span>${flightOrder.summary}</span>
					</td>
					<td style="text-align:center;">${flightOrder.userName }</td>
					<td style="text-align:center;width:110px;"><fmt:formatDate value="${flightOrder.createDate}" pattern="yyyy-MM-dd HH:mm" /></td>
					<td style="text-align:center;max-width: 140px;">${flightOrder.partnerName }</td>
					<td style="text-align:center;max-width: 140px;">${flightOrder.costCenterName }</td>
					<td style="text-align:center;">${flightOrder.customerCode }</td>
					<td style="text-align:center;">${flightOrder.totalAmount }</td>
					<td style="text-align:center;">${flightOrder.paymentMethod}</td>
					<td style="text-align:center;">${flightOrder.supName}</td>
					<td style="text-align:center;">${flightOrder.supSettleAmount }</td>
					<td style="text-align:center;">${flightOrder.orderState }</td>
					<td style="text-align:center;">
					    <c:if test="${flightOrder.billId != null}">已结算</c:if>
					    <c:if test="${flightOrder.billId == null}">未结算</c:if>
					</td>
					<td class="lastTd" style="text-align:center;">
						 <c:if test="${flightOrder.totalAmount > 0}">
							 <a class="drop_li" style="color: #23527c;" href='javascript:;' onclick="openFlightDetail('order/flightDetail/${flightOrder.orderId }')">查看</a>
						 </c:if>
                         <c:if test="${flightOrder.totalAmount < 0}">
							 <a class="danhao" style="color: #23527c;" href='javascript:;' onclick="openRefundDetail('order/refundFlightOrderDetail/${flightOrder.orderId }')">查看</a>
						 </c:if>
                    </td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<div>
	<form action="pay-channel.html" method="get" id="payForm">
		<input type="hidden"  name="orderIds"  >
	</form>
</div>
<div class="pagin" style="margin-bottom: 15px">
<jsp:include page="../common/pagination_ajax.jsp?callback=reloadFlightOrder"></jsp:include>
</div>
