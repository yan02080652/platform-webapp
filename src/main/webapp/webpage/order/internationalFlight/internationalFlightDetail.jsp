<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" autoFlush="false" buffer="600kb" %>
<link rel="stylesheet" href="/resource/css/order/ainteflight.css?version=${globalVersion}"/>
<style>
    /*页面验证信息提示*/
    label.promptError {
        line-height: 1;
        position: absolute;
        display: block;
        padding: 6px 12px;
        color: white;
        font-size: 12px;
        left: 8px;
        top: -24px;
        background: #e53935;
    }

    label.promptError:after {
        position: absolute;
        top: 18px;
        left: 4px;
    }

    #price input:read-only {
        cursor: not-allowed;
        background-color: #dddddd;
    }

    .newBaggage input[type='radio'] {
        width: 15px;
        margin-top: 13px;
        margin-left: 5px;

    }

    .newBaggage select {
        margin-top: 11px;
    }

    .newBaggage label {
        font-weight: 500;
        margin-right: 3px;
    }

    .newBaggage span {
        color: red;
    }

    .m-table-container {
        width: 100%;
        border: 1px #ddd solid;
        border-bottom: 0;
    }

    .m-table-container thead tr th {
        background: #999;
        padding: 5px 12px;
        color: #fff;
        border-bottom: 1px #ddd solid;
        border-right: 1px #ddd solid;
    }

    .m-table-container thead tr th:last-child {
        border-right: 0;
    }

    .m-table-container tbody tr td {
        padding: 5px 12px;
        /*color: #fff;*/
        border-bottom: 1px #ddd solid;
        border-right: 1px #ddd solid;
    }

    .m-table-container tbody tr td input {
        width: 100%;
        border-radius: 2px;
        padding: 5px 12px;
        box-sizing: border-box;
        height: 28px;
    }

    .m-table-container tbody tr td:last-child {
        border-right: 0;
    }

    .m-table-container input[type='checkbox'] {
        width: 10px;
    }

    select:disabled {
        background: #8D8D8D;
    }
</style>

<body style="background: #f1f6fa;">
<div class="flightTravel">
    <form id="interFlight">
        <div class="ftInfo">
            <input type="hidden" id="orderId" value="${order.id}">
            <!--差旅信息-->
            <div class="travelInfo">
                <span class="title">差旅信息</span>
                <div class="tInfo">
                    <div class="infoDiv">
                        <label>企业名称</label><span>*</span>
                        <input value="${order.cName}" name="cName" style="width: 160px" readonly/>
                        <input value="${order.cId}" name="cId" type="hidden">
                    </div>
                    <div class="infoDiv">
                        <label>出行类别</label>
                        <select name="travelType">
                            <c:forEach items="${travelTypes}" var="travelType">
                                <option value="${travelType.itemCode }"
                                        <c:if test="${travelType.itemCode eq order.passengers[0].travelCode}">selected</c:if> >${travelType.itemTxt }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="item_pull infoDiv">
                        <label>费用归属</label>
                        <div class="pullTitle">
                            <label>

                                <input type="text" class="item_div" value="${centerName}"
                                       code="${order.passengers[0].costUnitCode}" orgId="${order.passengers[0].orgId}"
                                       corpId="${order.passengers[0].corporationId}" style="width: 140px"
                                       placeholder="请选择费用归属" readonly/>
                            </label>
                            <input type="hidden" id="costUnitCode" value="${order.passengers[0].costUnitCode}">
                            <input type="hidden" id="orgId" value="${order.passengers[0].orgId}">
                            <input type="hidden" id="corpId" value="${order.passengers[0].corporationId}">
                        </div>
                        <div class="ul_b">
                            <ul class="item_ul">
                                <c:forEach items="${costCenter}" var="cost">
                                    <li code="${cost.id}" orgId="${cost.orgId}"
                                        corpId="${cost.corpId}">${cost.name}</li>
                                </c:forEach>
                            </ul>
                            <p class="otherExpense">使用其他成本核算单元</p>
                        </div>
                    </div>
                </div>
            </div>

            <!--预订人信息-->
            <div class="reservationInfo">
                <span class="title">预订人</span>
                <label style="padding-top: 20px;float: right;color: #acacac;font-size: 50%">手机号和邮箱至少填写一项</label>
                <div class="tInfo">
                    <div class="infoDiv nm">
                        <label>姓名</label><span>*</span>
                        <input type="text" placeholder="请输入姓名" name="userName" value="${order.userName}" readonly/>
                        <input type="hidden" value="${order.userId}" name="userId">
                    </div>
                    <div class="infoDiv tel" style="position: relative">
                        <label>手机号</label>
                        <input type="tel" placeholder="请输入手机号" name="userMobile" value="${order.userMobile}"/>
                    </div>
                    <div class="infoDiv email" style="position: relative">
                        <label>E-mail（选填）</label>
                        <input type="text" placeholder="请输入邮箱" name="userEmail" value="${order.userEmail}"/>
                    </div>
                </div>
            </div>
        </div>

        <!--行程-->
        <div class="ftInfo ftTravel">
            <div class="travelTitle clearfix">
                <span class="title">行程</span>
                <div class="trvType">行程类型:
                    <input type="radio" name="tripType" checked value="OW"
                           <c:if test="${order.tripType eq 'OW'}">checked</c:if>/>单程
                    <input type="radio" name="tripType" value="RT"
                           <c:if test="${order.tripType eq 'RT'}">checked</c:if>/>往返

                    <a class="addTrvbtn">添加行程</a>
                </div>
            </div>
            <div class="process clearfix defaultDiv">
                <div class="process_title fl">第一程</div>
                <input type="hidden" name="digital">
                <div class="proInfo fl">
                    <div class="clearX"><a class="clearcon">清空</a> <i class="iconfont icon-close"></i></div>
                    <div class="flightNo">
                        <div class="fnDiv">
                            <label>航班号</label><span>*</span><br>
                            <input type="text" placeholder="编号" name="flightNo" class="flightNo"
                                   onblur="toUpper(this)"/>
                        </div>
                        <div class="fnDiv" style="width:12%;">
                            <input class="wd share" type="checkbox" name="share"/><label
                                style="margin-left: 16px">共享航班</label><span>*</span><br/>
                            <input type="text" disabled="disabled" style="width: 93px" name="realFlightNo"
                                   placeholder="实际承运航班号" onblur="toUpper(this)" class="wd_input realFlightNo"/>
                        </div>
                        <div class="fnDiv">
                            <label>舱位编码</label><span>*</span><br/>
                            <input type="text" placeholder="编码" name="cabinCode" class="cabinCode"
                                   onblur="toUpper(this)"/>
                        </div>
                        <div class="fnDiv">
                            <label>起飞日期</label><span>*</span><br/>
                            <input type="text" placeholder="YYYYMMDD" name="fromDate" class="fromDate" readonly/>
                        </div>
                        <div class="fnDiv">
                            <label>到达日期</label><span>*</span><br/>
                            <input type="text" placeholder="YYYYMMDD" name="toDate" class="toDate" readonly/>
                        </div>
                        <div class="fnDiv">
                            <label>出发机场</label><span>*</span><br/>
                            <input type="text" placeholder="三字码" name="fromAirportCode" onblur="toUpper(this)"
                                   class="fromAirportCode"/>
                        </div>
                        <div class="fnDiv">
                            <label>出发航站楼</label><br/>
                            <input type="text" placeholder="编码" name="fromTerminal" onblur="toUpper(this)"
                                   class="fromTerminal"/>
                        </div>
                        <div class="fnDiv">
                            <label>到达机场</label><span>*</span><br/>
                            <input type="text" placeholder="三字码" name="toAirportCode" onblur="toUpper(this)"
                                   class="toAirportCode"/>
                        </div>
                        <div class="fnDiv">
                            <label>到达航站楼</label><br/>
                            <input type="text" placeholder="编码" name="toTerminal" onblur="toUpper(this)"
                                   class="toTerminal"/>
                        </div>
                        <div class="fnDiv">
                            <label>起飞时间</label><span>*</span><br/>
                            <input type="text" placeholder="HHMM" name="fromTime" class="fromTime"/>

                        </div>
                        <div class="fnDiv">
                            <label>到达时间</label><span>*</span><br/>
                            <input type="text" placeholder="HHMM" name="toTime" class="toTime"/>

                        </div>
                        <div class="fnDiv">
                            <label>机型</label><br/>
                            <input type="text" placeholder="录入机型" name="aircraftType" class="aircraftType"/>
                        </div>
                    </div>
                    <div class="stopFlight">
                        <p class="fnDiv" style="text-indent: 16px; line-height: 20px;">
                            <input class="wd stopOver" type="checkbox" name="stopOver"/>经停航班
                        </p>
                        <div class="fnDiv" style="width: 10.7%">
                            <label>经停城市</label><span>*</span><br/>
                            <input type="text" style="width: 100px" name="stopCityName" placeholder="城市名称"
                                   disabled="disabled" class="wd_input stopCityName"/>
                        </div>
                        <div class="fnDiv">
                            <label>经停时长</label><span>*</span><br/>
                            <input type="text" placeholder="HHMM" name="stopTime" disabled="disabled"
                                   class="wd_input stopTime"/>
                        </div>
                        <%--<div class="fnDiv">--%>
                        <%--<label>行李规定</label><span>*</span><br/>--%>
                        <%--<input style="width: 743px;" type="text" name="baggageRule" class="baggageRule"--%>
                        <%--placeholder="输入行李规定"/>--%>
                        <%--</div>--%>
                        <div class="newBaggage" style="position: relative">
                            <label>免费托运行李</label><span>*</span><br/>

                            <input type="radio" name="baggageType" class="baggageType" value="1"><label>无</label>

                            <input type="radio" name="baggageType" class="baggageType" value="2">
                            <select name="piece" class="piece">

                                <c:forEach items="${pieces}" var="piece">
                                    <option value="${piece}">${piece}</option>
                                </c:forEach>

                            </select><label> 件,</label>
                            <label>每件</label>
                            <select name="pieceWeight" class="pieceWeight">
                                <c:forEach items="${weights}" var="weight">
                                    <option value="${weight}">${weight}</option>
                                </c:forEach>

                            </select>
                            <label>公斤</label>

                            <input type="radio" name="baggageType" class="baggageType" value="3"><label>公斤</label>
                            <select name="weight" class="weight">
                                <c:forEach items="${weights}" var="weight">
                                    <option value="${weight}">${weight}</option>
                                </c:forEach>
                            </select>

                        </div>

                        <div class="newBaggage" style="margin-left: 260px">
                            <label>是否转机</label><span>*</span><br/>
                            <select name="turningPoint" class="turningPoint">
                                <option value="1">是</option>
                                <option value="0" selected>否</option>
                            </select>
                        </div>

                        <div class="newBaggage" style="margin-left: 10px">
                            <label>行李直达</label><span>*</span><br/>
                            <select name="baggageDirect" class="baggageDirect" disabled>
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </div>

                        <div class="newBaggage" style="margin-left: 10px">
                            <label>需过境签</label><span>*</span><br/>
                            <select name="transitVisa" class="transitVisa" disabled>
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </div>
                    </div>
                    <%--<div class="backRule clearfix" style="position: relative">--%>
                    <%--<div class="fnDiv fl">--%>
                    <%--<label>退改规则</label><span>*</span>--%>
                    <%--</div>--%>
                    <%--<input type="text" name="refundRule" placeholder="请录入退改的具体规则" class="refundRule"/>--%>
                    <%--<input style="margin-left: 9%" type="text" name="specialRule" class="specialRule"--%>
                    <%--placeholder="特殊规则或需重点提醒的规则可录入此处，可不填写"/>--%>
                    <%--</div>--%>
                </div>
            </div>
            <c:forEach items="${order.details}" var="detail" varStatus="vs">
                <div class="process clearfix ">
                    <div class="process_title fl">第${numMap[detail.digital]}程</div>
                    <input type="hidden" name="id" value="${detail.id}">
                    <input type="hidden" name="digital" value="${detail.digital}">
                    <div class="proInfo fl">
                        <div class="clearX"><a class="clearcon">清空</a> <i class="iconfont icon-close"></i></div>
                        <div class="flightNo">
                            <div class="fnDiv">
                                <label>航班号</label><span>*</span><br>
                                <input type="text" placeholder="编号" name="flightNo_${vs.index+1}"
                                       value="${detail.flightNo}" class="flightNo" onblur="toUpper(this)"/>
                            </div>
                            <div class="fnDiv" style="width:12%;">

                                <input class="wd share" type="checkbox" name="share_${vs.index+1}"
                                       <c:if test="${detail.share}">checked</c:if>/><label style="margin-left: 16px">共享航班</label><span>*</span><br/>
                                <input type="text" disabled="disabled" style="width: 93px"
                                       name="realFlightNo_${vs.index+1}" value="${detail.realFlightNo}"
                                       placeholder="实际承运航班号" onblur="toUpper(this)" class="wd_input realFlightNo"/>
                            </div>
                            <div class="fnDiv">
                                <label>舱位编码</label><span>*</span><br/>
                                <input type="text" placeholder="编码" name="cabinCode_${vs.index+1}"
                                       value="${detail.cabinCode}" class="cabinCode" onblur="toUpper(this)"/>
                            </div>
                            <div class="fnDiv">
                                <label>起飞日期</label><span>*</span><br/>
                                <fmt:formatDate value="${detail.fromDate}" pattern="yyyyMMdd" var="fromDate"/>
                                <input type="text" placeholder="YYYYMMDD" name="fromDate_${vs.index+1}" class="fromDate"
                                       value='${fromDate}' readonly/>
                            </div>
                            <div class="fnDiv">
                                <label>到达日期</label><span>*</span><br/>
                                <fmt:formatDate value="${detail.toDate}" pattern="yyyyMMdd" var="toDate"/>
                                <input type="text" placeholder="YYYYMMDD" name="toDate_${vs.index+1}" class="toDate"
                                       value='${toDate}' readonly/>
                            </div>
                            <div class="fnDiv">
                                <label>出发机场</label><span>*</span><br/>
                                <input type="text" placeholder="三字码" name="fromAirportCode_${vs.index+1}"
                                       value="${detail.fromAirportCode}" onblur="toUpper(this)"
                                       class="fromAirportCode"/>
                            </div>
                            <div class="fnDiv">
                                <label>出发航站楼</label><span>*</span><br/>
                                <input type="text" placeholder="编码" name="fromTerminal_${vs.index+1}"
                                       value="${detail.fromTerminal}" onblur="toUpper(this)" class="fromTerminal"/>
                            </div>
                            <div class="fnDiv">
                                <label>到达机场</label><span>*</span><br/>
                                <input type="text" placeholder="三字码" name="toAirportCode_${vs.index+1}"
                                       value="${detail.toAirportCode}" onblur="toUpper(this)" class="toAirportCode"/>
                            </div>
                            <div class="fnDiv">
                                <label>到达航站楼</label><span>*</span><br/>
                                <input type="text" placeholder="编码" name="toTerminal_${vs.index+1}"
                                       value="${detail.toTerminal}" onblur="toUpper(this)" class="toTerminal"/>
                            </div>
                            <div class="fnDiv">
                                <label>起飞时间</label><span>*</span><br/>
                                <input type="text" placeholder="HHMM" name="fromTime_${vs.index+1}"
                                       value="${detail.fromTime}" class="fromTime"/>

                            </div>
                            <div class="fnDiv">
                                <label>到达时间</label><span>*</span><br/>
                                <input type="text" placeholder="HHMM" name="toTime_${vs.index+1}"
                                       value="${detail.toTime}" class="toTime"/>

                            </div>
                            <div class="fnDiv">
                                <label>机型</label><br/>
                                <input type="text" placeholder="录入机型" name="aircraftType_${vs.index+1}"
                                       value="${detail.aircraftType}" class="aircraftType"/>
                            </div>
                        </div>
                        <div class="stopFlight">
                            <p class="fnDiv" style="text-indent: 16px; line-height: 20px;">
                                <input class="wd stopOver" type="checkbox" name="stopOver_${vs.index+1}"
                                       <c:if test="${detail.stopOver}">checked</c:if> />经停航班
                            </p>
                            <div class="fnDiv" style="width: 10.7%">
                                <label>经停城市</label><span>*</span><br/>
                                <input type="text" style="width: 100px" name="stopCityName_${vs.index+1}"
                                       value="${detail.stopCityName}" placeholder="城市名称" disabled="disabled"
                                       class="wd_input stopCityName"/>
                            </div>
                            <div class="fnDiv">
                                <label>经停时长</label><span>*</span><br/>
                                <input type="text" placeholder="HHMM" name="stopTime_${vs.index+1}"
                                       value="${detail.stopTime}" disabled="disabled" class="wd_input stopTime"/>
                            </div>
                                <%--<div class="fnDiv">--%>
                                <%--<label>行李规定</label><span>*</span><br/>--%>
                                <%--<input style="width: 743px;" type="text" name="baggageRule_${vs.index+1}" value="${detail.baggageRule}" class="baggageRule" placeholder="输入行李规定" />--%>
                                <%--</div>--%>
                            <div class="newBaggage" style="position: relative">
                                <label>免费托运行李</label><span>*</span><br/>

                                <input type="radio" name="baggageType_${vs.index+1}" class="baggageType" value="1"
                                       <c:if test="${detail.changeRefundRuleDto.baggageType eq 1}">checked</c:if> ><label>无</label>

                                <input type="radio" name="baggageType_${vs.index+1}" class="baggageType" value="2"
                                       <c:if test="${detail.changeRefundRuleDto.baggageType eq 2}">checked</c:if> >
                                <select name="piece_${vs.index+1}" class="piece"
                                        <c:if test="${detail.changeRefundRuleDto.baggageType ne 2}">disabled</c:if> >

                                    <c:forEach items="${pieces}" var="piece">
                                        <option value="${piece}"
                                                <c:if test="${detail.changeRefundRuleDto.baggagePieces eq piece}">selected</c:if> >${piece}</option>
                                    </c:forEach>

                                </select><label> 件,</label>
                                <label>每件</label>
                                <select name="pieceWeight_${vs.index+1}" class="pieceWeight"
                                        <c:if test="${detail.changeRefundRuleDto.baggageType ne 2}">disabled</c:if>>
                                    <c:forEach items="${weights}" var="weight">
                                        <option value="${weight}"
                                                <c:if test="${detail.changeRefundRuleDto.baggage eq weight}">selected</c:if> >${weight}</option>
                                    </c:forEach>

                                </select>
                                <label>公斤</label>

                                <input type="radio" name="baggageType_${vs.index+1}" class="baggageType" value="3"
                                       <c:if test="${detail.changeRefundRuleDto.baggageType eq 3}">checked</c:if>><label>公斤</label>
                                <select name="weight_${vs.index+1}" class="weight"
                                        <c:if test="${detail.changeRefundRuleDto.baggageType ne 3}">disabled</c:if>>
                                    <c:forEach items="${weights}" var="weight">
                                        <option value="${weight}"
                                                <c:if test="${detail.changeRefundRuleDto.baggage eq weight}">selected</c:if> >${weight}</option>
                                    </c:forEach>
                                </select>

                            </div>

                            <div class="newBaggage" style="margin-left: 260px">
                                <label>是否转机</label><span>*</span><br/>
                                <select name="turningPoint_${vs.index+1}" class="turningPoint">
                                    <option value="1"
                                            <c:if test="${detail.turningPoint}">selected</c:if> >是
                                    </option>
                                    <option value="0"
                                            <c:if test="${!detail.turningPoint}">selected</c:if> >否
                                    </option>
                                </select>
                            </div>

                            <div class="newBaggage" style="margin-left: 10px">
                                <label>行李直达</label><span>*</span><br/>
                                <select name="baggageDirect_${vs.index+1}" class="baggageDirect"
                                        <c:if test="${!detail.turningPoint}">disabled</c:if> >
                                    <option value="1"
                                            <c:if test="${detail.baggageDirect}">selected</c:if> >是
                                    </option>
                                    <option value="0"
                                            <c:if test="${!detail.baggageDirect}">selected</c:if> >否
                                    </option>
                                </select>
                            </div>

                            <div class="newBaggage" style="margin-left: 10px">
                                <label>需过境签</label><span>*</span><br/>
                                <select name="transitVisa_${vs.index+1}" class="transitVisa"
                                        <c:if test="${!detail.turningPoint}">disabled</c:if> >
                                    <option value="1"
                                            <c:if test="${detail.transitVisa}">selected</c:if> >是
                                    </option>
                                    <option value="0"
                                            <c:if test="${!detail.transitVisa}">selected</c:if> >否
                                    </option>
                                </select>
                            </div>
                        </div>
                            <%--<div class="backRule clearfix" style="position: relative">--%>
                            <%--<div class="fnDiv fl">--%>
                            <%--<label>退改规则</label><span>*</span>--%>
                            <%--</div>--%>
                            <%--<input type="text" name="refundRule_${vs.index+1}" placeholder="请录入退改的具体规则" value="${detail.refundRule}" class="refundRule" />--%>
                            <%--<input style="margin-left: 9%" type="text" name="specialRule_${vs.index+1}" class="specialRule" value="${detail.specialRule}" placeholder="特殊规则或需重点提醒的规则可录入此处，可不填写" />--%>
                            <%--</div>--%>


                    </div>
                </div>
            </c:forEach>
        </div>


        <div class="ftInfo ftUser" style="margin-top: 0px">
            <div class="travelTitle clearfix">
                <span class="title">退改</span>
                <span style="margin-top: 13px;font-size: small;margin-left: 30px">请按"金额<font color="#3cb371">&</font>起飞前几小时<font
                        color="#3cb371">&</font>金额"录入退改费，或 "-1" 表示"以航司为准"，"-2" 表示"不可退改"。如："800&2&-2"表示"起飞两小时前收取800元，起飞前2小时以内及起飞后不得退票"</span>
            </div>

            <table class="m-table-container">
                <thead>
                <th>适用航段</th>
                <th>退票</th>
                <th>改期</th>
                <th>操作</th>
                </thead>
                <tr>
                    <td style="width: 23%" id="detailsCheck">

                        <span class="checkSpan defaultDiv">
                            <span>
                                <input type="checkbox" checked disabled>
                            </span>
                            <span class="detailNo">第一程</span>
                        </span>

                        <c:forEach items="${order.details}" var="detail" varStatus="vs">
                            <span class="checkSpan">
                                   <span>
                                      <input type="checkbox" checked disabled>
                                   </span>
                            <span class="detailNo">第${numMap[vs.count]}程</span>
                            </span>
                        </c:forEach>

                    </td>


                    <td style="position: relative"><input type="text" name="refundRule"
                                                          value="${order.refundRuleDtos[0].refundRule}"></td>
                    <td style="position: relative"><input type="text" name="changeRule"
                                                          value="${order.refundRuleDtos[0].changeRule}"></td>
                    <td></td>
                </tr>
                <tr>
                    <td><span style="float: right;font-weight: 700">特殊说明</span></td>
                    <td><input type="text" name="specialRefundRule"
                               value="${order.refundRuleDtos[0].specialRefundRule}"></td>
                    <td><input type="text" name="specialChangeRule"
                               value="${order.refundRuleDtos[0].specialChangeRule}"></td>
                    <td></td>
                </tr>
            </table>


        </div>

        <!--乘机人-->
        <div class="ftInfo ftUser">
            <div class="travelTitle clearfix">
                <span class="title">乘机人</span>
                <c:if test="${order.paymentMethod == 'UNDER_LINE_PAY'}">
                    <div class="trvType">乘客类型
                        <select class="userType" name="passengerType">
                            <option value="ADU">成人</option>
                            <option value="CHD">儿童</option>
                            <option value="INF">婴儿</option>
                        </select>
                        <a class="addTrvbtn addStaff_flight">添加员工乘机人</a><a
                                class="addTrvbtn addNoStaff_flight">添加非员工乘机人</a>
                    </div>
                </c:if>
            </div>
            <div class="userInfo" id="users">
                <table class="userTab" cellspacing="1">
                    <thead>
                    <th>序号</th>
                    <th>全名<span>*</span></th>
                    <th>姓（拼音）Surname<span>*</span></th>
                    <th>名（拼音）Given name<span>*</span></th>
                    <th>国籍<span>*</span></th>
                    <th>性别<span>*</span></th>
                    <th>出生日期<span>*</span></th>
                    <th>证件类型<span>*</span></th>
                    <th>登机证件号<span>*</span></th>
                    <th>证件有效期<span>*</span></th>
                    <th>手机号</th>
                    <th>职级</th>
                    <th>票号<span>*</span></th>
                    <c:if test="${order.paymentMethod == 'UNDER_LINE_PAY'}">
                        <th>操作</th>
                    </c:if>
                    </thead>
                    <tr class="defaultDiv">
                        <td class='order' style="min-width: 40px">1</td>
                        <input type="hidden" name="userId" class="userId">
                        <input type="hidden" name="travellerType" class="travellerType">
                        <input type="hidden" name="passengerType" class="passengerType"/>
                        <td class="name" style="position: relative"><input type="text" class="name" name="name"/></td>
                        <td class="familyName" style="position: relative"><input type="text" class="surName"
                                                                                 name="surName"/></td>
                        <td class="givenName" style="position: relative"><input type="text" class="givenName"
                                                                                name="givenName"/></td>
                        <td class="nationality" style="position: relative"><input type="text" class="nationality"
                                                                                  name="nationality"/></td>
                        <td class="sexIpt">
                            <input type="radio" name="gender" value="M" class="gender" checked/>男
                            <input type="radio" name="gender" value="F" class="gender"/>女
                        </td>
                        <td style="position: relative"><input style="min-width: 130px;" type="text" name="birthday"
                                                              class="birthday" readonly/></td>
                        <td>
                            <select name="credentialType" class="credentialType">
                                <c:forEach items="${credentialTypeMap}" var="type">
                                    <option value="${type.key}"
                                            <c:if test="${type.key == 'PASSPORT'}">selected</c:if> >${type.value}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td style="position: relative"><input type="text" name="credentialNo" class="credentialNo"/>
                        </td>
                        <td style="position: relative"><input style="min-width: 130px;" type="text" name="dateOfExpiry"
                                                              class="dateOfExpiry" readonly/></td>
                        <td style="position: relative"><input style="min-width: 120px;" type="tel" name="mobile"
                                                              class="mobile"/></td>
                        <td>
                            <select name="empLevelCode" class="empLevelCode">
                                <c:forEach items="${empLevelList}" var="emplevel">
                                    <option type="text" value="${emplevel.itemCode}">${emplevel.itemTxt} </option>
                                </c:forEach>
                            </select>
                        </td>
                        <td style="position: relative" class="ticket"><input type="text" name="ticketNo"
                                                                             class="ticketNo"/>
                            <p class="ticketCodeNotice">联票票号以XXXXXXXXXX01-03格式录入</p>
                        </td>
                        <td><a class="delUser">删除</a></td>
                    </tr>
                    <c:forEach items="${order.passengers}" var="passenger" varStatus="vs">
                        <tr>
                            <td class='order' style="min-width: 40px">1</td>
                            <input type="hidden" name="id" value="${passenger.id}">
                            <input type="hidden" name="userId_${vs.index+1}" class="userId" value="${passenger.userId}">
                            <input type="hidden" name="travellerType_${vs.index+1}" class="travellerType"
                                   value="${passenger.travellerType}">
                            <input type="hidden" name="passengerType_${vs.index+1}" class="passengerType"
                                   value="${passenger.passengerType}"/>
                            <td class="name" style="position: relative"><input type="text" class="name"
                                                                               name="name_${vs.index+1}"
                                                                               value="${passenger.name}"/></td>
                            <td class="familyName" style="position: relative"><input type="text" class="surName"
                                                                                     name="surName_${vs.index+1}"
                                                                                     value="${passenger.surName}"/></td>
                            <td class="givenName" style="position: relative"><input type="text" class="givenName"
                                                                                    name="givenName_${vs.index+1}"
                                                                                    value="${passenger.givenName}"/>
                            </td>
                            <td class="nationality" style="position: relative"><input type="text" class="nationality"
                                                                                      name="nationality_${vs.index+1}"
                                                                                      value="${passenger.nationality}"/>
                            </td>
                            <td class="sexIpt">
                                <input type="radio" name="gender_${vs.index+1}" value="M" class="gender"
                                       <c:if test="${passenger.gender eq 'M'}">checked</c:if> />男
                                <input type="radio" name="gender_${vs.index+1}" value="F" class="gender"
                                       <c:if test="${passenger.gender eq 'F'}">checked</c:if> />女
                            </td>
                            <fmt:formatDate value="${passenger.birthday}" var="bitrhday" pattern="yyyy-MM-dd"/>
                            <td style="position: relative"><input style="min-width: 130px;" type="text"
                                                                  name="birthday_${vs.index+1}" class="birthday"
                                                                  value="${bitrhday}" readonly/></td>
                            <td>
                                <select name="credentialType_${vs.index+1}" class="credentialType">
                                    <c:forEach items="${credentialTypeMap}" var="type">
                                        <option value="${type.key}"
                                                <c:if test="${type.key == passenger.credentialType}">selected</c:if> >${type.value}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td style="position: relative"><input type="text" name="credentialNo_${vs.index+1}"
                                                                  class="credentialNo"
                                                                  value="${passenger.credentialNo}"/></td>
                            <td style="position: relative"><input style="min-width: 130px;" type="text"
                                                                  name="dateOfExpiry_${vs.index+1}"
                                                                  value="${passenger.dateOfExpiry}" class="dateOfExpiry"
                                                                  readonly/></td>
                            <td style="position: relative"><input style="min-width: 120px;" type="tel"
                                                                  name="mobile_${vs.index+1}"
                                                                  value="${passenger.mobile}" class="mobile"/></td>
                            <td>
                                <select name="empLevelCode_${vs.index+1}" class="empLevelCode">
                                    <c:forEach items="${empLevelList}" var="emplevel">
                                        <option type="text" value="${emplevel.itemCode}"
                                                <c:if test="${passenger.empLevelCode eq emplevel.itemCode}">selected</c:if> >${emplevel.itemTxt} </option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td style="position: relative" class="ticket">
                                <input type="text" name="ticketNo_${vs.index+1}" value="${passenger.ticketNo}"
                                       class="ticketNo"/>
                                <p class="ticketCodeNotice">联票票号以XXXXXXXXXX01-03格式录入</p>
                            </td>
                            <c:if test="${order.paymentMethod == 'UNDER_LINE_PAY'}">
                                <td><a class="delUser">删除</a></td>
                            </c:if>
                        </tr>
                    </c:forEach>

                </table>
            </div>
        </div>

        <!--金额-->
        <div class="ftInfo ftMoney">
            <span class="title">金额</span>
            <table class="moneyTab" cellpadding="1" cellspacing="1">
                <thead>
                <td>单人销售票价<span>*</span></td>
                <td>单人销售税费<span>*</span></td>
                <td>单人服务费<span>*</span></td>
                <td>单人结算票价<span>*</span></td>
                <td>单人结算税费<span>*</span></td>
                </thead>
                <tr id="price">

                    <td style="position: relative"><input type="number" name="singleTicketPrice"
                                                          value="${order.singleTicketPrice}"
                                                          <c:if test="${order.orderShowStatus ne 'WAIT_PAYMENT'}">readonly</c:if> />
                    </td>
                    <td style="position: relative"><input type="number" name="singleTaxFee"
                                                          value="${order.singleTaxFee}"
                                                          <c:if test="${order.orderShowStatus ne 'WAIT_PAYMENT'}">readonly</c:if> />
                    </td>
                    <td style="position: relative"><input type="number" name="singleExtraFee"
                                                          value="${order.singleExtraFee}"
                                                          <c:if test="${order.orderShowStatus ne 'WAIT_PAYMENT'}">readonly</c:if> />
                    </td>
                    <td style="position: relative"><input type="number" name="singleSettelPrice"
                                                          value="${order.singleSettelPrice}"/></td>
                    <td style="position: relative"><input type="number" name="singleSettelTaxFee"
                                                          value="${order.singleSettelTaxFee}"/></td>
                </tr>
            </table>
            <div class="CNY">
                <label>人数：<span id="passengerNum">${fn:length(order.passengers)}</span>人</label>
                <label>客户支付总额：CNY<span class="rd" id="totalSalePrice">0.00</span></label>
                <label>供应商结算总额：CNY<span id="totalSettelPrice">0.00</span></label>
                <label>毛利：CNY<span class="rd" id="profit">0.00</span></label>
            </div>
        </div>

        <!--供应商-->
        <div class="ftInfo">
            <!--供应商信息-->
            <div class="travelInfo">
                <span class="title">供应商信息</span>
                <div class="tInfo">
                    <div class="infoDiv" style="position: relative">
                        <label>PNR</label><span>*</span>
                        <input type="text" name="pnrCode" onblur="toUpper(this)" value="${order.pnrCode}"/>
                    </div>
                    <div class="infoDiv" style="position: relative">
                        <label>供应商名称</label><span>*</span>
                        <select name="issueChannel">
                            <option value="">--请选择供应商名称--</option>
                            <c:forEach items="${channels}" var="channel">
                                <option value="${channel.code}"
                                        <c:if test="${channel.code eq order.issueChannel}">selected</c:if> >${channel.name}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="infoDiv" style="position: relative">
                        <label>供应商单号</label><span>*</span>
                        <input type="text" name="supplierNo" value="${order.externalOrderId}"/>
                    </div>
                </div>
            </div>


            <!--联系人-->
            <div class="reservationInfo">
                <span class="title">联系人</span>
                <div class="tInfo">
                    <div class="infoDiv nm" style="position: relative">
                        <label>中文或英文姓名</label><span>*</span>
                        <input type="text" name="contactorName" placeholder="请输入联系人姓名" value="${order.contactorName}"/>
                    </div>
                    <div class="infoDiv tel" style="position: relative">
                        <label>手机号</label><span>*</span>
                        <input type="tel" placeholder="请输入手机号" name="contactorMobile" value="${order.contactorMobile}"/>
                    </div>
                    <div class="infoDiv email" style="position: relative">
                        <label>E-mail（选填）</label>
                        <input type="text" placeholder="请输入邮箱" name="contactorEmail" value="${order.contactorEmail}"/>
                    </div>
                </div>
            </div>


        </div>

        <!--btn-->
        <div class="interBtn"><a class="formBtn formBtnGray">确认保存</a></div>

        <div class="addStaff" style="display: none">
            <div class="addtitle">
                <h3>添加员工乘机人</h3>
                <p>
                    <input type="text" id="condition" placeholder="请输入部门名称/姓名"/>
                    <i class="iconfont icon-search"></i>
                </p>
                <p id="orgNames">
                    <a onclick="searchByOrgId(null)">${topPatern.fullname}</a>
                </p>
            </div>
            <div class="topUser">

            </div>
            <div class="depart">

            </div>
            <div class="staff">

            </div>
        </div>
    </form>
</div>


<%--树弹窗 --%>
<div class="otherExpenseH">
    <ul id="expenseDetail"></ul>
</div>

<script src="/resource/plugins/layer/tree.js?version=${globalVersion}"></script>
<script src="/resource/js/order/internationalFlightOrderDetail.js?version=${globalVersion}"></script>
<script type="text/javascript" src="/resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>
<link rel="stylesheet" href="/resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script src="/resource/plugins/expenseAttribution.js?version=${globalVersion}"></script>

</body>
