<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/trainBackDetail.css?version=${globalVersion}">
<div class="main">
        <div class="oContent fix">
            <div class="oContentL">
                <h2 class="oTitle">退票详情</h2>
                <div class="fix oNumber">
                    <div class="l">
                        <p >退票单号：<span id="refundId">${refund.id}</span></p>
                        
                    </div>
                </div>
                <div class="oState">
                    <h3 class="oStateTitle">${refund.status.message}</h3>
                    <div class="oProgress fix">
                        <div class="oProgressItem oProgressItemActive">
                            <p>提交申请</p>
                            <span><fmt:formatDate value="${refund.applyTime }" pattern="yyyy年MM月dd日  HH:mm:ss"/></span>
                        </div>
                        <div class="oProgressItem ${refund.status eq 'REFUNDING' or refund.status eq  'REFUNDED' ? 'oProgressItemActive':'' }">
                            <p>退款中</p>
                            <span><fmt:formatDate value="${refund.applyTime }" pattern="yyyy年MM月dd日  HH:mm:ss"/></span>
                        </div>
                        <div class="oProgressItem ${refund.status eq 'REFUNDED'?'oProgressItemActive':'' }">
                            <p id="last">已退款</p>
                            <span><fmt:formatDate value="${refund.finishTime }" pattern="yyyy年MM月dd日  HH:mm:ss"/></span>
                        </div>
                        <input type="hidden" value="${refund.status}" id="refundStatus"/>
                    </div>
                </div>
                <div class="oItem">
                    <h3 class="oItemTitle">退票信息</h3>
                    <div class="oItemLi">
                       
                        <p><span class="oItemLiLabel">乘客</span>
                           <c:forEach items="${order.passengerList}" var="p">
                               <span>${p.name}</span>
                           </c:forEach>
                        </p>
                        <p><span class="oItemLiLabel">原因</span><span id="reason">${refund.reason}</span></p>
                        
                    </div>
                </div>
                <div class="oItem">
                    <h3 class="oItemTitle">联系人</h3>
                    <div class="oItemLi">
                        <p><span class="oItemLiLabel">姓名</span><span>${order.contactor.name}</span></p>
                        <p><span class="oItemLiLabel">手机</span><span>${order.contactor.mobile}</span></p>
                        <p><span class="oItemLiLabel">邮箱</span><span>${order.contactor.email}</span></p>
                    </div>
                </div>
            </div>
           <!--右侧内容-->
            <div class="content_right">
                <div class="right_top">
                    <p class="top1">
                        <span class="Rtop_time"><fmt:formatDate value="${order.fromTime }" pattern="MM-dd"/> </span>
                        <span class="Rtop_week"><fmt:formatDate value="${order.fromTime }" pattern="E"/> </span>
                        <span class="Rtoop_place">${order.fromStation }-${order.arriveStation }</span>
                        <span class="Rtoop_rank">${order.seatTypeValue }</span>
                    </p>
                </div>
                <div class="right_top2">
                    <p class="start_endP">
                        <span class="start_time"><fmt:formatDate value="${order.fromTime }" pattern="HH:mm"/></span>
                        <span class="train_name">${order.trainCode }</span>
                        <span class="end_time"><fmt:formatDate value="${order.arriveTime }" pattern="HH:mm"/></span>
                    </p>
                    <p class="station_p">
                        <span class="start_station">${order.fromStation }</span>
                        <span class="Rtop2_line"></span>
                        <span class="over_time" id="costTime"></span>
                        <span class="Rtop2_line"></span>
                        <span class="end_station">${order.arriveStation }</span>
                    </p>
                    <p class="date_p">
                        <span class="start_date"><fmt:formatDate value="${order.fromTime }" pattern="MM月dd日"/></span>
                        <span class="end_date"><fmt:formatDate value="${order.arriveTime }" pattern="MM月dd日"/></span>
                    </p>
                </div>
                <!--费用明细-->
                <div class="detailed_box">
                    <div class="expense_detailDiv">费用明细</div>
                    <div class="ticketPlice_div fix"><span class="ticketPlice_left">车票总计</span><span
                            class="ticketPlice_right">¥<fmt:formatNumber  value="${order.totalAmount-order.servicePrice }" pattern="0.00"/> </span></div>
                    <div class="sum_div fix"><span class="sum_left">服务费合计</span><span class="sum_right">¥${order.servicePrice}</span></div>
                </div>
                <!--订单总额-->
                <div class="totalOrder_div fix">
                    <span class="totalOrder_left">订单总额</span><span class="totalOrder_right">¥${order.totalAmount}</span>
                </div>
            </div>
        </div>


    </div>
    
    
    
   <form id="refundDetail" method="post">
   
   </form>
   <script src="resource/js/order/trainBackDetail.js?version=${globalVersion}"></script>
   <script type="text/javascript">
$(function(){
	$("#costTime").text(dateDifference('${order.costTime}'));
});

// 计算时间差（传入的是分钟数）
function dateDifference(minute){
	if(minute>60){
		return (minute%60)==0?Math.floor((minute/60))+"h":Math.floor((minute/60))+"h"+(minute%60)+"m";
	}else{
		return minute+"m";
	}
}
function reload(){
	location.href = location.href = window.location.href;
}
</script>
