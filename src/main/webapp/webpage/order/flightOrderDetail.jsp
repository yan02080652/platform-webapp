<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/flightOrderDetail.css?version=${globalVersion}">

<div class="main">
	<div class="contentBox fix">
		<div class="oContentL">
			<!-- <p class="nav">
				<a ><span class="first">订单</span></a> 
				<a href="order/flightOrder"><span class="sub">机票订单</span></a> 
				<a href="javascript:history.go(0);"><span class="three">订单详情</span></a>
			</p> -->
			<h2 class="oTitle">
				<c:if test="${flightOrderDto.orderType ne 'TICKET_CHANGES' }">订单详情</c:if>
				<c:if test="${flightOrderDto.orderType eq 'TICKET_CHANGES' }">改签详情</c:if>
			</h2>
			<div class="fix oNumber">
				<div class="l">
					<p>
						<c:if test="${flightOrderDto.orderType ne 'TICKET_CHANGES' }">
							订单号：<span>${flightOrderDto.id }</span>
						</c:if>
						<c:if test="${flightOrderDto.orderType eq 'TICKET_CHANGES' }">
							改签单号：<span>${flightOrderDto.id }</span>&nbsp;&nbsp;
							<font color="#46a1bb">
								<a href="order/flightDetail/${flightOrderDto.originalOrderId }" target="_blank">原订单号：${flightOrderDto.originalOrderId }</a>
							</font>
						</c:if>
					</p>
				</div>
				<div class="r">
					<c:if test="${hasInsuranceOrder }">
						<button class="subsidiary-but" onclick="showInsuranceOrder('${flightOrderDto.id}')">查看附属产品</button>
					</c:if>
				</div>
				<%-- <div class="r">
					<c:if test="${currentUserId eq flightOrderDto.userId }">
						<c:if test="${hasInsuranceOrder }">
							 <button class="subsidiary-but" onclick="showInsuranceOrder('${flightOrderDto.id}')">查看附属产品</button>
						</c:if>
					    <input value="${insuranceOrderDtoIds }" type="hidden" id="insuranceAndOrderIds">
						<c:if test="${flightOrderDto.orderShowStatus  eq  'WAIT_PAYMENT' or flightOrderDto.orderShowStatus  eq  'AUTHORIZE_RETURN'}">
							<button class="cancel" onclick="orderCancel('${flightOrderDto.id}')">取消订单</button>
						</c:if>
						<c:if test="${flightOrderDto.orderShowStatus  eq  'WAIT_PAYMENT' or flightOrderDto.orderShowStatus  eq  'AUTHORIZE_RETURN'}">
							<button class="pay" onclick="orderPayment()">立即支付</button>
						</c:if>
						 <c:if test="${flightOrderDto.orderShowStatus eq 'ISSUED' or flightOrderDto.orderShowStatus eq 'CHANGED'}">
							 <button class="cancel" onclick = "changeBack('${flightOrderDto.id}')">申请退改</button>
						 </c:if>
					</c:if>
				</div> --%>
			</div>
			<div class="oState">
				<input value="${flightOrderDto.createDate.getTime() }" id="startTime" type="hidden">
				<input value="${flightOrderDto.orderShowStatus }" id="orderShowStatus" type="hidden">
				<h3 class="oStateTitle">${flightOrderDto.orderShowStatus.message }</h3>
				<div class="fix mb20">
					<div class="l">
						<p class="oCount">
							<c:if test="${flightOrderDto.orderShowStatus eq 'WAIT_PAYMENT' }">
								<span>请您在提交订单后<span id="timer"></span>分钟内完成支付，超时订单将被关闭</span>
							</c:if>
						</p>
					</div>
					<div class="r rule">
						<!-- <span  class="advanceDate"><span id="difference"></span></span>  -->
						<span class="discount">${cabinGrade }
						<c:choose>
							<c:when test="${flightOrderDto.orderFlights[0].facePrice eq flightOrderDto.orderFlights[0].cabinFullPrice }">全价</c:when>
							<c:otherwise><fmt:formatNumber value="${flightOrderDto.orderFlights[0].facePrice * 10 div flightOrderDto.orderFlights[0].cabinFullPrice }" pattern="#.0" />折</c:otherwise>
						</c:choose>
						</span>
					</div>
				</div>
				<div class="oProgress fix">
					<div class="oProgressItem" id="stage1">
						<p>下单</p>
						<span><fmt:formatDate value="${flightOrderDto.createDate }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</div>
					<div class="oProgressItem" id="stage2">
						<p>支付</p>
						<c:if test="${not empty flightOrderDto.paymentDate }">
							<span ><fmt:formatDate value="${flightOrderDto.paymentDate }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
						</c:if>
						<c:if test="${empty flightOrderDto.paymentDate and  flightOrderDto.orderShowStatus eq 'IN_CHANGE'}">
							<span ><fmt:formatDate value="${flightOrderDto.createDate }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
						</c:if>
					</div>
					<div class="oProgressItem" id="stage3">
						<p>出票成功</p>
						<c:if test="${flightOrderDto.orderShowStatus eq 'ISSUED' or flightOrderDto.orderShowStatus eq 'CHANGED'}">
							<span><fmt:formatDate value="${flightOrderDto.issueTicketTime }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
						</c:if>
					</div>
				</div>
			</div>
			<div class="oItem">
				<h3 class="oItemTitle">差旅信息</h3>
				<div class="oItemLi">

					<style>
						.oItemLi .whole-line{
							white-space: nowrap;
							display: flex;
						}
						.oItemLi .whole-line span{
							vertical-align: top;
							white-space: normal;
							word-break: break-all;
							display: inline-block;
						}
						.oItemLi .whole-line span:last-child{
							flex: 1;
						}
					</style>

					<p>
						<span class="oItemLiLabel">出行性质</span>
						<c:choose>
							<c:when test="${flightOrderDto.travelType eq 'PERSONAL'}">
								<span>因私</span>
							</c:when>
							<c:otherwise>
								<span>因公</span>
							</c:otherwise>
						</c:choose>
					</p>
					<c:if test="${flightOrderDto.travelType ne 'PERSONAL'}">

						<p>
							<span class="oItemLiLabel">差旅计划</span>
							<span>${flightOrderDto.travelPlanNo}</span>
						</p>

						<p>
							<span class="oItemLiLabel">出行类别</span>
							<span>${travel.itemTxt }</span>
						</p>
						<p class="whole-line">
							<span class="oItemLiLabel">出差事由</span>
							<span>${flightOrderDto.travelReason}</span>
						</p>
						<p>
							<span class="oItemLiLabel">费用归属</span>
							<span>${orgPath }</span>
						</p>
					</c:if>
				</div>
			</div>
			<c:if test="${not empty paySuccess }">
				<div class="oItem">
					<h3 class="oItemTitle">付款信息</h3>
					<c:if test="${not empty bpAmount and bpAmount > 0}">
						<div class="oItemLi">
							<p>
								<span class="oItemLiLabel">企业支付</span><span>${bpAmount}</span>
							</p>
						</div>
					</c:if>
					<c:if test="${not empty pAmount and pAmount > 0}">
						<div class="oItemLi">
							<p>
								<span class="oItemLiLabel">个人支付</span><span>${pAmount }</span>
							</p>
						</div>
					</c:if>
				</div>
			</c:if>
			<div class="oItem">
				<h3 class="oItemTitle">乘机人</h3>
				<c:forEach items="${flightOrderDto.passengers }" var="passenger">
					<div class="oItemLi">
						<p>
							<span class="oItemLiLabel">姓名</span><span>${passenger.name }</span>
						</p>
						<p>
							<span class="oItemLiLabel">证件</span><span>${passenger.credentialTypeValue } ${passenger.credentialNo }</span>
						</p>
						<p>
							<span class="oItemLiLabel">类型</span><span>${passenger.passengerTypeValue }</span>
						</p>
						<p>
							<span class="oItemLiLabel">票号</span>
							<c:choose>
								<c:when test="${not empty passenger.ticketNo }">
									<span class="oStateSub">${passenger.ticketNo }</span>
								</c:when>
								<c:otherwise>
									<span class="oStateSub">--</span>
								</c:otherwise>
							</c:choose>
						</p>
						<c:if test="${not empty passenger.status }">
							<p>
								<span class="oItemLiLabel">状态</span><span class="oStateSub">${passenger.status.message }</span>
							</p>
						</c:if>
					</div>
				</c:forEach>
			</div>
			<div class="oItem">
				<h3 class="oItemTitle">联系人</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">姓名</span><span>${flightOrderDto.contactor.name }</span>
					</p>
					<p>
						<span class="oItemLiLabel">手机</span><span>${flightOrderDto.contactor.mobile }</span>
					</p>
					<p>
						<span class="oItemLiLabel">邮箱</span><span>${flightOrderDto.contactor.email }</span>
					</p>
				</div>
			</div>
		</div>
		<div class="content_right">
			<c:forEach items="${flightOrderDto.orderFlights}" var="detail">
				<div class="right_top1">
					<p class="top1 fix">
						<span class="Rtop_time"><fmt:formatDate value="${detail.fromDate}" pattern="MM-dd"/></span>
						<span class="Rtop_week"><fmt:formatDate value="${detail.fromDate}" pattern="E"/></span>
						<span class="Rtoop_place">${detail.fromCityName }-${detail.toCityName }</span>
					</p>
				</div>
				<div class="rightItem right_top2 fix">
					<div class="flightIco">
						<img  alt="" src="http://${SYS_CONFIG['imgserver'] }/${carrierDto.logo }">
					</div>
					<div class="flightName">${detail.carrierName }</div>
					<div class="flightCode">${detail.flightNo }</div>
					<div class="flightType">${detail.aircraftType }</div>
					<div class="flightCabin">${gradeMap[detail.cabinClass] }</div>
				</div>

				<c:if test="${not empty detail.shareCarrier }">
					<div class="share fix">
						<p class="share-t"><i class="iconfont icon-arrow"></i><span>共享航班:</span></p>
						<p class="share-info"><span>实际乘坐</span>
							<img  alt="" src="http://${SYS_CONFIG['imgserver'] }/${detail.shareCarrier.logo }">
							<span>${detail.shareCarrier.nameShort} ${detail.realFlightNo }</span>
						</p>
					</div>
				</c:if>

				<div class="rightItem right_top3">
					<p class="fix">
						<span class="Rtop3_timeL"><fmt:formatDate value="${detail.fromDate}" pattern="HH:mm" /></span>
						<span class="Rtop3_line"></span>
						<span class="Rtop3_center">${detail.flightDuration }</span>
						<span class="Rtop3_line"></span>
						<span class="Rtop3_timeR"><fmt:formatDate value="${detail.toDate}" pattern="HH:mm"/>
                    <c:if test="${detail.fromDate.getDay() ne detail.toDate.getDay()}">
						<a href="javascript:;" class="dayHover">+1</a>
						<span class="dayTipb">
                       		 <a href="javascript:;" class="dayTip">到达时间为第2天,&nbsp;&nbsp;<fmt:formatDate value="${detail.toDate}" pattern="yyyy年MM月dd日  HH:mm"/></a>
                        </span>
					</c:if>
                      </span>
					</p>
					<p class="fix rt3-sub2">
						<span class="Rtop3_start">${detail.fromAirportName }</span>
						<span class="Rtop3_company">
                      	<c:if test="${not empty detail.stopCity and detail.stopCity !='' }"><b>停</b>${detail.stopCityName }</c:if>
                      </span>
						<span class="Rtop3_end">${detail.toAirportName }</span>
					</p>
				</div>


				<div class="backOrChange" style="padding-bottom: 20px;">
					<c:if test="${detail.refundChangeDetail.type eq 1}">
						<table>
							<tbody>
							<tr>
								<td></td>
								<c:forEach items="${detail.refundChangeDetail.headers}" var="head">
									<td>${head}</td>
								</c:forEach>
							</tr>
							<tr>
								<td>退票费</td>
								<c:forEach items="${detail.refundChangeDetail.refundAmountList}" var="amonut">
									<c:choose>
										<c:when test="${amonut > 0}">
											<td>¥ <span class="bOrCMoney">${amonut}</span>/人</td>
										</c:when>
										<c:otherwise>
											<td>免费退票</td>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</tr>
							<tr>
								<td>同舱改期费</td>
								<c:forEach items="${detail.refundChangeDetail.changeAmountList}" var="amonut">
									<c:choose>
										<c:when test="${amonut > 0}">
											<td>¥ <span class="bOrCMoney">${amonut}</span>/人</td>
										</c:when>
										<c:otherwise>
											<td>免费改期</td>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</tr>
							<tr>
								<td>签转条件</td>
								<td colspan="${fn:length(detail.refundChangeDetail.headers)}">${detail.refundChangeDetail.endorseRule}</td>
							</tr>
							<tr>
								<td>行李额</td>
								<td colspan="${fn:length(detail.refundChangeDetail.headers)}">${detail.refundChangeDetail.baggage}</td>
							</tr>
							<tr>
								<td>备注</td>
								<td colspan="${fn:length(detail.refundChangeDetail.headers)}">${detail.refundChangeDetail.remark}</td>
							</tr>
							</tbody>
						</table>
					</c:if>
					<c:if test="${detail.refundChangeDetail.type eq 2}">
						<table>
							<tbody>
							<tr>
								<td>退票费</td>
								<td>${detail.refundChangeDetail.headers[0]}</td>
							</tr>
							<tr>
								<td>同舱改期费</td>
								<td>${detail.refundChangeDetail.headers[1]}</td>
							</tr>
							<tr>
								<td>签转条件</td>
								<td>${detail.refundChangeDetail.endorseRule}</td>
							</tr>
							<tr>
								<td>行李额</td>
								<td>${detail.refundChangeDetail.baggage}</td>
							</tr>
							<tr>
								<td>备注</td>
								<td>${detail.refundChangeDetail.remark}</td>
							</tr>
							</tbody>
						</table>
					</c:if>
					<c:if test="${detail.refundChangeDetail.type eq 3}">
						<table>
							<tbody>
							<tr>
								<td>退票说明</td>
								<td class="tdInfo">${detail.refundChangeDetail.headers[0]}</td>
							</tr>
							<tr>
								<td>改期说明</td>
								<td class="tdInfo">${detail.refundChangeDetail.headers[1]}</td>
							</tr>
							<tr>
								<td>签转说明</td>
								<td class="tdInfo">${detail.refundChangeDetail.headers[2]}</td>
							</tr>
							</tbody>
						</table>
					</c:if>
				</div>

			</c:forEach>





			<div class="total_order fix">
				<div>
					<p class="total_order1">客户结算价</p>
					<p class="total_order3">¥<b class="total_order4" ><fmt:formatNumber type="number" value="${flightOrderDto.orderFee.totalOrderPrice+insuranceTotalPrice  }" pattern="0.00" maxFractionDigits="2"/></b></p>
				</div>
			</div>
			<div class="detailed_box">
				<div class="adult fix">
					<span class="l">成人</span><span class="xuxian"></span>
					<span class="detailed_spanR">¥${flightOrderDto.orderFee.salePrice + flightOrderDto.orderFee.departureTax
							+ flightOrderDto.orderFee.fuelTax + flightOrderDto.orderFee.upgradePrice
							+ flightOrderDto.orderFee.rescheduledPrice }
                      <a href="javascript:;">x</a><span>${fn:length(flightOrderDto.passengers)}</span></span>
				</div>
				<ul class="detailed_ul">

					<c:if test="${flightOrderDto.orderType eq 'ROUTINE'}">
						<li class="fix">
							<span>成人票</span><span class="xuxian"></span>
							<span class="detailed_spanR">
							  	<fmt:formatNumber value="${flightOrderDto.orderFee.salePrice  }" type="CURRENCY"/>
							  </span>
						</li>
						<li class="fix">
							<span>机场建设费</span><span class="xuxian"></span>
							<span class="detailed_spanR">
							 	 <fmt:formatNumber  value="${flightOrderDto.orderFee.departureTax  }" type="CURRENCY"/>
						 	 </span>
						</li>
						<li class="fix">
							<c:choose>
								<c:when test="${flightOrderDto.orderFee.fuelTax > 0  }">
									<span>燃油附加费</span><span class="xuxian"></span>
									<span class="detailed_spanR">
										  <fmt:formatNumber  value="${flightOrderDto.orderFee.fuelTax  }" type="CURRENCY"/>
									  </span>
								</c:when>
								<c:otherwise>
									<span>燃油附加费</span><span class="xuxian"></span><span
										class="detailed_spanR">免费</span>
								</c:otherwise>
							</c:choose>
						</li>
					</c:if>

					<c:if test="${flightOrderDto.orderType eq 'TICKET_CHANGES'}">
						<li class="fix">
							<span>同舱改期费</span><span class="xuxian"></span>
							<span class="detailed_spanR">
								  <fmt:formatNumber  value="${flightOrderDto.orderFee.rescheduledPrice  }" type="CURRENCY"/>
							  </span>
						</li>
						<li class="fix">
							<span>票面差价</span><span class="xuxian"></span>
							<span class="detailed_spanR">
								  <fmt:formatNumber  value="${flightOrderDto.orderFee.upgradePrice  }" type="CURRENCY"/>
							  </span>
						</li>
						<li class="fix">
							<span>税费差价</span><span class="xuxian"></span>
							<span class="detailed_spanR">
								  <fmt:formatNumber  value="${flightOrderDto.orderFee.departureTax + flightOrderDto.orderFee.fuelTax }" type="CURRENCY"/>
						  </span>
						</li>
					</c:if>

					<c:if test="${flightOrderDto.orderType eq 'REFUND_TICKET_REPURCHASE'}">
						<li class="fix">
							<span>新票票价</span><span class="xuxian"></span>
							<span class="detailed_spanR">
								  <fmt:formatNumber  value="${flightOrderDto.orderFee.salePrice  }" type="CURRENCY"/>
							  </span>
						</li>
						<li class="fix">
							<span>新票机建</span><span class="xuxian"></span>
							<span class="detailed_spanR">
								  <fmt:formatNumber  value="${flightOrderDto.orderFee.departureTax  }" type="CURRENCY"/>
							  </span>
						</li>
						<li class="fix">
							<span>新票燃油</span><span class="xuxian"></span>
							<span class="detailed_spanR">
							  	<fmt:formatNumber  value="${flightOrderDto.orderFee.fuelTax  }" type="CURRENCY"/>
							  </span>
						</li>
						<li class="fix">
							<span>原票退款</span><span class="xuxian"></span>
							<span class="detailed_spanR">
							  	<fmt:formatNumber  value="${flightOrderDto.orderFee.refundPrice  }" type="CURRENCY"/>
							  </span>
						</li>
					</c:if>
				</ul>
				<c:if test="${hasInsuranceOrder }">
					<div class="adult fix">
						<span class="l">保险</span><span class="xuxian"></span><span class="detailed_spanR">¥${insuranceTotalPrice }
					</div>
				</c:if>
				<c:if test="${flightOrderDto.orderFee.servicePrice > 0 }">
					<div class="adult fix">
						<span class="l">服务费</span><span class="xuxian"></span><span class="detailed_spanR">¥${flightOrderDto.orderFee.servicePrice }
	                      <a href="javascript:;">x</a>${fn:length(flightOrderDto.passengers)}</span>
					</div>
				</c:if>
			</div>
			<div class="total_order fix">
				<div>
					<p class="total_order1">供应商结算价</p>
					<p class="total_order3">¥<b class="total_order4" >${flightOrderDto.orderFee.totalSettlePrice  }</b></p>
				</div>
			</div>
		</div>
	</div>
</div>
<div>
	<form action="pay-channel.html" method="get" id="payForm">
		<input type="hidden"  name="orderIds"  value=${flightOrderDto.id }>
	</form>
</div>
<script src="resource/js/order/flightOrderDetail.js?version=${globalVersion}"></script>
<script type="text/javascript">
    $(function(){
        /* //订单创建时间
        var createOrderdate = new Date('${flightOrderDto.createDate}');
	//出发时间
	var fromDate = new Date('${flightOrderDto.orderFlights[0].fromDate}');
	//到达时间
	var toDate = new Date('${flightOrderDto.orderFlights[0].toDate}');
	var day = date.diffDay(date.format(createOrderdate,"yyyy-MM-dd"),date.format(fromDate,"yyyy-MM-dd"));
	if(day > 0){
		$("#difference").text("提前"+day+"天预订");
	}else{
		$("#difference").text("未提前预订");
	} */

        var orderShowStatus = '${flightOrderDto.orderShowStatus}';

        if(orderShowStatus=='WAIT_PAYMENT' || orderShowStatus=='WAIT_AUTHORIZE' || orderShowStatus=='AUTHORIZE_RETURN' ){
            $("#stage1").addClass("oProgressItemActive");

        }
        if(orderShowStatus=='IN_ISSUE' || orderShowStatus=='IN_CHANGE'){
            $("#stage1").addClass("oProgressItemActive");
            $("#stage2").addClass("oProgressItemActive");
        }
        if(orderShowStatus=='ISSUED' || orderShowStatus=='CHANGED'){
            $("#stage1").addClass("oProgressItemActive");
            $("#stage2").addClass("oProgressItemActive");
            $("#stage3").addClass("oProgressItemActive");
        }

    });

    //计算时间差（天数）
    function dateDifference1(d1,d2){
        var time1 = d1.getTime();
        var time2 = d2.getTime();
        return Math.floor(Math.abs((time1-time2)/(1*24*60*60*1000)));
    }
    //计算时间差（xx小时xx分钟）
    function dateDifference2(d1,d2){
        var time1 = d1.getTime();
        var time2 = d2.getTime();
        var minute = Math.abs((time1-time2)/(60*1000));
        if(minute>60){
            return (minute%60)==0?Math.floor((minute/60))+"h":Math.floor((minute/60))+"h"+(minute%60)+"m";
        }else{
            return minute+"m";
        }
    }

</script>
