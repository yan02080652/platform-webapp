<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>保险列表</title>
<link rel="stylesheet" href="/resource/css/order/qyb.lib.css?version=${globalVersion}">
<%--<script src="http://libs.baidu.com/jquery/1.11.3/jquery.min.js?version=${globalVersion}"></script>--%>
<link rel="stylesheet" href="/resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet"
	href="/resource/css/order/insuranceDetail.css?version=${globalVersion}">
</head>
<body>
	<!--网站主体-->
	<div class="insurance">
		<div class="listbox">

			<div class="listitme">
				<div class="listtext l">
					<div class="listhader">
						<div class="title">
							<p>保险订单</p>
							<span class="triangle"></span>
						</div>
						<div class="monery">
							<span>CNY</span> <span class="big">0</span>
						</div>
					</div>
					<c:if test="${not empty insuranceInfos }">
						<c:forEach items="${insuranceInfos }" var="insuranceInfo">
							<div class="listcontent">
								<ul>
									<li class="itme">
										<div class="itmehader ovh">
											<h4 class="itmehadertitle">${insuranceInfo.name }</h4>
											<input type="hidden" value="${insuranceInfo.salePrice }">
											<span class="insurance_count">(0份)</span>
										</div>
										<div class="itmecontent">
											<ol>
												<c:if test="${not empty insuranceOrderDtos }">
													<c:forEach items="${insuranceOrderDtos}"
														var="insuranceOrderDto">
														<c:if
															test="${insuranceInfo.productId == insuranceOrderDto.productId }">
															<input name="insuranceOrderDtoId" type="hidden" value="${insuranceOrderDto.id }">
															<input name="insuranceOrderDtoStatus" type="hidden" value="${insuranceOrderDto.status }">
															
															<c:if
																test="${not empty insuranceOrderDto.insuranceOrderDetails }">
																<c:forEach
																	items="${insuranceOrderDto.insuranceOrderDetails }"
																	var="insuranceOrderDetail">
																	<li class="ovh">
																		<div>${insuranceOrderDetail.insuredName }</div>
																		<input name="orderStatus" type="hidden" value="${insuranceOrderDetail.status}">
																		<div>${insuranceOrderDetail.status.message }</div>
																		<div>
																			<span class="gary">保单号:</span><span>${insuranceOrderDetail.policyNo }</span>
																		</div>
																		<%--<c:if test="${insuranceOrderDetail.status == 'ISSUED' }">
																			<div class="last">
																				<a href="/insurance/downLoadInsuranceOrder.html?policyId=${insuranceOrderDetail.id }">下载电子保单</a>
																			</div>
																		</c:if>--%>
																	</li>
																</c:forEach>
															</c:if>	
														</c:if>
													</c:forEach>
												</c:if>
											</ol>
										</div>
										<div class="detailtext">
											<p>${insuranceInfo.description }</p>
										</div>
									</li>
								</ul>
							</div>
						</c:forEach>
					</c:if>
				</div>
					<%--<c:choose>
						<c:when test="${insuranceOrderDtos[0].status eq 'WAIT_PAYMENT'}">
							<div class=" listbut r">
								<div class="success">
									<img src="/resource/common/images/shields.png" alt="">
								</div>
								<div class="notpaid" onclick="payForInsurance()" style='cursor: pointer'>支付</div>
							</div>
						</c:when>
						
						<c:otherwise>
							<div class=" listbut r">
			                    <div class="success" style="display: block">
			                        <img src="/resource/common/images/shields.png" alt="" >
			                    </div>
			                    <div class="notpaid" style="display: none"> 支付</div>
			                </div>
						</c:otherwise>
					</c:choose>--%>
				<div class="qingchu"></div>
			</div>
		</div>
	</div>
	<div id="submitSuccess">
	<form action="pay-channel.html" method="get" name="sbumitSuccessForm">
		<input type="hidden" name="orderIds" value="${orderIds }">
	</form>
</div>

</body>
<script src="/resource/js/order/iusuranceDetails.js?version=${globalVersion}"></script>
</html>