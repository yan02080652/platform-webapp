<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="result">
	<table class="table orderTable">
		<thead>
			<tr>
				<th class="tipL">退票单号</th>
				<th>原订单号</th>
				<th>退单情况</th>
				<th>退单人</th>
				<th>退单时间</th>
				<th>退款明细</th>
				<th>供应商应退额</th>
				<th>状态</th>
				<th>结算状态</th>
				<th class="tipR">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="refundOrder">
				<tr>
					<td class=" tipL" >${refundOrder.id}</td>
					<td style="text-align:center;">${refundOrder.orderId}</td>
					<td style="text-align:center;">
					    <span>${refundOrder.refundInfo.fromTime }</span><br>
						<span>${refundOrder.refundInfo.aim}</span>
					</td>
					<td style="text-align:center;max-width:110px;">${refundOrder.refundInfo.refundName}</td>
					<td style="text-align:center;width:110px;"><fmt:formatDate value="${refundOrder.applyTime}" pattern="yyyy-MM-dd HH:mm" /></td>
					<td style="text-align:center;">
					  
					    <span>退款金额:${refundOrder.refundTotalAmount }</span><br>
					    <c:if test="${refundOrder.status  eq 'REFUNDED' }">
					      <span>
					       <c:if test="${not empty channelMap[refundOrder.refundChannel] and refundOrder.refundTotalAmount > 0}">
					          ${channelMap[refundOrder.refundChannel]}: ${refundOrder.refundTotalAmount }
					       </c:if>      
					      </span>
					      <span class="status">${refundOrder.status.message}</span>
					    </c:if>
					    
					</td>
					<td style="text-align:center;">${refundOrder.supperRefundAmount * -1}</td>
					<td class="status" style="text-align:center;">${refundOrder.status.message }</td>
					
					<td style="text-align:center;">
					    <c:if test="${refundOrder.billId != null}">已结算</c:if>
					    <c:if test="${refundOrder.billId == null}">未结算</c:if>
					</td>
					
					 <td class="lastTd" style="text-align:center;">
					   <c:choose>
						  <c:when test="${refundOrder.orderCode==flightCode}">
			              	  <a class="danhao" href='javascript:;' onclick="openRefundDetail('order/refundFlightOrderDetail/${refundOrder.id }')" attr="order/refundFlightOrderDetail/${refundOrder.id }">查看</a>
						  </c:when>
						 
						  <c:when test="${refundOrder.orderCode==hotelCode}">
					           <a href='javascript:;' onclick="openRefundDetail('order/refundHotelDetail/${refundOrder.id }')" attr="order/refundHotelDetail/${refundOrder.id}" class="danhao">查看</a>
						  </c:when>
						 
						   <c:when test="${refundOrder.orderCode==trainCode}">
						       <a href='javascript:;' onclick="openRefundDetail('order/refundTrainDetail/${refundOrder.id }')" attr="order/refundTrainDetail/${refundOrder.id}" class="danhao">查看</a>
						  </c:when>
						  
						   <c:when test="${refundOrder.orderCode==generalCode}">
						       <a href='javascript:;' onclick="openRefundDetail('order/refundGeneralDetail/${refundOrder.id }')" attr="order/refundGeneralDetail/${refundOrder.id}" class="danhao">查看</a>
						  </c:when>						  
						  <c:otherwise>
						           
						  </c:otherwise>
						</c:choose>
                     </td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<div>
</div>
<div class="pagin" style="margin-bottom: 15px">
<jsp:include page="../common/pagination_ajax.jsp?callback=reloadRefundOrder"></jsp:include> 
</div>
<script type="text/javascript">
 
</script>