<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/trainOrderDetail.css?version=${globalVersion}">
<div >
	<div class="main fix">
		<div class="oContentL">
			<%-- <p class="nav">
				<a href="javascript:void(0);"><span class="first">订单</span></a> 
				<a href="order/trainOrder"><span class="sub">火车票订单</span></a> 
				<a href="order/trainDetail/${trainOrderDto.id}"><span class="three">订单详情</span></a>
			</p> --%>
			<h2 class="oTitle">订单详情</h2>
			<div class="fix oNumber">
				<div class="l">
					<p>
						订单号：<span id="id">${trainOrderDto.id}</span>
					</p>
					
				</div>
			 <%-- <c:if test="${headUserInfo.id eq trainOrderDto.userId }">
				<div class="r">
					<button class="cancel">取消订单</button>
					<button class="save" >立即支付</button>
				</div>
			</c:if> --%>
			</div>
			<div class="oState">
				<h3 class="oStateTitle">${trainOrderDto.orderShowStatus.message}</h3>
				<div class="fix mb20">
					<div class="l">
						<p class="oCount">
							<c:if test="${trainOrderDto.orderShowStatus eq 'WAIT_PAYMENT' }">
								<span>请您在提交订单后<span id="timer"></span>分钟内完成支付，超时订单将被关闭</span>
							</c:if>
						</p>
					</div>
				</div>
				<div class="oProgress fix">
					<div class="oProgressItem oProgressItemActive">
						<p>提交订单</p>
						<span><fmt:formatDate value="${trainOrderDto.createDate}" pattern="yyyy年MM月dd日   HH:mm:ss"/></span>
					</div>
					<div class="oProgressItem ${trainOrderDto.paymentStatus eq 'PAYMENT_SUCCESS'?'oProgressItemActive':'' }">
						<p>支付</p>
						<span><fmt:formatDate value="${trainOrderDto.paymentDate}" pattern="yyyy年MM月dd日   HH:mm:ss"/></span>
					</div>
					<div class="oProgressItem ${trainOrderDto.orderShowStatus eq 'ISSUED'?'oProgressItemActive':''}">
						<p>出票成功</p>
						<span>
						   <c:if test="${trainOrderDto.orderShowStatus eq 'ISSUED' }">
						     <fmt:formatDate value="${trainOrderDto.issuedDate}" pattern="yyyy年MM月dd日   HH:mm:ss"/>
						   </c:if>
						</span>
					</div>
					<input type="hidden" id="orderShowStatus" value="${trainOrderDto.orderShowStatus}"/>
					<input type="hidden" id="paymentStatus" value="${trainOrderDto.paymentStatus}"/>
					<input value="${trainOrderDto.lastUpdateDate.getTime()}" id="startTime" type="hidden">
				</div>
			</div>
			<div class="oItem">
	             <h3 class="oItemTitle">差旅信息</h3>
	             <div class="oItemLi">
					 <style>
						 .oItemLi .whole-line{
							 white-space: nowrap;
							 display: flex;
						 }
						 .oItemLi .whole-line span{
							 vertical-align: top;
							 white-space: normal;
							 word-break: break-all;
							 display: inline-block;
						 }
						 .oItemLi .whole-line span:last-child{
							 flex: 1;
						 }
					 </style>
					 <c:if test="${trainOrderDto.travelType eq 'PERSONAL'}">
						<p>
							<span class="oItemLiLabel">出行性质</span>
							<span>因私</span>
						</p>
					 </c:if>
					 <c:if test="${trainOrderDto.travelType eq 'BUSINESS'}">
						 <p>
							 <span class="oItemLiLabel">出行性质</span>
							 <span>因公</span>
						 </p>
						 <p>
							 <span class="oItemLiLabel">差旅计划</span>
							 <span>${trainOrderDto.travelPlanNo}</span>
						 </p>
						 <p><span class="oItemLiLabel">出行类别</span><span>${travel.itemTxt}</span></p>
						 <p class="whole-line">
							 <span class="oItemLiLabel">出差事由</span>
							 <span>${trainOrderDto.travelReason}</span>
						 </p>
						 <p><span class="oItemLiLabel">费用类型</span><span>${orgPath}</span></p>
					 </c:if>
	             </div>
			</div>
				<div class="oItem">
					<h3 class="oItemTitle">付款信息</h3>
					<c:if test="${personalPayAmount ne null}">
						<div class="oItemLi-cdy">个人支付</div>
						<div class="oItemLi oItemLiIndent">
							<p><span class="oItemLiLabel">支付金额</span><span> ¥${personalPayAmount}</span></p>
							<p><span class="oItemLiLabel">支付时间</span><span> <fmt:formatDate value="${trainOrderDto.paymentDate}" pattern="yyyy/MM/dd  HH:mm:ss"/></span></p>
						</div>
						</br>
					</c:if>

					<c:if test="${trainOrderDto.bpTotalAmount ne null and trainOrderDto.bpTotalAmount gt 0}">
						<div class="oItemLi-cdy">企业账户</div>
						<div class="oItemLi oItemLiIndent">
							<p><span class="oItemLiLabel">支付金额</span><span> ¥${trainOrderDto.bpTotalAmount}</span></p>
							<p><span class="oItemLiLabel">支付时间</span><span> <fmt:formatDate value="${trainOrderDto.paymentDate}" pattern="yyyy/MM/dd  HH:mm:ss"/></span></p>
							<p><span class="oItemLiLabel">交易编码</span><span> ${trainOrderDto.paymentPlanNo}</span></p>
						</div>
					</c:if>
				</div>

			<%--<div class="oItem">
				<h3 class="oItemTitle">付款信息</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">付款方式</span><span>${payItem.payMethod}</span>
					</p>
					<p>
						<span class="oItemLiLabel">交易编号</span><span>${payItem.tradeId}</span>
					</p>
					<p>
						<span class="oItemLiLabel">出差事由</span><span>${trainOrderDto.travelReason}</span>
					</p>
				</div>
			</div>--%>
			<div class="oItem">
				<h3 class="oItemTitle">乘客</h3>
				<c:forEach items="${trainOrderDto.passengerList }" var="passenger">
					<div class="oItemLi">
						<p>
							<span class="oItemLiLabel">姓名</span><span>${passenger.name }</span>
						</p>
						<p>
							<span class="oItemLiLabel">证件</span><span>${passenger.credentialType.msg} ${passenger.credentialNo }</span>
						</p>
						<p>
							<span class="oItemLiLabel">类型</span><span>${passenger.ticketType.msg }</span>
						</p>
						<p>
						   <c:if test="${not empty passenger.seatDetail}">
						      <span class="oItemLiLabel">席位</span>
						      <span class="oStateSub">${passenger.seatDetail}
								  <c:choose>
									  <c:when test="${trainOrderDto.seatType eq 'NOSEAT'}">
										  无座
									  </c:when>
									  <c:otherwise>
										  <c:if test="${passenger.isNoSeat}">
											  无座
										  </c:if>
										  <c:if test="${passenger.isNoSeat == false}">
											  ${trainOrderDto.seatTypeValue}
										  </c:if>
									  </c:otherwise>
								  </c:choose>
						          ${passenger.passengerShowStatus}
						      </span>
						   </c:if>
						</p>
					</div>
				</c:forEach>
			</div>
			<div class="oItem">
				<h3 class="oItemTitle">联系人</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">姓名</span><span>${trainOrderDto.contactor.name }</span>
					</p>
					<p>
						<span class="oItemLiLabel">手机</span><span>${trainOrderDto.contactor.mobile }</span>
					</p>
					<p>
						<span class="oItemLiLabel">邮箱</span><span>${trainOrderDto.contactor.email }</span>
					</p>
				</div>
			</div>
		</div>
		<div class="oContentR">
			<div class="right_top">
				<p class="top1">
					<span class="Rtop_time"><fmt:formatDate value="${trainOrderDto.fromTime }" pattern="MM-dd" /></span> 
					<span class="Rtop_week"><fmt:formatDate value="${trainOrderDto.fromTime }" pattern="E" /></span>
					<span class="Rtoop_place">${trainOrderDto.fromStation }-${trainOrderDto.arriveStation }</span> 
					<span class="Rtoop_rank">${trainOrderDto.seatTypeValue }</span>
				</p>
			</div>
			<div class="right_top2">
				<p>
					<span class="Rtop2_timeL"><fmt:formatDate value="${trainOrderDto.fromTime }" pattern="HH:mm" /></span> 
					<span class="Rtop2_line"></span>
					<span class="Rtop2_center" id="costTime"></span> 
					<span class="Rtop2_line"></span>
					<span class="Rtop2_timeR"><fmt:formatDate value="${trainOrderDto.arriveTime }" pattern="HH:mm" /></span>
				</p>
				<p>
					<span class="Rtop2_start">${trainOrderDto.fromStation }</span> 
					<span class="Rtop2_company">${trainOrderDto.trainCode }</span>
					<span class="Rtop2_end">${trainOrderDto.arriveStation }</span>
				</p>
                <br/>
				<c:if test="${trainOrderDto.orderType eq 'grabTickets' and trainOrderDto.issuedDate eq null}">
					<c:forEach items="${trainOrderDto.grabTaskList}" var="grabInfo">
                        <c:if test="${grabInfo.mainInfo eq false}">
                            <p>
                                <span>备选车次:&nbsp;
                                    <c:forEach items="${grabInfo.trainCodeArray}" var="code" varStatus="statu">
                                        <c:if test="${statu.index gt 0}">
                                            /&nbsp;${code}
                                        </c:if>
                                        <c:if test="${statu.index eq 0}">
                                            ${code}
                                        </c:if>
                                    </c:forEach>
                                </span>
                            </p>
                            <p>
                                <span>备选坐席:&nbsp;
                                    <c:forEach items="${grabInfo.seatTypeArray}" var="seat" varStatus="statu">
                                        <c:if test="${statu.index gt 0}">
                                            /&nbsp;${seat.msg}
                                        </c:if>
                                        <c:if test="${statu.index eq 0}">
                                            ${seat.msg}
                                        </c:if>
                                    </c:forEach>
                                </span>
                            </p>
                            <p>
                                <span>备选日期:&nbsp;
									<c:forEach items="${grabInfo.ticketTimeArray}" var="time" varStatus="statu">
                                        <c:if test="${statu.index gt 0}">
                                            /
                                            <fmt:formatDate value="${time}" pattern="MM-dd"/>
                                        </c:if>
                                        <c:if test="${statu.index eq 0}">
                                            <fmt:formatDate value="${time}" pattern="MM-dd"/>
                                        </c:if>
                                    </c:forEach>
								</span>
                            </p>
                        </c:if>
                    </c:forEach>
				</c:if>
			</div>
			<div class="detailed_box">
				<div class="detailed_show">
					<p class="fix">
						<span>费用明细</span>
						(<span>${ticketDetail}</span>)
					</p>
				</div>
                <c:if test="${trainOrderDto.orderType eq 'grabTickets'}">
					<div class="cost-detail" id="cost-detail">
                        <c:if test="${not empty trainOrderDto.paymentDate}">
						<div>
							<div>单人支付金额</div>
							<div>¥${amount}</div>
						</div>
						<div class="gary">
							<div>单人票价</div>
							<div>¥${amount - trainOrderDto.servicePrice / trainOrderDto.passengerList.size() }</div>
						</div>
						<div class="gary">
							<div>单人服务费</div>
							<div>¥${trainOrderDto.servicePrice / trainOrderDto.passengerList.size()}</div>
						</div>
                    </c:if>
                        <c:if test="${not empty trainOrderDto.issuedDate}">
						<div>
							<div>单人出票票价</div>
							<div>¥ ${trainOrderDto.totalSettlePrice/trainOrderDto.passengerList.size()}</div>
						</div>
						<div>
							<div>单人退回差额</div>
							<div>¥ ${refundBalance}</div>
						</div>
                    </c:if>
					</div>
					<br/>
				</c:if>
                <c:if test="${trainOrderDto.orderType eq 'ordinary' or empty trainOrderDto.issuedDate && trainOrderDto.orderType eq 'grabTickets'}">
					<div class="cost-detail" id="cost-detail">
						<div>
							<div>单人票价</div>
							<div>¥${trainOrderDto.ticketPrice }</div>
						</div>
						<div>
							<div>单人服务费</div>
							<div>¥${trainOrderDto.servicePrice / trainOrderDto.passengerList.size()}</div>
						</div>
					</div>
					<br/>
				</c:if>
                <div class="cost-detail">
                    <div>
                        <div class="total-money">订单总额</div>
                        <div>¥ <span class="red">${(amount - refundBalance) * trainOrderDto.passengerList.size()}</span></div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
<form method="post" id="hiddenForm" action="train/order/toRefund.html">
	  <input type="hidden" name="id" value="${trainOrderDto.id}"/>
</form>
<script src="resource/js/order/trainOrderDetail.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(function(){
	$("#costTime").text(dateDifference('${trainOrderDto.costTime}'));
});

//计算时间差（传入的是分钟数）
function dateDifference(minute){
	if(minute>60){
		return (minute%60)==0?Math.floor((minute/60))+"h":Math.floor((minute/60))+"h"+(minute%60)+"m";
	}else{
		return minute+"m";
	}
}
</script>
