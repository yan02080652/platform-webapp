<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/refundHotelDetail.css?version=${globalVersion}">

<div class="main fix">
    <div class="contentBox fix">
        <div class="oContentL">
            <h2 class="oTitle">订单详情</h2>
            <div class="fix oNumber">
                <div class="l">
                    <p>订单号：<span>${order.id }</span></p>
                </div>
                <div class="r">
                <!--<button class="btn btn_save">撤销退票</button>-->
                </div>
            </div>
            <div class="oState">
                <h3 class="oStateTitle">提交中</h3>
                <div class="fix oCount_box">
                    <div class="l">
                        <p class="oCount">实际退款金额¥<span>100.00</span>，如有问题请联系客服</p>
                    </div>
                    <!--<div class="r rule">-->
                    <!--<span>提前两天预订</span>-->
                    <!--<span>经济舱7折</span>-->
                    <!--<span>时段内最低价</span>-->
                    <!--</div>-->
                </div>
                <div class="oProgress fix">
                    <div class="oProgressItem oProgressItemActive">
                       <p>提交申请</p>
                       <span><fmt:formatDate value="${refundOrder.applyTime }" pattern="yyyy年MM月dd日  HH:mm:ss"/></span>
                    </div>
                    <div class="oProgressItem ${refundOrder.status eq 'REFUNDING' or refundOrder.status eq  'REFUNDED' ? 'oProgressItemActive':'' }">
                        <p>退款中</p>
                        <span><fmt:formatDate value="${refundOrder.applyTime }" pattern="yyyy年MM月dd日  HH:mm:ss"/></span>
                    </div>
                    <div class="oProgressItem ${refundOrder.status eq 'REFUNDED'?'oProgressItemActive':'' }">
                        <p id="last">已退款</p>
                        <span><fmt:formatDate value="${refundOrder.finishTime }" pattern="yyyy年MM月dd日  HH:mm:ss"/></span>
                    </div>
                </div>
                <!--<div class="handle fix">-->
                <!--<div class="handleL">-->
                <!--处理环节-->
                <!--</div>-->
                <!--<div class="handleR">-->
                <!--<p class="handleRTitle">处理消息</p>-->
                <!--<div class="handleRLi">-->
                <!--<p class="">您的退票单FR1253656812财务已退款,请您注意查收</p>-->
                <!--<p class="handleRName"><span>操作员：</span><span>误会潮</span><span>(2015-08-07&nbsp;&nbsp;07:36:00)</span></p>-->
                <!--</div>-->
                <!--<div class="handleRUl">-->
                <!--<div class="handleRLi">-->
                <!--<p class="">您的退票单FR1253656812财务已退款,请您注意查收</p>-->
                <!--<p class="handleRName"><span>操作员：</span><span>误会潮</span><span>(2015-08-07&nbsp;&nbsp;07:36:00)</span></p>-->
                <!--</div>-->
                <!--<div class="handleRLi">-->
                <!--<p class="">您的退票单FR1253656812财务已退款,请您注意查收</p>-->
                <!--<p class="handleRName"><span>操作员：</span><span>误会潮</span><span>(2015-08-07&nbsp;&nbsp;07:36:00)</span></p>-->
                <!--</div>-->
                <!--<div class="handleRLi">-->
                <!--<p class="">您的退票单FR1253656812财务已退款,请您注意查收</p>-->
                <!--<p class="handleRName"><span>操作员：</span><span>误会潮</span><span>(2015-08-07&nbsp;&nbsp;07:36:00)</span></p>-->
                <!--</div>-->
                <!--</div>-->
                <!--<div class="see_more">-->
                <!--查看更多-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
            </div>
            <div class="oItem">
                <h3 class="oItemTitle">退房信息</h3>
                <div class="oItemLi">
                    <p><span class="oItemLiLabel">乘客</span>
                    	<c:forEach items="${customers }" var="customer" varStatus="status">
		                    <span>${customer.name }</span>
                    	</c:forEach>
                    </p>
                    <p><span class="oItemLiLabel">原因</span><span>${refundOrder.reason }</span></p>
                </div>
            </div>
            <div class="oItem">
                <h3 class="oItemTitle">联系人</h3>
                <div class="oItemLi">
                    <p><span class="oItemLiLabel">姓名</span><span>${refundOrder.contactName }</span></p>
                    <p><span class="oItemLiLabel">手机</span><span>${refundOrder.contactPhone }</span></p>
                    <p><span class="oItemLiLabel">邮箱</span><span>${refundOrder.contactEmail }</span></p>
                </div>
            </div>
        </div>
        <!--右侧内容-->
        <div class="oContentR">
			<div class="rightTitle">
				<p>您选择的酒店</p>
			</div>
			<div class="rightHotel fix">
				<div class="l">
					<img class="hotelImg" src="${order.hotelImgUrl }" alt="">
				</div>
				<div class="desc">
					<p class="hotelName">${order.hotelName }</p>
					<p class="hotelAddress">${order.address }</p>
				</div>
			</div>
			<div class="hotelDetail">
				<p>
	                <label> 房型 ：</label><span>${order.roomName }</span>
	            </p>
	            <p>
	                <label> 床型 ：</label><span>${order.room.bedType }</span>
	            </p>
	            <p>
	                <label> 早餐 ：</label><span>
	                	<c:choose>
		                	<c:when test="${order.nightlyRates[0].breakfastCount eq '0'}">无早</c:when>
							<c:when test="${order.nightlyRates[0].breakfastCount eq '1'}">单早</c:when>
							<c:when test="${order.nightlyRates[0].breakfastCount eq '2'}">双早</c:when>
							<c:when test="${order.nightlyRates[0].breakfastCount eq '3'}">三早</c:when>
							<c:when test="${order.nightlyRates[0].breakfastCount eq '4'}">四早</c:when>
							<c:otherwise>无早</c:otherwise>
						</c:choose>
	                </span>
	            </p>
	            <p>
	                <label> 宽带 ：</label>
	                <span>
	                	<c:if test="${order.room.broadnet eq 0 }">无</c:if>
						<c:if test="${order.room.broadnet eq 1 }">免费宽带</c:if>
						<c:if test="${order.room.broadnet eq 2 }">收费宽带</c:if>
						<c:if test="${order.room.broadnet eq 3 }">免费WIFI</c:if>
						<c:if test="${order.room.broadnet eq 4 }">收费WIFI</c:if>
	                </span>
	            </p>
	            <p>
	                <label> 面积 ：</label><span>${order.room.area }</span>
	            </p>
	             <p>
	                <label> 楼层 ：</label><span>${order.room.floor }</span>
	            </p>
	           <!--  <p>
	                <label> 加床 ：</label><span>不可加床(?)</span>
	            </p> -->
				
			</div>
			<div class="total_order fix">
				<p>
					<span>共 <span>${order.roomNum }</span>间<span>${order.stayDay }</span>晚
					</span> <span class="r">服务费：<span>0</span>元/间夜
					</span>
				</p>
				<div class="feeDetail">
					<p>
						<span>房费合计</span> <span class="r"><span>¥</span>${order.totalPrice }</span>
					</p>
					<p>
						<span>服务费合计</span> <span class="r"><span>¥</span>${order.extraFee }</span>
					</p>
				</div>
				<p>
					<span class="total_order1">订单总额</span><span class="total_order2">¥<fmt:formatNumber value="${order.totalPrice + order.extraFee}" maxFractionDigits="2"/></span>
				</p>
			</div>

			<div class="hideHover">
				<c:forEach items="${order.nightlyRates }" var="bean" varStatus="status">
					<div class="detailItem">
						 <fmt:formatDate value="${bean.checkInDate }" pattern="E" var="dateOfweek"/>
	                     <p class="itemD"><span><fmt:formatDate value="${bean.checkInDate }" pattern="MM-dd"/></span> （<span>${fn:substring(dateOfweek,2,3) }</span>）</p>
	                     <p class="itemPrice">¥ <span>${bean.dayRate }</span></p>
	                     <p class="itemB">
	                     	<c:choose>
			                	<c:when test="${bean.breakfastCount eq '0'}">无早</c:when>
								<c:when test="${bean.breakfastCount eq '1'}">单早</c:when>
								<c:when test="${bean.breakfastCount eq '2'}">双早</c:when>
								<c:when test="${bean.breakfastCount eq '3'}">三早</c:when>
								<c:when test="${bean.breakfastCount eq '4'}">四早</c:when>
								<c:otherwise>无早</c:otherwise>
							</c:choose>
	                     </p>
	                 </div>
				</c:forEach>
         	</div>
		</div>
    </div>
</div>
    
    
<script src="resource/js/order/refundHotelDetail.js?version=${globalVersion}"></script>

