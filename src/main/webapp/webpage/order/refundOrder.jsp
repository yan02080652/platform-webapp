<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/flightOrderList.css?version=${globalVersion}">
	<!--右侧主体内容-->
	<div class="main ml10">
		<div class="sel">
		   <div class="mb20" id="sel3">
				<label for="begin">退单时间：</label> <input type="text" id="begin"
					class="date" name="begin" onclick="WdatePicker()"> - <input
					id="end" class="date" type="text" onclick="WdatePicker()"> 最近： <span
					class="sp-date" id="oneMonth" data-value="0">一月</span> <span  class="sp-date" id="oneYear" data-value="1">一年</span>
				<input type="checkbox" id="compPay" checked="checked" class="compPayCheck" style="margin-left: 25px;position:relative;top: 8px;"/><label for="compPay">仅公司支付</label>
				<label style="margin-left: 30px;">出行性质：</label> 
					<span class="sp-traveltype selected" status="all" data-value="" >全部</span>
            		<span class="sp-traveltype" status="business" data-value="BUSINESS">因公</span>
            		<span class="sp-traveltype" status="personal" data-value="PERSONAL">因私</span>
		   </div>
		   <input type="hidden" value="${state}" id="state"/>
			<div class="mb20" id="sel1">
				<label>退款类型：</label> 
					<span class="selAll" id="allOrderBizType" data-value="0">全部</span> 
					<c:forEach items="${orderBizType}" var="ss">
						<span data-value="${ss.code}">${ss.message }</span> 
					</c:forEach>
			</div>
			<div class="mb20" id="sel2">
				<label>状态：</label> 
					<span class="selAll" id="allRefundStatus" data-value="0">全部</span> 
					<c:forEach items="${refundStatus}" var="ss">
						<span data-value="${ss}">${ss.message }</span> 
					</c:forEach>
			</div>
			<div class="mb20">

				<jsp:include page="../common/businessSelecter.jsp"></jsp:include>

				<label for="keyWords" style="width: 60px;margin-left: 15px;">关键词：</label> <input id="keyWords"
					class="keyWords" style="width: 280px;" type="text" placeholder="请输入原订单号、预订人、供应商流水号">
				<span id="search" class="button" onclick="reloadRefundOrder()">搜索</span>
				<span id="exportButton" class="button" onclick="reloadExportOrder()" style="margin-left: 30px;">导出</span>
			</div>
		</div>
		<div id="refundOrderTable">
		</div>
	</div>
<script src="resource/js/order/refundOrderList.js?version=${globalVersion}"></script>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
