<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/flightOrderDetail.css?version=${globalVersion}">
<style>
.oProgressItem {
	width: 175px;
}
</style>
<div class="main">
	<div class="contentBox fix">
		<div class="oContentL">
			<h2 class="oTitle">订单详情</h2>
			<div class="fix oNumber">
				<div class="l">
					<p>
						订单号：<span>${generalOrder.id }</span>
					</p>
				</div>
			</div>
			<div class="oState">
				<h3 class="oStateTitle">${generalOrder.orderShowStatus.message }</h3>
				<div class="oProgress fix">
					<div class="oProgressItem" id="stage1">
						<p>提交需求</p>
						<span><fmt:formatDate value="${generalOrder.createDate }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</div>
					<div class="oProgressItem" id="stage2">
						<p>线下报价</p>
						<span><fmt:formatDate value="${generalOrder.offerDate }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</div>					
					<div class="oProgressItem" id="stage3">
						<p>付款成功</p>
						<span><fmt:formatDate value="${generalOrder.paymentDate }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</div>
					<div class="oProgressItem" id="stage4">
						<p>交付产品 </p>
						<span><fmt:formatDate value="${generalOrder.finishDate }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</div>
				</div>
			</div>
		 	<div class="oItem">
				<h3 class="oItemTitle">需求信息</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">服务类型</span>
						<span>${generalOrder.serviceName }</span>
					</p>
					<p>
						<span class="oItemLiLabel">出行人数</span>
						<span>${generalOrder.travellerCount }(人)</span>
					</p>
					<p>
						<span class="oItemLiLabel">行程时间</span>
						<span>${generalOrder.travellerDate }</span>
					</p>
					<p>
						<span class="oItemLiLabel">实际行程时间</span>
						<span>
							<fmt:formatDate value="${generalOrder.travelBeginDate }" pattern="yyyy-MM-dd"/> 至
							<fmt:formatDate value="${generalOrder.travelEndDate}" pattern="yyyy-MM-dd" />
						</span>
					</p>
					<p>
						<span class="oItemLiLabel">行程地点</span>
						<span>${generalOrder.destination }</span>
					</p>
					<p>
						<span class="oItemLiLabel">出行性质</span>
						<span>${generalOrder.travelType.msg }</span>
					</p>
					<c:if test="${generalOrder.travelType == 'BUSINESS'}">
					<p>
						<span class="oItemLiLabel">出行类别</span>
						<span>${travel }</span>
					</p>
					<p>
						<span class="oItemLiLabel">费用归属</span>
						<span>${orgPath }</span>
					</p>
					</c:if>
					<p>
						<span class="oItemLiLabel">详细描述</span>
						<span>${generalOrder.descr }</span>
					</p>
				</div>
			</div> 
			<div class="oItem">
				<h3 class="oItemTitle">产品信息</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">描述</span><span>${generalOrder.serviceDescr }</span>
					</p>
					<p>
						<span class="oItemLiLabel">附件</span><a style="color: #46a1bb;cursor:pointer;" onclick="fileDownLoad('${generalOrder.fileName}','http://${SYS_CONFIG['imgserver'] }/${generalOrder.fileUrls}')" >${generalOrder.fileName }</a><span></span>
					</p>
					<p>
						<span class="oItemLiLabel">客户结算价</span><span>${generalOrder.totalOrderPrice }</span>
					</p>
					<p>
						<span class="oItemLiLabel">供应商</span><span>${generalOrder.providerName }</span>
					</p>
					<p>
						<span class="oItemLiLabel">供应商订单号</span><span>${generalOrder.providerOrderNo }</span>
					</p>
					<p>
						<span class="oItemLiLabel">供应商结算价</span><span>${generalOrder.totalSettlePrice }</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div>
	<form action="pay-channel.html" method="get" id="payForm">
		<input type="hidden"  name="orderIds"  value=${generalOrder.id }>
	</form>
</div>

<script type="text/javascript">
$(function(){
	var orderShowStatus = '${generalOrder.orderShowStatus}';
 	if(orderShowStatus=='TO_BE_FEEDBACK' || orderShowStatus=='CANCELED'){
		$("#stage1").addClass("oProgressItemActive");
	}
	if(orderShowStatus=='WAIT_PAYMENT'|| orderShowStatus=='WAIT_AUTHORIZE' || orderShowStatus=='AUTHORIZE_RETURN'){
		$("#stage1").addClass("oProgressItemActive");
		$("#stage2").addClass("oProgressItemActive");
	}
	if(orderShowStatus=='ORDERED'){
		$("#stage1").addClass("oProgressItemActive");
		$("#stage2").addClass("oProgressItemActive");
		$("#stage3").addClass("oProgressItemActive");
	}
	if(orderShowStatus=='ORDER_SUCCESS'){
		$("#stage1").addClass("oProgressItemActive");
		$("#stage2").addClass("oProgressItemActive");
		$("#stage3").addClass("oProgressItemActive");
		$("#stage4").addClass("oProgressItemActive");
	}
	
});

function fileDownLoad(fileName,docUrl){
    window.location.href=docUrl;
}

</script>
