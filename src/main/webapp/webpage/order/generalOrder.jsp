<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/generalOrderList.css?version=${globalVersion}">

	<!--右侧主体内容-->
	<div class="main ml10">
		<div class="sel">
			<div class="mb20" id="sel1">
				<label for="begin">下单时间：</label> 
				<input type="text" id="begin"class="date" name="begin" onclick="WdatePicker()"> 
				<label>-</label> 
				<input id="end" class="date" onclick="WdatePicker()" type="text"> 
				<label>最近：</label> <span class="sp-date" id="oneMonth" data-value="0">一月</span> <span class="sp-date" id="oneYear" data-value="1">一年</span>
				<input type="checkbox" id="compPay" checked="checked" class="compPayCheck" style="margin-left: 25px;position:relative;top: 8px;margin-right: 25px;"/><label for="compPay">仅公司支付</label>
				<label style="margin-left: 30px;">出行性质：</label> 
					<span class="sp-traveltype selected" status="all" data-value="" >全部</span>
            		<span class="sp-traveltype" status="business" data-value="BUSINESS">因公</span>
            		<span class="sp-traveltype" status="personal" data-value="PERSONAL">因私</span>
			</div>
			<div class="mb20" id="sel2">
				<label>状态：</label> 
					<span class="selAll" id="allStatus" data-value="0">全部</span> 
					<c:forEach items="${showStatus }" var="ss">
						<span data-value="${ss }">${ss.message }</span> 
					</c:forEach>
			</div>
			<div class="mb20">
				<label for="issueChannel">供应商：</label> 
				<select id="issueChannel" name="issueChannel" style="width: 100px;height: 24px;">
					<option value="0">全部</opton>
					<c:forEach items="${issueChannels }" var="issueChannel">
						<option value="${issueChannel.code}">${issueChannel.name}</opton>
					</c:forEach>
				</select>
				<jsp:include page="../common/businessSelecter.jsp"></jsp:include>
				<label for="keyWords" style=" margin-left: 20px;width: 60px;">关键词：</label>
				<input id="keyWords" class="keyWords" type="text" placeholder="订单号 、PNR、供应单号">
				<span id="search" class="button" onclick="reloadGeneralOrder()">搜索</span>
				<span id="exportButton" class="button" onclick="reloadExportOrder()" style="margin-left: 30px;">导出</span>
			</div>
		</div>
		<div id="generalOrderTable">
		</div>
	</div>
<script src="resource/js/order/generalOrderList.js?version=${globalVersion}"></script>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
