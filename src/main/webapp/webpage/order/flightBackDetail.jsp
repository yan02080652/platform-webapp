<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/flightBackDetail.css?version=${globalVersion}">
<div class="main">
	<div class="contentBox fix">
		<div class="oContentL">
			<h2 class="oTitle">退票详情</h2>
			<div class="fix oNumber">
				<div class="l">
					<p>
						退票单号：<span>${refundOrderDto.id }</span>
					</p>
				</div>
			</div>
			<div class="oState">
				<h3 class="oStateTitle">${refundOrderDto.status.message }</h3>
				<div class="fix mb20">
					<div class="l">
						<c:if test="${refundOrderDto.status == 'ADUITING' }">
							<p class="oCount">客服将在退票提交后<span>24</span>小时内进行审核</p>
						</c:if>
					</div>
				</div>
				<div class="oProgress fix">
					<div class="oProgressItem <c:if test="${not empty refundOrderDto.applyTime }">oProgressItemActive</c:if>">
						<p>提交申请</p>
						<span><fmt:formatDate value="${refundOrderDto.applyTime }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</div>
					<div class="oProgressItem <c:if test="${not empty refundOrderDto.auditEndTime or not empty refundOrderDto.confirmTime}">oProgressItemActive</c:if>">
						<p>客服审核</p>
						<c:choose>
							<c:when test="${not empty refundOrderDto.auditEndTime}">
								<span><fmt:formatDate value="${refundOrderDto.auditEndTime }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
							</c:when>
							<c:otherwise>
								<span><fmt:formatDate value="${refundOrderDto.confirmTime }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
							</c:otherwise>
						</c:choose>
					</div>
					<div class="oProgressItem <c:if test="${refundOrderDto.status eq 'REFUNDED'and not empty refundOrderDto.confirmTime }">oProgressItemActive</c:if>">
						<p>财务退款</p>
						<c:if test="${refundOrderDto.status eq 'REFUNDED'}">
						    <span><fmt:formatDate value="${refundOrderDto.confirmTime }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
						</c:if>
					</div>
					<div class="oProgressItem <c:if test="${refundOrderDto.status eq 'REFUNDED'and not empty refundOrderDto.finishTime }">oProgressItemActive</c:if>">
						<p>退款完成</p>
                        <c:if test="${refundOrderDto.status eq 'REFUNDED'}">
						    <span><fmt:formatDate value="${refundOrderDto.finishTime }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
                        </c:if>
					</div>
				</div>
			</div>
			<c:if test="${refundOrderDto.status == 'REFUSED_REFUND' }">
				<div class="oItem">
					<h3 class="oItemTitle">拒绝理由</h3>
					<div class="oItemLi">
						<p>
							<span class="oItemLiLabel">原因</span><span>${refundOrderDto.refuseReason }</span>
						</p>
					</div>
				</div>
			</c:if>
			<c:if test="${not empty refundOrderDto.remark}">
				<div class="oItem">
					<h3 class="oItemTitle">拒单退款原因</h3>
					<div class="oItemLi">
						<p>
							<span class="oItemLiLabel">原因</span><span>${refundOrderDto.remark }</span>
						</p>
					</div>
				</div>
			</c:if>
			<div class="oItem">
				<h3 class="oItemTitle">退票信息</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">乘客</span>
						<c:forEach items="${refundOrderDto.orderDto.passengers }" var="passenger">
							<c:if test="${passenger.refundOrderId eq refundOrderDto.id }">
								<span>${passenger.name }</span>&nbsp;&nbsp;
							</c:if>
						</c:forEach>
					</p>
					<p>
						<span class="oItemLiLabel">原因</span><span>${refundOrderDto.reason }</span>
					</p>
				</div>
			</div>
			<div class="oItem">
				<h3 class="oItemTitle">联系人</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">姓名</span><span>${refundOrderDto.contactName }</span>
					</p>
					<p>
						<span class="oItemLiLabel">手机</span><span>${refundOrderDto.contactPhone }</span>
					</p>
					<p>
						<span class="oItemLiLabel">邮箱</span><span>${refundOrderDto.contactEmail }</span>
					</p>
				</div>
			</div>
		</div>
    	 <div class="content_right">
              <div class="right_top1">
                  <p class="top1 fix">
                      <span class="Rtop_time"><fmt:formatDate value="${flightOrderDto.orderFlights[0].fromDate}" pattern="MM-dd"/></span>
                      <span class="Rtop_week"><fmt:formatDate value="${flightOrderDto.orderFlights[0].fromDate}" pattern="E"/></span>
                      <span class="Rtoop_place">${flightOrderDto.orderFlights[0].fromCityName }-${flightOrderDto.orderFlights[0].toCityName }</span>
                  </p>
              </div>
              <div class="rightItem right_top2 fix">
                  <div class="flightIco">
                  	<img  alt="" src="http://${SYS_CONFIG['imgserver'] }/${carrierDto.logo }">
                  </div>
                  <div class="flightName">${carrierDto.nameShort }</div>
                  <div class="flightCode">${flightOrderDto.orderFlights[0].flightNo }</div>
                  <div class="flightType">${aircraftDto.name }</div>
                  <div class="flightCabin">${cabinGrade }</div>
              </div>
              <c:if test="${not empty flightOrderDto.orderFlights[0].shareCarrier }">
               <div class="share fix">
                   <p class="share-t"><i class="iconfont icon-arrow"></i><span>共享航班:</span></p>
                   <p class="share-info"><span>实际乘坐</span>
                   	<img  alt="" src="http://${SYS_CONFIG['imgserver'] }/${flightOrderDto.orderFlights[0].shareCarrier.logo }">
                  		<span>${flightOrderDto.orderFlights[0].shareCarrier.nameShort} ${flightOrderDto.orderFlights[0].realFlightNo }</span>
                  	</p>
               </div>
              </c:if>
              <div class="rightItem right_top3">
                  <p class="fix">
                      <span class="Rtop3_timeL"><fmt:formatDate value="${flightOrderDto.orderFlights[0].fromDate}" pattern="HH:mm" /></span>
                      <span class="Rtop3_line"></span>
                      <span class="Rtop3_center">${fn:substring(flightOrderDto.orderFlights[0].flightDuration, 0, 2)}h${fn:substring(flightOrderDto.orderFlights[0].flightDuration, 3, 5)}m</span>
                      <span class="Rtop3_line"></span>
                      <span class="Rtop3_timeR"><fmt:formatDate value="${flightOrderDto.orderFlights[0].toDate}" pattern="HH:mm"/>
                    <c:if test="${flightOrderDto.orderFlights[0].fromDate.getDay() ne flightOrderDto.orderFlights[0].toDate.getDay()}">
                     		<a href="javascript:;" class="dayHover">+1</a>
                        <span class="dayTipb">
                       		 <a href="javascript:;" class="dayTip">到达时间为第2天,&nbsp;&nbsp;<fmt:formatDate value="${flightOrderDto.orderFlights[0].toDate}" pattern="yyyy年MM月dd日  HH:mm"/></a>
                        </span>
                     </c:if>
                      </span>
                  </p>
                  <p class="fix rt3-sub2">
                      <span class="Rtop3_start">${flightOrderDto.orderFlights[0].fromAirportName }</span>
                      <span class="Rtop3_company">
                      	<c:if test="${not empty flightOrderDto.orderFlights[0].stopCity and flightOrderDto.orderFlights[0].stopCity !='' }"><b>停</b>${flightOrderDto.orderFlights[0].stopCityName }</c:if>
                      </span>
                      <span class="Rtop3_end">${flightOrderDto.orderFlights[0].toAirportName }</span>
                  </p>
              </div>
             <div class="backOrChange">
                 <table>
                     <tbody>
                        <tr>
                            <td>退票说明</td>
                            <td class="tdInfo">${flightOrderDto.endorseRefundRules[0].refundRule }</td>
                        </tr>
                        <tr>
                            <td>改期说明</td>
                            <td class="tdInfo">${flightOrderDto.endorseRefundRules[0].changeRule }</td>
                        </tr>
                         <tr>
                             <td>签转说明</td>
                             <td class="tdInfo">${flightOrderDto.endorseRefundRules[0].endorseRule }</td>
                         </tr>
                         <c:if test="${not empty flightOrderDto.endorseRefundRules[0].outRemark }">
	                    <tr>
	                        <td class="tdInfo">退改签备注</td>
	                      	<td>${flightOrderDto.endorseRefundRules[0].outRemark }</td>
	                    </tr>
                		   </c:if>
                 		   <c:if test="${not empty flightOrderDto.endorseRefundRules[0].extraRule }">
	                    <tr>
	                        <td class="tdInfo">退改签补充规定</td>
	                      	<td>${flightOrderDto.endorseRefundRules[0].extraRule }</td>
	                    </tr>
                		  </c:if>
                     </tbody>
                 </table>
             </div>
              <div class="total_order fix">
                  <div>
                      <p class="total_order1">订单总额</p>
                      <p class="total_order3">¥<b class="total_order4" >${flightOrderDto.orderFee.totalOrderPrice }</b></p>
                  </div>
              </div>
              <div class="detailed_box">
                  <div class="adult fix">
                      <span class="l">成人</span><span class="xuxian"></span><span class="detailed_spanR">¥${flightOrderDto.orderFee.salePrice + flightOrderDto.orderFee.departureTax + flightOrderDto.orderFee.fuelTax}
                      <a href="javascript:;">x</a><span >${fn:length(flightOrderDto.passengers)}</span></span>
                  </div>
                  <ul class="detailed_ul">
                      <li class="fix">
                          <span>成人票</span><span class="xuxian"></span><span class="detailed_spanR" >¥${flightOrderDto.orderFee.facePrice }</span>
                      </li>
                      <c:if test="${flightOrderDto.orderFee.departureTax   > 0}">
                       <li class="fix">
                           <span>机场建设费</span><span class="xuxian"></span><span class="detailed_spanR" >¥${flightOrderDto.orderFee.departureTax }</span>
                       </li>
                      </c:if>
                       <li class="fix">
                            <c:choose>
		                       	<c:when test="${flightOrderDto.orderFee.fuelTax > 0  }">
		                       		<span>燃油附加费</span><span class="xuxian"></span><span class="detailed_spanR" >¥${flightOrderDto.orderFee.fuelTax }</span>
		                       	</c:when>
		                       	<c:otherwise>
		                       		<span>燃油附加费</span><span class="xuxian"></span><span class="detailed_spanR" >免费</span>
		                       	</c:otherwise>
	                       </c:choose>
                       </li>
                      <c:if test="${flightOrderDto.orderFee.discountAmount > 0}">
                       <li class="fix">
                           <span>直减</span><span class="xuxian"></span><span class="detailed_spanR">¥${flightOrderDto.orderFee.discountAmount }</span>
                       </li>
                      </c:if>
                  </ul>
<!--                   <div class="adult fix"> -->
<!--                       <span class="l">保险</span><span class="xuxian"></span><span class="detailed_spanR">¥583 -->
<!--                       <a href="javascript:;">x</a>1</span> -->
<!--                   </div> -->
                  <c:if test="${flightOrderDto.orderFee.extraFee > 0 }">
	                  <div class="adult fix">
	                      <span class="l">服务费</span><span class="xuxian"></span><span class="detailed_spanR">¥${flightOrderDto.orderFee.extraFee }
	                      <a href="javascript:;">x</a>${fn:length(flightOrderDto.passengers)}</span>
	                  </div>
                  </c:if>
              </div>
          </div>
	</div>
</div>



<script src="resource/js/order/flightBackDetail.js?version=${globalVersion}"></script>
