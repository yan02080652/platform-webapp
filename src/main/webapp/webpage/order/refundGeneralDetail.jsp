<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/flightBackDetail.css?version=${globalVersion}">
<style>
.oProgressItem {
	width: 175px;
}
</style>
<div class="main">
	<div class="contentBox fix">
		<div class="oContentL">
			<h2 class="oTitle">订单详情</h2>
			<div class="fix oNumber">
				<div class="l">
					<p>
						订单号：<span>${refundOrder.id }</span>
					</p>
				</div>
			</div>
			<div class="oState">
				<h3 class="oStateTitle">${refundOrder.status.message }</h3>
				<div class="oProgress fix">
					<div class="oProgressItem" id="stage1">
						<p>提交申请</p>
						<span><fmt:formatDate value="${refundOrder.applyTime }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</div>
					<div class="oProgressItem" id="stage2">
						<p>客服审核</p>
						<span><fmt:formatDate value="${refundOrder.auditStartTime }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</div>
					<div class="oProgressItem" id="stage4">
						<p>退款完成 </p>
						<span><fmt:formatDate value="${refundOrder.finishTime }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</div>
				</div>
			</div>
		 	<div class="oItem">
				<h3 class="oItemTitle">退款信息</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">原因</span>
						<span>${refundOrder.reason }</span>
					</p>
				</div>
			</div> 
 			<div class="oItem">
				<h3 class="oItemTitle">联系人</h3>
				<div class="oItemLi">
 					<p>
						<span class="oItemLiLabel">姓名</span><span>${refundOrder.contactName }</span>
					</p>
					<p>
						<span class="oItemLiLabel">手机号码</span><span>${refundOrder.contactPhone }</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	
	var orderShowStatus = '${refundOrder.status}';
	
	if(orderShowStatus=='ADUITING'){
		$("#stage1").addClass("oProgressItemActive");
		$("#stage2").addClass("oProgressItemActive");
	}
	if(orderShowStatus=='REFUNDING'){
		$("#stage1").addClass("oProgressItemActive");
		$("#stage2").addClass("oProgressItemActive");
		//$("#stage3").addClass("oProgressItemActive");
	}
	if(orderShowStatus=='REFUNDED'){
		$("#stage1").addClass("oProgressItemActive");
		$("#stage2").addClass("oProgressItemActive");
		//$("#stage3").addClass("oProgressItemActive");
		$("#stage4").addClass("oProgressItemActive");
	} 
	
})
</script>
