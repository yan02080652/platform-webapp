<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
	.dash_tr td{
		border-top:none !important;
	}
</style>

<div class="result">
	<table class="table orderTable">
		<thead>
			<tr>
				<th class="tipL">订单号</th>
				<th align="center">订单情况</th>
				<th>预订人</th>
				<th>下单时间</th>
				<th>所属客户</th>
				<th>费用归属</th>
				<th>客户结算价</th>
				<th>支付方式</th>
				<th>所属供应商</th>
				<th>供应商结算价</th>
				<th>状态</th>
				<th>结算状态</th>
				<th class="tipR">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="trainOrder">
				<tr <c:if test="${!trainOrder.topLine}">class="dash_tr"</c:if>>
					<td class="tipL">
						<%--<a href='javascript:;' onclick="openTrainDetail('order/trainDetail/${trainOrder.orderId }')">--%>
							${trainOrder.orderId}
						<%--</a>--%>
					</td>
					<td style="text-align:center;max-width: 200px;">
						<span>${trainOrder.summary}</span>
					</td>
					<td style="text-align:center;min-width: 65px;max-width:110px;">${trainOrder.userName }</td>
					<td style="text-align:center;width:110px;"><fmt:formatDate value="${trainOrder.createDate}" pattern="yyyy-MM-dd HH:mm" /></td>
					<td style="text-align:center;max-width:140px;">${trainOrder.partnerName }</td>
					<td style="text-align:center;max-width: 140px;">${trainOrder.costCenterName }</td>
					<td style="text-align:center;">${trainOrder.totalAmount }</td>
					<td style="text-align:center;">${trainOrder.paymentMethod}</td>
					<td style="text-align:center;">${trainOrder.supName}</td>
					<td style="text-align:center;">${trainOrder.supSettleAmount }</td>
					<td style="text-align:center;">${trainOrder.orderState }</td>
					<td style="text-align:center;">
					    <c:if test="${trainOrder.billId != null}">已结算</c:if>
					    <c:if test="${trainOrder.billId == null}">未结算</c:if>
					</td>
					<td class='lastTd' style="text-align:center;">
						<c:if test="${trainOrder.totalAmount > 0}">
							<a class="drop_li" style="color: #23527c;" href='javascript:;' onclick="openTrainDetail('order/trainDetail/${trainOrder.orderId }')">查看</a>
						</c:if>
						<c:if test="${trainOrder.totalAmount < 0}">
							<a class="danhao" style="color: #23527c;" href='javascript:;' onclick="openTrainDetail('order/refundTrainDetail/${trainOrder.orderId }')">查看</a>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<div class="pagin" style="margin-bottom: 15px">
<jsp:include page="../common/pagination_ajax.jsp?callback=reloadTrainOrder"></jsp:include>
</div>
