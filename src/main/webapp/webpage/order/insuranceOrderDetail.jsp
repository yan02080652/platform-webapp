<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/flightOrderDetail.css?version=${globalVersion}">
<style>
.oProgressItem {
	width: 175px;
}
</style>
<div class="main">
	<div class="contentBox fix">
		<div class="oContentL">
			<h2 class="oTitle">订单详情</h2>
			<div class="fix oNumber">
				<div class="l">
					<p>
						订单号：<span>${insuranceOrderDto.id }</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 关联业务订单号：${insuranceOrderDto.orderId }
					</p>
				</div>
			</div>
			<div class="oState">
				<h3 class="oStateTitle">${insuranceOrderDto.status.message }</h3>
				<div class="oProgress fix">
					<div class="oProgressItem" id="stage1">
						<p>待支付</p>
					</div>
					<div class="oProgressItem" id="stage2">
						<p>待投保</p>
					</div>					
					<div class="oProgressItem" id="stage3">
						<p>已投保</p>
					</div>
					<div class="oProgressItem" id="stage4">
						<p>已取消 </p>
					</div>
				</div>
			</div>
		 	<div class="oItem">
				<h3 class="oItemTitle">订单信息</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">航班号/车次号</span>
						<span>${insuranceOrderDto.flightNo }</span>
					</p>
					<p>
						<span class="oItemLiLabel">保险人数</span>
						<span>${fn:length(insuranceOrderDto.insuranceOrderDetails) }(人)</span>
					</p>
					<p>
						<span class="oItemLiLabel">目的地信息</span>
						<span>${insuranceOrderDto.destination }</span>
					</p>
					<p>
						<span class="oItemLiLabel">保单生效日期</span>
						<span><fmt:formatDate value="${insuranceOrderDto.effectiveDate }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</p>
					<p>
						<span class="oItemLiLabel">保单结束日期</span>
						<span><fmt:formatDate value="${insuranceOrderDto.expireDate }" pattern="yyyy年MM月dd日 HH:mm:ss" /></span>
					</p>
					<p>
						<span class="oItemLiLabel">支付方式</span>
						<span>${payMethed }</span>
					</p>
				</div>
			</div> 
			<div class="oItem">
				<h3 class="oItemTitle">产品信息</h3>
				<c:forEach items="${insuranceOrderDto.insuranceOrderDetails }" var="insuranceOrderDetail">
					<div class="oItemLi">
						<p>
							<span class="oItemLiLabel">被保险人姓名</span><span>${insuranceOrderDetail.insuredName }</span>
						</p>
						<p>
							<span class="oItemLiLabel">保单状态</span><span>${insuranceOrderDetail.status.message }</span>
						</p>
					</div>
				</c:forEach>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">客户结算价</span><span>${insuranceOrderDto.totalPremium }</span>
					</p>
					<p>
						<span class="oItemLiLabel">供应商结算价</span><span>${insuranceOrderDto.totalSettlePrice }</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function(){
	var orderShowStatus = '${insuranceOrderDto.status}';
 	if(orderShowStatus=='WAIT_PAYMENT'){
		$("#stage1").addClass("oProgressItemActive");
	}
	if(orderShowStatus=='IN_ISSUE'|| orderShowStatus=='WAIT_AUTHORIZE'){
		$("#stage1").addClass("oProgressItemActive");
		$("#stage2").addClass("oProgressItemActive");
	}
	if(orderShowStatus=='ISSUED'){
		$("#stage1").addClass("oProgressItemActive");
		$("#stage2").addClass("oProgressItemActive");
		$("#stage3").addClass("oProgressItemActive");
	}
	if(orderShowStatus=='AUTHORIZE_RETURN' || orderShowStatus=='CANCELED' || orderShowStatus=='REFUNDED'){
		$("#stage1").addClass("oProgressItemActive");
		$("#stage2").addClass("oProgressItemActive");
		$("#stage3").addClass("oProgressItemActive");
		$("#stage4").addClass("oProgressItemActive");
	}
	
});

</script>
