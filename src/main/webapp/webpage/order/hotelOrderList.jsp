<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/hotelOrderDetail.css?version=${globalVersion}">

<style>
	.dash_tr td{
		border-top:none !important;
	}
</style>

<div class="result">
    <table class="table orderTable">
        <thead>
            <tr>
                <th class="tipL">订单号</th>
				<th align="center">订单情况</th>
				<th>预订人</th>
				<th>下单时间</th>
				<th>所属客户</th>
				<th>费用归属</th>
				<th>客户结算价</th>
				<th>支付方式</th>
				<th>所属供应商</th>
				<th>供应商结算价</th>
				<th>状态</th>
				<th>结算状态</th>
				<th class="tipR">操作</th>
            </tr>
        </thead>
        <tbody>
        	<c:forEach items="${pageList.list }" var="order" varStatus="status">
        		<tr <c:if test="${!order.topLine}">class="dash_tr"</c:if>>
	                <td class=" tipL">
	                	<%--<a href='javascript:;' onclick="openHotelDetail('order/hotelDetail/${order.orderId }')">--%>
							${order.orderId }
						<%--</a>--%>
					</td>
	                <td style="text-align:center;max-width: 200px;"><span>${order.summary }</span></td>
	                <td style="text-align:center;min-width: 65px;max-width:110px;">${order.userName }</td>
	                <td style="text-align:center;width:110px;"><fmt:formatDate value="${order.createDate}" pattern="yyyy-MM-dd HH:mm" /></td>
	                <td style="text-align:center;max-width: 140px;">${order.partnerName }</td>
					<td style="text-align:center;max-width: 140px;">${order.costCenterName }</td>
					<td style="text-align:center;">${order.totalAmount}</td>
					<td style="text-align:center;">${order.paymentMethod}</td>
					<td style="text-align:center;">${order.supName}</td>
					<td style="text-align:center;">${order.supSettleAmount }</td>
	                <td style="text-align:center;">${order.orderState }</td>
	                <td style="text-align:center;">
					    <c:if test="${order.billId != null}">已结算</c:if>
					    <c:if test="${order.billId == null}">未结算</c:if>
					</td>
	                <td class="lastTd" style="text-align:center;">
						 <c:if test="${order.totalAmount > 0}">
							 <a class="drop_li" style="color: #23527c;" onclick="openHotelDetail('order/hotelDetail/${order.orderId }')">查看</a>
						 </c:if>
						 <c:if test="${order.totalAmount < 0}">
							<a class="danhao" style="color: #23527c;" href='javascript:;' onclick="openHotelDetail('order/refundHotelDetail/${order.orderId }')">查看</a>
						 </c:if>
					</td>
	            </tr>
        	</c:forEach>
        </tbody>

    </table>
</div>
<div class="pagin" style="margin-bottom: 15px">
<jsp:include page="../common/pagination_ajax.jsp?callback=reloadOrderList"></jsp:include> 
</div>