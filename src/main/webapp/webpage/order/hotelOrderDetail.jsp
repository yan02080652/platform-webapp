<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" language="java"%> 
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/hotelOrderDetail.css?version=${globalVersion}">

<div class="main">
	<div class="oContent fix">
		<input type="hidden" value="${order.cancelTime }" id="cancelTime"/>
		<div class="oContentL">
			<!-- <p class="nav">
				<a href="javascript:void(0);"><span class="first">订单</span></a> 
				<a href="javascript:void(0);"><span class="sub">酒店订单</span></a> 
				<a href="javascript:void(0);"><span class="three">订单详情</span></a>
			</p> -->
			<h2 class="oTitle">订单详情</h2>
			<div class="fix oNumber">
				<div class="l">
					<p>
						订单号：<span>${order.id }</span>
					</p>
				</div>
				<%-- <div class="r">
					<c:if test="${order.orderShowStatus ne 'NO_SHOW' and order.orderShowStatus ne 'CHECKIN' and order.orderShowStatus ne 'CHECKOUT' and order.orderShowStatus ne 'CANCELED' and order.orderShowStatus ne 'WAIT_AUTHORIZE'}">
						<button class="cancel" orderId="${order.id }">取消订单</button>
					</c:if>
					<c:if test="${order.paymentType eq 'Prepay' and order.paymentStatus eq 'NOT_PAYMENT' and order.orderStatus ne 'TIMEOUT_CANCEL' and order.orderStatus ne 'CHANGE_CANCEL' and order.orderStatus ne 'CANCELED'}">
						<button class="pay" orderId="${order.id }">立即支付</button>
					</c:if>
					<c:if test="${order.paymentType eq 'CardGuarantee' and order.paymentStatus eq 'NOT_GUARANTEE' and order.orderStatus ne 'TIMEOUT_CANCEL' and order.orderStatus ne 'CHANGE_CANCEL' and order.orderStatus ne 'CANCELED'}">
						<button class="guarantee" orderId="${order.id }">立即担保</button>
					</c:if>
				</div> --%>
			</div>
			<div class="oState">
				<h3 class="oStateTitle">${order.orderShowStatus.status }</h3>
				<div class="fix mb20">
					<div class="l">
						<c:if test="${order.orderShowStatus eq 'CANCELED'}">
							<p class="oCount">
								${order.orderStatus.status }
							</p>
						</c:if>
						<input type="hidden" value="${order.orderShowStatus}" id="orderShowStatus"/>
						<input value="${order.lastUpdateTime.getTime()}" id="startTime" type="hidden"/>
						<c:if test="${order.orderShowStatus eq 'WAIT_PAYMENT' or order.orderShowStatus eq 'WAIT_GUARANTEE'}">
							<p class="oCount">
								<span>请您在提交订单后<span id="timer"></span>分钟内完成支付，超时订单将被关闭</span>
							</p>
						</c:if>
						<c:if test="${order.orderShowStatus eq 'CONFIRMING' or order.orderShowStatus eq 'CONFIRMED'}">
							<p class="oCount">
								<fmt:parseDate value="${order.cancelTime }" var="orderCancelTime" pattern="yyyy-MM-dd HH:mm:ss" />
								<c:choose>
									<c:when test="${order.manual}">
										${prepayRule}
									</c:when>
									<c:otherwise>
										${order.prepayNote}
									</c:otherwise>
								</c:choose>
							</p>
						</c:if>
					</div>
				</div>
				<div class="oProgress fix">
					<div class="oProgressItem oProgressItemActive">
						<p>提交订单</p>
						<span><fmt:formatDate value="${order.createDate }" pattern="yyyy年MM月dd日 HH:mm:ss"/></span>
					</div>
					<c:if test="${order.paymentType eq 'Prepay'}">
						<div class="oProgressItem <c:if test="${order.paymentStatus eq 'PAYMENT_SUCCESS' }">oProgressItemActive</c:if> ">
							<p>支付订单</p>
							<c:forEach items="${operations }" var="bean" varStatus="status">
								<c:if test="${bean.type eq 'USR_PAY' }">
									<span><fmt:formatDate value="${bean.operationDate }" pattern="yyyy年MM月dd日 HH:mm:ss"/></span>
								</c:if>
							</c:forEach>
						</div>
					</c:if>
					<div class="oProgressItem <c:if test="${order.orderStatus eq 'CONFIRMED' or order.orderStatus eq 'NO_SHOW' or order.orderStatus eq 'CHECKIN' or order.orderStatus eq 'CHECKOUT' }">oProgressItemActive</c:if>">
						<p>酒店确认</p>
						<span><fmt:formatDate value="${order.issuedDate}" pattern="yyyy年MM月dd日 HH:mm:ss"/></span>	
					</div>
					<div class="oProgressItem <c:if test="${order.orderStatus eq 'CHECKOUT' or order.orderStatus eq 'CHECKIN' }">oProgressItemActive</c:if>" >
						<p>预订完成</p>
					</div>
				</div>
			</div>
			<div class="oItem">
                <h3 class="oItemTitle">差旅信息</h3>
                <div class="oItemLi">
					<style>
						.oItemLi .whole-line{
							white-space: nowrap;
							display: flex;
						}
						.oItemLi .whole-line span{
							vertical-align: top;
							white-space: normal;
							word-break: break-all;
							display: inline-block;
						}
						.oItemLi .whole-line span:last-child{
							flex: 1;
						}
					</style>
					<p><span class="oItemLiLabel">出行性质</span><span>${order.travelType.msg }</span></p>
					<c:if test="${order.travelType ne 'PERSONAL'}">
						<p><span class="oItemLiLabel">差旅计划</span><span>${order.travelPlanNo}</span></p>
						<p><span class="oItemLiLabel">出行类别</span><span>${travel.itemTxt }</span></p>
						<p><span class="oItemLiLabel">费用类型</span><span>${costName }</span></p>
						<p class="whole-line">
							<span class="oItemLiLabel">出差事由</span>
							<span>${order.travelReason}</span>
						</p>
					</c:if>
                </div>
            </div>
            <c:if test="${not empty paySuccess}">
			<div class="oItem">
				<h3 class="oItemTitle">付款信息</h3>
				<c:if test="${personalPayAmount ne null}">
				<div class="oItemLi-cdy">个人支付</div>
				<div class="oItemLi oItemLiIndent">
						<p><span class="oItemLiLabel">支付金额</span><span> ¥${personalPayAmount}</span></p>
						<p><span class="oItemLiLabel">支付时间</span>
						<fmt:formatDate value="${order.paymentDate }" pattern="yyyy/MM/dd HH:mm"/>
						</p>
				</div>
					<br/k>
				</c:if>

				<c:if test="${order.bpTotalAmount ne null}">
					<div class="oItemLi-cdy">企业账户</div>
					<div class="oItemLi oItemLiIndent">
						<p><span class="oItemLiLabel">支付金额</span><span> ¥${order.bpTotalAmount}</span></p>
						<p><span class="oItemLiLabel">支付时间</span>
						<fmt:formatDate value="${order.paymentDate }" pattern="yyyy/MM/dd HH:mm"/>
						</p>
						<p><span class="oItemLiLabel">交易编码</span><span> ${tradeId}</span></p>
					</div>
				</c:if>
			</div>
			</c:if>
			<div class="oItem">
				<h3 class="oItemTitle">预订信息</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">入住日期</span><span><fmt:formatDate value="${order.arrivalDate }" pattern="yyyy-MM-dd"/> </span>
					</p>
					<p>
						<span class="oItemLiLabel">离店日期</span><span><fmt:formatDate value="${order.departureDate }" pattern="yyyy-MM-dd"/></span>
					</p>
					<p>
						<span class="oItemLiLabel">预订间数</span><span>${order.roomNum }</span>间
					</p>
					<p>
						<span class="oItemLiLabel">到店时间</span><span>${order.latestArrivalTime }</span>
					</p>
				</div>
			</div>
			<div class="oItem">
				<h3 class="oItemTitle">入住人</h3>
				<c:forEach items="${order.customers }" var="customer" varStatus="status">
					<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">姓名</span><span>${customer.name }</span>
					</p>
					<p>
						<span class="oItemLiLabel">费用分摊</span><span>${customer.amount }</span>元
					</p>
					<p>
						<span class="oItemLiLabel">确认号</span>
						<span>
							<c:choose>
								<c:when test="${not empty  customer.confirmNum}">
									${customer.confirmNum }
								</c:when>
								<c:otherwise>
									无
								</c:otherwise>
							</c:choose>
						</span>
					</p>
				</div>
				</c:forEach>
			</div>
			<div class="oItem">
				<h3 class="oItemTitle">联系人</h3>
				<div class="oItemLi">
					<p>
						<span class="oItemLiLabel">姓名</span><span>${order.contact.name }</span>
					</p>
					<p>
						<span class="oItemLiLabel">手机</span><span>${order.contact.mobile }</span>
					</p>
					<p>
						<span class="oItemLiLabel">邮箱</span><span>${order.contact.email }</span>
					</p>
				</div>
			</div>
			<c:if test="${not empty order.noteToHotel}">
				<div class="oItem" >
					<h3 class="oItemTitle">客户备注</h3>
					<div class="oItemLi">
						<p>
							<span class="oItemLiLabel">特殊需求</span>
							<span>${fn:substringBefore(order.noteToHotel, "-")}</span>

						</p>
						<p>
							<span class="oItemLiLabel"></span>
							<span>${fn:substringAfter(order.noteToHotel, "-")}</span>
						</p>

					</div>
				</div>
			</c:if>
		</div>
		<!-- 右侧内容 -->
		<div class="oContentR">
			<div class="rightTitle">
				<p>您选择的酒店</p>
			</div>
			<div class="rightHotel fix">
				<div class="l">
					<img class="hotelImg" src="${order.hotelImgUrl }" alt="">
				</div>
				<div class="desc">
					<p class="hotelName">${order.hotelName }</p>
					<p class="hotelAddress">${order.address }</p>
					<c:if test="${order.protocol}">
						<p style="margin-top:8px;"><span class="protocol">协议酒店</span></p>
					</c:if>
				</div>
			</div>
			<div class="hotelDetail">
				<p>
	                <label> 房型 ：</label><span>${order.roomName }</span>
	            </p>
	            <p>
	                <label> 床型 ：</label><span>${order.room.bedType }</span>
	            </p>
	            <p>
	                <label> 早餐 ：</label>
	                <span>
	                ${order.nightlyRates[0].breakFastDes}
	                </span>
	            </p>
	            <p>
	                <label> 宽带 ：</label>
	                <span>
	                	<c:if test="${order.room.broadnet eq 0 }">无</c:if>
						<c:if test="${order.room.broadnet eq 1 }">免费宽带</c:if>
						<c:if test="${order.room.broadnet eq 2 }">收费宽带</c:if>
						<c:if test="${order.room.broadnet eq 3 }">免费WIFI</c:if>
						<c:if test="${order.room.broadnet eq 4 }">收费WIFI</c:if>
	                </span>
	            </p>
	            <p>
	                <label> 面积 ：</label><span>${order.room.area }</span>
	            </p>
	             <p>
	                <label> 楼层 ：</label><span>${order.room.floor }</span>
	            </p>
	           <!--  <p>
	                <label> 加床 ：</label><span>不可加床(?)</span>
	            </p> -->
				
			</div>
			<div class="total_order fix">
				<p>
					<span>共 <span>${order.roomNum }</span>间<span>${order.stayDay }</span>晚
					</span> <span class="r">服务费：<span>0</span>元/间夜
					</span>
				</p>
				<div class="feeDetail">
					<p>
						<span>房费合计</span> <span class="r"><span>¥</span>${order.totalPrice }</span>
					</p>
					<p>
						<span>服务费合计</span> <span class="r"><span>¥</span>${order.extraFee }</span>
					</p>
				</div>
				<p>
					<span class="total_order1">客户结算价</span><span class="total_order2">¥<fmt:formatNumber value="${order.totalPrice + order.extraFee}" maxFractionDigits="2"/></span>
				</p>
				
				<p>
					<span class="total_order1">供应商结算价</span><span class="total_order2">${order.currencyCode}<fmt:formatNumber value="${order.totalCost}" maxFractionDigits="2"/></span>
				</p>
			</div>

			<div class="hideHover">
				<c:forEach items="${order.nightlyRates }" var="bean" varStatus="status">
					<div class="detailItem">
						 <fmt:formatDate value="${bean.checkInDate }" pattern="E" var="dateOfweek"/>
	                     <p class="itemD"><span><fmt:formatDate value="${bean.checkInDate }" pattern="MM-dd"/></span> （<span>${fn:substring(dateOfweek,2,3) }</span>）</p>
	                    <c:if test="${order.currencyCode ne 'RMB'}">
							<p class="itemPrice">${order.currencyCode} <span>${bean.originPrice }</span></p>
						</c:if>
	                     <p class="itemPrice">¥ <span>${bean.dayRate }</span></p>
	                     <p class="itemB">
	                     	${bean.breakFastDes}
	                     </p>
	                 </div>
				</c:forEach>
         	</div>
		</div>
	</div>
</div>
<script src="${baseRes }/js/order/hotelOrderDetail.js?version=${globalVersion}"></script>
