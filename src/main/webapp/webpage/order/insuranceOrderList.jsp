<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<style>
	.dash_tr td{
		border-top:none !important;
	}
</style>

<div class="result">
	<table class="table orderTable">
		<thead>
			<tr>
				<th class="tipL">保险单号</th>
				<th align="center">关联订单</th>
				<th align="center" style="max-width: 200px;">订单情况</th>
				<th>下单人</th>
				<th>下单时间</th>
				<th>所属客户</th>
				<th>费用归属</th>
				<th>客户结算价</th>
				<th>支付方式</th>
				<th>所属供应商</th>
				<th>供应商结算价</th>
				<th>状态</th>
				<th>结算状态</th>
				<th class="tipR">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="insuranceOrder">
				<tr <c:if test="${!order.topLine}">class="dash_tr"</c:if>>
					<td class="tipL" style="width:120px;">
						<%--<a href='javascript:;' onclick="openInsuranceDetail('order/insuranceDetail/${insuranceOrder.orderId }')">--%>
							${insuranceOrder.orderId}
						<%--</a>--%>
					</td>
					<td style="text-align:center;width:120px;">${insuranceOrder.orderId }</td>
					<td style="text-align:center;max-width: 200px;">
						${insuranceOrder.summary}
					</td>
					<td style="text-align:center;min-width: 65px;max-width:110px;">${insuranceOrder.userName }</td>
					<td style="text-align:center;width:110px;"><fmt:formatDate value="${insuranceOrder.createDate}" pattern="yyyy-MM-dd HH:mm" /></td>
					<td style="text-align:center;max-width: 140px;">${insuranceOrder.partnerName }</td>
					<td style="text-align:center;max-width: 140px;">${insuranceOrder.costCenterName }</td>
					<td style="text-align:center;">${insuranceOrder.totalAmount }</td>
					<td style="text-align:center;">${insuranceOrder.paymentMethod}</td>
					<td style="text-align:center;">${insuranceOrder.supName}</td>
					<td style="text-align:center;">${insuranceOrder.supSettleAmount}</td>
					<td style="text-align:center;">${insuranceOrder.orderState }</td>
					<td style="text-align:center;">
					    <c:if test="${insuranceOrder.billId != null}">已结算</c:if>
					    <c:if test="${insuranceOrder.billId == null}">未结算</c:if>
					</td>
					<td class="lastTd" style="text-align:center;">
						<c:if test="${insuranceOrder.totalAmount > 0}">
							<a class="drop_li" style="color: #23527c;" href='javascript:;' onclick="openInsuranceDetail('order/insuranceDetail/${insuranceOrder.insuranceId }')">查看</a>
						</c:if>
						<%--<c:if test="${insuranceOrder.totalAmount < 0}">--%>
							<%--<a class="danhao" style="color: #23527c;" href='javascript:;' onclick="openInsuranceDetail('order/refundInsuranceDetail/${insuranceOrder.orderId }')">查看</a>--%>
						<%--</c:if>--%>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<div class="pagin" style="margin-bottom: 15px">
<jsp:include page="../common/pagination_ajax.jsp?callback=reloadInsuranceOrder"></jsp:include> 
</div>
<script type="text/javascript">

</script>