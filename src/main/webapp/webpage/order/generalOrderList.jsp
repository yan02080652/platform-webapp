<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
	.dash_tr td{
		border-top:none !important;
	}
</style>

<div class="result">
	<table class="table orderTable">
		<thead>
			<tr>
				<th class="tipL">订单号</th>
				<th align="center">订单情况</th>
				<th>预订人</th>
				<th>下单时间</th>
				<th>所属客户</th>
				<th>费用归属</th>
				<th>客户结算价</th>
				<th>支付方式</th>
				<th>所属供应商</th>
				<th>供应商结算价</th>
				<th>状态</th>
				<th>结算状态</th>
				<th class="tipR">操作</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="generalOrder">
				<tr <c:if test="${!generalOrder.topLine}">class="dash_tr"</c:if> >
					<td class=" tipL">
						<%--<a href='javascript:;' onclick="openGeneralDetail('order/generalDetail/${generalOrder.orderId }')">--%>
							${generalOrder.orderId}
						<%--</a>--%>
					</td>
					<td style="text-align:center;max-width:200px;">
						${generalOrder.summary }
					</td>
					<td style="text-align:center;">${generalOrder.userName }</td>
					<td style="text-align:center;width: 110px;"><fmt:formatDate value="${generalOrder.createDate}" pattern="yyyy-MM-dd HH:mm" /></td>
					<td style="text-align:center;max-width:140px;">${generalOrder.partnerName }</td>
					<td style="text-align:center;max-width: 140px;">${generalOrder.costCenterName }</td>
					<td style="text-align:center;">${generalOrder.totalAmount}</td>
					<td style="text-align:center;">${generalOrder.paymentMethod}</td>
					<td style="text-align:center;">
						${generalOrder.supName }
					</td>
					<td style="text-align:center;">${generalOrder.supSettleAmount }</td>
					<td style="text-align:center;">${generalOrder.orderState }</td>
					
					<td style="text-align:center;">
					    <c:if test="${generalOrder.billId != null}">已结算</c:if>
					    <c:if test="${generalOrder.billId == null}">未结算</c:if>
					</td>
					
					<td class="lastTd" style="text-align:center;">
						 <c:if test="${generalOrder.totalAmount >= 0}">
							 <a class="drop_li" style="color: #23527c;" href='javascript:;' onclick="openGeneralDetail('order/generalDetail/${generalOrder.orderId }')">查看</a>
						 </c:if>
						 <c:if test="${generalOrder.totalAmount < 0}">
							<a class="danhao" style="color: #23527c;" href='javascript:;' onclick="openGeneralDetail('order/refundGeneralDetail/${generalOrder.orderId }')">查看</a>
						 </c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
<div>
	<form action="pay-channel.html" method="get" id="payForm">
		<input type="hidden"  name="orderIds"  >
	</form>
</div>
<div class="pagin" style="margin-bottom: 15px">
	<jsp:include page="../common/pagination_ajax.jsp?callback=reloadGeneralOrder"></jsp:include> 
</div>
<script type="text/javascript">
function openGeneralDetail(url){
	window.open(url);
}
</script>