<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="priceRuleApplyModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:600px;">
        <div class="modal-content" style="height:350px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">编辑企业适用的用车价格策略组</h4>
            </div>
            <div class="modal-body">
                <form:form modelAttribute="applyDto" id="applyDetailForm" cssClass="form-horizontal">
                    <form:hidden path="id" />
                    <div class="form-group">
                        <label class="col-sm-3 control-label">企业</label>
                        <div class="col-sm-9">
                            <label class="control-label">${applyDto.partnerName}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">价格策略组</label>
                        <div class="col-sm-9">
                            <label class="control-label">${applyDto.groupName}</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">有效期从</label>
                        <div class="col-sm-9">
                            <form:input path="startTime" cssClass="form-control datetime-picker" readonly="true" placeholder="选择生效起始日期" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">有效期至</label>
                        <div class="col-sm-9">
                            <form:input path="endTime" cssClass="form-control datetime-picker"  readonly="true" placeholder="选择生效起始日期" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <label type="button" class="btn btn-primary" onclick="submitPriceRuleApply()">保存</label>
                        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<script  src="webpage/pss/partner/priceRule/js/applyDetail.js?version=${globalVersion}"></script>

