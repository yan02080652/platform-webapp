<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="applypriceRuleGroupModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:1200px;">
        <div class="modal-content" style="height:550px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h2 class="modal-title">分配企业适用的用车价格策略组</h2>
            </div>
            <div class="modal-body">
                <div id="priceDetail" class="col-md-12 animated fadeInRight">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pull-left">
                                                <label>企业名称:${partner.name}</label>
                                                <input type="hidden" id="partnerid" value="${partner.id}">
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover" id="applyPartnerPriceRuleGroupTable">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" onchange="parent.selectAll()" id="selectAll"/></th>
                                            <th><label>策略组</label></th>
                                            <th><label>有效期从</label></th>
                                            <th><label>有效期至</label></th>
                                            <th><label>优先级</label></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <c:forEach items="${pageList.list }" var="groupDto">
                                            <tr class="odd gradeX">
                                                <td><input type="checkbox" name="id" value="${groupDto.id }"/></td>
                                                <td class="grade">${groupDto.name }</td>
                                                <td><input class=" form-control datetime-picker" name="startTime" readonly placeholder="选择生效起始日期" /></td>
                                                <td><input class=" form-control datetime-picker"  name="endTime"  readonly placeholder="选择生效起始日期" /></td>
                                                <td><input class=" form-control" type="text" name="priority" value=""></td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                                    <div class="pagin">
                                        <jsp:include page="../../../common/pagination_ajax.jsp?callback=reloadTable"></jsp:include>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭
                </button>
                <button type="button" class="btn btn-primary" onclick="parent.submitApplyPriceRuleGroup()">保存
                </button>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">

    $(".datetime-picker").jeDate({
        format:"YYYY-MM-DD hh:mm:ss"
    });
</script>


