<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="enterprisepPriceRuleManage">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>TMC企业价格策略管理  &nbsp;&nbsp;
            </div>
            <div id="priceDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-inline" method="post" >
                                            <nav class="text-left">
                                                <div class=" form-group">
                                                    <label class="control-label">查询条件：</label>
                                                    <input name="cName" id="c_name" class="form-control" placeholder="企业">
                                                    <input type="hidden" id="c_id">
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default" onclick="search_rule()">查询</button>
                                                    <button type="button" class="btn btn-default" onclick="reset_rule()">重置</button>
                                                </div>
                                            </nav>
                                        </form>
                                    </div>
                                </div>
                                <div  id="applyList_Table" style="margin-top: 15px">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="applyDetail"></div>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>
<script type="application/javascript" src="webpage/pss/partner/priceRule/js/apply.js?version=${globalVersion}"></script>
