$(function(){
    $(".datetime-picker").jeDate({
        format:"YYYY-MM-DD hh:mm:ss"
    });
})



function submitPriceRuleApply(){
    if($("#startTime").val() != '' &&$("#endTime").val() != '' && $("#startTime").val() > $("#endTime").val()){
        $.alert("有效期结束日期不能小于起始日期","提示");
        return;
    }
    $.ajax({
        url:"partner/priceRule/saveApply",
        type:"post",
        data:$('#applyDetailForm').serialize(),
        success:function(data){
            if(data.type=="success"){
                $.alert(data.txt,"提示");
                parent.backPriceRuleApply();
            }

        }
    })
}