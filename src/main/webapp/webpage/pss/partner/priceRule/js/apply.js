$('#c_name').click(function(){
    TR.select('partner',{
      type: 0
    },function(data){
        $('#c_name').val(data.name);
        $('#c_id').val(data.id);
    });
})
//查询
function search_rule(){
    if($('#c_id').val().trim()==''){
        alert('请选择企业');
        return;
    }
    reloadTable(1);
}
//重置
function reset_rule(){
    $("#c_id").val("");
    $("#c_name").val("");
    $("#applyList_Table").html("");
}
function reloadTable(pageIndex){

    $.ajax({
        url:"partner/priceRule/applyList",
        type: 'post',
        data:{
            partnerId:$('#c_id').val().trim(),
            pageIndex: pageIndex
        },success:function(data){
            $("#applyList_Table").html(data);
        }
    })
}
//获取修改详情
function getApplyPriceRule(id){
    var url= "partner/priceRule/apply?id="+id;
    $('#applyDetail').load(url, function (context, state) {
        if ('success' == state) {
            $('#priceRuleApplyModel').modal();
        }
    });

}
function backPriceRuleApply(){
    $("#priceRuleApplyModel").modal('hide');
    search_rule();
}
//删除
function deleteApplay(id){
    $.ajax({
        url:"partner/priceRule/deleteApply",
        type:"post",
        data:{id:id},
        success:function(data){
            if(data.type='success'){
                $.alert(data.txt, "提示");
                search_rule();
            }else{
                $.alert(data.txt, "提示");
            }
        }
    })
}
//返回
function backApply(){
    $("#applypriceRuleGroupModel").modal('hide');
    search_rule();
}
//获取企业用车策略组
function GetPartnerPriceRuleGroup(pageIndex){
    var id=$('#c_id').val().trim();
    if(id==''){
        alert('请选择企业');
        return;
    }
    var url= "partner/priceRule/getPartnerPriceRuleGroup?id="+id+"&pageIndex="+pageIndex;
    $('#applyDetail').load(url, function (context, state) {
        if ('success' == state) {
            $('#applypriceRuleGroupModel').modal();
        }
    });
}
//checkbox全选
function selectAll() {
    if ($("#selectAll").is(":checked")) {
        $(":checkbox").prop("checked", true);//所有选择框都选中
    } else {
        $(":checkbox").prop("checked", false);
    }
}
//保存企业分配策略组
function submitApplyPriceRuleGroup(){

    var jsonArr = [];
    var flag=true;
    $("#applyPartnerPriceRuleGroupTable tbody").find("tr").each(function(){
        var json = {};
        var checkedflag=$(this).find("td input[name=id]").is(":checked");
        if(checkedflag){
            var starttime=$(this).find("td input[name=startTime]").val();
            var endtime=$(this).find("td input[name=endTime]").val();
            var priority=$(this).find("td input[name=priority]");
            json["groupId"]=$(this).find("td input[name=id]").val();
            json["partnerId"]=$("#partnerid").val();
            if(starttime != '' &&endtime != '' && starttime >endtime){
                $.alert("有效期结束日期不能小于起始日期","提示");
                flag=false;
                return false;
            }
            json["startTime"]=starttime;
            json["endTime"]=endtime;
            if(priority){
                if(priority.val().trim()==''){
                    alert('优先级不能为空');
                    flag=false;
                    return false;
                }
                json[priority.attr("name")]=priority.val();
            }
            jsonArr.push(json);
        }
    });
    if(!flag){
        return false;
    }
    if(jsonArr.length==0){
        $.alert("请选择数据再提交","提示");
        return false;
    }
    $.ajax({
        url:"partner/priceRule/saveApplyPartnerPriceRuleGroup",
        type:"post",
        data: {
            formList: JSON.stringify(jsonArr)
        },
        success:function(data){
            if(data.type=="success"){
                backApply();
            }else{
                $.alert(data.txt,"提示");
            }
        }
    })
}