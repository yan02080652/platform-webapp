<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" id="applyListTable">
    <thead>
    <tr>
        <th><label>优先级</label></th>
        <th><label>策略组</label></th>
        <th><label>有效期从</label></th>
        <th><label>有效期至</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
     <c:forEach items="${pageList.list }" var="priceRuleApplyDto">
         <tr class="odd gradeX">
             <td>${priceRuleApplyDto.priority}</td>
             <td><a href="/car/priceRule/priceRuleGroup?id=${priceRuleApplyDto.groupId}">${priceRuleApplyDto.groupName}</a></td>
             <td><fmt:formatDate value="${priceRuleApplyDto.startTime }" pattern="yyy-MM-dd HH:mm:ss"/></td>
             <td><fmt:formatDate value="${priceRuleApplyDto.endTime }" pattern="yyy-MM-dd HH:mm:ss"/></td>
             <td class="text-center">
                 <a onclick="getApplyPriceRule('${priceRuleApplyDto.id }')" >修改</a>&nbsp;&nbsp;
                 <a onclick="deleteApplay('${priceRuleApplyDto.id }');">删除</a>
             </td>
         </tr>
     </c:forEach>
    </tbody>
    <tfoot>
    </tfoot>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=reloadTable"></jsp:include>
</div>
<div class="form-group"><button type="button" class="btn btn-default" onclick="GetPartnerPriceRuleGroup(1)" >分配价格策略组</button></div>



