<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
    table td{
        vertical-align:middle !important;
    }
    table .cbDo{
        width: 18px;
        height: 18px;
    }
    table th{
        vertical-align:middle !important;
        text-align: center;
    }
    table th label{
        padding-top: 3px;
    }

    table .colOut{
        display: none;
    }
    .table>tbody>tr>td:first-child{
        text-align: center;
    }
</style>
<div class="panel panel-default" style="border-top: none;">
    <div class="panel-body">
        <%--top--%>
        <div class="row">
            <div class="col-md-12">
                <!-- 查询条件 -->
                <div class="pull-right">
                    <input id="searchName" class="form-control" name="searchName" type="text" placeholder="客服人员"
                           style="width: 200px;display: initial;" value="${searchName}"/>
                    <button type="button" class="btn btn-default" style="vertical-align: top;" onclick="searchUsers()">查询</button>
                </div>
            </div>
        </div>
        <%--table body--%>
        <table class="table table-striped table-bordered table-hover" style="margin-top: 10px;" id="tableSkill">
            <thead>
            <tr>
                <th rowspan="2" width="200" style="text-align: center;"><label>单位</label></th>
                <th rowspan="2"><label>客服人员</label></th>
                <th colspan="7"><label>工作条线</label></th>
            </tr>
            <tr>
                <th><label>国内机票</label></th>
                <th><label>国际机票</label></th>
                <th><label>酒店</label></th>
                <th><label>火车</label></th>
                <th><label>用车</label></th>
                <th><label>保险</label></th>
                <th><label>需求单</label></th>

                <th class="colOut"><label>国际酒店</label></th>
            </tr>
            </thead>
            <tbody>
                <c:forEach items="${pageList.list}" var="sp">
                    <tr data-orgid="${sp.orgId}" data-userid="${sp.userId}">
                        <td class="tdOrg">${sp.orgName}</td>
                        <td>${sp.fullName}</td>
                        <td><input class="cbDo" type="checkbox" name="doFlight" <c:if test="${sp.serverSkill.doFlight eq 1}">checked</c:if>/></td>
                        <td><input class="cbDo" type="checkbox" name="doFlightOut" <c:if test="${sp.serverSkill.doFlightOut eq 1}">checked</c:if>/></td>
                        <td><input class="cbDo" type="checkbox" name="doHotel" <c:if test="${sp.serverSkill.doHotel eq 1}">checked</c:if>/></td>
                        <td><input class="cbDo" type="checkbox" name="doTrain" <c:if test="${sp.serverSkill.doTrain eq 1}">checked</c:if>/></td>
                        <td><input class="cbDo" type="checkbox" name="doCar" <c:if test="${sp.serverSkill.doCar eq 1}">checked</c:if>/></td>
                        <td><input class="cbDo" type="checkbox" name="doInsurance" <c:if test="${sp.serverSkill.doInsurance eq 1}">checked</c:if>/></td>
                        <td><input class="cbDo" type="checkbox" name="doGeneral" <c:if test="${sp.serverSkill.doGeneral eq 1}">checked</c:if>/></td>

                        <td class="colOut"><input class="cbDo" type="checkbox" name="doHotelOut" <c:if test="${sp.serverSkill.doHotelOut eq 1}">checked</c:if>/></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <div class="bottom-opt">
            <button type="button" class="btn btn-success pull-right" style="width: 100px;" onclick="saveSkill()">保存</button>
        </div>

        <div class="pagin">
            <jsp:include
                    page="/webpage/common/pagination_ajax.jsp?callback=toPage"></jsp:include>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        //合并单元格
       var trs =  $("#tableSkill tbody tr");
       for(var i=0;i<trs.length;) {
           var tr = trs[i];
           var orgId = $(tr).data('orgid');
           var col = 1;
           for(++i;i<trs.length;i++){
               var orgId2 = $(trs[i]).data('orgid');
               if(orgId == orgId2){
                   col ++;
                   $('.tdOrg',trs[i]).remove();
               }else{
                   break;
               }
           }
           if(col > 1) {
               $('.tdOrg', tr).attr('rowspan', col);
           }
       }

       //是否配置国际航班，国际酒店
       var out = 'Y' == '${out}';
       if(out) {
           $('.colOut').show();
       }
    });

    function searchUsers() {
        toPage(1);
    }

    function toPage(pageIndex){
        pageIndex = pageIndex || 1;
        var searchName = $("#searchName").val() || '';
        window.location.href = '/pss/serverPartner/skill?pageIndex='+pageIndex+'&searchName='+searchName;
    }
    
    function saveSkill() {
        var trs =  $("#tableSkill tbody tr");
        var props = ['doFlight','doHotel','doTrain','doInsurance','doGeneral','doCar','doFlightOut','doHotelOut'];
        var userSkills = [];
        $.each(trs, function (i, tr) {
            var skill = {
                userId:$(tr).data('userid')
            };
            $.each(props,function (j,prop) {
                skill[prop] = $('[name=' + prop + ']', tr).prop('checked') ? 1 : 0;
            });
            userSkills.push(skill);
        });
        var loadIndex = layer.load();
        $.ajax({
            url:'pss/serverPartner/saveSkill',
            type:'post',
            data:{
                skills:JSON.stringify(userSkills)
            },
            dataType:'json',
            success:function (data) {
                layer.close(loadIndex);
                layer.msg('保存成功。')
                if(parent.askRefreshOther) {
                    parent.askRefreshOther();
                }
            },
            error:function () {
                layer.close(loadIndex);
                layer.msg('保存失败', {
                    icon: 2,
                    time: 2000
                }, function(){
                    //window.location.reload();
                });

            }
        })
    }
</script>
