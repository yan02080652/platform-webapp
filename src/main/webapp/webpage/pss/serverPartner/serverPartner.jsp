<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
    table td{
        vertical-align:middle !important;
    }
    table th{
        text-align: center;
        vertical-align:middle !important;
    }
    table th label{
        padding-top: 3px;
    }

    table .colOut{
        display: none;
    }

    .fa-user-plus{
        /*display: none;*/
        cursor: pointer;
        font-size: 18px;
    }
    /*.td_users:hover .fa-user-plus{
        display: block;
    }*/
    .fa-close{
        cursor: pointer;
    }
    .has-length {
        color: blue;
        text-decoration: underline;
    }
    .no-length{
        color: grey;
        font-style:italic;
    }
</style>
<div class="panel panel-default" style="border-top: none;">
    <div class="panel-body">
        <%--top--%>
        <div class="row">
            <div class="col-md-12">
                <!-- 查询条件 -->
                <div class="pull-right">
                    <input id="searchName" class="form-control" name="searchName" type="text" placeholder="企业名称"
                           style="width: 200px;display: initial;" value="${searchName}"/>
                    <button type="button" class="btn btn-default" style="vertical-align: top;" onclick="searchUsers()">查询</button>
                </div>
            </div>
        </div>
        <%--table body--%>
        <table class="table table-striped table-bordered table-hover" style="margin-top: 10px;" id="tableSkill">
            <thead>
            <tr>
                <th rowspan="2" width="200"><label>企业</label></th>
                <th rowspan="2" width="300"><label>服务客服</label></th>
                <th colspan="6"><label>工作条线服务人数</label></th>
            </tr>
            <tr>
                <th><label>国内机票</label></th>
                <th><label>国际机票</label></th>
                <th><label>酒店</label></th>
                <th><label>火车</label></th>
                <th><label>保险</label></th>
                <th><label>需求单</label></th>
                <th><label>用车</label></th>

                <th class="colOut"><label>国际酒店</label></th>
            </tr>
            </thead>
            <tbody>
                <c:forEach items="${pageList.list}" var="sp">
                    <tr data-partnerid="${sp.partnerId}" data-name="${sp.partnerName}">
                        <td>${sp.partnerName}</td>
                        <td class="td_users" data-users="${sp.userIds}"><div class='fa fa-user-plus' aria-hidden='true' style='float: right;' title='分配客服' onclick="chooseUsers(this)"></div></td>
                        <td data-users="" data-key="doFlight"></td>
                        <td data-users="" data-key="doFlightOut"></td>
                        <td data-users="" data-key="doHotel"></td>
                        <td data-users="" data-key="doTrain"></td>
                        <td data-users="" data-key="doInsurance"></td>
                        <td data-users="" data-key="doGeneral"></td>
                        <td data-users="" data-key="doCar"></td>

                        <td class="colOut" data-users="" data-key="doHotelOut"></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="pagin">
            <jsp:include
                    page="/webpage/common/pagination_ajax.jsp?callback=toPage"></jsp:include>
        </div>
    </div>
</div>

<div id="dlgChooseUser" style="display: none;">
    <div id="table-left"  class="pull-left" style="width: 250px;">
        <table id="table-users" class="table" style="display:block;">
            <thead>
                <tr><th width="140" id="cs_title">企业客服</th><th width="60">操作</th></tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div id="table-right">
        <table id="table-choosed" class="table" style="display:block;">
            <thead>
                <tr><th width="140">已选择的客服</th><th width="60">操作</th></tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<script>
    var userNameMap = JSON.parse('${userNameMap}');
    var skills = JSON.parse('${skills}');
    var pageVal = {
        userSkill:{}//客服技能列表
    };

    $(document).ready(function () {
        var userSkill = {};
        $.each(skills, function (i, skill) {
            userSkill[skill.userId] = skill;
        });
        pageVal.userSkill = userSkill;
        //填充用户名称
        translateUserName($('.td_users'));
    });

    function searchUsers() {
        toPage(1);
    }

    function toPage(pageIndex){
        pageIndex = pageIndex || 1;
        var searchName = $("#searchName").val() || '';
        window.location.href = '/pss/serverPartner/serverPartner?pageIndex='+pageIndex+'&searchName='+searchName;
    }

    function translateUserName(tds){
        tds.each(function(i,td){
            var userIds = $(td).data('users');
            if(userIds){
                var btn = $('.fa-user-plus',td);
                var uids = userIds.split(',');
                for(var i=0;i<uids.length;i++) {
                    var name = userNameMap[uids[i]];
                    if(name) {
                        $("<span data-userid="+uids[i]+" class='user'>&nbsp;&nbsp;"+name+"</span><i class=\"fa fa-close\" aria-hidden=\"true\" title='移出' onclick='removeCsUser(this,"+uids[i]+")'>").insertBefore(btn);
                        //$("<span></span>").text(name).insertBefore(btn);
                    }
                }
            }
            showTip(td);
            //$(td).html(names.join(',')+"<div class='fa fa-user-plus' aria-hidden='true' style='float: right;' title='分配客服'></div>");
        });

    }

    //deperate
    /*function chooseUser(btn){
        var td = $(btn).parent();
        var key = $(td).data('key');
        var choosedUsers = $(td).data('users');
        var tr = $(td).parent();
        var partnerName = tr.data('name');
        pageVal.curTr = tr;
        var tbody = $('#table-users tbody');
        tbody.children().remove();
        layer.open({
            title:'客服分配 -【'+partnerName+'】',
            type:1,
            area:['450px','550px'],
            content:$('#dlgChooseUser'),
            btn:['确定','取消'],
            btn1:function(index){
                chooseSure();
                layer.close(index);
            }
        });

        var choosedUsers_map = {};
        var choosedUsers_arry = [];
        if(choosedUsers) {
            choosedUsers_arry = choosedUsers.split(',');
            $.each(choosedUsers_arry, function (i, userId) {
                if(userId) {
                    choosedUsers_map[userId] = true;
                }
            });
        }
        //显示客服列表
        getCs(key,function (users) {
            if(users) {
                $.each(users,function (i,user) {
                    var tr = $("<tr/>").data('userid',user.id).data('name',user.fullname);
                    tr.append($('<td/>').text(user.fullname));
                    var opt = null;
                    if(choosedUsers_map[user.id]){
                        opt = $("<i class=\"fa fa-check\" aria-hidden=\"true\"></i>");
                    }else{
                        opt = $("<button>＋</button>").click(function(){
                            addCsUser(this);
                        });
                    }
                    tr.append($('<td/>').append(opt));
                    tbody.append(tr);
                });
            }
        });

        //显示已选择的客服列表
        var tbody = $('#table-choosed tbody');
        tbody.children().remove();
        $.each(choosedUsers_arry,function (i,userId) {
            if(userId) {
                var tr = $("<tr/>").append($("<td/>").text(userNameMap[userId])).append("<td onclick='removeUser(this)'><i class=\"fa fa-close\" aria-hidden=\"true\" title='移出'></i></td>");
                tbody.append(tr);
            }
        });
    }

    var csUsers = {};
    //获取本企业的{key}工作条线客服
    function getCs(callback){
        if(pageVal.csUser)
        var users = csUsers[key];
        if(users){
            callback(users);
            return;
        }
        var loadIndex = layer.load();
        $.ajax({
            type: 'GET',
            url: 'pss/serverPartner/skillUsers',
            data: {
                type: key
            },
            async:false,
            success: function (data) {
                csUsers[key] = data;
                callback(data);
                layer.close(loadIndex);
            },
            error: function () {
                layer.msg('客服人员加载失败');
                layer.close(loadIndex);
            }
        });
    }
    
    function addCsUser(btn) {
        var tr = $(btn).parent().parent();
        $(btn).replaceWith("<i class=\"fa fa-check\" aria-hidden=\"true\"></i>");
        var userId = tr.data('userid');
        var name = tr.data('name');
        var tbody = $('#table-choosed tbody');

        var tr = $("<tr/>").data('userid',userId).data('name',name).append($("<td/>").text(name)).append("<td onclick='removeUser(this)'><i class=\"fa fa-close\" aria-hidden=\"true\" title='移出'></i></td>");
        tbody.append(tr);
        userNameMap[userId] = name;
    }

    function removeUser(removeTd){
        var choosedTr = $(removeTd).parent();
        var choosed_userId = $(choosedTr).data('userid');
        choosedTr.remove();
        $.each( $('#table-users tbody').children(),function (i,child) {
            var userId = $(child).data('userid');
            if(choosed_userId == userId) {
                var btn = $("<button>＋</button>").click(function(){
                    addCsUser(this);
                })
                $('.fa-check',child).replaceWith(btn);
                return false;
            }
        });
    }

    function chooseSure(){

    }*/

    function chooseUsers(btn) {
        pageVal.curBtn = btn;
        TR.select2({
            id:'dlg_relPartner',
            name:'选择客服人员',
            width:500,
            condition:[{
                code:'searchName',
                placeholder:'客服名称',
                enterSearch:true
            }/*,{
             code:'name',
             placeholder:'AAAAAA',
             width:300
             }*/],
            columns:[{
                property:'userName',
                caption:'客服',
                render:function (td,data) {
                    var name = data.fullName;
                    td.text(name);
                }
            },{
                property:'flight',
                caption:'机票',
                render:function (td,data) {
                    console.log(data)
                   var canServer = data.serverSkill?1 == data.serverSkill.doFlight:false;
                   if(canServer) {
                       td.text('√')
                   }
                }
            },{
                property:'hotel',
                caption:'酒店',
                render:function (td,data) {
                    var canServer = data.serverSkill?1 == data.serverSkill.doHotel:false;
                    if(canServer) {
                        td.text('√')
                    }
                }
            },{
                property:'train',
                caption:'火车',
                render:function (td,data) {
                    var canServer = data.serverSkill?1 == data.serverSkill.doTrain:false;
                    if(canServer) {
                        td.text('√')
                    }
                }
            },{
                property:'insurance',
                caption:'保险',
                render:function (td,data) {
                    var canServer = data.serverSkill?1 == data.serverSkill.doInsurance:false;
                    if(canServer) {
                        td.text('√')
                    }
                }
            },{
                property:'general',
                caption:'通用',
                render:function (td,data) {
                    var canServer = data.serverSkill?1 == data.serverSkill.doGeneral:false;
                    if(canServer) {
                        td.text('√')
                    }
                }
            },{
                property:'car',
                caption:'用车',
                render:function (td,data) {
                    var canServer = data.serverSkill?1 == data.serverSkill.doCar:false;
                    if(canServer) {
                        td.text('√')
                    }
                }
            }],
            service:'pss/serverPartner/searchCs',
            hasPage:true,
            forceSeach:true,
            callback:function (rs) {
                var userId = rs.userId;
                var td = $(pageVal.curBtn).parent();
                var choosed_ids = td.data('users');
                var flag = false;
                if(choosed_ids){
                    var ids = choosed_ids.split(',');
                    $.each(ids,function (i,id) {
                        if(userId == id) {
                            flag = true;
                            return false;
                        }
                    });
                }
                if(flag) {
                    layer.msg('已经存在');
                }else{
                    var partnerId = td.parent().data('partnerid');
                    ajaxAdd({
                        partnerId:partnerId,
                        userId:userId
                    },function () {
                        var fullname = rs.fullName;
                        $("<span data-userid="+userId+" class='user'>&nbsp;&nbsp;"+fullname+"</span><i class=\"fa fa-close\" aria-hidden=\"true\" title='移出' onclick='removeCsUser(this,"+userId+")'>").insertBefore(pageVal.curBtn);
                        if(choosed_ids) {
                            choosed_ids += ","+userId;
                        }else{
                            choosed_ids = userId+'';
                        }
                        td.data('users',choosed_ids);
                        pageVal.userSkill[userId] = rs.serverSkill || {};
                        userNameMap[userId] = fullname;
                        showTip($(pageVal.curBtn).parent());
                    });
                }
            }
        });
    }

    function ajaxAdd(data,callback) {
        var loadIndex = layer.load();
        $.ajax({
            url:'pss/serverPartner/addCsPartner',
            type:'post',
            data:data,
            dataType:'json',
            success:function () {
                layer.close(loadIndex);
                callback();
                layer.msg('分配成功');
            },
            error:function () {
                layer.close(loadIndex);
                layer.msg('分配失败，请重试');
            }
        })
    }

    function ajaxDel(data,callback) {
        var loadIndex = layer.load();
        $.ajax({
            url:'pss/serverPartner/removeCsPartner',
            type:'post',
            data:data,
            dataType:'json',
                success:function () {
                layer.close(loadIndex);
                callback();
                layer.msg('删除成功');
            },
            error:function () {
                layer.close(loadIndex);
                layer.msg('删除失败，请重试');
            }
        })
    }

    function removeCsUser(dom,userId){
        var td = $(dom).parent();
        var tr = td.parent();
        var partnerId = tr.data('partnerid');
        ajaxDel({
            partnerId:partnerId,
            userId:userId
        },function () {
            var users = td.data('users');
            var newIds = [];
            if(users) {
                var userIds = users.split(',')
                $.each(userIds,function (i,uid) {
                    if(userId != uid){
                        newIds.push(uid);
                    }
                });
            }
            td.data('users', newIds.join(','));
            showTip(td);

            $(dom).prev().remove();
            $(dom).remove();
        });
    }

    //显示某一行的提示
    function showTip(tdUser){
        var users = $(tdUser).data('users') || '';
        var userIds = users.split(',');
        var skillMap = {
            doFlight:[],
            doHotel:[],
            doTrain:[],
            doGeneral:[],
            doCar:[],
            doInsurance:[],
            doFlightOut:[],
            doHotelOut:[]
        };
        var names = [];
        $.each(userIds,function(i,userId){
            if(userId){
                var skillDto = pageVal.userSkill[userId];
                if(skillDto){
                    $.each(skillMap,function (k,v) {
                        if(1 == skillDto[k]){
                            v.push(userId);
                        }
                    });
                    names.push(userNameMap[userId]);
                }
            }
        });

        var tds = $(tdUser).parent().children();
        tds.each(function (i,td) {
            var tdEl = $(td);
            var key = $(tdEl).data('key');
            if(key) {
                var length = skillMap[key].length;
                tdEl.html("<span class="+(length > 0?"has-length":"no-length")+">"+length+"</span>");
                if(length > 0) {
                    var ns = findNames(skillMap[key]);
                    tdEl.unbind('click').bind('click',function(){
                        layer.tips(ns, this, {
                            tips: [1, '#3595CC'],
                            time: 3000
                        });
                    });
                }
            }
        });
    }

    function findNames(ids){
        var names = [];
        $.each(ids, function (i, id) {
            var name = userNameMap[id];
            names.push(name || id);
        });
        return names.join(',');
    }
</script>
