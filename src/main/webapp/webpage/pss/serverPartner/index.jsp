<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<c:choose>
    <c:when test="${no_cs_role}">
        <div>
            <h3>提示：请先联系管理员设置客服人员的角色编码为<b style="color: red;">CS</b>，再配置客服工作条线分配。</h3>
        </div>
    </c:when>
    <c:otherwise>
        <style>
            iframe{
                border: none;
            }
        </style>
        <link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">
        <script src="resource/plugins/sco/js/sco.panes.js"></script>
        <script src="resource/plugins/sco/js/sco.tab.js"></script>

        <ul id="myTab" class="nav nav-tabs" data-trigger="tab">
            <li><a href="#">工作条线分配</a></li>
            <li><a href="#">企业客服分配</a></li>
        </ul>
        <div class="pane-wrapper">
            <div><iframe src="pss/serverPartner/skill" width="100%" height="900"></iframe></div>
            <div><iframe id="framePartner" src="pss/serverPartner/serverPartner" width="100%" height="900"></iframe></div>
        </div>

        <script>
            function askRefreshOther() {
                document.getElementById('framePartner').contentWindow.location.reload(true);
            }
        </script>
    </c:otherwise>
</c:choose>

