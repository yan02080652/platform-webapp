<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
    table td{
        vertical-align:middle !important;
    }
</style>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>服务费模版
    </div>
    <div class="panel-body">
        <%--top--%>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <a class="btn btn-default" onclick="addTmpl()" role="button">新增模版</a>
                </div>
                <!-- 查询条件 -->
                <div class="pull-right">
                    <input id="tmplName_con" class="form-control" name="tmplName_con" type="text" placeholder="模版名称/关联企业名称"
                           style="width: 200px;display: initial;" value="${name}"/>
                    <button type="button" class="btn btn-default" onclick="searchTmpl()">查询</button>
                    <button type="button" class="btn btn-default" onclick="resetTmpl()">重置</button>
                </div>
            </div>
        </div>
        <%--table body--%>
        <table class="table table-striped table-bordered table-hover" style="margin-top: 10px;">
            <thead>
            <tr>
                <th><label>模版名称</label></th>
                <th width="120"><label>是否默认模版</label></th>
                <th><label>创建时间</label></th>
                <th><label>创建人</label></th>
                <th><label>最后修改时间</label></th>
                <th><label>最后修改人</label></th>
                <th><label>操作</label></th>
            </tr>
            </thead>
            <tbody>
                <c:forEach items="${list}" var="sc">
                    <tr id="tmpl_${sc.id}">
                        <td>${sc.name}</td>
                        <td><c:if test="${sc.defaultTmpl eq 1}">是</c:if><c:if test="${sc.defaultTmpl ne 1}">否</c:if></td>
                        <td><fmt:formatDate value="${sc.createTime }" pattern="yyy-MM-dd HH:mm:ss"/></td>
                        <td>${sc.createUserName}</td>
                        <td><fmt:formatDate value="${sc.updateTime }" pattern="yyy-MM-dd HH:mm:ss"/></td>
                        <td>${sc.updateUserName}</td>
                        <td width="210" style="text-align: left">
                            <c:if test="${sc.defaultTmpl ne 1}">
                                <a href="pss/serverCharge/relPartner?tmplId=${sc.id }">关联企业</a>&nbsp;&nbsp;
                            </c:if>
                            <a href="pss/serverCharge/detail?tmplId=${sc.id}">修改</a>&nbsp;&nbsp;
                            <a href="pss/serverCharge/detail?copy=Y&tmplId=${sc.id}">复制</a>&nbsp;&nbsp;
                            <c:if test="${sc.defaultTmpl ne 1}">
                                <a onclick="deleteTmpl(${sc.id })">删除</a>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</div>

<script>
    $('#tmplName_con').keyup(function (e) {
        if(e.key == "Enter"){
            searchTmpl();
        }
    });

    //新增模版
    function addTmpl(){
        window.location.href = '/pss/serverCharge/detail';
    }

    //搜索
    function searchTmpl() {
        var name = $('#tmplName_con').val();
        name = $.trim(name);
        var url = 'pss/serverCharge';
        if(name){
            url += "?name="+name;
        }
        window.location.href= url;
    }

    //重置
    function resetTmpl() {
        window.location.href= '/pss/serverCharge';
    }

    //删除模版
    function deleteTmpl(id){
        $.getJSON('pss/serverCharge/countRelPartner',{
            tmplId:id
        },function(rs){
            if(rs.count > 0){
                layer.alert('无法删除，因为该服务费模版存在关联企业',{
                    skin: 'layui-layer-molv'
                });
            }else{
                layer.confirm('确实要永久性地删除此服务费模版吗？', function(){
                    $.getJSON('pss/serverCharge/delete',{
                        tmplId:id
                    },function (data) {
                        console.log(data)
                        if(data.code > 0){
                            layer.msg('删除成功。');
                            $('#tmpl_'+id).remove();
                        }
                    });
                });

            }
        });
    }


</script>
