<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link href="webpage/pss/serverCharge/detail.css?version=${globalVersion}" rel="stylesheet">
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        <c:choose>
            <c:when test="${not empty detail.tmplId}">
                修改
            </c:when>
            <c:otherwise>
                新增
            </c:otherwise>
        </c:choose>
        服务费模版&nbsp;<a href="pss/serverCharge">返回</a>
       <%-- <span class="pull-right">
            模版名称<input type="text" id="tmplName" value="${detail.name}"/>
        </span>--%>
    </div>
    <div class="panel-body" style="background-color: #F8F8F8;">
        <form id="formServerCharge" class="form-horizontal" method="post">
            <input type="hidden" name="tmplId" class="form-control" value="${detail.tmplId}">
            <div style="height: 35px;">
                &nbsp;&nbsp;模版名称<input type="text" id="tmplName" value="${detail.name}"/>
                <span class="pull-right">
                    <input type="checkbox" id="cbNotInOrder" style="vertical-align:text-top;" <c:if test="${detail.notInOrder eq 1}">checked</c:if>>
                    <label for="cbNotInOrder" style="margin-left: 2px;">服务费不纳入订单金额</label>
                </span>
            </div>
            <div class="bar-title">在线下单服务目录</div>
            <table class="severChargeTable">
                <thead>
                    <td width="200">国内机票</td>
                    <td width="200">方式</td>
                    <td width="200">预订</td>
                    <td width="200">退订</td>
                    <td width="200">变更</td>
                </thead>
                <tbody>
                    <tr>
                        <td class="small-title">全部</td>
                        <td>
                            <select name="typeF10_calcType">
                                <option value="1" <c:if test="${detail.typeMap.F10.calcType eq 1}">selected</c:if>>按订单</option>
                                <option value="2" <c:if test="${detail.typeMap.F10.calcType eq 2}">selected</c:if>>按人数</option>
                            </select>
                        </td>
                        <td>
                            <input type="number" name="typeF10_book" value="${detail.typeMap.F10.book}">
                        </td>
                        <td>
                            <input type="number" name="typeF10_back" value="${detail.typeMap.F10.back}">
                        </td>
                        <td>
                            <input type="number" name="typeF10_change" value="${detail.typeMap.F10.change}">
                        </td>
                    </tr>
                </tbody>
            </table>
            <table class="severChargeTable">
                <thead>
                    <td width="200">国内酒店</td>
                    <td width="200">方式</td>
                    <td width="200">预订</td>
                    <td width="200">退订</td>
                    <td width="200">变更</td>
                </thead>
                <tbody>
                <tr>
                    <td class="small-title">到店付/担保</td>
                    <td>
                        <select name="typeH20_calcType">
                            <option value="1" <c:if test="${detail.typeMap.H20.calcType eq 1}">selected</c:if>>按订单</option>
                            <option value="3" <c:if test="${detail.typeMap.H20.calcType eq 3}">selected</c:if>>按间夜数</option>
                            <option value="4" <c:if test="${detail.typeMap.H20.calcType eq 4}">selected</c:if>>按房间数</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" name="typeH20_book" value="${detail.typeMap.H20.book}">
                    </td>
                    <td>
                        <input type="number" name="typeH20_back" value="${detail.typeMap.H20.back}">
                    </td>
                    <td>
                        <input type="number" name="typeH20_change" value="${detail.typeMap.H20.change}">
                    </td>
                </tr>
                <tr>
                    <td class="small-title">预付</td>
                    <td>
                        <select name="typeH21_calcType">
                            <option value="1" <c:if test="${detail.typeMap.H21.calcType eq 1}">selected</c:if>>按订单</option>
                            <option value="3" <c:if test="${detail.typeMap.H21.calcType eq 3}">selected</c:if>>按间夜数</option>
                            <option value="4" <c:if test="${detail.typeMap.H21.calcType eq 4}">selected</c:if>>按房间数</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" name="typeH21_book" value="${detail.typeMap.H21.book}">
                    </td>
                    <td>
                        <input type="number" name="typeH21_back" value="${detail.typeMap.H21.back}">
                    </td>
                    <td>
                        <input type="number" name="typeH21_change" value="${detail.typeMap.H21.change}">
                    </td>
                </tr>
                <tr>
                    <td class="small-title">协议</td>
                    <td>
                        <select name="typeH22_calcType">
                            <option value="1" <c:if test="${detail.typeMap.H22.calcType eq 1}">selected</c:if>>按订单</option>
                            <option value="3" <c:if test="${detail.typeMap.H22.calcType eq 3}">selected</c:if>>按间夜数</option>
                            <option value="4" <c:if test="${detail.typeMap.H22.calcType eq 4}">selected</c:if>>按房间数</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" name="typeH22_book" value="${detail.typeMap.H22.book}">
                    </td>
                    <td>
                        <input type="number" name="typeH22_back" value="${detail.typeMap.H22.back}">
                    </td>
                    <td>
                        <input type="number" name="typeH22_change" value="${detail.typeMap.H22.change}">
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="severChargeTable">
                <thead>
                <td width="200">国内火车</td>
                <td width="200">方式</td>
                <td width="200">预订</td>
                <td width="200">退订</td>
                <td width="200">变更</td>
                </thead>
                <tbody>
                    <tr>
                        <td class="small-title">全部</td>
                        <td>
                            <select name="typeT30_calcType">
                                <option value="1" <c:if test="${detail.typeMap.T30.calcType eq 1}">selected</c:if>>按订单</option>
                                <option value="2" <c:if test="${detail.typeMap.T30.calcType eq 2}">selected</c:if>>按人数</option>
                            </select>
                        </td>
                        <td>
                            <input type="number" name="typeT30_book" value="${detail.typeMap.T30.book}">
                        </td>
                        <td>
                            <input type="number" name="typeT30_back" value="${detail.typeMap.T30.back}">
                        </td>
                        <td>
                            <input type="number" name="typeT30_change" value="${detail.typeMap.T30.change}">
                        </td>
                    </tr>
                    <tr>
                        <td class="small-title">抢票</td>
                        <td>
                            <select name="typeHomeTrainGrab_calcType">
                                <option value="2" >按人数</option>
                            </select>
                        </td>
                        <td>
                            <input type="number" name="typeHomeTrainGrab_book" value="${detail.typeMap.HomeTrainGrab.book}">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="bar-title">代客下单
                <span class="pull-right">
                    <input id="sameWithBook" name="sameWithBook" type="checkbox" <c:if test="${detail.sameWithBook eq 1}">checked</c:if>/>
                    <label for="sameWithBook" class="lb-sameWithBook">与在线预订一致</label>
                </span>
            </div>
            <table class="severChargeTable helpTable">
                <thead>
                <td width="200">国内机票</td>
                <td width="200">方式</td>
                <td width="200">预订</td>
                <td width="200">退订</td>
                <td width="200">变更</td>
                </thead>
                <tbody>
                <tr>
                    <td class="small-title">全部</td>
                    <td>
                        <select name="typeHF40_calcType">
                            <option value="1" <c:if test="${detail.typeMap.HF40.calcType eq 1}">selected</c:if>>按订单</option>
                            <option value="2" <c:if test="${detail.typeMap.HF40.calcType eq 2}">selected</c:if>>按人数</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" name="typeHF40_book" value="${detail.typeMap.HF40.book}">
                    </td>
                    <td>
                        <input type="number" name="typeHF40_back" value="${detail.typeMap.HF40.back}">
                    </td>
                    <td>
                        <input type="number" name="typeHF40_change" value="${detail.typeMap.HF40.change}">
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="severChargeTable helpTable">
                <thead>
                <td width="200">国内酒店</td>
                <td width="200">方式</td>
                <td width="200">预订</td>
                <td width="200">退订</td>
                <td width="200">变更</td>
                </thead>
                <tbody>
                <tr>
                    <td class="small-title">到店付/担保</td>
                    <td>
                        <select name="typeHH50_calcType">
                            <option value="1" <c:if test="${detail.typeMap.HH50.calcType eq 1}">selected</c:if>>按订单</option>
                            <option value="3" <c:if test="${detail.typeMap.HH50.calcType eq 3}">selected</c:if>>按间夜数</option>
                            <option value="4" <c:if test="${detail.typeMap.HH50.calcType eq 4}">selected</c:if>>按房间数</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" name="typeHH50_book" value="${detail.typeMap.HH50.book}">
                    </td>
                    <td>
                        <input type="number" name="typeHH50_back" value="${detail.typeMap.HH50.back}">
                    </td>
                    <td>
                        <input type="number" name="typeHH50_change" value="${detail.typeMap.HH50.change}">
                    </td>
                </tr>
                <tr>
                    <td class="small-title">预付</td>
                    <td>
                        <select name="typeHH51_calcType">
                            <option value="1" <c:if test="${detail.typeMap.HH51.calcType eq 1}">selected</c:if>>按订单</option>
                            <option value="3" <c:if test="${detail.typeMap.HH51.calcType eq 3}">selected</c:if>>按间夜数</option>
                            <option value="4" <c:if test="${detail.typeMap.HH51.calcType eq 4}">selected</c:if>>按房间数</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" name="typeHH51_book" value="${detail.typeMap.HH51.book}">
                    </td>
                    <td>
                        <input type="number" name="typeHH51_back" value="${detail.typeMap.HH51.back}">
                    </td>
                    <td>
                        <input type="number" name="typeHH51_change" value="${detail.typeMap.HH51.change}">
                    </td>
                </tr>
                <tr>
                    <td class="small-title">协议</td>
                    <td>
                        <select name="typeHH52_calcType">
                            <option value="1" <c:if test="${detail.typeMap.HH52.calcType eq 1}">selected</c:if>>按订单</option>
                            <option value="3" <c:if test="${detail.typeMap.HH52.calcType eq 3}">selected</c:if>>按间夜数</option>
                            <option value="4" <c:if test="${detail.typeMap.HH52.calcType eq 4}">selected</c:if>>按房间数</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" name="typeHH52_book" value="${detail.typeMap.HH52.book}">
                    </td>
                    <td>
                        <input type="number" name="typeHH52_back" value="${detail.typeMap.HH52.back}">
                    </td>
                    <td>
                        <input type="number" name="typeHH52_change" value="${detail.typeMap.HH52.change}">
                    </td>
                </tr>
                </tbody>
            </table>
            <table class="severChargeTable helpTable">
                <thead>
                <td width="200">国内火车</td>
                <td width="200">方式</td>
                <td width="200">预订</td>
                <td width="200">退订</td>
                <td width="200">变更</td>
                </thead>
                <tbody>
                <tr>
                    <td class="small-title">全部</td>
                    <td>
                        <select name="typeHT60_calcType">
                            <option value="1" <c:if test="${detail.typeMap.HT60.calcType eq 1}">selected</c:if>>按订单</option>
                            <option value="2" <c:if test="${detail.typeMap.HT60.calcType eq 2}">selected</c:if>>按人数</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" name="typeHT60_book" value="${detail.typeMap.HT60.book}">
                    </td>
                    <td>
                        <input type="number" name="typeHT60_back" value="${detail.typeMap.HT60.back}">
                    </td>
                    <td>
                        <input type="number" name="typeHT60_change" value="${detail.typeMap.HT60.change}">
                    </td>
                </tr>
                </tbody>
            </table>



           <%-- <div class="type-row">
                <label class="small-title">国内机票</label>
                <label>出票</label>
                <input type="number" name="type10" value="${typeMap.a10}">
                <label>退票</label>
                <input type="number" name="type11" value="${typeMap.a11}">
                <label>改签</label>
                <input type="number" name="type12" value="${typeMap.a12}">
            </div>
            <div class="type-row">
                <label class="small-title">国内酒店</label>
                <label>现付/担保</label>
                <input type="number" name="type20" value="${typeMap.H20}">
                <label>预付</label>
                <input type="number" name="type21" value="${typeMap.H21}">
                <label>协议</label>
                <input type="number" name="type22" value="${typeMap.H22}">
            </div>
            <div class="type-row">
                <label class="small-title">国内火车</label>
                <label>出票</label>
                <input type="number" name="type30" value="${typeMap.T30}">
                <label>退票</label>
                <input type="number" name="type31" value="${typeMap.T31}">
                <label>改签</label>
                <input type="number" name="type32" value="${typeMap.T32}">
            </div>--%>
            <%--<div class="bar-title">代客下单
                <span class="pull-right">
                    <input id="sameWithBook" name="sameWithBook" type="checkbox" on <c:if test="${detail.sameWithBook eq 1}">checked</c:if>/>
                    <label for="sameWithBook" class="lb-sameWithBook">与在线预订一致</label>
                </span>
            </div>--%>
            <%--<div class="type-row">
                <label class="small-title">国内机票</label>
                <label>出票</label>
                <input type="number" name="type40" value="${typeMap.a40}">
                <label>退票</label>
                <input type="number" name="type41" value="${typeMap.a41}">
                <label>改签</label>
                <input type="number" name="type42" value="${typeMap.a42}">
            </div>
            <div class="type-row">
                <label class="small-title">国内酒店</label>
                <label>现付/担保</label>
                <input type="number" name="type50" value="${typeMap.a50}">
                <label>预付</label>
                <input type="number" name="type51" value="${typeMap.a51}">
                <label>协议</label>
                <input type="number" name="type52" value="${typeMap.a52}">
            </div>
            <div class="type-row">
                <label class="small-title">国内火车</label>
                <label>出票</label>
                <input type="number" name="type60" value="${typeMap.a60}">
                <label>退票</label>
                <input type="number" name="type61" value="${typeMap.a61}">
                <label>改签</label>
                <input type="number" name="type62" value="${typeMap.a62}">
            </div>--%>
            <div class="bar-title">线下服务（需求单 & 导入）</div>
            <%--<div class="type-row">
                <label class="small-title">国内机票</label>
                <label>出票</label>
                <input type="number" name="type70" value="${typeMap.a70}">
                <label>退票</label>
                <input type="number" name="type71" value="${typeMap.a71}">
                <label>改签</label>
                <input type="number" name="type72" value="${typeMap.a72}">
            </div>
            <div class="type-row">
                <label class="small-title">国内酒店</label>
                <label>现付/担保</label>
                <input type="number" name="type80" value="${typeMap.a80}">
                <label>预付</label>
                <input type="number" name="type81" value="${typeMap.a81}">
                <label>协议</label>
                <input type="number" name="type82" value="${typeMap.a82}">
            </div>
            <div class="type-row">
                <label class="small-title">国内火车</label>
                <label>出票</label>
                <input type="number" name="type90" value="${typeMap.a90}">
                <label>退票</label>
                <input type="number" name="type91" value="${typeMap.a91}">
                <label>改签</label>
                <input type="number" name="type92" value="${typeMap.a92}">
            </div>
            <div class="type-row">
                <label class="small-title">签证</label>
                <label>东南亚纸质</label>
                <input type="number" name="type100" value="${typeMap.a100}">
                <label>除东南亚外纸质</label>
                <input type="number" name="type101" value="${typeMap.a101}">
                <label>电子签证</label>
                <input type="number" name="type102" value="${typeMap.a102}">
                <label>加急</label>
                <input type="number" name="type103" value="${typeMap.a103}">
            </div>
            <div class="type-row">
                <label class="small-title">用车</label>
                <label>机场举牌</label>
                <input type="number" name="type110" value="${typeMap.a110}">
                <label>接送机(6点-23点)</label>
                <input type="number" name="type111" value="${typeMap.a111}">
                <label>接送机(23点-6点)</label>
                <input type="number" name="type112" value="${typeMap.a112}">
                <label>国内租车</label>
                <input type="number" name="type113" value="${typeMap.a113}">
                <label>国际租车</label>
                <input type="number" name="type114" value="${typeMap.a114}">
            </div>
            <div class="type-row">
                <label class="small-title">其他</label>
                <label>机场VIP</label>
                <input type="number" name="type120" value="${typeMap.a120}">
                <label>旅游意外险</label>
                <input type="number" name="type121" value="${typeMap.a121}">
            </div>--%>
            <div class="bottom-opt">
                <button type="button" class="btn btn-success" style="width: 100px;" onclick="saveDetail()">保存</button>
            </div>
        </form>

        <table class="severChargeTable cloneTable">
            <thead>
            <td width="200">国内火车</td>
            <td width="200">方式</td>
            <td width="200">预订</td>
            <td width="200">退订</td>
            <td width="200">变更</td>
            </thead>
            <tbody>
            </tbody>
        </table>

    </div>
</div>
<script>
    var detailTypeMap = {};
    <c:if test="${not empty detailTypeMap}">
        detailTypeMap = ${detailTypeMap};
    </c:if>
</script>
<script type="text/javascript" src="webpage/pss/serverCharge/detail.js?version=${globalVersion}"></script>