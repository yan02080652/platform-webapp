$(document).ready(function(){
    bingEvent();
});

//在线服务与代客下单服务对应关系
var typeMapRel = {
    F10:'HF40',
    H20:'HH50',
    H21:'HH51',
    H22:'HH52',
    T30:'HT60',
    HomeTrainGrab: 'HomeTTrainGrab'
};

//绑定页面时间
function bingEvent(){
    $.extend($.validator.messages, {
        min: $.validator.format("不能低于 {0} ")
    });

    $.each($('table [type=number]'),function (i,input) {
        var money = $(input).val();
        if(!money){
            $(input).val(0);
        }else{
            var money = parseFloat(money);
            $(input).val(money);
        }
    });

    bindSameWithBook();

    var form = $('#formServerCharge');

    var types = ['F10','H20','H21','H22','T30','HF40','HH50','HH51','HH52','HT60', 'HomeTrainGrab'];

    //加载字典数据
    var dicData = loadDicData();
    //创建字典对应的服务费场景
    var ddrs = createDicDom(dicData);
    if(ddrs.codes){
        types = types.concat(ddrs.codes);
    }

    var rules = {};
    $.each(types,function (i,code) {
       rules['type'+code+'_book'] = {
           digits:true,
           min:0
       };
        rules['type'+code+'_back'] = {
            digits:true,
            min:0
        };
        rules['type'+code+'_change'] = {
            digits:true,
            min:0
        };
    });

    $('#formServerCharge').validate({
        rules:rules,
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight,
        submitHandler:function (form) {
            //检查模版名称
            var rs = checkTmplName();
            if(!rs) {
                return;
            }

            var formData = $(form).serializeArray();
            var data = {
                details:[]
            };

            var typeMapValue = {};
            $.each(formData,function (i,d) {
               if(d.name.startWith('type')){
                   /*var v = parseInt(d.value);
                   if(v != 0){
                       typeMap[d.name.substr(4)] = d.value;
                   }*/
                   typeMapValue[d.name] = d.value;
               } else{
                   data[d.name] = d.value;
               }
            });
            data.tmplId = data.tmplId?parseInt(data.tmplId):null;
            data.sameWithBook = $('#sameWithBook').is(':checked')?1:0;

            var helpInput = {HF40:'F10',HH50:'H20',HH51:'H21',HH52:'H22',HT60:'T30'};
            $.each(types,function (i,type) {
                var sence = type;
                //代客下单与在线服务一直
                if(data.sameWithBook == 1 && helpInput[type]){
                    type = helpInput[type];
                }
                var detail = {
                    type:sence,
                    calcType:typeMapValue['type'+type+'_calcType'] || 1,
                    book:typeMapValue['type'+type+'_book'] || 0,
                    back:typeMapValue['type'+type+'_back'] || 0,
                    change:typeMapValue['type'+type+'_change'] || 0
                };
                data.details.push(detail)
            });

            data.name = $('#tmplName').val();
            data.notInOrder = $('#cbNotInOrder').is(':checked')?1:0;

            var loadingIndex = layer.load();
            $.ajax({
                type:'POST',
                url: 'pss/serverCharge/save',
                data:{
                    data:JSON.stringify(data)
                },
                dataType:'json',
                success:function (rs) {
                    layer.close(loadingIndex);
                    if('Y' == rs.code){
                        layer.msg('保存成功，即将返回列表页');
                        setTimeout(function(){
                            window.location.href = '/pss/serverCharge';
                        },200);
                    }else{
                        layer.msg('保存失败');
                    }
                },
                error:function () {
                    layer.close(loadingIndex);
                    layer.msg('保存失败');
                }
            });

        }
    });
};


//绑定与在线预订一致事件
function bindSameWithBook() {
    readOnlyHelp();
    $('#sameWithBook').click(readOnlyHelp);
    var form = $('#formServerCharge');
    $.each(typeMapRel,function (k, toK) {
        $("[name=type"+k+"_calcType]", form).change(function () {
            syncCharge($(this).val(),'type'+toK+'_calcType');
        });
        $("[name=type"+k+"_book]", form).change(function () {
            syncCharge($(this).val(),'type'+toK+'_book');
        });
        $("[name=type"+k+"_back]", form).change(function () {
            syncCharge($(this).val(),'type'+toK+'_back');
        });
        $("[name=type"+k+"_change]", form).change(function () {
            syncCharge($(this).val(),'type'+toK+'_change');
        });
    });
    function syncCharge(value,toType) {
        if($('#sameWithBook').is(':checked')){
            $("[name="+toType+"]", form).val(value);
        }
    }
}

function loadDicData(){
    var rs = null;
    $.ajax({
        type:'GET',
        url:'platform/syscfgCatalogCfg/getCfgDicTreeTable?sysId=OTA&dictCode=GENERAL_SERVICE_TYPE',
        async:false,
        dataType:'json',
        success:function(data){
            rs = data;
        },
        error:function () {
            console.error('字典GENERAL_SERVICE_TYPE子项加载失败');
            rs = [];
        }
    });
    return rs;
}

//创建字典对应的服务费场景
//只会构建二级项，一级项取其标题
function createDicDom(dicData){
    var rs = {};

    var childrenMap = {};
    $.each(dicData,function (i,d) {
        if(d.pid){
            var children = childrenMap[d.pid];
            if(children){
                children.push(d);
            }else{
                childrenMap[d.pid] = [d];
            }
        }
    });

    var btn = $('.bottom-opt');

    var codes = [];
    $.each(dicData,function (i,d) {
        if(!d.pid){
            var children = childrenMap[d.id];
            if(children){
                var table = $('.cloneTable').clone().removeClass('cloneTable');
                $('thead td:first',table).text(d.itemTxt);
                btn.before(table);

                $.each(children,function (j,child) {
                    if(child.itemCode){
                        var itemData = detailTypeMap[child.itemCode] || {};

                        var tr = $("<tr><td class=\"small-title\">"+child.itemTxt+"</td></tr>");
                        var key = 'type'+ child.itemCode;
                        var select = $("<select name='"+key+"_calcType'><option value='1'>按订单</option><option value='2'>按人数</option></select>");
                        select.val(itemData.calcType || 1);
                        tr.append($("<td/>").append(select));
                        var input = $("<input type='number' name='"+key+"_book'>").val(itemData.book || 0);
                        tr.append($("<td/>").append(input));
                        var input = $("<input type='number' name='"+key+"_back'>").val(itemData.back || 0);
                        tr.append($("<td/>").append(input));
                        var input = $("<input type='number' name='"+key+"_change'>").val(itemData.change || 0);
                        tr.append($("<td/>").append(input));

                        $('tbody',table).append(tr);
                        codes.push(child.itemCode);
                    }
                });

               /* var row = $("<div class='type-row'/>");
                row.append($("<label class='small-title'/>").text(d.itemTxt));

                $.each(children,function (j,child) {
                    if(child.itemCode){
                        row.append("<label>"+child.itemTxt+"</label>");
                        var input = $("<input type='number' name='type"+child.itemCode+"'>");
                        input.val(detailTypeMap[child.itemCode] || 0);
                        row.append(input);
                        codes.push(child.itemCode);
                    }
                });
                btn.before(row);*/
            }
        }
    });
    rs.codes = codes;
    return rs;
}

//保存
function saveDetail(){
    $('#formServerCharge').submit();
};

//检查模版名称唯一性
function checkTmplName() {
    var tmplNameDom = $('#tmplName');
    var tmplName = tmplNameDom.val();
    if(!tmplName){
        layer.msg('模版名称不能为空');
        setTimeout(function(){
            tmplNameDom.focus()
        },200);
        return false;
    }

    var tmplId = $('#formServerCharge [name=tmplId]').val();
    var ok = false;
    $.ajax({
        type:'POST',
        url:'pss/serverCharge/checkTmplName',
        async:false,
        data:{
            tmplId:tmplId,
            tmplName:tmplName
        },
        dataType:'json',
        success:function (rs) {
            ok = 'Y' == rs.code;
            if(!ok){
                layer.msg('模版名称重复，请更换');
                setTimeout(function(){
                    tmplNameDom.focus();
                },200);
            }

        }
    });
    return ok;
}

//禁用代客下单
function readOnlyHelp() {
    var form = $('#formServerCharge');
    if($('#sameWithBook').is(':checked')){
        $('.helpTable input,.helpTable select').attr('disabled','disabled').addClass('readonlyCol');
        $.each(typeMapRel,function (k, toK) {
            $('[name=type'+toK+'_calcType]',form).val($("[name=type"+k+"_calcType]", form).val());
            $('[name=type'+toK+'_book]',form).val($("[name=type"+k+"_book]", form).val());
            $('[name=type'+toK+'_back]',form).val($("[name=type"+k+"_back]", form).val());
            $('[name=type'+toK+'_change]',form).val($("[name=type"+k+"_change]", form).val());
        });
    }else{
        $('.helpTable input,.helpTable select').removeAttr('disabled').removeClass('readonlyCol');
    }
}