$(document).ready(function () {
    $('#cb-all').change(function () {
        var checked = $(this).is(':checked');
        $(".cb-partner").prop('checked',checked);
    });

    $(".cb-partner").change(function () {
        if(!$(this).is(':checked')) {
            $('#cb-all').prop('checked',false);
        }
    });
});

function moveOut(e) {
    var cb_checked = null;
    if(e){
        cb_checked = $(e).parent().parent().find('.cb-partner');
    }else{//批量移出
        cb_checked = $(".cb-partner:checked");
        if(cb_checked.length < 1) {
            layer.msg('至少选择一个企业');
            return;
        }
    }


    TR.select2({
        id:'dlg_serverChargeTmpl',
        name:'选择服务费模版',
        width:400,
        condition:[{
            code:'name',
            placeholder:'模版名称',
            enterSearch:true
        }],
        columns:[{
            property:'name',
            caption:'服务费模版名称'
        }],
        service:'pss/serverCharge/searchServerChargeTmpl',
        callback:function (rs) {
            if(rs && rs.id) {
                if(rs.id == tmplId) {
                    layer.msg('因服务费模版没有变化，移出失败');
                    return;
                }

                var relPartners = [];
                cb_checked.each(function (i,cb) {
                    var cbDom = $(cb);
                    relPartners.push({
                        partnerId:cbDom.data('partnerid'),
                        partnerName:cbDom.data('partnername'),
                        partnerStatus:cbDom.data('partnerstatus'),
                        tmplId:rs.defaultTmpl == 1?null:rs.id
                    });
                });
                //console.log(relPartners)
                moveRelPartner(relPartners);
            }
        }
    });
}

function deleteRel(e){
    layer.confirm('确认删除该企业模版关联信息？', {
        btn: ['确定','取消'] //按钮
    }, function(index){
        var cbDom = $(e).parent().parent().find('.cb-partner');
        var relPartners = [{
            partnerId:cbDom.data('partnerid'),
            partnerName:cbDom.data('partnername'),
            partnerStatus:cbDom.data('partnerstatus')
        }];
        moveRelPartner(relPartners);
        layer.close(index);
    });
}

function moveIn() {
    TR.select2({
        id:'dlg_relPartner',
        name:'选择合作伙伴',
        width:500,
        condition:[{
            code:'name',
            placeholder:'企业名称',
            enterSearch:true
        }/*,{
            code:'name',
            placeholder:'AAAAAA',
            width:300
        }*/],
        columns:[{
            property:'partnerName',
            caption:'名称'
        },{
            property:'tmplName',
            caption:'服务费模版',
            width:150
        }],
        service:'pss/serverCharge/searchRelPartner',
        hasPage:true,
        forceSeach:true,
        callback:function (rs) {
            if(rs && rs.partnerId) {
                rs.tmplId = tmplId;
                var relPartners = [rs];
                moveRelPartner(relPartners);
            }
        }
    });
}

function moveRelPartner(relPartners) {
    var loadingIndex = layer.load();
    $.ajax({
        type: 'POST',
        url: 'pss/serverCharge/moveIn',
        data: {
            relPartnerFormJson: JSON.stringify(relPartners)
        },
        dataType: 'JSON',
        success: function (rs) {
            if(rs && rs.count > 0){
                var pageIndex = parseInt(currentPage) - 1;
                toPage(pageIndex);
            }
            layer.close(loadingIndex);
        },
        error:function () {
            layer.close(loadingIndex);
        }
    });
}
/**
 * 查询
 */
function searchRelPartner(parameter) {
    var param = $.extend({
        partnerStatus:$('#partnerStatus').val(),
        partnerName:$('#partnerName').val()
    },parameter);
    toRelPartner(param);
}

function resetRelPartner() {
    toRelPartner();
}

function toRelPartner(params) {
    var baseUrl = 'pss/serverCharge/relPartner?tmplId='+tmplId;
    if(params) {
        $.each(params,function (key,value) {
            if(value){
                baseUrl += "&" + key + "=" + value;
            }
        });
    }
    window.location.href = baseUrl;
}

/**
 * 跳到某页
 * @param pageIndex
 */
function toPage(pageIndex) {
    searchRelPartner({
        pageIndex:pageIndex
    });
}