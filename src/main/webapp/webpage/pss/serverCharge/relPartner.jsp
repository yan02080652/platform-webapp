<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link href="webpage/pss/serverCharge/detail.css?version=${globalVersion}" rel="stylesheet">
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
        服务费模版&nbsp;设置关联企业&nbsp;<a href="pss/serverCharge">返回</a>
        <span class="pull-right">
            模版名称<input type="text" id="tmplName" value="${tmplName}" disabled/>
            <input type="hidden" id="tmplId" value="${tmplId}">
        </span>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-left">
                    <a class="btn btn-default" onclick="moveIn()"role="button">移入</a>
                    <a class="btn btn-default" onclick="moveOut()"role="button">批量移出</a>
                </div>
                <!-- 查询条件 -->
                <form class="form-inline" method="post" >
                    <nav class="text-right">
                        <div class="form-group">
                            <label class="control-label">企业状态</label>
                            <select id="partnerStatus" class="form-control">
                                <option value="-1" <c:if test="${partnerStatus eq -1}">selected</c:if>>不限</option>
                                <option value="0"<c:if test="${partnerStatus eq 0}">selected</c:if>>可用</option>
                                <option value="1" <c:if test="${partnerStatus eq 1}">selected</c:if>>不可用</option>
                            </select>
                        </div>
                        &nbsp;<div class="form-group">
                            <input id="partnerName" type="text" value="${partnerName}" class="form-control" placeholder="企业名称" style="width: 150px"/>
                        </div>
                        <div class="form-group" style="margin-left: 10px">
                            <button type="button" class="btn btn-default" onclick="searchRelPartner()">查询</button>
                            <button type="button" class="btn btn-default" onclick="resetRelPartner()">重置</button>
                        </div>
                    </nav>
                </form>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" style="margin-top: 10px;">
            <thead>
            <tr>
                <th width="50" style="text-align: center;"><input type="checkbox" id="cb-all"/></th>
                <th style="text-align: left;"><label>企业名称</label></th>
                <th width="100"><label>企业状态</label></th>
                <th width="200"><label>关联时间</label></th>
                <th width="150"><label>关联操作人</label></th>
                <th width="130"><label>操作</label></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${pageList.list }" var="li">
                <tr>
                    <td style="text-align: center;">
                        <input type="checkbox" class="cb-partner" data-partnerid="${li.partnerId}" data-partnername="${li.partnerName}" data-partnerstatus="${li.partnerStatus}"/>
                    </td>
                    <td style="text-align: left;">${li.partnerName}</td>
                    <td><c:if test="${li.partnerStatus == 0}">可用</c:if><c:if test="${li.partnerStatus == 1}">不可用</c:if></td>
                    <td><fmt:formatDate value="${li.relTime }" pattern="yyy-MM-dd HH:mm:ss"/></td>
                    <td>${li.relUserName}</td>
                    <td><a onclick="moveOut(this)">移出</a>&nbsp;&nbsp;&nbsp;<a onclick="deleteRel(this)">删除</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="pagin">
            <jsp:include
                    page="/webpage/common/pagination_ajax.jsp?callback=toPage"></jsp:include>
        </div>
    </div>
</div>
<script>
    var tmplId = '${tmplId}';
    var currentPage = '${pagelist.currentPage}';
    if(!tmplId) {
        layer.msg('缺少模版id',function () {
            window.location.href = '/pss/serverCharge';
        });
    }
</script>
<script type="text/javascript" src="webpage/pss/serverCharge/relPartner.js?version=${globalVersion}"></script>
