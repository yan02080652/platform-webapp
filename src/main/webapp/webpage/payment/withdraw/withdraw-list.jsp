<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<table class="table">
	<thead>
	<tr>
		<th>企业</th>
		<th>个人账户</th>
		<th>收款人姓名</th>
		<th>收款通道</th>
		<th>收款账户</th>
		<th>提现金额</th>
		<th>审核状态</th>
		<th>创建时间</th>
		<th>操作</th>
	</tr>
	</thead>
		<c:forEach var="withdraw" items="${pageList.list}">
			<tr>
				<td>${withdraw.partnerName}</td>
				<td>${withdraw.userName}</td>
				<td>${withdraw.fullname}</td>
				<td>${withdraw.channel}</td>
				<td>${withdraw.receivable}</td>
				<td><fmt:formatNumber value="${withdraw.amount * 0.01}" pattern="#,##0.##"/></td>
				<td>
					<c:choose>
						<c:when test="${withdraw.status == 0}">待审核</c:when>
						<c:when test="${withdraw.status == 1}">审核通过</c:when>
						<c:when test="${withdraw.status == 2}">审核拒绝</c:when>
						<c:when test="${withdraw.status == 3}">退款完成</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
				</td>
				<td><fmt:formatDate value="${withdraw.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				<td>
					<a onclick="showDetail('${withdraw.id}','${withdraw.accountId}');" >查看明细</a>
				</td>
			</tr>
		</c:forEach>
	<tbody>

	</tbody>
</table>
<div class="pagin">
	<jsp:include page="../../common/pagination_ajax.jsp?callback=queryPersonAccount"></jsp:include>
</div>