<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="change_account_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
				<h4 class="modal-title">用户提现</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form id="reflectAccountform" action="personAccount/withdraw/sure" class="form-horizontal" role="form">
						<input type="hidden" name="withdrawId" value="${withdrawRecord.id}">
						<input type="hidden" name="accountId" value="${personalAccountDto.id}">
						<input type="hidden" name="userId" value="${personalAccountDto.userId}">
						<input type="hidden" name="orginAmount" value="${personalAccountDto.balance}" >
						<div class="form-group">
							<label class="col-sm-3 control-label">个人账户</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" disabled value="${withdrawRecord.userName}"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">账户余额(元)</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" disabled value="<fmt:formatNumber value="${personalAccountDto.balance * 0.01}" pattern="#,##0.##"/>"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">收款人姓名</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" disabled value="${withdrawRecord.fullname}"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">收款通道</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" disabled value="${withdrawRecord.channel}"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">收款账户</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" disabled value="${withdrawRecord.receivable}"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">提现金额(元):</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" disabled value="<fmt:formatNumber value="${withdrawRecord.amount * 0.01}" pattern="#,##0.##"/>"/>
								<input type="hidden" value="${withdrawRecord.amount}" name="amount"/>
							</div>
						</div>
						<c:if test="${withdrawRecord.status == 1 or withdrawRecord.status == 3}">
							<div class="form-group">
								<label  class="col-sm-3 control-label">交易流水号:</label>
								<div class="col-sm-8">
									<input type="text" class="form-control" name="billId" <c:if test="${withdrawRecord.status == 3}">disabled</c:if> value="${withdrawRecord.tradeId}"/>
								</div>
							</div>
							<div class="form-group">
								<label  class="col-sm-3 control-label">备注:</label>
								<div class="col-sm-8">
									<textarea name="memo" rows="3" class="form-control" <c:if test="${withdrawRecord.status == 3}">disabled</c:if> >${withdrawRecord.memo}</textarea>
								</div>
							</div>
						</c:if>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<c:if test="${withdrawRecord.status == 0}">
					<button type="button" class="btn btn-default" onclick="reviewRejectOrPass('${withdrawRecord.id}','1')">审核通过</button>
					<button type="button" class="btn btn-default" onclick="reviewRejectOrPass('${withdrawRecord.id}','2')">审核拒绝</button>
				</c:if>
				<c:if test="${withdrawRecord.status == 1}">
					<button type="button" class="btn btn-default" onclick="sureWithdraw()">退款确认</button>
				</c:if>
			</div>
		</div>
	</div>
</div>

<script>
$(function () {
    var status = '${withdrawRecord.status}';
    debugger;
    if(status == 1){
		$("#reflectAccountform").validate({})
		$("input[name='billId']").rules('add',{
			required : true
		})
    }
})

function sureWithdraw() {
	var option = {
	    success : function (data) {
			if(data.type){
			    layer.msg(data.txt);
                queryList();
                $("#change_account_model").modal('hide')
			}else{
			    layer.msg(data.txt);
                $("#change_account_model").modal('hide')
			}
        }
	}
	if($("#reflectAccountform").valid()){
        $("#reflectAccountform").ajaxSubmit(option);
	}
}

//审核通过或拒绝
function reviewRejectOrPass(withdrawId,status) {
    $.post('personAccount/withdraw/review',{
        withdrawId : withdrawId,
        status : status
    },function (data) {
        if(data.type == 'success'){
            layer.msg(data.txt);
            queryList();
            $("#change_account_model").modal('hide')
        }else{
            layer.msg(data.txt);
            $("#change_account_model").modal('hide')
        }
    });
}
</script>