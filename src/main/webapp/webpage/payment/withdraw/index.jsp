<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>提现列表
			</div>
			<div class="col-md-9">
				<form class="form-inline" action="/personAccount/withdraw/list" id="queryform">
					<nav class="text-right">
						<div class="selectHeader form-group">
							<jsp:include page="../../common/businessSelecter.jsp"></jsp:include>
						</div>
						<div class=" form-group">
							<input type="text" class="form-control" name="keyword" placeholder="收款人姓名/账号" />
							<select class="form-control" name="status" style="width: 150px">
								<option value="-1">全部</option>
								<option value="0" selected>待审核</option>
								<option value="1">审核通过</option>
								<option value="2">审核拒绝</option>
								<option value="3">退款完成</option>
							</select>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default"
									onclick="queryList()">查询</button>
							<button type="button" class="btn btn-default"
									onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
		<hr style="margin-top:10px;"/>
		<div class="col-md-12" id="withdraw-list" style="margin-top: 20px;">

		</div>
	</div>
</div>

<div id="model1"></div>
<script>
    jQuery.validator.addMethod("checkWithdrawAmount", function(value,element) {
        var originalAmount =parseFloat($(element).attr("data-id"));
        var newAmount = parseFloat(value);
        return this.optional(element) || newAmount <= originalAmount;
    }, $.validator.format("提现金额不大于可提金额"));

	$(function () {
        queryList();
    })

	function queryList() {
		var option = {
		    success : function (data) {
				$('#withdraw-list').html("").html(data);
            }
		}
		$('#queryform').ajaxSubmit(option);
    }

    function resetForm() {
        $("#queryform :input").val("");
        $("select[name='status']").val("-1");
        queryList();
    }

    //显示Model
    function showModal(url, data, callback) {
        $('#model1').load(url, data, function(context, state) {
            if ('success' == state && callback) {
                callback();
            }
        });
    }

    function showDetail(withdrawId, accountId) {
        showModal('personAccount/withdraw/getById',{
            "withdrawId" : withdrawId,
			'accountId' : accountId
        },function () {
            $("#change_account_model").modal();
        });
    }

</script>