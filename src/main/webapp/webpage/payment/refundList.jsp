<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<style>
.drop_div{
	width:100px;
	position: relative;
	margin: auto;
}
.drop_input {
    cursor: pointer;
    padding: 0 0 0 8px;
    border: 1px solid #bbb;
    width: 100px;
    height: 24px;
    font-size: 14px;
    background: url('resource/image/downGray.png') no-repeat 80px 10px;
}

.drop_ul {
    display: none;
    position: absolute;
    z-index: 99;
    background-color: #FFFFFF;
    border: 1px solid #bbbbbb;
    border-top: none;
    list-style : none;
    padding-left: 0px;
    padding-right: 0px;
}

.drop_li {
    display: inline-block;
    padding: 0 0 0 8px;
    width: 100px;
    height: 20px;
    line-height: 20px;
    font-size: 14px;
    margin-bottom: 0px;
    margin-top: 0px;
    padding-left: 0px;
    cursor: pointer;
}
.drop_div:hover .drop_ul {
    display: block;
}
.drop_li:hover {
    background-color: #1176cc;
    border-color: #1176cc;
    color: #FFFFFF;
}
</style>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>退款订单列表&nbsp;&nbsp; 共计<span>${pageList.totalPage }页,${pageList.total }</span>条数据   &nbsp;&nbsp;
			</div>
			<div class="col-md-9">
				<form class="form-inline" action="payment/refund" method="post" id="queryform">
					<nav class="text-right">
						<div class="selectHeader form-group">
						   <jsp:include page="../common/businessSelecter.jsp"></jsp:include>
						</div>
						<div class=" form-group">
							<input name="orderId" type="text" value="${refundTask.orderId }" class="form-control" placeholder="订单编号" />
							<select class="form-control"  id="refundStatus" name="status" style="width: 150px">
								<option value="-1">全部</option>
								<option value="0">未处理</option>
								<option value="1">已处理</option>
								<option value="2">待确认</option>
							</select>
						</div>
						<input type="hidden" name="pageIndex"/>
						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="queryList()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
	
	<hr/>
		<div class="col-sm-12">
			<table id="contentTable"
				class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th class="sort-column">退款编号</th>
						<th class="sort-column" >订单编号</th>
						<th class="sort-column">退款金额</th>
						<th class="sort-column" >退款原因</th>
						<th class="sort-column">申请人</th>
						<th class="sort-column">申请时间</th>
						<th class="sort-column">状态</th>
						<th style="min-width: 100px">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="refund">
						<tr>
							<td>${refund.payRefundId}</td>
							<td>${refund.orderId}</td>
							<td><fmt:formatNumber value="${refund.amount*0.01}" pattern="#,##0.##"/></td>
							<td>${refund.refundReasonName}</td>
							<td>${refund.startUserName}</td>
							<td><fmt:formatDate value="${refund.createDate}" pattern="yyyy-MM-dd HH:mm" /> </td>
							<td>
								${refund.statusName}
							</td>
							<td>
								<c:if test="${empty refund.status or refund.status == 0}">
									<a class="btn btn-default"  role="button" onclick="conformRefund('${refund.id}',1)"> 线下退款确认</a>
									<a class="btn btn-default"  role="button" onclick="conformRefund('${refund.id}',0)"> 发起线上退款</a>
									<%-- <div class="drop_div">
		                                <input type="text" value="完成" class="drop_input" readonly="readonly">
		                                <ul class="drop_ul">
		                                    <li class="drop_li" onclick="conformRefund('${refund.id}',1)">
		                                      	  线下退款确认
		                                    </li>
		                                    <li class="drop_li" onclick="conformRefund('${refund.id}',0)">
		                                                                                                                         发起线上退款
		                                    </li>
		                                </ul>
		                            </div> --%>
								</c:if>
							</td>	
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../common/pagination_ajax.jsp?callback=queryList"></jsp:include>
			</div>
		</div>
	</div>
</div>

<div id="refundInfoDiv">
	<div class="modal fade" id="modelRefundInfo" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	   <div class="modal-dialog">
	      <div class="modal-content">
	         <div class="modal-header">
	            <button type="button" class="close" onclick="closeRefundInfoModal()" aria-hidden="true"> &times;</button>
	            <h4 class="modal-title">退款确认</h4>
	         </div>	         
	         <div class="modal-body">
	         	<div class="row">
		         	<form id="formRedundInfo" class="form-horizontal" role="form">
		         		<input type="hidden" name="id">
		         		<input type="hidden" name="type">
			         	<div class="form-group">
					      <label class="col-sm-3 control-label">退款渠道:</label>
						  <div class="col-sm-8">
						     <label class="radio-inline"><input  type="radio"  value="ALIPAY" checked="checked" name="paymentChannel" class="ignore"/>支付宝</label>
						     <lable class="radio-inline"><input  type="radio"  value="YEEPAY" name="paymentChannel" class="ignore"/>易宝</lable>
						     <lable class="radio-inline"><input  type="radio"  value="BANK_TRANSFER" name="paymentChannel" class="ignore"/>银行卡</lable>
						  </div>
					   </div>
					   <div class="form-group">
					      <label for="paymentAccount" class="col-sm-3 control-label">支付账号:</label>
						  <div class="col-sm-8">
						  	<input type="text" class="form-control" name="paymentAccount"/>
						  </div>
					   </div>
					   <div class="form-group">
					      <label for="paymentTradeId" class="col-sm-3 control-label">支付流水号:</label>
						  <div class="col-sm-8">
						  	<input type="text" class="form-control" name="paymentTradeId"/>
						  </div>
					   </div>
					   <div class="form-group">
					      <label for="paymentDestName" class="col-sm-3 control-label">收款方名称:</label>
						  <div class="col-sm-8">
						  	<input type="text" class="form-control" name="paymentDestName"/>
						  </div>
					   </div>
					   <div class="form-group">
					      <label for="paymentDestAccount" class="col-sm-3 control-label">收款方账号:</label>
						  <div class="col-sm-8">
						  	<input type="text" class="form-control" name="paymentDestAccount"/>
						  </div>
					   </div>
				   </form>
			   </div>
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn btn-default" onclick="closeRefundInfoModal()">关闭
	            </button>
	            <button type="button" class="btn btn-primary" onclick="refundOffline()">确认
	            </button>
	         </div>
	      </div>
		</div>
	</div>
</div>
<script type="text/javascript" >

$(document).ready(function(){
	$("#refundStatus").val("${refundTask.status}");
	$("#queryBpTmcId").val('${refundTask.tmcId}');
	$("#queryBpTmcName").val('${bpTmcName}');
	$("#queryBpId").val('${refundTask.partnerId}');
	$("#queryBpName").val('${bpName}');
});

function conformRefund(id,type) {
	//type 1是线下退款 0是线上退款
	
	if(type == 1){
		initFormField();
		$("input[name='id']").val(id);
		$("input[name='type']").val(type);
		$('#modelRefundInfo').modal();
	}else{
		$.confirm({
			title : '提示',
			confirmButton:'确认',
			cancelButton:'取消',
			content : '确认此笔退款?',
			confirm : function() {
				$.ajax({
					type:'GET',
					url:'payment/refund/conformRefund?id='+id+"&type="+type,
					success:function(msg){
						if(msg.type == 'error'){
							showErrorMsg(msg.txt);
						}else{
							showSuccessMsg(msg.txt);
							
							window.location.href="/payment/refund";
						}
					}
				});
			},
			cancel : function() {
			}
		});
	}
}

function closeRefundInfoModal(){
	$('#modelRefundInfo').modal('hide');
}

function refundOffline(){
	$("#formRedundInfo").submit();
}

function initFormField() {
	$("#formRedundInfo").validate({
		rules:{
			paymentAccount : {
				required : true
			},
			paymentTradeId : {
				required : true
			},
			paymentDestName : {
				required : true
			},
			paymentDestAccount : {
				required : true
			}
		},
		submitHandler : function(form) {
			var dataParam = $('#formRedundInfo').serializeArray();
			var dataInfo = {};
			$.each(dataParam,function(i,d){
				dataInfo[d.name] = d.value;
			});
			
			$.post("payment/refund/conformRefund",dataInfo,
				function(data){
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						window.location.href="/payment/refund";
					}else{
						showErrorMsg(data.txt);
					}
				}
			);
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

function queryList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;

	$("input[name='pageIndex']").val(pageIndex);

    $("#queryform").submit();
}
function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}


</script>