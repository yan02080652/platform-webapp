<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>


<style>
	.selectHeader label{
		display: none !important;
	}

</style>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业来款记录
			</div>
			<div class="col-md-12" style="padding-top: 10px;">
				<form class="form-inline" method="post" id="queryform" action="payment/entRecord">
					<nav class="text-right">
						<input type="hidden" name="pageIndex" id = "pageIndex"/>

						<div class=" form-group selectHeader">
							<jsp:include page="../../common/businessSelecter.jsp"></jsp:include>
						</div>

						<div class="input-group col-md-2">
							<input type="text" id="paymentName" name="paymentName" value="${recordForm.paymentName }" placeholder="付款单位"	class="form-control dropdown-common"> 
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"
									onclick="selectMyDropDown(this)">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
							</span>
						</div>		    
						<div class="form-group">
						      <input type="text" name="formDate" readonly="readonly" style="width: 110px;" onClick="WdatePicker()" class="form-control" value="<fmt:formatDate value="${recordForm.formDate }" pattern="yyyy-MM-dd"/>" placeholder="收款开始时间">
								--
						      <input type="text" name="toDate" readonly="readonly" style="width: 110px;" onClick="WdatePicker()" class="form-control" value="<fmt:formatDate value="${recordForm.toDate }" pattern="yyyy-MM-dd"/>" placeholder="收款结束时间">
					    </div>	
					            确认状态
 					    <div class="form-group">
						     <select class="form-control" name="status">
						     	  <option value="-99">全部</option>
						          <option value="0" ${recordForm.status == 0 ? 'selected' : ''}>未确认</option>
						          <option value="1" ${recordForm.status == 1 ? 'selected' : ''}>已确认</option>
						     </select>
						</div>
						可用金额
 					    <div class="form-group">
						     <select class="form-control" name="liquidationStatus">
						     	  <option value="ALL">全部</option>
						          <option value="OPEN" ${recordForm.liquidationStatus eq 'OPEN' ? 'selected' : ''}>有可用金额</option>
						          <option value="CLOSE" ${recordForm.liquidationStatus eq 'CLOSE' ? 'selected' : ''}>无可用金额</option>
						     </select>
						</div>						    				    
						<div class="form-group">
							<button type="button" class="btn btn-default" onclick="queryList()">查询</button>
							<button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
		</br>
		<hr style="margin-top:10px;"/>
		<div class="col-sm-12">
			<a data-pjax  style="float: left ;margin-bottom: 10px;" class="btn btn-default" href="payment/entRecord/detail" >记录新的来款</a>
			<table id="contentTable"
				class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th class="sort-column" style="text-align: center;">付款企业</th>
						<th class="sort-column">付款单位(法人)</th>
						<th class="sort-column" width="100px;">付款日期</th>
						<th class="sort-column" width="130px;">参考凭证号</th>
						<th class="sort-column" width="100px;">付款金额</th>
						<th class="sort-column" width="110px;">剩余可用金额</th>
						<th class="sort-column" width="120px;">确认状态</th>
						<th class="sort-column" >备注</th>
						<!-- <th class="sort-column" width="90px;">清账状态</th> -->
						<!-- <th class="sort-column" width="90px;">记录人</th>
						<th class="sort-column" width="100px;">记录时间</th> -->
						<th width="100px;">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="enPay">
						<tr>
							<td style="text-align: center;">${enPay.bpName}</td>
							<td>${enPay.paymentName}</td>
							<td><fmt:formatDate value="${enPay.paymentDate}"   pattern="yyyy-MM-dd"/></td>
							<td>${enPay.refRecord}</td>
							<td><fmt:formatNumber value="${enPay.amount*0.01}" pattern="#,##0.##"/></td>
							<td><fmt:formatNumber value="${enPay.remainAmount*0.01}" pattern="#,##0.##"/></td>
							<td>
								<c:if test="${enPay.status == 0}"><font color="#ba2626">未确认</font></c:if>
								<c:if test="${enPay.status == 1}">已确认</c:if>
							</td>
							<td>${enPay.remark}</td>
							<!-- <td>
								<c:if test="${enPay.status == 1}">
									<c:if test="${enPay.liquidationStatus eq 'OPEN'}"><font color="#ba2626">未清账</font></c:if>
									<c:if test="${enPay.liquidationStatus eq 'CLOSE'}">已清账</c:if>
								</c:if>
							</td>
							 -->
							<td>
								<a href="javascript:;" onclick="liquidationEnt('${enPay.id}','${enPay.status }')">
									<%-- <c:if test="${enPay.liquidationStatus eq 'OPEN'}">清账</c:if>
									<c:if test="${enPay.liquidationStatus eq 'CLOSE'}">清账记录</c:if> --%>
									明细及清账
								</a>
								<%-- <c:if test="${enPay.status == 0}"><a href="javascript:;" onclick="confirmEntRecord('${enPay.id }')">确认来款</a></c:if> --%>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryList"></jsp:include>
			</div>
		</div>
	</div>
</div>

<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script type="text/javascript" >

    $(function () {
        var bpId = "${recordForm.bpId }";
        var bpName = "${recordForm.bpName }";

        var tmcId = "${recordForm.tmcId }";
        var bpTmcName = "${recordForm.bpTmcName }";

        $("#queryBpId").val(bpId);
        $("#queryBpName").val(bpName);

        $("#queryBpTmcId").val(tmcId);
        $("#queryBpTmcName").val(bpTmcName);

    });



function queryList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$('#pageIndex').val(pageIndex);
	$("#queryform").submit();
}
function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}

function liquidationEnt(id,status){
	/* if(status == 0){
		$.alert("此笔付款还未确认！","提示");
		return;
	} */
	window.location.href = "/payment/entRecord/detail?recordId="+id;
}


function selectMyDropDown(dom){
	if(!$("#queryBpId").val()){
		$.alert('请先选择企业','提示');
		return;
	}
	
	$('.dropdown-common-list').remove();
	$.get('widget/corp/getCorpList',{
		partnerId : $("#queryBpId").val()
	},function(data){
		TR.select(data,{
			key:'custom1',//多个自定义下拉选择时key值不能重复
			dom:dom,
			name:'invoiceTitle',//default:name
			allowBlank:false//是否允许空白选项
		},function(result){
			$("#paymentName").val(result ? result.invoiceTitle : null);
		});
		
	});
}

/* function confirmEntRecord(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认此笔来款?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'payment/entRecord/confirmEntRecord?id='+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						queryList();
						showSuccessMsg(msg.txt);
					}
				}
			});
		}
	});
} */
</script>