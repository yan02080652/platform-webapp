<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<thead>
	<td style="text-align: center;">结算期间</td>
	<td style="text-align: center;">账户名称</td>
	<td style="text-align: center;">账单金额(元)</td>
	<td style="text-align: center;">未清账金额(元)</td>
	<td style="text-align: center;">创建人</td>
	<td style="text-align: center;">创建时间</td>
	<td style="text-align: center;">本次清账金额(元)</td>
</thead>
<c:forEach items="${bills}" var="bill"  varStatus="status" >
	<tr>
		<td><fmt:formatDate value="${bill.liqDateBegin}" pattern="yyyy/MM/dd"/> - <fmt:formatDate value="${bill.liqDateEnd}" pattern="yyyy/MM/dd"/></td>
		<td>${bill.accountName}</td>
		<td><fmt:formatNumber value="${bill.billAmount*0.01}" pattern="#,##0.##"/></td>
		<td><fmt:formatNumber value="${bill.remainAmount*0.01}" pattern="#,##0.##"/><input type="hidden" id="billRemain-${bill.id}" value="${bill.remainAmount}"/></td>
		<td>${bill.createUserName}</td>
		<td><fmt:formatDate value="${bill.createDate}" pattern="yyyy-MM-dd"/></td>
		<td>
			<input type="hidden" name="billFrom[${status.index}].id" value="${bill.id }"/>
			<input style="width:80px;" billid="${bill.id}" inputflag="liqAmount" type="number" data-id="${bill.remainAmount*0.01}" name="billFrom[${status.index}].remainAmountInput" value="${bill.remainAmountInput}" />
		</td>
	</tr>
</c:forEach>

<script type="text/javascript">
$(document).ready(function() {
    $("#billList-model").validate({
        submitHandler : function(form) {
			var billTotal = parseFloat(0);
			var cflag = true;
            $('#billList-model').find("input[inputflag=liqAmount]").each(function(){
            	var val = parseFloat($(this).val());
				billTotal = billTotal + val;
				var billId = $(this).attr('billid');
				var billRemain = parseFloat($('#billRemain-' + billId).val());
				if(val * 100 > billRemain) {
					$(this).focus();
					$.alert("账单清帐金额不能大于账单未清账金额");
					cflag = false;
				}
			});
            
            if(!cflag) {
            	return;
            }
            
			var remainAmount = '${remainAmount}';
        	if(billTotal > parseFloat(remainAmount)){
        		$.alert("账单清帐总金额不能大于企业来款未清账金额");
        		return;
        	}else{
        		form.submit();
        	}
        },
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight
    });
    
    $('#billList-model').find("input[type=text]").each(function(){
  	   	 $(this).rules('add', {
  		   	 checkTenTwoNumber :true,
  		  	 checkAmount : true
  	     });
    });
});
</script>