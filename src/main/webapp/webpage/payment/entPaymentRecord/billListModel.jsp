<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>


<div class="modal fade" id="modelBpAccountCorp" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
   <div class="modal-dialog" style="width: 1000px;">
      <div class="modal-content" style="width: 1000px;">
         <div class="modal-header">
            <button type="button" class="close" onclick="closeCorpModal()" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">选择账单</h4>
         </div>
         <input type="hidden" name="accountId" id ="billListModelAccountId" value="${account.id }"/>
         <%-- <div style="margin-top: 15px">
			<label class="col-sm-2 control-label" style="margin-top: 7px;">选择账户:</label>
			<div class="input-group col-md-5">
				<input type="text" id="billListModelAccountName" name="accountName" readonly="readonly" value="${account.accountName }" class="form-control dropdown-common"> <span class="input-group-btn">
					<button class="btn btn-default" type="button"
						onclick="selectMyDropDownForBill(this)">
						<i class="fa fa-caret-down" aria-hidden="true"></i>
					</button>
				</span>
			</div>
		 </div>	 --%>
         <div class="modal-body">
         	<form id="billList-model" action="payment/entRecord/saveLiquidActionRecord" method="post">
         		<input type="hidden" name="paymentId" id="paymentId" value="${paymentId }"/>
	         	<table style="margin-bottom : 0px" id="selectCorpListTable" class="table table-bordered table-hover">
	         		
				</table>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="closeCorpModal()">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="submitBill()">确认
            </button>
         </div>
      </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	findBillList($('#billListModelAccountId').val());

});
 
 
function closeCorpModal(){
	$('#modelBpAccountCorp').modal('hide');
}
 
 function selectMyDropDownForBill(dom){
    $('.dropdown-common-list').remove();
 	
	$.get('widget/account/getAccountList',{
		partnerId : $("#bpId").val()
	},function(data){
		TR.select(data,{
			key:'custom2',//多个自定义下拉选择时key值不能重复
			dom:dom,
			name:'accountName',//default:name
			allowBlank:true//是否允许空白选项
		},function(result){
			$("#billListModelAccountId").val(result ? result.id : null);
			$("#billListModelAccountName").val(result ? result.accountName : null);
			
			findBillList(result.id);
			
		});
	});
}
 function findBillList(accountId){
	 $.post('payment/entRecord/findBillList',{
		 bpId : $("#bpId").val(),
		 accountId : accountId,
		 paymentId :$("#paymentId").val()
	 },function(data){
		 $('#selectCorpListTable').html("");
		 $('#selectCorpListTable').html(data);
	 });
 }
 
//选择账单提交
 function submitBill() {
 	$('#billList-model').submit();
 }
 </script>