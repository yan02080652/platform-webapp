<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业付款信息 - 
				<c:choose>
					<c:when test="${record.id != '' && record.id != null}">清账</c:when>
					<c:otherwise>添加</c:otherwise>
				</c:choose>
				&nbsp;&nbsp;<a data-pjax href="payment/entRecord">返回</a>
			</div>
			<input type="hidden" name="paymentId" id="paymentId" value="${record.id }" />
			<div class="panel-body">
				<form id="record_form" class="form-horizontal bv-form">
					<input name="id" id="recordId" type="hidden" value="${record.id}" />
					<div class="form-group">
						<label class="col-sm-2 control-label">企业名称:</label>
						<div class="col-sm-3" style="float: left;">
							<input type="hidden" name="bpId" id="bpId" value="${record.bpId }" /> 
							<input type="text" name="bpName" id="bpName" onclick="chooseDep()" class="form-control" value="${record.bpName }">
						</div>
						<label class="col-sm-2 control-label">付款单位:</label>
						<div class="col-md-3" style="display: table;">
							<input type="hidden" name="corpId" id="corpId" value="${record.corpId }" /> 
							<input type="text" name="paymentName" readonly="readonly" value="${record.paymentName }" class="form-control dropdown-common paymentName"> 
							<span class="input-group-btn">
								<button class="btn btn-default" id="selectPaymentName" type="button" onclick="selectMyDropDown(this)">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
							</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">收款金额(元):</label>
						<div class="col-sm-3">
							<input type="number" name="amount" class="form-control" value="${record.amount/100 }">
						</div>
						<label class="col-sm-2 control-label">收款日期:</label>
						<div class="col-sm-3">
							<input type="text" name="paymentDate" class="form-control" onClick="WdatePicker()" readonly="readonly" value="<fmt:formatDate value="${record.paymentDate }" pattern="yyyy-MM-dd"/>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">收款方式:</label>
						<div class="col-sm-3">
							<select class="form-control" name="paymentChannel" value="${record.paymentChannel }">
								<option value="BANK_ONLINE">网银支付</option>
								<option value="OTHER">其他</option>
							</select>
						</div>
						<label class="col-sm-2 control-label">参考凭证号:</label>
						<div class="col-sm-3">
							<input type="text" name="refRecord" class="form-control" value="${record.refRecord }">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">企业流水摘要:</label>
						<div class="col-sm-3" >
							<textarea rows="2" name="remark1" class="form-control" value="${fn:split(record.remark, "$$$")[0]}">${fn:split(record.remark, "$$$")[0]}</textarea>
						</div>
						<label class="col-sm-2 control-label">内部备注:</label>
						<div class="col-sm-3" >
							<textarea rows="2" name="remark2" class="form-control" value="${fn:split(record.remark, "$$$")[1]}">${fn:split(record.remark, "$$$")[1]}</textarea>
						</div>
					</div>
				</form>
				<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-3" style="padding-left: 0px;">
							<c:if test="${empty record}"><button class="btn btn-default" id="submit_record" onclick="submitrecord()">保存基本信息</button></c:if>
						</div>
						<div class="col-sm-3" style="padding-left: 0px;">
							<c:if test="${not empty record and record.status == 0}"><button class="btn btn-default" onclick="confirmEntRecord('${record.id }')">确认收款</button></c:if>
						</div>
					</div>
			</div>
		</div>
	</div>
	<c:if test="${not empty record and record.status == 1}">
		<div id="bill-model" class="col-lg-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="glyphicon glyphicon-home" aria-hidden="true"></span>清账记录&nbsp;&nbsp;    ${liquidAmountTxt } &nbsp;&nbsp;&nbsp;&nbsp;
					<c:if test="${record.liquidationStatus eq 'OPEN' }">
						<a data-pjax onclick="selectBill();">清账</a>
					</c:if>
				</div>
				<div class="panel-body">
					<div class="form-group">
					 	<div class="col-sm-12">
					 		<table id="treeTablePartnerTmc" class="table table-bordered">
								<thead>
									<tr>
										<th>清账金额(元)</th>
										<th style="text-align: center;">账户名称</th>
										<th style="text-align: center;">结算期间</th>
										<th style="text-align: center;">操作人</th>
										<th style="text-align: center;">清账日期</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${recordList1 }" var="record">
										<tr>
											<td><fmt:formatNumber value="${record.liquidationAmount*0.01}" pattern="#,##0.##" /></td>
											<td>${record.accountName }</td>
											<td><fmt:formatDate value="${record.liqDateBegin}" pattern="yyyy/MM/dd"/> - <fmt:formatDate value="${record.liqDateEnd}" pattern="yyyy/MM/dd"/></td>
											<td>${record.createUserName }</td>
											<td><fmt:formatDate value="${record.createDate }"
													pattern="yyyy-MM-dd" /></td>
											<td><a onclick="billDetail('${record.billId }')">查看账单</a>&nbsp;&nbsp;
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
					 	</div>
					</div>
				</div>
			</div>
		</div>
	</c:if>
</div>
<div id="bpAccoountCorpDiv"></div>
<form action="bpAccount/settlement/billDetail" id="billDetail_form" target="_blank">
	<input type="hidden" name="id" id="billDetailId">
</form>

<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script type="text/javascript">
//选择企业
function chooseDep() {
	TR.select('partner', {
		type : 0
	}, function(data) {
		$("#bpId").val(data.id);
		$("#bpName").val(data.name);
	});
}
function selectMyDropDown(dom) {
	$('.dropdown-common-list').remove();

	$.get('widget/corp/getCorpList', {
		partnerId : $("#bpId").val()
	}, function(data) {
		TR.select(data, {
			key : 'custom1',//多个自定义下拉选择时key值不能重复
			dom : dom,
			name : 'invoiceTitle',//default:name
			allowBlank : false
		//是否允许空白选项
		}, function(result) {
			$("#corpId").val(result ? result.id : null);
			$(".paymentName").val(result ? result.invoiceTitle : null);
		});
	});
}

function submitrecord() {
	$('#record_form').submit();
}

function confirmEntRecord(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认此笔收款?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'payment/entRecord/confirmEntRecord?id='+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						window.location.href="/payment/entRecord/detail?recordId="+id;
					}
				}
			});
		}
	});
}

$(document).ready(function() {
	var record = '${record}';
	if (record) {
		$('#record_form').find('input,textarea,select').attr('disabled', 'true');
		$('#selectPaymentName').attr('disabled', 'true');
		//$('#summitRecordDiv').hide();
	} else {
		$('#bill-model').hide();
	}
	
	$("#record_form").validate({
		rules : {
			bpName : {
				required : true,
				minlength : 1
			},
			paymentName : {
				required : true,
				minlength : 1
			},
			paymentDate : {
				required : true
			},
			amount : {
				required : true,
				minlength : 1
			},
			paymentChannel : {
				required : true
			},
			refRecord : {
				required : true
			}
		},
		submitHandler : function(form) {
			var dataParam = $('#record_form').serializeArray();
			var dataInfo = {};
			$.each(dataParam,function(i,d){
				dataInfo[d.name] = d.value;
			});
			$.post("payment/entRecord/save",dataInfo,
				function(data){
					if (data.type == 'success') {
						window.location.href="/payment/entRecord/detail?recordId="+data.txt;
					} else {
						showErrorMsg(data.txt);
					}
			});
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
});
function selectBill() {
	showCorpModal("payment/entRecord/getBillList?bpId="+ $('#bpId').val() + "&paymentId=" + $('#paymentId').val(),function(a) {
		$('#modelBpAccountCorp').modal();
	});
}

function showCorpModal(url, callback) {
	$('#bpAccoountCorpDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

//账单详情
function billDetail(id) {
	$('#billDetailId').val(id);
	$('#billDetail_form').submit();
}
</script>
