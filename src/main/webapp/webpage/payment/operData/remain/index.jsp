<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业应收账款查询
			</div>
			<div class="col-md-10">
				<form class="form-inline" id="queryform">
					<nav class="text-right">
						<div class=" form-group selectHeader">
							<jsp:include page="../../../common/businessSelecter.jsp"></jsp:include>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="queryBillList()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
		<hr/>
		
		<div class="col-sm-12" id="billListDiv">
		</div>
	</div>
</div>

<div id="bpAccountFieldDiv"></div>
<script type="text/javascript" >
$(document).ready(function(){
	queryBillList();
});


function resetForm(){
	$("#queryform :input").val("");
    queryBillList(1);
}

function queryBillList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("input[name='pageIndex']").val(pageIndex);
	
	var dataParam = $('#queryform').serializeArray();
	var dataInfo = {};
	$.each(dataParam,function(i,d){
		dataInfo[d.name] = d.value;
	});

	$('#billListDiv').load("payment/operData/remainList",dataInfo, function(context, state) {
		if ('success' == state) {
		}
	});
}

</script>