<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<table style="margin-bottom : 0px" class="table table-bordered">
	<thead>
		<tr>
			<th class="sort-column" style="text-align: center;" >企业名称</th>
			<th class="sort-column" >账户名称</th>
			<th class="sort-column" >应收账款</th>
			<th style="min-width: 70px">操作</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pageList.list}" var="bill" varStatus="status">
			<tr>
				<td style="text-align:center;">${bill.bpName}</td>
				<td>${bill.accountName}</td>
				<td><fmt:formatNumber value="${bill.remainAmount*0.01}" pattern="#,##0.##"/></td>
				<td>
					<a class="btn btn-default" href="javascript:;" onclick="goToBillList('${bill.accountId}')" role="button">查看账单列表</a>
					<a class="btn btn-default" href="javascript:;" onclick="goToPayRecordList('${bill.bpId}')" role="button">查看付款列表</a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<div class="pagin">
	<jsp:include page="../../../common/pagination_ajax.jsp?callback=queryBillList"></jsp:include>
</div>
<script type="text/javascript" >
    function goToBillList(accountId){
        var begin = '1970-01-01';
        var end = formatDate();
        window.open("/bill/manage?confirmStatus=CONFIRM&beginDate="+begin+"&endDate="+end+"&accountId="+accountId);
    }

    function goToPayRecordList(bpId) {
        var begin = '1970-01-01';
        var end = formatDate();
        window.open("/payment/entRecord?status=1&liquidationStatus=OPEN&formDate="+begin+"&toDate="+end+"&bpId="+bpId);
    }
    
    function formatDate() {
        var d = new Date(),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
</script>