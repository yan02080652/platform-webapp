<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<style>
	.selectHeader label{
		display: none !important;
	}

</style>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业结算信息查询

			</div>
			<div class="col-md-12" style="padding-top: 10px;">
				<form class="form-inline" id="queryform">
					<nav class="text-right">
						<div class=" form-group selectHeader">
							<input type="hidden" id="pageIndex" name="pageIndex" />
							<jsp:include page="../../../common/businessSelecter.jsp"></jsp:include>
						 </div>
						结算区间
						<div class="form-group">
							<input type="text" placeholder="结算区间" onclick="WdatePicker({skin:'whyGreen',dateFmt:'yyyy年MM月',vel:'d244_2'})" class="form-control"/>
							<input type="hidden" name="billGap" id="d244_2">
						</div>
						创建时间
						<div class=" form-group input-group">
							<input name="beginDate" type="text" onclick="WdatePicker()" class="form-control" style="width: 105px" placeholder="开始时间" /><span class="input-group-btn"/>
							<input name="endDate" type="text" onclick="WdatePicker()" class="form-control" style="width: 105px" placeholder="结束时间" /><span class="input-group-btn"/>
						</div>
						<div class=" form-group">
							结算状态
							<select class="form-control"  id="billStatus" name="billStatus" style="width: 110px" placeholder="清账状态">
								<option value="" selected="selected">全部</option>
								<option value="0">无账单</option>
								<option value="1">账单未确认</option>
								<option value="2">账单已确认</option>
								<option value="9">已清账</option>
							</select>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="queryBillList()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
		<hr/>
		
		<div class="col-sm-12" id="billListDiv">
		</div>
	</div>
</div>

<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script type="text/javascript" >
$(document).ready(function(){
    initDate()
	queryBillList();
});

function initDate() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    if(month == 1){
        year = year -1;
        month = "12";
	}else{
        month = month - 1;
	}
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    var gapDate = year+"-"+month ;
    var grapDateCh = year + "年" + month + "月";
    $('input[name="billGap"]').val(gapDate);
    $('input[name="billGap"]').prev().val(grapDateCh);
}

function resetForm(){
	$("#queryform :input").val("");
    queryBillList(1);
}

function queryBillList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("input[name='pageIndex']").val(pageIndex);
	
	var dataParam = $('#queryform').serializeArray();
	var dataInfo = {};
	$.each(dataParam,function(i,d){
		dataInfo[d.name] = d.value;
	});

	$('#billListDiv').load("payment/operData/billList",dataInfo, function(context, state) {
		if ('success' == state) {
		}
	});
}

</script>