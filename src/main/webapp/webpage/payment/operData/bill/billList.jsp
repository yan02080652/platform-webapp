<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<table style="margin-bottom : 0px" class="table table-bordered">
	<thead>
		<tr>
			<th class="sort-column" style="text-align: center;" >企业名称</th>
			<th class="sort-column" >账户名称</th>
			<th class="sort-column" >创建时间</th>
			<th class="sort-column" >结算期间</th>
			<th class="sort-column">备注</th>
			<th class="sort-column" >账单金额</th>
			<th class="sort-column" >审核状态</th>
			<th class="sort-column" >开票状态</th>
			<th class="sort-column" >清账状态</th>
			<th style="min-width: 70px">操作</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pageList.list}" var="bill" varStatus="status">
			<tr>
				<td style="text-align:center;">${bill.bpName}</td>
				<td>${bill.accountName}</td>
				<td><fmt:formatDate value="${bill.createDate }" pattern="yyyy-MM-dd" /></td>
				<td><fmt:formatDate value="${bill.liqDateBegin}" pattern="yyyy-MM-dd"/> - <fmt:formatDate value="${bill.liqDateEnd}" pattern="yyyy-MM-dd"/></td>
				<td>${bill.remark}</td>
				<td><fmt:formatNumber value="${bill.billAmount*0.01}" pattern="#,##0.##"/></td>
				<td>
					<c:if test="${bill.status eq 'NOT_CONFIRM'}"><font color="#ba2626">待确认</font></c:if>
					<c:if test="${bill.status eq 'CONFIRM'}">已确认</c:if>
				</td>
				<td>
					<c:if test="${bill.status eq 'CONFIRM'}">
						<c:if test="${bill.invoiceAmount >= bill.billAmount and bill.billAmount>0}">已开票</c:if>
						<c:if test="${bill.invoiceAmount>0 and bill.invoiceAmount<bill.billAmount}">
							<font color="#ba2626">部分开票</font>
						</c:if>
						<c:if test="${empty bill.invoiceAmount or bill.invoiceAmount==0 }">
							<font color="#ba2626">未开票</font>
						</c:if>
					</c:if>
				</td>
				<td>
					<c:if test="${bill.status eq 'CONFIRM'}">
						<c:choose>
							<c:when test="${bill.liquidationStatus eq 'OPEN' && bill.remainAmount > 0 && bill.remainAmount < bill.billAmount}">
								<font color="#ba2626">部分清账</font>
							</c:when>
							<c:when test="${bill.liquidationStatus eq 'CLOSE'}">
								已清账
							</c:when>
							<c:otherwise><font color="#ba2626">未清账</font></c:otherwise>
						</c:choose>
					</c:if>
				</td>
				<td>
					<c:choose>
						<c:when test="${empty bill.createDate}">
							<a class="btn btn-default" href="javascript:;" onclick="goToAccountDetail('${bill.accountId}')" role="button">账户结算明细</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-default" style="display: inline;" href="javascript:;" onclick="queryBillDetail('${bill.id}')" role="button">账单明细</a>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<div class="pagin">
	<jsp:include page="../../../common/pagination_ajax.jsp?callback=queryBillList"></jsp:include>
</div>
<script type="text/javascript" >

function queryBillDetail(id){
    window.open("/bpAccount/settlement/billDetail?id="+id+"&linkFrom=billList");
}
function goToAccountDetail(id){
    window.open("/bpAccount/settlement/accountDetail?id="+id);
}

</script>