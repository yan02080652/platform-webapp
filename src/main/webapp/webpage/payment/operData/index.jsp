<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业运营数据总台
			</div>
		</div>	
		<hr style="margin-top:10px;"/>
		<div class="row">
			<div class="col-md-2">
            	【业务统计】
            </div>
            </br>
			<div class="col-md-7" style="margin-top:3px;">
					<nav class="text-left form-inline">
					    <form action="payment/operData/findOrderLiqAmount" id = "liqAmount_form">
					    	订单统计日期
							<div class="form-group">
							      <input type="text" name="beginDate" readonly="readonly" style="width: 110px;" onClick="WdatePicker()" class="form-control" placeholder="开始时间">
									--
							      <input type="text" name="endDate" readonly="readonly" style="width: 110px;" onClick="WdatePicker()" class="form-control"  placeholder="结束时间">
						    </div>	
							<div class="form-group">
								<button type="button" class="btn btn-default" onclick="queryLiqAmount()">查询</button>
							</div>
						</form>	
					</nav>
			</div>
			</br>
			<div class="col-md-7 form-inline" style="margin-top:5px;">
			 	<nav class="text-left" id="orderLiqAmount_div" ></nav>
			</div>						
		</div>
		<hr style="margin-top:10px;"/>
		<div class="row">
			<div class="col-md-2">
            	【账单查询】
            </div>
            </br>	
			<div class="col-md-7" style="margin-top:3px;">
					<nav class="text-left form-inline">
			            <form action="payment/operData/findConfirmAmount" id = "confirmAmount_form">
			             	账单创建日期
							<div class="form-group">
							      <input type="text" name="beginDate" readonly="readonly" style="width: 110px;" onClick="WdatePicker()" class="form-control" placeholder="开始时间">
									--
							      <input type="text" name="endDate" readonly="readonly" style="width: 110px;" onClick="WdatePicker()" class="form-control" placeholder="结束时间">
						    </div>	
							<div class="form-group">
								<button type="button" class="btn btn-default" onclick="queryConfirmAmount()">查询</button>
							</div>
						</form>	
					</nav>		
			</div>
			</br>
			<div class="col-md-7" style="margin-top:5px;">
			   <nav class="text-left" id ="confirmAmount_div"></nav>
			</div>
		</div>
		<hr style="margin-top:10px;"/>
		<div class="row">
			<div class="col-md-2">
            	【应收账款查询】
            </div>
            </br>		
			<div class="col-md-7">
					<nav class="text-left form-inline">

						<div class="form-group">
							截止当前时间的 应收账款：${billAmount }元 <a onclick="goToBillList()">查看</a>
						</div>
					</nav>		
			</div>
			<div class="col-md-5">
			</div>
		</div>			
	</div>
</div>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script>
function queryLiqAmount(){
	var liqAmount = {
		success:function(data){
			if(!data.total){
				data.total = 0;
			}
			if(!data.close){
				data.close = 0;
			}
			if(!data.open){
				data.open = 0;
			}
			var orderLiqAmount = "<p>订单总额： "+data.total+"元&nbsp;&nbsp;&nbsp;&nbsp;已结算订单："+data.close+"元&nbsp;&nbsp;&nbsp;&nbsp; 未结算订单："+data.open+"元</p>";
			$("#orderLiqAmount_div").html(orderLiqAmount);
		}
	}
	$("#liqAmount_form").ajaxSubmit(liqAmount);
}

function queryConfirmAmount(){
	var confirmAmount = {
		success:function(data){
			console.log(data);
			if(!data.CONFIRM){
				data.CONFIRM = 0;
			}
			if(!data.NOT_CONFIRM){
				data.NOT_CONFIRM = 0;
			}
			var orderLiqAmount = "<p>已确认："+data.CONFIRM+"元 &nbsp;&nbsp;&nbsp;&nbsp;未确认："+data.NOT_CONFIRM+"元</p>";
			$("#confirmAmount_div").html(orderLiqAmount);
		}
	}
	$("#confirmAmount_form").ajaxSubmit(confirmAmount);
}

function goToBillList(){
	window.open("/bill/manage?confirmStatus=CONFIRM");
}
</script>