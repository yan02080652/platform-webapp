<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<style type="text/css">
	.col-md-10 {
		width: 100%;
	}
</style>
<hr style="margin-top:10px;"/>
<div class="col-md-10" style="margin-top: 20px;">

	<table class="table">
		<tr>
			<td style="border-top:0px;font-weight:bold;">总计</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.totalAmount == null ? 0 : partnerOrderAmount.totalAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
			    <fmt:formatNumber value="${partnerOrderAmount.refundAmount == null ? 0 : partnerOrderAmount.refundAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.flightAmount == null ? 0 : partnerOrderAmount.flightAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.refundFlightAmount == null ? 0 : partnerOrderAmount.refundFlightAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.internationalFlightAmout == null ? 0 : partnerOrderAmount.internationalFlightAmout}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.refundInternationalFlightAmout == null ? 0 : partnerOrderAmount.refundInternationalFlightAmout}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.hotelAmount == null ? 0 : partnerOrderAmount.hotelAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.refundHotelAmount == null ? 0 : partnerOrderAmount.refundHotelAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.trainAmount == null ? 0 : partnerOrderAmount.trainAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.refundTrainAmount == null ? 0 : partnerOrderAmount.refundTrainAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.insuranceAmount == null ? 0 : partnerOrderAmount.insuranceAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.refundInsuranceAmount == null ? 0 : partnerOrderAmount.refundInsuranceAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.carAmount == null ? 0 : partnerOrderAmount.carAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.generalAmount == null ? 0 : partnerOrderAmount.generalAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px">
				<fmt:formatNumber value="${partnerOrderAmount.refundGeneralAmount == null ? 0 : partnerOrderAmount.refundGeneralAmount}" pattern="#,##0.##"/>
			</td>
			<td style="border-top:0px"></td>
		</tr>
		<tr>
			<th>企业</th>
			<th style="text-align: center;">订单总额</th>
			<th style="text-align: center;">退单总额</th>
			<th style="text-align: center;">国内机票</th>
			<th style="text-align: center;">国内机票退单</th>
			<th style="text-align: center;">国际机票</th>
			<th style="text-align: center;">国际机票退单</th>
			<th style="text-align: center;">酒店订单</th>
			<th style="text-align: center;">酒店退单</th>
			<th style="text-align: center;">火车票订单</th>
			<th style="text-align: center;">火车票退单</th>
			<th style="text-align: center;">保险订单</th>
			<th style="text-align: center;">保险退单</th>
			<th style="text-align: center;">用车订单</th>
			<th style="text-align: center;">其他订单</th>
			<th style="text-align: center;">其他退单</th>
			<th style="text-align: center;">操作</th>
		</tr>
		<tbody>
			<c:forEach var="order" items="${pageList.list}">
				<tr>
					<td>${order.partnerName}</td>
					<td>
						<fmt:formatNumber value="${order.totalAmount == null ? 0 : order.totalAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.refundAmount == null ? 0 : order.refundAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.flightAmount == null ? 0 : order.flightAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.refundFlightAmount == null ? 0 : order.refundFlightAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.internationalFlightAmout == null ? 0 : order.internationalFlightAmout}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.refundInternationalFlightAmout == null ? 0 : order.refundInternationalFlightAmout}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.hotelAmount == null ? 0 : order.hotelAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.refundHotelAmount == null ? 0 : order.refundHotelAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.trainAmount == null ? 0 : order.trainAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.refundTrainAmount == null ? 0 : order.refundTrainAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.insuranceAmount == null ? 0 : order.insuranceAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.refundInsuranceAmount == null ? 0 : order.refundInsuranceAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.carAmount == null ? 0 : order.carAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.generalAmount == null ? 0 : order.generalAmount}" pattern="#,##0.##"/>
					</td>
					<td>
						<fmt:formatNumber value="${order.refundGeneralAmount == null ? 0 : order.refundGeneralAmount}" pattern="#,##0.##"/>
					</td>
					<td><a onclick="showPartnerOrder('${order.partnerId}');" >详情</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<div class="pagin">
		<jsp:include page="../../common/pagination_ajax.jsp?callback=queryLiqAmount"></jsp:include>
	</div>
</div>