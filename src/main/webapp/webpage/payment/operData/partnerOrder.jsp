<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>


<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业订单查询
			</div>
			<div class="col-md-9">
				<nav class="text-left form-inline">
					<form action="payment/operData/orderList" id="queryform">
						<div class=" form-group selectHeader">
							<jsp:include page="../../common/businessSelecter.jsp"></jsp:include>
						</div>
						<div class="form-group">
							<input type="hidden" name="pageIndex">

							<input type="text" name="beginDate" style="width: 110px;" onClick="WdatePicker()" class="form-control" placeholder="开始时间">
							-
							<input type="text" name="endDate" style="width: 110px;" onClick="WdatePicker()"  class="form-control"  placeholder="结束时间">
						</div>
						<div class=" form-group">
							出行性质
							<select class="form-control"  id="travelType" name="travelType" style="width: 90px" placeholder="出行性质">
								<option value="">全部</option>
								<option value="BUSINESS">因公</option>
								<option value="PERSONAL">因私</option>
							</select>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default" onclick="queryLiqAmount()">查询</button>
							<button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
						</div>
					</form>
				</nav>
			</div>
		</div>
		<div id="partner_order_list">

		</div>
	</div>
</div>
<div id="model1"></div>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script>
$(function () {
    initDate();
    var travelType = '${travelType}';
    $("#travelType").val(travelType);
    var option = {
        success : function (data) {
			$("#partner_order_list").html(data);
        }
	}
    $("#queryform").ajaxSubmit(option);
})


function resetForm(){
    $("#queryform :input").val("");
    var option = {
        success : function (data) {
            $("#partner_order_list").html(data);
        }
    }
    $("#queryform").ajaxSubmit(option);
}

function queryLiqAmount(pageIndex){
    pageIndex = pageIndex ? pageIndex : 1;
    $("input[name='pageIndex']").val(pageIndex);
    var option = {
        success : function (data) {
            $("#partner_order_list").html(data);
        }
    }
	$("#queryform").ajaxSubmit(option);
}

function showPartnerOrder(parterId) {
//	var travelType = '${travelType}';
//    $("#travelType").val(travelType);
    showModal("payment/operData/detail",{
        partnerId : parterId,
        beginDate : $('input[name="beginDate"]').val(),
        endDate : $('input[name="endDate"]').val(),
		tmcId: $("#queryBpTmcId").val(),
		travelType: $("#travelType").val()
	},function(data){
        $('#partner_order_detail').modal();
    });
}

//显示Model
function showModal(url, data, callback) {
    $('#model1').load(url, data, function(context, state) {
        if ('success' == state && callback) {
            callback();
        }
    });
}

function initDate(){
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth()+1;
    var begin ;
    var end ;

    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
	begin = year+"-"+month+"-"+ "01";

    var day = new Date(year,month,0);

	end = year + "-"+ month + "-" + day.getDate();

    $('input[name="beginDate"]').val(begin);
    $('input[name="endDate"]').val(end);
}

</script>