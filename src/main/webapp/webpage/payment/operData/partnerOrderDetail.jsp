<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="partner_order_detail" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 800px;">
		<div class="modal-content" style="width: 800px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
				<h4 class="modal-title">
					企业订单数据详情
				</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">
						<dl class="dl-horizontal">
							<dt><h4>订单类型统计</h4></dt>
							<dd></dd>
							<c:forEach items="${partnerOrderForm.orderTypeCount}" var="order">
								<span>${order.orderType}：${order.count}笔 / <fmt:formatNumber value="${order.amount == null ? 0 : order.amount}" pattern="#,##0.##"/>元</span></br>
							</c:forEach>
						</dl>
					</div>
					<div class="col-md-4">
						<dl class="dl-horizontal">
							<dt><h4>下单渠道统计</h4></dt>
							<dd></dd>
							<c:forEach items="${partnerOrderForm.orderOriginCount}" var="order">
								<span>${order.origin}：${order.count}笔 / <fmt:formatNumber type="percent" maxIntegerDigits="3" value="${order.rate}" /></span></br>
							</c:forEach>
						</dl>
					</div>
					<div class="col-md-4">
						<dl class="dl-horizontal">
							<dt><h4>支付方式统计</h4></dt>
							<dd></dd>
							<c:forEach items="${partnerOrderForm.paymentMethod}" var="order">
								<span>${order.channel}：${order.count}笔 /<fmt:formatNumber value="${order.amount == null ? 0 : order.amount}" pattern="#,##0.##"/>元</span></br>
							</c:forEach>
						</dl>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>