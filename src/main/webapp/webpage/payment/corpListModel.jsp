<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<style>
	.selectedCorp{
		background-color: #FFBB66;
	}
</style>

<div class="modal fade" id="modelBpAccountCorp" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" onclick="closeCorpModal()" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">选择法人</h4>
         </div>
         <div class="modal-body">
         	<table style="margin-bottom : 0px" id="selectCorpListTable" class="table table-bordered table-hover">
         		<thead>
         			<td>法人名称</td>
         			<td>社会信用代码</td>
         			<td>开户行</td>
         			<td>开户账号</td>
         		</thead>
         		<c:forEach items="${partnerCorps}" var="partnerCorp">
         			<tr style="cursor: pointer;" onclick="clickTrCorp(this)" attr="${partnerCorp.id }">
         				<td>${partnerCorp.invoiceTitle }</td>
         				<td>${partnerCorp.socialCreditCode }</td>
         				<td>${partnerCorp.accountBank }</td>
         				<td>${partnerCorp.accountNumber }</td>
         			</tr>
         		</c:forEach>
         		<c:forEach items="${hasSetList}" var="hasSet">
         			<tr style="background: #F8F8FF;" attr="${hasSet.id }">
         				<td>${hasSet.invoiceTitle }</td>
         				<td>${hasSet.socialCreditCode }</td>
         				<td>${hasSet.accountBank }</td>
         				<td>${hasSet.accountNumber }</td>
         			</tr>
         		</c:forEach>
			</table>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="closeCorpModal()">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="selectCorp()">确认
            </button>
         </div>
      </div>
	</div>
</div>

<script>
 $(document).ready(function() {
	var selectedList = new Array();
	 //将 已选择的去掉    
	 $('.corpLists').each(function (i){
		 selectedList.push($(this).attr("attr"));
	 });
	 
	 $('#selectCorpListTable').find("tr").each(function (i){
		 var corpId = $(this).attr('attr');
		 for (var i = 0;i < selectedList.length;i++ ){
			 if(selectedList[i] == corpId){
				/*  $(this).remove(); */
				 $(this).removeAttr("style");
				 $(this).removeAttr("onclick");
				 $(this).attr("style", "background: #F8F8FF;");
			 }
		 }
	 });
	 
 });
 
 function clickTrCorp(cur){
	 $('.selectCorpListTable').find("tr").removeClass('activeTree');
	 $(cur).addClass('selectedCorp');
 }
 
 function closeCorpModal(){
	 $('#modelBpAccountCorp').modal('hide');
 }
 
 function selectCorp(){
	 var corpElem = $('.selectedCorp');
	 if(!corpElem || corpElem.length == 0){
		 $.alert("请选择一个法人！",'提示'); 
	 }else{
		var corpId =  $('.selectedCorp').attr('attr');
		var html = "<tr class='corpLists' attr='" + corpId +"'>";
		$('.selectedCorp').find("td").each(function (i){
			if(i <= 1){//只取前两个
				html = html + "<td>" + $(this).html() + "</td>";
			}
		});
		html = html + '<td style="text-align: center;"><p onclick="deleteCorp(this)" style="margin-left: 5px;cursor:pointer;margin-bottom: 0px;" title="删除">X</p></td>';
		html += "</tr>";
		$("#bpAccountScopeCorpTable").append(html);
		$('#modelBpAccountCorp').modal('hide');
	 }
}
 
 </script>