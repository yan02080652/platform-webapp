<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
	.form-horizontal .form-group{margin-left: 0px;margin-right: 0px;}
</style>
<div class="panel panel-default">
	<div class="panel-heading">
		<i class="glyphicon glyphicon-home" aria-hidden="true"></i>TMC退款单
	</div>
	<div class="panel-body">
		<%--top--%>
		<div class="row">
			<div class="col-md-12">
				<div class="pull-left">
					<a class="btn btn-default" onclick="addRefund()" role="button">新增退款单</a>
				</div>
				<!-- 查询条件 -->
				<div class="pull-right">
					<input id="c_tmcId" name="c_tmcId" type="hidden" value="${tmcId}"/>
					<input id="c_tmcName" class="form-control" name="c_tmcName" type="text" placeholder="TMC"
						   style="width: 200px;display: initial;" value="${tmcName}" onclick="selectConPartner()" readonly/>
					<input id="c_orderId" class="form-control" name="c_orderId" type="text" placeholder="订单号"
						   style="width: 200px;display: initial;" value="${orderId}"/>
					<button type="button" class="btn btn-default" onclick="searchRefund()">查询</button>
					<button type="button" class="btn btn-default" onclick="resetRefund()">重置</button>
				</div>
			</div>
		</div>
		<%--table body--%>
		<table style="margin-top : 8px" class="table table-bordered">
			<thead>
			<tr>
				<th class="sort-column" >TMC</th>
				<th class="sort-column" >订单号</th>
				<th class="sort-column" >订单所属企业</th>
				<th class="sort-column" >订单摘要</th>
				<th class="sort-column" >订单金额</th>
				<th class="sort-column" >应退金额</th>
				<th class="sort-column" width="100">创建时间</th>
			</tr>
			</thead>
			<tbody>
			<c:forEach items="${pageList.list}" var="p">
				<tr>
					<td>${partnerNames[p.tmcId]}</td>
					<td>${p.orderId}</td>
					<td>${partnerNames[p.partnerId]}</td>
					<td>${p.orderSummary}</td>
					<td>${p.orderAmount}</td>
					<td>${p.refundAmount}</td>
					<td><fmt:formatDate value="${p.createTime }" pattern="yyy-MM-dd"/></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<div class="pagin">
			<jsp:include page="../../common/pagination_ajax.jsp?callback=queryPage"></jsp:include>
		</div>
	</div>
</div>


<form id="formRefund" class="form-horizontal" method="post" action="payment/payRefund/save" style="display: none;padding-top: 15px;">
	<div class="form-group">
		<label class="col-sm-2 control-label">关联订单号</label>
		<div class="col-sm-9 input-group">
			<input type="text" name="orderId" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">TMC</label>
		<div class="col-sm-9 input-group" onclick="selectPartner()">
			<input type="text" name="tmcName" class="form-control" readonly>
			<input type="hidden" name="tmcId">
			<span class="input-group-addon"><i class="fa fa-align-justify"></i></span>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">所属企业</label>
		<div class="col-sm-9 input-group">
			<input type="text" name="partnerName" class="form-control" readonly>
			<input type="hidden" name="partnerId">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">订单摘要</label>
		<div class="col-sm-9 input-group">
			<input type="text" name="orderSummary" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">订单原金额</label>
		<div class="col-sm-9 input-group">
			<input type="text" name="orderAmount" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">应退金额</label>
		<div class="col-sm-9 input-group">
			<input type="number" name="refundAmount" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">备注</label>
		<div class="col-sm-9 input-group">
			<textarea name="memo" class="form-control"></textarea>
		</div>
	</div>
</form>


<script type="text/javascript" >

var curDlgIndex = null;
$(document).ready(function(){
    var form = $('#formRefund');
    form.find('[name=orderId]').blur(function (e) {
        var orderId = $(this).val();
		$.getJSON('payment/payRefund/getOrderInfo',{
		    orderId:orderId
		},function (data) {
            setValue(data);
        });
    });

    form.validate({
        rules:{
            tmcName:{
                required:true
			},
            refundAmount:{
                required:true
            }
		},
        messages:{
            tmcName:{
                required:'请选择一个TMC'
            },
		},
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight,
        submitHandler: function(form) {

            var formData = $(form).serializeArray();
            var data = {};
            $.each(formData,function (i,o) {
				if(o.value) {
                    data[o.name] = o.value;
                }
            });

            $.ajax({
                type:'POST',
				url:'payment/payRefund/save',
				data:data,
				success:function (rs) {
                    layer.msg(rs.txt);
                    layer.close(curDlgIndex);
                    if(rs.type == 'success'){
                        searchRefund();
                    }
                },
				error:function () {
                    layer.close(curDlgIndex);
                }
			});
        }
    });
});

function queryPage(page) {
    searchRefund({
        page:page
	});
}

function resetRefund(){
    $('#c_tmcId').val(null);
    $('#c_tmcName').val(null);
    $('#c_orderId').val(null);
}

function searchRefund(con) {
    var param = $.extend(con,{
        orderId:$('#c_orderId').val(),
        tmcId:$('#c_tmcId').val()
    })
	var paramArr = [];
	$.each(param,function (k,v) {
		if(v){
            paramArr.push(k + "=" + v);
		}
    });
	window.location.href = "/payment/payRefund?"+paramArr.join('&');
}

/**
 * 添加一个退款单
 */
function addRefund() {
    $('#formRefund')[0].reset();
	layer.open({
	    title:'新增退款单',
	    type:1,
		content:$('#formRefund'),
        area:['600px','500px'],
		btn:['确定','取消'],
        zIndex:100,
        btn1:function(index){
            curDlgIndex = index;
            $('#formRefund').submit();
		},
		btn2:function (index) {
			layer.close(index);
        }
    });
}

/**
 * 选择TMC
 */
function selectPartner() {
	TR.select('partner',{
        type:1
    },function (rs) {
		setValue({
		    tmcId:rs.id,
			tmcName:rs.name
		})
    });
}

function selectConPartner() {
    TR.select('partner',{
        type:1
	},function (rs) {
        $('#c_tmcId').val(rs.id);
        $('#c_tmcName').val(rs.name);
    });
}

function setValue(obj){
    if(!obj){
        return;
	}
    var form = $('#formRefund');
    $.each(obj,function (k, v) {
        form.find("[name=" + k + "]").val(v);
    })
}

</script>