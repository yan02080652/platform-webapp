<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
	.jiesuanInput{
		border-left-width: 0px;
	    border-top-width: 0px;
	    border-right-width: 0px;
	    width: 60px;
	    height: 28px;
	    border-color: #444444;
	}
	.modal-body {
		max-height: 450px;
		overflow-y: auto;
	}
</style>
<div class="modal fade" id="modelBpAccountField" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">${typeTitle}</h4>
         </div>
         <div class="modal-body">
         	<form modelAttribute="bpAccountDto" class="form-horizontal" id="bpAccountForm" role="form">
         		<p style="font-weight:bold;margin-left: 15px;margin-bottom: 5px;">基础信息</p>
         		<hr style="margin-top:0px;margin-bottom: 10px;"/>
            	<input type="hidden" id="bpAccountId" name="id" value="${bpAccount.id}"/>
            	<input type="hidden" id="bpAccountBpId" name="bpId" value="${bpAccount.bpId}"/>
            	<div id="partnerInfoDiv1" class="form-group">
			      <label for="bpName" class="col-sm-3 control-label">企业名称</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" onclick="selectBpPartner()" id="bpAccountBpName" name="bpName" value="${bpName}" placeholder="企业名称"/>
			      </div>
			   </div>
			   <div id="partnerInfoDiv2" class="form-group" style="display: none;">
			      <label for="bpName" class="col-sm-3 control-label">企业名称</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" readonly="readonly" name="bpName" value="${bpName}" placeholder="企业名称"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="accountName" class="col-sm-3 control-label">账户名称</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" id="accountName" name="accountName" value="${bpAccount.accountName}" placeholder="账户名称"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="accountType" class="col-sm-3 control-label">账户类型</label>
			      <div class="col-sm-9">
			         <select class="form-control" id="bpAaccountType" name="accountType" placeholder="账户类型">
						<option value="B">预存账户</option>
						<option value="C">授信账户</option>
					</select>
			      </div>
			   </div>
			   <div class="form-group" id="creditAmountDiv" style="display: none;">
				    <label for="creditAmount" class="col-sm-3 control-label">授信额度</label>
				    <div class="col-sm-3" >
				   	  <input type="number" class="form-control" id="bpAccountCreditAmount" readonly="readonly" value="${grantedCredit}" placeholder="授信额度"/>
				    </div>
				    <div class="col-sm-3" id="changeCreditAmountButton">
				   	  <a class="btn btn-default"  role="button" style="background-color: #99DD00;" onclick="changeCreditAmount()">调整额度</a>
				    </div>
				    <div class="col-sm-3" id="changeLogButton">
				   	  <a class="btn btn-default"  role="button" style="background-color: #99DD00;" onclick="changeLog()">变更记录</a>
				    </div>
				</div>
				<div class="form-group">
			      <label for="warnLine" class="col-sm-3 control-label">警戒金额</label>
			      <div class="col-sm-9">
			         <input type="number" class="form-control" name="warnLine" value="${bpAccount.warnLine*0.01}" placeholder="余额不足多少元时"/>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="warnPhone" class="col-sm-3 control-label">通知电话</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" name="warnPhone" value="${bpAccount.warnPhone}" placeholder="余额不足电话通知"/>
			      </div>
			   </div>
				<div class="form-group">
					<label class="col-sm-3 control-label">联系人</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="contactName" value="${bpAccount.contactName}" placeholder="联系人姓名"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">手机号</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="contactMobile" value="${bpAccount.contactMobile}" placeholder="联系人手机号"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">邮箱</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="contactEmail" value="${bpAccount.contactEmail}" placeholder="联系人邮箱"/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">发票邮寄地址</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="contactAddress" value="${bpAccount.contactAddress}" placeholder="发票邮寄地址"/>
					</div>
				</div>
				<%-- <div class="form-group" id="balanceAmountDiv" style="display: none;">
				    <label for="creditAmount" class="col-sm-3 control-label">账户余额</label>
				    <div class="col-sm-3">
				   	  <input type="number" class="form-control" id="bpAccountBalanceAmount" readonly="readonly" value="${bpAccount.balanceAmount*0.01}"/>
				    </div>
				    <div class="col-sm-3" id="changeAmountButton">
				   	  <a class="btn btn-default"  role="button" style="background-color: #99DD00;width:82px;" onclick="addAmount()">充值</a>
				    </div>
				</div> --%>
				<div>
					<span style="font-weight:bold;margin-left: 15px;margin-bottom: 5px; ">适用法人 </span> 
					<span style="margin-left: 15px;cursor:pointer;font-size: 22px;" onclick="addCorp()" title="新增">+</span> 
				</div>
         		<hr style="margin-top:0px;margin-bottom: 10px;"/>
         		<div>
         			<table style="margin-bottom : 0px;margin-left: 25px;width: 550px;" class="table table-bordered" id="bpAccountScopeCorpTable">
         				<tbody>
         					<c:forEach items="${partnerCorps}" var="partnerCorp">
			         			<tr class="corpLists" attr="${partnerCorp.id }" id="${partnerCorp.id }">
			         				<td>${partnerCorp.invoiceTitle }</td>
			         				<td>${partnerCorp.socialCreditCode }</td>
			         				<%-- <td>${partnerCorp.accountBank }</td>
			         				<td>${partnerCorp.accountNumber }</td> --%>
			         				<td style="text-align: center;"><p onclick="deleteCorp(this)" style="margin-left: 5px;cursor:pointer;margin-bottom: 0px;" title="删除">X</p></td>
			         			</tr>
			         		</c:forEach>
         				</tbody>
					</table>
         		</div>
         		
         		<p style="font-weight:bold;margin-left: 15px;margin-bottom: 5px;margin-top: 15px;">结算规则</p>
         		<hr style="margin-top:0px;margin-bottom: 10px;"/>
         		<div class="form-group">
			      <label for="liquidationDay" class="col-sm-3 control-label">企业结算日</label>
			      <div class="col-sm-9">
			         &nbsp;&nbsp;
			         <input type="number" class="jiesuanInput" name="liquidationDay" value="${bpAccount.liquidationDay}"/>
			         <font color="red">正数为指定日期，负数为月末前多少天</font>
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="overdueDays" class="col-sm-3 control-label">账单逾期天数</label>
			      <div class="col-sm-9">
			       	&nbsp;&nbsp;
			       	<input type="number" class="jiesuanInput" name="overdueDays" value="${bpAccount.overdueDays}" />天&nbsp; 账单生成后
			      </div>
			   </div>
			   <div class="form-group">
			      <label for="lateFee" class="col-sm-3 control-label">滞纳金</label>
			      <div class="col-sm-9">
			         &nbsp;&nbsp;
			         <input type="number" class="jiesuanInput" name="lateFee" value="${bpAccount.lateFee}"/>‱  &nbsp;每日
			      </div>
			   </div>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="submitAuthField()">保存
            </button>
         </div>
      </div>
	</div>
	<div id="bpAccoountChangeLogDiv">
		<div class="modal fade" id="modelBpAccountChangeLog" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
		   <div class="modal-dialog">
		      <div class="modal-content">
		         <div class="modal-header">
		            <button type="button" class="close" onclick="closeChangeLogModal()" aria-hidden="true"> &times;</button>
		            <h4 class="modal-title">变更历史</h4>
		         </div>
		         <div class="modal-body">
		         	<table style="margin-bottom : 0px" class="table table-bordered table-hover table-striped">
		         		<thead>
		         			<td>变更人</td>
		         			<td>变更时间</td>
		         			<td>原授信额度</td>
		         			<td>修改后额度</td>
		         			<td>备注</td>
		         		</thead>
		         		<c:forEach items="${changeLogs}" var="changeLog">
		         			<tr>
		         				<td>${changeLog.updateUserName }</td>
		         				<td><fmt:formatDate value="${changeLog.updateDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
		         				<td>${changeLog.oldValue*0.01 }</td>
		         				<td>${changeLog.newValue*0.01 }</td>
		         				<td>${changeLog.remark }</td>
		         			</tr>
		         		</c:forEach>
					</table>
		         </div>
		         <div class="modal-footer">
		            <button type="button" class="btn btn-default" onclick="closeChangeLogModal()">关闭
		            </button>
		            <button type="button" class="btn btn-primary" onclick="closeChangeLogModal()">确认
		            </button>
		         </div>
		      </div>
			</div>
		</div>
	</div>
</div>

<div id="bpAccoountCorpDiv"></div>
	
<div id="rechargeAmountDiv"></div>

<script>
 $(document).ready(function() {
	var bpAccountId = $("#bpAccountId").val();
	if(bpAccountId != null && bpAccountId != ''){
		$("#accountName").attr("readonly","readonly");
		$("#partnerInfoDiv1").hide();
		$("#partnerInfoDiv2").show();
		$("#bpAaccountType").val("${bpAccount.accountType}");
		
		$("#creditAmountDiv").show();
		$("#balanceAmountDiv").show();
		
	}
 });
 
 function closeChangeLogModal(){
	 $('#modelBpAccountChangeLog').modal('hide');
 }
 
 function changeLog(){
	 $('#modelBpAccountChangeLog').modal();
 }
 
 function selectBpPartner(){
     var type = '${type}';
     var partnerType = null;
     if(type == 'tmc'){
         partnerType = 1;
     }else{
         partnerType = 0;
     }
	TR.select('partner',{
		type:partnerType//固定参数
	},function(data){
		$("#bpAccountBpId").val(data.id);
		$("#bpAccountBpName").val(data.name);
		$("#bpAccountScopeCorpTable tr").remove();
		$("#accountName").val("");
	});
 }
 
 function addCorp(){
	 //得到选定法人列表的id
	var trIds = new Array();
	var a=document.getElementById("bpAccountScopeCorpTable").getElementsByTagName("tr");
	var ids = null;
	if(a.length>0){	
		for (var i=0;i<a.length;i++) {
			trIds[i] = a[i].id;
		}
		ids = trIds.join(",");
	}
	//得到账户id  和企业id
	 var bpAccountBpId = $("#bpAccountBpId").val();
	 if(!bpAccountBpId){
		 $.alert("请先选择企业！",'提示');
		 return false;
	 }
	 showCorpModal("bpAccount/manage/getCorpList?partnerId=" + bpAccountBpId + "&ids=" + ids, function(a) {
		$('#modelBpAccountCorp').modal();
	 });
 }

function showCorpModal(url, callback) {
	$('#bpAccoountCorpDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function changeCreditAmount(){
	showChangeAmountModal("bpAccount/manage/rechargeAmount?amountType=credit", function(a) {
		$('#modelRechargeAmount').modal();
	 });
}

function addAmount(){
	showChangeAmountModal("bpAccount/manage/rechargeAmount?amountType=balance", function(a) {
		$('#modelRechargeAmount').modal();
	 });
}

function showChangeAmountModal(url, callback) {
	$('#rechargeAmountDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function selectCorp(){
	 var corpElem = $('.selectedCorp');
	 if(!corpElem || corpElem.length == 0){
		 $.alert("请选择一个法人！",'提示'); 
	 }else{
		var corpId =  $('.selectedCorp').attr('attr');
		var html = "<tr class='corpLists' attr='" + corpId +"'>";
		$('.selectedCorp').find("td").each(function (i){
			html = html + "<td>" + $(this).html() + "</td>";
			
		});
		html = html + '<td style="text-align: center;"><p onclick="deleteCorp(this)" style="margin-left: 5px;cursor:pointer;margin-bottom: 0px;" title="删除">X</p></td>';
		html += "</tr>";
		$("#bpAccountScopeCorpTable").append(html);
		$('#modelBpAccountCorp').modal('hide');
	 }
}

function deleteCorp(cur){
	$(cur).parent().parent().remove();
}
 
 </script>