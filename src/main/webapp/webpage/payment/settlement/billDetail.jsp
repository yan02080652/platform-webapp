<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/css/order/qyb.lib.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/order/template.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">

<style>
.nomalFond{
	font-weight:normal;
}

.button {
    display: inline-block;
    background-color: #1176cc;
    color: white;
    width: 135px;
    text-align: center;
    margin-left: 0;
    border-radius:2px;
    cursor: pointer;
    margin-left: 30px;
    line-height: 32px;
}

.button:hover{
    background-color: #0063a0;
}

.buttonInvoice {
    display: inline-block;
    background-color: #00AA88;
    color: white;
    width: 75px;
    text-align: center;
    margin-left: 0;
    border-radius:2px;
    cursor: pointer;
    margin-left: 30px;
    line-height: 32px;
}

.buttonInvoice:hover{
    background-color: #008866;
}

.pd0{
	padding-right: 0px;
}

.buttonMain{
	display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
</style>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>账单明细
                 <input type="hidden" id="linkFromInput" value="${linkFrom}">
             	 <%--&nbsp;&nbsp;<a data-pjax href="javascript:;" onclick="window.history.back()" >返回</a>--%>
             </div>
             <div class="panel-body">
               	 <form modelAttribute="billDto" class="form-horizontal" id="billBaseInfoForm" role="form">
					 <input name="accountId" id="accountId" type="hidden" value="${billInfo.accountId }"/>
					 <input name="id" id="billId" type="hidden" value="${billInfo.id}"/>
					 <input name="bpId" type="hidden" value="${billInfo.bpId}"/>
					 <input name="saveMode" id="saveMode" type="hidden"/>
					 <input id="tabIndex" type="hidden" value="${tabIndex }"/>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">企业名称:</label>
					    <div class="col-sm-2">
					      <input type="text" name="bpName" readonly="readonly" class="form-control" value="${billInfo.bpName }">
					    </div>
					    <label class="col-sm-1 control-label nomalFond">账户名称:</label>
					    <div class="col-sm-2">
					      <input type="text" name="accountName" readonly="readonly" class="form-control" value="${billInfo.accountName }">
					    </div>
					    <label class="col-sm-1 control-label nomalFond">账单生成日期:</label>
					    <div class="col-sm-2">
					      <input type="text" name="createDate" readonly="readonly" class="form-control" value="<fmt:formatDate value='${billInfo.createDate }' pattern='yyyy-MM-dd'/>">
					    </div>				    
					 </div>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label nomalFond">账单金额:</label>
					    <div class="col-sm-2">
					      <input type="number" name="billAmount" readonly="readonly" class="form-control" value="${billInfo.billAmount*0.01 }">
					    </div>
					    <label class="col-sm-1 control-label nomalFond">结算期间:</label>
					    <div class="col-sm-1" style="padding-right: 0px;">
					      <input type="text" name="liqDateBegin" onclick="WdatePicker()" class="form-control" value="<fmt:formatDate value='${billInfo.liqDateBegin }' pattern='yyyy-MM-dd'/>">
					    </div>
					    <div class="col-sm-1" style="padding-left: 0px;">
					    	<input type="text" name="liqDateEnd" onclick="WdatePicker()" class="form-control" value="<fmt:formatDate value='${billInfo.liqDateEnd }' pattern='yyyy-MM-dd'/>">
					    </div>
					    <label class="col-sm-1 control-label nomalFond">逾期日:</label>
					    <input type="hidden" value="${overDays }" id="overdueDays">
					    <div id="overdueDateDiv" class="col-sm-2">
					      <input type="text" name="overdueDate" onclick="WdatePicker()" class="form-control" value="<fmt:formatDate value='${billInfo.overdueDate }' pattern='yyyy-MM-dd'/>">
					    </div>
					    <div id="overdueDaysDiv" class="col-sm-1" style="display: none;">
					       <p style="margin-bottom: 0px;margin-top: 6px;"><font color="red">已逾期${overDays }天</font></p>
					    </div>	
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">账单备注</label>
					    <div class="col-sm-8">
					      <input type="text" name="remark" class="form-control" value="${billInfo.remark }">
					    </div>
					 </div>
					 <%-- <div class="form-group">
					 	<label class="col-sm-2 control-label nomalFond">订单金额:</label>
					    <div class="col-sm-2">
					      <input type="number" name="orderAmount" readonly="readonly" class="form-control" value="${billInfo.orderAmount*0.01 }">
					    </div>
					    <label class="col-sm-1 control-label nomalFond">已开票金额:</label>
					    <div class="col-sm-2" id="openedAmount">
					      <input type="number" name="invoiceAmount" readonly="readonly" class="form-control" value="${billInfo.invoiceAmount*0.01 }">
					    </div>
					    <div class="col-sm-1" id="queryInvoice" style="padding-left: 0px;display: none;">
					      <span  onclick="queryInvoice('${billInfo.id}')" class="buttonInvoice">查看发票</span>
					    </div>
					    <label class="col-sm-1 control-label nomalFond">未开票金额:</label>
					    <div class="col-sm-2" id="unOpenAmount">
					      <c:if test="${(billInfo.billAmount-billInfo.invoiceAmount)*0.01 >= 0}">
					      	<input type="number" readonly="readonly" class="form-control" value="${(billInfo.billAmount-billInfo.invoiceAmount)*0.01 }">
					      </c:if>
					      <c:if test="${(billInfo.billAmount-billInfo.invoiceAmount)*0.01 <0}">
					      	<input type="number" readonly="readonly" class="form-control" value="0.0">
					      </c:if>
					    </div>
					    <div class="col-sm-1" id="openInvoice" style="padding-left: 0px;display: none;">
					      <span  onclick="openInvoice('${billInfo.id}')" class="buttonInvoice">开票</span>
					    </div>
					 </div>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label nomalFond">调整金额:</label>
					    <div class="col-sm-2">
					      <input type="number" name="adjustAmount" readonly="readonly" class="form-control" value="${billInfo.adjustAmount*0.01 }">
					    </div>
					    <label class="col-sm-1 control-label nomalFond">系统补贴:</label>
					    <div class="col-sm-2">
					      <input type="number" id="subsidyTotal" readonly="readonly" class="form-control" value="${(billInfo.adjustAmount*-1+canUseSubsidy)*0.01 }">
					    </div>
					    <label class="col-sm-1 control-label nomalFond">本次使用:</label>
					    <div class="col-sm-2">
					      <input type="number" onchange="userSubsidy(this)" id="curUserAmount" class="form-control" value="${billInfo.adjustAmount*-1*0.01 }">
					    </div>
					 </div>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label nomalFond">调整说明:</label>
					    <div class="col-sm-8">
					      <input type="text" name="adjustDesc" class="form-control" value="${billInfo.adjustDesc }">
					    </div>
					 </div> --%>
					 <input type="hidden" name="remainAmount" value="${billInfo.remainAmount*0.01 }">
					 <input type="hidden" name="adjustAmountBefore" value="${billInfo.adjustAmount*-1*0.01 }">
					 <div class="form-group">
					   <%--  <label class="col-sm-2 control-label nomalFond">最后修改人:</label>
					    <div class="col-sm-2">
					      <input type="text" readonly="readonly" class="form-control" value="${billInfo.updateUserName }">
					    </div> --%>
					    <label class="col-sm-4 control-label nomalFond"></label>
						   <div class="col-sm-6">

							   <div class="row">
									<div class="col-sm-3">
										<c:if test="${billInfo.liquidationStatus ne 'CLOSE' && billInfo.status != 'CANCEL' && not empty billInfo.id }">
											<span onclick="cancelBill('${billInfo.id}')" class="button">账单作废</span>
										</c:if>
									</div>
									<div class="col-sm-9" id="billSaveBaseInfoDiv">

										<c:if test="${empty billInfo.id or billInfo.status eq 'DRAFT'}">
											<span title="操作后续账单明细前 请先保存账单基本信息" onclick="saveBillBaseInfo('draft')" class="button">保存草稿</span>
										</c:if>
										<c:if test="${not empty billInfo.id and billInfo.status eq 'DRAFT'}">
											<span title="操作后此订单将发送给客户确认" onclick="sendBill('${billInfo.id}', '${billInfo.billAmount}')" class="button">发送账单</span>
										</c:if>
										<c:if test="${not empty billInfo.id and billInfo.status eq 'NOT_CONFIRM'}">
											<span onclick="saveBillBaseInfo('update')" class="button">保存修改</span>
										</c:if>
										<c:if test="${not empty billInfo.id and billInfo.status eq 'NOT_CONFIRM'}">
											<span onclick="confirmBill('${billInfo.id}')" class="button">客户确认</span>
										</c:if>

									</div>
							   </div>

						   </div>
					 </div>
				</form>
				<div class="form-horizontal">
					 <c:if test="${not empty billInfo.id}"><%-- 修改的时候才显示清账信息 --%>
					 	 <div class="form-group" style="margin-bottom: 0px;">
					 		<label class="col-sm-1 control-label" style="font-size: 17px;"></label>
					 		<div class="col-sm-8">
					 			<span style="margin-top: 6px;">
					 				<font style="font-weight: bold;font-size: 17px;margin-left: 30px;"> 
					 					已清账金额:<fmt:formatNumber value="${(billInfo.billAmount-unLiquidAmount)*0.01}" pattern="#,##0.##"/>元&nbsp;&nbsp;
					 					未清账金额:<fmt:formatNumber value="${unLiquidAmount*0.01}" pattern="#,##0.##"/>元
					 					<%-- <c:if test="${unLiquidAmount > 0 }">
					 						已清账金额:<fmt:formatNumber value="${(billInfo.billAmount-unLiquidAmount)*0.01}" pattern="#,##0.##"/>元&nbsp;&nbsp;
					 						未清账金额:<fmt:formatNumber value="${unLiquidAmount*0.01}" pattern="#,##0.##"/>元
					 					</c:if>
					 					<c:if test="${unLiquidAmount <= 0 }">
					 						已清账
					 					</c:if> --%>
					 				</font>
					 			</span>
								<c:if test="${billInfo.status ne 'CANCEL'}">
									<span class="button" onclick="reloadExportOrder(${billInfo.id})">导出</span>
								</c:if>
					 		</div>
					 	</div>
					 	<div class="form-group">
					 		<div class="col-sm-1">
					 		</div>
						 	<div class="col-sm-9">
						 		<c:if test="${liquidPayRecordList.size() > 0}">
						 			<table class="table table-striped table-hover table-condensed dataTables-example dataTable" style="margin-left: 30px;">
										<thead>
											<tr>
												<th class="sort-column" style="width: 100px;text-align: left;">付款日期</th>
												<th class="sort-column" style="min-width: 70px;text-align: center;">付款方式</th>
												<th class="sort-column" style="min-width: 90px;text-align: center;">付款流水号</th>
												<th class="sort-column" style="width: 120px;text-align: center;">付款金额</th>
												<th class="sort-column" style="width: 120px;text-align: center;">本账单清账金额</th>
												<th class="sort-column" style="width: 100px;text-align: center;">清账日期</th>
												<th class="sort-column" style="width: 90px;text-align: center;">清账人</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${liquidPayRecordList}" var="liquidPayRecord">
												<tr>
													<td style="border: 0px;"><fmt:formatDate value="${liquidPayRecord.paymentDate}" pattern="yyyy-MM-dd"/></td>
													<td style="border: 0px;">${liquidPayRecord.payTypeName}</td>
													<td style="border: 0px;">${liquidPayRecord.refRecord}</td>
													<td style="border: 0px;"><fmt:formatNumber value="${liquidPayRecord.payAmount*0.01}" pattern="#,##0.##"/></td>
													<td style="border: 0px;"><fmt:formatNumber value="${liquidPayRecord.liquidationAmount*0.01}" pattern="#,##0.##"/></td>
													<td style="border: 0px;"><fmt:formatDate value="${liquidPayRecord.createDate}" pattern="yyyy-MM-dd"/></td>
													<td style="border: 0px;">${liquidPayRecord.createUserName}</td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
						 		</c:if>
							</div>
						</div>
						<div class="form-group">
						 	<div class="col-sm-1">
						 	</div>
						 	<div class="col-sm-9" style="margin-left: 30px;">
						 		<ul id="myTab" class="nav nav-tabs" data-trigger="tab">
						 			<li><a href="#orderDetail" onclick="javascript:;">账单构成</a></li>
									<li><a href="#orderList" onclick="javascript:;">订单明细</a></li>
									<li><a href="#invoiceInfo" onclick="javascript:;">发票信息</a></li>
								 </ul>
								 <div class="pane-wrapper">
								 	<div id="billSummary">
								 		<p style="padding-top: 10px;padding-left: 15px;background-color: #f9f9f9;margin-top: 10px;padding-bottom: 10px;">订单结算：<fmt:formatNumber value="${billInfo.orderAmount*0.01 }" pattern="#,##0.##"/>元</p>
							 			<table class="table table-condensed dataTables-example dataTable" style="margin-left: 10px;">
											<tbody>
												<c:forEach items="${billStatList}" var="billStat">
													<tr>
														<td width="150px" style="border: 0px;padding-left: 35px;">${billStat.bizName}</td>
														<td width="150px" style="border: 0px;"><fmt:formatNumber value="${billStat.amount*0.01}" pattern="#,##0.##"/>元</td>
														<td style="border: 0px;text-align: left;">${billStat.remark}</td>
														<%-- <td><input type="text" style="width: 100%" name="billStatRemark" value="${billStat.remark}"></td> --%>
														<td width="10px" style="border: 0px;"></td>
													</tr>
												</c:forEach>
												<c:forEach items="${billAdjitemDtos}" var="billAdjitemDto">
													<tr style="background-color: #f9f9f9;">
														<td width="150px" style="border: 0px;">
															<c:if test="${billAdjitemDto.category eq 'TAX'}">调整项-附加税额</c:if>
															<c:if test="${billAdjitemDto.category eq 'SUBSIDY'}">调整项-补贴减免</c:if>
															<c:if test="${billAdjitemDto.category eq 'SERVICE_FEE'}">调整项-服务费</c:if>
															<c:if test="${billAdjitemDto.category eq 'OTHER'}">调整项-其它</c:if>
														</td>
														<td width="150px" style="border: 0px;"><fmt:formatNumber value="${billAdjitemDto.amount*0.01}" pattern="#,##0.##"/>元</td>
														<td style="border: 0px;text-align: left;">${billAdjitemDto.remark}</td>
														<td width="10px" style="border: 0px;">
															<c:if test="${billInfo.liquidationStatus ne 'CLOSE' and billInfo.status ne 'CONFIRM' and billInfo.status ne 'CANCEL'}">
																<span onclick="deleteAdjitem('${billAdjitemDto.id}');" class="glyphicon glyphicon-remove"></span>
															</c:if>
														</td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
										<c:if test="${billInfo.liquidationStatus ne 'CLOSE' and billInfo.status ne 'CONFIRM' and billInfo.status ne 'CANCEL'}">
											<span onclick="adjustBillAmount('${billInfo.id}')" style="margin-top: 10px;margin-left: 15px;width: 90px;" class="button">+账单调整</span>&nbsp;&nbsp;&nbsp;&nbsp;当前可用补贴金额：<fmt:formatNumber value="${canUseSubsidy*0.01 }" pattern="#,##0.##"/>元
										</c:if>
								 	</div>
								 	<div id="billOrderList">
								 	</div>
								 	<div id="invoiveInfo">
								 		<p style="padding-top: 10px;font-size:16px;padding-left: 15px;background-color: #f9f9f9;margin-top: 10px;padding-bottom: 10px;">已开票金额：<fmt:formatNumber value="${billInfo.invoiceAmount*0.01 }" pattern="#,##0.##"/>元</p>
								 		<table class="table  table-condensed dataTables-example dataTable" style="margin-left: 10px;">
											<tbody>
												<c:forEach items="${invoiceDtos}" var="invoiceDto">
													<tr>
														<td style="border: 0px;">${invoiceDto.invoiceDest}</td>
														<td style="border: 0px;">
															<c:if test="${invoiceDto.invoiceType eq 'tem'}">实物发票</c:if>
															<c:if test="${invoiceDto.invoiceType eq 'FLIGHT'}">机票行程单</c:if>
															<c:if test="${invoiceDto.invoiceType eq 'OTHER'}">其它</c:if>
														</td>
														<td style="border: 0px;"><fmt:formatNumber value="${invoiceDto.totalAmount*0.01}" pattern="#,##0.##"/>元</td>
														<td style="border: 0px;">${invoiceDto.invoiceNo}</td>
														<td style="border: 0px;"><fmt:formatDate value="${invoiceDto.invoiceDate}" pattern="yyyy-MM-dd"/></td>
														<td style="border: 0px;">
															<c:if test="${invoiceDto.shippingStatus eq 'NOTSHIP'}">未寄送</c:if>
															<c:if test="${invoiceDto.shippingStatus eq 'SHIPED'}">已寄送</c:if>
														</td style="border: 0px;">
														<td style="border: 0px;"><a href="javascript:;" onclick="queryinvoiceDetail('${invoiceDto.id}')">查看明细</a></td>
													</tr>
												</c:forEach>
											</tbody>
										</table>
								 		<p style="padding-top: 10px;padding-left: 15px;">未开票金额：<fmt:formatNumber value="${(billInfo.billAmount-billInfo.invoiceAmount)*0.01 }" pattern="#,##0.##"/>元
								 		<c:if test="${billInfo.status eq 'CONFIRM'}">
								 			<span onclick="openInvoice('${billInfo.id}')" class="buttonInvoice">开票</span></p>
								 		</c:if>
								 	</div>
								 </div>
						 	</div>
						 </div>
					 </c:if>
				</div>
             </div>
         </div> 
	</div>
</div>

<div id="openInvoiceDiv"></div>

<div id="adjustBillAmountDiv">
	<div class="modal fade" id="modelAdjustBillAmount" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	   <div class="modal-dialog">
	      <div class="modal-content">
	         <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
	            <h4 class="modal-title">账单调整</h4>
	         </div>
	         <div class="modal-body">
				<div class="row">
					<form modelAttribute="billAdjitemDto" class="form-horizontal" id="adjustBillAmountForm" role="form">
						 <input name="billId" type="hidden" id='adjustBillAmountBillId' value="${billInfo.id}"/>
						 <div class="form-group">
						    <label class="col-sm-3 control-label nomalFond">调整类型:</label>
						    <div class="col-sm-7 inputNewInfo">
							      <select class="form-control" name="category" onchange="selectCategory(this)">
							      	 <option value="TAX">附加税额</option>
							      	 <option value="SUBSIDY">补贴减免</option>
							      	 <option value="SERVICE_FEE">服务费</option>
							      	 <option value="OTHER">其它</option>
								  </select>
						    </div>
						 </div>
						 <div class="form-group">
						    <label class="col-sm-3 control-label nomalFond">调整金额:</label>
						    <div class="col-sm-7 inputNewInfo">
						      <input type="number" name="amount" required="required" class="form-control" >
						    </div>
						 </div>
						 <div class="form-group">
						    <label class="col-sm-3 control-label nomalFond">备注:</label>
						    <div class="col-sm-7">
						      <input type="text"  name="remark" class="form-control" >
						    </div>
						 </div>
						 <%-- <div class="form-group" style="display: none;" id="canUseSubsidyDiv">
						    <label class="col-sm-3 control-label nomalFond">可用补贴:</label>
						    <div class="col-sm-7 inputNewInfo">
						      <input type="number"  value="${canUseSubsidy*0.01 }" readonly="readonly" class="form-control" >
						    </div>
						 </div> --%>
						 <p style=";margin-left: 70px;">当前可用补贴金额：<fmt:formatNumber value="${canUseSubsidy*0.01 }" pattern="#,##0.##"/></p>
						 
						 <p style="color: red;margin-left: 70px;">金额请注意正负，正数增加账单金额，负数减少账单金额</p>
					</form>
				</div>		
	         </div>
	         <div class="modal-footer">
	            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
	            </button>
	            <button type="button" class="btn btn-primary" onclick="saveBillAdjitem()">保存
	            </button>
	         </div>
	      </div>
		</div>
	</div>
</div>

<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tab.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tooltip.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(document).ready(function(){
	initFormField();
	
	var overdueDays = $('#overdueDays').val();
	if(overdueDays > 0){
		$('#overdueDaysDiv').show();
	}
	
	var billId = $('#billId').val();
	if(billId){
		/* $('#unOpenAmount').removeClass('col-sm-2');
		$('#openedAmount').removeClass('col-sm-2');
		$('#unOpenAmount').addClass('pd0');
		$('#openedAmount').addClass('pd0');
		$('#unOpenAmount').addClass('col-sm-1');
		$('#openedAmount').addClass('col-sm-1'); */
		
		$('#openInvoice').show();
		/* $('#queryInvoice').show(); */
	}
	loadBillOrderList();
	
	//查看订单金额、账单金额、调整金额三者关系是否一致 不一致提示调整 调整金额
	/* var orderAmount = $("input[name='orderAmount']").val();
	var adjustAmount = $("input[name='adjustAmount']").val();
	var billAmount = $("input[name='billAmount']").val();
	
	if(parseFloat(orderAmount) + parseFloat(adjustAmount) != parseFloat(billAmount)){
		$.alert('订单金额 +调整金额≠账单金额 ,请调整红包使用金额 ','提示');
	} */
	
	var tabIndex = $('#tabIndex').val();
	if(tabIndex == "orderList"){//说明默认要打开账单明细tab
		$('#myTab a[href="#orderList"]').click();
	}
	
	
	//判断订单状态 如果是关闭的 则不能操作该订单
	var liquidationStatus = '${billInfo.liquidationStatus }';
	var status = '${billInfo.status }';
	if(liquidationStatus == "CLOSE" || status == "CONFIRM"){ //关闭的账单不能再操作//确认的账单也不能操作
		$('#billSaveBaseInfoDiv').hide();
	
		$("#curUserAmount").attr("readonly","readonly");
		$("input[name='remark']").attr("readonly","readonly");
		$("input[name='overdueDate']").attr("readonly","readonly");
		$("input[name='adjustDesc']").attr("readonly","readonly");
		
		$('#openInvoice').hide();
		/* $('#unOpenAmount').removeClass('pd0');
		$('#unOpenAmount').removeClass('col-sm-1');
		$('#unOpenAmount').addClass('col-sm-2'); */
	}
});

//删除调整项
function deleteAdjitem(id) {
	$.post('bpAccount/settlement/deleteAdjitem',{
	    id:id
	},function(data){
        if (data){
            window.location.href="/bpAccount/settlement/billDetail?id="+$('#adjustBillAmountBillId').val()+"&linkFrom="+$("#linkFromInput").val();
        }else{
            showErrorMsg(data.txt);
        }
	})
}

function loadBillOrderList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("#queryBillOrderListform input[name='pageIndex']").val(pageIndex);
	var dataParam = $('#queryBillOrderListform').serialize();
	dataParam = dataParam + "&billId=" + $('#billId').val();
	$('#billOrderList').load("bpAccount/settlement/billOrderList",dataParam, function(context, state) {
		if ('success' == state) {
		}
	});
}

//导出账单
function reloadExportOrder(id){

    window.location.href='/bpAccount/settlement/exportBill.html?id='+id;
}

function queryinvoiceDetail(id){
	window.open("payment/invoice/detail?invoiceId="+id);
}

function backBillList(accountId,linkFrom){
	if(linkFrom && linkFrom == 'billList'){
		window.location.href = "/bill/manage";
	}else{
		window.location.href = "/bpAccount/settlement/accountDetail?id="+accountId;
	}
}

function queryInvoice(billId){
	var billId = $('#billId').val();
	var url = "payment/invoice?billId="+billId;
	//window.open(url,"","width=1200,height=600,top=50,left=100");
	window.open(url);
}

function openInvoice(billId){
	showOpenInvoiceModal("bpAccount/settlement/openInvoice?billId="+billId, function() {
		$('#modelOpenInvoice').modal();
	});
}

function showOpenInvoiceModal(url, callback) {
	$('#openInvoiceDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}


function initFormField() {
	$("#billBaseInfoForm").validate({
		rules:{
			liqDateBegin : {
				required : true
			},
			liqDateEnd : {
				required : true
			},
			overdueDate : {
				required : true
			},
			remark :{
			    maxlength : 100
			}
		},
		submitHandler : function(form) {
			var dataParam = $('#billBaseInfoForm').serializeArray();
			var dataInfo = {};
			$.each(dataParam,function(i,d){
				dataInfo[d.name] = d.value;
			});
			dataInfo.billAmount = parseInt(dataInfo.billAmount*100);
			//dataInfo.orderAmount = dataInfo.orderAmount*100;
			//dataInfo.adjustAmount = dataInfo.adjustAmount*100;
			//dataInfo.invoiceAmount = dataInfo.invoiceAmount*100;
			dataInfo.remainAmount = parseInt(dataInfo.remainAmount * 100);
			
			var cDate = dataInfo.createDate.split("-");
			dataInfo.createDate = new Date(cDate[0],cDate[1]-1,cDate[2]);
			
			var liqDateBegin = dataInfo.liqDateBegin.split("-");
			dataInfo.liqDateBegin = new Date(liqDateBegin[0],liqDateBegin[1]-1,liqDateBegin[2]);
			var liqDateEnd = dataInfo.liqDateEnd.split("-");
			dataInfo.liqDateEnd = new Date(liqDateEnd[0],liqDateEnd[1]-1,liqDateEnd[2]);
			
			var oDate = dataInfo.overdueDate.split("-");
			dataInfo.overdueDate = new Date(oDate[0],oDate[1]-1,oDate[2]);
			dataInfo.saveMode = $("#saveMode").val();
			$.post("bpAccount/settlement/saveOrUpdateBill",dataInfo,
				function(data){
					if (data.type == 'success') {
						showSuccessMsg("保存成功！");
						window.location.href="/bpAccount/settlement/billDetail?id="+data.txt;
					}else{
						showErrorMsg(data.txt);
					}
				}
			);
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

function saveBillBaseInfo(saveMode){
	$("#saveMode").val(saveMode);
	$("#billBaseInfoForm").submit();
	
}

function userSubsidy(cur){
	if(parseFloat($(cur).val()) > parseFloat($("#subsidyTotal").val())){
		$.alert("金额不能大于能使用的补贴",'提示');
		$(cur).val($("input[name='adjustAmountBefore']").val());
		return;
	}
	if(parseFloat($(cur).val()) > parseFloat($("input[name='billAmount']").val())){
		$.alert("金额不能大于账单金额",'提示');
		$(cur).val($("input[name='adjustAmountBefore']").val());
		return;
	}
	
	//调整  账单金额、调整金额 
	$("input[name='adjustAmountBefore']").val(parseFloat($(cur).val()));
	$("input[name='adjustAmount']").val(parseFloat($(cur).val()) * -1);
	$("input[name='billAmount']").val(parseFloat($("input[name='orderAmount']").val())-parseFloat($(cur).val()));
}

function adjustBillAmount(billId){
	$('#modelAdjustBillAmount').modal();
}

function selectCategory(cur){
	/* var category = $(cur).val();
	if(category == "SUBSIDY"){
		$("#canUseSubsidyDiv").show();
	}else{
		$("#canUseSubsidyDiv").hide();
	} */
}

function saveBillAdjitem(){
	var dataParam = $('#adjustBillAmountForm').serializeArray();
	var dataInfo = {};
	$.each(dataParam,function(i,d){
		dataInfo[d.name] = d.value;
	});
	dataInfo.amount = parseInt(dataInfo.amount*100);
	
	if(dataInfo.category == 'SUBSIDY') {
		var amount = dataInfo.amount;
		if(amount > 0) {
			alert('补贴金额调整应为负数');
			return;
		}
		var remainSubsidy = 0;
		if('${canUseSubsidy}') {
			remainSubsidy = parseInt('${canUseSubsidy}');
		}
		if(remainSubsidy <= 0) {
			alert('当前账户无可用补贴金额');
			return;
		}
		if(amount + remainSubsidy < 0) {
			alert('调整金额不能大于可用补贴金额');
			return;
		}
	}
	$.post("bpAccount/settlement/saveOrUpdateBillAdjitem",dataInfo,
		function(data){
			if (data.type == 'success') {
				window.location.href="/bpAccount/settlement/billDetail?id="+$('#adjustBillAmountBillId').val()+"&linkFrom="+$("#linkFromInput").val();
			}else{
				showErrorMsg(data.txt);
			}
		}
	);
}

function sendBill(id, amount){

    var message = "确认发送此账单?发送后客户将能看见此账单！";
    if(amount < 0){
        message = "当前账单金额小于0，账单发送后客户将可看到账单，是否发送？";
	}else if(amount === '0'){
        message = "当前账单金额等于0，账单发送后客户将可看到账单，是否发送？";
	}

	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : message,
		confirm : function() {
			var data = {
				billId: id
			};
			$.post("bpAccount/settlement/sendBill", data,
				function(data){
					if (data.type == 'success') {
						showSuccessMsg("保存成功！");
						window.location.href="/bpAccount/settlement/billDetail?id="+id;
					}else{
						showErrorMsg(data.txt);
					}
				}
			);
		}
	});
}

function confirmBill(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认此账单?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'bill/manage/confirmBill?id='+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						window.location.href="/bpAccount/settlement/billDetail?id="+id+"&linkFrom=billList";
						showSuccessMsg(msg.txt);
					}
				}
			});
		}
	});
}

function cancelBill(id) {

    $.confirm({
        title : '提示',
        confirmButton:'确认',
        cancelButton:'取消',
        content : '作废此账单?',
        confirm : function() {
            $.ajax({
                type:'GET',
                url:'/bill/manage/cancelBill?id='+id,
                success:function(data){
                    if(data.code == 1){
                        showErrorMsg(data.msg);
                    }else{
                        window.location.href="/bpAccount/settlement/billDetail?id="+id+"&linkFrom=billList";
                        showSuccessMsg(data.msg);
                    }
                }
            });
        }
    });
}

</script> 