<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>

</style>

<div class="modal fade" id="modelOrderList" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
   <div class="modal-dialog" style="width: 1000px;">
      <div class="modal-content" style="width: 1000px;">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">添加订单</h4>
         </div>
         <div class="modal-body">
			<div class="row" style="margin-bottom: 5px;">
				<div class="col-md-3" style="margin-top: 10px;">
			              	订单列表&nbsp;&nbsp; 共计<span><font id="orderListTotalPage">${pageList.totalPage }</font>页,<font id="orderListTotalNum">${pageList.total }</font></span>条数据
				</div>
				<div class="col-md-9">
					<form class="form-inline" id="queryBillOrderListform1">
						<nav class="text-right">
							<div class=" form-group" style="margin-right: 0px;">
								<input type="hidden" id="pageIndex1" name="pageIndex" />
								<input type="text" name="queryStartDate" onclick="WdatePicker()" style="width: 120px" class="form-control" value="${queryStartDate }" placeholder="起始时间">
								<input type="text" name="queryEndDate" onclick="WdatePicker()" style="width: 120px" class="form-control" value="${queryEndDate }" placeholder="结束时间">
								<input name="orderNo" type="text" class="form-control" style="width: 160px" placeholder="订单编号" />
								<select class="form-control" id='bizType1' name="bizType" style="width: 120px">
									<option value="">全部类型</option>
									<option value="FLIGHT">国内机票</option>
									<option value="INTERNATIONAL_FLIGHT">国际机票</option>
									<option value="HOTEL">酒店</option>
									<option value="TRAIN">火车票</option>
									<option value="CAR">用车</option>
									<option value="GENERAL">通用订单</option>
								</select>
							</div>
							<div class="form-group" style="margin-left: 0px;margin-right: 0px;">
								<button type="button" class="buttonMain btn-default" style="background: #d9e0e0;"
									onclick="loadBillOrderTable()">查询</button>
								<!-- <button type="button" class="btn btn-default" id="addAllOrderButton" title="添加满足查询条件的所有订单"
									onclick="addAllOrder()">添加所有订单</button> -->
							</div>
						</nav>
					</form>
				</div>
			</div>
			<div id="orderListModel"></div>		
         </div>
         <div class="modal-footer">
            <button type="button" class="btn-default buttonMain" style="background: #d9e0e0;" data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn-primary buttonMain" id="sureButton" onclick="sureSelects()">添加所选订单
            </button>
            <button type="button" class="btn-primary buttonMain" id="addAllOrderButton" onclick="addAllOrder()">添加符合条件的所有订单
            </button>
         </div>
      </div>
</div>


<script type="text/javascript">
 $(document).ready(function() {
	 loadBillOrderTable();
 });
 
 function selectAll1(){
	if ($("#selectAll").is(":checked")) {
        $("#billOrderTable input:checkbox").prop("checked", true);//所有选择框都选中
    } else {
        $("#billOrderTable input:checkbox").prop("checked", false);
    }
 }
 
 function sureSelects(){
	var checkedNum = $("#billOrderTable input[name='orderNos']:checked").length;
    if(checkedNum==0){
       $.alert("请选择你要添加的订单!","提示");
       return false;
    }
	  
  	var orderNos = new Array();
  	$("#billOrderTable input[name='orderNos']:checked").each(function(){
  		orderNos.push($(this).val());
  	});
  	var billId = $('#billIdOrderList').val();
  	 
  	var tabIndex = 'orderList';
  	
  	$('#sureButton').attr("disabled", true);
	setTimeout(function(){
		$('#sureButton').removeAttr("disabled");
	}, 3000);
	
	var queryStartDate = $("input[name='queryStartDate']").val();
	var queryEndDate = $("input[name='queryEndDate']").val();
	var loading = layer.load();
  	 
  	$.ajax({
		type:'GET',
		url:'bpAccount/settlement/addOrders',
		data:{
			billId: billId,
			queryStartDate:queryStartDate,
			queryEndDate:queryEndDate,
			orderNos: orderNos.join(',')
		},
		success:function(msg){
			layer.close(loading);
			if(msg.type == 'error'){
				showErrorMsg(msg.txt);
			}else{
				$('#modelOrderList').modal('hide');
				var linkFromInput = $("#linkFromInput").val();
				var url = "bpAccount/settlement/billDetail?id="+billId+"&tabIndex="+tabIndex;
				if(linkFromInput){
					url += "&linkFrom="+linkFromInput;
				}
				window.location.href=url;
				showSuccessMsg('操作成功');
			}
		}
	});
 }
 
 function loadBillOrderTable(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("#pageIndex1").val(pageIndex);
	var dataParam = $('#queryBillOrderListform1').serialize();
	var billId = $('#billIdOrderList').val();
	dataParam = dataParam + "&billId="+billId;
	var loading = layer.load();
	$('#orderListModel').load("bpAccount/settlement/loadBillOrderTable",dataParam, function(context, state) {
		layer.close(loading);
	});
}
 
 //添加满足查询条件的所有订单
 function addAllOrder(){
	 var num = $('#orderListTotalNum').html();
	 var dataParam = $('#queryBillOrderListform1').serialize();
	 var billId = $('#billIdOrderList').val();
	 dataParam = dataParam + "&billId=" + $('#billId').val();
	 $.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认添加全部订单('+num+'条)?',
		confirm : function() {
			$('#addAllOrderButton').attr("disabled", true);
			setTimeout(function(){
				$('#addAllOrderButton').removeAttr("disabled");
			}, 3000);
			var loading = layer.load();
			$.ajax({
				type:'GET',
				url:'bpAccount/settlement/addAllOrder',
				data:dataParam,
				success:function(msg){
					layer.close(loading);
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						$('#modelOrderList').modal('hide');
						var linkFromInput = $("#linkFromInput").val();
						var url = "bpAccount/settlement/billDetail?id="+billId+"&tabIndex="+tabIndex;
						if(linkFromInput){
							url += "&linkFrom="+linkFromInput;
						}
						window.location.href=url;
						showSuccessMsg('操作成功');
					}
				}
			});
		}
	});
 }
 </script>