<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">

<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>账户概览
             	 &nbsp;&nbsp;<a data-pjax href="javascript:window.history.back()" >返回</a>
             </div>
             <input type="hidden" id="bpAccountId" value="${bpAccount.id }"/>
             <div class="panel-body">
             	<p style="font-size: 18px;font-weight:bold;">${bpAccount.accountName } &nbsp;商旅账户 </p>
             	<p>统计时间：<fmt:formatDate value="${nowDate }" pattern="yyyy-MM-dd HH:ss"/></p>
             	<br/>
             	<span>
	             	<c:if test="${bpAccount.balanceAmount >= 0 }">账户现有资金：<fmt:formatNumber value="${bpAccount.balanceAmount*0.01}" pattern="#,##0.##"/></c:if>
	             	<font style="color: red;"><c:if test="${bpAccount.balanceAmount < 0 }">系统代垫资金：<fmt:formatNumber value="${bpAccount.balanceAmount*-1*0.01 }" pattern="#,##0.##"/></c:if></font>
             	</span>
             	<span style="margin-left: 150px">
             		授信额度： <fmt:formatNumber value="${bpAccount.creditAmount*0.01 }" pattern="#,##0.##"/>
             	</span>
             	<span style="margin-left: 150px">
             		<font style="font-weight: bold;">剩余可用额度：<fmt:formatNumber value="${(bpAccount.creditAmount + bpAccount.balanceAmount)*0.01}" pattern="#,##0.##"/></font>
             	</span>
             </div>
             
             <ul id="myTab" class="nav nav-tabs" data-trigger="tab">
				<li><a onclick="queryBill('${bpAccount.id}')">未清账账单</a></li>
				<li><a onclick="queryAccountFlow('${bpAccount.id}')">账户流水查询</a></li>
			 </ul>
			 <div class="pane-wrapper">
			 	<div id="billInfo"></div>
			 	<div id="accountFlow"></div>
			 </div>
         </div>
	</div>
</div>


<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tab.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tooltip.js?version=${globalVersion}"></script>
<script type="text/javascript">

$(document).ready(function(){
	//加载账单信息
	var accountId = $('#bpAccountId').val();
	queryBill(accountId);
});

function queryBill(id){
	$('#billInfo').load("bpAccount/settlement/billList?id="+id, function(context, state) {
		
	});
}
	
function queryAccountFlow(id){
	$('#accountFlow').load("bpAccount/settlement/accountFlow?id="+id, function(context, state) {
		
	});
}
</script> 