<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<table id="billOrderTable" class="table">
	<thead>
		<tr>
			<th><input type="checkbox" onchange="selectAll1()" id="selectAll"/></th>
			<th class="sort-column" style="min-width: 95px;text-align: left;">业务类型</th>
			<th class="sort-column" style="width: 140px;text-align: center;">订单号</th>
			<th class="sort-column" style="width: 150px;text-align: center;">下单信息</th>
			<th class="sort-column" style="min-width: 70px;text-align: center;">费用归属</th>
			<th class="sort-column" style="min-width: 150px;text-align: center;">金额信息</th>
			<%--<th class="sort-column" style="min-width: 80px;text-align: center;">支付方式</th>--%>
			<th class="sort-column" style="min-width: 290px;max-width: 330px;text-align: center;">订单摘要</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pageList.list}" var="totalOrderForm">
			<c:if test="${not empty totalOrderForm.orderForm}">
				<tr>
					<td style="vertical-align:middle;" rowspan="${totalOrderForm.size }"><input type="checkbox" name="orderNos" value="${totalOrderForm.orderForm.orderNo }"/></td>
					<td style="text-align: left;">${totalOrderForm.orderForm.orderTypeName}</td>
					<td>${totalOrderForm.orderForm.orderNo}</td>
					<td><fmt:formatDate value="${totalOrderForm.orderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/>&nbsp;&nbsp;${totalOrderForm.orderForm.applyUserName}</td>
					<td>${totalOrderForm.orderForm.costCenterName}</td>
					<td><fmt:formatNumber value="${totalOrderForm.orderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br>${paymentMethodMap[totalOrderForm.orderForm.paymentMethod]}</td>
					<td style="font-size: 12px;">${totalOrderForm.orderForm.desc}</td>
				</tr>
			</c:if>
			<c:forEach items="${totalOrderForm.insureOrderForms}" var="insureOrderForm">
				<tr>
					<td style="border-top: none;text-align: left;">${insureOrderForm.orderTypeName}</td>
					<td style="border-top: none;">${insureOrderForm.orderNo}</td>
					<td style="border-top: none;"><fmt:formatDate value="${insureOrderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/>&nbsp;&nbsp;${insureOrderForm.applyUserName}</td>
					<td style="border-top: none;">${insureOrderForm.costCenterName}</td>
					<td style="border-top: none;"><fmt:formatNumber value="${insureOrderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br>${paymentMethodMap[insureOrderForm.paymentMethod]}</td>
					<td style="border-top: none;font-size: 12px;">${insureOrderForm.desc}</td>
				</tr>
			</c:forEach>
			<c:forEach items="${totalOrderForm.refundOrderForms}" var="refundOrderForm" varStatus="status">
				<c:if test="${status.index == 0 and empty totalOrderForm.orderForm}">
					<tr>
						<td style="vertical-align:middle;" rowspan="${totalOrderForm.size }"><input type="checkbox" name="orderNos" value="${refundOrderForm.orderNo }"/></td>
						<td style="text-align: left;">${refundOrderForm.orderTypeName}</td>
						<td style="">${refundOrderForm.orderNo}</td>
						<td style=""><fmt:formatDate value="${refundOrderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/>&nbsp;&nbsp;${refundOrderForm.applyUserName}</td>
						<td style="">${refundOrderForm.costCenterName}</td>
						<td style=""><fmt:formatNumber value="${refundOrderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br>${paymentMethodMap[refundOrderForm.paymentMethod]}</td>
						<td style="font-size: 12px;">${refundOrderForm.desc}</td>
					</tr>
				</c:if>
				<c:if test="${not empty totalOrderForm.orderForm or status.index != 0}">
					<tr>
						<td style="border-top: none;text-align: left;">${refundOrderForm.orderTypeName}</td>
						<td style="border-top: none;">${refundOrderForm.orderNo}</td>
						<td style="border-top: none;"><fmt:formatDate value="${refundOrderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/>&nbsp;&nbsp;${refundOrderForm.applyUserName}</td>
						<td style="border-top: none;"></td>
						<td style="border-top: none;"><fmt:formatNumber value="${refundOrderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br></td>
						<td style="border-top: none;font-size: 12px;">${refundOrderForm.desc}</td>
					</tr>
				</c:if>
			</c:forEach>
			<c:forEach items="${totalOrderForm.refundInsureOrderForms}" var="refundInsureOrderForm">
				<tr>
					<td style="border-top: none;text-align: left;">${refundInsureOrderForm.orderTypeName}</td>
					<td style="border-top: none;">${refundInsureOrderForm.orderNo}</td>
					<td style="border-top: none;"><fmt:formatDate value="${refundInsureOrderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/>&nbsp;&nbsp;${refundInsureOrderForm.applyUserName}</td>
					<td style="border-top: none;">${refundInsureOrderForm.costCenterName}</td>
					<td style="border-top: none;"><fmt:formatNumber value="${refundInsureOrderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br>${paymentMethodMap[refundInsureOrderForm.paymentMethod]}</td>
					<td style="border-top: none;font-size: 12px;">${refundInsureOrderForm.desc}</td>
				</tr>
			</c:forEach>
		</c:forEach>
	</tbody>
</table>
<div class="pagin">
	<jsp:include page="../../common/pagination_ajax.jsp?callback=loadBillOrderTable"></jsp:include>
</div>


<script type="text/javascript">
$(document).ready(function() {
	 var totalPage = '${pageList.totalPage }';
	 var total = '${pageList.total }';
	 $('#orderListTotalPage').html(totalPage);
	 $('#orderListTotalNum').html(total);
});
</script>