<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<style>
.button {
    display: inline-block;
    background-color: #1176cc;
    color: white;
    width: 90px;
    text-align: center;
    margin-left: 0;
    border-radius:2px;
    cursor: pointer;
    margin-left: 30px;
}

.button:hover{
    background-color: #0063a0;
}
</style>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<table class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th class="sort-column" style="min-width: 80px;text-align: left;">结算期间</th>
						<th class="sort-column" style="min-width: 100px;text-align: center;">账单金额</th>
						<th class="sort-column" style="max-width: 100px;text-align: center;">其中-订单金额</th>
						<th class="sort-column" style="max-width: 100px;text-align: center;">其中-调整金额</th>
						<th class="sort-column" style="max-width: 100px;text-align: center;">未清账金额</th>
						<th class="sort-column" style="max-width: 100px;text-align: center;">状态</th>
						<th class="sort-column" style="min-width: 80px;;text-align: center;">创建人</th>
						<th class="sort-column" style="min-width: 80px;;text-align: center;">创建时间</th>
						<th style="min-width: 130px">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${bills}" var="bill">
						<tr>
							<td><fmt:formatDate value="${bill.liqDateBegin}" pattern="yyyy/MM/dd"/> - <fmt:formatDate value="${bill.liqDateEnd}" pattern="yyyy/MM/dd"/></td>
							<td><fmt:formatNumber value="${bill.billAmount*0.01}" pattern="#,##0.##"/></td>
							<td><fmt:formatNumber value="${bill.orderAmount*0.01}" pattern="#,##0.##"/></td>
							<td><fmt:formatNumber value="${bill.adjustAmount*0.01}" pattern="#,##0.##"/></td>
							<td><fmt:formatNumber value="${bill.remainAmount*0.01}" pattern="#,##0.##"/></td>
							<td><c:if test="${bill.status eq 'NOT_CONFIRM'}">待确认</c:if>
								<c:if test="${bill.status eq 'CONFIRM'}">已确认</c:if>
								<c:if test="${bill.status eq 'CANCEL'}">已废弃</c:if>
								<c:if test="${bill.status eq 'DRAFT'}">草稿</c:if>
							</td>
							<td>${bill.createUserName}</td>
							<td><fmt:formatDate value="${bill.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td>
								<a href="javascript:void(0)" onclick="billDetail(${bill.id})">查看明细</a>
							</td>
						</tr>
					</c:forEach>
					<tr>
						<td id="needSettleAmountTd" style="background-color: #DDDDDD;height: 35px;vertical-align: middle;line-height: 35px;" colspan="9">
							待结算订单：
							<%-- 待结算订单：<fmt:formatNumber value="${totalPrice}" pattern="#,##0.##"/>元
							<c:if test="${totalPrice > 0}">
								<span onclick="createBill('${accountId}')" class="button">创建账单</span>
							</c:if> --%>
						</td>
					</tr>
				</tbody>
			</table>
			
		</div>
	</div>
</div>

<script type="text/javascript" >
$(document).ready(function(){
	//得到待结算金额
	$.ajax({
		type:'GET',
		url:'bpAccount/settlement/getSettleAmount?id='+$("#bpAccountId").val(),
		success:function(data){
			var html = data + "元";
			html += "<span onclick='createBill("+$("#bpAccountId").val()+")' class='button'>创建账单</span>";
			$("#needSettleAmountTd").append(html);
		}
	});
	
});

function billDetail(id){
	window.location.href = "/bpAccount/settlement/billDetail?id="+id;
}

function createBill(accountId){
	window.location.href = "/bpAccount/settlement/billDetail?accountId="+accountId;
}

</script>