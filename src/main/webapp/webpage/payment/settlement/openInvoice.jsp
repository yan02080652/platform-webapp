<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
.inputNewInfo{
	width: 240px;
	padding-left: 0px;
}
</style>

<div class="modal fade" id="modelOpenInvoice" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
   <div class="modal-dialog" style="width: 750px;">
      <div class="modal-content" style="width: 750px;">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">开票</h4>
         </div>
         <div class="modal-body">
			<div class="row">
				<form modelAttribute="invoiceDto" class="form-horizontal" id="invoiceDetailForm" role="form">
					 <input name="accountId" id="accountId" type="hidden" value="${billForm.accountId }"/>
					 <input name="tmcId" type="hidden" value="${billForm.tmcId}"/>
					 <input name="bpId" type="hidden" value="${billForm.bpId}"/>
					 <input name="billId" type="hidden" id='invoiceDetailBillId' value="${billForm.id}"/>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">企业名称:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="bpName" readonly="readonly" class="form-control" value="${billForm.bpName }">
					    </div>
					    <label class="col-sm-2 control-label nomalFond">账户名称:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="accountName" readonly="readonly" class="form-control" value="${billForm.accountName }">
					    </div>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">账单金额:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" id="invoiceBillAmount" readonly="readonly" class="form-control" value="${billForm.billAmount*0.01 }">
					    </div>
					    <label class="col-sm-2 control-label nomalFond">未开票金额:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <c:if test="${(billForm.billAmount-billForm.invoiceAmount)*0.01 >=0}">
					      	<input type="text" readonly="readonly" id="invoiceBillAmountUnOpen" class="form-control" value="${(billForm.billAmount-billForm.invoiceAmount)*0.01 }">
					      </c:if>
					      <c:if test="${(billForm.billAmount-billForm.invoiceAmount)*0.01 <0}">
					      	<input type="text" readonly="readonly" id="invoiceBillAmountUnOpen" class="form-control" value="0.0">
					      </c:if>
					    </div>
					 </div>
					 <hr>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label nomalFond">发票类型:</label>
					    <div class="col-sm-4 inputNewInfo">
						      <select class="form-control" readonly="readonly" name="invoiceType">
						      	 <c:forEach items="${invoiceTypes}" var="invoiceType">
						      		<option value="${invoiceType.itemCode }">${invoiceType.itemTxt }</option>
						      	 </c:forEach>
							  </select>
					    </div>
					    <label class="col-sm-2 control-label nomalFond">开票方:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="invoiceSource" class="form-control" value="">
					    </div>
					 </div>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label nomalFond"> 开票金额:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="number" onchange="changeOrderAmount(this)"  id="invoiceOpenAmount" name="amount" class="form-control" >
					    </div>
					    <label class="col-sm-2 control-label nomalFond"> 受票方:</label>
					    <div class="col-sm-4 inputNewInfo">
					    	<input type="hidden" name="invoiceDestCorpid" value="${corpAccount }">
					    	<input type="text" id="myDropDown" name="invoiceDest" value="${corpAccountName }" class="form-control dropdown-common myDropDownInput" style="width: 191px;float: left;"> 
					    	<span class="input-group-btn" style="width: 50px;">
								<button class="btn btn-default" type="button" onclick="selectCorp(this)"> <i class="fa fa-caret-down" aria-hidden="true"></i></button>
							</span>
					    </div>
					 </div>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label nomalFond"> 税费:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="number" onchange="getTotalAmount()" id="invoiceTax" name="tax" class="form-control" >
					    </div>
					    <label class="col-sm-2 control-label nomalFond">发票号码:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text"  name="invoiceNo" class="form-control" >
					    </div>
					    
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond"> 发票总金额:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="number" readonly="readonly" id="invoiceTotalAmount" name="totalAmount" class="form-control" >
					    </div>
					    <label class="col-sm-2 control-label nomalFond">物流状态:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <select class="form-control" name="shippingStatus">
					      	 <option value="NOTSHIP">未寄送</option>
					      	 <option value="SHIPED">已寄送</option>
						  </select>
					    </div>
					 </div>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label nomalFond">开票人:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" class="form-control" readonly="readonly" value='${curUserName }'>
					    </div>
					    <label class="col-sm-2 control-label nomalFond">物流单号:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="shippingInfo" class="form-control">
					    </div>
					    
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">开票日期:</label>
					    <div class="col-sm-4 inputNewInfo">
					      <input type="text" name="invoiceDate" class="form-control" onclick="WdatePicker()">
					    </div>
						 <%--<c:if test="${invoiceDto.shippingStatus eq 'SHIPED'}">--%>
							 <label class="col-sm-2 control-label nomalFond">寄送日期:</label>
							 <div class="col-sm-4 inputNewInfo">
								 <input type="text" name="shippingDate" class="form-control" onclick="WdatePicker()">
							 </div>
						 <%--</c:if>--%>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label nomalFond">备注:</label>
					    <div class="col-sm-10" style="padding-left: 0px;width: 611px">
					      <input type="text"  name="remark" class="form-control" >
					    </div>
					 </div>
				</form>
			</div>		
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="saveInvoice()">保存
            </button>
         </div>
      </div>
	</div>
</div>


<script type="text/javascript">
$(document).ready(function() {
	initInvoiceFormField();
});

function saveInvoice(){
	$('#invoiceDetailForm').submit();
}

function initInvoiceFormField() {
	$("#invoiceDetailForm").validate({
		rules:{
			invoiceSource : {
				required : true
			},
			invoiceDest : {
				required : true
			},
			invoiceNo : {
				required : true
			},
			amount : {
				required : true
			},
			tax : {
				required : true
			},
			invoiceDate : {
				required : true
			},
			totalAmount : {
				required : true
			}
		},
		submitHandler : function(form) {
			var billId = $('#invoiceDetailBillId').val();
			var dataParam = $('#invoiceDetailForm').serializeArray();
			var 	dataInfo = {};
			$.each(dataParam,function(i,d){
				dataInfo[d.name] = d.value;
			});
			dataInfo.totalAmount = dataInfo.totalAmount*100;
			dataInfo.orderAmount = dataInfo.amount*100;
			dataInfo.tax = dataInfo.tax*100;
			dataInfo.amount = dataInfo.amount*100;
			
			var cDate = dataInfo.invoiceDate.split("-");
			dataInfo.invoiceDate = new Date(cDate[0],cDate[1]-1,cDate[2]);

            var sDate = dataInfo.shippingDate.split("-");

            if(sDate[0]){
                dataInfo.shippingDate = new Date(sDate[0],sDate[1]-1,sDate[2]);
			}else{
                dataInfo.shippingDate = new Date('1970','00','01');
			}

			$.post("bpAccount/settlement/saveInvoice",dataInfo,
				function(data){
					if (data.type == 'success') {
						window.location.href="/bpAccount/settlement/billDetail?id="+billId;
					}else{
						showErrorMsg(data.txt);
					}
				}
			);
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}

function selectCorp(dom){
	/* var data = [{
		code:1,
		name:'自定义选择1'
	},{
		code:2,
		name:'自定义选择2'
	},{
		code:3,
		name:'自定义选择3'
	},{
		code:4,
		name:'自定义选择4'
	}]; */
	var billId = $('#invoiceDetailBillId').val();
	$.getJSON('bpAccount/settlement/getAllCorps?billId='+billId,function(data){
		TR.select(data,{
			key:'custom1',//多个自定义下拉选择时key值不能重复
			dom:dom,
			name:'invoiceTitle',//default:name
			allowBlank:true//是否允许空白选项
		},function(result){
			$('#myDropDown').val(result?result.invoiceTitle:null);
			$("input[name='invoiceDestCorpid']").val(result?result.id:null);
		});
	});
	
}

function changeAmount(dom){
	var invoiceBillAmountUnOpen = parseFloat($('#invoiceBillAmountUnOpen').val());
	var amount = parseFloat($(dom).val());
	
	if(amount > invoiceBillAmountUnOpen){
		$(dom).val(0);
		$.alert('开票金额不能大于账单未开票金额','提示');
	}else{
		getTotalAmount();
	}
}

function getTotalAmount(){
	var invoiceTax = $('#invoiceTax').val();
	invoiceTax =  parseFloat(invoiceTax ? invoiceTax : 0);
	var invoiceOpenAmount = $('#invoiceOpenAmount').val();
	invoiceOpenAmount =  parseFloat(invoiceOpenAmount ? invoiceOpenAmount : 0);
	$('#invoiceTotalAmount').val(invoiceOpenAmount + invoiceTax);
}

function changeOrderAmount(dom){
	var invoiceBillAmountUnOpen = parseFloat($('#invoiceBillAmountUnOpen').val());
	var amount = parseFloat($(dom).val());
	
	if(amount > invoiceBillAmountUnOpen){
		$(dom).val(0);
		$.alert('开票金额不能大于账单未开票金额','提示');
	}
}


</script>