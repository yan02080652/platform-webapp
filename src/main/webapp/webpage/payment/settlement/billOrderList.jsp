<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<style>
</style>

<div class="row">
	<div class="col-md-4" style="margin-top: 10px;padding-left: 25px;">
              	订单列表&nbsp;&nbsp; 共计<span>${pageList.totalPage }页,${pageList.total }</span>条数据
	</div>
	<div class="col-md-8">
		<form class="form-inline" id="queryBillOrderListform">
			<nav class="text-right">
				<div class=" form-group" style="margin-right: 0px;">
					<input type="hidden" id="pageIndex" name="pageIndex" />
					<input type="hidden" id="billIdOrderList" value="${billId }" />
					<input name="orderNo" type="text" value="${orderNo }" class="form-control" placeholder="订单编号" />
					<select class="form-control" id='bizType' name="bizType" style="width: 150px">
						<option value="">全部类型</option>
						<option value="FLIGHT">国内机票</option>
						<option value="INTERNATIONAL_FLIGHT">国际机票</option>
						<option value="HOTEL">酒店</option>
						<option value="TRAIN">火车票</option>
						<option value="CAR">用车</option>
						<option value="GENERAL">通用订单</option>
					</select>
				</div>
				<div class="form-group" style="margin-left: 0px;margin-right: 0px;">
					<button type="button" class="buttonMain btn-default" style="background: #d9e0e0;"
						onclick="loadBillOrderList()">查询</button>
					<c:if test="${liquidationStatus ne 'CLOSE' && billStatus ne 'CONFIRM' && billStatus ne 'CANCEL'}">
						<button type="button" class="buttonMain btn-default" style="background: #d9e0e0;"
						onclick="addBillOrder()">添加订单</button>
					</c:if>
					<c:if test="${liquidationStatus ne 'CLOSE' && billStatus ne 'CONFIRM' && billStatus ne 'CANCEL'}">
						<button type="button" class="buttonMain btn-default" style="background: #d9e0e0;"
								onclick="batchRemove()" >批量移除</button>
					</c:if>
				</div>
			</nav>
		</form>
	</div>
</div>
		
<table class="table" style="margin-left: 10px;">
	<thead>
		<tr>
			<th><input type="checkbox" onchange="selectAllOrderList1()" id="selectAllOrderList"/></th>
			<th class="sort-column" style="min-width: 110px;text-align: left;">业务类型</th>
			<th class="sort-column" style="width: 140px;text-align: center;">订单号</th>
			<%--<th class="sort-column" style="width: 150px;text-align: center;">下单时间</th>--%>
			<%--<th class="sort-column" style="min-width: 70px;text-align: center;">预订人</th>--%>
			<th class="sort-column" style="width: 150px;text-align: center;">下单信息</th>
			<th class="sort-column" style="min-width: 100px;text-align: center;">费用归属</th>
			<%--<th class="sort-column" style="min-width: 80px;text-align: center;">金额</th>--%>
			<%--<th class="sort-column" style="min-width: 80px;text-align: center;">支付方式</th>--%>
			<th class="sort-column" style="min-width: 100px;text-align: center;">金额信息</th>
			<th class="sort-column" style="min-width: 290px;max-width: 350px;text-align: center;">摘要</th>
			<th class="sort-column" style="width: 90px;text-align: center;">操作</th>
		</tr>
	</thead>
	<tbody class="selectAllOrderList">
		<c:forEach items="${pageList.list}" var="totalOrderForm">
			<c:if test="${not empty totalOrderForm.orderForm}">
				<tr>
					<td style="vertical-align:middle;" rowspan="${totalOrderForm.size }"><input type="checkbox" name="orderNos" value="${totalOrderForm.orderForm.orderNo }"/></td>
					<td style="text-align: left;">${totalOrderForm.orderForm.orderTypeName}</td>
					<td><a href="javascript:void(0)" style="color: #337ab7;" onclick="orderDetail('${totalOrderForm.orderForm.orderNo}','${totalOrderForm.orderForm.orderType }')">${totalOrderForm.orderForm.orderNo}</a></td>
					<td><fmt:formatDate value="${totalOrderForm.orderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/>&nbsp;&nbsp;${totalOrderForm.orderForm.applyUserName}</td>
					<%--<td>${totalOrderForm.orderForm.applyUserName}</td>--%>
					<td>${totalOrderForm.orderForm.costCenterName}</td>
					<td><fmt:formatNumber value="${totalOrderForm.orderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br>${paymentMethodMap[totalOrderForm.orderForm.paymentMethod]}</td>
					<%--<td>${paymentMethodMap[totalOrderForm.orderForm.paymentMethod]}</td>--%>
					<td style="font-size: 12px;">${totalOrderForm.orderForm.desc}</td>
					<td style="vertical-align:middle;" rowspan="${totalOrderForm.size }">
						<c:if test="${liquidationStatus ne 'CLOSE' && billStatus ne 'CONFIRM'}">
							<a href="javascript:void(0)" style="color: #337ab7;" onclick="removeOrder('${totalOrderForm.orderForm.orderNo}')">移除</a>&nbsp;&nbsp;
						</c:if>
					</td>
				</tr>
			</c:if>
			<c:forEach items="${totalOrderForm.insureOrderForms}" var="insureOrderForm">
				<tr>
					<td style="border-top: none;text-align: left;">${insureOrderForm.orderTypeName}</td>
					<td style="border-top: none;"><a href="javascript:void(0)" style="color: #337ab7;" onclick="orderInsuranceDetail('${insureOrderForm.orderNo}')">${insureOrderForm.orderNo}</a></td>
					<td style="border-top: none;"><fmt:formatDate value="${insureOrderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/> &nbsp;&nbsp;${insureOrderForm.applyUserName}</td>
					<td>${insureOrderForm.costCenterName}</td>
					<td style="border-top: none;"><fmt:formatNumber value="${insureOrderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br>${paymentMethodMap[insureOrderForm.paymentMethod]}</td>
					<td style="border-top: none;font-size: 12px;">${insureOrderForm.desc}</td>
				</tr>
			</c:forEach>
			<c:forEach items="${totalOrderForm.refundOrderForms}" var="refundOrderForm" varStatus="status" >
				<c:if test="${status.index == 0 and empty totalOrderForm.orderForm}">
					<tr>
						<td style="vertical-align:middle;" rowspan="${totalOrderForm.size }"><input type="checkbox" name="orderNos" value="${refundOrderForm.orderNo }"/></td>
						<td style="text-align: left;">${refundOrderForm.orderTypeName}</td>
						<td style=""><a href="javascript:void(0)" style="color: #337ab7;" onclick="orderRefundDetail('${refundOrderForm.orderNo}')">${refundOrderForm.orderNo}</a></td>
						<td style=""><fmt:formatDate value="${refundOrderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/>&nbsp;&nbsp;${refundOrderForm.applyUserName}</td>
						<td style="">${refundOrderForm.costCenterName}</td>
						<td style=""><fmt:formatNumber value="${refundOrderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br>${paymentMethodMap[refundOrderForm.paymentMethod]}</td>
						<td style="font-size: 12px;">${refundOrderForm.desc}</td>
						<td style="vertical-align:middle;" rowspan="${totalOrderForm.size }">
							<c:if test="${liquidationStatus ne 'CLOSE' && billStatus ne 'CONFIRM'}">
								<a href="javascript:void(0)" style="color: #337ab7;" onclick="removeOrder('${refundOrderForm.orderNo}')">移除</a>&nbsp;&nbsp;
							</c:if>
						</td>
					</tr>
				</c:if>
				<c:if test="${not empty totalOrderForm.orderForm or status.index != 0}">
					<tr>
						<td style="border-top: none;text-align: left;">${refundOrderForm.orderTypeName}</td>
						<td style="border-top: none;"><a href="javascript:void(0)" style="color: #337ab7;" onclick="orderRefundDetail('${refundOrderForm.orderNo}')">${refundOrderForm.orderNo}</a></td>
						<td style="border-top: none;"><fmt:formatDate value="${refundOrderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/>&nbsp;&nbsp;${refundOrderForm.applyUserName}</td>
						<td style="border-top: none;">${refundOrderForm.costCenterName}</td>
						<td style="border-top: none;"><fmt:formatNumber value="${refundOrderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br>${paymentMethodMap[refundOrderForm.paymentMethod]}</td>
						<td style="border-top: none;font-size: 12px;">${refundOrderForm.desc}</td>
					</tr>
				</c:if>
			</c:forEach>
			<c:forEach items="${totalOrderForm.refundInsureOrderForms}" var="refundInsureOrderForm">
				<tr>
					<td style="border-top: none;text-align: left;">${refundInsureOrderForm.orderTypeName}</td>
					<td style="border-top: none;">${refundInsureOrderForm.orderNo}</td>
					<td style="border-top: none;"><fmt:formatDate value="${refundInsureOrderForm.orderDate}" pattern="yyyy-MM-dd HH:mm"/>&nbsp;&nbsp;${refundInsureOrderForm.applyUserName}</td>
					<td style="border-top: none;">${refundInsureOrderForm.costCenterName}</td>
					<td style="border-top: none;"><fmt:formatNumber value="${refundInsureOrderForm.orderPrice}" pattern="#,##0.##"/>&nbsp;&nbsp;<br>${paymentMethodMap[refundInsureOrderForm.paymentMethod]}</td>
					<td style="border-top: none;font-size: 12px;">${refundInsureOrderForm.desc}</td>
				</tr>
			</c:forEach>
			
		</c:forEach>
	</tbody>
</table>

<div class="pagin">
	<jsp:include page="../../common/pagination_ajax.jsp?callback=loadBillOrderList"></jsp:include>
</div>

<div id="needSettleOrderListDiv"></div>

<script type="text/javascript" >
$(document).ready(function(){
	$("#bizType").val("${bizType}");
});

function orderDetail(orderNo,orderType){
	if(orderType == 'FLIGHT'){
		window.open("/order/flightDetail/"+orderNo);
	}else if(orderType == 'HOTEL'){
		window.open("/order/hotelDetail/"+orderNo);
	}else if(orderType == 'TRAIN'){
		window.open("/order/trainDetail/"+orderNo);
	}else if(orderType == 'GENERAL'){
		window.open("/order/generalDetail/"+orderNo);
	}else if(orderType == 'INTERNATIONAL_FLIGHT'){
	    window.open("/order/internaltionalFlightOrder/"+orderNo);
	}

}

function orderRefundDetail(orderNo){
	$.ajax({
		type:'GET',
		url:'bpAccount/settlement/getRefundOrderType?orderNo='+orderNo,
		success:function(msg){
			orderType = msg.txt;
			if(orderType == 'FLIGHT'){
				window.open("/order/refundFlightOrderDetail/"+orderNo);
			}else if(orderType == 'HOTEL'){
				window.open("/order/refundHotelDetail/"+orderNo);
			}else if(orderType == 'TRAIN'){
				window.open("/order/refundTrainDetail/"+orderNo);
			}else if(orderType == 'GENERAL'){
				window.open("/order/refundGeneralDetail/"+orderNo);
			}else if(orderType == 'GENERAL'){
                window.open("/order/refundFlightOrderDetail/"+orderNo);
            }
		}
	});
}

function selectAllOrderList1() {
    if ($("#selectAllOrderList").is(":checked")) {
        $(".selectAllOrderList input:checkbox").prop("checked", true);//所有选择框都选中
    } else {
        $(".selectAllOrderList input:checkbox").prop("checked", false);
    }
}

function batchRemove(){

    var checkedNum = $(".selectAllOrderList input[name='orderNos']:checked").length;
    if(checkedNum==0){
        $.alert("请选择你要添加的订单!","提示");
        return false;
    }

    var orderNos = new Array();
    $(".selectAllOrderList input[name='orderNos']:checked").each(function(){
        orderNos.push($(this).val());
    });

    var billId = $('#billIdOrderList').val();
    var tabIndex = 'orderList';

    $.confirm({
        title : '提示',
        confirmButton:'确认',
        cancelButton:'取消',
        content : '确认移除订单?',
        confirm : function() {

            var loading = layer.load();
            $.ajax({
                type:'post',
                url:'bpAccount/settlement/batchRemoveOrder?orderNos='+orderNos+ "&billId="+billId,
                success:function(msg){

                    layer.close(loading);
                    if(msg == 'error'){
                        showErrorMsg(msg.txt);
                    }else{
                        window.location.href="/bpAccount/settlement/billDetail?id="+billId+"&tabIndex="+tabIndex;
                        showSuccessMsg('操作成功!');
                    }
                },
				error: function () {
                    layer.close(loading);
                }
            });
        }
    });


}

function orderInsuranceDetail(orderNo){
	window.open("order/insuranceDetail/"+orderNo);
}

function removeOrder(orderNo){
	var billId = $('#billIdOrderList').val();
	var tabIndex = 'orderList';
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认移除此订单?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'bpAccount/settlement/removeOrder?orderNo='+orderNo+ "&billId="+billId,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						window.location.href="/bpAccount/settlement/billDetail?id="+billId+"&tabIndex="+tabIndex;
						showSuccessMsg('操作成功!');
					}
				}
			});
		}
	});
}

function markControversial(orderNo){
	var tabIndex = 'orderList';
	var billId = $('#billIdOrderList').val();
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确定标记为争议订单并从账单中移除?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'bpAccount/settlement/markControversial?orderNo='+orderNo + "&billId="+billId,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						window.location.href="/bpAccount/settlement/billDetail?id="+billId+"&tabIndex="+tabIndex;
						showSuccessMsg('操作成功');
					}
				}
			});
		}
	});
}

function addBillOrder(){
	var billId = $('#billIdOrderList').val();
	if(billId){
		showListModal("bpAccount/settlement/needSettleOrderList?billId="+billId, function() {
			$('#modelOrderList').modal();
		});
	}else{
		$.alert('请先保存基本信息！','提示');
		return;
	}
}

function showListModal(url, callback) {
	$('#needSettleOrderListDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

$("input[name='orderNo']").keydown(function(e) {
    if (e.keyCode == 13) {
        event.keyCode = 0;//屏蔽回车键
        event.returnValue = false;
    }
});

</script>