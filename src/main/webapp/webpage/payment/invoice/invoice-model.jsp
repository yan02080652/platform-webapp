<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>发票明细
             	 &nbsp;&nbsp;<a data-pjax href="payment/invoice" >返回</a>
             </div>
             <div class="panel-body">
            	<form action="" class="form-horizontal bv-form" novalidate="novalidate">
					 <div class="form-group">
					    <label class="col-sm-2 control-label">企业名称:</label>
					    <div class="col-sm-3">
					      <input type="text"  class="form-control" value="${invoice.bpName }" readonly="readonly">
					    </div>
					    <label class="col-sm-2 control-label">账户名称:</label>
					    <div class="col-sm-3">
					      <input type="text" class="form-control" value="${accountName }" readonly="readonly">
					    </div>	
					 </div>	
					 <div class="form-group">
					    <label class="col-sm-2 control-label">发票类型:</label>
					    <div class="col-sm-3">
					      <input type="text" class="form-control" value="${invoice.invoiceTypeName }" readonly="readonly">
					    </div>		
					    <label class="col-sm-2 control-label">开票方：</label>
					    <div class="col-sm-3">
				    	  <input type="text" class="form-control" value="${invoice.invoiceSource }" readonly="readonly">
					    </div>			    
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">开票金额(元)：</label>
					    <div class="col-sm-3">
				    	  <input type="text" class="form-control" value="<fmt:formatNumber value="${invoice.amount*0.01 }" pattern="#,##0.##"/>" readonly="readonly">
					    </div>
					     <label class="col-sm-2 control-label">受票方:</label>
					    <div class="col-sm-3">
					      <input type="text" class="form-control" value="${invoice.invoiceDest }" readonly="readonly">
					    </div>	
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">税费(元)：</label>
					    <div class="col-sm-3">
				    	  <input type="text" class="form-control" value="<fmt:formatNumber value="${invoice.tax*0.01 }" pattern="#,##0.##"/>" readonly="readonly">
					    </div>
					     <label class="col-sm-2 control-label">发票号码:</label>
					    <div class="col-sm-3">
					      <input type="text" class="form-control" value="${invoice.invoiceNo }" readonly="readonly">
					    </div>
					   
					 </div>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label">发票总金额(元)：</label>
					    <div class="col-sm-3">
				    	  <input type="text" class="form-control" value="<fmt:formatNumber value="${invoice.totalAmount*0.01}" pattern="#,##0.##"/>" readonly="readonly">
					    </div>
					    <label class="col-sm-2 control-label">物流状态:</label>
					    <div class="col-sm-3">
					      <input type="text" class="form-control" value="${invoice.shippingStatusName }" readonly="readonly">
					    </div>
					 </div>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label">开票人:</label>
					    <div class="col-sm-3">
					      <input type="text" class="form-control" value="${openUserName }" readonly="readonly">
					    </div>
					    <label class="col-sm-2 control-label">开票日期:</label>
					    <div class="col-sm-3">
					      <input type="text" class="form-control" readonly="readonly"	value="<fmt:formatDate value="${invoice.invoiceDate}" pattern="yyyy-MM-dd"/>" >
					    </div>
					 </div>
					 <div class="form-group">
					 	<label class="col-sm-2 control-label">物流单号:</label>
					    <div class="col-sm-3">
					      <input type="text" id="shippingInfoInput" class="form-control" value="${invoice.shippingInfo }" <c:if test = "${invoice.shippingStatus eq 'SHIPED'}">readonly="readonly"</c:if>>
					    </div>
					    <label class="col-sm-2 control-label">寄送日期：</label>
					    <div class="col-sm-2">
				    	  <input type="text" id="shippingDateInput" class="form-control" onclick="WdatePicker()" <c:if test = "${invoice.shippingStatus eq 'SHIPED'}">readonly="readonly"</c:if> value="<fmt:formatDate value="${invoice.shippingDate }" pattern="yyyy-MM-dd"/>">
					    </div>
					    <div class="col-sm-1" style="padding-left:0px;width:80px;">
					    	 <input type="button"  class="form-control" style="width: 90px;" id="shippingStatusId" value="${invoice.shippingStatusName }" onclick="shipping(${invoice.id});" 
					    	 <c:if test = "${invoice.shippingStatus eq 'SHIPED'}">disabled</c:if> />
					    </div>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-2 control-label">备注</label>
					    <div class="col-sm-8">
					      <textarea rows="3" class="form-control" readonly="readonly" value="${invoice.remark }"></textarea>
					    </div>
					 </div>
				 </form>		 		 				 					 			 				 
             </div>
         </div> 
	</div>
</div>

<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script type="text/javascript">
function shipping(invoiceId){
	
	var shippingDate = $('#shippingDateInput').val();
	var shippingInfo = $('#shippingInfoInput').val();
	if(!shippingInfo){
		$.alert('请填写物流单号！','提示');
		return;
	}
	if(!shippingDate){
		$.alert('请填写寄送日期！','提示');
		return;
	}
	
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认此发票已经寄送?',
		confirm : function() {
			$.post('payment/invoice/update',{
				invoiceId : invoiceId,
				shippingDate : shippingDate,
				shippingInfo : shippingInfo
			},function(data){
				/* $('#shippingStatusId').val("已寄送");
				$('#shippingStatusId').attr('disabled','disabled'); */
				window.location.href = "/payment/invoice/detail?invoiceId="+invoiceId;
			});
		}
	});
}
</script>