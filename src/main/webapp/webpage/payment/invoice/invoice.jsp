<%@ page import="com.tem.platform.security.authorize.PermissionUtil" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>系统发票查询&nbsp;&nbsp;
			</div>
			<div class="col-md-12" style="padding-top: 10px;">
				<form class="form-inline" method="post" id="queryform" action="payment/invoice">
					<nav class="text-right">
						<input type="hidden" name="pageIndex" id = "pageIndex"/>
						<input type="hidden" name="billId"/>

						<div class=" form-group selectHeader">
							<jsp:include page="../../common/businessSelecter.jsp"></jsp:include>
						</div>

						<div class="input-group col-md-2">
							<input type="text" id="invoiceDest" name="invoiceDest" value="${invoiceForm.invoiceDest }" placeholder="受票方"	class="form-control dropdown-common myDropDownInput"> <span class="input-group-btn">
								<button class="btn btn-default" type="button"
									onclick="selectMyDropDown(this)">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
							</span>
						</div>					    
					             物流状态：
 					    <div class="form-group">
						    <div class="col-sm-2">
							     <select class="form-control" name="shippingStatus">
							     	  <option value="ALL">全部</option>
							          <option value="NOTSHIP" <c:if test="${invoiceForm.shippingStatus eq 'NOTSHIP'}">selected="true"</c:if> >未寄送</option>
							          <option value="SHIPED" <c:if test="${invoiceForm.shippingStatus eq 'SHIPED'}">selected="true"</c:if> >已寄送</option>
							     </select>
						    </div>
						</div>	
						
						 清账状态：
 					    <div class="form-group">
						    <div class="col-sm-2">
							     <select class="form-control" name="liqStatus">
							     	  <option value="ALL">全部</option>
							          <option value="OPEN" <c:if test="${invoiceForm.liqStatus eq 'OPEN'}">selected="true"</c:if> >未清账</option>
							          <option value="CLOSE" <c:if test="${invoiceForm.liqStatus eq 'CLOSE'}">selected="true"</c:if> >已清账</option>
							     </select>
						    </div>
						</div>					    				    
						<div class="form-group">
							<button type="button" class="btn btn-default"
								onclick="queryList()">查询</button>
							<button type="button" class="btn btn-default"
								onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
		</br>
		<hr style="margin-top:10px;"/>
		<div class="col-sm-12">
			<table id="contentTable"
				class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
				<thead>
					<tr>
						<th class="sort-column">企业</th>
						<th class="sort-column">受票单位</th>
						<th class="sort-column" >开票方</th>
						<th class="sort-column">发票类型</th>
						<th class="sort-column" >发票金额(元)</th>
						<th class="sort-column" >发票号</th>
						<th class="sort-column" >开票时间</th>
						<th class="sort-column" >物流状态</th>
						<th class="sort-column" >操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pageList.list}" var="invoice">
						<tr>
							<td>${invoice.bpName}</td>
							<td>${invoice.invoiceDest}</td>
							<td>${invoice.invoiceSource}</td>
							<td>${invoice.invoiceTypeName}</td>
							<td><fmt:formatNumber value="${invoice.totalAmount*0.01}" pattern="#,##0.##"/></td>
							<td>${invoice.invoiceNo}</td>
							<td><fmt:formatDate value="${invoice.invoiceDate}"   pattern="yyyy-MM-dd"/></td>
							<td>${invoice.shippingStatusName }</td>
							<td><a href = "payment/invoice/detail?invoiceId=${invoice.id }">查看明细</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryList"></jsp:include>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" >

    $(function () {
        var partnerId = "${invoiceForm.partnerId }";
        var bpName = "${invoiceForm.bpName }";

        var tmcId = "${invoiceForm.tmcId }";
        var bpTmcName = "${invoiceForm.bpTmcName }";

        $("#queryBpId").val(partnerId);
        $("#queryBpName").val(bpName);

        $("#queryBpTmcId").val(tmcId);
        $("#queryBpTmcName").val(bpTmcName);

    });

    function queryList(pageIndex) {
        pageIndex = pageIndex ? pageIndex : 1;
        $('#pageIndex').val(pageIndex);
        $("#queryform").submit();
    }

    function resetForm() {
        $("#queryform :input").val("");
        $("#queryform").submit();
    }


    function selectMyDropDown(dom) {
        $('.dropdown-common-list').remove();

        if (!$("#queryBpId").val()) {
            $.alert('请先选择企业！', '提示');
            return;
        }

        $.get('widget/corp/getCorpList', {
            partnerId: $("#queryBpId").val()
        }, function (data) {

            TR.select(data, {
                key: 'custom1',//多个自定义下拉选择时key值不能重复
                dom: dom,
                name: 'invoiceTitle',//default:name
                allowBlank: true//是否允许空白选项
            }, function (result) {
                console.log(result);
                $("#invoiceDest").val(result ? result.invoiceTitle : null);
            });

        });

    }
</script>