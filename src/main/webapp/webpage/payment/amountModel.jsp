<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="modal fade" id="modelRechargeAmount" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content" style="width: 450px">
         <div class="modal-header" style="width: 450px">
            <button type="button" class="close" onclick="closeAmountModal()" aria-hidden="true"> &times;</button>
            <h4 class="modal-title">${titleModel}</h4>
         </div>
         <div class="modal-body">
         <form class="form-horizontal" role="form">
            	<input type="hidden" name="amountType" value="${amountType}"/>
			    <div class="form-group">
			      <label class="col-sm-2 control-label">金额</label>
			      <div class="col-sm-10">
			         <input type="number" class="form-control" name="reChangeAmount"/>
			      </div>
			   </div>
			   <div class="form-group">
			      	 <label class="col-sm-2 control-label">备注</label>
			      <div class="col-sm-10">
			         <input type="text" class="form-control" name="remark"/>
			      </div>
			   </div>
		 </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="closeAmountModal()">关闭
            </button>
            <button type="button" class="btn btn-primary" onclick="sureReChange()">确认
            </button>
         </div>
      </div>
	</div>
</div>

<script type="text/javascript">
 
 function closeAmountModal(){
	 $('#modelRechargeAmount').modal('hide');
 }
 
 function sureReChange(){
	 var bpAccountId = $('#bpAccountId').val();
	 var amountType = $('input[name="amountType"]').val();
	 var reChangeAmount = $('input[name="reChangeAmount"]').val();
	 var remark = $('input[name="remark"]').val();
	 if(!reChangeAmount){
		 $.alert('请输入金额','提示');
	 }
	 var dataInfo = {};
	 dataInfo.bpAccountId = bpAccountId;
	 dataInfo.amountType = amountType;
	 dataInfo.reChangeAmount = parseFloat(reChangeAmount*100);
	 dataInfo.remark = remark;
	 
	 $.post("bpAccount/manage/saveReChangeAmount",dataInfo,
		function(data){
			if (data.type == 'success') {
				$('#modelRechargeAmount').modal('hide');
				if(amountType == 'credit'){
					$('#bpAccountCreditAmount').val(parseFloat(reChangeAmount));
				}else{
					var bpAccountBalanceAmount = parseFloat($('#bpAccountBalanceAmount').val());
					$('#bpAccountBalanceAmount').val(bpAccountBalanceAmount + parseFloat(reChangeAmount));
				}
			}else{
				showErrorMsg(data.txt);
			}
		}
	 );
}
 
 </script>