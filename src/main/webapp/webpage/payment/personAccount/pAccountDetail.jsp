<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">

<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>账户概览
             	 &nbsp;&nbsp;<a style="color:#337ab7" data-pjax href="personAccount/index" >返回</a>
             </div>
             <div class="panel-body">
             	<p style="font-size: 18px;font-weight:bold;">${pAccount.fullname } &nbsp;个人账户 </p>
             	<br/>
             	<span>
	             	<c:if test="${pAccount.balance >= 0 }">账户现有资金：<fmt:formatNumber value="${pAccount.balance*0.01}" pattern="#,##0.##"/></c:if>
             	</span>
             	<span style="margin-left: 150px">
             		授信额度： <fmt:formatNumber value="${pAccount.creditAmount*0.01 }" pattern="#,##0.##"/>
             	</span>
             	<span style="margin-left: 150px">
             		<font style="font-weight: bold;">剩余可用额度:<fmt:formatNumber value="${(pAccount.balance + pAccount.creditAmount)*0.01}" pattern="#,##0.##"/></font>
             	</span>
             </div>
             
             <ul id="myTab" class="nav nav-tabs" data-trigger="tab">
				<li><a onclick="loadAccountFlow('${pAccount.userId}')">账户流水查询</a></li>
				<li><a onclick="loadWithdraw('${pAccount.userId}')">提现记录</a></li>
			 </ul>
			 <div class="pane-wrapper" style="margin-bottom: 20px;">
				<div id="accountFlow"></div>
			 	<div id="withdrawList"></div>
			 </div>
         </div>
	</div>
</div>


<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tab.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tooltip.js?version=${globalVersion}"></script>
<script type="text/javascript">

$(document).ready(function(){
	//加载账单信息
	var userId = ${pAccount.userId};
    loadAccountFlow(userId);
});
	
function loadAccountFlow(userId){
	$('#accountFlow').load("personAccount/accountFlow?userId="+userId, function(context, state) {
		
	});
}

function loadWithdraw(userId){
    $('#withdrawList').load("personAccount/withdrawList?userId="+userId, function(context, state) {

    });
}
</script> 