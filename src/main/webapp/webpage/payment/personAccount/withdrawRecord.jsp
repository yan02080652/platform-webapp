<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<link href="webpage/payment/personAccount/pAccTradeRecord.css?version=${globalVersion}" rel="stylesheet">

<div class="content ml10" style="margin-left: 20px;">
    <div class="sel" style="width: 1140px;padding-top: 20px;padding-left: 0px;margin-bottom: 0px;">
    	<form id="withdrawRecordForm" action="personAccount/withdrawList">
    	<input name="userId" value="${userId}" type="hidden"/>
    	<input name="pageIndex" id="withdrawAccountPageIndex" type="hidden"/>
    	<input name="range" value="${range }" id="withdrawAccountRange" type="hidden"/>
        <div class="mb20" id="sel1">
			申请时间：
			<input type="text" id="begin1" class="date" onclick="selectDate()" value="${startDate }" name="startDate"> 
			-
			<input id="end1" class="date" type="text" name="endDate" onclick="selectDate()" value="${endDate }" > 
			 最近： <span id="oneWeek1" data-value="0">一周</span><span data-value="1" id="oneMonth1">一月</span> <span id="oneYear1" data-value="2">一年</span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 关键词：
            <input id="keyWords1" class="keyWords1" name="keyword" type="text" value="${keyword }" placeholder="请输入收款人、账户">
            <span id="search" onclick="queryPAccTradeRecord()" class="button">搜索</span>
		</div>
        </form>
    </div>
    <div class="result">
        <table class="table">
            <thead>
            <tr>
                <th class="tipL">申请时间</th>
                <th>收款人姓名</th>
                <th>收款通道</th>
                <th>收款账户</th>
                <th>提现金额</th>
				<th>审核状态</th>
                <th style="min-width: 150px">备注</th>
            </tr>
            </thead>
            <tbody>
	            <c:forEach items="${pageList.list}" var="withdraw">
		            <tr>
		                <td><fmt:formatDate value="${withdraw.createTime }" pattern="yyyy-MM-dd HH:mm" /></td>
		                <td>
		                	${withdraw.fullname }
		                </td>
		                <td>
		                	${withdraw.channel }
		                </td>
		                <td>${withdraw.receivable }</td>
		                <td>￥<fmt:formatNumber value="${withdraw.amount*0.01 }" pattern="#,##0.##"/> </td>
		                <td>
							<c:choose>
								<c:when test="${withdraw.status == 0}">待审核</c:when>
								<c:when test="${withdraw.status == 1}">审核通过</c:when>
								<c:when test="${withdraw.status == 2}">审核拒绝</c:when>
								<c:when test="${withdraw.status == 3}">退款完成</c:when>
								<c:otherwise></c:otherwise>
							</c:choose>
						</td>
						<td>
		                	${withdraw.memo }
		                </td>
		            </tr>
				</c:forEach>
            </tbody>

        </table>
    </div>
    <div class="pagin">
		<jsp:include page="../../common/pagination_ajax.jsp?callback=queryWithdraw"></jsp:include>
	</div>
</div>
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 

<script type="text/javascript">
function selectDate(){
	WdatePicker({isShowClear:false,readOnly:true});
	$("#oneWeek1").removeClass("selected");
	$("#oneMonth1").removeClass("selected");
	$("#oneYear1").removeClass("selected");

    $("#withdrawAccountRange").val(-1);
}
function queryWithdraw(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("#withdrawAccountPageIndex").val(pageIndex);
	console.log($("#withdrawRecordForm").serialize());
	$('#withdrawList').load("personAccount/withdrawList",$("#withdrawRecordForm").serialize(), function(context, state) {
		
	});
}

$("#sel1 span").click(function () {
	//更新时间，然后发送ajax请求
	initDate($(this).attr("data-value"));
	$("#withdrawAccountRange").val($(this).attr("data-value"));
    queryWithdraw();
	
    $(this).addClass("selected").siblings().removeClass("selected");
});

//初始化
$(function(){
	var range = $("#withdrawAccountRange").val();
	if(range == 0){
		$("#oneWeek1").addClass("selected");
	}else if(range == 1){
		$("#oneMonth1").addClass("selected");
	}else if(range == 2){
		$("#oneYear1").addClass("selected");
	}
	
});

//更新时间
function initDate(range){
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	if(range == 0){//一周
		$("#begin1").val(new Date(date.getTime() - 7 * 24 * 3600 * 1000).format("yyyy-MM-dd"));
	}else if(range == 1){//一个月
		if((month-1)==0){//跨年时要判断
			year--;
			month=12;
		}else{
			month--;
		}
		$("#begin1").val(year+"-"+month+"-"+day);
	}else if(range == 2){//一年 
		$("#begin1").val((year-1)+"-"+month+"-"+day);
	}
	
	$("#end1").val(new Date().format("yyyy-MM-dd"));
}
</script>

