<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<link href="webpage/payment/personAccount/pAccTradeRecord.css?version=${globalVersion}" rel="stylesheet">

<div class="content ml10" style="margin-left: 20px;">
    <div class="sel" style="width: 1140px;padding-top: 20px;padding-left: 0px;margin-bottom: 0px;">
    	<form id="pAccTradeAccountForm" action="personAccount/accountFlow">
    	<input name="userId" value="${userId}" type="hidden"/>
    	<input name="pageIndex" id="pAccTradeAccountPageIndex" type="hidden"/>
    	<input name="range" value="${range }" id="pAccTradeAccountRange" type="hidden"/>
        <div class="mb20" id="sel1">
			交易时间：
			<input type="text" id="begin" class="date" onclick="selectDate()" value="${startDate }" name="startDate"> 
			-
			<input id="end" class="date" type="text" name="endDate" onclick="selectDate()" value="${endDate }" > 
			 最近： <span id="oneWeek" data-value="0">一周</span><span data-value="1" id="oneMonth">一月</span> <span id="oneYear" data-value="2">一年</span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 关键词：
            <input id="keyWords" class="keyWords" name="keyword" type="text" value="${keyword }" placeholder="请输入订单号、流水号">
            <span id="search" onclick="queryPAccTradeRecord()" class="button">搜索</span>
		</div>
        </form>
    </div>
    <div class="result">
        <table class="table">
            <thead>
            <tr>
                <th class="tipL">交易时间</th>
                <th>交易号</th>
                <th>交易类型</th>
                <th>交易人</th>
                <th>交易金额</th>
                <th style="min-width: 150px">备注</th>
            </tr>
            </thead>
            <tbody>
	            <c:forEach items="${pageList.list}" var="pAccTradeRecord">
		            <tr>
		                <td><fmt:formatDate value="${pAccTradeRecord.tradeTime }" pattern="yyyy-MM-dd HH:mm" /></td>
		                <td>
		                	${pAccTradeRecord.billId }
		                </td>
		                <td>
		                	${pAccTradeRecord.tradeInstruction }
		                </td>
		                <td>${pAccTradeRecord.createUserName }</td>
		                <td>￥<fmt:formatNumber value="${pAccTradeRecord.amount*0.01 }" pattern="#,##0.##"/> </td>
		                <td>
		                	${pAccTradeRecord.memo }
		                </td>
		            </tr>
				</c:forEach>
            </tbody>

        </table>
    </div>
    <div class="pagin">
		<jsp:include page="../../common/pagination_ajax.jsp?callback=queryPAccTradeRecord"></jsp:include>
	</div>
</div>
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 

<script type="text/javascript">
function selectDate(){
	WdatePicker({isShowClear:false,readOnly:true});
	$("#oneWeek").removeClass("selected");
	$("#oneMonth").removeClass("selected");
	$("#oneYear").removeClass("selected");

    $("#pAccTradeAccountRange").val(-1);
}
function queryPAccTradeRecord(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("#pAccTradeAccountPageIndex").val(pageIndex);
	$('#accountFlow').load("personAccount/accountFlow",$("#pAccTradeAccountForm").serialize(), function(context, state) {
		
	});
}

$("#sel1 span").click(function () {
	//更新时间，然后发送ajax请求
	initDate($(this).attr("data-value"));
	$("#pAccTradeAccountRange").val($(this).attr("data-value"));
	queryPAccTradeRecord();
	
    $(this).addClass("selected").siblings().removeClass("selected");
});

//初始化
$(function(){
	var range = $("#pAccTradeAccountRange").val();
	if(range == 0){
		$("#oneWeek").addClass("selected");
	}else if(range == 1){
		$("#oneMonth").addClass("selected");
	}else if(range == 2){
		$("#oneYear").addClass("selected");
	}
	
});

//更新时间
function initDate(range){
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	if(range == 0){//一周
		$("#begin").val(new Date(date.getTime() - 7 * 24 * 3600 * 1000).format("yyyy-MM-dd"));
	}else if(range == 1){//一个月
		if((month-1)==0){//跨年时要判断
			year--;
			month=12;
		}else{
			month--;
		}
		$("#begin").val(year+"-"+month+"-"+day);
	}else if(range == 2){//一年 
		$("#begin").val((year-1)+"-"+month+"-"+day);
	}
	
	$("#end").val(new Date().format("yyyy-MM-dd"));
}
</script>

