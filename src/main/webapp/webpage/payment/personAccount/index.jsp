<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>个人账户列表
			</div>
			<div class="col-md-9">
				<form class="form-inline" action="personAccount/index" method="post" id="queryform">
					<nav class="text-right">
						<div class="selectHeader form-group">
							<jsp:include page="../../common/businessSelecter.jsp"></jsp:include>
						</div>
						<input type="hidden" name="pageIndex"/>
						<div class="form-group">
							<button type="button" class="btn btn-default" onclick="queryList()">查询</button>
							<button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>
		</div>
		<hr style="margin-top:10px;"/>
		<div class="col-md-12" style="margin-top: 20px;">
			<table class="table">
				<thead>
				<tr>
					<th>姓名</th>
					<th>账户余额(元)</th>
					<th>授信额度(元)</th>
					<th>开户时间</th>
					<th>最后修改时间</th>
					<th>操作</th>
				</tr>
				</thead>
					<c:forEach var="account" items="${pageList.list}">
						<tr>
							<td>${account.fullname}</td>
							<td><fmt:formatNumber value="${account.balance * 0.01}" pattern="#,##0.##"/></td>
							<td><fmt:formatNumber value="${account.creditAmount * 0.01}" pattern="#,##0.##"/></td>
							<td><fmt:formatDate value="${account.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td><fmt:formatDate value="${account.lastModifyDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td><a onclick="accountDetail('${account.id}');" >详情</a></td>
						</tr>
					</c:forEach>
				<tbody>

				</tbody>
			</table>
			<div class="pagin">
				<jsp:include page="../../common/pagination_ajax.jsp?callback=queryList"></jsp:include>
			</div>
		</div>
	</div>
</div>

<script>
function accountDetail(accountId) {
    window.open("/personAccount/detail?id=" + accountId);
}

$(function () {
    $("#queryBpTmcId").val('${condition.tmcId}');
    $("#queryBpTmcName").val('${bpTmcName}');

    $("#queryBpId").val('${condition.partnerId}');
    $("#queryBpName").val('${bpName}');
})

function queryList(pageIndex){
    pageIndex = pageIndex ? pageIndex : 1;

    $("input[name='pageIndex']").val(pageIndex);

    $("#queryform").submit();
}

function resetForm(){
    $("#queryform :input").val("");
    $("#queryform").submit();
}

</script>