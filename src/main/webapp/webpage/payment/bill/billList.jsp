<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<table style="margin-bottom : 0px" class="table table-bordered">
	<thead>
		<tr>
			<th class="sort-column" style="text-align: center;" >企业名称</th>
			<th class="sort-column" >账户名称</th>
			<th class="sort-column" >结算期间</th>
			<th class="sort-column" >账单金额</th>
			<th class="sort-column" >创建时间</th>
			<th class="sort-column" >账单状态</th>
			<th class="sort-column" >开票状态</th>
			<th class="sort-column" >清账状态</th>
			<th class="sort-column" >未清账金额</th>
			<th style="min-width: 70px">操作</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pageList.list}" var="bill" varStatus="status">
			<tr>
				<td style="text-align:center;">${bill.bpName}</td>
				<td>${bill.accountName}</td>
				<td><fmt:formatDate value="${bill.liqDateBegin}" pattern="yyyy/MM/dd"/> - <fmt:formatDate value="${bill.liqDateEnd}" pattern="yyyy/MM/dd"/></td>
				<td><fmt:formatNumber value="${bill.billAmount*0.01}" pattern="#,##0.##"/></td>
				<td><fmt:formatDate value="${bill.createDate }" pattern="yyyy-MM-dd" /></td>
				<td>
					<c:if test="${bill.status eq 'DRAFT'}"><font color="#ba2626">草稿</font></c:if>
					<c:if test="${bill.status eq 'NOT_CONFIRM'}"><font color="#ba2626">待确认</font></c:if>
					<c:if test="${bill.status eq 'CONFIRM'}">已确认</c:if>
					<c:if test="${bill.status eq 'CANCEL'}">已废弃</c:if>
				</td>
				<td>
					<c:if test="${bill.status eq 'CONFIRM'}">
						<c:if test="${bill.invoiceAmount >= bill.billAmount and bill.billAmount>0}">已开票</c:if>
						<c:if test="${bill.invoiceAmount>0 and bill.invoiceAmount<bill.billAmount}">
							<font color="#ba2626">部分开票</font>
						</c:if>
						<c:if test="${empty bill.invoiceAmount or bill.invoiceAmount==0 }">
							<font color="#ba2626">未开票</font>
						</c:if>
					</c:if>
				</td>
				<td>
					<c:if test="${bill.status eq 'CONFIRM'}">
						<c:choose>
							<c:when test="${bill.liquidationStatus eq 'OPEN' && bill.remainAmount > 0 && bill.remainAmount < bill.billAmount}">
								<font color="#ba2626">部分清账</font>
							</c:when>
							<c:when test="${bill.liquidationStatus eq 'CLOSE'}">
								已清账
							</c:when>
							<c:otherwise><font color="#ba2626">未清账</font></c:otherwise>
						</c:choose>
					</c:if>
				</td>
				<td><fmt:formatNumber value="${bill.remainAmount*0.01}" pattern="#,##0.##"/></td>
				<td>
					<%-- <c:if test="${bill.status eq 'NOT_CONFIRM'}">
						<a class="btn btn-default" href="javascript:;" onclick="confirmBill('${bill.id}')" role="button">确认账单</a>
					</c:if> --%>
					<a class="btn btn-default" style="display: inline;" href="javascript:;" onclick="queryBillDetail('${bill.id}')" role="button">查看明细</a>	
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<div class="pagin">
	<jsp:include page="../../common/pagination_ajax.jsp?callback=queryBillList"></jsp:include>
</div>
<script type="text/javascript" >
$(document).ready(function(){
	
});

function queryBillDetail(id){
	window.location.href = "/bpAccount/settlement/billDetail?id="+id+"&linkFrom=billList";
}

function confirmBill(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认此账单?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'bill/manage/confirmBill?id='+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						queryBillList();
						showSuccessMsg(msg.txt);
					}
				}
			});
		}
	});
}

</script>