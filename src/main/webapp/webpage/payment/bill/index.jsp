<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>

<style>
	.selectHeader label{
		display: none !important;
	}

</style>

<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业账单管理&nbsp;&nbsp;
			</div>
		</div>
			<div class="col-md-12" style="margin-top: 10px;margin-bottom:12px;float: right;">
				<form class="form-inline" id="queryform">
					<nav class="text-right">
						<div class="form-group selectHeader">
							<input type="hidden" id="pageIndex" name="pageIndex" />
							<input type="hidden" id="accountId" name="accountId" value="${accountId }" />
							<jsp:include page="../../common/businessSelecter.jsp"></jsp:include>
						</div>
						<div class="form-group input-group">
							<input name="accountName" type="text" value="${accountName }" onclick="selectMyDropDown()" class="form-control" style="width: 150px" placeholder="所属账户" /><span class="input-group-btn">
								<button class="btn btn-default" id="accountSelect" type="button" onclick="selectMyDropDown()"> <i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
							</span>
						</div>
						<div class="form-group input-group">
							<input name="beginDate" type="text" value="${beginDate }" onclick="WdatePicker()" class="form-control" style="width: 105px" placeholder="开始时间" /><span class="input-group-btn"/>
							<input name="endDate" type="text"  value="${endDate }" onclick="WdatePicker()" class="form-control" style="width: 105px" placeholder="结束时间" /><span class="input-group-btn"/>
						</div>
						<div class=" form-group">
							确认状态
							<select class="form-control"  id="confirmStatus" name="confirmStatus" style="width: 90px" placeholder="账单状态">
								<option value="">全部</option>
								<option value="DRAFT">草稿</option>
								<option value="NOT_CONFIRM">待确认</option>
								<option value="CONFIRM">已确认</option>
								<option value="CANCEL">已废弃</option>
							</select>
						</div>
						<div class=" form-group">
							开票状态
							<select class="form-control"  id="invoiceStatus" name="invoiceStatus" style="width: 90px" placeholder="开票状态">
								<option value="">全部</option>
								<option value="OPEN">已开票</option>
								<option value="UNOPEN">未开票</option>
								<option value="SOMEOPEN">部分开票</option>
								<option value="UNCOMP">未完成</option>
							</select>
						</div>
						<div class=" form-group">
							清账状态
							<select class="form-control"  id="billStatus" name="billStatus" style="width: 90px" placeholder="清账状态">
								<option value="">全部</option>
								<option value="CLOSE">已清账</option>
								<option value="OPEN">未清账</option>
							</select>
						</div>
						<div class="form-group">
							<button type="button" class="btn btn-default"
									onclick="queryBillList()">查询</button>
							<button type="button" class="btn btn-default"
									onclick="resetForm()">重置</button>
						</div>
					</nav>
				</form>
			</div>

			<hr>
			<div class="col-sm-12" id="billListDiv">
			</div>
		</div>

</div>

<div id="bpAccountFieldDiv"></div>
<script src="${baseRes}/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script>
<script type="text/javascript" >
$(document).ready(function(){
	var confirmStatus = '${confirmStatus}';
	$("#confirmStatus").val(confirmStatus);
	var billStatus = '${billStatus}';
	$("#billStatus").val(billStatus);
	
	queryBillList();
});

function selectMyDropDown(){
	var partnerId = $("#queryBpId").val();
	if(!partnerId){
		$("input[name='accountName']").blur();
		$.alert('请先选择企业！','提示');
		return;
	}
	
	$('.dropdown-common-list').remove();
	
	$.get('widget/account/getAccountList',{
		partnerId : partnerId
	},function(data){
		TR.select(data,{
			key:'custom1',//多个自定义下拉选择时key值不能重复
			dom:$('#accountSelect'),
			name:'accountName',//default:name
			allowBlank:true//是否允许空白选项
		},function(result){
			$("input[name='accountId']").val(result ? result.id : null);
			$("input[name='accountName']").val(result ? result.accountName : null);
		});
		
	});

}

function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}

function queryBillList(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("input[name='pageIndex']").val(pageIndex);
	
	var dataParam = $('#queryform').serializeArray();
	var dataInfo = {};
	$.each(dataParam,function(i,d){
		dataInfo[d.name] = d.value;
	});

	var loadIndex = layer.load();
	$('#billListDiv').load("bill/manage/billList",dataInfo, function(context, state) {
	    layer.close(loadIndex);
		if ('success' == state) {
		}
	});
}

</script>