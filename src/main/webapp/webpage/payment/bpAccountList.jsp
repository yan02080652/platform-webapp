<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="panel panel-default">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
                 <span class="glyphicon glyphicon-home" aria-hidden="true"></span>${title}账户列表&nbsp;&nbsp; 共计<span>${pageList.totalPage }页,${pageList.total }</span>条数据   &nbsp;&nbsp;
			</div>
			<div class="col-md-8">
				<c:if test="${type ne 'me'}">
					<form class="form-inline" id="queryform" method="post" action="bpAccount/manage">
						<nav class="text-right">
							<div class=" form-group">
								<input type="hidden" id="pageIndex" name="pageIndex" />
								<input type="hidden" name="type" value="${type}"/>
								<input type="hidden" id="queryBpId" name="c_partnerId" value="${c_partnerId }" />
								<input name="bpName" type="text" id="queryBpName" value="${bpName }" onclick="selectPartner()" class="form-control" placeholder="企业名称" />
									<%--<input name="accountName" type="text" value="${accountName }" class="form-control" placeholder="账户名称" />
                                    <select class="form-control"  id="accountType" name="accountType" style="width: 150px" placeholder="账户类型">
                                        <option value="">账户类型</option>
                                        <option value="B">预存账户</option>
                                        <option value="C">授信账户</option>
                                    </select>--%>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-default"
										onclick="queryBpAccount()">查询</button>
								<button type="button" class="btn btn-default"
										onclick="resetForm()">重置</button>
							</div>
						</nav>
					</form>
				</c:if>
			</div>
		</div>
	
	<hr/>
		<div class="col-sm-12" style="margin-bottom: 10px;">
			<div class="pull-left">
				<c:if test='${m}'><a class="btn btn-default" href="javascript:;" onclick="addBpAccount()" role="button">添加账户</a></c:if>
			</div>
		</div>
		
		<div class="col-sm-12">
			<table style="margin-bottom : 0px"
				class="table table-bordered">
				<thead>
					<tr>
						<th class="sort-column" style="text-align:center;" >企业名称</th>
						<th class="sort-column" >账户名称</th>
						<th class="sort-column" >账户类型</th>
						<th class="sort-column" >账户余额</th>
						<th class="sort-column" >授信额度</th>
						<th class="sort-column" >可用额度</th>
						<th class="sort-column" >账户状态</th>
						<th class="sort-column" >开户时间</th>
						<th class="sort-column" >最后修改时间</th>
						<th class="sort-column" >最后修改人</th>
						<th style="min-width: 110px">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${bpGroupList}" var="bpGroup">
						<tr>
							<td style="text-align:center;vertical-align:middle;" rowspan="${fn:length(bpGroup.bpList) }">${bpGroup.bpName }</td>
							<c:forEach items="${bpGroup.bpList}" var="bpAccount" varStatus="status">
								<c:choose>
									<c:when test="${empty bpAccount.id}">
										<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
									</c:when>
									<c:when test="${(bpAccount.creditAmount*0.01 + bpAccount.balanceAmount*0.01)<=bpAccount.warnLine*0.01 or (bpAccount.creditAmount != 0 and (bpAccount.creditAmount*0.01 + bpAccount.balanceAmount*0.01)<= (bpAccount.creditAmount*0.01*0.1))}">
										<c:if test="${status.count == 1}">
											<td title="可用余额预警" style="text-align:center;background: #FF9797;">${bpAccount.accountName}</td>
											<td title="可用余额预警" style="background: #FF9797;">${bpAccount.accountTypeName}</td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatNumber value="${bpAccount.balanceAmount*0.01}" pattern="#,##0.##"/></td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatNumber value="${bpAccount.creditAmount*0.01}" pattern="#,##0.##"/></td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatNumber value="${bpAccount.creditAmount*0.01 + bpAccount.balanceAmount*0.01}" pattern="#,##0.##"/></td>
											<td title="可用余额预警" style="background: #FF9797;">
												<c:if test="${bpAccount.state eq 'ACTIVE'}">可用的</c:if>
												<c:if test="${bpAccount.state eq 'DISABLED'}">已禁用</c:if>
											</td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatDate value="${bpAccount.creteDate}" pattern="yyyy-MM-dd HH:mm" /> </td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatDate value="${bpAccount.lastModifyDate}" pattern="yyyy-MM-dd HH:mm" /> </td>
											<td title="可用余额预警" style="background: #FF9797;">${bpAccount.lastModifyUserName}</td>
											<td style="background: #FF9797;">
												<c:if test="${bpGroup.canChange}">
													<c:if test='${m}'> <a href="javascript:;" onclick="updateBpAccount('${bpAccount.id}')">修改</a> </c:if>
													<c:choose>
														<c:when test="${bpAccount.state eq 'ACTIVE'}">
															<c:if test='${m}'> <a href="javascript:;" onclick="disableAccount('${bpAccount.id}')">停用</a> </c:if>
														</c:when>
														<c:otherwise>
															<c:if test='${m}'> <a href="javascript:;" onclick="reOpenBpAccount('${bpAccount.id}')">启用</a> </c:if>
														</c:otherwise>
													</c:choose>
													<c:if test='${l}'> <a href="javascript:;" onclick="settleAccount('${bpAccount.id}')">结算</a> </c:if>
												</c:if>
											</td>
										</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${status.count == 1}">
											<td style="text-align:center;">${bpAccount.accountName}</td>
											<td>${bpAccount.accountTypeName}</td>
											<td><fmt:formatNumber value="${bpAccount.balanceAmount*0.01}" pattern="#,##0.##"/></td>
											<td><fmt:formatNumber value="${bpAccount.creditAmount*0.01}" pattern="#,##0.##"/></td>
											<td><fmt:formatNumber value="${bpAccount.creditAmount*0.01 + bpAccount.balanceAmount*0.01}" pattern="#,##0.##"/></td>
											<td>
												<c:if test="${bpAccount.state eq 'ACTIVE'}">可用的</c:if>
												<c:if test="${bpAccount.state eq 'DISABLED'}">已禁用</c:if>
											</td>
											<td><fmt:formatDate value="${bpAccount.creteDate}" pattern="yyyy-MM-dd HH:mm" /> </td>
											<td><fmt:formatDate value="${bpAccount.lastModifyDate}" pattern="yyyy-MM-dd HH:mm" /> </td>
											<td>${bpAccount.lastModifyUserName}</td>
											<td>
												<c:if test="${bpGroup.canChange}">
													<c:if test='${m}'> <a href="javascript:;" onclick="updateBpAccount('${bpAccount.id}')">修改</a> </c:if>
													<c:choose>
														<c:when test="${bpAccount.state eq 'ACTIVE'}">
															<c:if test='${m}'> <a href="javascript:;" onclick="disableAccount('${bpAccount.id}')">停用</a> </c:if>
														</c:when>
														<c:otherwise>
															<c:if test='${m}'> <a href="javascript:;" onclick="reOpenBpAccount('${bpAccount.id}')">启用</a> </c:if>
														</c:otherwise>
													</c:choose>
													<c:if test='${l}'> <a href="javascript:;" onclick="settleAccount('${bpAccount.id}')">结算</a> </c:if>
												</c:if>
											</td>
										</c:if>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</tr>
						<c:forEach items="${bpGroup.bpList}" var="bpAccount" varStatus="status">
							<c:choose>
								<c:when test="${(bpAccount.creditAmount*0.01 + bpAccount.balanceAmount*0.01)<=bpAccount.warnLine*0.01 or (bpAccount.creditAmount != 0 and (bpAccount.creditAmount*0.01 + bpAccount.balanceAmount*0.01)<= (bpAccount.creditAmount*0.01*0.1))}">
									<c:if test="${status.count > 1}">
										<tr>
											<td title="可用余额预警" style="text-align:center;background: #FF9797;">${bpAccount.accountName}</td>
											<td title="可用余额预警" style="background: #FF9797;">${bpAccount.accountTypeName}</td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatNumber value="${bpAccount.balanceAmount*0.01}" pattern="#,##0.##"/></td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatNumber value="${bpAccount.creditAmount*0.01}" pattern="#,##0.##"/></td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatNumber value="${bpAccount.creditAmount*0.01 + bpAccount.balanceAmount*0.01}" pattern="#,##0.##"/></td>
											<td title="可用余额预警" style="background: #FF9797;">
												<c:if test="${bpAccount.state eq 'ACTIVE'}">可用的</c:if>
												<c:if test="${bpAccount.state eq 'DISABLED'}">已禁用</c:if>
											</td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatDate value="${bpAccount.creteDate}" pattern="yyyy-MM-dd HH:mm" /> </td>
											<td title="可用余额预警" style="background: #FF9797;"><fmt:formatDate value="${bpAccount.lastModifyDate}" pattern="yyyy-MM-dd HH:mm" /> </td>
											<td title="可用余额预警" style="background: #FF9797;">${bpAccount.lastModifyUserName}</td>
											<td style="background: #FF9797;">
												<c:if test="${bpGroup.canChange}">
													<c:if test='${m}'> <a href="javascript:;" onclick="updateBpAccount('${bpAccount.id}')">修改</a> </c:if>
													<c:choose>
														<c:when test="${bpAccount.state eq 'ACTIVE'}">
															<c:if test='${m}'> <a href="javascript:;" onclick="disableAccount('${bpAccount.id}')">停用</a> </c:if>
														</c:when>
														<c:otherwise>
															<c:if test='${m}'> <a href="javascript:;" onclick="reOpenBpAccount('${bpAccount.id}')">启用</a> </c:if>
														</c:otherwise>
													</c:choose>
													<c:if test='${l}'> <a href="javascript:;" onclick="settleAccount('${bpAccount.id}')">结算</a> </c:if>
												</c:if>
											</td>
										</tr>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${status.count > 1}">
										<tr>
											<td style="text-align:center;">${bpAccount.accountName}</td>
											<td>${bpAccount.accountTypeName}</td>
											<td><fmt:formatNumber value="${bpAccount.balanceAmount*0.01}" pattern="#,##0.##"/></td>
											<td><fmt:formatNumber value="${bpAccount.creditAmount*0.01}" pattern="#,##0.##"/></td>
											<td><fmt:formatNumber value="${bpAccount.creditAmount*0.01 + bpAccount.balanceAmount*0.01}" pattern="#,##0.##"/></td>
											<td>
												<c:if test="${bpAccount.state eq 'ACTIVE'}">可用的</c:if>
												<c:if test="${bpAccount.state eq 'DISABLED'}">已禁用</c:if>
											</td>
											<td><fmt:formatDate value="${bpAccount.creteDate}" pattern="yyyy-MM-dd HH:mm" /> </td>
											<td><fmt:formatDate value="${bpAccount.lastModifyDate}" pattern="yyyy-MM-dd HH:mm" /> </td>
											<td>${bpAccount.lastModifyUserName}</td>
											<td>
												<c:if test="${bpGroup.canChange}">
													<c:if test='${m}'> <a href="javascript:;" onclick="updateBpAccount('${bpAccount.id}')">修改</a> </c:if>
													<c:choose>
														<c:when test="${bpAccount.state eq 'ACTIVE'}">
															<c:if test='${m}'> <a href="javascript:;" onclick="disableAccount('${bpAccount.id}')">停用</a> </c:if>
														</c:when>
														<c:otherwise>
															<c:if test='${m}'> <a href="javascript:;" onclick="reOpenBpAccount('${bpAccount.id}')">启用</a> </c:if>
														</c:otherwise>
													</c:choose>
													<c:if test='${l}'> <a href="javascript:;" onclick="settleAccount('${bpAccount.id}')">结算</a> </c:if>
												</c:if>
											</td>
										</tr>
									</c:if>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:forEach>
				</tbody>
			</table>
			<c:if test="${type ne 'me'}">
				<div class="pagin">
					<jsp:include page="../common/pagination_ajax.jsp?callback=queryBpAccount"></jsp:include>
				</div>
			</c:if>
		</div>
	</div>
</div>

<div id="bpAccountFieldDiv"></div>
<script type="text/javascript" >
	var type = '${type}';
$(document).ready(function(){
	$("#accountType").val("${accountType}");
});

function selectPartner(){
    var partnerType = null;
    if(type == 'tmc'){
        partnerType = 1;
	}else{
        partnerType = 0;
	}
	TR.select('partner',{
		type:partnerType//固定参数
	},function(data){
		$("#queryBpId").val(data.id);
		$("#queryBpName").val(data.name);
	});
}

document.onkeydown = function(e){
	e = e||window.event;
	if(e.keyCode==116){//116 是f5按键代码
		$("#queryform :input").val("");
		$("#queryform").submit();
	}
};

function disableAccount(id) {
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认禁用此账户?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'bpAccount/manage/cancelBpAccount?id='+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						$("#queryform").submit();
						showSuccessMsg(msg.txt);
					}
				}
			});
		}
	});
}

function queryBpAccount(pageIndex){
	pageIndex = pageIndex ? pageIndex : 1;
	$("input[name=pageIndex]").val(pageIndex);
	$("#queryform").submit();
}
function resetForm(){
	$("#queryform :input").val("");
	$("#queryform").submit();
}

function reOpenBpAccount(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : '确认重新启用此账户?',
		confirm : function() {
			$.ajax({
				type:'GET',
				url:'bpAccount/manage/reOpenBpAccount?id='+id,
				success:function(msg){
					if(msg.type == 'error'){
						showErrorMsg(msg.txt);
					}else{
						$("#queryform").submit();
						showSuccessMsg(msg.txt);
					}
				}
			});
		}
	});
}

function settleAccount(id){
	window.location.href = "/bpAccount/settlement/accountDetail?id=" + id;//+"&type="+type;
}

function addBpAccount() {
	showFieldModal("bpAccount/manage/detail?type=${type}", function() {
		$('#modelBpAccountField').modal();
		initFormField();
	});
}

function updateBpAccount(id) {
	showFieldModal("bpAccount/manage/detail?id=" + id + "", function(a) {
		$('#modelBpAccountField').modal();
		initFormField();
	});
}

function showFieldModal(url, callback) {
	$('#bpAccountFieldDiv').load(url, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function submitAuthField() {
	$("#bpAccountForm").submit();
}

function initFormField() {
	$("#bpAccountForm").validate({
		rules:{
			bpName : {
				required : true
			},
			liquidationDay : {
				required : true
			},
			warnPhone : {
				mobile : true
			},
			/*
			overdueDays : {
				required : true
			},
			lateFee : {
				required : true
			}, */
			accountName : {
				required : true,
				minlength : 1,
				maxlength : 50,
				remote : {
					url : "bpAccount/manage/checkAccountName",
					type : "post",
					data : {
						id: function() {
							return $("#bpAccountId").val();
						},
						partnerId: function(){
							return $("#bpAccountBpId").val();
						}
					},
					dataFilter : function(data) {
						var resultMes = eval("("+data+")");
						if (resultMes.type == 'success') {
							return true;
						} else {
							return false;
						}
					}
				}
			}
		},
		messages : {  
			accountName : {  
	            remote: "该账户名称已存在！"
	          }
	    },
		submitHandler : function(form) {
			var corpSelects = new Array();
			$("#bpAccountScopeCorpTable tr").each(function() {  
				corpSelects.push($(this).attr('attr'));
	        }); 
			
			/* var dataParam = $("#bpAccountForm").serialize();
			dataParam = dataParam + "&corpSelect="+corpSelects.join(","); */
			
			var dataParam = $('#bpAccountForm').serializeArray();
			var dataInfo = {};
			$.each(dataParam,function(i,d){
				dataInfo[d.name] = d.value;
			});
			if(dataInfo.creditAmount){
				dataInfo.creditAmount = dataInfo.creditAmount*100;//金额转到分
			}
			if(dataInfo.warnLine){
				dataInfo.warnLine = dataInfo.warnLine*100;//金额转到分
			}
			dataInfo.corpSelect = corpSelects.join(",");
			
			if(corpSelects.length == 0){
				$.alert("请选择法人！",'提示'); 
				return false;
			}
			
			$.post("bpAccount/manage/saveOrUpdate",dataInfo,
				function(data){
					if (data.type == 'success') {
						$('#modelBpAccountField').modal('hide');
						$("#queryform").submit();
					}
				}
			);
		},
		errorPlacement : setErrorPlacement,
		success : validateSuccess,
		highlight : setHighlight
	});
}


</script>