<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<!-- 任务列表 -->
<br/>
<div class="panel panel-default"> 
<div class="panel-body">
<!-- 引用自flight/carrier/index.base -->
<div class="row">
		<div class="col-xs-12">
			<div class="pull-left">
				<a class="btn btn-primary" onclick="batchMaintainPrices()" role="button">批量维护房价</a>
				<a class="btn btn-default" onclick="toAddHouseType()" role="button"><span class="glyphicon glyphicon-plus"></span>新增房型</a>
				<button type="button" class="btn btn-default" onclick="toAddPricingPlan()"><span class="glyphicon glyphicon-plus"></span>新增价格计划</button>
			</div>
			<div class="form-inline">
			<nav class="text-right">
			<div class=" form-group">
                 <a class="btn btn-primary" onclick="" role="button" >前7天</a>
               </div>
               <div class=" form-group">
			  <input type="text" class="form-control" placeholder="_年_月_日" style="width:115px"/>
			  <input type="hidden" id="endDate3" class="form-control  Wdate" type="text" onfocus="WdatePicker({minDate:'#F{$dp.$D(\'startDate3\')}'})" style="width: 120px"/>
			</div>
			<div class=" form-group">
			  <a class="btn btn-primary" onclick="" role="button" >后7天</a>
			</div>
			</nav>
			</div>
		</div>
  <div>
	<table class="table table-striped table-bordered table-hover" id="carrierTable">
		<thead>
		<tr>
          <th><label>房型/价格计划名称</label></th>
	      <th><label>06-06<br/>周一 </label></th>
          <th><label>06-07<br/>周二</label></th>
          <th><label>06-08<br/>周三</label></th>
          <th><label>06-09<br/>周四</label></th>
          <th><label>06-10<br/>周五</label></th>
          <th><label>06-11<br/>周六</label></th>
          <th><label>06-12<br/>周日</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
		<tbody id="myTaskTbody">
			<%-- <c:forEach items="${pageList.list }" var="orderTask"> --%>
				<tr>
					<td colspan="9">
						<span><span style="font-weight:bold;">大床房</span>&nbsp;<span class="glyphicon glyphicon-chevron-down"></span></span>
						<span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
						<span class="eRatePlan">共<font color="red">7</font>条价格计划
						<span onclick="editRatePlan()" class="glyphicon glyphicon-plus-sign" style="color:#003C9D"/></span>
						<span class="eRlan" style="display:none">
						<span onclick="" class="glyphicon glyphicon-pencil" style="color:#003C9D"></span>
						<span onclick="" class="glyphicon glyphicon-remove" style="color:#003C9D"></span>
						</span>
				    </td>
				</tr>
				<tr>
				  <td ><span>双早&nbsp;&nbsp;&nbsp;&nbsp;<font  style="background: #FFEE99 url(bgimage.gif) no-repeat fixed top;size:6px;color:#C63150">预付</font></span></td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				   <td><a onclick="edit(this)" id="edit">修改</a>
				  <a onclick="del(this)" id="del">删除</a>
				  <a onclick="save(this)" style="display:none" id="save">保存</a>
				  </td>
				</tr>
				<tr>
				  <td><span>单早&nbsp;&nbsp;&nbsp;&nbsp;<font style="background: #BBFFEE url(bgimage.gif) no-repeat fixed top;size:6px;color:#0044BB;">现付</font></span></td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td>￥0</td>
				  <td><a onclick="" >修改</a>
				  <a onclick="" >删除</a>
				  <a onclick="" style="display:none" id="save">保存</a>
				  </td>
				</tr>
				
				
					<tr>
					<td colspan="9">
						<span><span style="font-weight:bold;">双床房</span>&nbsp;<span class="glyphicon glyphicon-chevron-down"></span></span>
						<span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
						<span>共<font color="red">7</font>条价格计划</span>
						
				    </td>
				</tr>
				<tr>
				  <td><span>双早&nbsp;&nbsp;&nbsp;&nbsp;<font  style="background:  #FFEE99 url(bgimage.gif) no-repeat fixed top;size:6px;color:#C63150">预付</font>&nbsp;&nbsp;
				  <font  style="background:  #0066FF url(bgimage.gif) no-repeat fixed top;size:6px;color:#FFFFFF">协议</font></span></td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td><a onclick="edit(this)" id="edit">修改</a>
				  <a onclick="delete()" id="del">删除</a>
				  <a onclick="save(this)" style="display:none" id="save">保存</a>
				  </td>
				</tr>
				<tr>
				  <td><span>单早&nbsp;&nbsp;&nbsp;&nbsp;<font  style="background: #BBFFEE url(bgimage.gif) no-repeat fixed top;size:6px;color:#0044BB;">现付</font></span></td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td>￥100</td>
				  <td><a onclick=")" >修改</a>
				  <a onclick="" >删除</a>
				  <a onclick="" style="display:none" id="save">保存</a>
				  </td>
				</tr>
		</tbody>
		
	</table>
  </div>
 </div>
</div>

<!-- 模态框 -->
<div id="model1"></div>

<script src="webpage/hotel/protocol/js/editHotelRooms.js?version=${globalVersion}"></script>
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 
