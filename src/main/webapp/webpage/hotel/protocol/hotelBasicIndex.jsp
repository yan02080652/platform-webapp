<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<link rel="stylesheet" href="${baseRes }/plugins/sco/css/scojs.css?version=${globalVersion}">

<ul id="myTab" class="nav nav-tabs" data-trigger="tab">
	<li><a  onclick="hotelBasicInformation('${hotelId}')">酒店信息</a></li>
	<li><a  onclick="editHotelRooms('${hotelId}')">房型信息</a></li>
	<li><a  onclick="editPricePlan('${hotelId}')">价格计划信息</a></li>
</ul>
<div class="pane-wrapper">
	<!-- 酒店基本信息 -->
	<div id="hotelBasicInformation">
	</div>
	<!-- 房型信息 -->
	<div id="editHotelRooms">
	</div>
	<!-- 房型信息 -->
	<div id="editPricePlan">
	</div>
</div>
<input id="f" type="hidden" value="${f}">
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 
<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tab.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tooltip.js?version=${globalVersion}"></script>
<script src="webpage/hotel/protocol/js/hotelBasicIndex.js?version=${globalVersion}"></script>