$(function(){
	reloadSerachHotel();
	
});

//分页查询
function reloadSerachHotel(pageIndex){
	var loading = layer.load();
	
	//加载列表
	var originType = $('#originType').val();
	var cityId = $('#citySelect').attr("cityId");
	var protocolComId = $('#queryBpId').val();
	var hotelName = $('#hotelName').val();
	$.ajax({
		url : 'hotel/protocol/serachHotel',
		data : {
			originType : originType,
			cityId : cityId,
			protocolComId : protocolComId,
			hotelName : hotelName,
			pageIndex : pageIndex
		},
		success : function(data) {
			layer.close(loading);
			$("#serach_table").html(data);
		},
		error: function() {
			layer.close(loading);
		}
		
	});
	
}

function toHoteBaseInfo(e){
	window.location.href="/hotel/protocol/toHoteRates?f="+e;
	
	
}
function toHoteRoomRates(e){
	window.location.href="/hotel/protocol/toHoteRates?f="+e;
	
	
}
function toAddHotel(){
	window.location.href="/hotel/protocol/toHoteRates";
}


function syncHotels(){
	var hotelId = $('#elongHotelId').val();
    var originType = $('#originType').val();
    if(originType == '0'){
    	layer.msg('请选择数据源',{icon: 2});
    	return;
	}
	if(hotelId == ''){
		layer.msg('请填写酒店 id',{icon: 2});
		return;
	}
    layer.confirm('是否同步酒店信息', {title: '提示'}, function (index) {
        $.ajax({
            url: '/hotel/protocol/syncHotels',
            type: 'post',
            data: {
                hotelId: hotelId,
                originType:originType
            },success: function(data){
            	if(data.code == '-1'){
                    layer.msg(data.msg,{icon:2});
				}else{
                    layer.msg("同步成功",{icon:1});
				}

            }
        })
    });
}

