var sub = true;
$(".btn-primary").click(function(){
	sub = false;
	$("#form-horizontal").submit();
});

$(document).ready(function() {
	
	
    $("#form-horizontal").validate({
        rules : {
            name:{required:true,maxlength:50},
            area:{required:true,maxlength:3},
            bedType:{required:true},
            broadnet:{required:true},
            capcity:{required:true},
            state:{required:true},
            floor:{}
        },
        messages:{
        	
        },
        submitHandler : function(form) {
            if(sub == true){
            	return;
            }
            sub = true;
			$.ajax({
				cache : true,
				type : "POST",
				url : "hotel/protocol/AddHouseType",
				data : $('#form-horizontal').serialize(),
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						layer.alert("添加成功");
						$('#form-horizontal')[0].reset();
					} else {
						showErrorMsg(data.txt);
					}
				}
			});    
			
        },
        errorPlacement : setErrorPlacement,
        success : validateSuccess,
        highlight : setHighlight
    });
});