$(function(){
	var f = $("#f").val() || 0;
	changeTab(f);
});

function changeTab(index){
	var $tab = $.scojs_tab('#myTab');
	$tab.select(index);
}

function hotelBasicInformation(hotelId){
	$.ajax({
		type:"post",
		url:"hotel/protocol/hotelBasicInformation",
		data:{
			hotelId:hotelId
		},
		success:function(data){
			$("#hotelBasicInformation").html(data);
		}
	});
	
}
//我的任务Tab
function editHotelRooms(hotelId){
	$.ajax({
		type:"post",
		url:"hotel/protocol/editHotelRooms",
		data:{
			hotelId:hotelId
		},
		success:function(data){
			$("#editHotelRooms").html(data);
		}
	});
}

function editPricePlan(hotelId){
	$.ajax({
		type:"post",
		url:"hotel/protocol/editPricePlan",
		data:{
			hotelId:hotelId
		},
		success:function(data){
			$("#editPricePlan").html(data);
		}
	});
}
