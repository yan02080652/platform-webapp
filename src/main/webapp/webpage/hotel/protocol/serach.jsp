<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
 <table class="table table-striped table-bordered table-hover" id="carrierTable">
	<thead>
		<tr>
          <th><label>数据源</label></th>
          <th><label>城市</label></th>
	      <th><label>酒店名称 </label></th>
          <th><label>状态</label></th>
          <th><label>操作</label></th>
        </tr>
     </thead>
	<tbody>
		<c:forEach items="${pageList.list }" var="hb">
			<tr>
			  <td>${hb.originTypeName}</td>
			  <td>${hb.cityName}</td>
			  <td>${hb.name}</td>
			  <td>
			  <c:choose>
			    <c:when test="${hb.hotelStatus eq 'CLOSE'}">
			      	    禁用
			    </c:when>
			    <c:otherwise>
			       	   启用
			    </c:otherwise>
			  </c:choose>
			  </td>
			  <td>
			    <c:if test="${hb.originType == 7}">
			    	<a href="" >修改</a>
			    	<a href="" >删除</a>
			    </c:if>
			    <a href="javaScript:void(0)" onclick="toHotelCopy()">复制</a>
			    <a href="javaScript:void(0)" onclick="syncSingleHotel(${hb.hotelId},'${hb.originType}')">同步</a>
			  </td>
		  </tr>
		</c:forEach> 
	</tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadSerachHotel"></jsp:include> 
</div>