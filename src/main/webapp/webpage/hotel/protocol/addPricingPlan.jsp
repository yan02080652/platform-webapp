<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<br/>
<br/>
<br/>


<form class="form-horizontal">
<div class="col-md-12">

   <div class="col-md-3">
		<div class="panel panel-default" >
		     <div class="panel-body">
		        <div class="form-group">
                 <h4 style="margin-left: 20px;">第一步：请选择试用的房型:</h4>
				</div>
		        <div class="panel panel-default" style="background:#EEEEEE">
		            <div class="panel-body">
		                 <div class="form-group">
						    <div class="checkbox">
								  <label>
								    <input type="checkbox" value="">
								   大床房
								  </label>
							 </div>
						  </div>
						  <div class="form-group">
						    <div class="checkbox">
								  <label>
								    <input type="checkbox" value="">
								    双床房
								  </label>
							 </div>
						  </div>
						  <div class="form-group">
						    <div class="checkbox">
								  <label>
								    <input type="checkbox" value="">
								   高级大床房
								  </label>
							 </div>
						  </div>
						  <div class="form-group">
						    <div class="checkbox">
								  <label>
								    <input type="checkbox" value="">
								   高级双床房
								  </label>
							 </div>
						  </div>
						  <div class="form-group">
						    <div class="checkbox">
								  <label>
								    <input type="checkbox" value="">
								   行政房
								  </label>
							 </div>
						  </div>
						  <div class="form-group">
						    <div class="checkbox">
								  <label>
								    <input type="checkbox" value="">
								   套房
								  </label>
							 </div>
						  </div>
					<div/>
		        </div>
		     </div>
	     </div>
	  </div>
	</div>  

    <div class="col-md-8">
		<div class="panel panel-default">
		     <div class="panel-body">
		        <div class="form-group">
                 <h4 style="margin-left: 90px;">第二步：请填写价格计划信息:</h4>
				</div>
		     <div class="form-group">
                 <label for="exampleInputName2" class="col-sm-3 control-label">价格计划名称：</label>
				    <div class="col-sm-6">
				      <input type="text" class="form-control" id="" placeholder="价格计划名称">
				    </div>
			  </div>
		     <div class="form-group">
		       <label for="exampleInputName2" class="col-sm-3 control-label">早餐份数：</label>
                 <div class="col-sm-6">
      	           <select class="form-control"  id="" name="" style="width: 158px">
						<option value="1">单早</opton>
						<option value="2" selected="selected">双早</opton>
						<option value="3">三早</opton>
			       </select>  
                </div>
		     </div>
		     
		     <div class="form-group">
		      <label for="exampleInputName2" class="col-sm-3 control-label">支付方式：</label>
				<div class="col-sm-6">
					<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">现付
					</label>
					<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">预付
					</label>
				</div>
			 </div>
			 
			 <div class="form-group">
		      <label for="exampleInputName2" class="col-sm-3 control-label">是否为协议价格：</label>
				<div class="col-sm-6">
					<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">是
					</label>
					<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">否
					</label>
				</div>
			 </div>
			 
			  <div class="form-group">
                 <label for="exampleInputName2" class="col-sm-3 control-label">协议企业：</label>
				    <div class="col-sm-6">
				      <input type="text" class="form-control" id="" placeholder="中文/拼音搜索">
				    </div>
			  </div>
			  
			   <div class="form-group">
                 <label for="exampleInputName2" class="col-sm-3 control-label">协议代码：</label>
				    <div class="col-sm-6">
				      <input type="text" class="form-control" id="" placeholder="协议代码">
				    </div>
			  </div>
			  
			   <div class="form-group" style="margin-left: 83px;">
				  <button type="submit" class="btn btn-primary" style="width:100px;heigth:15px">保存</button>
				  &nbsp;&nbsp;&nbsp;&nbsp;
				  <button type="submit" class="btn btn-default" style="width:100px;heigth:15px">取消</button>
				</div>
		     
		     
		  </div>
		</div>
	</div>

</div>
</form>
