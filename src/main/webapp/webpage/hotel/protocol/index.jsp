<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/plugins/city/cityselect.css?version=${globalVersion}">
<div class="panel panel-default" >
   <div class="panel-body">
	   <div class="form-group" style="margin-left: 30px;">
	   		<div class="form-inline">
	            		<label for="SupplierId" class="control-label" >数据源:</label>
	            		<div class="form-group">
		        	<select class="form-control"  id="originType" style="width: 150px">
	        	 	    <option value="0" selected="selected">不限</opton>
	        	 	    <c:forEach items="${originType}" var="o">
	        	 	        <option value="${o.message}" >${o.name}</opton>
	        	 	    </c:forEach>
					</select>
					</div>
				   <div class=" form-group"  style="margin-left: 24px;">
				     <label for="EnterprisesId" class="control-label" >城市:</label>
					 <div class="form-group">
		              <input type="text" class="cityinput form-control" style="width: 178px;padding-left: 10px;" id="citySelect" placeholder="请输入目的地">
					  </div>
			        </div>
			       
			       <div class=" form-group">
			            <input type="text" id="hotelName" style="margin-left: 24px;" class="form-control" placeholder="酒店名称" style="width:250px"/>
	               </div>
			       <div class="form-group">
			            <input type="hidden" id="queryBpId"  class="form-control"/>
			            <input type="text" id="queryBpName" style="margin-left: 24px;" onchange="changeBpName()" onclick="selectPartner()"  class="form-control" placeholder="协议企业" style="width:250px;"/>
	               </div>	
					
					<div class="form-group">
					    <button  class="btn btn-primary" style="width:100px;margin-left: 40px;" onclick="reloadSerachHotel()">搜索</button>
					</div>
			 </div>
		</div>
	    <div class="form-group form-inline" style="margin-left: 16px;">
	         <%--<button  class="btn btn-primary" style="width:100px;heigth:15px" onclick="toAddHotel()"><span class="glyphicon glyphicon-plus"></span>新增酒店</button>--%>
			<button  class="btn btn-primary" style="width:100px;heigth:15px;" onclick="syncHotels()"><span class="glyphicon glyphicon-plus"></span>同步酒店</button>
			<div class="form-group">
				<input type="text" class="form-control"   style="width: 300px;padding-left: 10px;" placeholder="酒店ID：可以多个,英文','分隔" id="elongHotelId">
			</div>
		</div>
	    <div class="col-md-10">
	       <div class="form-group" id="serach_table">
	        
	       </div>
	    </div>
    </div>
</div>

<script type="text/javascript" src="resource/plugins/city/cityselect.js?version=${globalVersion}"></script>
<script type="text/javascript" src="webpage/hotel/protocol/js/index.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(function(){
	new Vcity.CitySelector({input:'citySelect'});
});

function selectPartner(){
	TR.select('partner',{
		//type:0//固定参数
	},function(data){
		$("#queryBpId").val(data.id);
		$("#queryBpName").val(data.name);
	});
} 

function changeBpName(){
	if($("#queryBpName").val() == ""){
		$("#queryBpId").val("");
	}
}
</script>
