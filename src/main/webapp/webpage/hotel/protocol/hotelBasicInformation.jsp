<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/plugins/layer/skin/layui.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/bootstrap/fileinput.min.css?version=${globalVersion}">
<br/>
<br/>
<div class="col-md-10">
    <div class="panel panel-default">
       <form class="form-horizontal">
			<div class="form-group">
			   	<label for="exampleInputName2" class="col-sm-2 control-label">酒店名称：</label>
                <div class="col-sm-6">
                  <input type="text" class="form-control" name="name" value="${name }"  placeholder="酒店名称" ">
                </div>
	        </div>
            <div class="form-group">
		  		 <label for="exampleInputName2" class="col-sm-2 control-label">酒店地址：</label>
                 <div class="col-sm-8" style="margin-left: -13px;">
                     <div id="city">
	                       <div class="col-sm-3">
	                         <select type="text" class="form-control prov"  placeholder="省"></select>
	                        </div>
	                       <div class="col-sm-3">
	                       <select type="text" class="form-control city"  placeholder="市" disabled="disabled"></select>
	                        </div>
	                       <div class="col-sm-3">
	                       <select type="text" class="form-control dist"  placeholder="区/县" disabled="disabled"></select>
	                        </div>
                      </div>  
                      <div class="col-sm-3">
                      	 <input type="text" class="form-control" id="" placeholder="商圈">
                      </div>
                  </div>
            </div>
            <div class="form-group">   
              <div class="col-sm-2"></div>
               <div class="col-sm-8">
                  <input type="text" class="form-control" name="address" value="${address }" placeholder="详细地址" >
                </div>
            </div>   
            <div class="form-group"> 
                <label for="exampleInputName2" class="col-sm-2 control-label">酒店星级：</label>
		            <div class="col-sm-6">
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" value="0" > 经济/客栈
						</label>
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" value="3" > 三星/舒适
						</label>
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" value="4" > 四星/高档
						</label> 
						<label class="radio-inline">
						  <input type="radio" name="inlineRadioOptions" value="5" > 五星/豪华
						</label>
				   </div>
		    </div>
			<div class="form-group">
			    <label for="exampleInputName2" class="col-sm-2 control-label">酒店电话：</label>
			    <div class="col-sm-3">
			      <input type="text" class="form-control" name="phone" value="${phone }" placeholder="酒店电话" >
			    </div>
			     <label for="exampleInputName2" class="col-sm-2 control-label">酒店传真:</label>
			    <div class="col-sm-3">
			      <input type="text" class="form-control" name="fax" value="${fax }" placeholder="酒店传真" >
			    </div>
			 </div>
			 <div class="form-group">
			    <label for="exampleInputName2" class="col-sm-2 control-label">开业时间：</label>
			    <div class="col-sm-3">
			       <input  id="startDate3" class="form-control" type="text" name="establishmentDate" value="${establishmentDate }" onclick="WdatePicker()" placeholder="开业时间"/>
			    </div>
			    <label for="exampleInputName2" class="col-sm-2 control-label">装修时间:</label>
			    <div class="col-sm-3">
			      <input  id="startDate3" class="form-control" type="text" name="renovationDate" value="${renovationDate }" onclick="WdatePicker()" placeholder="装修时间"/>
			    </div>
			 </div>
			 <div class="form-group">
			    <label for="exampleInputName2" class="col-sm-2 control-label">集团：</label>
			    <div class="col-sm-3">
			      <input type="text" class="form-control" id="" placeholder="集团">
			    </div>
			    <label for="exampleInputName2" class="col-sm-2 control-label">品牌:</label>
			    <div class="col-sm-3">
			      <input type="text" class="form-control" id="" placeholder="品牌" >
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputName2" class="col-sm-2 control-label">经度：</label>
			    <div class="col-sm-3">
			      <input type="text" class="form-control" name="longitude" value="${longitude }" placeholder="经度">
			    </div>
			    <label for="exampleInputName2" class="col-sm-2 control-label">维度:</label>
			    <div class="col-sm-3">
			      <input type="text" class="form-control" name="latitude" value="${latitude }" placeholder="维度" >
			    </div>
			  </div>
  
			 <div class="form-group">
			  <label for="exampleInputName2" class="col-sm-2 control-label">主要设施：</label>	
			   <div class="col-sm-6">	 
			     <label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox1" value="option1"  > 免费wifi
				</label>
				<label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox2" value="option2"> 收费wifi
				</label>
				<label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox3" value="option3"> 免费宽带
				</label>
				  <label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox1" value="option1"> 收费宽带
				</label>
				<label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox2" value="option2"> 免费停车
				</label>
				<label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox3" value="option3"> 收费停车
				</label> 
				  <label class="checkbox-inline" style="margin-left: 0px;">
				  <input type="checkbox" id="inlineCheckbox1" value="option1"> 免费接机
				</label>
				<label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox2" value="option2"> 收费接机
				</label>
				<label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox3" value="option3"> 餐厅
				</label>
				  <label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox1" value="option1"> 健身房
				</label>
				<label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox2" value="option2"> 游泳池
				</label>
				<label class="checkbox-inline">
				  <input type="checkbox" id="inlineCheckbox3" value="option3"> 会议室
				</label>
		       </div>       
			 </div>
			 <div class="form-group">
				   <label for="exampleInputName2" class="col-sm-2 control-label">支持的信用卡：</label>	
				   <div class="col-sm-8">	 
					     <label class="checkbox-inline">
						  <input type="checkbox" id="inlineCheckbox1" value="option1"> 万事达（Master）
						</label>
						<label class="checkbox-inline">
						  <input type="checkbox" id="inlineCheckbox2" value="option2"> VISA
						</label>
						<label class="checkbox-inline">
						  <input type="checkbox" id="inlineCheckbox3" value="option3"> JCB
						</label>
						  <label class="checkbox-inline">
						  <input type="checkbox" id="inlineCheckbox1" value="option1"> 银联（UnionPay）
						</label>
						<label class="checkbox-inline">
						  <input type="checkbox" id="inlineCheckbox2" value="option2"> 大菜Diners
						</label>
						<label class="checkbox-inline">
						  <input type="checkbox" id="inlineCheckbox3" value="option3"> 运通（AEC）
						</label> 
		      		</div>       
			 </div>
	 
			 <div class="form-group">
			     <label for="exampleInputName2" class="col-sm-2 control-label">酒店介绍：</label>
			   <div class="col-sm-8">	 
			     <textarea class="form-control" name="introduce" id="hotelIntroduce" rows="3" ></textarea>
			   </div>
			  </div>
	  
			  <div class="form-group">
			     <label for="exampleInputName2" class="col-sm-2 control-label">酒店状态：</label>
			   <div class="col-sm-8">	 
			       <label class="radio-inline">
					 <input type="radio" name="hotelStatus" <c:if test="${hotelStatus eq 'OPEN' }"> checked="checked" </c:if> value="OPEN"> 启动
		           </label>
				   <label class="radio-inline">
					 <input type="radio" name="hotelStatus" <c:if test="${hotelStatus eq 'CLOSE' }"> checked="checked" </c:if> value="CLOSE"> 禁用
				   </label> 
			   </div>
			  </div>
			  <div class="form-group">
			    <a class="btn btn-primary" class="col-sm-2 control-label" onclick="" role="button" style="margin-left: 100px;width:150px;">保存</a>
			  </div>
       </form>
    </div>
</div>
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 
<script type="text/javascript" src="resource/plugins/cityCascade/jquery.cityselect.js?version=${globalVersion}"></script>  
<script>
$("#city").citySelect({  
    url:"resource/plugins/cityCascade/city.min.js",  
    prov:"", //省份 
    city:"", //城市 
    dist:"", //区县 
    nodata:"none" //当子集无数据时，隐藏select 
});
</script>