<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<!-- 任务列表 -->
<br/>
<div class="panel panel-default"> 
<div class="panel-body">
  
  <div class="pull-left col-sm-10" style="margin-bottom: 10px;">
	 <a class="btn btn-primary" onclick="toAddHouseType()" role="button">新增房型</a>
  </div>
  <div class="col-sm-10">
	<table class="table table-striped table-bordered table-hover" id="carrierTable">
		<thead>
		<tr>
          <th><label>房型名称</label></th>
	      <th><label>床型</label></th>
          <th><label>窗户</label></th>
          <th><label>平均面积(㎡)</label></th>
          <th><label>楼层</label></th>
          <th><label>可住人数</label></th>
          <th><label>状态</label></th>
          <th><label>操作</label></th>
        </tr>
    </thead>
	<tbody id="myTaskTbody">
		
	</tbody>
		
	</table>
  </div>
 </div>
</div>

<!-- 模态框 -->
<div id="model1"></div>

<script src="webpage/hotel/protocol/js/editHotelRooms.js?version=${globalVersion}"></script>
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 
