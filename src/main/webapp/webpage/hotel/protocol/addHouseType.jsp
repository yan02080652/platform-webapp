 <%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/plugins/layer/skin/layui.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/bootstrap/fileinput.min.css?version=${globalVersion}">

<script src="/webpage/hotel/protocol/js/addHouseType.js?version=${globalVersion}" type="text/javascript"/>

<!-- 状态栏 -->
<!-- 引用自return "hotel/tmc/index.base"; -->
<style>
.roomType{
  width:300px;
}
</style>

 <br/>
 <br/>
	

<div class="col-md-10">
<div class="panel panel-default">
<form class="form-horizontal" method="post" id="form-horizontal">
<br/>
 <br/>
  <div class="form-group">
    <label for="exampleInputName2" class="col-sm-2 control-label">房&nbsp;&nbsp;型&nbsp;名&nbsp;称：</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" id="" placeholder="房型名称" name="name">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputName2" class="col-sm-2 control-label">面积（m²）：</label>
    <div class="col-sm-2">
      <input type="text" class="form-control" id="" placeholder="面积" name="area">
    </div>
    <label for="exampleInputName2" class="col-sm-2 control-label">楼层（层）：</label>
    <div class="col-sm-2">
      <input type="text" class="form-control" id="" placeholder="楼层" name="floor">
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputName2" class="col-sm-2 control-label">床&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;型：</label>
    <div class="col-sm-2">
		   <select class="form-control"  id="" name="bedType" style="width: 158px">
					<option value="" selected="selected">请选择床型</opton>
					<option value="1" >大床</opton>
					<option value="2" >双床</opton>
					<option value="3" >单人床</opton>
					<option value="4" >三张床</opton>
					<option value="5" >水床</opton>
					<option value="6" >圆床</opton>
					<option value="7" >榻榻米</opton>
					<option value="7" >上下铺</opton>
					<option value="7" >其他</opton>
					
		   </select> 
    </div>
    <label for="exampleInputName2" class="col-sm-2 control-label">上&nbsp;&nbsp;网&nbsp;情&nbsp;况：</label>
    <div class="col-sm-2">
	        <select class="form-control"  id="" name="broadnet" style="width: 158px">
					<option value="" selected="selected">请选择上网情况</opton>
					<option value="1" >无</opton>
					<option value="2" >免费wifi</opton>
					<option value="3" >收费wifi</opton>
					<option value="4" >免费宽带</opton>
					<option value="5" >收费宽带</opton>
			 </select> 
    </div>
  </div>
  <div class="form-group">
    <label for="exampleInputName2" class="col-sm-2 control-label">窗&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;户：</label>
    <div class="col-sm-2">
      	        <select class="form-control"  id="" name="isWindow" style="width: 158px">
					<option value="" selected="selected">请选择是否有窗</opton>
					<option value="1" >有窗</opton>
					<option value="2" >无窗</opton>
					<option value="3" >内窗</opton>
					<option value="4" >部分有窗</opton>
			    </select> 
    </div>
    <label for="exampleInputName2" class="col-sm-2 control-label">可&nbsp;&nbsp;住&nbsp;人&nbsp;数：</label>
    <div class="col-sm-2">
      	      <select class="form-control"  id="" name="capcity" style="width: 158px">
					<option value="" selected="selected">请选择可住人数</opton>
					<option value="1" >1</opton>
					<option value="2" >2</opton>
					<option value="3" >3</opton>
					<option value="4" >4</opton>
					<option value="5" >5</opton>
					<option value="6" >6</opton>
					<option value="7" >7</opton>
					<option value="8" >8</opton>
					<option value="9" >9</opton>
					<option value="10">10</opton>
			 </select> 
    </div>
  </div>
<div class="form-group">
    <label for="exampleInputName2" class="col-sm-2 control-label">加&nbsp;&nbsp;床&nbsp;规&nbsp;则：</label>
    <div class="col-sm-2">
      	      <select class="form-control"  id="" name="addBedRule" style="width: 158px">
					<option value="" selected="selected">请选择</opton>
					<option value="1" >不可加床</opton>
					<option value="2" >可加床</opton>
			 </select> 
    </div>
    <label for="exampleInputName2" class="col-sm-2 control-label">状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态：</label>
    <div class="col-sm-2">
      	       <select class="form-control"  id="" name="state" style="width: 158px">
					<option value="1" selected="selected">开启</opton>
					<option value="2">关闭</opton>
			 </select>  
    </div>
  </div>
  <br/>
  <div class="form-group" style="margin-left: 83px;">
  <button type="submit" class="btn btn-primary" style="width:100px;heigth:15px">保存</button>
  &nbsp;&nbsp;&nbsp;&nbsp;
  <button type="submit" class="btn btn-default" style="width:100px;heigth:15px">取消</button>
  </div>
  <br/>
 <br/>
</form>
</div>
</div>

	
