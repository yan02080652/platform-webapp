<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" id="ruleTable">
	<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectAllRule()" id="selectAllRule" /></th>
          <th><label>模板名称</label></th>
	      <th><label>产品类型配置 </label></th>
		 <th><label>过滤非立即确认的产品</label></th>
		 <th><label>预订服务费</label></th>
		 <th><label>退房服务费</label></th>
		 <th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="bean">
         <tr class="odd gradeX">
        	 <td><input type="checkbox" name="ids" value="${bean.id }"/></td>
             <td>${bean.name }</td>
        	 <td>
	        	${bean.products }
        	 </td>
        	  <td>${bean.filterImmediatelyConfirm ? '是' : '否'}</td>
        	  <td>${bean.bookingFee }</td>
        	  <td>${bean.refundFee }</td>
             <td class="text-center" >
             	<a onclick="getRule('${bean.id }')" >修改</a> |
             	<a onclick="deleteRule('${bean.id }');">删除</a> 
             	<%-- <a onclick="relateCompany('${bean.id }');">关联企业</a> --%>
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadPriceRuleTemplate"></jsp:include> 
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>