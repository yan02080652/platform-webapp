<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="ruleMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>酒店数据聚合&nbsp;&nbsp;
			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="form-inline col-sm-10">
											 <ul id="myTab" class="nav nav-tabs">
												<li>
													<a href="javascript:void(0);" path="own-city">
														城市聚合
													</a>
												</li>
												<li>
													<a href="javascript:void(0);" path="own-geo">
														商圈/行政区聚合
													</a>
												</li>
							                    <li>
							                        <a href="javascript:void(0);" path="own-brand">
							                            品牌聚合
							                        </a>
							                    </li>
							                    <li>
							                        <a href="javascript:void(0);" path="own-hotel">
							                            酒店聚合
							                        </a>
							                    </li>
							                    <li>
							                        <a href="javascript:void(0);" path="own-fac">
							                            酒店设施聚合
							                        </a>
							                    </li>
												 <li>
													 <a href="javascript:void(0);" path="own-roomfac">
														房型设施聚合
													 </a>
												 </li>
												 <li>
													 <a href="javascript:void(0);" path="mateRule">
														其他规则
													 </a>
												 </li>
							                </ul>
										</div>
									</div>
									<div  id="rule_Table" style="margin-top: 15px">

					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div id="ruleListDiv"></div>


<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>

<script>
$(function(){
    $("#myTab a:eq(0) ").click();
})
function errorMsg(ele, msg) {
    layer.tips(msg, ele, {
        tips: [1, '#3595CC'],
        time: 4000
    });
};

$("#myTab a").click(function(){
	var path = $(this).attr('path');
	var url = "/hotel/ownData/" + path;
    $("#myTab li").removeClass("active");
    $(this).parent().addClass("active");
	$.get(url,function(data){
		$("#rule_Table").html(data);

	})

})
</script>