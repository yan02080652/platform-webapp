<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" style="table-layout:fixed" >
    <thead>
    <tr>
        <th width="10%"><label>类型</label></th>
        <th width="10%"><label>聚合编码</label></th>
        <th width="10%"><label>酒店名称</label></th>
        <th width="10%"><label>城市</label></th>
        <th width="10%"><label>酒店地址</label></th>
        <th width="10%"><label>酒店电话</label></th>
        <th width="10%"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean" varStatus="status">
        <tr class="odd gradeX" style=<c:choose>
            <c:when test="${status.index eq 0}">'background:#D3F1F8'</c:when>
            <c:otherwise></c:otherwise>
        </c:choose>>
            <td><c:choose>
                <c:when test="${status.index eq 0}">待匹配数据</c:when>
                <c:otherwise>疑似数据</c:otherwise>
            </c:choose></td>
            <td>${bean.ownId}</td>
            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"><div title="${bean.ownName}">${bean.ownName}</div></td>
            <td>${bean.ownCityName}</td>
            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;"><div title="${bean.ownAdress}">${bean.ownAdress}</div></td>
            <td>${bean.ownPhone}</td>
            <td>
                <c:if test="${status.index gt 0}">
                    <a href="javascript:void(0);" onclick="addOwn(${bean.id})">关联</a>
                    <a href="javascript:void(0);" onclick="removeSuspect(${bean.id})">解除</a>
                </c:if>
            </td>
        </tr>
    </c:forEach>

    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=search_suspect"></jsp:include>
</div>
<script type="text/javascript">
    //checkbox全选居中，固定宽度
    $("table tr").each(function(){
        $(this).find(":first").attr("style","text-align: center;width:100px");
    })
    /**
     * 添加聚合
     * @param id
     * @param type
     */
    function addOwn(id){
        $.ajax({
            url:'/hotel/ownData/addOwn',
            type:'post',
            data:{
                id:id
            },success:function(data){
                if(data.success){
                    layer.msg("操作成功",{icon: 1});
                    search_suspect();
                    search_hotel_refresh();
                }else{
                    layer.msg("确认失败:" + data.msg,{icon: 2});
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2});
            }
        })
    }

    /**
     * 解除疑似聚合关系
     * @param id
     */
    function removeSuspect(id){
        $.ajax({
            url:'/hotel/ownData/removeSuspect',
            type:'post',
            data:{
                id:id
            },success:function(data){
                if(data.success){
                    layer.msg("操作成功",{icon: 1});
                    search_suspect();
                    search_hotel_refresh();
                    backOwn();
                }else{
                    layer.msg("解除失败:" + data.msg,{icon: 2});
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2});
            }
        })
    }

</script>
