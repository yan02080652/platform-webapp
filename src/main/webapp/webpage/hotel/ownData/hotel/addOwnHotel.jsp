<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="modal fade" id="mode_own_hotel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">设置聚合关系</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="detailForm" role="form">
                    <input type="hidden" value="${id}" id="origin_hotel_id" />
                    <nav class="text-left">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">所属城市：</label>
                            <div class="col-sm-2">
                                <input id="districtName" class="form-control"  readonly="true" onclick="chooseDistrictData()"  value="${cityName}"/>
                                <input type="hidden" id="own_city_ids" value="${ownCityId}"/>
                            </div>
                            <label class="col-sm-2 control-label" >关键字搜索：</label>
                            <div class="col-sm-2">
                                <input class="form-control" id="hotelName" placeholder="聚合酒店名称/聚合酒店地址" />
                            </div>
                            <div class="col-sm-1">
                                <button type="button" class="btn btn-default" onclick="searchHotel(1);">搜索</button>
                            </div>
                            <div class="col-sm-2">
                                <button type="button" class="btn btn-primary control-label" onclick="addHotelToOwn()">+设为新聚合酒店</button>
                            </div>
                        </div>
                    </nav>
                    <div id="own_hotel_table">

                    </div>
                </form>
            </div>
           <%-- <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary" onclick="addHotelToOwn()">设置为聚合酒店</button>
            </div>--%>
        </div>
    </div>


</div>
<script type="text/javascript">
    function searchHotel(pageIndex){
        var index = layer.load();
        $.ajax({
            url: '/hotel/ownData/loadOwnHotelData',
            type: 'post',
            data: {
                hotelName: $('#hotelName').val(),
                ownCityIds:$('#own_city_ids').val(),
                origin_hotel_id:$('#origin_hotel_id').val(),
                pageIndex: pageIndex,
                pageSize: 10
            },success: function(data){
                $('#own_hotel_table').html(data);
                layer.close(index);
            }
        })
    }

    //关联酒店
    function relate(ownId){
        var hotelId = $('#origin_hotel_id').val();
        layer.confirm("确认关联此酒店?",function(){
            $.ajax({
                url: '/hotel/ownData/relateHotel',
                type: 'post',
                async: false,
                data: {
                    ownId: ownId,
                    hotelId: hotelId
                },success: function(data){
                    if(data.success){
                        layer.msg("操作成功: " + data.msg,{icon: 1});
                        search_hotel_refresh();
                        $('#mode_own_hotel').modal('hide');
                        $('#ownHotelDetail').hidden();

                    }else{
                        layer.msg("操作失败: " + data.msg,{icon: 2})
                    }
                },error: function(){
                    layer.msg("系统异常",{icon: 2})
                }
            })
        })
    }

    //设置为聚合酒店
    function addHotelToOwn(){
        var hotelId = $('#origin_hotel_id').val();
        $.ajax({
            url: '/hotel/ownData/addHotelToOwn',
            type: 'post',
            async: false,
            data: {
                hotelId: hotelId
            },success: function(data){
                if(data.success){
                    layer.msg("操作成功: " + data.msg,{icon: 1})
                    setTimeout(refresh,1000);
                }else{
                    layer.msg("操作失败: " + data.msg,{icon: 2})
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2})
            }
        })
    }

    var getDistrictDataChoosedData = JSON.parse('${city}'||'[]');
    function chooseDistrictData() {

        new choose.districtData({
            id:'selMultiDistrictData',//给id可以防止一直新建对象
            empty:false,
            multi:true,
            width: 500,
            getChoosedData:function () {
                return getDistrictDataChoosedData;
            }
        },function (data) {
            getDistrictDataChoosedData = [];
            var districtDesc ='';
            var districtIds ='';
            if (data) {
                $(data).each(function (i, item) {
                    getDistrictDataChoosedData.push({id: item.id, nameCn: item.nameCn});
                    if (i <= 2) {
                        districtDesc += item.nameCn+",";
                    }
                    if (i == 3) {
                        districtDesc = districtDesc.substr(0, districtDesc.length - 1) + '  ...';
                    }
                    if(i == 0){
                        districtIds += item.id;
                    }else{
                        districtIds +=","+item.id;
                    }
                });
                $("#own_city_ids").val(districtIds);
                $("#districtName").val(districtDesc);
            }

        });
    }

    function refresh(){
        search_hotel_refresh();
        $('#mode_own_hotel').modal('hide');
        $('#ownHotelDetail').hidden();

    }
</script>