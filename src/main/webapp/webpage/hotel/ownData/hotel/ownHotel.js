$(function(){
    $('#originType').multiselect({
        enableClickableOptGroups: true,
        numberDisplayed:5,
        buttonWidth: '100px',
        allSelectedText:'全部',
        includeSelectAllOption: true,
        selectAllText: '全部',
        nonSelectedText:'全部'
    });
    initOriginType();
    search_hotel(1);
})

function initOriginType(){
    var array = new Array();
    $('#originType option').each(function(){
        array.push($(this).val());
    })
    $('#originType').multiselect('select',array);
}

//查询
function search_hotel(pageIndex){
    var index = layer.load();
    $.ajax({
        url: '/hotel/ownData/hotelList',
        type: 'post',
        data: {
            ownId: $('#ownId').val(),
            hotelId: $('#hotelId').val(),
            keyword:$('#keyword').val(),
            matchType: $('#matchType').val(),
            originType: $('#originType').val().join(','),
            districtType:$('#districtType').val(),
            cityIds:$('#cityIds').val(),
            pageIndex: pageIndex,
            pageSize: 10
        },success: function(data){
            layer.close(index);
            $('#own_list').html(data);
        },error: function(){
            layer.close(index);
            layer.msg("系统异常",{icon: 2})
        }

    })
}

function search_hotel_refresh(){
    search_hotel($('#own_list  .cur').text());
}
function reset_hotel(){
    $('#ownId').val('');
    $('#hotelId').val('');
    $('#keyword').val('');
    $('#matchType').val('1');
    initOriginType();
    $('#sysDistrictName').val('');
    $('#cityIds').val('');
    $('#districtType').val('');
    search_hotel(1);
}

//脱离聚合关系
function awayFormOwn(id){
    layer.confirm("确认解除聚合?",function(){
        $.ajax({
            url: '/hotel/ownData/awayHotel',
            type: 'post',
            async:false,
            data: {
                id: id
            },success: function(data){
                if(data.success){
                    layer.msg("操作成功",{icon: 1});
                    search_own_hotel_list();
                    search_hotel_refresh();
                }else{
                    layer.msg("解除失败:" + data.msg,{icon: 2});
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2})
            }
        })
    })

}

//增加聚合酒店
function addOwnHotel(id){
    $.ajax({
        url: '/hotel/ownData/addOwnHotel',
        type: 'post',
        data: {
            id: id
        },success: function(data){
            $('#ownHotelDetail').html(data);
            $('#ownHotelDetail').show();
            $('#mode_own_hotel').modal();
        }
    })
}

//显示疑似聚合列表
function forwordSuspectList(originId,type){
    $.post("hotel/ownData/hotelSuspectList",{originId:originId,type:type},function(data){
        $("#ruleListDiv").html(data);
        $("#ruleMain").hide();
        $("#ruleListDiv").show();
    });
}
//显示聚合酒店列表
function forwordOwnList(id){
    $.post("hotel/ownData/hotelOwnList",{id:id},function(data){
        $("#ruleListDiv").html(data);
        $("#ruleMain").hide();
        $("#ruleListDiv").show();
    });
}
//返回
function backOwn(){
    $("#ruleMain").show();
    $("#ruleListDiv").hide();
}

var getSysDistrictDataChoosedData = JSON.parse('[]');
function chooseSysDistrictData() {

    new choose.districtData({
        id:'selMultiDistrictData',//给id可以防止一直新建对象
        empty:false,
        multi:true,
        width: 500,
        getChoosedData:function () {
            return getSysDistrictDataChoosedData;
        }
    },function (data) {
        getSysDistrictDataChoosedData = [];
        var districtDesc ='';
        var districtIds ='';
        if (data) {
            $(data).each(function (i, item) {
                getSysDistrictDataChoosedData.push({id: item.id, nameCn: item.nameCn});
                if(i == 0){
                    districtDesc += item.nameCn;
                } else if (i <= 2) {
                    districtDesc += ","+item.nameCn;
                }
                if (i == 3) {
                    districtDesc = districtDesc + '  ...';
                }
                if(i == 0){
                    districtIds += item.id;
                }else{
                    districtIds +=","+item.id;
                }
            });
            $("#cityIds").val(districtIds);
            $("#sysDistrictName").val(districtDesc);
        }

    });
}