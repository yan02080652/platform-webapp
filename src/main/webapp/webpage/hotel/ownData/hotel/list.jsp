<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" style="table-layout:fixed" >
    <thead>
    <tr>
        <th width="10%"><label>聚合编码</label></th>
        <th width="20%"><label>酒店名称</label></th>
        <th width="10%"><label>来源渠道</label></th>
        <th width="10%"><label>渠道编码</label></th>
        <th width="10%"><label>城市</label></th>
        <th width="20%"><label>酒店地址</label></th>
        <th width="10%"><label>酒店电话</label></th>
        <th width="10%"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr class="odd gradeX">
            <td>
                <c:if test="${bean.ownId eq -1}">疑似聚合</c:if>
                <c:if test="${bean.ownId gt 0}">${bean.ownId}</c:if>
                <c:if test="${bean.ownId eq 0}">未聚合</c:if>
            </td>
            <td style="word-break:break-all;">${bean.name}</td>
            <td>${bean.origin.name}</td>
            <td>${bean.hotelId}</td>
            <td>${bean.cityName}</td>
            <td style="word-break:break-all;">${bean.address}</td>
            <td>${bean.phone}</td>
            <td>
                <c:choose>
                    <c:when test="${condition.matchType eq 3}">
                        <a href="javascript:void(0);" onclick="forwordSuspectList(${bean.id},1)">设置</a>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${bean.ownId gt 0}">
                            <a href="javascript:void(0);" onclick="forwordOwnList(${bean.id})">编辑</a>
                        </c:if>
                        <c:if test="${bean.ownId eq 0}">
                            <a href="javascript:void(0);" onclick="addOwnHotel(${bean.id})">设置</a>
                        </c:if>
                    </c:otherwise>
                </c:choose>

            </td>
        </tr>
    </c:forEach>

    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=search_hotel"></jsp:include>
</div>
<script type="text/javascript">
    //checkbox全选居中，固定宽度
    $("table tr").each(function(){
        $(this).find(":first").attr("style","text-align: center;width:100px");
    })
</script>