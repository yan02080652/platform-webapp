<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>编辑&nbsp;&nbsp;
                <a data-pjax="" onclick="backOwn()">返回</a>
            </div>
            <div id="rightDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <input type="hidden" id="id" value="${id}">
                                <div id="own_hotel_list" style="margin-top: 15px">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        search_own_hotel_list();
    })
    //checkbox全选居中，固定宽度
    $("table tr").each(function(){
        $(this).find(":first").attr("style","text-align: center;width:100px");
    })

    /**
     * 查询
     */
    function search_own_hotel_list(pageIndex){
        var index=layer.load();
        $.ajax({
            url:'/hotel/ownData/hotelOwnTable',
            type:'post',
            async:false,
            data:{
                id:$("#id").val(),
                pageIndex:pageIndex
            },success:function(data){
                layer.close(index);
                $('#own_hotel_list').html(data);
            },error:function(){
                layer.close(index);
                layer.msg("系统异常",{icon: 2});
            }
        })
    }
</script>
