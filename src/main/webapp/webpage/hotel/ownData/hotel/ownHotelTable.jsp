<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="width:10%;"><label>类型</label></th>
        <th style="width:10%;"><label>聚合编码</label></th>
        <th style="width:10%;"><label>所属城市</label></th>
        <th style="width:20%;"><label>酒店名称</label></th>
        <th style="width:20%;"><label>酒店地址</label></th>
        <th style="width:10%;"><label>酒店电话</label></th>
        <th style="width:10%;"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean" varStatus="status">
        <tr style=<c:choose>
        <c:when test="${status.index eq 0}">"background:#D3F1F8"</c:when>
            <c:otherwise></c:otherwise>
                </c:choose>>
            <td>
                <c:choose>
                    <c:when test="${status.index eq 0}">待匹配数据</c:when>
                    <c:otherwise>聚合数据</c:otherwise>
                </c:choose>
            </td>
            <td>
                <c:choose>
                    <c:when test="${bean.id eq 0 }"></c:when>
                    <c:otherwise>${bean.id}</c:otherwise>
                </c:choose>
            </td>
            <td>${bean.cityName}</td>
            <td>${bean.name}</td>
            <td>${bean.address}</td>
            <td>${bean.phone}</td>
            <td>
                <c:if test="${bean.id gt 0 }">
                    <a href="javascript:void(0);" onclick="relate(${bean.id});">关联</a>
                </c:if>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=searchHotel"></jsp:include>
</div>