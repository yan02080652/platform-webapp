<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" type="text/css" src="/resource/css/bootstrap/bootstrap-multiselect.css" />
<script type="text/javascript" src="/resource/js/bootstrap/bootstrap-multiselect.js?version=${globalVersion}"></script>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
<%--                        <div class="pull-left">
                            &lt;%&ndash;<a class="btn btn-default" onclick="synDistrictData()" role="button">手动更新城市</a>&ndash;%&gt;
                        </div>--%>
                        <!-- 查询条件 -->
                        <form:form modelAttribute="ownHotelCondition" cssClass="form-inline" method="post">
                            <nav class="text-right">
                                <div class=" form-group">
                                    <label class="control-label">关键字：</label>
                                    <form:input path="keyword" cssClass="form-control" style="width:80px"/>
                                </div>
                                <div class=" form-group">
                                    <label class="control-label">聚合编码：</label>
                                    <form:input path="ownId" cssClass="form-control" style="width:80px"/>
                                </div>
                                <div class=" form-group">
                                    <label class="control-label">渠道编码：</label>
                                    <form:input path="hotelId" cssClass="form-control" style="width:80px" />
                                </div>

                                <div class="form-group">
                                    <label class="control-label">所属城市：</label>
                                        <input id="sysDistrictName" Class="form-control" readonly="true" style="width:80px" onclick="chooseSysDistrictData()" />
                                        <form:hidden path="cityIds"/>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">渠道来源：</label>
                                    <form:select path="originType" cssClass="form-control" multiple="multiple" style="display:none;">
                                        <form:options items="${originTypes}" itemLabel="name" itemValue="message"/>
                                    </form:select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">城市类型：</label>
                                    <form:select path="districtType" cssClass="form-control">
                                        <form:option value="">全部</form:option>
                                        <form:options items="${districtTypes}" itemLabel="message" itemValue="code"/>
                                    </form:select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">聚合状态：</label>
                                    <form:select path="matchType" cssClass="form-control">
                                        <form:option value="1">已聚合</form:option>
                                        <form:option value="2">未聚合</form:option>
                                        <form:option value="3">疑似聚合</form:option>
                                    </form:select>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-default" onclick="search_hotel(1)">查询
                                    </button>
                                    <button type="button" class="btn btn-default" onclick="reset_hotel()">重置</button>
                                </div>
                            </nav>
                        </form:form>
                    </div>
                </div>
                <div id="own_list" style="margin-top: 15px">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- 详细页面 -->
<div id="ownHotelDetail" style="display:none;"></div>
<script src="/webpage/hotel/ownData/hotel/ownHotel.js"/>