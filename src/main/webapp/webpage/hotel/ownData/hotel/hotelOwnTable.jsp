<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="width:10%;"><label>聚合编码</label></th>
        <th style="width:20%;"><label>酒店名称</label></th>
        <th style="width:10%;"><label>来源渠道</label></th>
        <th style="width:10%;"><label>渠道酒店编码</label></th>
        <th style="width:10%;"><label>城市</label></th>
        <th style="width:20%;"><label>酒店地址</label></th>
        <th style="width:10%;"><label>酒店电话</label></th>
        <th style="width:10%;"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean" varStatus="status">
        <tr class="odd gradeX" style=<c:choose>
                <c:when test="${status.index eq 0}">'background:#D3F1F8'</c:when>
                <c:otherwise></c:otherwise>
        </c:choose>>
            <td>${bean.ownId}</td>
            <td>${bean.name}</td>
            <td>${bean.origin.name}</td>
            <td>${bean.hotelId}</td>
            <td>${bean.cityName}</td>
            <td>${bean.address}</td>
            <td>${bean.phone}</td>

            <td>
                <a href="javascript:void(0);" onclick="awayFormOwn(${bean.id});">解除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=search_own_hotel_list"></jsp:include>
</div>