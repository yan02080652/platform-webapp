
function validateRoomResult(){
	if($("#name").val()==''){
		errorMsg($("#name"), '显示名称必填!');
        return false;
	}
	if($("#mateRule").val()==''){
		errorMsg($("#mateRule"), '匹配规则');
        return false;
	}
	return true;
}
$(function(){
	reloadMateList();
})


$("#mateTypeTag").change(function(){
		reloadMateList();
})

function addMateRule(){
    $.ajax({
        url: '/hotel/ownData/addMateRule',
        type: 'post',
        data: {
            mateType: $('#mateTypeTag').val()
        },success: function(data){
            $('#mateRuleDetail').html(data);
            $('#mateRuleDetail').show();
            $('#mode_mateRuleDetail').modal();
        }
    })
}

function submitMateRule(){
   if(!validateRoomResult()) {
    return;
   }
   $.ajax({
        cache : true,
        type : "POST",
        url : "hotel/ownData/saveMateRule",
        data : $('#mateRuleForm').serialize(),
        async : false,
        error : function(request) {
            $.alert("请求失败，请刷新重试","提示");
        },
        success : function(data) {
        	
            if (data.type == 'success') {
            	$.alert(data.txt,"提示");
            	$("#name").val("");
            	$("#mateRule").val("");
            } else {
                $.alert(data.txt,"提示");
            }
            reloadMateList();
            $('#mode_mateRuleDetail').modal('hide');
            $('#mateRuleDetail').hidden();


        }
    });
}


function reloadMateList(pageIndex){
	$.post('hotel/ownData/searchMate',{
        pageIndex:pageIndex,
        mateType:$("#mateTypeTag").val()
    },function(data){
        $("#mateRuleList").html(data);
    });
}

function deleteRule(id){
	$.post('hotel/ownData/deleteMate',{
		id:id
	},function(data){
		 if (data.code == '0') {
         	 $.alert(data.msg,"提示");
         } else {
             $.alert(data.txt,"提示");
         }
		 reloadMateList();
	})
}

function updateStatus(id,status){
	$.post('hotel/ownData/saveMateRule',{
		id:id,
		status:status
	},function(data){
		 if (data.type == 'success') {
			 if(status==true){
				 $.alert("已启用","提示");
			 }else{
				 $.alert("已禁用","提示");
			 }
         } else {
             $.alert("状态异常","提示");
         }
		 reloadMateList();
	})
}


