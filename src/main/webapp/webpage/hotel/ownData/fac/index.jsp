<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" type="text/css" src="/resource/css/bootstrap/bootstrap-multiselect.css" />
<script type="text/javascript" src="/resource/js/bootstrap/bootstrap-multiselect.js?version=${globalVersion}"></script>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <%--<a class="btn btn-default" onclick="synDistrictData()" role="button">手动更新城市</a>--%>
                        </div>
                        <!-- 查询条件 -->
                        <form:form modelAttribute="ownHotelFacilitiesCondition" cssClass="form-inline" method="post">
                            <form:hidden path="amenityType"></form:hidden>
                            <nav class="text-right">
                                <div class="form-group">
                                    <label class="control-label">关键字：</label>
                                    <form:input path="name" cssClass="form-control" style="width:100px" />
                                </div>
                                <div class=" form-group">
                                    <label class="control-label">聚合编码：</label>
                                    <form:input path="ownId" cssClass="form-control" style="width:100px"/>
                                </div>
                                <div class=" form-group">
                                    <label class="control-label">渠道设施编码：</label>
                                    <form:input path="amenityId" cssClass="form-control" style="width:100px"/>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">渠道来源：</label>
                                    <form:select path="originType" cssClass="form-control" multiple="multiple" style="display:none;">
                                        <form:options items="${originTypes}" itemLabel="name" itemValue="message"/>
                                    </form:select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">聚合状态：</label>
                                    <form:select path="match" cssClass="form-control">
                                        <form:option value="">全部</form:option>
                                        <form:option value="true">已聚合</form:option>
                                        <form:option value="false">未聚合</form:option>
                                    </form:select>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-default" onclick="search_fac(1)">查询
                                    </button>
                                    <button type="button" class="btn btn-default" onclick="reset_fac()">重置</button>
                                </div>
                            </nav>
                        </form:form>
                    </div>
                </div>
                <div id="own_fac_list" style="margin-top: 15px">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- 详细页面 -->
<div id="ownHotelFacDetail" style="display:none;"></div>
<script src="/webpage/hotel/ownData/fac/ownHotelFac.js"/>