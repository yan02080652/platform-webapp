<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th><label>聚合编码</label></th>
        <th><label>聚合设施名</label></th>
        <th ><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr>
            <td>${bean.id}</td>
            <td>${bean.name}</td>
            <td><a href="javascript:void(0);" onclick="relate(${bean.id});">关联</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=searchFac"></jsp:include>
</div>