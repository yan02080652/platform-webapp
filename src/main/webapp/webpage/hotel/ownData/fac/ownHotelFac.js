$(function(){
    $('#originType').multiselect({
        enableClickableOptGroups: true,
        numberDisplayed:5,
        buttonWidth: '100px',
        allSelectedText:'全部',
        includeSelectAllOption: true,
        selectAllText: '全部',
        nonSelectedText:'全部'
    });
    initOriginType();
    search_fac();
})

function initOriginType(){
    var array = new Array();
    $('#originType option').each(function(){
        array.push($(this).val());
    })
    $('#originType').multiselect('select',array);
}

/**
 * 查询
 */
function search_fac(pageIndex){
    var index=layer.load();
    $.ajax({
        url:'/hotel/ownData/facList',
        type:'post',
        data:{
            ownId:$('#ownId').val(),
            amenityType:$('#amenityType').val(),
            amenityId:$('#amenityId').val(),
            name:$('#name').val(),
            originType:$('#originType').val().join(','),
            match:$('#match').val(),
            pageIndex:pageIndex
        },success:function(data){
            layer.close(index);
            $('#own_fac_list').html(data);
        },error:function(){
            layer.close(index);
            layer.msg("系统异常",{icon: 2})
        }
    })
}

/**
 * 重置
 */
function reset_fac(){
    $('#ownId').val('');
    $('#amenityId').val('');
    $('#name').val('');
    initOriginType();
    $('#match').val('');
    search_fac(1);
}

/**
 * 脱离聚合关系
 * @param id
 */
function awayFromOwnFac(id){
    layer.confirm("确认脱离聚合?",function() {
        $.ajax({
            url: 'hotel/ownData/awayFac',
            type: 'post',
            data: {
                id: id
            }, success: function (data) {
                if (data.success) {
                    layer.msg("操作成功", {icon: 1});
                    search_fac();
                } else {
                    layer.msg("脱离失败:" + data.msg, {icon: 2});
                }
            }, error: function () {
                layer.msg("系统异常", {icon: 2});
            }
        });
    });
}

/**
 * 添加聚合关系
 * @param id
 */
function addOwnHotelFac(id,name,originName){
    $.ajax({
        url: '/hotel/ownData/addOwnHotelFac',
        type: 'post',
        data: {
            id: id,
            name:name,
            amenityType:$('#amenityType').val(),
            originName:originName
        },success: function(data){
            $('#ownHotelFacDetail').html(data);
            $('#ownHotelFacDetail').show();
            $('#mode_own_hotelfac').modal();
        }
    })
}