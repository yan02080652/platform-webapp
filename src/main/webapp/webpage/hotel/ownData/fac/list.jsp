<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" >
    <thead>
    <tr>
        <th><label>聚合编码</label></th>
        <th><label>设施名称</label></th>
        <th><label>来源渠道</label></th>
        <th><label>渠道设施编码</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr class="odd gradeX">
            <td>
                <c:if test="${bean.ownId gt 0}">${bean.ownId}</c:if>
                <c:if test="${bean.ownId eq 0}">未聚合</c:if>
            </td>
            <td>${bean.name}</td>
            <td>${bean.supply.name}</td>
            <td>${bean.propertyId}</td>
            <td>
                <c:if test="${bean.ownId gt 0}">
                    <a href="javascript:void(0);" onclick="awayFromOwnFac(${bean.id})">脱离</a>
                </c:if>
                <c:if test="${bean.ownId eq 0}">
                    <a href="javascript:void(0);" onclick="addOwnHotelFac(${bean.id},'${bean.name}','${bean.supply.name}')">设置</a>
                </c:if>
            </td>
        </tr>
    </c:forEach>

    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=search_fac"></jsp:include>
</div>
<script type="text/javascript">
    //checkbox全选居中，固定宽度
    $("table tr").each(function(){
        $(this).find(":first").attr("style","text-align: center;width:100px");
    })
</script>