<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="modal fade" id="mode_own_hotelfac" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">设置聚合关系</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="detailForm" role="form">
                    <input type="hidden" id="id" value="${id}">
                    <input type="hidden" id="amenityType" value="${amenityType}">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >渠道设施名称：</label>
                        <label class="col-sm-2 control-label" style="text-align:left;">${name}</label>
                        <label class="col-sm-2 control-label" style="text-align:left;">来源渠道：</label>
                        <label class="col-sm-2 control-label" style="text-align:left;">${originName}</label>
                        <div class="col-lg-offset-1 col-sm-2">
                            <button type="button" class="btn btn-primary control-label" onclick="addHotelFacToOwn()">+设为新聚合设施</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">聚合编码/名称：</label>
                        <div class="col-sm-6">
                            <input class="form-control" id="facName" placeholder="聚合编码/名称" value="${name}"/>
                        </div>
                        <div class="col-lg-offset-1 col-sm-2">
                            <button type="button" class="btn btn-default" onclick="searchFac();">搜索</button>
                        </div>
                    </div>
                    <div id="own_hotelfac_table">

                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<script type="text/javascript">
    function searchFac(pageIndex){
        var index = layer.load();
        var facName = $('#facName').val();
        $.ajax({
            url: '/hotel/ownData/loadOwnHotelFacData',
            type: 'post',
            data: {
                facName: facName,
                amenityType:$('#amenityType').val(),
                pageIndex: pageIndex,
                pageSize: 10
            },success: function(data){
                $('#own_hotelfac_table').html(data);
                layer.close(index);
            }
        })
    }

    //关联商圈
    function relate(ownId){
        var facId = $('#id').val();
        layer.confirm("确认关联此设施?",function(){
            $.ajax({
                url: '/hotel/ownData/relateHotelFac',
                type: 'post',
                async: false,
                data: {
                    ownId: ownId,
                    id: facId
                },success: function(data){
                    if(data.success){
                        layer.msg("操作成功: " + data.msg,{icon: 1});
                        refresh();
                    }else{
                        layer.msg("操作失败: " + data.msg,{icon: 2});
                    }
                },error: function(){
                    layer.msg("系统异常",{icon: 2});
                }
            })
        })
    }

    function refresh(){
        search_fac();
        $('#mode_own_hotelfac').modal('hide');
    }

    //设置为聚合酒店
    function addHotelFacToOwn(){
        var facId = $('#id').val();
        $.ajax({
            url: '/hotel/ownData/addHotelFacToOwn',
            type: 'post',
            async: false,
            data: {
                id: facId
            },success: function(data){
                if(data.success){
                    layer.msg("操作成功: " + data.msg,{icon: 1});
                    refresh();
                }else{
                    layer.msg("操作失败: " + data.msg,{icon: 2});
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2});
            }
        })
    }
</script>