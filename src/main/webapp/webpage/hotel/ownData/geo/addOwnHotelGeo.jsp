<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="modal fade" id="mode_own_hotelgeo" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">设置聚合关系</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="detailForm" role="form">
                    <input type="hidden" value="${id}" id="origin_geo_id" />
                    <input type="hidden" value="${ownCityId}" id="own_city_id" />
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >渠道商圈/行政区：</label>
                        <label class="col-sm-2 control-label" style="text-align:left;">${name}</label>
                        <label class="col-sm-2 control-label" style="text-align:left;">来源渠道：</label>
                        <label class="col-sm-2 control-label" style="text-align:left;">${originName}</label>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-primary control-label" onclick="addHotelGeoToOwn()">+设为聚合商圈/行政区</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >聚合渠道商圈/行政区：</label>
                        <div class="col-sm-4">
                            <input class="form-control" id="geoName" placeholder="聚合渠道商圈/行政区" value="${name}"/>
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control" id="cityName" placeholder="上级城市"/>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-default" onclick="searchGeo();">搜索</button>
                        </div>
                    </div>
                    <div id="own_hotelgeo_table">

                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<script type="text/javascript">
    function searchGeo(pageIndex){
        var index = layer.load();
        $.ajax({
            url: '/hotel/ownData/loadOwnHotelGeoData',
            type: 'post',
            data: {
                geoName: $('#geoName').val(),
                cityName: $('#cityName').val(),
                ownCityId:$('#own_city_id').val(),
                pageIndex: pageIndex,
                pageSize: 10
            },success: function(data){
                $('#own_hotelgeo_table').html(data);
                layer.close(index);
            }
        })
    }

    //关联商圈
    function relate(ownId){
        var geoId = $('#origin_geo_id').val();
        layer.confirm("确认关联此商圈?",function(){
            $.ajax({
                url: '/hotel/ownData/relateHotelGeo',
                type: 'post',
                async: false,
                data: {
                    ownId: ownId,
                    geoId: geoId
                },success: function(data){
                    if(data.success){
                        layer.msg("操作成功: " + data.msg,{icon: 1})
                        setTimeout(refresh,1000);
                    }else{
                        layer.msg("操作失败: " + data.msg,{icon: 2})
                    }
                },error: function(){
                    layer.msg("系统异常",{icon: 2})
                }
            })
        })
    }

    //设置为聚合酒店
    function addHotelGeoToOwn(){
        var geoId = $('#origin_geo_id').val();
        $.ajax({
            url: '/hotel/ownData/addHotelGeoToOwn',
            type: 'post',
            async: false,
            data: {
                geoId: geoId
            },success: function(data){
                if(data.success){
                    layer.msg("操作成功: " + data.msg,{icon: 1});
                    setTimeout(refresh,1000);
                }else{
                    layer.msg("操作失败: " + data.msg,{icon: 2});
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2});
            }
        })
    }
    function refresh(){
        search_geo_refresh();
        $('#mode_own_hotelgeo').modal('hide');

    }
</script>