<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" >
    <thead>
    <tr>
        <th><label>类型</label></th>
        <th><label>聚合编码</label></th>
        <th><label>商圈/行政区名</label></th>
        <th><label>来源渠道</label></th>
        <th><label>渠道编码</label></th>
        <th><label>城市</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr class="odd gradeX">
            <td>
                <c:if test="${bean.geoType eq 1}">行政区</c:if>
                <c:if test="${bean.geoType eq 2}">商圈</c:if>
                <c:if test="${bean.geoType eq 3}">机场</c:if>
                <c:if test="${bean.geoType eq 4}">火车站</c:if>
                <c:if test="${bean.geoType eq 5}">地铁</c:if>
                <c:if test="${bean.geoType eq 6}">景点景区</c:if>
                <c:if test="${bean.geoType eq 7}">下辖县</c:if>
                <c:if test="${bean.geoType eq 8}">地标</c:if>
            </td>
            <td>
                <c:if test="${bean.ownId eq -1}">疑似聚合</c:if>
                <c:if test="${bean.ownId gt 0}">${bean.ownId}</c:if>
                <c:if test="${bean.ownId eq 0}">未聚合</c:if>
            </td>
            <td>${bean.geoName}</td>
            <td>${bean.supply.name}</td>
            <td>${bean.geoId}</td>
            <td>${bean.cityName}</td>
            <td>
                <c:if test="${bean.ownId gt 0}">
                    <a href="javascript:void(0);" onclick="awayFormOwnGeo(${bean.id})">脱离</a>
                </c:if>
                <c:if test="${bean.ownId eq 0}">
                    <a href="javascript:void(0);" onclick="addOwnHotelGeo(${bean.id},'${bean.geoName}','${bean.supply.name}')">设置</a>
                </c:if>
            </td>
        </tr>
    </c:forEach>

    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=search_geo"></jsp:include>
</div>
<script type="text/javascript">
    //checkbox全选居中，固定宽度
    $("table tr").each(function(){
        $(this).find(":first").attr("style","text-align: center;width:100px");
    })
</script>