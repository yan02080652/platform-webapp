$(function(){
    $('#originType').multiselect({
        enableClickableOptGroups: true,
        numberDisplayed:5,
        buttonWidth: '100px',
        allSelectedText:'全部',
        includeSelectAllOption: true,
        selectAllText: '全部',
        nonSelectedText:'全部'
    });
    initOriginType();
    search_geo();
})

function initOriginType(){
    var array = new Array();
    $('#originType option').each(function(){
        array.push($(this).val());
    })
    $('#originType').multiselect('select',array);
}
/**
 * 查询
 * @param pageIndex
 */
function search_geo(pageIndex){
    var index = layer.load();
    $.ajax({
        url: '/hotel/ownData/geoList',
        type: 'post',
        data:{
            ownId: $('#ownId').val(),
            geoId: $('#geoId').val(),
            keyword:$('#keyword').val(),
            match: $('#match').val(),
            originType: $('#originType').val().join(','),
            geoType: $('#geoType').val(),
            cityIds:$('#cityIds').val(),
            pageIndex: pageIndex,
            pageSize: 10
        },success:function(data){
            layer.close(index);
            $('#own_geo_list').html(data);
        },error:function(){
            layer.close(index);
            layer.msg("系统异常",{icon: 2})
        }

    });
}

function search_geo_refresh(){
    search_geo($('#own_geo_list  .cur').text());
}
/**
 * 重置
 */
function reset_geo(){
    $('#ownId').val('');
    $('#geoId').val('');
    $('#match').val('');
    $('#keyword').val('');
    initOriginType();
    $('#geoType').val('0');
    $('#geoSysDistrictName').val('');
    $('#cityIds').val('');
    search_geo(1);
}

/**
 * 添加聚合
 */
function addOwnHotelGeo(id,geoName,originName){
    $.ajax({
        url: '/hotel/ownData/addOwnHotelGeo',
        type: 'post',
        data: {
            id: id,
            geoName:geoName,
            originName:originName
        },success: function(data){
            $('#ownHotelGeoDetail').html(data);
            $('#ownHotelGeoDetail').show();
            $('#mode_own_hotelgeo').modal();
        }
    })
}

//脱离聚合关系
function awayFormOwnGeo(id){
    layer.confirm("确认脱离聚合?",function(){
        $.ajax({
            url: '/hotel/ownData/awayGeo',
            type: 'post',
            data: {
                id: id
            },success: function(data){
                if(data.success){
                    layer.msg("操作成功",{icon: 1});
                    search_geo($('#own_geo_list .cur').text());
                }else{
                    layer.msg("脱离失败:" + data.msg,{icon: 2});
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2})
            }
        })
    })

}


var getGeoSysDistrictDataChoosedData = JSON.parse('[]');
function chooseGeoSysDistrictData() {

    new choose.districtData({
        id:'selMultiDistrictData',//给id可以防止一直新建对象
        empty:false,
        multi:true,
        width: 500,
        getChoosedData:function () {
            return getGeoSysDistrictDataChoosedData;
        }
    },function (data) {
        getGeoSysDistrictDataChoosedData = [];
        var districtDesc ='';
        var districtIds ='';
        if (data) {
            $(data).each(function (i, item) {
                getGeoSysDistrictDataChoosedData.push({id: item.id, nameCn: item.nameCn});
                if(i == 0){
                    districtDesc += item.nameCn;
                } else if (i <= 2) {
                    districtDesc += ","+item.nameCn;
                }
                if (i == 3) {
                    districtDesc = districtDesc + '  ...';
                }
                if(i == 0){
                    districtIds += item.id;
                }else{
                    districtIds +=","+item.id;
                }
            });
            $("#cityIds").val(districtIds);
            $("#geoSysDistrictName").val(districtDesc);
        }

    });
}