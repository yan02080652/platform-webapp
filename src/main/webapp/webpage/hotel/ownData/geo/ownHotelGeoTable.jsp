<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="width:20%;"><label>聚合编码</label></th>
        <th style="width:20%;"><label>类型</label></th>
        <th style="width:20%;"><label>上级城市</label></th>
        <th style="width:20%;"><label>聚合商圈/行政区名</label></th>
        <th style="width:20%;"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr>
            <td>${bean.id}</td>
            <td>
                <c:if test="${bean.geoType eq 1}">行政区</c:if>
                <c:if test="${bean.geoType eq 2}">商圈</c:if>
            </td>
            <td>${bean.cityName}</td>
            <td>${bean.geoName}</td>
            <td><a href="javascript:void(0);" onclick="relate(${bean.id});">关联</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=searchGeo"></jsp:include>
</div>