<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="modal fade" id="mode_own_brand" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">设置聚合关系</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="detailForm" role="form" >
                    <input type="hidden" value="${id}" id="origin_brand_id" />
                    <input type="hidden" value="${originType}" id="origin_brand_originType" />
                    <div class="form-group">
                        <label class="col-sm-2 control-label">渠道品牌名称：</label>
                        <label class="col-sm-6 control-label" style="text-align:left;">${name}</label>
                        <div class="col-lg-offset-1 col-sm-2">
                            <button type="button" class="btn btn-primary control-label" onclick="addBrandToOwn()">+设为新聚合品牌</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">聚合品牌名：</label>
                        <div class="col-sm-6">
                            <input class="form-control" id="brandName" placeholder="聚合品牌名" value="${name}"/>
                        </div>
                        <div class="col-lg-offset-1 col-sm-2">
                            <button type="button" class="btn btn-default" onclick="searchBrand();">搜索</button>
                        </div>
                    </div>
                    <div id="own_brand_table">

                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<script type="text/javascript">
    function searchBrand(pageIndex){
        var index = layer.load();
        var brandName = $('#brandName').val();
        $.ajax({
            url: '/hotel/ownData/loadOwnHotelBrandData',
            type: 'post',
            data: {
                name: brandName,
                pageIndex: pageIndex,
                pageSize: 10
            },success: function(data){
                $('#own_brand_table').html(data);
                layer.close(index);
            }
        })
    }

    //关联品牌
    function relate(ownId){
        var brandId = $('#origin_brand_id').val();
        var originType=$('#origin_brand_originType').val();
        layer.confirm("确认关联此品牌?",function(){
            $.ajax({
                url: '/hotel/ownData/relateBrand',
                type: 'post',
                async: false,
                data: {
                    ownId: ownId,
                    brandId: brandId,
                    originType:originType
                },success: function(data){
                    if(data.success){
                        layer.msg("操作成功: " + data.msg,{icon: 1});
                        setTimeout(refresh,1000);
                    }else{
                        layer.msg("操作失败: " + data.msg,{icon: 2});
                    }
                },error: function(){
                    layer.msg("系统异常",{icon: 2});
                }
            })
        })
    }

    function refresh(){
        search_brand_refresh();
        $('#mode_own_brand').modal('hide');
        $('#ownBrandDetail').hidden();
    }
    //设置为聚合品牌
    function addBrandToOwn(){
        var brandId = $('#origin_brand_id').val();
        var originType = $('#origin_brand_originType').val();
        $.ajax({
            url: '/hotel/ownData/addBrandToOwn',
            type: 'post',
            async: false,
            data: {
                brandId: brandId,
                originType:originType
            },success: function(data){
                if(data.success){
                    layer.msg("操作成功: " + data.msg,{icon: 1});
                    setTimeout(refresh,1000);
                }else{
                    layer.msg("操作失败: " + data.msg,{icon: 2})
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2})
            }
        })
    }
</script>