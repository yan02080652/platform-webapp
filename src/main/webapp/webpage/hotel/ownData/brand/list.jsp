<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover"  style="table-layout:fixed" >
    <thead>
    <tr>
        <th ><label>聚合编码</label></th>
        <th ><label>品牌名称</label></th>
        <th ><label>来源渠道</label></th>
        <th ><label>渠道编码</label></th>
        <th ><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="bean">
        <tr class="odd gradeX">
            <td>
                <c:if test="${bean.ownId eq -1}">疑似聚合</c:if>
                <c:if test="${bean.ownId gt 0}">${bean.ownId}</c:if>
                <c:if test="${bean.ownId eq 0}">未聚合</c:if>
            </td>
            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${bean.shortName }</td>
            <td>${bean.supply.name}</td>
            <td>${bean.id}</td>
            <td class="text-center" >
                <c:if test="${bean.ownId gt 0}">
                    <a onclick="awayBrand('${bean.id }',${bean.originType})" >脱离</a>
                </c:if>
                <c:if test="${bean.ownId eq 0 }">
                    <a onclick="addMatchBrand('${bean.id }',${bean.originType},'${bean.shortName}');">设置</a>
                </c:if>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=search_brand"></jsp:include>
</div>


