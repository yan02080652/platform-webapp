$(function(){
    $('#originType').multiselect({
        enableClickableOptGroups: true,
        numberDisplayed:5,
        buttonWidth: '100px',
        allSelectedText:'全部',
        includeSelectAllOption: true,
        selectAllText: '全部',
        nonSelectedText:'全部'
    });
    initOriginType();
    search_brand();
})

function initOriginType(){
    var array = new Array();
    $('#originType option').each(function(){
        array.push($(this).val());
    })
    $('#originType').multiselect('select',array);
}
/**
 * 重置
 */
function reset_brand(){
    $('#ownId').val('');
    $('#brandId').val('');
    $('#name').val('');
    $('#match').val('');
    initOriginType();
    search_brand(1);
}
/**
 * 首页加载列表
 * @param pageIndex
 */
function search_brand(pageIndex){
    var index = layer.load();
    $.ajax({
        url: '/hotel/ownData/brandList',
        type: 'post',
        data: {
            ownId: $('#ownId').val(),
            brandId: $('#brandId').val(),
            name:$('#name').val(),
            match: $('#match').val(),
            originType: $('#originType').val().join(','),
            pageIndex: pageIndex,
            pageSize: 10
        },success: function(data){
            layer.close(index);
            $('#own_brand_list').html(data);
        },error: function(){
            layer.close(index);
            layer.msg("系统异常",{icon: 2})
        }

    })
}

function search_brand_refresh(){
    search_brand($('#own_brand_list .cur').text());
}
/**
 * 增加聚合品牌
 */
function addMatchBrand(id,originType,name){
    $.ajax({
        url: '/hotel/ownData/addOwnHotelBrand',
        type: 'post',
        data: {
            id: id,
            originType:originType,
            name: name
        },success: function(data){
            $('#ownHotelBrandDetail').html(data);
            $('#ownHotelBrandDetail').show();
            $('#mode_own_brand').modal();
        }
    })
}

/**
 * 解除聚合关系
 */
function awayBrand(id,originType){
    layer.confirm('确认脱离聚合关系吗?',function (index) {
        layer.close(index);
        var loading = layer.load();
        $.ajax({
            url: 'hotel/ownData/awayBrand',
            type: 'post',
            data:{
                id:id,
                originType:originType
            },
            success: function (data) {
                layer.close(loading);
                if (data.success) {
                    layer.msg("操作成功",{icon: 1});
                    search_brand_refresh();
                } else {
                    layer.msg("脱离失败:" + data.msg,{icon: 2});
                }
            },error:function () {
                layer.close(loading);
                $.alert('系统繁忙,请稍后重试');
            }
        });
    });
}