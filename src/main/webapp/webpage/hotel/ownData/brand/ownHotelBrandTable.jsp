<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="width:8%;"><label>聚合编码</label></th>
        <th style="width:30%;"><label>品牌名称</label></th>
        <th style="width:13%;"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr>
            <td>${bean.id}</td>
            <td>${bean.shortName}</td>
            <td><a href="javascript:void(0);" onclick="relate(${bean.id});">关联</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=searchBrand"></jsp:include>
</div>