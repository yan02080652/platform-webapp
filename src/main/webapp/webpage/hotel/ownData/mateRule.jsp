<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<form:form  cssClass="form-inline"
								   method="post" >
							<div class="pull-left">
								<div class="form-group">
									<div class="btn-group">
										<select  name="mateType"  class="form-control" id="mateTypeTag">
											<c:forEach items="${mateTypes}" var="mateType">
												<option  value="${mateType}">${mateType.value}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>

							<div class="pull-right">
								<div class=" form-group">
									<label class="control-label">关键字：</label>
									<input id="keywords" class="form-control" style="width:200px"/>
								</div>

								<div class="form-group">
									<button type="button" class="btn btn-default" onclick="reloadMateList(1)">搜索
									</button>
									<button type="button" class="btn btn-default btn-primary" onclick="addMateRule()">+设置聚合关系</button>
								</div>
							</div>
						</form:form>
					</div>
				</div>
				<div id="mateRuleList" style="margin-top: 15px">

				</div>
			</div>
		</div>
	</div>
</div>
<!-- 详细页面 -->
<div id="mateRuleDetail" style="display:none;"></div>
						
						
<script type="text/javascript" src="webpage/hotel/ownData/mateRule.js?version=${globalVersion}"></script>