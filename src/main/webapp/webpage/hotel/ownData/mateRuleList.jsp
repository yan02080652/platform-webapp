<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover"  style="table-layout:fixed" >
	<thead>
		<tr>
			 <th width="15%"><label>显示名称</label></th>
			 <th width="45%"><label>匹配规则</label></th>
			 <th width="20%"><label>操作</label></th>
		</tr>
	</thead>
	<tbody>
		 <c:forEach items="${pageList.list }" var="bean">
			 <tr class="odd gradeX">
				 <td>${bean.name }</td>
				 <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${bean.mateRule }</td>
				 <td class="text-center" >
					  <a onclick="deleteRule('${bean.id }');">脱离</a>
				 </td>
			  </tr>
		  </c:forEach>
	</tbody>
</table>
<div class="pagin">
	<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadMateList"></jsp:include>
</div>

