<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover">
    <thead>
    <tr>
        <th style="width:8%;"><label>聚合编码</label></th>
        <th style="width:30%;"><label>聚合城市名</label></th>
        <th style="width:10%;"><label>聚合城市等级</label></th>
        <th style="width:10%;"><label>上级城市</label></th>
        <th style="width:13%;"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr>
            <td>${bean.id}</td>
            <td>${bean.nameCn}</td>
            <td>
                <c:choose>
                    <c:when test="${bean.level == '2'}">
                    省份
                    </c:when>
                    <c:when test="${bean.level == '3'}">
                        城市
                    </c:when>
                    <c:otherwise>
                        区县
                    </c:otherwise>
                </c:choose>
            </td>
            <td>${bean.namePath}</td>
            <td><a href="javascript:void(0);" onclick="relate(${bean.id});">关联</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=searchCity"></jsp:include>
</div>