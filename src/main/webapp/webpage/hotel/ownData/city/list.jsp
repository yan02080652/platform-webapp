<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" >
    <thead>
    <tr>
        <th><label>聚合编码</label></th>
        <th><label>聚合城市名</label></th>
        <th><label>来源渠道</label></th>
        <th><label>城市等级</label></th>
        <th><label>渠道城市编码</label></th>
        <th><label>上级城市名称</label></th>
        <th><label>渠道城市名称</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr class="odd gradeX">
            <td>
                <c:choose>
                    <c:when test="${bean.districtId == null}">
                        未聚合
                    </c:when>
                    <c:otherwise>
                        ${bean.districtId}
                    </c:otherwise>
                </c:choose>
            </td>
            <td>${bean.districtName}</td>
            <td>${bean.supply.name}</td>
            <td>${bean.cityType}</td>
            <td>${bean.code}</td>
            <td>${bean.parentName}</td>
            <td>${bean.nameCn}</td>
            <td>
                <c:choose>
                    <c:when test="${bean.districtId == null}">
                        <a href="javascript:void(0);" onclick="addOwnHotelCity(${bean.id},'${bean.nameCn}','${bean.supply.name}')">设置</a>
                    </c:when>
                    <c:otherwise>
                        <a href="javascript:void(0);" onclick="awayFromOwnCity(${bean.id})">脱离</a>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </c:forEach>

    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=search_city"></jsp:include>
</div>
<script type="text/javascript">
    //checkbox全选居中，固定宽度
    $("table tr").each(function(){
        $(this).find(":first").attr("style","text-align: center;width:100px");
    })
</script>