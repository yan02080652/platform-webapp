$(function(){
    $('#originType').multiselect({
        enableClickableOptGroups: true,
        numberDisplayed:5,
        buttonWidth: '100px',
        allSelectedText:'全部',
        includeSelectAllOption: true,
        selectAllText: '全部',
        nonSelectedText:'全部'
    });
    initOriginType();
    search_city();
})

function initOriginType(){
    var array = new Array();
    $('#originType option').each(function(){
        array.push($(this).val());
    })
    $('#originType').multiselect('select',array);
}
/**
 * 查询
 */
function search_city(pageIndex){
    var index=layer.load();
    $.ajax({
        url:'/hotel/ownData/cityList',
        type:'post',
        data:{
            ownId:$('#ownId').val(),
            cityId:$('#cityId').val(),
            keyword:$('#keyword').val(),
            originType:$('#originType').val().join(','),
            match:$('#match').val(),
            pageIndex:pageIndex
        },success:function(data) {
            layer.close(index);
            $('#own_city_list').html(data);

        },error:function(){
            layer.close(index);
            layer.msg("系统异常",{icon: 2})
        }
    })
}
function search_city_refresh(){
    search_city($('#own_city_list  .cur').text());
}
/**
 * 重置
 */
function reset_city(){
    $('#ownId').val('');
    $('#cityId').val('');
    $('#keyword').val('');
    initOriginType();
    $('#match').val('');
    search_city(1);
}

/**
 * 脱离聚合关系
 * @param id
 */
function awayFromOwnCity(id){
    layer.confirm("确认脱离聚合?",function(){
        $.ajax({
            url: '/hotel/ownData/awayCity',
            type: 'post',
            data: {
                id: id
            },success: function(data){
                if(data.success){
                    layer.msg("操作成功",{icon: 1});
                    search_city($('.cur').text());
                }else{
                    layer.msg("脱离失败:" + data.msg,{icon: 2});
                }
            },error: function(){
                layer.msg("系统异常",{icon: 2})
            }
        })
    })
}

/**
 * 添加聚合关系
 * @param id
 */
function addOwnHotelCity(id,name,originName){
    $.ajax({
        url: '/hotel/ownData/addOwnHotelCity',
        type: 'post',
        data: {
            id: id,
            name: name,
            originName:originName
        },success: function(data){
            $('#ownHotelCityDetail').html(data);
            $('#ownHotelCityDetail').show();
            $('#mode_own_hotelcity').modal();
        }
    })
}