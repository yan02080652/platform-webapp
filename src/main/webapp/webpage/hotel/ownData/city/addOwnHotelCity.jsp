<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="modal fade" id="mode_own_hotelcity" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">新增聚合关系</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="detailForm" role="form">
                    <input type="hidden" value="${id}" id="origin_city_id" />
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >渠道城市名称：</label>
                        <label class="col-sm-2 control-label" style="text-align:left;">${name}</label>
                        <label class="col-sm-2 control-label" style="text-align:left;">来源渠道：</label>
                        <label class="col-sm-2 control-label" style="text-align:left;">${originName}</label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" >聚合城市名称：</label>
                        <div class="col-sm-6">
                            <input class="form-control" id="cityName" placeholder="聚合城市名称" value="${name}"/>
                        </div>
                        <div class="col-sm-3">
                            <button type="button" class="btn btn-default" onclick="searchCity();">搜索</button>
                        </div>
                    </div>
                    <div id="own_city_table">

                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<script type="text/javascript">
    function searchCity(pageIndex){
        var index = layer.load();
        var cityName = $('#cityName').val();
        $.ajax({
            url: '/hotel/ownData/loadOwnCityData',
            type: 'post',
            data: {
                cityName: cityName,
                pageIndex: pageIndex,
                pageSize: 10
            },success: function(data){
                $('#own_city_table').html(data);
                layer.close(index);
            }
        })
    }

    //关联城市
    function relate(ownId){
        var cityId = $('#origin_city_id').val();
        layer.confirm("确认关联此城市?",function(){
            $.ajax({
                url: '/hotel/ownData/relateCity',
                type: 'post',
                async: false,
                data: {
                    ownId: ownId,
                    cityId: cityId
                },success: function(data){
                    if(data.success){
                        layer.msg("操作成功: " + data.msg,{icon: 1});
                        search_city_refresh();
                        $('#mode_own_hotelcity').modal('hide');

                    }else{
                        layer.msg("操作失败: " + data.msg,{icon: 2});
                    }
                },error: function(){
                    layer.msg("系统异常",{icon: 2});
                }
            })
        })
    }
</script>