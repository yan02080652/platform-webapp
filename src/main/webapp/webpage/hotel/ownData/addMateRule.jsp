<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div class="modal fade" id="mode_mateRuleDetail" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:70%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">新增聚合关系</h4>
            </div>
            <div class="modal-body">
                <form:form class="form-horizontal" id="mateRuleForm" role="form" modelAttribute="hotelMateRuleDto">
                    <form:hidden path="mateType" value="${hotelMateRuleDto.mateType}"></form:hidden>
                    <div class="effectiveDate" hidden>
                        <p>名称建立聚合关系后，所有数据源中相同名称都将之间建立聚合关系</p>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2" style="text-align: right;"><span class="reqMark">*</span>显示名称:</div>
                        <div class="col-sm-2">
                            <input name="name"  class="form-control"  style="width: 100%;" placeholder="输入显示名称" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2" style="text-align: right;"><i class="effective-date fa fa-question-circle layui-word-aux"></i>匹配名称</div>
                        <div class="col-sm-5">
                            <input name="mateRule"  placeholder ="请输入需要聚合的名称,用-相隔,如双床房-双人房-双人间..." class="form-control"  style="width: 100%;"  />
                        </div>
                    </div>

                </form:form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submitMateRule()">保存</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
    var intercept={
        showHelpTip: function (className, dom) {
            $(document).on('mouseover', className, function () {
                var that = this;
                var index = layer.tips(dom, that, {
                    tips: [2, '#1AA094'],
                    time: 0,
                    area: '500px'
                });
                $(that).on('mouseleave', function () {
                    layer.close(index);
                })
            });
        },
        tipData: [
            {className: ".effective-date", dom: $(".effectiveDate").html()},
        ],
    }
    intercept.showHelpTip(".effective-date", $(".effectiveDate").html());
</script>
