<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" >
    <thead>
    <tr>
        <th><label>编号</label></th>
        <th><label>类型</label></th>
        <th><label>来源编码</label></th>
        <th><label>来源编码名称</label></th>
        <th><label>聚合编码</label></th>
        <th><label>聚合编码名称</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr class="odd gradeX">
            <td>
                ${bean.id}
            </td>
            <td>
                <c:if test="${bean.type eq 5}">城市</c:if>
                <c:if test="${bean.type eq 1}">酒店</c:if>
            </td>
            <td>${bean.originId}</td>
            <td>${bean.originName}</td>
            <td>${bean.ownId}</td>
            <td>${bean.ownName}</td>
            <td>
                <a href="javascript:void(0);" onclick="addOwn(${bean.id})">增加聚合<c:if test="${bean.type eq 5}">城市</c:if><c:if test="${bean.type eq 1}">酒店</c:if></a>
            </td>
        </tr>
    </c:forEach>

    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../../common/pagination_ajax.jsp?callback=search_suspect"></jsp:include>
</div>
<script type="text/javascript">
    //checkbox全选居中，固定宽度
    $("table tr").each(function(){
        $(this).find(":first").attr("style","text-align: center;width:100px");
    })
</script>