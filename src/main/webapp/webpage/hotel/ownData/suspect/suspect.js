$(function(){
    search_suspect();
})

/**
 * 查询
 */
function search_suspect(pageIndex){
    var index=layer.load();
    $.ajax({
        url:'/hotel/ownData/suspectList',
        type:'post',
        data:{

            type:$('#type').val(),
            pageIndex:pageIndex
        },success:function(data){
            layer.close(index);
            $('#suspect_list').html(data);
        },error:function(){
            layer.close(index);
            layer.msg("系统异常",{icon: 2});
        }
    })
}

/**
 * 重置
 */
function reset_suspect(){
    $('#type').val('');
    search_suspect(1);
}

/**
 * 添加聚合
 * @param id
 * @param type
 */
function addOwn(id){
    $.ajax({
        url:'/hotel/ownData/addOwn',
        type:'post',
        data:{
            id:id
        },success:function(data){
            if(data.success){
                layer.msg("操作成功",{icon: 1});
                search_suspect();
            }else{
                layer.msg("聚合失败:" + data.msg,{icon: 2});
            }
        },error: function(){
            layer.msg("系统异常",{icon: 2});
        }
    })
}