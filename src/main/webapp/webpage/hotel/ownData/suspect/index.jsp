<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-left">
                            <%--<a class="btn btn-default" onclick="synDistrictData()" role="button">手动更新城市</a>--%>
                        </div>
                        <!-- 查询条件 -->
                        <form:form modelAttribute="suspectCondition" cssClass="form-inline" method="post">
                            <nav class="text-right">
                                <div class="form-group">
                                    <label class="control-label">疑似来源：</label>
                                    <form:select path="type" cssClass="form-control">
                                        <form:option value="">全部</form:option>
                                        <form:option value="1">酒店</form:option>
                                        <form:option value="5">城市</form:option>
                                    </form:select>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-default" onclick="search_suspect(1)">查询
                                    </button>
                                    <button type="button" class="btn btn-default" onclick="reset_suspect()">重置</button>
                                </div>
                            </nav>
                        </form:form>
                    </div>
                </div>
                <div id="suspect_list" style="margin-top: 15px">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- 详细页面 -->
<div id="suspectDetail" style="display:none;"></div>
<script src="/webpage/hotel/ownData/suspect/suspect.js"/>