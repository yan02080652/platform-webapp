<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<style>
    td {
        text-align: center;
    }
    .word {
        width: 200px;
        display: block;
        text-overflow: ellipsis;
        -webkit-line-clamp: 1;
        white-space: nowrap;
        overflow: hidden;
        text-align: center;
    }
</style>
<link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">
 <div class="panel panel-default">
    	 <div class="panel-heading">
     			   <i class="glyphicon glyphicon-home" aria-hidden="true"></i>
      			  路由规则维护&nbsp;<a data-pjax onclick="backRuleMain()">返回</a>
    	  </div>
          <div class="panel-body">
              <div class="col-sm-12">
		        <div class="col-sm-12">

				                <form:form id="ruleForm" cssClass="form-horizontal bv-form" method="post" action="hotel/routeRule/save"
				                           modelAttribute="routeRuleDto">
				                    <form:hidden path="id"/>
									<div class="page-header">
										<h3>基础信息 |&nbsp;
											<button class="layui-btn layui-btn-normal" type="submit">
												<i class="layui-icon">&#xe63c;保存</i>
											</button>
										</h3>
									</div>
				                    <div class="form-group">
				                        <label class="col-sm-2 control-label">规则名称</label>
				                        <div class="col-sm-3">
				                            <form:input path="ruleName" cssClass="form-control" required="true" maxlength="20"/>
				                        </div>
				                    </div>
				                    <div class="form-group">
				                        <label class="col-sm-2 control-label">规则级别</label>
				                        <div class="col-sm-3 ">
				                            <form:radiobuttons path="ruleGrade" cssClass="" itemLabel="message" />
				                        </div>
				                    </div>
				                    <div class="form-group">
				                        <label class="col-sm-2 control-label">适用范围</label>
				                        <div class="col-sm-3">
				                            <input type="text" class="form-control partner" name="scopeName" value="${routeRuleDto.scopeName}" placeholder="选择合作伙伴" readonly/>
				                            <form:hidden path="scope"/>
				                        </div>
				                    </div>
				                    <div class="form-group">
				                        <label class="col-sm-2 control-label">区域类型</label>
				                        <div class="col-sm-3">
				                            <input type="radio" name="areaType" value="1" <c:if test="${routeRuleDto.areaType eq '1'}">checked</c:if>  checked="checked"/>国内
				                            <input type="radio" name="areaType" <c:if test="${routeRuleDto.areaType eq '2'}">checked</c:if> value="2"/>国际
				                        </div>
				                    </div>
				                    <div class="form-group">
				                        <label class="col-sm-2 control-label">是否禁用</label>
				                        <div class="col-sm-3">
				                            <form:checkbox path="isDisable" label="是" cssClass="check"/>
				                        </div>
				                    </div>
				                    <c:if test="${ not empty routeRuleDto.id}">
				                        <div class="form-group">
				                            <label class="col-sm-2 ">创建人:</label>
				                            <div class="col-sm-3">
				                                <label>${routeRuleDto.createrName }</label>
				                            </div>
				
				                            <label class="col-sm-2 ">创建时间:</label>
				                            <div class="col-sm-3">
				                                <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
				                                                       value="${routeRuleDto.createDate }"/></label>
				                            </div>
				                        </div>
				                        	<c:if test="${not empty  routeRuleDto.modifierName}">
					                        <div class="form-group">
					                            <label class="col-sm-2 ">最后修改人:</label>
					                            <div class="col-sm-3">
					                                <label>${routeRuleDto.modifierName }</label>
					                            </div>
					
					                            <label class="col-sm-2 ">最后修改时间:</label>
					                            <div class="col-sm-3">
					                                <label><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
					                                                       value="${routeRuleDto.lastUpdateDate }"/></label>
					                            </div>
					                        </div>
					                        </c:if>
				                      </c:if>
				                </form:form>
				            </div>
             <c:if test="${routeRuleDto.id != null}">
                <!-- 配置项-->
                <div class="col-sm-12">
                    <div class="page-header">
                        <h3>配置信息 |&nbsp;
                            <button class="layui-btn layui-btn-normal" onclick="getRouteRuleItem(${routeRuleDto.id},null,${routeRuleDto.areaType})">
                                <i class="layui-icon">&#xe61f;连接</i>
                            </button>
                        </h3>
                    </div>
                    <div>
                        <table class="table table-striped table-bordered" lay-skin="row" id="itemTable">
                            <thead>
                            <tr>
                                <td><label>连接名称</label></td>
                                <td><label>优先级</label></td>
                                <td><label>参与查询</label></td>
                                <td><label>自动出票</label></td>
                                <td><label>状态</label></td>
                                <td><label>操作</label></td>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${routeRuleDto.routeRuleItems}" var="item">
                                <tr>
                                    <td>${item.hubsCode.concat("-").concat(item.hotelHubsConnectResult.hubsConnectName)}</td>
                                    <td>${item.priority}</td>
                                    <td><input type="checkbox" <c:if test="${item.query}">checked</c:if> disabled></td>
                                    <td><input type="checkbox" <c:if test="${item.autoIssue}">checked</c:if> disabled></td>
                                    <td>${item.isDisable?"禁用":"启用"}</td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-sm"
                                                onclick="getRouteRuleItem(${routeRuleDto.id},${item.id},${routeRuleDto.areaType})">编辑
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm"
                                                onclick="deleteItem(${item.id},${routeRuleDto.id})">删除
                                        </button>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
             </c:if>
             </div>
         </div>
</div>
<script type="text/javascript" src="resource/plugins/json-utils/jquery.serializejson.min.js"></script>
<script type="text/javascript">
    $(function () {
        ruleGradeChange('${routeRuleDto.ruleGrade}')
        $('input:radio[name="ruleGrade"]').change(function () {
            ruleGradeChange($(this).val());

            //清空内容
            $("[name='scopeName']").val("");
            $("[name='scope']").text("");
        })

        function ruleGradeChange(v){
            var type = 0;
            if (v == 'PLATFORM') {
                type = 1;
            }
            $(".partner").click(function () {
                TR.select('partner', {
                    type: type
                }, function (result) {
                    $("[name='scopeName']").val(result.name);
                    $("[name='scope']").val(result.id);
                });
            });
        }


    })

    $("#ruleForm").validate(
        {
            rules : {
                ruleName : {
                    required : true,
                    maxlength : 20
                },
                ruleGrade : {
                    required : true,
                },
                scope : {
                    required : true,
                },
              
            },
            submitHandler : function(form) {
                submitRule();
            },
            errorPlacement : setErrorPlacement,
            success : validateSuccess,
            highlight : setHighlight
        }
    );

    //提交表单
    function submitRule() {
        $.ajax({
            cache: true,
            type: "POST",
            url: "hotel/routeRule/save",
            data: $('#ruleForm').serialize(),
            async: false,
            error: function (request) {
                showErrorMsg("请求失败，请刷新重试");
            },
            success: function (result) {
            	 debugger;
            	 if (result.code == '0') {
            		 
            		 layer.alert(result.msg, {icon: 1})
                    getRule(result.data.id);
                 } else {
                     layer.alert(result.msg, {icon: 2})
                 }
            }
        });
    }
  //配置项详情页
    function getRouteRuleItem(routeRuleId, id,areaType) {
        var url = "hotel/routeRule/getRouteRuleItem?routeRuleId=" + routeRuleId+"&areaType="+areaType;
        if (id) {
            url += "&id=" + id;
        }
        layer.open({
            type: 2,
            area: ['900px', '600px'],
            title: "连接配置",
            closeBtn: 1,
            content: url,
        });
    }
  


</script>