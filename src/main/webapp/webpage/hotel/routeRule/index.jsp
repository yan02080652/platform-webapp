<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="ruleMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>路由规则   &nbsp;&nbsp;
			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12">
										<div class="pull-left">
											<a class="btn btn-default" onclick="getRule('')" role="button">新增规则</a>
											<a class="btn btn-default" onclick="batchDeleteRule()" role="button">批量删除</a>
										</div>
										 <form class="form-inline" method="post">
                                            <nav class="text-right">
                                                <div class=" form-group">
                                                    <label class="control-label">规则级别</label>
                                                    <select class="form-control" id="routeRuleGrade">
                                                        <option value="">全部</option>
                                                        <c:if test="${not empty gradeMap }">
                                                            <c:forEach items="${gradeMap }" var="grade">
                                                                <option value="${grade}">${grade.message }</option>
                                                            </c:forEach>
                                                        </c:if>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">企业名称</label>
                                                    <div class="input-group ">
                                                        <input type="hidden" name="pid" id="pid"/>
                                                        <input type="text" id="pname" class="form-control"
                                                               placeholder="请选择企业" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">规则类型</label>
                                                    <select class="form-control" id="regionType">
                                                        <option value="">全部</option>
                                                        <option value="1">国内</option>
                                                        <option value="2">国际</option>

                                                    </select>
                                                </div>
                                                <div class="form-group" style="margin-left: 20px">
                                                    <button type="button" class="btn btn-default"
                                                            onclick="reloadRouteRule()">查询
                                                    </button>
                                                    <button type="button" class="btn btn-default"
                                                            onclick="reset_rule()">重置
                                                    </button>
                                                </div>
                                            </nav>
                                        </form>
										</div>
									</div>
									<div  id="rule_Table" style="margin-top: 15px">
					
					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 详细页面 -->
<div id="ruleDetail" style="display:none;"></div>
<script type="text/javascript" src="webpage/hotel/routeRule/routeRule.js?version=${globalVersion}"></script>