<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    body {
        background-color: #FFFFFF;
    }

    .layui-form-label {
        box-sizing: content-box;
    }

    .box {
        padding-top: 20px;
    }

    .layui-form-switch {
        width: 52px;
    }

    .carrierBtn {
        margin-top: 3px;
    }

    .codes {
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        width: 500px
    }

</style>
<div class="box">
    <form class="layui-form" action="">
        <input type="hidden" name="id" value="${itemDto.id}"/>
        <input type="hidden" id="routeRuleId" name="routeRuleId" value="${routeRuleId}"/>
        <input type="hidden" name="hubsCode" value="${itemDto.hubsCode}"/>
        <input type="hidden" name="hubsConnectId" value="${itemDto.hubsConnectId}"/>
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">优先级</label>
                <div class="layui-input-inline">
                    <input type="text" name="priority" value="${itemDto.priority}" lay-verify="required|positiveNum"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">连接账号</label>
                <div class="layui-input-inline">
                    <select name="connectAcc" lay-verify="select|required" lay-filter="connectAcc">
                        <option value="">请选择</option>
                        <c:forEach items="${connectMap}" var="map">
                            <optgroup label="${map.key}">
                                <c:forEach items="${map.value}" var="hubsConnect">
                                    <option value="${hubsConnect.hubsCode.concat("-").concat(hubsConnect.id)}"
                                            <c:if test="${itemDto.hubsConnectId eq hubsConnect.id}">selected</c:if> >${hubsConnect.hubsConnectName}</option>
                                </c:forEach>
                            </optgroup>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">参与查询</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="query" title="开启" value="1"
                           <c:if test="${itemDto.query}">checked</c:if>>
                </div>
            </div>
            
        </div>

        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">自动出票</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="autoIssue" title="开启" value="1"
                           <c:if test="${itemDto.autoIssue}">checked</c:if>>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <input type="checkbox" name="isDisable" lay-skin="switch" lay-text="启用|禁用" value="0"
                       <c:if test="${!itemDto.isDisable}">checked</c:if>>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit="" lay-filter="itemFrom">保存</button>
                <button onclick="closeIframe()" class="layui-btn layui-btn-primary">取消</button>
            </div>
        </div>
    </form>
</div>
<script>
    //保存已选清单
    var carrierData = [];
    var form;
    $(function () {
        layui.use('form', function () {
            form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
            //验证规则
            form.verify({
                positiveNum: [/[1-9]\d*$/, '优先级必须正整数']
            });
            //监听select
            form.on('select(connectAcc)', function (data) {
                var hubsCode = data.value.split("-")[0];
                render(hubsCode);
            });

            //监听提交
            form.on('submit(itemFrom)', function (data) {
                //保存配置项
                var formData = dataHandler(data.field);
                $.ajax({
                    url: "hotel/routeRule/saveRouteRuleItem",
                    data: formData,
                    type: "post",
                    success: function (result) {
                        if (result) {
                        	parent.getRule($("#routeRuleId").val());
                            closeIframe();
                        } else {
                            layer.msg("保存失败，请稍后重试！", {icon: 2});
                        }
                    }
                })
                return false;
            });

            render('${itemDto.hubsCode}');
        });
    })

    //根据hubsCode查询hub,控制页面效果
    function render(hubsCode) {
        $.get("hotel/hubs/getHubByCode?code=" + hubsCode, function (result) {
            if (result) {
                var query = result.isSupportSearch;
                var checkPrice = result.isCanCheckprice;
                var payAfterSeat = result.isCanSeat;
                var autoIssue = result.isSupportDraw;
                if (!query) {
                    $("input[name='query']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='query']").prop('disabled', false);
                }
                if (!autoIssue) {
                    $("input[name='autoIssue']").prop('checked', false).prop('disabled', true);
                } else {
                    $("input[name='autoIssue']").prop('disabled', false);
                }
                //更新页面渲染
                form.render();
            }
        })
    }

    //关闭iframe
    function closeIframe() {
        //先得到当前iframe层的索引,再执行关闭
        var index = parent.layer.getFrameIndex(window.name);
        parent.layer.close(index);
    }

    //表单数据处理
    function dataHandler(data) {
        if (!data.query) {
            data.query = "0";
        }
        if (!data.autoIssue) {
            data.autoIssue = "0";
        }
        if (!data.isDisable) {
            data.isDisable = "1";
        }
        var split = data.connectAcc.split("-");
        data.hubsCode = split[0];
        data.hubsConnectId = split[1];
        return data;
    }
    
    
    
    
</script>