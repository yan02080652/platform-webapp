$(function(){
	reloadRouteRule();
	$("#pname").click(function () {
	    TR.select('partner', {
	        //type:0//固定参数
	    }, function (data) {
	        $("#pid").val(data.id);
	        $("#pname").val(data.name);
	        reloadRouteRule();
	    });
	});
})

function reloadRouteRule(pageIndex){
	//加载列表
	$.post('hotel/routeRule/list',{
		ruleGrade:$("#routeRuleGrade").val(),
		pid:$("#pid").val(),
		areaType:$("#regionType").val(),
		pageIndex:pageIndex
		},function(data){
		$("#rule_Table").html(data);
	});
}

//转到添加或修改页面
function getRule(id){
	$.post("hotel/routeRule/getRule",{'id':id},function(data){
		$("#ruleDetail").html(data);
		$("#ruleMain").hide();
		$("#ruleDetail").show();
	});
}

//查找用户
function search_rule(){
	reloadRouteRule();
}

//重置
function reset_rule(){
	$("#routeRuleGrade").val("");
	$("#pid").val("");
	$("#pname").val("");
	$("#regionType").val("");
	reloadRouteRule();
}

//checkbox全选
function selectAllRule(){
	if ($("#selectAllRule").is(":checked")) {
		 $("#ruleTable input[type=checkbox]").prop("checked", true);//所有选择框都选中
    } else {
    	 $("#ruleTable input[type=checkbox]").prop("checked", false);
    }
}

//返回
function backRuleMain(){
	window.location.reload();
	//validator.resetForm();
	//$("#ruleDetail").hide().html("");
	//$("#ruleMain").show();
	//reloadRouteRule();
}

//批量删除
function batchDeleteRule(){
    //判断至少选了一项
    var checkedNum = $("#ruleTable input[name='ids']:checked").length;
    if(checkedNum==0){
        $.alert("请选择你要删除的记录!","提示");
        return false;
    }
    
    var ids = new Array();
    $("#ruleTable input[name='ids']:checked").each(function(){
    	ids.push($(this).val());
    });
    
    confirmDeleteRule(ids.toString());
}

//单个删除
function deleteRule(id) {
	confirmDeleteRule(id);
};

//确认删除
function confirmDeleteRule(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除所选记录?",
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "hotel/routeRule/delete",
				data : {
					ids : id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadRouteRule();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}


function deleteItem(id,ruleId) {
    layer.confirm('你确定要删除该配置项吗?', function (index) {
        layer.close(index);
        var loadIndex = layer.load();
        $.ajax({
            type: "get",
            url: "hotel/routeRule/deleteRouteRuleItem?id=" + id,
            success: function (result) {
                layer.close(loadIndex);
                if (!result) {
                    layer.msg("删除失败，请稍后重试!", {icon: 2, time: 2000})
                    return false;
                }
                getRule(ruleId);
            },
            error: function () {
                layer.msg("系统错误，请稍后刷新重试！", {icon: 2, time: 2000});
                layer.close(loadIndex);
            }
        });
    });
}


