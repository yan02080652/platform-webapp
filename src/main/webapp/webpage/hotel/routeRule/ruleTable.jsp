<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" id="ruleTable">
	<thead>
		<tr>
		  <th><input type="checkbox" onchange="selectAllRule()" id="selectAllRule" /></th>
	      <th><label>规则名称</label></th>
          <th><label>规则级别</label></th>
		  <th><label>适用范围</label></th>
		  <th><label>区域类型</label></th>
		  <th><label>创建用户</label></th>
		  <th><label>创建时间</label></th>
		  <th><label>修改用户</label></th>
		  <th><label>修改时间</label></th>

		  <th><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="bean">
         <tr class="odd gradeX">
        	 <td><input type="checkbox" name="ids" value="${bean.id }"/></td>
			 <td>${bean.ruleName }</td>
			 <td>${bean.ruleGrade.message }</td>
			 <td>${bean.scopeName }</td>
			 <td>${bean.areaType=='1' ? '国内' : '国际'}</td>
			 <td>${bean.createrName}</td>
			 <td><fmt:formatDate value="${bean.createDate }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
			 <td>${bean.modifierName}</td>
			 <td><fmt:formatDate value="${bean.lastUpdateDate }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
             <td class="text-center" >
             	<a onclick="getRule('${bean.id }')" >修改</a> |
             	<a onclick="deleteRule('${bean.id }');">删除</a> 
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadRouteRule"></jsp:include> 
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:50px");
})
</script>