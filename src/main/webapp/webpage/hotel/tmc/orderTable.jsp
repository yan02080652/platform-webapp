<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>


<!-- 订单状态选项 -->
	<div class="tab" style="margin-bottom: 5px">
		<ul class="fix" id="orderStatus">
			<c:forEach items="${orderStatuslist }" var="orderStatus">
				<c:choose>
					<c:when test="${orderStatus.orderShowStatus eq currentOrderStatus }">
						<li class="active"><span data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.status }(${orderStatus.count })</span></li>
					</c:when>
					<c:otherwise>
						<li ><span data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.status }(${orderStatus.count })</span></li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</div>

<!-- 订单列表 -->

	<table class="table table-bordered" id="orderTable">
		<thead>
			<tr>
				<th style="width: 20%">酒店信息</th>
				<th style="width: 20%">入住人信息</th>
				<th style="width: 20%">收支信息</th>
				<th style="width: 20%">处理信息</th>
				<th style="width: 20%">供应信息</th>
			</tr>
		</thead>
		<tbody>
		
		<c:set var="nowDate" value="<%=System.currentTimeMillis()%>"></c:set>
		
			<c:forEach items="${pageList.list }" var="hotelOrder">
					<tr>
						<td colspan="5">
							<span>企业：${hotelOrder.cName } </span>
							<span>预订人：${hotelOrder.userName }</span>
							<span>订单号：<font color="blue"><a href="/order/hotelDetail/${hotelOrder.id }" target="_Blank">${hotelOrder.id }</a></font></span>
							<span>来源:<font color="blue">${hotelOrder.orderOriginType.message}</font></span>
							<span>标签：${hotelOrder.orderShowStatus.status }</span>
							<span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${hotelOrder.createDate }" /></span>
						    <span><a onclick="logList('${hotelOrder.id}')">日志</a></span>
						    <%-- <c:if test="${flightOrder.orderShowStatus eq 'IN_ISSUE' and (nowDate-flightOrder.lastUpdateDate.time)/(1000*60) > 5}">
						    	&nbsp;&nbsp;
						    	<span><font color="red">订单出票超时！</font></span>
						    </c:if> --%>
					    </td>
					</tr>
					<tr>
						<td>
							<span>
								<fmt:formatDate value="${hotelOrder.arrivalDate }" pattern="yyyy-MM-dd"/> - 
								<fmt:formatDate value="${hotelOrder.departureDate }" pattern="yyyy-MM-dd"/>
								<span> ${hotelOrder.stayDay} 晚</span>
							</span>
							<br/>
							<span>${hotelOrder.hotelName }</span>
							<br/>
							<span>${hotelOrder.roomName }</span>&nbsp;&nbsp;<span>${hotelOrder.roomNum }间</span>
							<br/>
							<c:if test="${not empty hotelOrder.noteToHotel}">
								<a onclick="showNoteToHotel('${hotelOrder.noteToHotel}')">客户备注</a>
							</c:if>
						</td>
						<td>
							<span>
							 ${hotelOrder.customerNames}
						    </span>
						</td>
						<td>
							<span>应收￥${hotelOrder.finalTotalPrice }</span>
							<span>
								<c:choose>
									<c:when test="${hotelOrder.paymentStatus eq 'PAYMENT_SUCCESS'}">已收</c:when>
									<c:otherwise>未收</c:otherwise>
								</c:choose>
							</span><br/>
								<span>应付 ${hotelOrder.currencyCode} ${hotelOrder.totalCost }</span>
							<span>
								<c:choose>
									<c:when test="${hotelOrder.supPay}">已付</c:when>
									<c:otherwise>未付</c:otherwise>
								</c:choose>
							</span><br/>
							<c:if test="${not empty hotelOrder.paymentPlanNo }">
								<span><a onclick="loadIncomeDetail('${hotelOrder.id}')">明细</a></span>
							</c:if>
						</td>
						<td>
							<span>
								${hotelOrder.taskInfo}<br/>
								<c:if test="${not empty hotelOrder.processTag }">
									${hotelOrder.processTag.msg }:
								</c:if>
								<c:if test="${hotelOrder.mark!=null and hotelOrder.mark eq 'DIRECT' }">
									<span style="color:red"> 在途协议酒店</span><br/>
								</c:if>
								${hotelOrder.orderShowStatus.status } - ${hotelOrder.orderStatus.status }
							</span><br/>
							<span>
								<c:if test="${hotelOrder.processTag eq 'SYSTEMPROCESSING' }"><a onclick="receive()">客服介入</a></c:if>
								<c:if test="${hotelOrder.processTag eq 'CUSTOMERSERVICEPROCESSING' }">${hotelOrder.orderTaskDto.operatorName }</c:if>
							</span>
						 </td>
						<td>
							<c:if test="${hotelOrder.operationLog.status eq 'FAIL' }">
								<a data-trigger="tooltip" data-content="失败原因：${hotelOrder.operationLog.description }">
								<font color="red">${hotelOrder.operationLog.type.message }${hotelOrder.operationLog.status.message }</font></a><br/>
							</c:if>
							${hotelOrder.issueChannelDto.name}&nbsp;&nbsp;
							<c:if test="${not empty hotelOrder.externalOrderId }">
								<a data-trigger="tooltip" data-content="${hotelOrder.externalOrderId }">订单号</a><br/>
								状态: ${hotelOrder.externalOrderShowStatus.message } <br/>
							</c:if>
							<c:if test="${hotelOrder.orderShowStatus eq 'CONFIRMING' or hotelOrder.orderShowStatus eq 'CONFIRMED'}">
								<a onclick="cancelOrder('${hotelOrder.id}','${hotelOrder.orderTaskDto.id}')"> 取消订单</a>
							</c:if>
						</td>
					</tr>
			</c:forEach>
		</tbody>
	</table>
<div class="pagin" style="margin-bottom: 20px">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadOrderList"></jsp:include>
</div>

<script>
$("#orderStatus li").click(function() {
	submitForm(null,$(this).find("span").attr("data-value"));
});
</script>