<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<table class="table table-bordered  table-striped table-hover" style="margin:0 auto;width:98%">
	<thead>
		<tr>
			<th width="10%">操作人</th>
			<th width="15%">操作类型</th>
			<th width="10%">状态</th>
			<th width="40%">描述</th>
			<th width="20%">操作时间</th>
		</tr>
	</thead>
	<tbody>
	
		<c:forEach items="${logList }" var="log">
			<tr>
				<td>${log.operator }</td>
				<td>${log.type.message }</td>
				<td>${log.status.message }</td>
				<td>${log.description }</td>
				<td><fmt:formatDate value="${log.operationDate }"  pattern="yyyy-MM-dd HH:mm:ss"/></td>
			</tr>
		 </c:forEach>
	</tbody>
</table>