<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<table class="table table-bordered  table-striped table-hover">
	<thead>
	<tr>
		<th>支付通道</th>
		<th>支付金额</th>
	</tr>
	</thead>
	<tbody>
	<c:forEach items="${incomeDetail}" var="in">
		<tr>
			<td>${in.tradeType}</td>
			<td><fmt:formatNumber value="${in.amount}" type="currency"/> </td>
		</tr>
	</c:forEach>
	</tbody>
</table>