<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<style>
    .hideHoverSub {
        max-width: 320px;
    }

    .detailItem {
        float: left;
        width: 80px;
        font-size: 12px;
        background-color: #fff;
        color: #3e3a39;
    }

    .detailItem > p {
        height: 20px;
        line-height: 20px;
        text-align: center;
        margin: 0 0 0;
    }

    .itemPrice span {
        color: #ff6700;
        font-weight: bold;
    }

    .itemD {
        background: #dddddd;
        border: 1px solid #ddd;
    }

    .detailItemSub {
        text-align: center;
        border: 1px solid #f7f7f7;
        line-height: 20px;
        padding: 8px 0;
    }
</style>
<div class="modal fade" id="ticket_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="flush()">&times;</button>
                <h4 class="modal-title">
                    <span class="label label-primary">${hotelOrderDto.hotelName }</span>
                    <span class="label label-success">${hotelOrderDto.roomName } ${hotelOrderDto.roomNum }间</span>
                    <span class="label label-info"><fmt:formatDate value="${hotelOrderDto.arrivalDate }"
                                                                   pattern="yyyy-MM-dd"/> ~ <fmt:formatDate
                            value="${hotelOrderDto.departureDate }"
                            pattern="yyyy-MM-dd"/> ${hotelOrderDto.stayDay }晚</span>
                </h4>
            </div>
            <div class="modal-body col-md-12">
                <div class="">
                    <span class="col-md-3 control-label ">订单号:&nbsp;&nbsp;${hotelOrderDto.id }</span>
                    <span class="col-md-3 control-label ">标签:&nbsp;&nbsp;${hotelOrderDto.orderShowStatus.status }</span>
                    <span class="col-md-3 control-label ">订单状态:&nbsp;&nbsp;${hotelOrderDto.orderStatus.status }</span>
                    <span class="col-md-3 control-label ">支付状态:&nbsp;&nbsp;${hotelOrderDto.paymentStatus.status }</span>
                    <span class="col-md-6 control-label ">企业:&nbsp;&nbsp;${hotelOrderDto.cName }</span>
                    <span class="col-md-6 control-label ">预订人:&nbsp;&nbsp;${hotelOrderDto.userName }</span>
                </div>
                <!-- <h4>订单产品信息</h4> -->
                <%-- <div>
                    <span class="label label-primary">${hotelOrderDto.hotelName }</span>
                    <span class="label label-success">${hotelOrderDto.roomName } ${hotelOrderDto.roomNum }间</span>
                    <span class="label label-info"><fmt:formatDate value="${hotelOrderDto.arrivalDate }" pattern="yyyy-MM-dd"/> ~ <fmt:formatDate value="${hotelOrderDto.departureDate }" pattern="yyyy-MM-dd"/> ${hotelOrderDto.stayDay }晚</span>
                </div> --%>

                <h4 class="col-md-5 label label-warning">新单产品信息</h4>
                <span class="col-md-1"></span>
                <h4 class="col-md-6 label label-default">原单产品信息</h4>
                <div class="col-md-6">
                    <form:form cssClass="form-horizontal " id="ticketForm" role="form" modelAttribute="hotelOrderDto"
                               action="hotel/tmc/repair.html">
                        <form:hidden path="id"/>
                        <form:hidden path="issueChannelMsg"/>
                        <input type="hidden" id="needAgain" name="needAgain"/>
                        <input type="hidden" name="taskId" value="${taskId }">
                        <c:forEach items="${hotelOrderDto.customers }" var="customer" varStatus="status">
                            <input type="hidden" name="customers[${status.index }].id" value="${customer.id }">
                        </c:forEach>
                        <div class="form-group">
                            <label class="col-md-3 control-label">供应商</label>
                            <div class="col-md-7">
                                <form:select path="issueChannel" id="hotelOriginType" cssClass="form-control">
                                    <form:options items="${dictCodes }" itemLabel="name" itemValue="code"/>
                                </form:select>
                                    <%--<select name="hotelOriginType" Class="form-control">
                                        <c:forEach items="${channelList }" var="channel">
                                            <option value="${channel.code }">${channel.name }</option>
                                        </c:forEach>
                                    </select>--%>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">供应商单号</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="externalOrderId" name="externalOrderId"
                                       placeholder="请输入外部平台订单号"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">酒店确认号</label>
                            <div class="col-md-7">
                                <input type="text" class="form-control" id="confirmNum" name="confirmNum"
                                       placeholder="请输入酒店确认号"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">退改规则</label>
                            <div class="col-md-7">
                                <form:textarea path="prepayNote" cssClass="form-control" placeholder="请输入预付规则"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">备注</label>
                            <div class="col-md-7">
                                <form:textarea path="orderNote" cssClass="form-control" placeholder="请输入备注"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">供应商结算价</label>
                            <div class="col-md-7">
                                <div class="hideHoverSub fix">
                                    <c:forEach items="${hotelOrderDto.nightlyRates }" var="bean" varStatus="status">
                                        <div class="detailItem">
                                            <fmt:formatDate value="${bean.checkInDate }" pattern="EEEE" var="inDate"/>
                                            <p class="itemD"><span><fmt:formatDate value="${bean.checkInDate }"
                                                                                   pattern="MM-dd"/> </span>
                                                （<span>${fn:substring(inDate,2,3) }</span>）</p>
                                            <div class="detailItemSub">
                                                <p class="itemB">
                                                    <c:choose>
                                                        <c:when test="${bean.breakfastCount eq 1}">单早</c:when>
                                                        <c:when test="${bean.breakfastCount eq 2}">双早</c:when>
                                                        <c:when test="${bean.breakfastCount eq 3}">三早</c:when>
                                                        <c:when test="${bean.breakfastCount eq 4}">四早</c:when>
                                                        <c:otherwise>无早</c:otherwise>
                                                    </c:choose>
                                                </p>
                                                <input type="hidden" name="nightlyRates[${status.index }].id"
                                                       value="${bean.id }">
                                                <p class="itemPrice">¥<input
                                                        name="nightlyRates[${status.index }].dayRate" maxlength="7"
                                                        style="width:50px;border-radius:3px;" class="date_rate"></p>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </form:form>
                </div>
                <div class="col-md-6" style="">
                    <div class="row">
                        <label class="col-md-3 control-label">供应商:</label><span class="col-md-9">${hotelOrderDto.originName}</span>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">供应商单号:</label><span
                            class="col-md-9">${hotelOrderDto.externalOrderId }</span>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">酒店确认号:</label><span
                            class="col-md-9">${hotelOrderDto.customers[0].confirmNum }</span>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">退改规则:</label><span
                            class="col-md-9">${hotelOrderDto.orderPrepayRule.description }</span>
                    </div>
                    <div class="row">
                        <label class="col-md-3 control-label">备注:</label><span
                            class="col-md-9">${hotelOrderDto.orderNote }</span>
                    </div>
                    <div class="hideHoverSub fix">
                        <c:forEach items="${hotelOrderDto.nightlyRates }" var="bean" varStatus="status">
                            <div class="detailItem">
                                <fmt:formatDate value="${bean.checkInDate }" pattern="EEEE" var="inDate"/>
                                <p class="itemD"><span><fmt:formatDate value="${bean.checkInDate }"
                                                                       pattern="MM-dd"/> </span>
                                    （<span>${fn:substring(inDate,2,3) }</span>）</p>
                                <div class="detailItemSub">
                                    <p class="itemB">
                                        <c:choose>
                                            <c:when test="${bean.breakfastCount eq 1}">单早</c:when>
                                            <c:when test="${bean.breakfastCount eq 2}">双早</c:when>
                                            <c:when test="${bean.breakfastCount eq 3}">三早</c:when>
                                            <c:when test="${bean.breakfastCount eq 4}">四早</c:when>
                                            <c:otherwise>无早</c:otherwise>
                                        </c:choose>
                                    </p>
                                    <p class="itemPrice">¥<span
                                            style="width:50px;border-radius:3px;">${bean.dayRate }</span></p>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <p>
                        <span class="col-md-4 ">销售总价:¥<span style="color: #ff6700;"
                                                            class="total_sale">${hotelOrderDto.totalPrice }</span></span>
                        <span class="col-md-5 ">结算总价:${hotelOrderDto.currencyCode}${hotelOrderDto.totalCost }</span>
                    </p>
                </div>
            </div>
            <div>
                <input type="hidden" id="totalCost" name="totalCost">

                <span class="col-md-2 ">结算总价:¥<span style="color: #ff6700;" class="total_cost">0.00</span></span>
                <span class="col-md-2 ">补单利润:¥<span style="color: #ff6700;" class="total_win">0.00</span></span>
                <span class="col-md-2 ">原订单利润:¥${hotelOrderDto.totalPrice - hotelOrderDto.outerOrder.totalPriceExchanged }</span>
                <c:if test="${againExist eq false and hotelOrderDto.manual eq false and hotelOrderDto.mark ne 'DIRECT' }">
                    <span class="col-md-2 ">
                        是否再次向酒店确认:
                    </span>
                    <span class="col-md-2 ">
                        <input type="radio" name="needAgain" value="1" checked="checked">  需要
                        <input type="radio" name="needAgain" value="0">  不需要
                    </span>
                </c:if>

                <div class="modal-footer ">
                    <button type="button" class="btn btn-default" onclick="submitTicketForm();">确认</button>
                    <button type="button" class="btn btn-default"
                            onclick="cancelSubmit(${hotelOrderDto.id}, ${taskId})">放弃
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.date_rate').on('keyup', function () {
            var cost = $(this).val();
            var reg = /^\d+\.?(\d{1,2})?$/;
            while (!reg.test($(this).val()) && $(this).val() != "") {
                $(this).val(checkStr($(this).val()));
            }
            var $rates = $('.date_rate');
            var totalCost = 0;
            for (var i = 0; i < $rates.length; i++) {
                if ($rates.eq(i).val() != '') {
                    var price = 100 * $rates.eq(i).val();
                    totalCost += price;
                }
            }
            $('.total_cost').text(totalCost / 100)
            $('#totalCost').val(totalCost / 100);
            $('.total_win').text((100 * $('.total_sale').text() - totalCost) / 100);

        })

        //检查是否符合金额格式:只能输入数字且最多保留小数点后两位
        function checkStr(str) {
            str = str.substring(0, str.length - 1);
            return str;
        }
    })
</script>