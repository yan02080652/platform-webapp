<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<!-- 状态栏 -->
<input type="hidden" id="currentTaskType" />
<div >
	<!-- 左边状态 -->
	<div class="tab pull-left">
			<ul class="fix" id="myTaskStatusTab">
				<li class="tab1 active" data-value="PROCESSING" onclick="submitAjax(null,'PROCESSING')">处理中(${typeMap.PROCESSING==null?0:typeMap.PROCESSING})</li>
				<li class="tab2" data-value="ALREADY_PROCESSED" onclick="submitAjax(null,'ALREADY_PROCESSED')">近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})</li>
			</ul>
		<div id="h_my_content">
			<div class="content1">
				<p>
					<a onclick="submitAjax(null,'PROCESSING','WAIT_URGE')">待催单(${typeMap.WAIT_URGE==null?0:typeMap.WAIT_URGE })</a>
					<a onclick="submitAjax(null,'PROCESSING','WAIT_CONFIRM')">待确认(${typeMap.WAIT_CONFIRM==null?0:typeMap.WAIT_CONFIRM })</a>
					<a onclick="submitAjax(null,'PROCESSING','CONFIRM_AGAIN')">酒店确认(${typeMap.CONFIRM_AGAIN==null?0:typeMap.CONFIRM_AGAIN })</a>
					<a onclick="submitAjax(null,'PROCESSING','WAIT_TICKET')">待出票(${typeMap.WAIT_TICKET==null?0:typeMap.WAIT_TICKET })</a>
					<a onclick="submitAjax(null,'PROCESSING','WAIT_REFUND')">待退款(${typeMap.WAIT_REFUND==null?0:typeMap.WAIT_REFUND })</a>
				</p>
			</div>
            <div class="content2">
                <p>&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_URGE')">催单(${alreadyMap.WAIT_URGE==null?0:alreadyMap.WAIT_URGE })</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_CONFIRM')">确认(${alreadyMap.WAIT_CONFIRM==null?0:alreadyMap.WAIT_CONFIRM })</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a onclick="submitAjax(null,'ALREADY_PROCESSED','CONFIRM_AGAIN')">酒店确认(${alreadyMap.CONFIRM_AGAIN==null?0:alreadyMap.CONFIRM_AGAIN })</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_TICKET')">出票(${alreadyMap.WAIT_TICKET==null?0:alreadyMap.WAIT_TICKET })</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_REFUND')">退款(${alreadyMap.WAIT_REFUND==null?0:alreadyMap.WAIT_REFUND })</a>
                </p>
            </div>
		</div>
     </div>

</div>


<!-- 任务列表 -->
<div>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th style="width: 20%">酒店信息</th>
				<th style="width: 20%">入住人信息</th>
				<th style="width: 20%">收支信息</th>
				<th style="width: 20%">处理信息</th>
				<th style="width: 20%">供应信息</th>
			</tr>
		</thead>
		<tbody id="myTaskTbody">
			<c:forEach items="${pageList.list }" var="orderTask" varStatus="index">
				<tr>
					<td colspan="5">
						<span>企业：${orderTask.orderDto.cName } </span>
						<span>预订人：${orderTask.orderDto.userName }</span>
						<span>订单号：<font color="blue"><a href="/order/hotelDetail/${orderTask.orderDto.id }" target="_Blank">${orderTask.orderDto.id }</a></font></span>
						<span>来源:<font color="blue">${orderTask.orderDto.orderOriginType.message}</font></span>
						<span>标签：${orderTask.orderDto.orderShowStatus.status }</span>
						<span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${orderTask.orderDto.createDate }" /></span>
					    <span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span>
					    <span>任务创建时间:<font color="blue"><fmt:formatDate value="${orderTask.createDate }" pattern="yyyy-MM-dd HH:mm:ss"/></font></span>
					    <span hidden><font color="red">任务超时</font></span>
					    <input class="proDate" type="hidden" value='<fmt:formatDate value="${orderTask.maxProcessDate }" pattern="yyyy-MM-dd HH:mm:ss"/>'>
					    <c:if test="${orderTask.taskStatus == 'PROCESSING' }">
						    <label>任务延时</label>
						    <select  onchange="delay('${orderTask.id }',$(this).val())">
						    	<option value="-1">--
						    	<option value="5">5分钟
						    	<option value="15">15分钟
						    	<option value="60">1小时
						    	<option value="1440">1天
						    </select>
					    </c:if>
				    </td>
				</tr>
				<tr>
					<td>
						<span>
							<fmt:formatDate value="${orderTask.orderDto.arrivalDate }" pattern="yyyy-MM-dd"/> - 
							<fmt:formatDate value="${orderTask.orderDto.departureDate }" pattern="yyyy-MM-dd"/>
							<span> ${orderTask.orderDto.stayDay} 晚</span>
						</span>
						<br/>
						<span>${orderTask.orderDto.hotelName }</span>
						<br/>
						<span>${orderTask.orderDto.roomName }</span>&nbsp;&nbsp;<span>${orderTask.orderDto.roomNum }间</span>
						<br/>
						<!--  客人填写的备注-->
						<c:if test="${not empty orderTask.orderDto.noteToHotel}">
							<a onclick="showNoteToHotel('${orderTask.orderDto.noteToHotel}')">客户备注</a>
						</c:if>
					</td>
					<td>
						<span>
							<c:forEach items="${orderTask.orderDto.customers }" var="customer" varStatus="status">
									<c:if test="${!status.last }">${customer.name }/</c:if>
									<c:if test="${status.last }">${customer.name }</c:if>
							</c:forEach>
						</span>
					</td>
					<td>
						<span>应收￥${orderTask.orderDto.finalTotalPrice }</span>
						<span>
							<c:choose>
								<c:when test="${orderTask.orderDto.paymentStatus eq 'PAYMENT_SUCCESS'}">已收</c:when>
								<c:otherwise>未收</c:otherwise>
							</c:choose>
						</span><br/>
						<span>应付 ${orderTask.orderDto.currencyCode} ${orderTask.orderDto.totalCost }</span>
						<span>
							<c:choose>
								<c:when test="${orderTask.orderDto.supPay}">已付</c:when>
								<c:otherwise>未付</c:otherwise>
							</c:choose>
						</span><br/>
						<c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
								<span><a onclick="loadIncomeDetail('${orderTask.orderDto.id}')">明细</a></span>
						</c:if>
					</td>
					<td>
						<span>任务：${orderTask.taskType.message} ${orderTask.taskStatus.message }</span><br/>
						<span>${orderTask.operatorName }</span><br/>
						<c:if test="${orderTask.orderDto.protocol}">
							<span>协议代码: ${orderTask.orderDto.noteToGuest}</span><br/>
						</c:if>
						<c:if test="${orderTask.orderDto.mark!=null and orderTask.orderDto.mark eq 'DIRECT' }">
							<span style="color:red"> 在途协议酒店</span><br/>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'PROCESSING' }">
							<span>
								<a onclick="transfer('${orderTask.id}','${orderTask.operatorId }')">交接</a>
								<!-- 待催单 -->
								<c:if test="${orderTask.taskType eq 'WAIT_URGE'}">
									 <a onclick="urgeDone('${orderTask.orderDto.id}','${orderTask.id}')"> | 已催单</a>
									 <a onclick="confirmDone('${orderTask.orderDto.id}','${orderTask.id}')"> | 已完毕</a>
								</c:if>
								<!-- 待确认 -->
								<c:if test="${orderTask.taskType eq 'WAIT_CONFIRM'}">
									 <a onclick="confirm('${orderTask.orderDto.id}','${orderTask.id}')"> | 已确认</a>
									<a onclick="cancelOrder('${orderTask.orderDto.id}','${orderTask.id}')"> | 拒单</a>
								</c:if>
								<!-- 待补单 -->
								<c:if test="${orderTask.taskType eq 'WAIT_TICKET'}">
									<a onclick="handRepair('${orderTask.orderDto.id}','${orderTask.id }')"> | 外采</a>
									<a onclick="cancelSubmit('${orderTask.orderDto.id}','${orderTask.id}')"> | 拒单退款</a>
								</c:if>
								<c:if test="${orderTask.taskType eq 'CONFIRM_AGAIN'}">
									<a onclick="confirmAgainDone('${orderTask.orderDto.id}','${orderTask.id}')"> | 已确认</a>
									<%--<a onclick="handRepair('${orderTask.orderDto.id}','${orderTask.id }')"> | 外采</a>--%>
									<a onclick="cancelOrder('${orderTask.orderDto.id}','${orderTask.id}',true)"> | 拒单退款</a>
								</c:if>
								<!-- 待退款 -->
								<c:if test="${orderTask.taskType eq 'WAIT_REFUND'}">
									<a onclick="reviewRefund('${orderTask.orderDto.id}','${orderTask.id}')"> | 审核</a>
								</c:if>
							 </span>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' and not empty orderTask.result}">
							<span>处理结果：${orderTask.result}</span>
						</c:if>
					 </td>
					<td>
						<span>
							<c:if test="${empty orderTask.orderDto.externalOrderId and orderTask.orderDto.mark ne 'DIRECT'}">
								<font color="red">供应商下单失败</font>
							</c:if>
							<c:if test="${orderTask.taskType eq 'WAIT_URGE'}">
								<fmt:formatDate value="${orderTask.orderDto.createDate}" pattern="yyyy-MM-dd" var="createDate"/>
								<fmt:formatDate value="${orderTask.orderDto.arrivalDate}" pattern="yyyy-MM-dd" var="arrivalDate"/>
								<c:choose>
									<c:when test="${createDate == arrivalDate}">
										<font color="red">当日订单,已超过45分钟未被确认房间</font>
									</c:when>
									<c:otherwise>
										<font color="red">非当日订单,已超过60分钟未被确认房间</font>
									</c:otherwise>
								</c:choose>
							</c:if>
						</span>
						<br>
							酒店电话: ${orderTask.orderDto.phone}
						<br>
						<span>
							${orderTask.orderDto.issueChannelDto.name}
							<c:if test="${ orderTask.orderDto.mark ne 'DIRECT'}">
							&nbsp;&nbsp;
							<a data-trigger="tooltip" data-content="${orderTask.orderDto.externalOrderId }">订单号</a>
							</c:if>
						</span>
						<br>
						<span id="status_${orderTask.orderDto.id }">
							状态：${orderTask.orderDto.externalOrderShowStatus.message } - ${orderTask.orderDto.externalOrderStatus.status }
						</span><br>
						<c:if test="${ not empty orderTask.tipsContent and orderTask.orderDto.mark ne 'DIRECT'}">
							<span> <a data-trigger="tooltip" data-content="${orderTask.tipsContent}" style="color:red">客服提示</a></span><br/>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'PROCESSING' }">
							<span>
								<a onclick="refreash('${orderTask.orderDto.id}','${orderTask.orderDto.externalOrderId }','${orderTask.id }')">刷新</a> 
								<c:if test="${orderTask.orderDto.orderShowStatus eq 'CONFIRMING' or orderTask.orderDto.orderShowStatus eq 'CONFIRMED'}">
									<a onclick="cancelOrder('${orderTask.orderDto.id}','${orderTask.id }')"> | 取消</a> 
								</c:if>
								<c:if test="${orderTask.orderDto.orderShowStatus eq 'CONFIRMING' and orderTask.taskType eq 'WAIT_TICKET'}">
									<a onclick="supPay('${orderTask.orderDto.id}','${orderTask.id }')"> | 转系统出票</a>
								</c:if>
							</span>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
		
	</table>
</div>

<!-- 模态框 -->
<div id="model1"></div>

<!-- <div class="modal fade" id="rejectOrder_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            		<h4 class="modal-title">拒单退款</h4>
         </div>
         <div class="modal-body">
	         <form  class="form-horizontal" id="rejectOrderForm" role="form">
	         	<h4>请确认未支付给供应商，或支付后供应商同意退款</h4>
	         	<h4>请输入拒单退款原因:(企业客人可见)</h4>
	         	<input type="text" class="form-control" id="remark"  placeholder="请输入拒单退款原因(至少4个字)"/>
	         </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="submitRejectOrderForm()">拒单退款</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">放弃</button>
         </div>
      </div>
</div>
</div> -->


<div class="pagin" style="margin-bottom: 22px">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="webpage/hotel/tmc/js/myTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
    $(function(){
        var status = "${taskStatus}";
        $("#myTaskStatusTab li").each(function(){
            if($(this).attr("data-value")==status){

                $(this).addClass("active");
                $(this).siblings().removeClass("active");
                if(status=='PROCESSING'){
                    $('#h_my_content .content1').show();
                    $('#h_my_content .content2').hide();
                }else if(status=='ALREADY_PROCESSED'){
                    $('#h_my_content .content1').hide();
                    $('#h_my_content .content2').show();
                }
                return false;
            }
        });

    });
</script>