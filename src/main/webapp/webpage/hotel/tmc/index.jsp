<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.tem.platform.security.authorize.PermissionUtil"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<link rel="stylesheet" href="${baseRes }/plugins/sco/css/scojs.css?version=${globalVersion}">
<link rel="stylesheet" href="webpage/flight/tmc/css/tmc.css?version=${globalVersion}">
<%-- 只有客服经理能看到全部任务列表 --%>
<% session.setAttribute("jurisdiction", PermissionUtil.checkTrans("COMMON_TMC_SERVICE", "HOTEL_M"));   %>

<ul id="myTab" class="nav nav-tabs" data-trigger="tab">
	<li><a onclick="getMyTask()">我的任务</a></li>
	<c:if test="${sessionScope.jurisdiction ==0}">
		<li><a onclick="getAllTask()">全部任务</a></li>
	</c:if>

	<li><a onclick="getOrderList()">订单查询</a></li>
</ul>
<div class="pane-wrapper">
	<!-- 我的任务 -->
	<div id="myTask">
		<jsp:include page="myTask.jsp"></jsp:include>
	</div>
	<!-- 全部任务 -->
	<c:if test="${sessionScope.jurisdiction ==0}">
		<div id="allTask">
		</div>
	</c:if>

	<!-- 订单查询 -->
	<div id="orderList">
	</div>

	<%--用来判断上次刷新前是选择的那个tab页，很繁琐，无奈之举 --%>
	<input type="hidden" id="myTask_pageIndex"/>
	<input type="hidden" id="myTask_task_status"/>
	<input type="hidden" id="myTask_task_type"/>
	<input type="hidden" id="allTask_pageIndex"/>
	<input type="hidden" id="allTask_task_status"/>
	<input type="hidden" id="allTask_task_type"/>
	<input type="hidden" id="is_flush" value="true"/>
</div>
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 
<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tab.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tooltip.js?version=${globalVersion}"></script>
<script src="webpage/hotel/tmc/js/index.js?version=${globalVersion}"></script>

<%--酒店催单model层 --%>
<div class="modal fade" id="urgeDone_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            		<h4 class="modal-title">客服催单</h4>
         </div>
         <div class="modal-body">
	         <form  class="form-horizontal" id="urgeDoneOrderForm" role="form">
				 <input type="hidden" name="orderId"/>
				 <input type="hidden" name="taskId"/>
	         	<!-- <h4>请确认未支付给供应商，或支付后供应商同意退款</h4> -->
	         	<h4>请输入此次催单结果：</h4>
	         	<input type="text" class="form-control"  name="remark" placeholder="请输入酒店催单结果"/>
	         </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="submitUrgeDoneOrderForm()">已催单</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">放弃</button>
         </div>
      </div>
	</div>
</div>

<%--酒店催单处理完毕model层 --%>
<div class="modal fade" id="handled_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            		<h4 class="modal-title">催单处理完成</h4>
         </div>
         <div class="modal-body">
	         <form  class="form-horizontal" id="handledOrderForm" role="form">
				 <input type="hidden" name="orderId"/>
				 <input type="hidden" name="taskId"/>
	         	<!-- <h4>请确认未支付给供应商，或支付后供应商同意退款</h4> -->
	         	<h4>请输入催单完成结果：</h4>
	         	<input type="text" class="form-control"  name="remark" placeholder="请输入酒店催单结果"/>
	         </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="submitHandledOrderForm()">催单完毕</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">放弃</button>
         </div>
      </div>
	</div>
</div>

<div id="noteToHotel" hidden>
	<jsp:include page="noteHotel.jsp"></jsp:include>
</div>

<script type="text/javascript">
	getMyTask();
</script>