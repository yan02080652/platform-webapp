<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div>
	<input type="hidden" id="currentTaskType" />
	<!-- 状态栏 -->
	<div class="tab pull-left">
		<ul class="fix" id="allTaskStatusTab">
			<li class="tab1 active" data-value="WAIT_PROCESS" onclick="reloadAllTask(null ,'WAIT_PROCESS')">待处理(${waitTaskTypeMap.WAIT_PROCESS==null?0:waitTaskTypeMap.WAIT_PROCESS})</li>
			<li class="tab2" data-value="PROCESSING" onclick="reloadAllTask(null,'PROCESSING')">处理中(${typeMap.PROCESSING==null?0:typeMap.PROCESSING})</li>
			<li class="tab3" data-value="ALREADY_PROCESSED" onclick="reloadAllTask(null,'ALREADY_PROCESSED')">近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})</li>
		</ul>
		<div id="h_all_content">
		<div class="content1" >
			<p>
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_URGE')">待催单(${waitTaskTypeMap.WAIT_URGE==null?0:waitTaskTypeMap.WAIT_URGE })</a>
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_CONFIRM')">待确认(${waitTaskTypeMap.WAIT_CONFIRM==null?0:waitTaskTypeMap.WAIT_CONFIRM })</a>
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','CONFIRM_AGAIN')">酒店确认(${waitTaskTypeMap.CONFIRM_AGAIN==null?0:waitTaskTypeMap.CONFIRM_AGAIN })</a>
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_TICKET')">待出票(${waitTaskTypeMap.WAIT_TICKET==null?0:waitTaskTypeMap.WAIT_TICKET })</a>
				<a onclick="reloadAllTask(null,'WAIT_PROCESS','WAIT_REFUND')">待退款(${waitTaskTypeMap.WAIT_REFUND==null?0:waitTaskTypeMap.WAIT_REFUND })</a>
			</p>
		</div>
		<div class="content2" >
			<p>
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_URGE')">待催单(${typeMap.WAIT_URGE==null?0:typeMap.WAIT_URGE })</a>
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_CONFIRM')">待确认(${typeMap.WAIT_CONFIRM==null?0:typeMap.WAIT_CONFIRM })</a>
				<a onclick="reloadAllTask(null,'PROCESSING','CONFIRM_AGAIN')">酒店确认(${typeMap.CONFIRM_AGAIN==null?0:typeMap.CONFIRM_AGAIN })</a>
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_TICKET')">待出票(${typeMap.WAIT_TICKET==null?0:typeMap.WAIT_TICKET })</a>
				<a onclick="reloadAllTask(null,'PROCESSING','WAIT_REFUND')">待退款(${typeMap.WAIT_REFUND==null?0:typeMap.WAIT_REFUND })</a>
			</p>
		</div>
		<div class="content3" >
			<p>&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_URGE')">催单(${alreadyMap.WAIT_URGE==null?0:alreadyMap.WAIT_URGE })</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_CONFIRM')">确认(${alreadyMap.WAIT_CONFIRM==null?0:alreadyMap.WAIT_CONFIRM })</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','CONFIRM_AGAIN')">酒店确认(${alreadyMap.CONFIRM_AGAIN==null?0:alreadyMap.CONFIRM_AGAIN })</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_TICKET')">出票(${alreadyMap.WAIT_TICKET==null?0:alreadyMap.WAIT_TICKET })</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a onclick="reloadAllTask(null,'ALREADY_PROCESSED','WAIT_REFUND')">退款(${alreadyMap.WAIT_REFUND==null?0:alreadyMap.WAIT_REFUND })</a>
			</p>
		</div>
		</div>
	</div>
	<!-- 任务类型 -->
	<input type="hidden" id="task_type" value="WAIT_PROCESS"/>
	<input type="hidden" id="task_status" />
	
	<!-- 搜索条件 -->
	<div>
		<form class="form-inline" method="post" id="orderSearchForm">
			<nav>
				<div class=" form-group">
					<input  id="search_Field2" type="text" class="form-control" placeholder="订单号/处理人" style="width: 300px"/>				
					<button type="button" class="btn btn-default" onclick="submitAjax_all()">搜索</button>
					<button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
				</div>
			</nav>
		</form>
	</div>
	<div>
		<label><font color="red" size="4">刷新倒计时:<span id="countDown">9</span>秒</font></label>
		<button id="isCloseRefresh" value="false"><span>关闭</span>刷新</button>
	</div>
	
</div>


<!-- 任务列表 -->
<div style="margin-top: 10px">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th style="width: 20%">酒店信息</th>
				<th style="width: 20%">入住人信息</th>
				<th style="width: 20%">收支信息</th>
				<th style="width: 20%">处理信息</th>
				<th style="width: 20%">供应信息</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="orderTask">
				<tr>
					<td colspan="5">
						<span>企业：${orderTask.orderDto.cName } </span>
						<span>预订人：${orderTask.orderDto.userName }</span>
						<span>订单号：<font color="blue"><a href="/order/hotelDetail/${orderTask.orderDto.id }" target="_Blank">${orderTask.orderDto.id }</a></font></span>
						<span>来源:<font color="blue">${orderTask.orderDto.orderOriginType.message}</font></span>
						<span>标签：${orderTask.orderDto.orderShowStatus.status }</span>
						<span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${orderTask.orderDto.createDate }" /></span>
					    <span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span>
				    </td>
				</tr>
				<tr>
					<td>
						<span>
							<fmt:formatDate value="${orderTask.orderDto.arrivalDate }" pattern="yyyy-MM-dd"/> - 
							<fmt:formatDate value="${orderTask.orderDto.departureDate }" pattern="yyyy-MM-dd"/>
							<span> ${orderTask.orderDto.stayDay} 晚</span>
						</span>
						<br/>
						<span>${orderTask.orderDto.hotelName }</span>
						<br/>
						<span>${orderTask.orderDto.roomName }</span>&nbsp;&nbsp;<span>${orderTask.orderDto.roomNum }间</span>
					<%-- 	<span>${orderTask.orderDto.orderFlights[0].fromAirportName }${orderTask.orderDto.orderFlights[0].fromTerminal } - 
								${orderTask.orderDto.orderFlights[0].toAirportName }${orderTask.orderDto.orderFlights[0].toTerminal }</span><br/>
						<span>${orderTask.orderDto.orderFlights[0].flightNo } ${cabinClassMap[orderTask.orderDto.orderFlights[0].cabinClass ]} &nbsp;${orderTask.orderDto.orderFlights[0].cabinClass }</span>
						<span><a>SS</a></span> --%>
						<br/>
						<c:if test="${not empty orderTask.orderDto.noteToHotel}">
							<a onclick="showNoteToHotel('${orderTask.orderDto.noteToHotel}')">客户备注</a>
						</c:if>
					</td>
					<td>
						<span>
							<c:forEach items="${orderTask.orderDto.customers }" var="customer" varStatus="status">
									<c:if test="${!status.last }">${customer.name }/</c:if>
									<c:if test="${status.last }">${customer.name }</c:if>
							</c:forEach>
						</span>
					</td>
					<td>
						<span>应收￥${orderTask.orderDto.finalTotalPrice }</span>
						<span>
							<c:choose>
								<c:when test="${orderTask.orderDto.paymentStatus eq 'PAYMENT_SUCCESS'}">已收</c:when>
								<c:otherwise>未收</c:otherwise>
							</c:choose>
						</span><br/>
						<span>应付 ${orderTask.orderDto.currencyCode} ${orderTask.orderDto.totalCost }</span>
						<span>
							<c:choose>
								<c:when test="${orderTask.orderDto.supPay}">已付</c:when>
								<c:otherwise>未付</c:otherwise>
							</c:choose>
						</span><br/>
						<c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
								<span><a onclick="loadIncomeDetail('${orderTask.orderDto.id}')">明细</a></span>
						</c:if>
					</td>
					<td>
						<span>任务：${orderTask.taskType.message} ${orderTask.taskStatus.message }</span><br/>
						<span>${orderTask.operatorName }</span><br/>
						<c:if test="${orderTask.orderDto.protocol}">
							<span>协议代码: ${orderTask.orderDto.noteToGuest}</span><br/>
						</c:if>
						<c:if test="${orderTask.orderDto.mark!=null and orderTask.orderDto.mark eq 'DIRECT' }">
							<span style="color:red">在途协议酒店</span><br/>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'WAIT_PROCESS' }">
							<span><a onclick="transfer('${orderTask.id}','${orderTask.operatorId }',false)">分配</a></span>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' and not empty orderTask.result}">
							<span>处理结果：${orderTask.result}</span>
						</c:if>
					 </td>
					<td>
						<span>
							<c:if test="${orderTask.taskType eq 'WAIT_URGE'}">
								<fmt:formatDate value="${orderTask.orderDto.createDate}" pattern="yyyy-MM-dd" var="createDate"/>
								<fmt:formatDate value="${orderTask.orderDto.arrivalDate}" pattern="yyyy-MM-dd" var="arrivalDate"/>
								<c:choose>
									<c:when test="${createDate == arrivalDate}">
										<font color="red">当日订单,已超过45分钟未被确认房间</font>
									</c:when>
									<c:otherwise>
										<font color="red">非当日订单,已超过60分钟未被确认房间</font>
									</c:otherwise>
								</c:choose>
							</c:if>
						</span>
						<br>
						<span>
							${orderTask.orderDto.issueChannelDto.name}
							<c:if test="${ orderTask.orderDto.mark ne 'DIRECT'}">
							&nbsp;&nbsp;
							<a data-trigger="tooltip" data-content="${orderTask.orderDto.externalOrderId }">订单号</a>
							</c:if>
						</span>
						<br>
						<span class="status_${orderTask.orderDto.id }">
							状态：${orderTask.orderDto.externalOrderShowStatus.message } - ${orderTask.orderDto.externalOrderStatus.status }
						</span><br>
						<c:if test="${ not empty orderTask.tipsContent and orderTask.orderDto.mark ne 'DIRECT'}">
							<span> <a data-trigger="tooltip" data-content="${orderTask.tipsContent}" style="color:red">客服提示</a></span><br/>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'PROCESSING' }">
							<span>
								<a onclick="refreash('${orderTask.orderDto.id}','${orderTask.orderDto.externalOrderId }','${orderTask.id }')">刷新</a>  
								<c:if test="${orderTask.orderDto.orderShowStatus eq 'CONFIRMED'}">
									<a onclick="cancelOrder('${orderTask.orderDto.id}','${orderTask.id }')"> | 取消</a> 
								</c:if>
							</span>
						</c:if>
						<!--已处理 未结算 线下处理过的确认订单 可以进行订单修改操作-->
						<c:if test="${orderTask.ticketType eq 'OFFLINE' and orderTask.taskStatus eq 'ALREADY_PROCESSED' and orderTask.orderDto.supplierLiqStatus ne 'CLOSE' and orderTask.orderDto.orderStatus eq 'CONFIRMED'}">
							<span>
								<a onclick="showLineData('${orderTask.orderDto.id}','${orderTask.id}')">线下订单修改</a>
							</span>
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
</div>
<input type="hidden" id="is_flush" value="true"/>
<!-- 模态框 -->
<div id="model2"></div>
<div id="model3"></div>
<div class="pagin" style="margin-bottom: 22px">
	<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="webpage/hotel/tmc/js/allTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(function(){
	var status = "${taskStatus}";
	console.log(status)
	$("#allTaskStatusTab li").each(function(){
		if($(this).attr("data-value")==status){
		
			$(this).addClass("active");
			$(this).siblings().removeClass("active");
			if(status == 'WAIT_PROCESS'){
				 $('#h_all_content .content1').show();
			     $('#h_all_content .content2').hide();
			     $('#h_all_content .content3').hide();
			}else if(status=='PROCESSING'){
				 $('#h_all_content .content1').hide();
				 $('#h_all_content .content2').show();
			     $('#h_all_content .content3').hide();
			}else if(status=='ALREADY_PROCESSED'){
				 $('#h_all_content .content1').hide();
			     $('#h_all_content .content2').hide();
			     $('#h_all_content .content3').show();
			}
			return false;
		}
	});
});
</script>