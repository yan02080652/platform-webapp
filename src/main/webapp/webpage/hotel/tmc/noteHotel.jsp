<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" href="resource/plugins/layer/skin/layui.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/bootstrap/fileinput.min.css?version=${globalVersion}">
<style>
 .remark-container table{
     border: 1px #ddd solid;
     width: 100%;
 }
 .remark-container table tr th{
     white-space: nowrap;
     border-right: 1px #ddd solid;
     padding: 15px 20px;
     text-align: center;
     vertical-align: middle;
 }
 .remark-container table thead{
     border-bottom: 1px #ddd solid;
     background: #ddd;
 }
 .remark-container table tr td{
     border-bottom: 1px #ddd solid;
     border-right: 1px #ddd solid;
     text-align: center;
     padding: 15px 20px;
     vertical-align: middle;
 }
 .remark-container table tr:nth-child(2) td{
     padding: 40px 20px;
 }
 .remark-container table tr td:last-child, .remark-container table tr th:last-child{
     border-right: none;
 }
</style>

    <div class="modal-body remark-container">
        <table>
            <colgroup>
                <col width="100">
            </colgroup>
            <thead>
            <tr>
                <th>录入项</th>
                <th>内容</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>勾选项</td>
                <td id="noteToHotelMsg1"></td>
            </tr>
            <tr>
                <td>填写项</td>
                <td id="noteToHotelMsg2"></td>
            </tr>
            </tbody>
        </table>
    </div>


    <script src="resource/plugins/layer/layui.js?version=${globalVersion}"></script>
    <script src="resource/plugins/layer/tree.js?version=${globalVersion}"></script>
