<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<style>
    .itemPrice{
        color: red;
    }
    #refund_model form .row.row-bar{
        padding: 0 15px;
    }
    #refund_model form .row.row-bar .label-info, #refund_model form .row.row-bar .label-success{
        padding: 5px;
        text-align: left;
        border-radius: 6px;
    }
    #refund_model .priceDetail .price_d_list li{
        display: inline-block;
        margin-right: 10px;
        border: 1px #ddd solid;
        border-radius: 4px;
        text-align: center;
    }
    #refund_model .priceDetail .price_d_list li h5{
        margin: 0;
        background: #ddd;
        padding: 5px 10px;
        font-size: 12px;
        color: #666;
        font-weight: 500;
    }
    #refund_model .priceDetail .price_d_list li .d_body {
        padding: 8px 0;
    }
    #refund_model .priceDetail .price_d_list li p {
        margin: 0;
    }
    #refund_model .priceDetail .price_d_list li p span{
        color: #f0945e;
    }
</style>

<input type="hidden" id="orderId" value="${order.id}">
<input type="hidden" id="taskId" value="${taskId}">

<div class="modal fade" id="refund_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:70%;font-size: 11px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">酒店线下取消任务</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row form-group row-bar">
                        <span class="col-md-12 label label-info">原始订单: ${order.id}</span>
                    </div>
                    <div class="row form-group">
                        <b class="col-md-2 control-label">预订人:</b>
                        <p class="col-md-4 control-label">${order.cName} ${order.userName}</p>
                        <b class="col-md-2 control-label">酒店名称: </b>
                        <p class="col-md-4 control-label">${order.hotelName}</p>
                    </div>
                    <div class="row form-group">
                        <b class="col-md-2 control-label">入离日期: </b>
                        <p class="col-md-4 control-label"><fmt:formatDate value="${order.arrivalDate}" pattern="yyyy-MM-dd"/> - <fmt:formatDate value="${order.departureDate}" pattern="yyyy-MM-dd"/> 共${order.stayDay}晚</p>
                        <b class="col-md-2 control-label">房型名称:</b>
                        <p class="col-md-4 control-label">${order.roomName}</p>
                    </div>
                    <div class="row form-group">
                        <b class="col-md-2 control-label">价格计划: </b>
                        <p class="col-md-4 control-label">${order.ratePlanName}</p>
                        <b class="col-md-2 control-label">支付方式: </b>
                        <p class="col-md-4 control-label">${order.paymentType.message}</p>
                    </div>
                    <div class="row form-group">
                        <b class="col-md-2 control-label">取消规则: </b>
                        <p class="col-md-4 control-label">${order.orderPrepayRule.description}</p>
                        <b class="col-md-2 control-label">订单总额: </b>
                        <p class="col-md-4 control-label" onclick="priceDetail()">
                            ¥<span class="itemPrice totalPrice">${order.finalTotalPrice}</span>&nbsp;&nbsp;&nbsp;
                            <a >查看明细
                                <span class="glyphicon glyphicon glyphicon-menu-down"></span>
                            </a>
                        </p>
                    </div>
                    <div class="priceDetail" style="display:none;">
                        <hr/>
                        <div class="row form-group">
                            <b class="col-md-2 control-label">每日价格: </b>
                            <ul class="col-md-10 control-label price_d_list">
                                <c:forEach items="${order.nightlyRates}" var="bean" varStatus="status">
                                    <li>
                                        <h5><fmt:formatDate value="${bean.checkInDate}" pattern="MM-dd(EE)"/></h5>
                                        <div class="d_body">
                                            <p>${bean.breakFastDes}</p>
                                            <p>￥<span>${bean.dayRate}</span></p>
                                        </div>
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                        <div class="row form-group" style="display: none;">
                            <b class="col-md-2 control-label">服务费: </b>
                            <p class="col-md-10 control-label extraFee">${order.extraFee}</p>
                        </div>
                        <hr/>
                    </div>

                    <div class="row form-group">
                        <b class="col-md-2 control-label">供应商结算: </b>
                        <div class="table-responsive col-md-9">
                            <table class="table table-bordered">
                                <tr>
                                    <td>供应商</td>
                                    <td>结算单号</td>
                                    <td>结算金额</td>
                                    <td>备注</td>
                                </tr>
                                <tr>
                                    <td>${order.issueChannelDto.name}</td>
                                    <td>${order.externalOrderId}</td>
                                    <td id="totalCost">${order.totalCost}</td>
                                    <td>${order.orderNote}</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row form-group row-bar">
                        <span class="col-md-12 label label-success ">退款申请处理</span>
                    </div>

                    <div class="row form-group">
                        <b class="col-md-2 control-label">处理结果: </b>
                        <p class="col-md-10 control-label">
                            <input type="radio" name="cancelBtn" value="0"> 不可取消
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="cancelBtn" value="1"> 允许取消
                        </p>
                    </div>

                    <div class="allowCancel" style="display:none;">
                        <hr/>
                        <div class="row form-group">
                            <b class="col-md-2 control-label">供应商结算: </b>
                            <div class="table-responsive col-md-9">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>结算单号</td>
                                        <td>退款金额</td>
                                        <td>收取罚金</td>
                                        <td>备注</td>
                                    </tr>
                                    <tr>
                                        <td>${order.externalOrderId}</td>
                                        <td><input id="refundAmount" class="form-control" onkeyup="clearNoNum(this,false);" placeholder="退款金额不可大于${order.totalCost}"></td>
                                        <td id="fine"></td>
                                        <td><input id="orderNote" class="form-control"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row form-group">
                            <b class="col-md-2 control-label">客户结算:</b>
                            <p class="col-md-5" style="display: flex;justify-content: space-around;align-items: center"><span style="width: 45%">退款调整:</span> <input id="refundFee" class="form-control" value="0" onkeyup="clearNoNum(this,true);" placeholder="应退金额不可大于订单金额"></p>
                            <p class="col-md-2 control-label" style="display: flex;justify-content: space-around;align-items: center"><span style="width: 45%;line-height: 34px">应退金额:</span> ¥<span class="itemPrice dealAmount"></span></p>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer ">
                <btn class="btn btn-default"  onclick="delay()">稍后处理</btn>
                <btn class="btn btn-info" onclick="doSubmit()">完成审核</btn>
            </div>
        </div>
    </div>
</div>

<script>
    function priceDetail(){
        if($('.priceDetail').is(":hidden")){
            $('.priceDetail').show();
        }else{
            $('.priceDetail').hide();
        }
    }

    $('input[name=cancelBtn]').click(function(){
        if(this.value == 0){
            $('.allowCancel').hide();
        }else{
            $('.allowCancel').show();
        }
    })

    $('#refundAmount').blur(function(){
        var cost = $('#totalCost').text();
        var refund = $(this).val();
        if(refund == ''){
            refund = 0;
        }
        var fine = parseFloat(cost) - parseFloat(refund)
        if(fine < 0){
            $(this).val("")
            return;
        }
        $('#fine').text(fine);
        cacularRefundAmount();
    })

    $('#refundFee').blur(function(){
        cacularRefundAmount();
    })

    function cacularRefundAmount(){
        var totalPrice = $('.totalPrice').text();
        var fine = $('#fine').text();
        var refund = $('#refundFee').val();
        var extraFee = $('.extraFee').text();
        if(refund == ''){
            refund = 0;
        }
        if(fine == ''){
            fine = 0;
        }
        var amount = parseFloat(totalPrice) - parseFloat(refund) - parseFloat(fine) - parseFloat(extraFee);
        if(amount < 0 || amount > totalPrice){
            $('#refundFee').val("0")
            $('.dealAmount').text("");
            return;
        }
        $('.dealAmount').text(amount);
    }

    //稍后处理
    function delay(){
        $('#refund_model').modal('hide');
        $('#is_flush').val("true");
        setTimeout(function(){
            submitAjax(null,'PROCESSING',null);
        },200);
    }

    /**
     * 完成审核
     */
    var flag = 0;
    function doSubmit(){
        var allowCancel = $('input[name=cancelBtn]:checked').val();
        if(!allowCancel){
            layer.msg("请完善相关信息",{icon:5});
            return;
        }

        if(allowCancel == 1&& $('.dealAmount').text() == ""){
            layer.msg("请完善相关信息",{icon:5});
            return;
        }

        if(flag == 0){
            flag = 1;
            $.ajax({
                url: '/hotel/tmc/reviewPass.json',
                type: 'post',
                data: {
                    orderId: $('#orderId').val(),
                    refundAmount: $('.dealAmount').text(),
                    orderNote: $('#orderNote').text(),
                    taskId: $('#taskId').val(),
                    allowCancel: allowCancel
                },success: function(data){
                    if(data.code == 0){
                        $('#refund_model').modal('hide');
                        $('#is_flush').val("true");
                        layer.msg("恭喜你,任务处理成功啦！",{icon:1})
                        setTimeout(function(){
                            submitAjax(null,'PROCESSING',null);
                        },200);
                    }else{
                        layer.msg(data.msg,{icon:2})
                    }
                    flag = 0;
                },error: function(e){
                    layer.msg("系统繁忙，请稍后刷新重试！",{icon:2});
                    flag = 0;
                }
            })
        }
    }

    function clearNoNum(obj,negative) {
        var negativeFlag = false;
        if (negative && obj.value != '' && obj.value.substr(0, 1) == '-') {
            obj.value.replace("-","");
            negativeFlag = true;
        }

        //修复第一个字符是小数点 的情况.
        if (obj.value != '' && obj.value.substr(0, 1) == '.') {
            obj.value = "";
        }
        obj.value = obj.value.replace(/^0*(0\.|[1-9])/, '$1');//解决 粘贴不生效
        obj.value = obj.value.replace(/[^\d.]/g, "");  //清除“数字”和“.”以外的字符
        obj.value = obj.value.replace(/\.{2,}/g, "."); //只保留第一个. 清除多余的
        obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3');//只能输入两个小数
        if (obj.value.indexOf(".") < 0 && obj.value != "") {//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
            if (obj.value.substr(0, 1) == '0' && obj.value.length == 2) {
                obj.value = obj.value.substr(1, obj.value.length);
            }
        }

        if(negativeFlag){
            obj.value = "-" + obj.value;
        }

    }


</script>