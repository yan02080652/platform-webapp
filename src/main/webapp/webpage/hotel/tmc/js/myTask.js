function reloadMyTaskTable(pageIndex){
	var taskStatus = $("#myTaskStatusTab .active").attr("data-value");
    var taskType = $("#currentTaskType").val();
	submitAjax(pageIndex,taskStatus,taskType);
}

//刷新
function refreash(orderId,thirdId,taskId){
	if(thirdId==''||thirdId==null){
		return;
	}
	$.ajax({
		url: 'hotel/tmc/refreashOrderDetail',
		type: 'post',
		async: false,
		data: {
			orderId: orderId,
			thirdId: thirdId
		},
		success: function(data){
			if(data.result){
				$('#status_' + orderId).text('状态: ' + data.showStatus + '（' + data.orderStatus + '）');
				if(data.isShow){
					$('#confirm_' + orderId).html('<a onclick=confirmDone('+orderId+','+taskId+') style=color:#87af1c;> | 处理完毕</a> ');
				}
			}else{
				layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
		}
	})
}

function showNoteToHotel(msg) {
	console.log(msg);
	$("#noteToHotelMsg1").text(msg.split("-")[0]);
	$("#noteToHotelMsg2").text(msg.split("-")[1]);
    layer.open({
        type: 1,
        area: ['600px', '400px'],
        title: "客户备注",
        closeBtn: 1,
        shadeClose: true,
        // shade :0,
        content: $("#noteToHotel"),
    });
}

//确认
function confirm(orderId,taskId){
	showConfirm("已与酒店确认留房?",function(){
		var loadIndex = layer.load();
		$.ajax({
			url: 'hotel/tmc/confirm',
			type: 'post',
			async: false,
			data: {
				orderId: orderId,
				taskId: taskId
			},
			success: function(data){
				layer.close(loadIndex)
				if(data.success){
					layer.msg("确认成功",{icon:1})
					setTimeout(function(){
						submitAjax(null,'PROCESSING',null);
					},200);
				}else{
					layer.msg("确认失败:" + data.msg,{icon:2})
				}
			},
			error: function(e){
				layer.close(loadIndex);
				layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
			}
		})
	});
}

//取消订单
function cancelOrder(orderId,taskId,refund){
	showConfirm("确认取消?",function(){
		var loadIndex = layer.load();
		$.ajax({
			url: 'hotel/tmc/cancelOrder',
			type: 'post',
			async: false,
			data: {
				orderId: orderId,
				taskId: taskId
			},
			success: function(data){
				layer.close(loadIndex)
				layer.msg(data.msg,{icon:1})
				if(data.result){
					setTimeout(function(){
						submitAjax(null,'PROCESSING',null);
					},200);
				}
			},
			error: function(e){
				layer.close(loadIndex)
				layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
			}
		})
	})
}

//重新支付
function supPay(orderId,taskId){
	layer.confirm('请确保账户余额充足,确定重新支付?', {title: "系统提示"}, function (index) {
		layer.close(index);
		var loadIndex = layer.load();
		$.ajax({
			url: 'hotel/tmc/supPay',
			type: 'post',
			async: false,
			data: {
				orderId: orderId,
				taskId: taskId
			},
			success: function(data){
				layer.close(loadIndex);
				if(data.success){
					layer.msg("恭喜你,任务处理成功啦！",{icon:1})
					setTimeout(function(){
						submitAjax(null,'PROCESSING',null);
					},200);
				}else{
					layer.msg(data.msg,{icon:2})
				}
			},
			error: function(e){
				layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
				layer.close(loadIndex);
			}
		})
	})
}

//催单
function urgeDone(orderId,taskId){
	
	$("#urgeDone_model").modal({
        backdrop: 'static'
    });
    $('#is_flush').val("false");
	$("#urgeDoneOrderForm input[name='orderId']").val(orderId);
	$("#urgeDoneOrderForm input[name='taskId']").val(taskId);
	$("#urgeDoneOrderForm").validate(
        {
            rules: {
                remark: {
                    required: true,
                    minlength: 4,
                    maxlength:30,
                },
            },
            submitHandler: function (form) {
                var orderId = $("#urgeDoneOrderForm input[name='orderId']").val();
                var taskId = $("#urgeDoneOrderForm input[name='taskId']").val();
                var remark = $("#urgeDoneOrderForm input[name='remark']").val();
                var layer_index = layer.load(0);
                $.ajax({
                    type: "POST",
                    url: "hotel/tmc/urgeDone",
                    data: {
                        orderId: orderId,
                        taskId: taskId,
                        remark: remark,
                    },
                    async: false,
                    error: function (request) {
                        layer.close(layer_index);
                        layer.msg("请求失败，请刷新重试", {icon: 2})
                        $('#urgeDone_model').modal('hide');
                        $('#is_flush').val("true");
                        setTimeout(function () {
                        	submitAjax(null,'PROCESSING',null);
                        }, 200);
                    },
                    success: function (data) {
                        layer.close(layer_index);
                        $('#urgeDone_model').modal('hide');
                        $('#is_flush').val("true");
                        if (data) {
                            layer.msg("完成一次催单！", {icon: 1, offset: 't', time: 1000})
                            setTimeout(function () {
                            	submitAjax(null,'PROCESSING',null);
                            }, 200);
                        } else {
                            layer.msg("系统错误，请刷新重试！", {icon: 2, offset: 't', time: 1000})
                            setTimeout(function () {
                            	submitAjax(null,'PROCESSING',null);
                            }, 200);
                        }
                    }
                });
            },
            errorPlacement: function (error, element) {
                layer.tips($(error).text(), $(element), {
                    tips: [1, '#bb0b07'],
                    tipsMore: true,
                });
            },
        });
}

function submitUrgeDoneOrderForm(){
	 $("#urgeDoneOrderForm").submit();
}

//催单完成
function handled(orderId,taskId){
	$("#handled_model").modal({
        backdrop: 'static'
    });
    $('#is_flush').val("false");
    $("#handledOrderForm input[name='orderId']").val(orderId);
    $("#handledOrderForm input[name='taskId']").val(taskId);
    $("#handledOrderForm").validate(
            {
                rules: {
                    remark: {
                        required: true,
                        minlength: 4,
                        maxlength:30,
                    },
                },
                submitHandler: function (form) {
                    var orderId = $("#handledOrderForm input[name='orderId']").val();
                    var taskId = $("#handledOrderForm input[name='taskId']").val();
                    var remark = $("#handledOrderForm input[name='remark']").val();
                    var layer_index = layer.load(0);
                    $.ajax({
                        type: "POST",
                        url: "hotel/tmc/handled",
                        data: {
                            orderId: orderId,
                            taskId: taskId,
                            remark: remark,
                        },
                        async: false,
                        error: function (request) {
                            layer.close(layer_index);
                            layer.msg("请求失败，请刷新重试", {icon: 2})
                            $('#handled_model').modal('hide');
                            $('#is_flush').val("true");
                            setTimeout(function () {
                            	submitAjax(null,'PROCESSING',null);
                            }, 200);
                        },
                        success: function (data) {
                            layer.close(layer_index);
                            $('#handled_model').modal('hide');
                            $('#is_flush').val("true");
                            if (data) {
                                layer.msg("催单任务完成！", {icon: 1, offset: 't', time: 1000})
                                setTimeout(function () {
                                	submitAjax(null,'PROCESSING',null);
                                }, 200);
                            } else {
                                layer.msg("系统错误，请刷新重试！", {icon: 2, offset: 't', time: 1000})
                                setTimeout(function () {
                                	submitAjax(null,'PROCESSING',null);
                                }, 200);
                            }
                        }
                    });
                },
                errorPlacement: function (error, element) {
                    layer.tips($(error).text(), $(element), {
                        tips: [1, '#bb0b07'],
                        tipsMore: true,
                    });
                },
            });
    
}

function submitHandledOrderForm(){
	 $("#handledOrderForm").submit();
}

//确认完毕
function confirmDone(orderId,taskId){
	$.ajax({
		url: 'hotel/tmc/confirmDone',
		type: 'post',
		async: false,
		data:{
			'orderId': orderId,
			'taskId': taskId,
		},
		success: function(data){
			if(data){
				layer.msg("恭喜你,任务处理成功啦！",{icon:1})
				setTimeout(function(){
					submitAjax(null,'PROCESSING',null);
				},200);
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon:2})
		}
	})
}




//交接
function transfer(taskId,operatorId){
	TR.select('user', {type : 'me'}, function(data){

		$.ajax({
			type:"post",
			url:"hotel/tmc/transfer",
			data:{
				taskId:taskId,
				operatorId:operatorId,
				userId:data.id,
				userName:data.fullname
			},
			success:function(rs){
				if(rs){
					showPromptBox("任务已转交给"+data.fullname);
					setTimeout(function(){
						submitAjax(null,'PROCESSING',null);
					},200);
				}else{
					layer.msg("系统错误，请刷新重试！",{icon:2})
				}
			}
		});
	});
}

//显示Model
function showModal(url, data, callback) {
	$('#model1').load(url, data, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

//手动补单
function handRepair(orderId,taskId){
	showModal("hotel/tmc/handRepair",{orderId:orderId,taskId:taskId},function(data){
		$('#ticket_model').modal();
        $('#is_flush').val("false");
	});
}

//退款审核
function reviewRefund(orderId,taskId){
	showModal("hotel/tmc/reviewRefund",{orderId:orderId,taskId:taskId},function(data){
		$('#refund_model').modal();
		$('#is_flush').val("false");
	});
}

//二次确认
function confirmAgainDone(orderId,taskId){
    // 二次确认提示框
    showConfirm("请与酒店联系，确保客人可正常入住后再完成任务。",function(){
        $.ajax({
            url: 'hotel/tmc/confirmAgainDone',
            type: 'post',
            async: false,
            data:{
                'orderId': orderId,
                'taskId': taskId,
            },
            success: function(data){
                if(data){
                    layer.msg("恭喜你,任务处理成功啦！",{icon:1})
                    setTimeout(function(){
                        submitAjax(null,'PROCESSING',null);
                    },200);
                }
            },
            error: function(e){
                layer.msg("系统繁忙，请稍后刷新重试！",{icon:2})
            }
        });
    })
}

//提交补单
function submitTicketForm(){
	showConfirm("补单订单结算之后将不可修改,是否确认补单?",function(){
		if(!validate()){
			return;
		}
		$("#issueChannelMsg").val($("#hotelOriginType option:selected").text());
		var needAgain = $('input[name=needAgain]:checked').val();

		$('#needAgain').val(needAgain ? needAgain : false);
		$.ajax({
			type:"post",
			url:"hotel/tmc/repair",
			data:$('#ticketForm').serialize(),
			success:function(data){
				if(data){
					$('#ticket_model').modal('hide');
                    $('#is_flush').val("true");
					showPromptBox("补单任务完成!")
					setTimeout(function(){
						submitAjax(null,'PROCESSING',null);
					},200);
				}else{
					layer.msg("系统异常，保存失败!",{icon:2})
				}
			},
			error:function(){
				layer.msg("系统繁忙，请刷新重试",{icon:2})
			}
		});
	})
}

//取消提交补单
function cancelSubmit(orderId, taskId){
	//放过该订单,用户发送失败短信
	showConfirm("放弃之后此订单将取消并退款,确认放弃?",function(){
		$.ajax({
			type:"post",
			url:"hotel/tmc/cancelRepair",
			data:{orderId: orderId, taskId: taskId},
			success:function(data){
				if(data.result){
					$('#ticket_model').modal('hide');
                    $('#is_flush').val("true");
					showPromptBox("放弃完成! 用户退款中")
					setTimeout(function(){
						submitAjax(null,'PROCESSING',null);
					},200);
				}else{
					layer.msg(data.msg,{icon:2})
				}
			},
			error:function(){
				layer.msg("系统繁忙，请刷新重试",{icon:2})
			}
		});
	})
}

function validate(){
	var result = true;
	if($('#hotelOriginType').val() == ''){
		showError($('#hotelOriginType'),'请选择供应商');
		result = false;
	}
	if($('#externalOrderId').val().trim() == ''){
		showError($('#externalOrderId'),'请输入供应商订单号');
		result = false;
	}
	var $rates = $('.date_rate');
	for(var i = 0 ;i< $rates.length;i++){
		if($rates.eq(i).val().trim() == ''){
			showError($rates.eq(i),'请输入每日价格');
			result = false;
		}
	}
	
	return result;
}

//错误提示
function showError($dom,msg){
	layer.tips(msg, $dom, {
		  tips: [1, '#3595CC'],
		  time: 4000
		});
	$dom.focus();
}

//确认提示
function showConfirm(msg,_callback){
	var index = layer.alert(msg, {
		  btn: ['确定','取消']
		}, function(){
			layer.close(index);
			_callback();
		},function(){
			
		});
}

function submitAjax(pageIndex, taskStatus, taskType) {
	if ($('#is_flush').val() == "false") {
		return;
	}
    $("#currentTaskType").val(taskType);
    $("#myTask_pageIndex").val(pageIndex);
    $("#myTask_task_status").val(taskStatus);
    $("#myTask_task_type").val(taskType);
    // var layer_index = layer.load(0);
    $.ajax({
        type: "post",
        url: "hotel/tmc/getMyTaskTable",
        data: {
            taskStatus: taskStatus,
            pageIndex: pageIndex,
            taskType: taskType
        },
        success: function (data) {
            // layer.close(layer_index);
            $("#myTask").html(data);
        },
        error: function () {
            // layer.close(layer_index);
            layer.msg("系统繁忙，请刷新重试", {icon: 2, offset: 't', time: 1000})
        }
    });
}

//右下角提示
function showPromptBox(content){
	layer.open({
		  type: 0,//基本层类型
		  title: "系统提示",
		  closeBtn: 0,//不显示关闭x关闭
		  btn:[],//不显示按钮
		  shade: 0,
		  area: ['250px', '200px'],//大小
		  offset: 'rb', //右下角弹出
		  time: 3000, //3秒后自动关闭
		  shift: 2,//动画0-6
		  content: content, 
		});
}

// 定时任务
function taskTimer() {
    var pageIndex = $("#myTask_pageIndex").val();
    var taskStatus = $("#myTask_task_status").val();
    var taskType = $("#myTask_task_type").val();

    if (taskStatus == '') {
        taskStatus = 'PROCESSING';
    }
    submitAjax(pageIndex, taskStatus, taskType);


}


function delay(taskId,delay){
	if(delay != -1){
		$.ajax({
			url:"hotel/tmc/updateMaxProcessDate",
			post:"get",
			data:{taskId:taskId,delay:delay},
			success:function(){
				layer.msg("任务延时成功!",{icon:1,offset:'t',time:1000})
				submitAjax(null,'PROCESSING',null);
			}
		});
	}
}

function dateAdd(time,strInterval, Number) {   
    var dtTmp = time;
    switch (strInterval) {   
        case 's' :return new Date(Date.parse(dtTmp) + (1000 * Number));  
        case 'n' :return new Date(Date.parse(dtTmp) + (60000 * Number));  
        case 'h' :return new Date(Date.parse(dtTmp) + (3600000 * Number));  
        case 'd' :return new Date(Date.parse(dtTmp) + (86400000 * Number));  
        case 'w' :return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number)); 
        case 'q' :return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number*3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
        case 'm' :return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
        case 'y' :return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
    }  
};

Date.prototype.Format = function(fmt) {
    var o = {
         "M+" : this.getMonth() + 1, //月份  
         "d+" : this.getDate(), //日  
         "H+" : this.getHours(), //小时  
         "m+" : this.getMinutes(), //分  
         "s+" : this.getSeconds(), //秒  
         "q+" : Math.floor((this.getMonth() + 3) / 3), //季度  
         "S" : this.getMilliseconds() //毫秒  
    };
    if (/(y+)/.test(fmt)){
         fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for ( var k in o){
         if (new RegExp("(" + k + ")").test(fmt)){
             fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]): (("00" + o[k]).substr(("" + o[k]).length)));
         }
    }
    return fmt;
}

function flush() {
    $('#is_flush').val("true");
}