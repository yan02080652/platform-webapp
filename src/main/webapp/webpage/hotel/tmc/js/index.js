//我的任务定时器
var timerFlag ;
//全部任务定时器
var allTask_timer;


function showNoteToHotel(msg) {
    console.log(msg);
    $("#noteToHotelMsg1").text(msg.split("-")[0]);
    $("#noteToHotelMsg2").text(msg.split("-")[1]);
    layer.open({
        type: 1,
        area: ['600px', '400px'],
        title: "客户备注",
        closeBtn: 1,
        shadeClose: true,
        // shade :0,
        content: $("#noteToHotel"),
    });
}

//我的任务Tab
function getMyTask(){
    clearInterval(allTask_timer);
    var load_index = layer.load();
    $.ajax({
        type:"post",
        url:"hotel/tmc/getMyTaskTable",
        data:{taskStatus: 'PROCESSING'},
        success:function(data){
            layer.close(load_index);
            $("#myTask").html(data);
        },
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }
    });
}
//全部任务Tab
function getAllTask(){
    clearInterval(timerFlag);
    var load_index = layer.load();
    $.ajax({
        type:"post",
        url:"hotel/tmc/allTask",
        data:{taskStatus: 'WAIT_PROCESS'},
        success:function(data){
            layer.close(load_index);
            $("#allTask").html(data);
        },
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }
    });
}
//订单列表Tab
function getOrderList(){
    clearInterval(timerFlag);
    clearInterval(allTask_timer);
    var load_index = layer.load();
    $.ajax({
        type:"post",
        url:"hotel/tmc/orderList",
        data:{},
        success:function(data){
            layer.close(load_index);
            $("#orderList").html(data);
        },
        error:function(){
            layer.close(load_index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
        }

    });
}

//订单操作日志
function logList(orderId){
	layer.open({
		  type: 2,
		  area: ['800px', '500px'],
		  title: "订单日志",
		  closeBtn: 1,
		  shadeClose: true,
		 // shade :0,
		  content: "hotel/tmc/getOrderLog?orderId="+orderId,
		});
}

//加载收入明细
function loadIncomeDetail(orderId){
	layer.open({
		  type: 2,
		  area: ['600px', '400px'],
		  title: "收入明细",
		  closeBtn: 1,
		  shadeClose: true,
		 // shade :0,
		  content: "hotel/tmc/getIncomeDetail?orderId="+orderId,
		});
}