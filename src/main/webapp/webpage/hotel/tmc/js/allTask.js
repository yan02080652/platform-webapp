$(function(){
	//清空定时器
	clearInterval(allTask_timer);
	
    refreshAllTask();
})

function reloadMyTaskTable(pageIndex){

    var taskStatus = $("#allTaskStatusTab .active").attr("data-value");
    var taskType = $("#currentTaskType").val();
    reloadAllTask(pageIndex,taskStatus,taskType);
}

function showNoteToHotel(msg) {
    console.log(msg);
    $("#noteToHotelMsg1").text(msg.split("-")[0]);
    $("#noteToHotelMsg2").text(msg.split("-")[1]);
    layer.open({
        type: 1,
        area: ['600px', '400px'],
        title: "客户备注",
        closeBtn: 1,
        shadeClose: true,
        // shade :0,
        content: $("#noteToHotel"),
    });
}

//刷新
function refreash(orderId,thirdId,taskId){
	$.ajax({
		url: 'hotel/tmc/refreashOrderDetail',
		type: 'post',
		async: false,
		data: {
			orderId: orderId,
			thirdId: thirdId
		},
		success: function(data){
			if(data.result){
				$('.status_' + orderId).text('状态: ' + data.showStatus + '（' + data.orderStatus + '）');
				if(data.isShow){
					$('.confirm_' + orderId).html('<a onclick=confirmDone('+orderId+','+taskId+') style=color:#87af1c;> | 处理完毕</a> ');
				}
			}else{
				layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
		}
	})
}

//催单完毕
function urgeDone(orderId,taskId){
	$.ajax({
		url: 'hotel/tmc/urgeDone',
		type: 'post',
		async: false,
		data:{
			'orderId': orderId,
			'taskId': taskId,
		},
		success: function(data){
			if(data){
				layer.msg("恭喜你,催单成功啦！",{icon:1})
				setTimeout(function(){
					submitAjax_all(null,'PROCESSING',null);
				},200);
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon:2})
		}
	})
}

//取消订单
function cancelOrder(orderId,taskId){
	$.ajax({
		url: 'hotel/tmc/cancelOrder',
		type: 'post',
		async: false,
		data: {
			orderId: orderId,
			taskId: taskId
		},
		success: function(data){
			if(data.result){
				layer.msg(data.msg,{icon: 1})
				setTimeout(function(){
					submitAjax_all(null,'PROCESSING',null);
				},200);
			}else{
				layer.msg(data.msg,{icon:2})
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
		}
	})
}

//确认完毕
function confirmDone(orderId,taskId){
	$.ajax({
		url: 'hotel/tmc/confirmDone',
		type: 'post',
		async: false,
		data:{
			'orderId': orderId,
			'taskId': taskId,
		},
		success: function(data){
			if(data){
				layer.msg("恭喜你,任务处理成功啦！",{icon:1})
				setTimeout(function(){
					submitAjax_all(null,'PROCESSING',null);
				},200);
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon:2})
		}
	})
}

//交接
function transfer(taskId,operatorId){
	TR.select('user',{type: "me"},function(data){
		console.log(data);
		
		$.ajax({
			type:"post",
			url:"hotel/tmc/transfer",
			data:{
				taskId:taskId,
				operatorId:operatorId,
				userId:data.id,
				userName:data.fullname
			},
			success:function(rs){
				if(rs){
					showPromptBox("任务交给"+data.fullname);
					setTimeout(function(){
						submitAjax_all(null,'WAIT_PROCESS',null);
					},200);
				}else{
					layer.msg("系统错误，请刷新重试！",{icon:2})
				}
			}
		});
	});
}

//显示Model
function showModal(url, data, callback) {
	$('#model2').load(url, data, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

function showModal2(url, data, callback) {
    $('#model3').load(url, data, function(context, state) {
        if ('success' == state && callback) {
            callback();
        }
    });
}
//手动补单
function handRepair(orderId,taskId){
	showModal("hotel/tmc/handRepair",{orderId:orderId,taskId:taskId,flag:true},function(data){
		$('#ticket_model2').modal();
	});
}

function submitTicketForm(){
	showConfirm("保存之后将不可修改,是否确认补单?",function(){
		if(!validate()){
			return;
		}
		$.ajax({
			type:"post",
			url:"hotel/tmc/repair",
			data:$('#ticketForm').serialize(),
			success:function(data){
				if(data){
					$('#ticket_model2').modal('hide');
					showPromptBox("补单任务完成!")
					setTimeout(function(){
						submitAjax_all();
//					submitAjax_all(null,'PROCESSING',null);
					},200);
				}else{
					layer.msg("系统异常，保存失败!",{icon:2})
				}
			},
			error:function(){
				layer.msg("系统繁忙，请刷新重试",{icon:2})
			}
		});
	})
}

function validate(){
	var result = true;
	if($('#hotelOriginType').val() == ''){
		showError($('#hotelOriginType'),'请选择供应商');
		result = false;
	}
	if($('#externalOrderId').val().trim() == ''){
		showError($('#externalOrderId'),'请输入供应商订单号');
		result = false;
	}
	var $rates = $('.date_rate');
	for(var i = 0 ;i< $rates.length;i++){
		if($rates.eq(i).val().trim() == ''){
			showError($rates.eq(i),'请输入每日价格');
			result = false;
		}
	}
	
	return result;
}

//取消提交补单
function cancelSubmit(orderId){
	//放过该订单,用户发送失败短信
	showConfirm("放弃之后此订单将取消并退款,确认放弃?",function(){
		$.ajax({
			type:"post",
			url:"hotel/tmc/cancelRepair",
			data:{orderId: orderId},
			success:function(data){
				if(data.result){
					$('#ticket_model2').modal('hide');
					showPromptBox("放弃完成! 用户退款中")
					setTimeout(function(){
						submitAjax_all();
//						submitAjax_all(null,'PROCESSING',null);
					},200);
				}else{
					layer.msg(data.msg,{icon:2})
				}
			},
			error:function(){
				layer.msg("系统繁忙，请刷新重试",{icon:2})
			}
		});
	})
}

//错误提示
function showError($dom,msg){
	layer.tips(msg, $dom, {
		  tips: [1, '#3595CC'],
		  time: 4000
		});
	$dom.focus();
}

//确认提示
function showConfirm(msg,_callback){
	var index = layer.alert(msg, {
		  btn: ['确定','取消'],
		  skin: 'layui-layer-lan' //样式类名
		}, function(){
			layer.close(index);
			_callback();
		},function(){
			
		});
}

function submitAjax_all(pageIndex,taskStatus,taskType){

    $("#currentTaskType").val(taskType);
    $("#allTask_pageIndex").val(pageIndex);
    $("#allTask_task_status").val(taskStatus);
    $("#allTask_task_type").val(taskType);
	//查询条件
	var field1 = $("#search_Field2").val().trim();
	
	if(!taskStatus){
		taskStatus = $("#allTaskStatusTab .active").attr("data-value");
	}
	
	$.ajax({
		type:"post",
		url:"hotel/tmc/allTask",
		data:{
			taskStatus:taskStatus,
			pageIndex:pageIndex,
			taskType:taskType,
			keyword:field1,
		},
		success:function(data){
			$("#allTask").html(data);
			$("#search_Field2").val(field1);
		},
		error:function(){
			layer.msg("系统繁忙，请刷新重试")
		}
	});
}

//重置查询条件
function resetForm(){
	$('#orderSearchForm')[0].reset();
	// 再次请求
    reloadAllTask();
}

//右下角提示
function showPromptBox(content){
	layer.open({
		  type: 0,//基本层类型
		  title: "系统提示",
		  closeBtn: 0,//不显示关闭x关闭
		  btn:[],//不显示按钮
		  shade: 0,
		  area: ['250px', '200px'],//大小
		  offset: 'rb', //右下角弹出
		  time: 3000, //3秒后自动关闭
		  shift: 2,//动画0-6
		  content: content, 
		});
}

//是否自动刷新
$("#isCloseRefresh").click(function(){
	var $span = $(this).find("span");
	if ($("#isCloseRefresh").attr("value") == "true") {
		$("#isCloseRefresh").attr("value", "false");
		$span.text("关闭");
		refreshAllTask();
	} else {
		$("#isCloseRefresh").attr("value", "true");
		$span.text("开启");
		
	}
});

//allTask:自动刷新
var allTask_count = 9;
var allTask_timer ;
function refreshAllTask() {

    if ($('#is_flush').val() == "false") {
        return;
    }
	
	if ($("#isCloseRefresh").attr("value") == "true") {
		return;
	}
	$("#countDown").text(allTask_count);
	allTask_count--  ;
	if(allTask_count >= 0){
		allTask_timer = setTimeout(refreshAllTask, 1000);
	}else{
        var pageIndex = $("#allTask_pageIndex").val();
        var taskStatus = $("#allTask_task_status").val();
        var taskType = $("#allTask_task_type").val();
        if (taskStatus == '') {
            taskStatus = 'WAIT_PROCESS';
        }
        reloadAllTask(pageIndex,taskStatus,taskType);
	}
}


function reloadAllTask(pageIndex,taskStatus,taskType){
    $("#currentTaskType").val(taskType);
    pageIndex = pageIndex ? pageIndex : 1;
    $("#allTask_pageIndex").val(pageIndex);
    $("#allTask_task_status").val(taskStatus);
    $("#allTask_task_type").val(taskType);

    //查询条件
    var field1 = $("#search_Field2").val().trim();

    if(!taskStatus){
        taskStatus = $("#allTaskStatusTab .active").attr("data-value");
    }

    var layer_index = layer.load(0);
    $.ajax({
        type:"post",
        url:"hotel/tmc/allTask",
        data:{
            taskStatus:taskStatus,
            pageIndex:pageIndex,
            taskType:taskType,
            keyword:field1,
        },
        success:function(data){
            layer.close(layer_index);
            $("#allTask").html(data);
            $("#search_Field2").val(field1);
        },
        error:function(){
            layer.close(layer_index);
            layer.msg("系统繁忙，请刷新重试",{icon:2})
        }
    });
}

function showLineData(orderId,taskId){

    showModal2("hotel/tmc/showLineData",{orderId:orderId,taskId:taskId},function(data){
        $('#line_data').modal();
        $('#is_flush').val("false");
    });


}
