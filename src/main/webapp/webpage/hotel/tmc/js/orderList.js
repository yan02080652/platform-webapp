$(function(){
	submitForm();
})

//分页函数
function reloadOrderList(pageIndex){
	submitForm(pageIndex,$("#orderTable").find("li[class='active']").find("span").attr("data-value"));
}

function showNoteToHotel(msg) {
    console.log(msg);
    $("#noteToHotelMsg1").text(msg.split("-")[0]);
    $("#noteToHotelMsg2").text(msg.split("-")[1]);
    layer.open({
        type: 1,
        area: ['600px', '400px'],
        title: "客户备注",
        closeBtn: 1,
        shadeClose: true,
        // shade :0,
        content: $("#noteToHotel"),
    });
}

//取消订单
function cancelOrder(orderId,taskId){
	$.ajax({
		url: 'hotel/tmc/cancelOrder',
		type: 'post',
		async: false,
		data: {
			orderId: orderId,
			taskId: taskId
		},
		success: function(data){
			if(data.result){
				layer.msg(data.msg,{icon: 1})
				setTimeout(function(){
					submitAjax(null,'PROCESSING',null);
				},200);
			}else{
				layer.msg(data.msg,{icon:2})
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
		}
	})
}


function submitForm(pageIndex,orderStatus){

	var dateType = $("#searchField1").val();
	var startDate = $("#startDate3").val();
	var endDate = $("#endDate3").val();
	var field1 = $("#searchField2").val().trim();
	var field2 = $("#searchField3").val().trim();
	var field3 = $("#searchField4").val().trim();
	var hotelName = $("#hotelName").val().trim();

	var index = layer.load(0);
	
	$.ajax({
		url: "hotel/tmc/orderTable",
		type: "post",
		data: {	
			dateType: dateType,
			start: startDate,
			end: endDate,
			customerOrPhone: field1,
			orderNumber: field2,
			partnerName: field3,
			orderStatus: orderStatus,
			hotelName: hotelName,
			pageIndex:pageIndex,
			pageSize: 10
		},
		success:function(data){
			layer.close(index);
			$("#orderTable").html(data);
		},
		error:function(){
            layer.close(index);
            layer.msg("系统异常，请刷新重试",{icon: 2,offset:'t',time:1000})
		}
	});
	
}

//重置查询条件
function resetSearchForm(){
	$('#searchForm')[0].reset();
	$("#orderStatus li").each(function(){
		$(this).removeClass("active");
	})
	// 再次请求
	submitForm();
}

//TODO 客服介入
function receive(){
	layer.alert("客服介入")
}