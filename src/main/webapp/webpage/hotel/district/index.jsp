<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="ruleMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>酒店城市维护   &nbsp;&nbsp;

			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12">
										<div class="pull-left">
											<a class="btn btn-default" onclick="synDistrictData()" role="button">手动更新城市</a>
											<a class="btn btn-default" onclick="synElongData()" role="button">同步艺龙城市</a>
										</div>
										<!-- 查询条件 -->
										 <form class="form-inline" method="post" >
											<nav class="text-right">
												<div class=" form-group">
												 <label class="control-label">城市名：</label>
													<input name="name" id="name" value="${name}" type="text"  class="form-control" placeholder="中文/全拼/简拼" style="width: 250px"/>
												</div>
												<%--<div class="form-group">
													<label class="control-label">所属城市：</label>
													<input name="keyword" id="belongCity" type="text"  class="form-control" placeholder="中文" style="width: 250px"/>
												</div>--%>
												<div class=" form-group">
													<select class="form-control" style="width: 100px">
														<option data-code = "">全部</option>
														<c:choose>
															<c:when test="${empty isMatching}">
																<option  data-code = "true">匹配</option>
																<option data-code="false">未匹配</option>
															</c:when>
															<c:when test="${isMatching eq true}">
																<option  data-code = "true" selected="selected">匹配</option>
																<option data-code="false">未匹配</option>
															</c:when>
															<c:otherwise>
																<option  data-code = "true">匹配</option>
																<option data-code="false" selected="selected">未匹配</option>
															</c:otherwise>
														</c:choose>
													</select>
												</div>
												<div class="form-group">
													<button type="button" class="btn btn-default" onclick="search_districts()">查询</button>
													<button type="button" class="btn btn-default" onclick="reset_district()">重置</button>
												</div>
											</nav>
										 </form>
										</div>
									</div>
									<div  id="rule_Table" style="margin-top: 15px">

					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 详细页面 -->
<div id="ruleDetail" style="display:none;"></div>
<script type="text/javascript" src="webpage/hotel/district/district.js?version=${globalVersion}"></script>
