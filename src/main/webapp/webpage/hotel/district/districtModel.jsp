<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
         <div class="panel panel-default">
             <div class="panel-heading">
                <h3 align="center">酒店城市修改</h3>
             </div>
             <div class="panel-body">
                 <form:form id="districtForm" cssClass="form-horizontal bv-form" method="post" action="hotel/district/update" modelAttribute="district">
                 	<form:hidden path="id"/>
					 <div class="form-group">
						 <label class="col-sm-2 control-label">城市中文名</label>
						 <div class="col-sm-6">
							 <form:input path="nameCn" cssClass="form-control"/>
						 </div>
					 </div>
					 <div class="form-group">
							 <label class="col-sm-2 control-label">城市中文全拼</label>
						 <div class="col-sm-6">
							 <form:input path="nameEn" cssClass="form-control"/>
						 </div>
					 </div>
					 <div class="form-group">
						 <label class="col-sm-2 control-label">城市中文简拼</label>
						 <div class="col-sm-6">
							 <form:input path="shortName" cssClass="form-control"/>
						 </div>
					 </div>
					 <div class="form-group">
						 <label class="col-sm-2 control-label">所属城市</label>
						 <div class="col-sm-6">
							 <form:input path="districtName" cssClass="form-control" readonly="true" onclick="chooseDistrictData()"/>
							 <form:hidden path="districtId"/>
							 <form:hidden path="districtPinYin"/>
						 </div>
					 </div>
			</form:form>
				 <input type="hidden" id="name" value="${name}"/>
				 <input type="hidden" id="isMatching" value="${isMatching}">
				 <input type="hidden" id="pageIndex" value="${pageIndex}">
			<div class="form-group">
				<div class="col-sm-8">
					<center>
					    <a class="btn btn-primary"  onclick="submitDistrict()">确认修改</a>

						<a class="btn btn-default"  onclick="backUrlWithCondition()">返回不修改</a>
					</center>
				</div>
			</div>
         </div> 
	     </div>
	</div>
</div>
<script type="text/javascript">

//提交表单
function submitDistrict(){
    if (!validateForm()) {
        return;
	}
	$.ajax({
		type : "POST",
		url : "hotel/district/update",
		data : $('#districtForm').serialize(),
		async : false,
		error : function(request) {
			showErrorMsg("请求失败，请刷新重试");
		},
		success : function(data) {
            $.alert(data.txt,"提示");
            setTimeout(backUrlWithCondition, 500);
		}
	});
}

function validateForm() {

	if ($('#nameCn').val().trim() == '') {
		$.alert('城市中文名不能为空');
		return false;
	}
    if ($('#nameEn').val().trim() == '') {
        $.alert('城市中文全拼不能为空');
        return false;
    }
    if ($('#shortName').val().trim() == '') {
        $.alert('城市中文简拼不能为空');
        return false;
    }
    return true;
}

function backUrl() {
    location.href = '/hotel/district';
}

function backUrlWithCondition() {
    var isMatching = $("#isMatching").val();

    location.href = '/hotel/district?&isMatching='+isMatching;
    var loadingList = layer.load();
}

var getDistrictDataChoosedData = JSON.parse('${city}' || '[]');
function chooseDistrictData() {

    new choose.districtData({
        id:'selMultiDistrictData',//给id可以防止一直新建对象
        empty:true,
        getChoosedData:function () {
            return getDistrictDataChoosedData;
        }
    },function (data) {
       
        getDistrictDataChoosedData = [];
        if (data) {
            var city = {id: data.id, nameCn: data.nameCn};
            getDistrictDataChoosedData.push(city);
            $("#districtId").val(data.id);
            $("#districtName").val(data.nameCn);
            $("#districtPinYin").val(data.nameEn);
        }
    });
}
</script>