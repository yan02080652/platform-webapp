$(function(){
    reloadDistrict();
})

function synDistrictData() {
    layer.confirm('确认更新城市数据吗?',function (index) {
        layer.close(index);
        var loading = layer.load();
        $.ajax({
            url: "/hotel/district/batchUpdate",
            type: "get",
            async: true,
            success: function (data) {
                layer.close(loading);
                if (data) {
                    location.href = "/hotel/district";
                } else {
                    $.alert('系统繁忙,请稍后重试');
                }
            },error:function () {
                layer.close(loading);
                $.alert('系统繁忙,请稍后重试');
            }
        });
    });

}

var cityList;
$.ajax({
    url: "hotel/district/list",
    type: "post",
    datatype: "json",
    async: true,
    success: function (data) {
        cityList = data;

    },error:function () {
        layer.close(loading);
        $.alert('系统繁忙,请稍后重试');
    }
});


function reloadDistrict(pageIndex){
	var loadingList = layer.load();
	//加载列表
	$.post('/hotel/district/list',
		{
			pageIndex:pageIndex,
            name : $('#name').val(),
            isMatching : $("option:selected").attr('data-code')
		},function(data){
            layer.close(loadingList);
			$("#rule_Table").html(data);
	});
}


function search_districts(){
    var pageIndex = $("#pageIndex").val();
    reloadDistrict(pageIndex);
}
//转到添加或修改页面
function getDistrictById(id){
    var  isMatching = $("option:selected").attr('data-code');
    $.post("/hotel/district/getDistrict",{id:id,isMatching:isMatching},function(data){
        $("#ruleDetail").html(data);
        $("#ruleMain").hide();
        $("#ruleDetail").show();
    });
}


//重置
function reset_district(){
    $('select option').eq(0).attr('selected', 'selected');
    reloadDistrict();

}

//返回
function backStationMain(){
    reloadDistrict();
}

//同步艺龙城市
function synElongData(){
    layer.confirm('确认同步艺龙城市数据吗?',function (index) {
        layer.close(index);
        var loading = layer.load();
        $.ajax({
            url: "hotel/district/synElongData",
            type: "post",
            success: function (data) {
                layer.close(loading);
                if (data.success) {
                    $.alert('同步成功');
                } else {
                    $.alert('系统繁忙,请稍后重试');
                }
            },error:function () {
                layer.close(loading);
                $.alert('系统繁忙,请稍后重试');
            }
        });
    });
}





