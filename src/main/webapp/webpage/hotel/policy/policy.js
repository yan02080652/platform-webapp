$(function(){
    reloadPolicy();
})

//加载列表
function reloadPolicy(pageIndex){
    var loadingList = layer.load();
    //加载列表
    $.ajax({
        url: '/hotel/policy/list',
        type: 'post',
        data:{
            pageIndex : pageIndex,
            keywords : $("#keyWords").val(),
            status :$("#status").val(),
            validity :$("#validity").val(),
        },success:function(data){
            layer.close(loadingList);
            $("#rule_Table").html(data);
        }
    })
}

//跳转至新增修改政策
function getPolicy(id){
    $.post('hotel/policy/detail',
        {
        id:id
        },function(data){
        $("#ruleDetail").html(data);
        $("#ruleMain").hide();
        $("#ruleDetail").show();
        });
}

//重置查询条件
function resetSelect(){
    $('#keyWords').val('');
    $("#status").val("");
    $("#validity").val("");
    reloadPolicy();
}
//删除政策
function deletePolicy(id){
    layer.confirm('确认删除政策数据吗?',function (index) {
        layer.close(index);
        var loading = layer.load();
        $.ajax({
            url: '/hotel/policy/delete',
            data:{id:id},
            success: function (data) {
                layer.close(loading);
                alert(data.txt);
                if (data.type=='success') {
                    location.href = "/hotel/policy";
                } else {
                    $.alert('系统繁忙,请稍后重试');
                }
            },error:function () {
                layer.close(loading);
                $.alert('系统繁忙,请稍后重试');
            }
        });
    });
}
//返回首页
function backRuleMain(){
    reloadPolicy();
}
