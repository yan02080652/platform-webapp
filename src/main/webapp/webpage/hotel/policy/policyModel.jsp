<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<style>
    .reqMark {
        color: red;
        font-weight: bold;
        font-size: 15px;
    }
</style>
<div class="panel-body">
    <div class="row">
    <div class="col-md-12" style="margin-bottom: 5px">
            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>酒店政策信息
            <c:choose>
                <c:when test="${hotelPolicyDto.id !=null}">修改</c:when>
                <c:otherwise>添加</c:otherwise>
            </c:choose>
            <a href="hotel/policy">返回</a>
    </div>
            <div class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
            <div class="panel panel-default"><div class="panel-body">
                <div class="row">
                <ul id="myTab" class="nav nav-tabs">
                    <li class="active">
                        <a href="#baseTab">
                            基础信息
                        </a>
                    </li>
                    <li><a href="#hotelTab">酒店选择</a></li>
                    <li>
                        <a href="#policyDetailListTab">政策细则</a>
                    </li>
                </ul>
                <div id="myTabContent" class="tab-content" style="margin-top: 15px;margin-left:15px;">
                    <div class="tab-pane fade in active" id="baseTab" >

                        <form:form id="policyForm" cssClass="form-horizontal" method="post" action="hotel/policy/update" modelAttribute="hotelPolicyDto">
                            <form:hidden path="id"/>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><span class="reqMark">*</span>政策名称</label>
                                <div class="col-sm-5">
                                    <form:input path="name" placeholder="政策名称" cssClass="form-control" maxlength="50"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">优先级</label>
                                <div class="col-sm-5">
                                    <form:input path="level" placeholder="优先级" cssClass="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><span class="reqMark">*</span>适用入住日期
                                    <i class="effective-date fa fa-question-circle layui-word-aux"></i>
                                </label>
                                <div class="col-sm-5">
                                    <form:input path="checkIn" placeholder="适用入住日期(示例:20180214-20180222)" cssClass="form-control" maxlength="100"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><span class="reqMark">*</span>报价展示标签</label>
                                <div class="col-sm-5">
                                    <form:input path="showLabe" placeholder="报价展示标签" cssClass="form-control" maxlength="10"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><span class="reqMark">*</span>显示特惠标签</label>
                                <div class="col-sm-5">
                                    <label class="radio-inline">
                                        <form:checkbox path="isShowLabe" value="1" label="显示"/>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><span class="reqMark">*</span>状态</label>
                                <div class="col-sm-5">
                                    <label class="radio-inline">
                                        <form:radiobutton value="1" label="启用" path="status" checked="true"/>
                                    </label>
                                    <label class="radio-inline">
                                        <form:radiobutton value="0" label="停用" path="status" />
                                    </label>
                                </div>
                            </div>
                            <div id="operatorInfo">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">创建人</label>
                                <label  class="col-sm-1 control-label">${hotelPolicyDto.creator}</label>
                                <label class="col-sm-2 control-label">创建时间</label>
                                <label path="createTime" class="col-sm-2 control-label"> <fmt:formatDate value="${hotelPolicyDto.createTime}" pattern="yyyy-MM-dd　HH：mm"/></label>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">最后修改人</label>
                                <label class="col-sm-1 control-label">${hotelPolicyDto.updater}</label>
                                <label class="col-sm-2 control-label">最后修改时间</label>
                                <label  class="col-sm-2 control-label"><fmt:formatDate value="${hotelPolicyDto.updateTime}" pattern="yyyy-MM-dd　HH：mm"/></label>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-6">
                                    <a class="btn btn-primary" onclick="savePolicy()" >保存</a>
                                </div>
                            </div>
                        </form:form>
                        <div class="effectiveDate" hidden>
                            <p>每个日期区间按照【起始日期】+【"-"】+【截止日期】构成，多个日期区间以英文","分隔</p>
                            <p>示例:20180214-20180222,20181001-20181007</p>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="hotelTab">
                        <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">酒店品牌</label>
                            <div class="col-sm-5">
                                <input type="hidden" id="hotelBrandId" value="${hotelPolicyDto.hotelBrandId}">
                                <input class="form-control" id="brandsDesc" value="${hotelPolicyDto.brandName}"  readonly onclick="selHotelBrandData()" style="width: 100%;"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" >适用酒店</label>
                            <div class="col-sm-5">
                                <input type="hidden" id="hdapplyType" value="${hotelPolicyDto.applyType}">
                                <label class="radio-inline"><input type="radio" name="applyType" value="1" <c:if test="${hotelPolicyDto.applyType==null||hotelPolicyDto.applyType==1}">checked</c:if> />适用所有酒店</label>
                                <label class="radio-inline"><input type="radio" name="applyType" value="2" <c:if test="${hotelPolicyDto.applyType==2}">checked</c:if> />适用以下酒店</label>
                                <label class="radio-inline"><input type="radio" name="applyType" value="3" <c:if test="${hotelPolicyDto.applyType==3}">checked</c:if> />不适用以下酒店</label>
                            </div>
                        </div>
                        <div  id="policyHotel">

                        </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-6">
                                <a class="btn btn-primary" onclick="savePolicyOtherInfo()" >保存</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade " id="policyDetailListTab">
                        <div style="margin:0 15px" id="policyDetailList">

                        </div>
                    </div>
                </div>
                </div>
            </div>
            </div></div></div>
            </div>

</div>
</div>
<%--<input type="hidden" name="policyHotelRooms" >--%>
<div id="detailDiv"></div>
<script type="text/javascript" src="webpage/hotel/policy/policyDetail.js?version=${globalVersion}"></script>