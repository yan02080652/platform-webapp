<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="form-group">
    <div class="pull-left">
        <button type="button" class="btn btn-primary" onclick="getPolicyDetail()" >+新增细则</button>
    </div>
</div>
<div class="form-group">
    <table class="table table-striped table-bordered table-hover" id="ruleTable" style="table-layout:fixed">
        <thead>
        <tr>
            <th width="10%"><label>基准来源</label></th>
            <th width="15%"><label>适用入住日期</label></th>
            <th width="15%"><label>适用房型</label></th>
            <th width="10%"><label>早餐份数</label></th>
            <th width="10%"><label>支付方式</label></th>
            <th width="10%"><label>入住人数</label></th>
            <th width="10%"><label>销售价格</label></th>
            <th width="10%"><label>取消规则</label></th>
            <th width="10%"><label>操作</label></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${pageList.list}" var="hotelPolicyDetailDto">
        <tr>
            <td>${hotelPolicyDetailDto.originName}</td>
            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${hotelPolicyDetailDto.checkIn}</td>
            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${hotelPolicyDetailDto.roomNames=="all"?"全部":hotelPolicyDetailDto.roomNames}</td>
            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${hotelPolicyDetailDto.breakfastCount=="all"?"不限":hotelPolicyDetailDto.breakfastCount}</td>
            <td>${hotelPolicyDetailDto.payType==1?"预付":"现付"}</td>
            <td>${hotelPolicyDetailDto.customerNumber==-1?"不限":hotelPolicyDetailDto.customerNumber}</td>
            <td>${hotelPolicyDetailDto.floatingAmount==0?"固定金额":"浮动金额"}</td>
            <td>${hotelPolicyDetailDto.cancelType==1?"自有取消规则":"标准取消规则"}</td>
            <td class="text-center" >
                <a onclick="getPolicyDetail(${hotelPolicyDetailDto.id})" >修改</a>
                <a onclick="deletePolicyDetail(${hotelPolicyDetailDto.id})" >删除</a>
            </td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadPolicyDetailList"></jsp:include>
    </div>
</div>

