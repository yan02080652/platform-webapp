$(function(){
    initControl();

    intercept.showHelpTip(".effective-date", $(".effectiveDate").html());
    initValidate();
})

//tab切换
var policyId;
function initControl(){
    policyId=$('#id').val();
    if(policyId==''){
        $('#operatorInfo').hide();
        if($('input[name=showLabe]').val()==''){
            $('input[name=showLabe]').val('直销价');
        }
    }else{
        $('#myTab a:eq(1)').tab().show();
        if(!checkHotelInfo()){
            /*$('#myTab a:eq(2)').tab().hide();*/
            searchDis(false);
        }
        $('#operatorInfo').show();
    }
    $('#myTab a').click(function (e) {
        e.preventDefault();
        var flag=true;
        if($(this).attr("href")=="#hotelTab"){
            if(policyId==''){
                $.alert("请完善基础信息,并保存","提示");
                return;
            }
            applyTypeChange();
            var isshow=checkHotelInfo();
            var applyType=$("input[type=radio][name='applyType']:checked").val();
            if(applyType==2||applyType==3){
                loadPolicyHotelList(1);
                searchDis(isshow);
            }

            if(isshow){
                $('#myTab a:eq(2)').tab().show();
            }
        }else if($(this).attr("href")=="#policyDetailListTab"){
            if($("#hdapplyType").val()==null||$("#hdapplyType").val()==""){
                $.alert("请完善酒店信息,并保存","提示");
                return;
            }
            if(checkHotelInfo()){
                reloadPolicyDetailList();
            }else{
                flag=false;
                $.alert("请设置酒店信息", "提示");
            }
        }
        if(flag){
            $(this).tab('show');
        }

    })
}

function errorMsg(ele, msg) {
    layer.tips(msg, ele, {
        tips: [1, '#3595CC'],
        time: 4000
    });
};


/***************政策*****************/
//跳转至新增修改政策
function getPolicy(id){
    $.post('hotel/policy/detail',
        {
            id:id
        },function(data){
            $("#ruleDetail").html(data);
            $("#ruleMain").hide();
            $("#ruleDetail").show();
        });
}

function dateCheck(){
    $("#checkIn").on("keyup",function(){
        var pattern=/^((((1[6-9]|[2-9]\d)\d{2})(0?[13578]|1[02])(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0?[13456789]|1[012])(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})0?2(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0?229))-((((1[6-9]|[2-9]\d)\d{2})(0?[13578]|1[02])(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0?[13456789]|1[012])(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})0?2(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0?229))$/;
        var pattern1=/(\d{4})(\d{2})(\d{2})/;
        var len=$(this).val().length;
        var obj=$(this);
        var startdate,enddate;
        if(len==17){
            startdate=$(this).val().split('-')[0].replace(pattern1, '$1-$2-$3');
            enddate=$(this).val().split('-')[1].replace(pattern1, '$1-$2-$3');
            if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                obj.val('');
                return false;
            }
        }else if(len>=35&&(len-35)%18==0){
            var arr=$(this).val().split(',');
            arr.forEach(function(value,index,item){
                if(!pattern.test(item[index])){
                    obj.val('');
                    return false;
                }
                if(index==0){
                    startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                    enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                    if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                        obj.val('');
                        return false;
                    }
                }else{
                    if(new Date(enddate).getTime()>new Date(item[index].split('-')[0].replace(pattern1, '$1-$2-$3')).getTime()){
                        obj.val('');
                        return false;
                    }
                    startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                    enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                    if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                        obj.val('');
                        return false;
                    }
                }
            })
        }

    })
}
//验证
function initValidate(){
    dateCheck();
   /* $("#policyForm").bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                message: '政策名称验证失败',
                validators: {
                    notEmpty: {
                        message: '政策名称不能为空'
                    },
                    stringLength:{
                        max:50,
                        message:'最大可输入50字符'
                    }
                }
            },
            checkIn: {
                validators: {
                    notEmpty: {
                        message: '适用入住日期不能为空'
                    }
                }
            },
            level:{
                validators:{
                    regexp:{
                        regexp:/^0|[1-9][0-9]?$/,
                        message:'请输入"1-99"的正数'
                    }
                }
            },
            showLabe: {
                validators: {
                    notEmpty: {
                        message: '报价展示标签不能为空'
                    }
                }
            }
        }
    });*/

}
//保存酒店政策信息
function savePolicy(){
    var checkIn=$.trim($("input[name=checkIn]").val());
    var pattern=/^((((1[6-9]|[2-9]\d)\d{2})(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0[13456789]|1[012])(0[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})02(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0229))-((((1[6-9]|[2-9]\d)\d{2})(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0[13456789]|1[012])(0[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})02(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0229))$/;
    var pattern1=/(\d{4})(\d{2})(\d{2})/;
    if(checkIn!=''){
        var arr=checkIn.split(',');
        var flag=true;
        var startdate,enddate;
        arr.forEach(function(value,index,item){
            if(!pattern.test(item[index])){
                flag=false;
                return false;
            }
            if(index==0){
                startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                    flag=false;
                    return false;
                }
            }else{
                if(new Date(enddate).getTime()>new Date(item[index].split('-')[0].replace(pattern1, '$1-$2-$3')).getTime()){
                    flag=false;
                    return false;
                }
                startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                    flag=false;
                    return false;
                }
            }
        })
        if(!flag){
            errorMsg($("input[name=checkIn]"), '适用适用入住日期格式不正确，请重新输入');
            //alert("适用适用入住日期格式不正确，请重新输入");
            return;
        }
    }
    if($("input[name=name]").val()==''){
        errorMsg($("input[name=name]"), '政策名称不能为空');
        return;
    }
    if($("input[name=checkIn]").val()==''){
        errorMsg($("input[name=checkIn]"), '适用入住日期不能为空');
        return;
    }
    var level=$.trim($("input[name=level]").val());

    if(level!='' && !/^([1-9][0-9]?|100)$/.test(level)){
        errorMsg($("input[name=level]"), '请输入"1-100"的正数');
        return;
    }
    if($("input[name=showLabe]").val()==''){
        errorMsg($("input[name=showLabe]"), '报价展示标签不能为空');
        return;
    }

  /*  var bootstrapValidator = $("#policyForm").data('bootstrapValidator');
    bootstrapValidator.validate();
    if(bootstrapValidator.isValid()) {*/
        $.ajax({
            type: "POST",
            url: "hotel/policy/update",
            data: $('#policyForm').serialize(),
            async: false,
            error: function (request) {
                showErrorMsg("请求失败，请刷新重试");
            },
            success: function (data) {

                if(data.type=='success') {
                    if(policyId==''){
                        $.alert("新增成功", "提示");
                        getPolicy(data.txt);
                        //location.href = "/hotel/policy/detail?id="+data.data;
                    }else{
                        $.alert(data.txt, "提示");
                        //location.href = "/hotel/policy/detail?id="+policyId;
                        getPolicy(policyId);
                    }

                }
                //initControl();
            }
        });
    //}
}
//保存酒店政策品牌等信息
function savePolicyOtherInfo(){
    var applytype=$("input[type=radio][name='applyType']:checked").val();

    if(applytype=="2"||applytype=="3"){
        if(!checkHotelInfo(applytype)){
            $.alert("请设置酒店信息", "提示");
          return;
        }
    }

    $.ajax({
        type: "POST",
        url: "hotel/policy/updatePolicyOther",
        data: {
            id:policyId,
            hotelBrandName:$("#brandsDesc").val(),
            hotelBrandId:$("#hotelBrandId").val(),
            applyType:applytype
        },
        async: false,
        error: function (request) {
            showErrorMsg("请求失败，请刷新重试");
        },
        success: function (data) {
            $("#hdapplyType").val(applytype);
            $.alert(data.txt, "提示");
        }
    })
}
var intercept={
    showHelpTip: function (className, dom) {
        $(document).on('mouseover', className, function () {
            var that = this;
            var index = layer.tips(dom, that, {
                tips: [2, '#1AA094'],
                time: 0,
                area: '500px'
            });
            $(that).on('mouseleave', function () {
                layer.close(index);
            })
        });
    },
    tipData: [
        {className: ".effective-date", dom: $(".effectiveDate").html()},
    ],
}

/***************酒店******************/
var getHotelBrandChoosedData=[];
var getHotelChoosedData=[];
//酒店品牌
function selHotelBrandData() {
    new choose.hotelBrand({
        id:'selBrand',//给id可以防止一直新建对象
        multi:false,
        empty:true,
        getChoosedData:function () {
            return getHotelBrandChoosedData;
        }
    },function (data) {
        console.log("sss", data);
        var brandsDesc = '';
        getHotelBrandChoosedData = [];
        if (data) {
            $(data).each(function (i, item) {
                getHotelBrandChoosedData.push({id: item.id, shortName: item.shortName});
                if (i <= 2) {
                    brandsDesc += item.shortName+",";
                }
                if (i == 3) {
                    brandsDesc = brandsDesc.substr(0, brandsDesc.length - 1) + '  ...';
                }
            });
            if (getHotelBrandChoosedData.length <= 3) {
                brandsDesc = brandsDesc.substr(0, brandsDesc.length - 1);
            }
        }
        $('#brandsDesc').val(brandsDesc);
        if(getHotelBrandChoosedData.length>0){
            $('#hotelBrandId').val(getHotelBrandChoosedData[0].id);
        }else{
            $('#hotelBrandId').val("");
        }
        var applytype=$("input[type=radio][name='applyType']:checked").val();
        if(applytype=="1"){
            $('#policyHotel').empty();
        }else{
            loadPolicyHotelList();
        }

    });
};
//酒店
function selHotelData() {
    getHotelChoosedData = [];
    new choose.hotelByBrand($("#hotelBrandId").val(),{
        //id:'selMultiHotel',//给id可以防止一直新建对象
        multi:true,
        empty:true,
        getChoosedData:function () {
            return getHotelChoosedData;
        }
    },function (data) {
        var brandsDesc = '';
        getHotelChoosedData = [];
        if (data) {
            $(data).each(function (i, item) {
                if(getHotelChoosedData.indexOf(item)==-1)
                    getHotelChoosedData.push({hotelId: item.id, name: item.name});

            });
            //新增酒店
            addHotels();
        }
    });
};
//新增酒店
function addHotels(){
    if(getHotelChoosedData.length==0){
        $.alert("请选择酒店", "提示");
        return;
    }
    $.post('/hotel/policy/addHotels',{
        policyId:policyId,
        hotels: JSON.stringify(getHotelChoosedData),
        brandId:$("#hotelBrandId").val()
    },function(data){
       if(data.type=='success'){
           loadPolicyHotelList();
       }else{
           $.alert(data.txt,"提示");
       }
    })
}
//删除酒店
function batchHotelDeletes(){
    var ids=[];
    var checks=$("#hoteltable input[name='cb-hotel']:checked");
    checks.each(function(){
        if(ids.indexOf($(this).attr('data-hotel'))==-1)
            ids.push($(this).attr('data-hotel'));
    })
    deleteHotels(ids);
}
function deleteHotel(id){
    var ids=[];
    ids.push(id);
    deleteHotels(ids);
}
function deleteHotels(ids){
    if(ids instanceof Array){
        if(ids.length==0){
            $.alert("请选择酒店数据", "提示");
            return;
        }
        $.confirm({
            title : '提示',
            confirmButton:'确认',
            cancelButton:'取消',
            content : "确认删除所选记录?",
            confirm : function() {
                $.ajax({
                    cache : true,
                    type : "POST",
                    url : "hotel/policy/deleteHotels",
                    data : {
                        policyId:policyId,
                        ids: JSON.stringify(ids)
                    },
                    async : false,
                    error : function(request) {
                        showErrorMsg("请求失败，请刷新重试");
                    },
                    success : function(data) {
                        $.alert(data.txt,"提示");
                        if(data.type=='success'){
                            loadPolicyHotelList(1);
                            searchShow();
                        }
                    }
                });
            },
            cancel : function() {
            }
        });
    }
}
//适用酒店类型切换
function applyTypeChange(){

    $("input[type=radio][name='applyType']").off("change").change(function() {
        if(this.value ==1){
            $('#policyHotel').empty();
        }else{
            searchShow();
            loadPolicyHotelList(1);
        }
    })
}
//加载策略适用酒店
function loadPolicyHotelList(pageIndex){
    $.ajax({
        type: "POST",
        url: "hotel/policy/policyHotelList",
        data: { policyId:policyId,
            brandId: $('#hotelBrandId').val(),
            hotelName:typeof($('#hotelNameSearch').val())=="undefined"?"":$('#hotelNameSearch').val(),
            pageIndex:pageIndex
        },
        async: false,
        error: function (request) {
            showErrorMsg("请求失败，请刷新重试");
        },
        success: function (data) {
            $('#policyHotel').html(data);
            initHotel();
        }
    })
}
//查询
function hotelSearch(){
    loadPolicyHotelList();
}
//重置
function resetHotelSearch(){
    $("#hotelNameSearch").val('');
    loadPolicyHotelList();
}
//全选
function allcheck() {
    var nn = $("#allboxs").is(":checked");
    var namebox = $("#hoteltable input[name='cb-hotel']");
    for(var i = 0; i < namebox.length; i++) {
            namebox[i].checked=(nn == true);
    }
}
function initHotel(){
    $("#hoteltable input[name='cb-hotel']").click(function(){
        if(!$(this).is(":checked")){
            $("#allboxs").prop("checked",false);
        }
    })
}
//是否显示酒店搜索框
function searchShow(){
    searchDis(checkHotelInfo())
}
function searchDis(flag){
    if(flag){
        $("#div_search").show();
    }else{
        $("#div_search").hide();
        $("#hotelNameSearch").val('');
    }
}

/****************政策细则*******************/
var getHotelRoomChoosedData;
var hotelRoomSelectMulti=true;
//酒店房型
function selHotelRoomData() {
    new choose.hotelRoomByBrandAndHotelIds(policyId,{
        //id:'selMultiHotel',//给id可以防止一直新建对象
        multi:hotelRoomSelectMulti,
        empty:true,
        getChoosedData:function () {
            return getHotelRoomChoosedData;
        }
    },function (data) {
        var hotelRoomsDesc = '';
        var hotelRoomsContent='';
        getHotelRoomChoosedData=[];
        if (data) {
            $(data).each(function (i, item) {
                getHotelRoomChoosedData.push({name: item.name});
                if (i <= 2) {
                    hotelRoomsDesc += item.name+",";
                }
                if (i == 3) {
                    hotelRoomsDesc = hotelRoomsDesc.substr(0, hotelRoomsDesc.length - 1) + '  ...';
                }
                if(i <= 9){
                    hotelRoomsContent+=item.name +",";
                }
                if (i == 10) {
                    hotelRoomsContent = hotelRoomsContent.substr(0, hotelRoomsContent.length - 1) + '  ...';
                }
            });
            if (getHotelRoomChoosedData.length > 0 ) {

                if (getHotelRoomChoosedData.length <= 3) {
                    hotelRoomsDesc = hotelRoomsDesc.substr(0, hotelRoomsDesc.length - 1);
                }
                if(getHotelRoomChoosedData.length <= 10){
                    hotelRoomsContent = hotelRoomsContent.substr(0, hotelRoomsContent.length - 1);
                }
            }
            $("#hotelRoomNameDesc").val(hotelRoomsDesc);
            $("#hotelRoomsContent").html(hotelRoomsContent);
        }
    });
};
//检查酒店数据
function checkHotelInfo(applyType){
    var res=false;
    $.ajax({
        type: "POST",
        url: "hotel/policy/checkHotelInfo",
        data: {policyId:policyId,applyType:applyType,brandId:$('#hotelBrandId').val()},
        async: false,
        error: function (request) {
            showErrorMsg("请求失败，请刷新重试");
        },
        success: function (data) {
            if(data.type=='success'){
                res=true;
            }
        }
    })
    return res;
}

//跳转至新增修改酒店政策细则
function getPolicyDetail(detailId){
    var url="hotel/policy/policyDetail?id="+policyId;
    if(detailId){
        url+="&detailId="+detailId;
    }
    $('#detailDiv').load(url, function (context, state) {
        if ('success' == state) {
            $('#policyDetailModel').modal();
            initPolicyDetail();
           /* var time_id = setInterval(function(){
                if(isLoaded) {
                    clearInterval(time_id);
                    setRooms();
                }
            },1000);*/
        }
    });
}

//加载酒店政策详情列表
function reloadPolicyDetailList(pageIndex){
    var loadingList = layer.load();
    //加载列表
    $.post('/hotel/policy/policyDetailList',
        {
            policyId:policyId,
            pageIndex:pageIndex
        },function(data){
            layer.close(loadingList);
            $("#policyDetailList").html(data);
        });
}

//删除酒店政策细则
function deletePolicyDetail(id){
    $.confirm({
        title : '提示',
        confirmButton:'确认',
        cancelButton:'取消',
        content : "确认删除所选记录?",
        confirm : function() {
            $.ajax({
                cache : true,
                type : "POST",
                url : "hotel/policy/deletePolicyDetail",
                data : {
                    policyId:policyId,
                    id: id
                },
                async : false,
                error : function(request) {
                    showErrorMsg("请求失败，请刷新重试");
                },
                success : function(data) {
                    if(data.type=='success'){
                        reloadPolicyDetailList();
                    }else{
                        $.alert(data.txt,"提示");
                    }
                }
            });
        },
        cancel : function() {
        }
    });
}


function salePriceTypeDis(check){
    $("#input_floatingAmount").attr('disabled',check);
    $("#fixedAmount_salePriceType2").attr('disabled',check);
    $("#fixedAmount_salePriceType1").attr('disabled',!check);
    if(check){
        $("#input_floatingAmount").val('');
        $("#fixedAmount_salePriceType2").val('');
    }else{
        $("#fixedAmount_salePriceType1").val('');
    }
}
//房型初始化
/*function initPolicyRooms(){
    if($("#basicOrigin").val()=="self"){
        $("input[name=cbx_roomNames][value='all']").prop("checked",false).attr("disabled","disabled");
    }else{
        $("input[name=cbx_roomNames][value='all']").removeAttr("disabled");
    }
    $("#basicOrigin").on("change",function(){
        if($(this).val()=='self'){
            $("input[name=cbx_breakfastCount]").prop("checked",false);
            $("input[name=cbx_roomNames]").prop("checked",false);
            $("input[name=cbx_breakfastCount][value='all']").prop("checked",false).attr("disabled","disabled");
            $("input[name=cbx_roomNames][value='all']").prop("checked",false).attr("disabled","disabled");
            $("#customerNumber").find("option[value='-1']").attr("selected",false).attr("disabled","disabled");
            $("#divsaleprice").hide();
            $("#span_cancelType").hide();
        }else{
            $("input[name=cbx_breakfastCount][value='all']").removeAttr("disabled");
            $("input[name=cbx_roomNames][value='all']").removeAttr("disabled");
            $("#customerNumber").find("option[value='-1']").removeAttr("disabled");
            $("#divsaleprice").show();
            $("#span_cancelType").show();
        }
        initSelectAndCheckContent();
    });

    //房型
    var roomNames=$("input[name=roomNames]").val();
    debugger;
    if(roomNames!=''&&roomNames!='all'){
        var rooms=roomNames.split(',');
        $("input[name=cbx_roomNames]").each(function(){
            var v=$(this);
            rooms.forEach(function(value,index,item){
                if(item[index]==v.val()){
                    v.prop("checked",true);
                }
            })
        })
    }else if(roomNames=='all'){

        var namebox = $("input[name=cbx_roomNames]");
        for(var i = 0; i < namebox.length; i++) {
            namebox[i].checked=true;
        }
    }
    $("input[name=cbx_roomNames]").click(function(){
        if($("#basicOrigin").val()=="self"){
            var nn = $(this).is(":checked")
            var namebox = $("input[name=cbx_roomNames]");
            for(var i = 0; i < namebox.length; i++) {
                namebox[i].checked=false;
            }
            $(this).prop("checked",nn);
            return true;
        }
        if($(this).val()=="all"){
            var nn = $(this).is(":checked")
            var namebox = $("input[name=cbx_roomNames]");
            for(var i = 0; i < namebox.length; i++) {
                namebox[i].checked=(nn == true);
            }
        }else{
            if(!$(this).is(":checked")){
                $("input[name=cbx_roomNames][value=all]").prop("checked",false);
            }
        }
    })
}*/
//页面初始化
function initPolicyDetail(){
    if($("#basicOrigin").val()=="self"){
        $("input[name=cbx_breakfastCount][value='all']").prop("checked",false).attr("disabled","disabled");
        $("#customerNumber").find("option[value='-1']").attr("selected",false).attr("disabled","disabled");
        $("#span_cancelType").hide();
        $("#divsaleprice").hide();
    }else{
        $("input[name=cbx_breakfastCount][value='all']").removeAttr("disabled");
        $("#customerNumber").find("option[value='-1']").removeAttr("disabled");
        $("#divsaleprice").show();
    }
    if($("input[name=selfCancelType]").val()=="3"){
        $(".cancel_rule").show();
    }else{
        $(".cancel_rule").hide();
    }
    $("#basicOrigin").on("change",function(){
        if($(this).val()=='self'){
            $("input[name=cbx_roomNames]").prop("checked",false);
            $("input[name=cbx_breakfastCount][value='all']").prop("checked",false).attr("disabled","disabled");
            $("#customerNumber").find("option[value='-1']").attr("selected",false).attr("disabled","disabled");
            $("#divsaleprice").hide();
            $("#span_cancelType").hide();
        }else{
            $("input[name=cbx_breakfastCount][value='all']").removeAttr("disabled");
            $("#customerNumber").find("option[value='-1']").removeAttr("disabled");
            $("#divsaleprice").show();
            $("#span_cancelType").show();
        }

        initSelectAndCheckContent();
    });

    $("#policydetailForm #checkIn").on("keyup",function(){
        var pattern=/^((((1[6-9]|[2-9]\d)\d{2})(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0[13456789]|1[012])(0[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})02(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0229))-((((1[6-9]|[2-9]\d)\d{2})(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0[13456789]|1[012])(0[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})02(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0229))$/;
        var pattern1=/(\d{4})(\d{2})(\d{2})/;
        var len=$(this).val().length;
        var obj=$(this);
        var startdate,enddate;
        if(len==17){
            startdate=$(this).val().split('-')[0].replace(pattern1, '$1-$2-$3');
            enddate=$(this).val().split('-')[1].replace(pattern1, '$1-$2-$3');
            if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                obj.val('');
                return false;
            }
        }else if(len>=35&&(len-35)%18==0){
            var arr=$(this).val().split(',');
            arr.forEach(function(value,index,item){
                if(!pattern.test(item[index])){
                    obj.val('');
                    return false;
                }
                if(index==0){
                    startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                    enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                    if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                        obj.val('');
                        return false;
                    }
                }else{
                    if(new Date(enddate).getTime()>new Date(item[index].split('-')[0].replace(pattern1, '$1-$2-$3')).getTime()){
                        obj.val('');
                        return false;
                    }
                    startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                    enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                    if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                        obj.val('');
                        return false;
                    }
                }
            })
        }

    })
    //房型
    var roomNames=$("input[name=roomNames]").val();
   /* if(roomNames!=''&&roomNames!='all'){
        var rooms=roomNames.split(',');
        $("input[name=cbx_roomNames]").each(function(){
            var v=$(this);
            rooms.forEach(function(value,index,item){
                if(item[index]==v.val()){
                    v.prop("checked",true);
                }
            })
        })
    }else*/ if(roomNames=='all'){
        $("input[name=cbx_roomNames]").attr("checked",true);
        $("#hotelRoomNameDesc").attr("disabled",true);
       /* var namebox = $("input[name=cbx_roomNames]");
        for(var i = 0; i < namebox.length; i++) {
            namebox[i].checked=true;
        }*/
    }else{
        var hotelRoomsDesc='';
        var hotelRoomsContent='';
        $.each(getHotelRoomChoosedData,function(i,item){
            if (i <= 2) {
                hotelRoomsDesc += item.name+",";
            }
            if (i == 3) {
                hotelRoomsDesc = hotelRoomsDesc.substr(0, hotelRoomsDesc.length - 1) + '  ...';
            }
            if(i <= 9){
                hotelRoomsContent+=item.name +",";
            }
            if (i == 10) {
                hotelRoomsContent = hotelRoomsContent.substr(0, hotelRoomsContent.length - 1) + '  ...';
            }
        });
        if (getHotelRoomChoosedData.length > 0 ) {

            if (getHotelRoomChoosedData.length <= 3) {
                hotelRoomsDesc = hotelRoomsDesc.substr(0, hotelRoomsDesc.length - 1);
            }
            if(getHotelRoomChoosedData.length <= 10){
                hotelRoomsContent = hotelRoomsContent.substr(0, hotelRoomsContent.length - 1);
            }
        }
        $("#hotelRoomNameDesc").val(hotelRoomsDesc);
        $("#hotelRoomsContent").html(hotelRoomsContent);
    }
    $("input[name=cbx_roomNames]").click(function(){
        var check=$(this).is(":checked");
        $("#hotelRoomNameDesc").attr("disabled",check);
        if(check){
            getHotelRoomChoosedData = [];
            $("#hotelRoomNameDesc").val('');
            $("#hotelRoomsContent").html('');
        }

        /*if($("#basicOrigin").val()=="self"){
            var nn = $(this).is(":checked")
            var namebox = $("input[name=cbx_roomNames]");
            for(var i = 0; i < namebox.length; i++) {
                namebox[i].checked=false;
            }
            $(this).prop("checked",nn);
            return true;
        }
        if($(this).val()=="all"){
            var nn = $(this).is(":checked")
            var namebox = $("input[name=cbx_roomNames]");
            for(var i = 0; i < namebox.length; i++) {
                namebox[i].checked=(nn == true);
            }
        }else{
            if(!$(this).is(":checked")){
                $("input[name=cbx_roomNames][value=all]").prop("checked",false);
            }
        }*/
    })
    //早餐份数
    var breakfastCount=$("input[name=breakfastCount]").val();
    if(breakfastCount!=''&&breakfastCount!='all'){
        var breakfast=breakfastCount.split(',');
        $("input[name=cbx_breakfastCount]").each(function(){
            var v=$(this);
            breakfast.forEach(function(value,index,item){
                if(item[index]==v.val()){
                    v.prop("checked",true);
                }
            })
        })
    }else if(breakfastCount=='all'){
        var namebox = $("input[name=cbx_breakfastCount]");
        for(var i = 0; i < namebox.length; i++) {
            namebox[i].checked=true;
        }
    }
    $("input[name=cbx_breakfastCount]").click(function(){
        if($("#basicOrigin").val()=="self"){
            var nn = $(this).is(":checked")
            var namebox = $("input[name=cbx_breakfastCount]");
            for(var i = 0; i < namebox.length; i++) {
                namebox[i].checked=false;
            }
            $(this).prop("checked",nn);
            return;
        }
        if($(this).val()=="all"){
            var nn = $(this).is(":checked")
            var namebox = $("input[name=cbx_breakfastCount]");
            for(var i = 0; i < namebox.length; i++) {
                namebox[i].checked=(nn == true);
            }
        }else{
            if(!$(this).is(":checked")){
                $("input[name=cbx_breakfastCount][value=all]").prop("checked",false);
            }
        }
    })
    //固定金额
    var floatingamount=$("input[name=floatingAmount]").val();
    var salepricetype ="";
    if((floatingamount!=""&&floatingamount!=0)&&$("input[name=fixedAmount]").val()!=""){
        salepricetype="2";
        salePriceTypeDis(false);
        $("#input_floatingAmount").val(floatingamount);
    }else{
        salepricetype="1";
        $("input[name=floatingAmount]").val('');
        salePriceTypeDis(true);
    }
    $("#fixedAmount_salePriceType"+salepricetype).val($("input[name=fixedAmount]").val());
    $(":radio[name=salePriceType][value='" + salepricetype + "']").prop("checked", "checked");
    $("input[name=salePriceType]").on("change",function(){
        if(this.value=="1"){
            salePriceTypeDis(true);
        }else{
            salePriceTypeDis(false);
        }
    });

    $("input[name=selfCancelType]").on("change",function(){
        initCancelTypeInput();
    });
    $("input[name=cancelType]").on("change",function(){

        initCancelType();
    });

    $("input[name=limitCancelType]").on("change",function(){
        initCancelTypeInput();
    });
    //自有|标准取消规则
    if($("input[name=cancelType]:checked").val()==1){
        $(".self_cancel_type").show();
        if( $('input:radio[name=selfCancelType]:checked').val()==3){
            $(".cancel_rule").show();
        }
    }else{
        $(".self_cancel_type").hide();
        $(".cancel_rule").hide();
    }
    $("input[name=cancelType]").on("change",function(){
        if($(this).val()=="1"){
            $(".self_cancel_type").show();
            if(  $('input:radio[name=selfCancelType]:checked').val()==3){
                $(".cancel_rule").show();
            }
        }else{
            $(".self_cancel_type").hide();
            $(".cancel_rule").hide();
        }
    })
    $("input[name=selfCancelType]").on("change",function(){
        if($(this).val()=="3"){
            $(".cancel_rule").show();
        }else{
            $(".cancel_rule").hide();
        }
    })
    if($("input[name=selfCancelType]:checked").val()=="3"){
        var limitcanceltype=$("input[name=limitCancelType]:checked").val();
        var free_cancel_time=$("input[name=freeCancelTime]").val();
        var pay_cancel_time=$("input[name=payCancelTime]").val();
        var pay_cancel_type=$("input[name=payCancelType]").val();
        var cancel_pay_amount=$("input[name=cancelPayAmount]").val();
        if(limitcanceltype=="FreeAndCant"){
            $("#free_cancel_time_FreeAndCant").val(free_cancel_time);
        }else if(limitcanceltype=="PayAndCant"){
            $("#input_pay_cancel_time_PayAndCant").val(pay_cancel_time);
            $("#input_cancel_pay_amount_PayAndCant").val(cancel_pay_amount);
            $("#select_pay_cancel_type_PayAndCant").val(pay_cancel_type);
        }else if(limitcanceltype=="FreeAndPayAndCant"){
            $("#free_cancel_time_FreeAndPayAndCant").val(free_cancel_time);
            $("#input_pay_cancel_time_FreeAndPayAndCant").val(pay_cancel_time);
            $("#input_cancel_pay_amount_FreeAndPayAndCant").val(cancel_pay_amount);
            $("#select_pay_cancel_type_FreeAndPayAndCant").val(pay_cancel_type);
        }
    }
    if($("#select_pay_cancel_type_PayAndCant").val()=="1"){
        $("#div_pay_cancel_type_PayAndCant").hide();
    }else{
        $("#div_pay_cancel_type_PayAndCant").show();
        if($("#select_pay_cancel_type_PayAndCant").val()=="2"){
            $("#span_cancel_pay_amount_PayAndCant_2").hide();
            $("#span_cancel_pay_amount_PayAndCant_3").show();
        } else if($("#select_pay_cancel_type_PayAndCant").val()=="3") {
            $("#span_cancel_pay_amount_PayAndCant_2").show();
            $("#span_cancel_pay_amount_PayAndCant_3").hide();
        }
    }
    $("#select_pay_cancel_type_PayAndCant").on("change",function(){
        if($(this).val()=="1"){
            $("#div_pay_cancel_type_PayAndCant").hide();
        }else{
            $("#div_pay_cancel_type_PayAndCant").show();
            if($(this).val()=="2"){
                $("#span_cancel_pay_amount_PayAndCant_2").hide();
                $("#span_cancel_pay_amount_PayAndCant_3").show();
            } else if($(this).val()=="3") {
                $("#span_cancel_pay_amount_PayAndCant_2").show();
                $("#span_cancel_pay_amount_PayAndCant_3").hide();
            }
        }
        $("#input_cancel_pay_amount_PayAndCant").val('');
    })
    if($("#select_pay_cancel_type_FreeAndPayAndCant").val()=="1"){
        $("#div_pay_cancel_type_FreeAndPayAndCant").hide();
    }else{
        $("#div_pay_cancel_type_FreeAndPayAndCant").show();
        if($("#select_pay_cancel_type_FreeAndPayAndCant").val()=="2"){
            $("#span_cancel_pay_amount_FreeAndPayAndCant_2").hide();
            $("#span_cancel_pay_amount_FreeAndPayAndCant_3").show();
        } else if($("#select_pay_cancel_type_FreeAndPayAndCant").val()=="3") {
            $("#span_cancel_pay_amount_FreeAndPayAndCant_2").show();
            $("#span_cancel_pay_amount_FreeAndPayAndCant_3").hide();
        }
    }
    $("#select_pay_cancel_type_FreeAndPayAndCant").on("change",function(){
        if($(this).val()=="1"){
            $("#div_pay_cancel_type_FreeAndPayAndCant").hide();
        }else{
            $("#div_pay_cancel_type_FreeAndPayAndCant").show();
            if($(this).val()=="2"){
                $("#span_cancel_pay_amount_FreeAndPayAndCant_2").hide();
                $("#span_cancel_pay_amount_FreeAndPayAndCant_3").show();
            } else if($(this).val()=="3") {
                $("#span_cancel_pay_amount_FreeAndPayAndCant_2").show();
                $("#span_cancel_pay_amount_FreeAndPayAndCant_3").hide();
            }

        }
        $("#input_cancel_pay_amount_FreeAndPayAndCant").val('');
    })
}

function initSelectAndCheckContent() {
    $("input[name=cbx_breakfastCount]").prop("checked",false);
    $("input[name=cbx_roomNames][value='all']").prop("checked",false);
    $("#policydetailForm #checkIn").val('');
    $("#fixedAmount_salePriceType1").val('');
    $("#input_floatingAmount").val('');
    $("#fixedAmount_salePriceType2").val('');
    $("#policyDetailModel #checkIn").val('');
    $("#cbx_roomNames").val('');
    getHotelRoomChoosedData=[];
    $("#hotelRoomsContent").html('');
    $("#hotelRoomNameDesc").attr("disabled",false);
    $("#cbx_breakfastCount").val('');
    $("#payType").val('');
    $("#customerNumber").val('');
    $("input:radio[name=salePriceType][value='1']").prop('checked','checked');
    $("input:radio[name=cancelType][value='1']").prop('checked','checked');
    //$("input:radio[name=selfCancelType][value='1']").prop('checked','checked');
    $("input:radio[name=limitCancelType][value='FreeAndCant']").prop('checked','checked');
    initCancelType();
    $("input:radio[name=selfCancelType][value='1']").prop('checked','checked');
    salePriceTypeDis(true);
    if($("input[name=selfCancelType]").val()=="3"){
        $(".cancel_rule").show();
    }else{
        $(".cancel_rule").hide();
    }
    if($("input[name=cancelType]").val()=="1"){
        $(".self_cancel_type").show();
        if(  $('input:radio[name=selfCancelType]:checked').val()==3){
            $(".cancel_rule").show();
        }
    }else{
        $(".self_cancel_type").hide();
        $(".cancel_rule").hide();
    }
}

function initCancelType(){
    $('input:radio[name="selfCancelType"]').removeAttr('checked');
    $("input:radio[name=selfCancelType][value='1']").prop('checked','checked');
    $('input:radio[name=limitCancelType]').removeAttr('checked');
    $("input:radio[name=limitCancelType][value='1']").prop('checked','checked');
    initCancelTypeInput();
}
function initCancelTypeInput(){
    $("#free_cancel_time_FreeAndCant").val('');
    $("#input_pay_cancel_time_PayAndCant").val('');
    $("#input_cancel_pay_amount_PayAndCant").val('');
    $("#free_cancel_time_FreeAndPayAndCant").val('');
    $("#input_pay_cancel_time_FreeAndPayAndCant").val('');
    $("#input_pay_cancel_time_FreeAndPayAndCant").val('');
    $("#input_cancel_pay_amount_FreeAndPayAndCant").val('');
    $("#select_pay_cancel_type_PayAndCant").val('');
    $("#select_pay_cancel_type_FreeAndPayAndCant").val('');
}
//校验
function validateResult(){
    if($.trim($("#basicOrigin").find("option:selected").val())==''){
        errorMsg($("#basicOrigin"), '基准来源不能为空');
        return false;
    }
    if(!$("input:checkbox[name='cbx_roomNames']").is(":checked") && getHotelRoomChoosedData.length==0){
        errorMsg($("input:checkbox[name='cbx_roomNames']"), '房型不能为空');
        return false;
    }
    if(!$("input:checkbox[name='cbx_breakfastCount']").is(":checked")){
        errorMsg($("input:checkbox[name='cbx_breakfastCount']"), '早餐份数不能为空');
        return false;
    }

    if($("#payType").val()==''){
        errorMsg($("#payType"), '支付方式不能为空');
        return false;
    }
    if($("#customerNumber").val()==''){
        errorMsg($("#customerNumber"), '入住人数不能为空');
        return false;
    }

    return true;
}
//保存酒店政策细则
function submitPolicyDetail(){
    if(!validateResult()){
        return false;
    }
    var checkIn=$.trim($(".checkIn_input").val());
    var pattern=/^((((1[6-9]|[2-9]\d)\d{2})(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0[13456789]|1[012])(0[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})02(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0229))-((((1[6-9]|[2-9]\d)\d{2})(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0[13456789]|1[012])(0[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})02(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0229))$/;
    var pattern1=/(\d{4})(\d{2})(\d{2})/;
    if(checkIn!=''){
        var arr=checkIn.split(',');
        var flag=true;
        var startdate,enddate;
        arr.forEach(function(value,index,item){
            if(!pattern.test(item[index])){
                flag=false;
                return false;
            }
            if(index==0){
                startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                    flag=false;
                    return false;
                }
            }else{
                if(new Date(enddate).getTime()>new Date(item[index].split('-')[0].replace(pattern1, '$1-$2-$3')).getTime()){
                    flag=false;
                    return false;
                }
                startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                    flag=false;
                    return false;
                }
            }
        })
        if(!flag){
            alert("适用适用入住日期格式不正确，请重新输入");
            return;
        }
    }
    var free_cancel_time='',pay_cancel_time='0',pay_cancel_type='',cancel_pay_amount='';
    $("input[name=originName]").val($("#basicOrigin").find("option:selected").text());
    if($("input[name=cancelType]:checked").val()=="1"&&$("input[name=selfCancelType]:checked").val()=="3"){
        var limitcanceltype=$("input[name=limitCancelType]:checked").val();
        if(limitcanceltype==''){
            errorMsg($("input[name=limitCancelType]"),"请选择限时取消类型");
            $("input[name=limitCancelType]").focus();
            return false;
        }
        if(limitcanceltype=="FreeAndCant"){
            free_cancel_time=$("#free_cancel_time_FreeAndCant").val();
            if(free_cancel_time==""){
                errorMsg($("#free_cancel_time_FreeAndCant"),"请输入时间");
                $("#free_cancel_time_FreeAndCant").focus();
                return false;
            }
            if(!/^((0|[1-9]\d*))$/.test(free_cancel_time)){
                errorMsg($("#free_cancel_time_FreeAndCant"),"请输入数字");
                $("#free_cancel_time_FreeAndCant").focus();
                return false;
            }
        }else if(limitcanceltype=="PayAndCant"){
            pay_cancel_time=$("#input_pay_cancel_time_PayAndCant").val();
            cancel_pay_amount=$("#input_cancel_pay_amount_PayAndCant").val();
            pay_cancel_type=$("#select_pay_cancel_type_PayAndCant").val();
            if(pay_cancel_time==""){
                errorMsg($("#input_pay_cancel_time_PayAndCant"),"请输入时间");
                $("#input_pay_cancel_time_PayAndCant").focus();
                return false;
            }
            if(!/^((0|[1-9]\d*))$/.test(pay_cancel_time)){
                errorMsg($("#input_pay_cancel_time_PayAndCant"),"请输入数字");
                $("#input_pay_cancel_time_PayAndCant").focus();
                return false;
            }
            if(pay_cancel_type=="2"){
                if(cancel_pay_amount==""){
                    errorMsg($("#input_cancel_pay_amount_PayAndCant"),"请输入金额百分比");
                    $("#input_cancel_pay_amount_PayAndCant").focus();
                    return false;
                }
                var reg=/^(0|100|([1-9][0-9]?))(\.\d{0,4})?$/;
                if(!reg.test(cancel_pay_amount)){
                    errorMsg($("#input_cancel_pay_amount_PayAndCant"),"请输入一个数,0-100,最多四位小数");
                    $("#input_cancel_pay_amount_PayAndCant").focus();
                    return false;
                }
            }else if(pay_cancel_type=="3"){
                if(cancel_pay_amount==""){
                    errorMsg($("#input_pay_cancel_time_PayAndCant"),"请输入金额");
                    $("#input_cancel_pay_amount_PayAndCant").focus();
                    return false;
                }
                var reg=/^(0|[1-9]\d*)(\.\d{0,4})?$/;
                if(!reg.test(cancel_pay_amount)){
                    errorMsg($("#input_cancel_pay_amount_PayAndCant"),"请输入一个数,最多四位小数");
                    $("#input_cancel_pay_amount_PayAndCant").focus();
                    return false;
                }
            }

            if(pay_cancel_type==""){
                errorMsg($("#select_pay_cancel_type_PayAndCant"),"请输入类型");
                $("#select_pay_cancel_type_PayAndCant").focus();
                return false;
            }
        }else if(limitcanceltype=="FreeAndPayAndCant"){
            free_cancel_time=$("#free_cancel_time_FreeAndPayAndCant").val();
            pay_cancel_time=$("#input_pay_cancel_time_FreeAndPayAndCant").val();
            cancel_pay_amount=$("#input_cancel_pay_amount_FreeAndPayAndCant").val();
            pay_cancel_type=$("#select_pay_cancel_type_FreeAndPayAndCant").val();
            if(free_cancel_time==""){
                errorMsg($("#free_cancel_time_FreeAndPayAndCant"),"请输入时间");
                $("#free_cancel_time_FreeAndPayAndCant").focus();
                return false;
            }
            if(!/^((0|[1-9]\d*))$/.test(free_cancel_time)){
                errorMsg($("#free_cancel_time_FreeAndPayAndCant"),"请输入数字");
                $("#free_cancel_time_FreeAndPayAndCant").focus();
                return false;
            }
            if(pay_cancel_time==""){
                errorMsg($("#input_pay_cancel_time_FreeAndPayAndCant"),"请输入时间");
                $("#input_pay_cancel_time_FreeAndPayAndCant").focus();
                return false;
            }
            var reg=/^(0|[1-9]{1,2})$/;
            if(!reg.test(pay_cancel_time)){
                errorMsg($("#input_pay_cancel_time_FreeAndPayAndCant"),"请输入正整数");
                $("#input_pay_cancel_time_FreeAndPayAndCant").focus();
                return false;
            }

            if(pay_cancel_type=="2"){
                if(cancel_pay_amount==""){
                    errorMsg($("#input_cancel_pay_amount_FreeAndPayAndCant"),"请输入金额百分比");
                    $("#input_cancel_pay_amount_FreeAndPayAndCant").focus();
                    return false;
                }
                var reg=/^(0|100|([1-9][0-9]?))(\.\d{0,4})?$/;
                if(!reg.test(cancel_pay_amount)){
                    errorMsg($("#input_cancel_pay_amount_FreeAndPayAndCant"),"请输入一个数,0-100,最多四位小数");
                    $("#input_cancel_pay_amount_FreeAndPayAndCant").focus();
                    return false;
                }
            }else if(pay_cancel_type=="3"){
                if(cancel_pay_amount==""){
                    errorMsg($("#input_cancel_pay_amount_FreeAndPayAndCant"),"请输入金额");
                    $("#input_cancel_pay_amount_FreeAndPayAndCant").focus();
                    return false;
                }
                var reg=/^(0|[1-9]\d*)(\.\d{0,4})?$/;
                if(!reg.test(cancel_pay_amount)){
                    errorMsg($("#input_cancel_pay_amount_FreeAndPayAndCant"),"请输入一个数,最多四位小数");
                    $("#input_cancel_pay_amount_FreeAndPayAndCant").focus();
                    return false;
                }
            }
            if(pay_cancel_type==""){
                errorMsg($("#select_pay_cancel_type_FreeAndPayAndCant"),"请输入类型");
                $("#select_pay_cancel_type_FreeAndPayAndCant").focus();
                return false;
            }
            if(free_cancel_time<=pay_cancel_time){
                errorMsg($("#input_pay_cancel_time_FreeAndPayAndCant"),"前者时间需大于后者时间");
                return false;
            }
        }
    }
    $("input[name=freeCancelTime]").val(free_cancel_time);
    $("input[name=payCancelTime]").val(pay_cancel_time);
    $("input[name=payCancelType]").val(pay_cancel_type);
    $("input[name=cancelPayAmount]").val(cancel_pay_amount);
    var salepricetype=$("input[name=salePriceType]:checked").val();
    if(salepricetype=="1"){
        $("input[name=fixedAmount]").val($("#fixedAmount_salePriceType1").val());
        $("input[name=floatingAmount]").val(0);
        if($("input[name=fixedAmount]").val()==''){
            errorMsg($("#fixedAmount_salePriceType1"),"请输入金额");
            $("#fixedAmount_salePriceType1").focus();
            return false;
        }
        var reg = /^(-?[1-9]\d{0,3}|0)(\.\d{1,2})?$/;
        if (!reg.test($("input[name=fixedAmount]").val()))
        {
            errorMsg($("#fixedAmount_salePriceType1"),"输入一个数，最多2位小数，最大数据9999.99");
            $("#fixedAmount_salePriceType1").focus();
            return false;
        }
    }else{
        $("input[name=fixedAmount]").val($("#fixedAmount_salePriceType2").val());

        if($("#input_floatingAmount").val()==""){
            errorMsg($("#input_floatingAmount"),"请输入金额");
            $("#input_floatingAmount").focus();
            return false;
        }

        var reg = /^((([1-9][0-9]?)(\.\d{1,4})?)|100(\.0{0,4})?|(0\.\d{1,4}))$/;
        if (!reg.test($("input[name=input_floatingAmount]").val()))
        {
            errorMsg($("#input_floatingAmount"),"请输入一个正数,最大100,最多四位小数");
            $("#input_floatingAmount").focus();
            return false;
        }

        $("input[name=floatingAmount]").val($("#input_floatingAmount").val());

        if($("input[name=fixedAmount]").val()==''){
            $("input[name=fixedAmount]").val(0);
        }
        var reg = /^(-?[1-9]\d{0,3}|0)(\.\d{1,2})?$/;
        var price2 = $("input[name=fixedAmount]").val().replace("-","");
        if (!reg.test(price2))
        {
            errorMsg($("#fixedAmount_salePriceType2"),"输入一个数，最多2位小数，最大数据9999.99");
            $("#fixedAmount_salePriceType2").focus();
            return false;
        }
    }

    var roomNames='' ;
    if($("input:checkbox[name='cbx_roomNames']").is(":checked")){
        roomNames = "all";
    }else{
        if(getHotelRoomChoosedData.length>0){
            $.each(getHotelRoomChoosedData,function(index,item){
                roomNames+=item.name+",";
            });
            roomNames = roomNames.substr(0, roomNames.length - 1);
        }
    }
    var breakfasts = $("input:checkbox[name='cbx_breakfastCount']:checked").map(function(index,elem) {
        return $(elem).val();
    }).get().join(',');
    $("input[name=roomNames]").val(roomNames);
    $("input[name=breakfastCount]").val(breakfasts);
    $.ajax({
        cache : true,
        type : "POST",
        url : "hotel/policy/savePolicyDetail",
        data : $('#policydetailForm').serialize(),
        async : false,
        error : function(request) {
            showErrorMsg("请求失败，请刷新重试");
        },
        success : function(data) {
            if(data.type=='success'){
                $.alert(data.txt,"提示");
                $('#policyDetailModel').modal('hide');
                reloadPolicyDetailList();
            }else{
                $.alert(data.txt,"提示");
            }
        }
    });
}

