<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<div class="modal fade" id="policyDetailModel" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <c:choose>
                    <c:when test="${empty hotelPolicyDetailDto.id}">
                        <h4 class="modal-title">新增细则</h4>
                    </c:when>
                    <c:otherwise>
                        <h4 class="modal-title">修改细则</h4>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="modal-body form-horizontal bv-form">
                <form:form modelAttribute="hotelPolicyDetailDto" class="form-horizontal" id="policydetailForm" role="form" action="hotel/policy/savePolicyDetail">
                    <form:hidden path="id"/>
                    <form:hidden path="hotelPolicyId"/>
                    <form:hidden path="payCancelType"/>
                    <form:hidden path="freeCancelTime"/>
                    <form:hidden path="payCancelTime"/>
                    <form:hidden path="cancelPayAmount"/>
                    <form:hidden path="originName"/>
                    <form:hidden path="roomNames"/>
                    <form:hidden path="breakfastCount"/>
                    <form:hidden path="fixedAmount"/>
                    <form:hidden path="floatingAmount"/>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">基准来源*</label>
                        <div class="col-sm-5 ">
                            <form:select path="basicOrigin"  cssClass="form-control input-sm">
                                <form:options items="${origins }" itemLabel="hubsName" itemValue="hubsCode" itemText="hubsName"/>
                            </form:select>
                        </div>
                        <div class="col-sm-4"></div>
                    </div>


                   <div class="form-group">
                       <label class="col-sm-3 control-label">适用入住日期</label>
                       <div class="col-sm-5">
                           <form:textarea path="checkIn"  placeholder=' 每个日期区间按照【起始日期】+【"-"】+【截止日期】构成，多个日期区间以英文","分隔 示例:20180214-20180222,20181001-20181007' maxlength="200" class="form-control input-sm checkIn_input" style="resize: none;" ></form:textarea>
                       </div>
                       <div class="col-sm-4"></div>
                   </div>
                   <div class="form-group">
                       <label class="col-sm-3 control-label">适用房型</label>
                       <div class="col-sm-9 div-rooms">
                           <label class="col-sm-2 checkbox-inline">
                               <input type="checkbox" name="cbx_roomNames" value="all" >全部
                           </label>
                           <div class="col-sm-3">
                            <input type="text" class="form-control" id="hotelRoomNameDesc" value="${roomNames}" readonly onclick="selHotelRoomData()" style="with:100%" >
                           </div>
                           <span class="col-sm-7" id="hotelRoomsContent"></span>
                          <%-- <c:choose>
                               <c:when test="${ empty rooms or fn:contains(rooms,'all' )}">
                                   <label class="checkbox-inline">
                                       <input type="checkbox" name="cbx_roomNames" value="all" checked >全部
                                   </label>
                               </c:when>
                               <c:otherwise>
                                   <label class="checkbox-inline">
                                       <input type="checkbox" name="cbx_roomNames" value="all" >全部
                                   </label>
                                   <c:forEach items="${fn:split(rooms,',')}" var="bean" varStatus="status">
                                       <label class="checkbox-inline">
                                           <input type="checkbox" name="cbx_roomNames" value="${bean}" >${bean}
                                       </label>
                                   </c:forEach>
                               </c:otherwise>
                           </c:choose>--%>
                       </div>
                   </div>
                   <div class="form-group">
                       <label class="col-sm-3 control-label">早餐份数</label>
                       <div class="col-sm-9">
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="all"/>不限
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="0"/>无早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="1"/>单早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="2"/>双早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="3"/>三早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="4"/>四早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="5"/>五早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="6"/>六早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="7"/>七早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="8"/>八早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="9"/>九早
                           </label>
                           <label class="checkbox-inline">
                               <input type="checkbox" name="cbx_breakfastCount" value="10"/>十早
                           </label>
                       </div>
                   </div>
                   <div class="form-group">
                       <label class="col-sm-3 control-label">支付方式</label>
                       <div class="col-sm-3">
                           <form:select path="payType" cssClass="form-control input-sm" style="width:50%">
                               <form:option value="">请选择</form:option>
                               <form:option value="1">预付</form:option>
                               <form:option value="2">现付</form:option>
                           </form:select>
                       </div>
                       <label class="col-sm-1 control-label">入住人数</label>
                       <div class="col-sm-3">
                           <form:select path="customerNumber" cssClass="form-control input-sm" style="width:50%">
                               <form:option value="">请选择</form:option>
                               <form:option value="-1">不限</form:option>
                               <form:option value="1">1</form:option>
                               <form:option value="2">2</form:option>
                               <form:option value="3">3</form:option>
                               <form:option value="4">4</form:option>
                               <form:option value="5">5</form:option>
                               <form:option value="6">6</form:option>
                               <form:option value="7">7</form:option>
                               <form:option value="8">8</form:option>
                               <form:option value="9">9</form:option>
                               <form:option value="10">10</form:option>
                           </form:select>
                       </div>
                   </div>

                   <div class="form-group">
                       <label class="col-sm-3 control-label">销售价格*</label>
                       <div class="col-md-offset-3">
                           <div class="col-sm-2">
                               <label class="radio-inline"><form:radiobutton path="salePriceType" value="1"   checked="true" />固定金额</label>

                           </div>
                           <div class="col-sm-2">
                               <input type="text" id="fixedAmount_salePriceType1" name="input_salePriceType1" class="form-control input-sm"/>
                           </div>
                           <div class="col-sm-8 form-inline " id="divsaleprice">
                                   <div class="col-sm-3">
                                       <label class="radio-inline"><form:radiobutton path="salePriceType" value="2" />浮动金额</label>
                                   </div>
                                   <div class="col-sm-9 form-inline">
                                       基准价格*<input type="text" id="input_floatingAmount"  name="input_floatingAmount" class="form-control input-sm" style="width:80px" />
                                      <%-- <form:input path="floatingAmount" class="form-control input-sm" style="width:80px" ></form:input>--%>
                                       +<input type="text" id="fixedAmount_salePriceType2"  name="input_salePriceType2" class="form-control input-sm" style="width:80px" />元
                                   </div>
                           </div>
                        </div>

                   </div>
                   <div class="form-group ">
                       <label class="col-sm-3 control-label">取消规则*</label>
                       <div class="col-sm-5">
                           <label class="radio-inline"><form:radiobutton path="cancelType" value="1" checked="true" />自有取消规则</label>
                           <label  class="radio-inline"><span id="span_cancelType"><form:radiobutton path="cancelType" value="2" />标准取消规则</span></label>

                       </div>
                        </div>
                   <div class="form-group self_cancel_type" >
                        <div class="col-md-offset-3  col-sm-5">
                            <label class="radio-inline"><form:radiobutton path="selfCancelType" value="1"  checked="true"/>免费取消</label>
                            <label class="radio-inline"><form:radiobutton path="selfCancelType" value="2" />不可取消</label>
                            <label class="radio-inline"><form:radiobutton path="selfCancelType" value="3" />限时取消</label>
                        </div>
                  </div>
                    <div class="form-group cancel_rule">
                    <div class="col-md-offset-3  col-sm-9 form-inline">
                        <form:radiobutton path="limitCancelType" value="FreeAndCant" checked="true" />
                            距到店当日24点前
                            <input type="text" class="form-control input-sm" id="free_cancel_time_FreeAndCant" name="free_cancel_time_FreeAndCant_name" style="width:80px"/>
                            小时以上可以免费变更取消
                    </div>

                    </div>
                    <div class="form-group cancel_rule">
                    <div class="col-md-offset-3  col-sm-9 form-inline">
                        <form:radiobutton path="limitCancelType" value="PayAndCant" />
                            距到店当日24点前
                        <input type="text" class="form-control input-sm" id="input_pay_cancel_time_PayAndCant" name="input_pay_cancel_time_PayAndCant_name" style="width:80px"/>
                            小时以上需扣除
                            <select id="select_pay_cancel_type_PayAndCant" class="form-control input-sm">
                                <option value="">请选择</option>
                                <option value="1">首晚房费</option>
                                <option value="2">订单金额</option>
                                <option value="3">固定金额</option>
                            </select>
                            <span id="div_pay_cancel_type_PayAndCant">
                                <input id="input_cancel_pay_amount_PayAndCant" name="input_cancel_pay_amount_PayAndCant_name" class="form-control input-sm" style="width:80px"><span id="span_cancel_pay_amount_PayAndCant_2">元</span><span id="span_cancel_pay_amount_PayAndCant_3">%</span></span>，之后不能变更取消
                    </div>
                    </div>
                    <div class="form-group cancel_rule">
                    <div class="col-md-offset-3  col-sm-9 form-inline">
                         <form:radiobutton path="limitCancelType" value="FreeAndPayAndCant"/>
                            距到店当日24点前
                            <input type="text" class="form-control input-sm" id="free_cancel_time_FreeAndPayAndCant" name="free_cancel_time_FreeAndPayAndCant" style="width:80px"/>
                                小时以上可以免费变更取消， 距到店当日24点前 <input type="text" class="form-control input-sm" id="input_pay_cancel_time_FreeAndPayAndCant" name="input_pay_cancel_time_FreeAndPayAndCant_name" style="width:80px"/>小时以上需扣除
                                <select id="select_pay_cancel_type_FreeAndPayAndCant" class="form-control input-sm">
                                    <option value="">请选择</option>
                                    <option value="1">首晚房费</option>
                                    <option value="2">订单金额</option>
                                    <option value="3">固定金额</option>
                                </select>
                        <span id="div_pay_cancel_type_FreeAndPayAndCant">
                            <input id="input_cancel_pay_amount_FreeAndPayAndCant" name="input_cancel_pay_amount_FreeAndPayAndCant_name" class="form-control input-sm" style="width:80px"><span id="span_cancel_pay_amount_FreeAndPayAndCant_2">元</span><span id="span_cancel_pay_amount_FreeAndPayAndCant_3">%</span></span>，之后不能变更取消
                    </div>
                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="submitPolicyDetail()">保存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<script>
    getHotelRoomChoosedData = JSON.parse('${rooms}' || '[]');
</script>
