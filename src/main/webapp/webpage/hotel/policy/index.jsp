<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="ruleMain">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>酒店价格政策库
            </div>
            <div id="rightDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- 查询条件 -->
                                        <form class="form-inline" >
                                            <nav class="text-right">
                                                <div class="form-group pull-left">
                                                    <button type="button" class="btn btn-primary" onclick="getPolicy()">新增酒店政策</button>
                                                </div>
                                                <div class=" form-group">
                                                    <label class="control-label">关键字搜索</label>
                                                    <input  id="keyWords"  type="text"  class="form-control" placeholder="政策名称/酒店名称" maxlength="50" style="width: 250px"/>
                                                </div>
                                                <div class=" form-group">
                                                    <label class="control-label">状态</label>
                                                    <select class="form-control" id="status">
                                                        <option value = "">全部</option>
                                                        <option value = "1">启用</option>
                                                        <option value = "0">停用</option>
                                                    </select>
                                                </div>
                                                <div class=" form-group">
                                                    <label class="control-label">政策有效期</label>
                                                    <select class="form-control" id="validity" >
                                                        <option value="">全部</option>
                                                        <option value = "1">即将过期政策</option>
                                                        <option value = "0">已经过期政策</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary" onclick="reloadPolicy()">搜索</button>
                                                    <button type="button" class="btn btn-default" onclick="resetSelect()">清空条件</button>
                                                </div>

                                            </nav>
                                        </form>
                                    </div>
                                </div>
                                <div  id="rule_Table" style="margin-top: 15px">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 详细页面 -->
<div id="ruleDetail" style="display:none;"></div>
<script type="text/javascript" src="webpage/hotel/policy/policy.js?version=${globalVersion}"></script>
