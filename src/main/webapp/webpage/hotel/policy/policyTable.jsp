<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover" style="table-layout:fixed" id="ruleTable">
    <thead>
    <tr>
        <th width="10%"><label>优先级</label></th>
        <th width="20%"><label>政策名称</label></th>
        <th width="30%"><label>涉及酒店/品牌</label></th>
        <th width="20%"><label>适用入住日期</label></th>
        <th width="10%"><label>状态</label></th>
        <th width="10%"><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list}" var="bean">
        <tr <c:if test="${bean.lessCurrentDate}">style="background-color: #aaaaaa"</c:if>>
            <td>${bean.level}</td>
            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${bean.name}</td>
            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${bean.hotelOrBlandNames==""?"适用所有酒店":bean.hotelOrBlandNames}</td>
            <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${bean.checkIn}</td>
            <td>
                <c:choose>
                <c:when test="${bean.status==1}">启用</c:when>
                <c:otherwise>停用</c:otherwise>
                </c:choose>
            </td>
            <td class="text-center" >
                <a onclick="getPolicy(${bean.id})" >修改</a>
                <a onclick="deletePolicy(${bean.id})" >删除</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadPolicy"></jsp:include>
</div>
