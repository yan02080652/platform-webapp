<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="form-group" id="div_search">
    <div class="col-sm-offset-2 col-sm-6">
        <div class="col-sm-2">
            <label class="control-label">酒店名称</label>
        </div>
        <div class="col-sm-6">
            <input type="text" class="form-control" id="hotelNameSearch" value="${hotelName}" placeholder="酒店名称">
        </div>
        <div class="col-sm-4">
            <a class="btn btn-primary" onclick="hotelSearch()">查询</a>
            <a class="btn btn-default" onclick="resetHotelSearch()">重置</a>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
        <table class="table table-striped table-bordered table-hover" id="hoteltable">
            <thead>
            <tr>
                <th><input type="checkbox" id="allboxs" onclick="allcheck()"></th>
                <th><label>酒店名称</label></th>
                <th><label>操作</label></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${pageList.list}" var="bean">
                <tr>
                    <td><input type="checkbox" name="cb-hotel" data-hotel="${bean.id}"></td>
                    <td>${bean.hotelName}</td>
                    <td><a onclick="deleteHotel(${bean.id})">删除</a></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
        <div class="pagin">
            <jsp:include page="../../common/pagination_ajax.jsp?callback=loadPolicyHotelList"></jsp:include>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
        <div class="col-sm-2">
            <a class="btn btn-primary" onclick="selHotelData()">+酒店</a>
        </div>
        <div class="col-sm-2">
            <a class="btn btn-default" onclick="batchHotelDeletes()">批量删除</a>
        </div>
    </div>
</div>
