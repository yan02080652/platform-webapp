<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<div id="ruleMain">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>价格计划列表 &nbsp;&nbsp;
            </div>
            <div id="rightDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <!-- 查询条件 -->
                                            <form class="form-inline" method="post">
                                                <nav class="text-left">
                                                    <div class=" form-group">
                                                        <label class="control-label">${condition.hotelName} &nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                        <input type="text" class="form-control partner" name="cName" value="${cName}" placeholder="请选择企业名称" readonly/>
                                                    </div>
                                                    <div class="form-group">
                                                        <lebel>&nbsp;&nbsp;&nbsp;&nbsp;</lebel>
                                                        <select id="planType" class="form-control">
                                                            <option value="">所有计划</option>
                                                            <option value="1">开启</option>
                                                            <option value="0">关闭</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <lebel>&nbsp;&nbsp;&nbsp;&nbsp;</lebel>
                                                        <select id="payType" class="form-control">
                                                            <option value="">所有支付方式</option>
                                                            <option value="SelfPay">现付</option>
                                                            <option value="Prepay">预付</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        &nbsp;&nbsp;&nbsp;
                                                        <button type="button" class="btn btn-default btn-primary" onclick="reloadList(1)">搜索</button>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <button type="button" class="btn btn-default btn-primary"  onclick="addRatePlan()">+新增价格计划
                                                        </button>
                                                    </div>
                                                    <div class="form-group">
                                                     	<input type="hidden" id="showLabelTemp" name="showLabelTemp"  />
                                                     	<input type="hidden" id="isFlash" name="isFlash"  />
                                                    	<button type="button" class="btn btn-default btn-primary"  onclick="saveShowLabel()">设置标签名称
                                                        </button>
                                                    </div>
                                                </nav>
                                            </form>
                                        </div>


                                    </div>
                                </div>
                                <div id="rule_Table" style="margin-top: 15px">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="${baseDes}/webpage/hotel/new_protocol/ratePlan.js"></script>
<script>
queryData.hotelId = '${condition.id}';
queryData.cId = '${condition.CId}';
queryData.originType = '${condition.originType}';
</script>
