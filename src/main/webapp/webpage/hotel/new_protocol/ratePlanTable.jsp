<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover" id="ruleTable">
    <thead>
    <tr>
        <th><label>计划名称</label></th>
        <th><label>标签名称</label></th>
        <th><label>适用房型 </label></th>
        <th><label>支付方式</label></th>
        <th><label>适用时间</label></th>
        <th><label>状态</label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="bean">
        <tr class="odd gradeX">
            <td>${bean.name }</td>
            <td>${bean.showLabel }</td>
            <td>${bean.roomName }</td>
            <td>
                <c:if test="${bean.paymentType eq 'All'}">全部</c:if>
                <c:if test="${bean.paymentType eq 'SelfPay'}">现付</c:if>
                <c:if test="${bean.paymentType eq 'Prepay'}">预付</c:if>
            </td>
            <td>${bean.suitableDate }</td>
            <td>${bean.status ? '开启' : '关闭' }</td>
            <td class="text-center">
                <a class="detail" onclick="addRatePlan(${bean.id})">编辑</a>
                <a class="changeStatus" onclick="changeStatus(${bean.id},${!bean.status},${bean.canOpen})">${ bean.status ? '关闭' : '开启'}</a>
                <c:if test="${!bean.status}">
                    <a class="delete text-danger" onclick="delRatePlan(${bean.id})">删除</a>
                </c:if>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadList"></jsp:include>
</div>
<script type="text/javascript">
$(function(){
	$("#showLabelTemp").val("${showLabelTemp}");
	$("#isFlash").val("${isFlash}");
})

</script>