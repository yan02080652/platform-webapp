<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" href="resource/plugins/city/cityselect.css?version=${globalVersion}">
<div id="ruleMain">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>协议酒店政策 &nbsp;&nbsp;
            </div>
            <div id="rightDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <!-- 查询条件 -->
                                            <div class="form-inline" >
                                                <nav class="text-left">
                                                    <div class=" form-group">
                                                        <label class="control-label">企业:</label>
                                                        <input type="text" class="form-control partner" name="cName" value="" placeholder="请选择企业" readonly autocomplete="off"/>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">城市:</label>
                                                        <%--<input class="form-control" id="cityId" placeholder="请输入目的地">--%>
                                                        <input type="text" class="cityinput form-control" autocomplete="off" style="width: 178px;padding-left: 10px;" id="citySelect" placeholder="请输入目的地">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">酒店名称:</label>
                                                        <input class="form-control" placeholder="酒店名称" id="hotelName" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-default btn-primary"
                                                                onclick="reloadList(1)">搜索
                                                        </button>
                                                        <button type="button" class="btn btn-default"
                                                                onclick="reset_search()">清空条件
                                                        </button>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div id="rule_Table" style="margin-top: 15px">
                                    <jsp:include page="protocol_table.jsp" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="add_protocol"></div>

<script type="text/javascript" src="resource/plugins/city/cityselect.js?version=${globalVersion}"></script>
<script src="${baseDes}/webpage/hotel/new_protocol/protocol.js"></script>