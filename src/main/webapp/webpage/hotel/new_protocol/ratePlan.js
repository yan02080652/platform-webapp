var queryData = {};
$(function(){
    reloadList(1);

    $(".partner").click(function () {
        TR.select('partner', {
            type: 0
        }, function (result) {
            $("[name='cName']").val(result.name);
            queryData.cId = result.id;
            reloadList(1);
        });
    });
})

//改变状态
function changeStatus(id,status,canOpen){
    if(status && !canOpen){
        layer.msg("适用日期时间已过,计划不可开启!",{icon: 5})
        return;
    }
    var msg = status? "启用":"禁用";
    layer.confirm("确认"+msg+"价格计划?",function(index){
        $.ajax({
            url: '/hotel/newProtocol/saveRatePlan',
            type: 'post',
            data: {
                id: id,
                status: status
            },success: function(data){
                layer.close()
                if(data.success){
                    layer.msg(msg+ "成功",{icon: 1})
                    reloadList(1);
                }else{
                    layer.msg(msg + "失败:" + data.msg,{icon: 2})
                }
            },error: function(){
                layer.close()
                layer.msg('系统异常',{icon: 2})
            }
        })
    })
}

//删除价格计划
function delRatePlan(id){
    layer.confirm("确认删除价格计划?",function(index){
        $.ajax({
            url: '/hotel/newProtocol/delRatePlan',
            type: 'post',
            data: {
                id: id
            },success: function(data){
                layer.close()
                if(data.success){
                    layer.msg("删除成功",{icon: 1})
                    reloadList(1)
                }else{
                    layer.msg("删除失败:" + data.msg,{icon: 2})
                }
            },error: function(){
                layer.close()
                layer.msg('系统异常',{icon: 2})
            }
        })
    })
}

//加载价格计划
function reloadList(pageIndex){
    if(queryData.cId == ''){
        return;
    }
    queryData.pageIndex = pageIndex;
    queryData.status = $('#planType').val();
    queryData.paymentType = $('#payType').val();
    $.ajax({
        url: '/hotel/newProtocol/ratePlanList',
        data: queryData,
        success: function(data){
            $('#rule_Table').html(data);
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })
}

//新增价格计划
function addRatePlan(id){
    if(queryData.cId == ''){
        layer.msg("请先选择企业",{icon: 5})
        return;
    }
    if($("#showLabelTemp").val() == null||$("#showLabelTemp").val() == ''){
    	layer.msg("请先设置标签名称",{icon: 5})
        return;
    }
    queryData.id = id;
    var content;
    $.ajax({
        url: '/hotel/newProtocol/editRatePlan',
        type: 'post',
        async: false,
        data: queryData,
        success: function(data){
            content = data;
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })

    var lindex = layer.open({
        title: '价格计划信息',
        type: 1,
        area: ['1200px', '550px'],
        content: content,
        btn: ['保存','取消'],
        yes: function (index) {
            if(!validate()){
                return;
            }
            var showLabelTemp = $("#showLabelTemp").val();
            $("#showLabelOne").val(showLabelTemp);
            
            $.ajax({
                url: '/hotel/newProtocol/saveRatePlan',
                type: 'post',
                async: false,
                data: $('#ratePlanForm').serialize(),
                success: function(data){
                    if(data.success){
                        layer.close(lindex)
                        layer.msg("保存成功",{icon: 1});
                        reloadList(1);
                    }else{
                        layer.msg("保存失败:" + data.msg,{icon: 2});
                    }
                },error: function(){
                    layer.msg("系统异常",{icon: 2})
                }
            })
        },
        btn2: function () {
            layer.close();
        }
    })
}

function validate(){
    //房型必选
    if($('[name=roomId]:checked').length == 0){
        layer.msg("请选择房型")
        return false;
    }
    if($('#name').val() == ''){
        layer.msg("价格计划名称必填")
        return false;
    }
    if($('#code').val() == ''){
        layer.msg("协议代码必填")
        return false;
    }
    if($('[name=paymentType]:checked').length == 0){
        layer.msg("请选择支付方式")
        return false;
    }
    if($('[name=cancelRule]:checked').val().startWith('其他规则') && $('#cancel').val() == ''){
        layer.msg("请描述取消规则")
        $('#cancel').focus();
        return false;
    }
    if($('#price').val() == ''){
        layer.msg("价格必填")
        $('#price').focus()
        return false;
    }
    var reg=/^[1-9]{1}\d*(\.\d{1,2})?$/;
    if(!reg.test($('#price').val())){
        layer.msg("价格格式错误")
        $('#price').focus()
        return false;
    }
    if($('#suitableDate').val().trim() == ''){
        layer.msg("适用日期必填")
        return false;
    }
    //TODO 适用日期校验
    var suitableDate = $('#suitableDate').val().trim();
    if(suitableDate.length<21){
        layer.msg("适用日期格式错误")
        return false;
    }
    while(suitableDate.length >= 21){
        var date = suitableDate.substring(0,21);
        reg=/[\d]{4}[\/][\d]{2}[\/][\d]{2}[-][\d]{4}[\/][\d]{1,2}[\/][\d]{2}/g;
        if(!reg.test(date)){
            layer.msg("适用日期格式错误")
            return false;
        }

        suitableDate = suitableDate.substring(21,suitableDate.length).trim();
        if(suitableDate.length > 0 && suitableDate.length<21){
            layer.msg("适用日期格式错误");
            return false;
        }
    }
    if($('[name=weekSet]:checked').length == 0){
        layer.msg("周有效必选")
        return false;
    }

    var roomName = "";
    for(var i = 0;i< $('[name=roomId]:checked').length;i++){
        var roomId = $('[name=roomId]:checked').eq(i).val();
        roomName += $('#roomId_'+roomId).val() + ",";
    }
    roomName = roomName.substring(0,roomName.length-1);
    $('#roomName').val(roomName);
    return true;
}

function saveShowLabel(){
	
	if(queryData.cId == ''){
	   layer.msg("请先选择企业",{icon: 5})
	   return;
    }
	
	var content;
    $.ajax({
        url: '/hotel/newProtocol/editShowLabel',
        type: 'post',
        async: false,
        data: queryData,
        success: function(data){
            content = data;
        },error: function(){
            layer.msg("系统异常",{icon: 2})
        }
    })

    var lindex = layer.open({
        title: '标签名称',
        type: 1,
        area: ['200px', '132px'],
        content: content,
        btn: ['保存','取消'],
        yes: function (index) {
            var showLabel =$("#showLabel").val();
       	    if(showLabel==null || showLabel ==''){
       		  layer.msg("标签名称必填");
       		  return false;
       	    }
       	    if(showLabel.length >4 ){
       	      layer.msg("标签名称长度不能超过4个字符");
       	      return false;
       	    }
        	queryData.showLabel = showLabel;
      	    $.ajax({
      	        url: '/hotel/newProtocol/saveShowLabel',
      	        data: queryData,
      	        success: function(data){
      	        	$('#showLabelTemp').attr("value",showLabel);
      	        	layer.msg("保存、更新成功");
      	        	layer.close(lindex);
      	        	var isFlash = $('#isFlash').val();
      	        	if(isFlash == '1'){
      	        		reloadList(1);
      	        	}
      	        },error: function(){
      	            layer.msg("系统异常",{icon: 2})
      	        }
      	    })
        },
        btn2: function () {
            layer.close();
        }
    })
	    
}

