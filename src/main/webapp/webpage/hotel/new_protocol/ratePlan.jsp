<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<form:form cssClass="form-horizontal" id="ratePlanForm" action="/hotel/newProtocol/saveRatePlan" modelAttribute="ratePlan">
    <form:hidden path="id" />
    <form:hidden path="cId" />
    <form:hidden path="hotelId" />
    <form:hidden path="roomName" />
    <form:hidden path="originType" />
    <div class="col-md-2" >
        <h5>第一步: 选择适用房型</h5>
        <div class="form-group">
            <div class="col-sm-10 checkbox-inline col-sm-offset-2">
                <div class="col-sm-12">
                	<input id="showLabelOne" type="hidden" name="showLabel"  />
                    <input type="checkbox" onclick="selectAllRoom(this.checked)"/>全选
                </div>
                <c:forEach items="${rooms}" var="bean" varStatus="status">
                    <div class="col-sm-12">
                        <input type="hidden" id="roomId_${bean.originType}-${bean.roomId}" value="${bean.name}">
                        <form:checkbox path="roomId" label="${bean.name}" value="${bean.originType}-${bean.roomId}" data="${bean.name}"/>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    <div class="col-md-5 form-horizontal " >
        <h5>第二步: 填写价格计划信息</h5>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">名称:</label>
            <div class="col-sm-8">
                <form:input path="name" cssClass="form-control" placeholder="规则名称" maxlength="50"/>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">协议代码:</label>
            <div class="col-sm-8">
                <form:input path="code" cssClass="form-control" placeholder="协议代码" maxlength="20"/>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">早餐份数:</label>
            <div class="col-sm-8">
                <form:select path="breakfastCount" cssClass="form-control">
                    <form:option value="0">无</form:option>
                    <form:option value="1">1份</form:option>
                    <form:option value="2">2份</form:option>
                    <form:option value="3">3份</form:option>
                    <form:option value="4">4份</form:option>
                    <form:option value="5">5份</form:option>
                    <form:option value="6">6份</form:option>
                    <form:option value="7">7份</form:option>
                    <form:option value="8">8份</form:option>
                    <form:option value="9">9份</form:option>
                </form:select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">其它服务:</label>
            <div class="col-sm-8">
                <form:input path="otherService" cssClass="form-control" placeholder="其它服务" maxlength="100"/>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">支付方式:</label>
            <div class="radio-inline col-sm-8">
                <div class="col-sm-6">
                    <form:radiobutton path="paymentType" checked="true" value="SelfPay" label="现付"></form:radiobutton>
                </div>
                <div class="col-sm-6">
                    <form:radiobutton path="paymentType" value="Prepay" label="预付"></form:radiobutton>
                </div>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">入住人数:</label>
            <div class="col-sm-8">
                <form:select path="maxCount" cssClass="form-control">
                    <form:option value="1">1</form:option>
                    <form:option value="2">2</form:option>
                    <form:option value="3">3</form:option>
                    <form:option value="4">4</form:option>
                    <form:option value="5">5</form:option>
                    <form:option value="6">6</form:option>
                    <form:option value="7">7</form:option>
                    <form:option value="8">8</form:option>
                    <form:option value="9">9</form:option>
                </form:select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">取消规则:</label>
            <div class="radio-inline col-sm-8">
                <div class="col-sm-6">
                    <form:radiobutton path="cancelRule" value="免费取消" label="免费取消" checked="true"></form:radiobutton>
                </div>
                <div class="col-sm-6">
                    <form:radiobutton path="cancelRule" value="不可取消" label="不可取消"></form:radiobutton>
                </div>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <div class="radio-inline col-sm-offset-4 col-sm-8">
                <div class="col-sm-6 ">
                    <form:radiobutton path="cancelRule" value="其他规则" label="其他规则"></form:radiobutton>
                </div>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <div class="col-sm-8 col-sm-offset-4">
                <form:input path="cancelRule" cssClass="form-control" id="cancel" placeholder="" maxlength="200"/>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">状态:</label>
            <div class="radio-inline col-sm-8">
                <div class="col-sm-6">
                    <form:radiobutton path="status" value="1" label="开启" checked="true"/>
                </div>
                <div class="col-sm-6">
                    <form:radiobutton path="status" value="0" label="关闭"/>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <h5>第三步: 填写价格计划金额</h5>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">价格(元):</label>
            <div class="col-sm-8">
                <form:input path="price" cssClass="form-control" placeholder="0.00" maxlength="10"/>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label class="control-label col-sm-4">适用日期:</label>
            <div class="col-sm-8">
                <form:textarea path="suitableDate" cssClass="form-control" rows="4" placeholder="时间段录入格式：  2018/05/21-2022/07/21   多时间段录入请换行"/>
            </div>
        </div>

        <div class="form-group col-sm-12">
            <div class="col-sm-8 checkbox-inline col-sm-offset-4">
                <div class="col-sm-3">
                    <form:checkbox path="weekSet" value="0" label="全选" checked="true"/>
                </div>
                <div class="col-sm-3">
                    <form:checkbox path="weekSet" value="1" label="一" checked="true"/>
                </div>
                <div class="col-sm-3">
                    <form:checkbox path="weekSet" value="2" label="二" checked="true"/>
                </div>
                <div class="col-sm-3">
                    <form:checkbox path="weekSet" value="3" label="三" checked="true"/>
                </div>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <div class="col-sm-8 checkbox-inline col-sm-offset-4">
                <div class="col-sm-3">
                    <form:checkbox path="weekSet" value="4" label="四" checked="true"/>
                </div>
                <div class="col-sm-3">
                    <form:checkbox path="weekSet" value="5" label="五" checked="true"/>
                </div>
                <div class="col-sm-3">
                    <form:checkbox path="weekSet" value="6" label="六" checked="true"/>
                </div>
                <div class="col-sm-3">
                    <form:checkbox path="weekSet" value="7" label="日" checked="true"/>
                </div>
            </div>
        </div>
    </div>
</form:form>

<script>
    $('[name=weekSet]').change(function(){
        var value = $(this).val();
        var check = $(this).is(":checked")
        if(value == 0){
            for(var i = 1;i< $('[name=weekSet]').length;i++){
                $('[name=weekSet]')[i].checked = check;
            }
        }else if(check == false){
            $('[name=weekSet]')[0].checked = false;
        }
    })

    $('[type=radio][name=cancelRule]').change(function(){
        var value = $(this).val();
        if(value == '免费取消' || value == '不可取消'){
            $('#cancel').val('')
            $('#cancel').hide();
        }else{
            $('#cancel').show();
        }
    })

    var cancelRule = '${ratePlan.cancelRule}';
    if(cancelRule == '' || cancelRule == '免费取消' || cancelRule == '不可取消'){
        $('#cancel').val('')
        $('#cancel').hide('')
    }else {
        $('#cancelRule3').attr("checked",true);
    }

    var roomId = '${ratePlan.roomId}';
    if(roomId != ''){
        for(var i = 0;i<roomId.split(",").length;i++){
            $('[name=roomId][value='+roomId.split(",")[i]+']').attr('checked',true);
        }
    }
    var roomName = '${ratePlan.roomName}';
    if(roomName != ''){
        for(var i = 0;i<roomName.split(",").length;i++){
            $('[name=roomId][data="'+roomName.split(",")[i]+'"]').attr('checked',true);
        }
    }

    var weekSet = '${ratePlan.weekSet}';
    if(weekSet != ''){
        for(var i = 0;i< $('[name=weekSet]').length;i++){
            if(weekSet.indexOf($('[name=weekSet]')[i].value) == -1){
                $('[name=weekSet]').eq(i).attr('checked',false);
            }
        }
    }

    var paymentType = '${ratePlan.paymentType}';
    if(paymentType != 'Prepay'){
        $('#cancelRule2').attr('disabled',true);
        $('#cancelRule3').attr('disabled',true);
    }else{
        $('#cancelRule2').attr('disabled',false);
        $('#cancelRule3').attr('disabled',false);
    }

    $('input[name=paymentType]').change(function(){
        if($(this).val() == 'SelfPay'){
            $('#cancelRule1').click();
            $('#cancelRule2').attr('disabled',true);
            $('#cancelRule3').attr('disabled',true);
        }else{
            $('#cancelRule2').attr('disabled',false);
            $('#cancelRule3').attr('disabled',false);
        }
    })

    function selectAllRoom(val){
        var rooms = $('[name=roomId]');
        for(var i =0 ;i<rooms.length;i++){
            rooms.eq(i).attr("checked",val);
        }
    }
</script>