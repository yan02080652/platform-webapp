<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<div class="modal fade" id="mode_addProtocol" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="overflow-y:auto">
    <div class="modal-dialog" style="width:60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                <h4 class="modal-title">选择添加的协议酒店</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-6">
                            <input id="search_name" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <div class="btn btn-info" onclick="searchHotel(1);">搜索</div>
                        </div>
                    </div>

                    <div id="hotel_list">

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function searchHotel(pageIndex) {
        var name = $('#search_name').val();
        if(name == ''){
            return;
        }
        var index = layer.load();
        $.ajax({
            url: '/hotel/newProtocol/searchHotel',
            type: 'post',
//            async: false,
            data: {
                hotelName: name,
                cityId: $('#citySelect').attr("cityId"),
                pageIndex: pageIndex
            },success: function(data){
                layer.close(index);
                $('#hotel_list').html(data);
            },error: function(){
                layer.close(index);
                layer.msg("系统异常",{icon: 2})
            }
        })
    }
</script>