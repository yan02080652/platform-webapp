<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover" id="ruleTable">
    <thead>
    <tr>
        <th><label>城市</label></th>
        <th><label>酒店名称 </label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="bean">
        <tr class="odd gradeX">
            <td>${bean.cityName }</td>
            <td>${bean.hotelName }</td>
            <td class="text-center">
                <a class="detail" hotelId="${bean.hotelId}" hotelName="${bean.hotelName}" originType="${bean.originType}">查看</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <button type="button" class="pull-left btn btn-default btn-primary" onclick="addProtocol()">新增协议酒店</button>
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadList"></jsp:include>
</div>
<script type="text/javascript">
    //跳转价格计划列表页
    $('.detail').click(function(){
        location.href = '/hotel/newProtocol/ratePlanIndex?id='+$(this).attr('hotelId')+"&cId="+queryData.cId + "&hotelName="+$(this).attr('hotelName') + "&originType="+$(this).attr('originType')+"&cName=" + $('[name=cName]').val()
    })
</script>