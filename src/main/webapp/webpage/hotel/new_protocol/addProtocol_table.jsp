<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<table class="table table-striped table-bordered table-hover" id="">
    <thead>
    <tr>
        <th><label>&nbsp;  </label></th>
        <th><label>酒店名称 </label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="bean">
        <tr class="odd gradeX">
            <td>
                <input type="radio" name="hotelId" class="hotel_id" hotelId="${bean.hotelId}" hotelName="${bean.name}" originType="${bean.originType}">
            </td>
            <td>${bean.name }</td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=searchHotel"></jsp:include>
</div>
<div class="form-group">
    <label class="btn btn-info col-sm-offset-5 col-sm-2" onclick="submit()" style="margin-top:10px;">下一步</label>
</div>
<script type="text/javascript">
    function submit(){
        if($('[name=hotelId]:checked').length == 0){
            layer.msg("请选择一条数据")
            return;
        }
        var hotelId = $('[name=hotelId]:checked').attr("hotelId")
        var hotelName = $('[name=hotelId]:checked').attr("hotelName")
        var originType = $('[name=hotelId]:checked').attr("originType")

        location.href = '/hotel/newProtocol/ratePlanIndex?id='+hotelId+"&cId="+queryData.cId + "&hotelName="+hotelName + "&originType="+originType+"&cName=" + $('[name=cName]').val()

    }
</script>