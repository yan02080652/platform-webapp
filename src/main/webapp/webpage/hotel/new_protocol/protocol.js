var queryData = {
    cId : ''
};
$(function () {

    new Vcity.CitySelector({input:'citySelect'});

    $(".partner").click(function () {
        TR.select('partner', {
            type: 0
        }, function (result) {
            $("[name='cName']").val(result.name);
            queryData.cId = result.id;
        });
    });

    reloadList(1);
})

//加载列表
function reloadList(pageIndex) {
    queryData.cityId = $('#citySelect').attr("cityId");
    queryData.hotelName = $('#hotelName').val() || '';
    queryData.pageIndex = pageIndex;
    $.ajax({
        url: '/hotel/newProtocol/list',
        type: 'post',
        data: queryData,
        success: function (data) {
            $('#rule_Table').html(data);
        }, error: function () {
            layer.msg("系统异常", {icon: 2});
        }
    })

}

//重置条件
function reset_search() {
    window.location.reload(true);
    //queryData = {
    //    cId : ''
    //};
    //$('#cityId').val('');
    //$('#citySelect').val('');
    //$('#hotelName').val('')
    //$("[name='cName']").val('');
    //reloadList(1);
}

//新增协议酒店
function addProtocol(){
    $('.add_protocol').load('/hotel/newProtocol/addProtocol',function(){
        $('#mode_addProtocol').modal();
    })

}
