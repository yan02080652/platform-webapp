<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<table class="table table-striped table-bordered table-hover"  style="table-layout:fixed" id="ruleTable">
	<thead>
		<tr>
	      <th width="5%"><label>规则编码</label></th>
	      <th width="20%"><label>规则范围</label></th>
	      <th width="20%"><label>规则说明</label></th>
	      <th width="30%"><label>涉及品牌/酒店</label></th>
		  <th width="10%"><label>状态</label></th>
		  <th width="15%"><label>操作</label></th>
        </tr>
    </thead>
     <tbody>
        <c:forEach items="${pageList.list }" var="bean">
         <tr class="odd gradeX">
        	 <input type="hidden" value="${bean.id}"/>
        	 <td>${bean.code }</td>
        	 <td>${bean.partnerName }</td>
             <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${bean.declaration }</td>
			 <td style="overflow:hidden;white-space:nowrap;text-overflow:ellipsis;">${bean.hotelOrBrandNames}</td>
			 <td>${bean.status ? '启用' : '停用'}</td>
             <td class="text-center" >
				 <c:if test="${bean.status}">
					 <a onclick="updateRule('${bean.id }', false)" >停用</a> |
				 </c:if>
				 <c:if test="${!bean.status}">
					 <a onclick="updateRule('${bean.id }', true)" >启用</a> |
				 </c:if>
				  <a onclick="getRule('${bean.id }', true);">复制</a> |
				  <a onclick="getRule('${bean.id}', false)" >修改</a> |
				  <a onclick="deleteRule('${bean.id }');">删除</a>
             </td>
         </tr>
       </c:forEach>
    </tbody>
</table>
<div class="pagin">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadOrderInterceptRule"></jsp:include>
</div>
<script type="text/javascript">
//checkbox全选居中，固定宽度
$("table tr").each(function(){
	$(this).find(":first").attr("style","text-align: center;width:150px");
})
</script>