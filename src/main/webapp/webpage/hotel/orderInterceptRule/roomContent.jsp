<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<c:choose>
	<c:when test="${ empty rooms or fn:contains(rooms,'all' )}">
		<label class="checkbox-inline">
			<input type="checkbox" name="cbx_roomNames" value="all" >全部
		</label>
	</c:when>
	<c:otherwise>
		<label class="checkbox-inline"> <input type="checkbox" name="cbx_roomNames" value="all">全部
		</label>
		<c:forEach items="${fn:split(rooms,',')}" var="bean" varStatus="status">
			<label class="checkbox-inline"> <input type="checkbox"
				name="cbx_roomNames" value="${bean}">${bean}
			</label>
		</c:forEach>
	</c:otherwise>
</c:choose>
<script>
$(function(){
	//房型
    var roomNames=$("input[name=roomNames]").val();
    if(roomNames!=''&&roomNames!='all'){
        var rooms=roomNames.split(',');
        $("input[name=cbx_roomNames]").each(function(){
            var v=$(this);
            rooms.forEach(function(value,index,item){
                if(item[index]==v.val()){
                    v.prop("checked",true);
                }
            })
        })
    }else if(roomNames=='all'){
        var namebox = $("input[name=cbx_roomNames]");
        for(var i = 0; i < namebox.length; i++) {
            namebox[i].checked=true;
        }
    }
    $("input[name=cbx_roomNames]").click(function(){
        if($(this).val()=="all"){
            var nn = $(this).is(":checked")
            var namebox = $("input[name=cbx_roomNames]");
            for(var i = 0; i < namebox.length; i++) {
                namebox[i].checked=(nn == true);
            }
        }else{
            if(!$(this).is(":checked")){
                $("input[name=cbx_roomNames][value=all]").prop("checked",false);
            }else{
            	 var namebox = $("input[name=cbx_roomNames]");
            	 var result=0;
                 for(var i = 0; i < namebox.length; i++) {
                   	 if(i==0){
                   		result++;
                   	 }else{
                   		 if(namebox[i].checked){
                   			result++; 
                   		 }
                   	 }
                 }
                 if(result==namebox.length){
                	 $("input[name=cbx_roomNames][value=all]").prop("checked",true);
                 }
            }
            
        }
    })
    
    
})
</script>