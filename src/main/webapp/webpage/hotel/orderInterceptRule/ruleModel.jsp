<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<style type="text/css">
	body{ font-size:14px; line-height:24px;}
	.form-group{
		padding-bottom: 15px;
		padding-top: 15px;
	}
	.move_left{
		position: relative;
		right: 160px;
	}

	.control-label .reqMark{
	     color: red; 
   		 font-weight: bold;
   		 font-size: 15px;
	}
</style>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="panel">
			<div class="panel-heading">
				<span class="glyphicon glyphicon-home" aria-hidden="true"></span>酒店拦截规则设置
 				 
 				
				<div class="panel-body">
				<div class="form-inline col-sm-10">
				 <ul id="myTab" class="nav nav-tabs">
                    <li>
                        <a href="javascript:void(0);" id="#baseTab">
                            基础信息
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" id="#hotelTab">
                            酒店选择
                        </a>
                    </li>
                    
                </ul>
				</div>
				<div id="mainContent">
					
				</div>
				</div>
			</div>

		</div>
	</div>
</div>
<script type="text/javascript">
var interceptId='${id}';
var isClone=false;
$(function(){
	if('${isClone}'=='true'){
		isClone=true;
	}
	$.post('hotel/orderInterceptRule/getBasic', {
		interceptId : interceptId,
		isClone : isClone
	}, function(data) {
		$('#mainContent').html(data);
		$("#myTab li:eq(0)").addClass("active");
		$("#myTab li:eq(1)").removeClass("active");
		if(isClone){
		   interceptId='';
		}
	})
})
var tag="baseTab";
$("#myTab a").click(function(){
	if($(this).attr("id")=="#baseTab"&&tag=="hotelTab"){
			$.post('hotel/orderInterceptRule/getBasic', {
				interceptId : interceptId,
				isClone : isClone
			}, function(data) {
				$('#mainContent').html(data);
				$("#myTab li:eq(0)").addClass("active");
				$("#myTab li:eq(1)").removeClass("active");
				tag="baseTab";
			})
		
	}else if($(this).attr("id")=="#hotelTab"&&tag=="baseTab"){
		if(interceptId!=''&&isClone!=true){
			$.post('hotel/orderInterceptRule/getHotelMsg', {
				interceptId : interceptId,
				isClone : isClone
			}, function(data) {
				$('#mainContent').html(data);
				$("#myTab li:eq(1)").addClass("active");
				$("#myTab li:eq(0)").removeClass("active");
				tag="hotelTab";
			})
		}else{
			$.alert("请保存基础信息","提示");
		}
	}
})

</script>