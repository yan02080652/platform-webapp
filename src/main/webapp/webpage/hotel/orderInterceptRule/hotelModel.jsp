<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<style type="text/css">
	body{ font-size:14px; line-height:24px;}
	.form-group{
		padding-bottom: 15px;
		padding-top: 15px;
	}
	.move_left{
		position: relative;
		right: 160px;
	}

	.control-label .reqMark{
	     color: red; 
   		 font-weight: bold;
   		 font-size: 15px;
	}
</style>
<form:form  action="hotel/orderInterceptRule/saveHotel" cssClass="form-horizontal bv-form"
								method="post" modelAttribute="ruleDto" id="ruleForm">
						<form:hidden path="creatorId"/>
						<form:hidden path="updaterId"/>
						<form:hidden path="roomNames"/>
						<form:hidden path="isIncludeWeek"/>
						<form:hidden path="isIncludeWork"/>
						<form:hidden path="isLimitOrigin"/>
						<div class="form-inline col-sm-10" style="margin-top: 20px;">
							<label  class="control-label col-sm-2">酒店星级:</label>
							<div class="col-sm-2">
								<label class="checkbox-inline">
									<input id="id" name="id"  class="form-control"  style="width: 100%;" value="${ruleDto.id}"  type="hidden" />
									<input type="checkbox" id="isLimitStar" name="isLimitStar" ${not empty ruleDto.isLimitStar and ruleDto.isLimitStar ? 'checked' : ''}  />所有星级
								</label>
							</div>
							<div class="col-sm-8">
								<c:forEach items="${starRateTypes}" var="s">
									<label class="checkbox-inline">
										<input type="checkbox" name="stars" ${not empty ruleDto.isLimitStar and ruleDto.isLimitStar ? 'checked' : '' } value="${s.star}">${s.desc}
									</label>
								</c:forEach>
							</div>
						</div>
						<div class="form-inline col-sm-10" style="margin-top: 20px;">
								<label  class="control-label col-sm-2">酒店品牌:</label>
								<div class="col-sm-8">
                                <input type="hidden" name="brandId"  id="hotelBrandId" value="${ruleDto.brandId}">
                                <input class="form-control" id="brandsDesc" value="${ruleDto.brandsDesc}"  readonly onclick="selHotelBrandData()" style="width:100%;"/>
                           		</div>
						</div>
						<div class="form-inline col-sm-10" style="margin-top: 20px;margin-bottom: 20px;">			
							<label  class="control-label col-sm-2">适用酒店:</label>
								<div class="col-sm-2">
								<label class="radio-inline"><input type="radio" id="applyType" name="applyType" value="1" <c:if test="${ruleDto.applyType==1}">checked</c:if> />适用所有酒店</label>
								</div>
								<div class="col-sm-2">
                                <label class="radio-inline"><input type="radio" name="applyType" value="2" <c:if test="${ruleDto.applyType==2}">checked</c:if> />适用以下酒店</label>
								</div>
								<div class="col-sm-2">
								 <label class="radio-inline"><input type="radio" name="applyType" value="3" <c:if test="${ruleDto.applyType==3}">checked</c:if> />不适用以下酒店</label>
								</div>
						</div>
						<div class="form-horizontal"  >
	                        <div  id="policyHotel" >
	                        </div>
                        </div>
                        <div class="form-inline col-sm-10" style="margin-top: 20px;">
								<label  class="control-label col-sm-2">客服提示:</label>
								<div class="col-sm-8">
									<textarea class="form-control" name="tip"  style="width:100%;">${ruleDto.tip}</textarea>
								</div>
			
						</div>
						<div class="form-inline col-sm-10"  style="margin-top: 20px;">
								<label class="col-sm-2 control-label"><span class="reqMark">*</span>适用房型:</label>
								<div class="col-sm-8">
									 <label class="checkbox-inline">
											<input type="checkbox" name="cbx_roomNames" value="all" >全部
									  </label>
									 <input type="text" class="form-control" id="hotelRoomNameDesc" value="${roomNames}" readonly onclick="selHotelRoomData()" style="with:100%" >
								</div>
						</div>
						<div class="form-inline col-sm-10"  style="margin-top: 20px;">
								<label class="col-sm-2 control-label"></label>
								 <span class="col-sm-8" id="hotelRoomsContent"></span>
						</div>
						<div class="col-sm-12 col-sm-offset-3" style="margin-top: 20px;">
							<div class="col-sm-2">
								<button type="button" class="btn btn-primary" onclick="submitRule()">保存</button>
							</div>
						
							<div class="col-sm-2">
								<button type="button" class="btn btn-primary" onclick="backRuleMain()">返回</button>
							</div>
						</div>
</form:form>
<script>

var getHotelChoosedData =  JSON.parse('${hotelList}' || '[]');
var getHotelBrandChoosedData = JSON.parse('${brandList}' || '[]');
var getHotelRoomChoosedData = JSON.parse('${rooms}' || '[]');


//设置星级选中
if ('${starsJSONString}') {
    var starsJSONString = '${starsJSONString}';
    var starsCheckbox = $("#isLimitStar").parents('.form-inline').find(':checkbox');
    for (var i = 1; i < starsCheckbox.length; i++) {
        if (starsJSONString.match(new RegExp(starsCheckbox[i].value, "gm"))) {
            $(starsCheckbox[i]).prop('checked', true);
            continue;
		}
	}
}

$("#isLimitStar").click(function () {
	if ($(this).is(':checked')) {
        $(this).val(true);
	} else {
        $(this).val(false);
	}
});

// 订单提交校验
function validateResult() {
	var result = false;
    $("#isLimitStar").parents('.form-inline').find(":checkbox").each(function (i, item) {
		   if ($(this).is(":checked")) {
            result |= true;
		   }
    });
    var applyType = $("input[name='applyType']:checked").val();
    //如果没有选择酒店星级
    if (!result) {
    	if(applyType==null){
    		 errorMsg($("#applyType"), '酒店星级未选择条件下，适用酒店按钮必选！');
    		 return false;
    	}else if(applyType!=1){
    		if($("#hotelNum").val()==0){
    		 errorMsg($("#applyType"), '需选择具体酒店数据！');
    		 return false;
    		}
    	}
	    }
    //如果选择适用酒店则需要选择酒店数据
    if(applyType!=null&&applyType!=1){
    	if($("#hotelNum").val()==0){
   		 errorMsg($("#addHotel"), '需选择具体酒店数据！');
   		 return false;
   		}
    }
    if(!$("input:checkbox[name='cbx_roomNames']").is(":checked") && getHotelRoomChoosedData.length==0){
        errorMsg($("input:checkbox[name='cbx_roomNames']"), '房型不能为空');
        return false;
    }
    var roomNames='' ;
    if($("input:checkbox[name='cbx_roomNames']").is(":checked")){
        roomNames = "all";
    }else{
        if(getHotelRoomChoosedData.length>0){
            $.each(getHotelRoomChoosedData,function(index,item){
                roomNames+=item.name+",";
            });
            roomNames = roomNames.substr(0, roomNames.length - 1);
        }
    }
    $("input[name=roomNames]").val(roomNames);
    return true;
}
function submitRule(){
   if(!validateResult()) {
    return;
   } 
    $.ajax({
        cache : true,
        type : "POST",
        url : "hotel/orderInterceptRule/saveHotel",
        data : $('#ruleForm').serialize(),
        async : false,
        error : function(request) {
            showErrorMsg("请求失败，请刷新重试");
        },
        success : function(data) {
            if (data.type == 'success') {
            	
                showSuccessMsg(data.txt);
                $("#ruleMain").show();
                $("#ruleDetail").hide().html("");
                reset_rule();
            } else {
                $.alert(data.txt,"提示");
            }
        }
    });
}



//点击全部,选中或者去掉子选项
$("input[name^='isLimit']").click(function () {
    if ($(this).is(":checked")) {
        $(this).parents('.form-inline').find(':checkbox').prop('checked', true);
        $(this).val(true);
    } else {
        $(this).parents('.form-inline').find(':checkbox').prop('checked', false);
        $(this).val(false);
    }
});

//是否去掉全部
$(":checkbox:not([name^='isLimit'])").click(function () {
    if ($(this).prop('name') == 'isIncludeWeek'||$(this).prop('name') == 'isIncludeWork') { //特殊
        return;
	}
    if (!$(this).is(":checked")) {
        $(this).parents('.form-inline').find(':checkbox:eq(0)').prop('checked', false);
        $(this).parents('.form-inline').find(':checkbox:eq(0)').val(false);
    }
});



//是否勾选上全部
$(":checkbox:not([name^='isLimit'])").click(function () {
    if ($(this).is(":checked")) {
        var result = 0;
        var labels = $(this).parent().siblings();
        for (var i = 0 ; i < labels.length; i++) {
            if (!$(labels[i]).find(":checkbox").is(':checked')) {
                break;
            }
            result ++;
        }
        if (result == labels.length) {
            $(this).parents('.form-inline').find(':checkbox:eq(0)').prop('checked', true);
            $(this).parents('.form-inline').find(':checkbox:eq(0)').val(true);
        }
    }
});




/***************拦截规则******************/
$(function(){
initControl();
})
function  initControl(){
applyTypeChange();
var applyType = $("input[name='applyType']:checked").val();
if(applyType!=1&&applyType!=null){
	loadIntercpetHotelList(1);
}
}
/***************酒店******************/

var getHotelBrandChoosedData = [];
var getHotelChoosedData = [];
//酒店品牌
function selHotelBrandData() {
	getHotelBrandChoosedData = [];
	new choose.hotelBrand({
		id : 'selBrand',//给id可以防止一直新建对象
		multi : false,
		empty : true,
		getChoosedData : function() {
			return getHotelBrandChoosedData;
		}
	}, function(data) {
		var brandsDesc = '';
		getHotelBrandChoosedData = [];
		if (data) {
			$(data).each(
					function(i, item) {
						getHotelBrandChoosedData.push({
							id : item.id,
							shortName : item.shortName
						});
						if (i <= 2) {
							brandsDesc += item.shortName + ",";
						}
						if (i == 3) {
							brandsDesc = brandsDesc.substr(0,
									brandsDesc.length - 1)
									+ '  ...';
						}
					});
			if (getHotelBrandChoosedData.length <= 3) {
				brandsDesc = brandsDesc.substr(0, brandsDesc.length - 1);
			}
		}
		$('#brandsDesc').val(brandsDesc);
		if (getHotelBrandChoosedData.length > 0) {
			$('#hotelBrandId').val(getHotelBrandChoosedData[0].id);
		} else {
			$('#hotelBrandId').val("");
		}
		var applyType = $("input[name='applyType']:checked").val();
		if(applyType!=1&&applyType!=null){
			loadIntercpetHotelList(1);
		}
		clearRoom();
	});
};
//酒店
function selHotelData() {
	getHotelChoosedData = [];
	new choose.hotelByBrand($("#hotelBrandId").val(), {
		//id:'selMultiHotel',//给id可以防止一直新建对象
		multi : true,
		empty : true,
		getChoosedData : function() {
			return getHotelChoosedData;
		}
	}, function(data) {
		console.log("sss", data);
		var brandsDesc = '';
		getHotelChoosedData = [];
		if (data) {
			$(data).each(function(i, item) {
				if (getHotelChoosedData.indexOf(item) == -1)
					getHotelChoosedData.push({
						hotelId : item.id,
						name : item.name
					});

			});
			//新增酒店
			addHotels();
		}
	});
};

//酒店房型
function selHotelRoomData() {
	var  applyType=1;
	if($("input[name='applyType']:checked").val()!=undefined){
		applyType=$("input[name='applyType']:checked").val();
	}
    new choose.hotelRooms($("#hotelBrandId").val(),applyType,interceptId,{
        //id:'selMultiHotel',//给id可以防止一直新建对象
        multi:true,
        empty:true,
        getChoosedData:function () {
            return getHotelRoomChoosedData;
        }
    },function (data) {
        var hotelRoomsDesc = '';
        var hotelRoomsContent='';
        getHotelRoomChoosedData=[];
        if (data) {
            $(data).each(function (i, item) {
                getHotelRoomChoosedData.push({name: item.name});
                if (i <= 2) {
                    hotelRoomsDesc += item.name+",";
                }
                if (i == 3) {
                    hotelRoomsDesc = hotelRoomsDesc.substr(0, hotelRoomsDesc.length - 1) + '  ...';
                }
                if(i <= 9){
                    hotelRoomsContent+=item.name +",";
                }
                if (i == 10) {
                    hotelRoomsContent = hotelRoomsContent.substr(0, hotelRoomsContent.length - 1) + '  ...';
                }
            });
            if (getHotelRoomChoosedData.length > 0 ) {

                if (getHotelRoomChoosedData.length <= 3) {
                    hotelRoomsDesc = hotelRoomsDesc.substr(0, hotelRoomsDesc.length - 1);
                }
                if(getHotelRoomChoosedData.length <= 10){
                    hotelRoomsContent = hotelRoomsContent.substr(0, hotelRoomsContent.length - 1);
                }
            }
            $("#hotelRoomNameDesc").val(hotelRoomsDesc);
            $("#hotelRoomsContent").html(hotelRoomsContent);
        }
    });
};
//房型
var roomNames=$("input[name=roomNames]").val();
if(roomNames=='all'){
    $("input[name=cbx_roomNames]").attr("checked",true);
    $("#hotelRoomNameDesc").attr("disabled",true);
}else{
    var hotelRoomsDesc='';
    var hotelRoomsContent='';
    $.each(getHotelRoomChoosedData,function(i,item){
        if (i <= 2) {
            hotelRoomsDesc += item.name+",";
        }
        if (i == 3) {
            hotelRoomsDesc = hotelRoomsDesc.substr(0, hotelRoomsDesc.length - 1) + '  ...';
        }
        if(i <= 9){
            hotelRoomsContent+=item.name +",";
        }
        if (i == 10) {
            hotelRoomsContent = hotelRoomsContent.substr(0, hotelRoomsContent.length - 1) + '  ...';
        }
    });
    if (getHotelRoomChoosedData.length > 0 ) {

        if (getHotelRoomChoosedData.length <= 3) {
            hotelRoomsDesc = hotelRoomsDesc.substr(0, hotelRoomsDesc.length - 1);
        }
        if(getHotelRoomChoosedData.length <= 10){
            hotelRoomsContent = hotelRoomsContent.substr(0, hotelRoomsContent.length - 1);
        }
    }
    $("#hotelRoomNameDesc").val(hotelRoomsDesc);
    $("#hotelRoomsContent").html(hotelRoomsContent);
}
$("input[name=cbx_roomNames]").click(function(){
    var check=$(this).is(":checked");
    $("#hotelRoomNameDesc").attr("disabled",check);
    if(check){
        getHotelRoomChoosedData = [];
        $("#hotelRoomNameDesc").val('');
        $("#hotelRoomsContent").html('');
    }
})
function clearRoom(){
	 getHotelRoomChoosedData = [];
     $("#hotelRoomNameDesc").val('');
     $("#hotelRoomsContent").html('');
}
//新增酒店
function addHotels() {
	if (getHotelChoosedData.length == 0) {
		$.alert("请选择酒店", "提示");
		return;
	}
	$.post('hotel/orderInterceptRule/addHotels', {
		interceptId : interceptId,
		hotels : JSON.stringify(getHotelChoosedData),
		brandId : $("#hotelBrandId").val()
	}, function(data) {
		if (data.type == 'success') {
			loadIntercpetHotelList(1);
			clearRoom();
		} else {
			$.alert(data.txt, "提示");
		}
	})
}
//删除酒店
function batchHotelDeletes() {
	var ids = [];
	var checks = $("#hoteltable input[name='cb-hotel']:checked");
	checks.each(function() {
		if (ids.indexOf($(this).attr('data-hotel')) == -1)
			ids.push($(this).attr('data-hotel'));
	})
	deleteHotels(ids);
}
function deleteHotel(id) {
	var ids = [];
	ids.push(id);
	deleteHotels(ids);
}
function deleteHotels(ids) {
	if (ids instanceof Array) {
		if (ids.length == 0) {
			$.alert("请选择酒店数据", "提示");
			return;
		}
		$.confirm({
			title : '提示',
			confirmButton : '确认',
			cancelButton : '取消',
			content : "确认删除所选记录?",
			confirm : function() {
				$.ajax({
					cache : true,
					type : "POST",
					url : "hotel/orderInterceptRule/deleteHotels",
					data : {
						interceptId : interceptId,
						ids : JSON.stringify(ids)
					},
					async : false,
					error : function(request) {
						showErrorMsg("请求失败，请刷新重试");
					},
					success : function(data) {
						$.alert(data.txt, "提示");
						if (data.type == 'success') {
							loadIntercpetHotelList(1);
							clearRoom();
						}
					}
				});
			},
			cancel : function() {
			}
		});
	}
}
//适用酒店类型切换
function applyTypeChange() {
	$("input[type=radio][name='applyType']").change(function() {
		if (this.value == 1) {
			$('#policyHotel').empty();
		} else {
			loadIntercpetHotelList(1);
		}
		clearRoom();
	})
}
//加载拦截适用酒店
function loadIntercpetHotelList(pageIndex) {
	$.post('hotel/orderInterceptRule/interceptHotelList', {
		interceptId : interceptId,
		brandId : $('#hotelBrandId').val(),
		hotelName : $('#hotelNameSearch').val(),
		pageIndex : pageIndex
	}, function(data) {
		$('#policyHotel').html(data);
		initHotel();
	})
}
//查询
function hotelSearch() {
	loadIntercpetHotelList();
}
//重置
function resetHotelSearch() {
	$("#hotelNameSearch").val('');
	loadIntercpetHotelList();
}
//全选
function allcheck() {
	var nn = $("#allboxs").is(":checked");
	var namebox = $("#hoteltable input[name='cb-hotel']");
	for (var i = 0; i < namebox.length; i++) {
		namebox[i].checked = (nn == true);
	}
}
function initHotel() {
	$("#hoteltable input[name='cb-hotel']").click(function() {
		if (!$(this).is(":checked")) {
			$("#allboxs").prop("checked", false);
		}
	})
}
</script>