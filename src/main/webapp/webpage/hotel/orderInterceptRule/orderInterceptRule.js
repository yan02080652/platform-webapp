reloadOrderInterceptRule();

function search_rule(){
    reloadOrderInterceptRule();
}
function reset_rule(){
    $("#keyWords").val("");
    $('#partnerIds').val("");
    $('#partnerNames').val("");
    reloadOrderInterceptRule();
}

function errorMsg(ele, msg) {
    layer.tips(msg, ele, {
        tips: [1, '#3595CC'],
        time: 4000
    });
};
function reloadOrderInterceptRule(pageIndex){
    var loadingList = layer.load();
    var keyWords = $('#keyWords').val();
    var partnerIds = $('#partnerIds').val();
    $.post('hotel/orderInterceptRule/list',{
    	keyWords:keyWords,
        pageIndex:pageIndex,
        partnerIds:partnerIds
    },function(data){
        layer.close(loadingList);
        $("#rule_Table").html(data);
    });
}

//启用
function updateRule(id, ruleStatus) {
    $.post('hotel/orderInterceptRule/updateStatus',{
        'id' : id,
        'status' : ruleStatus
    },function(data){
        if (data.type == 'success') {
        	if(ruleStatus==1){
        		 $.alert('已启用',"提示");
        	}else{
        		 $.alert('已停用',"提示");
        	}
            reloadOrderInterceptRule();
        } else {
            $.alert(data.txt,"提示");
        }

    });
}

function deleteRule(id) {
    $.confirm({
        title: '提示',
        confirmButton: '确认',
        cancelButton: '取消',
        content: "确认删除所选记录?",
        confirm: function () {
            $.post('hotel/orderInterceptRule/delete',{
                id : id
            },function(data){
                if (data.type == 'success') {
                    $.alert(data.txt,"提示");
                    reloadOrderInterceptRule();
                } else {
                    $.alert(data.txt,"提示");
                }

            });
        },
        cancel: function () {
        }
    });
}



//转到添加或修改页面
function getRule(id, isClone){
    $.post("hotel/orderInterceptRule/toRule",{'id':id , isClone: isClone},function(data){
        $("#ruleDetail").html(data);
        $("#ruleMain").hide();
        $("#ruleDetail").show();
    });
}

//返回
function backRuleMain(){
   
    $("#ruleMain").show();
    $("#ruleDetail").hide().html("");
    reset_rule();
}

