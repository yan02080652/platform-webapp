<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="ruleMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>酒店拦截规则清单&nbsp;&nbsp;
			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12">
										<div class="pull-left">
											<a class="btn btn-default" onclick="getRule('')" role="button">新增规则</a>
										</div>
										<!-- 查询条件 -->
											<form class="form-inline" method="post" >
												<nav class="text-right">
													<div class=" form-group" id="partner">
													 <label class="control-label">适用范围：</label>
														<input name="partnerName" id="partnerNames" class="form-control" placeholder="TMC/企业" readonly>
														<input type="hidden" id="partnerIds">
													</div>
													<div class=" form-group">
													 <label class="control-label">关键字搜索：</label>
														<input name="keyWords" id="keyWords" class="form-control" placeholder="规则说明/酒店名称">
													</div>
													<div class="form-group">
														<button type="button" class="btn btn-default" onclick="search_rule()">搜索</button>
														<button type="button" class="btn btn-default" onclick="reset_rule()">重置</button>
													</div>
												</nav>
											</form>
										</div>
									</div>
									<div  id="rule_Table" style="margin-top: 15px">

					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 详细页面 -->
<div id="ruleDetail" style="display:none;"></div>
<script type="text/javascript" src="webpage/hotel/orderInterceptRule/orderInterceptRule.js?version=${globalVersion}"></script>
<link rel="stylesheet" href="resource/plugins/jedate-3.8/skin/jedate.css?version=${globalVersion}"/>
<script type="text/javascript" src="resource/plugins/jedate-3.8/jquery.jedate.js?version=${globalVersion}"></script>
<script>
var isAdmin = '${isAdmin}';
if (isAdmin == 'false') {
    $('#partnerNames').click(function(){
        //企业
        TR.select('partner',{type:0},function(data){
            $('#partnerNames').val(data.name);
            $('#partnerIds').val(data.id);
        });
    });
} else {
    $('#partnerNames').click(function(){
        TR.select('partner',function(data){
            $('#partnerNames').val(data.name);
            $('#partnerIds').val(data.id);
        });
    });
}
</script>