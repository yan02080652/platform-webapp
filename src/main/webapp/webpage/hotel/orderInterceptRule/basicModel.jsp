<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<style type="text/css">
	body{ font-size:14px; line-height:24px;}
	.form-group{
		padding-bottom: 15px;
		padding-top: 15px;
	}
	.move_left{
		position: relative;
		right: 160px;
	}

	.control-label .reqMark{
	     color: red; 
   		 font-weight: bold;
   		 font-size: 15px;
	}
</style>
<form:form  action="hotel/orderInterceptRule/save" cssClass="form-horizontal bv-form"
								method="post" modelAttribute="ruleDto" id="ruleForm">
						<form:hidden path="tmcId" />
						<form:hidden path="creatorId"/>
						<form:hidden path="updaterId"/>
						<form:hidden path="roomNames"/>
						<form:hidden path="stars"/>
						<form:hidden path="applyType"/>
					  
						<div class="form-inline col-sm-10" style="margin-top: 20px;">
							
								<label  class="control-label col-sm-2"><span class="reqMark">*</span>规则说明:</label>
								<div class="col-sm-2">
									<input id="code" name="code"  class="form-control"  style="width: 100%;" value="${ruleDto.code}"  readonly />
									<input id="id" name="id"  class="form-control"  style="width: 100%;" value="${ruleDto.id}"  type="hidden" />
									<input id="isClone" name="isClone"  class="form-control"  style="width: 100%;" value="${isClone}"  type="hidden" />
									<input id="isLimitStar" name="isLimitStar"  class="form-control"  style="width: 100%;" value="${ruleDto.isLimitStar}"  type="hidden" />
									
								</div>
								<div class="col-sm-6">
									<form:input path="declaration" cssClass="form-control" placeholder="规则说明" cssStyle="width: 100%;"/>
								</div>
						</div>
						<div class="form-inline col-sm-10" style="margin-top: 20px;">
								<label  class="control-label col-sm-2"><span class="reqMark">*</span>规则类型:</label>
								<div class="col-sm-2">
									<label class="radio-inline ">
										<input type="radio" name="level"  value="tmc"  ${ruleDto.tmcId eq ruleDto.partnerId ? "checked" : ""}> TMC级
									</label>
									<label class="radio-inline">
										<input type="radio" name="level"   value="partner"  ${ruleDto.tmcId ne ruleDto.partnerId ? "checked" : ""} > 企业级
									</label>
								</div>
								<form:hidden path="partnerId"/>
								<div id="application">
								<label class="col-sm-1 control-label">适用范围:</label>
								<div class="col-sm-5">
									<input  id="partnerName" class="form-control" placeholder="TMC/企业" value="${ruleDto.partnerName}" style="width: 100%;" readonly />
									
								</div>
								</div>
						</div>
						<div class="form-inline col-sm-10" style="margin-top: 20px;">
							
								<label class="control-label col-sm-2"><span class="reqMark">*</span>渠道来源:</label>
								<div class="col-sm-2">
									<label class="checkbox-inline"> 
									<input type="checkbox" id="isLimitOrigin" name="isLimitOrigin"  ${not empty ruleDto.isLimitOrigin and ruleDto.isLimitOrigin ? 'checked' : ''}  />全部
									</label>
								</div>
							
								<div class="col-sm-6">
									<c:forEach items="${allOrgin}" var="s">
										<label class="checkbox-inline"> 
										<input type="checkbox"	name="originCodes" ${not empty ruleDto.isLimitOrigin and ruleDto.isLimitOrigin ? 'checked' : '' } value="${s.hubsCode}">${s.hubsName}
										</label>
									</c:forEach>
								</div>
						</div>
						<div class="form-inline col-sm-10" style="margin-top: 20px;">
						
							<label  class="control-label col-sm-2"><span class="reqMark">*</span>生效时段:</label>
							<div class="col-sm-2">
								<input class="form-control time-picker"  name="starTime" id="starTime"  readonly placeholder="选择起始时间" value="${not empty ruleDto.starTime ? ruleDto.starTime:'00:00'}"/>
							</div>
							<div style="float:left;margin-left:20px">
								<label>至</label>
							</div>
							<div class="col-sm-2" >
								<input class="form-control time-picker"  name="endTime" id="endTime"    readonly placeholder="选择终止时间" value="${not empty ruleDto.endTime ? ruleDto.endTime:'23:59'}"/>
							</div>
							<div class="col-sm-2" >
								<label class="checkbox-inline">
								<input type="checkbox" id="isIncludeWork" name="isIncludeWork" ${ruleDto.isIncludeWork ? 'checked' : ''} value="${ruleDto.isIncludeWork == true ? true : false}">周一至周五
								</label>
							</div>
							<div class="col-sm-2">
									<label class="checkbox-inline">
										<input type="checkbox" id="isIncludeWeek" name="isIncludeWeek" ${ruleDto.isIncludeWeek ? 'checked' : ''} value="${ruleDto.isIncludeWeek == true ? true : false}">含周六周日
									</label>
							</div>
						</div>
                        <div class="form-inline col-sm-10" style="margin-top: 20px;">
								<label  class="control-label col-sm-2">支付方式:</label>
								<div class="col-sm-2">
									<label class="checkbox-inline">
										预付
										<input type="hidden" id="payType" name="payType"  class="form-control"  value="1" />
									</label>
								</div>
						</div>
						<div class="form-inline col-sm-10" style="margin-top: 20px;">
								<label  class="control-label col-sm-2"><span class="reqMark">*</span>销售价格:</label>
								<div class="col-sm-2">
									<input  id="salePrice" name="salePrice"  class="form-control" value="${ruleDto.salePrice}" />
								</div>
								<label  class="control-label col-sm-1">	元以上</label>
						</div>
						<div class="form-inline col-sm-10" style="margin-top: 20px;">
								<label  class="control-label col-sm-2"><i class="effective-date fa fa-question-circle layui-word-aux"></i>入住日期:
								</label>
								<div class="col-sm-4">
									<input id="checkIn" name="checkIn" placeholder="适用入住日期(示例:20180214-20180222)" class="form-control"  style="width: 100%;" value="${ruleDto.checkIn}"   />
								</div>
								<label  class="control-label col-sm-1">	距当前</label>
								<div class="col-sm-2">	
									<input type="number"   id="distanceFromCurrent" name="distanceFromCurrent"  class="form-control"  style="width: 100%;" value="${ruleDto.distanceFromCurrent}"   />
								</div>
								<label  class="control-label col-sm-1">天以上</label>
						</div>
						<div  class="form-inline col-sm-10" style="margin-top: 20px;">
							<label  class="control-label col-sm-2"><span class="reqMark">*</span>状态:</label>
							<div class="col-sm-8">
								<label class="radio-inline">
									<input type="radio" name="status"  value="true" ${ruleDto.status ? "checked" : ""}>启用
								</label>
								<label class="radio-inline">
									<input type="radio" name="status"   value="false" ${ruleDto.status ? "" : "checked"}>禁用
								</label>
							</div>
						</div>
						<div class="effectiveDate" hidden>
                            <p>每个日期区间按照【起始日期】+【"-"】+【截止日期】构成，多个日期区间以英文","分隔</p>
                            <p>示例:20180214-20180222,20181001-20181007</p>
                        </div>
                        <div class="col-sm-12 col-sm-offset-3" style="margin-top: 20px;">
							<div class="col-sm-2">
								<button type="button" class="btn btn-primary" onclick="submitRule()">保存</button>
							</div>
						
							<div class="col-sm-2">
								<button type="button" class="btn btn-primary" onclick="backRuleMain()">返回</button>
							</div>
						</div>
</form:form>
<script>
$(".time-picker").jeDate({
    format: "hh:mm"
});

if ($(":radio[value=tmc]").is(':checked')&&isAdmin == "false") {
    $("#application").hide();
}

// 适用范围
$(":radio[value=partner]").click(function () {
    $('#partnerName').val('');
    $('#partnerId').val('');
	if ($(this).is(":checked")) {
        $("#application").show();
        $('#partnerName').click(function(){
            partner(0);// partner
        });
	}
});
$(":radio[value=tmc]").click(function () {
    $('#partnerName').val('');
    $('#partnerId').val('');
    if ($(this).is(":checked")) {
       if (isAdmin == "true") {
           $('#partnerName').click(function(){
               partner(1);// TMC
           });
	   } else {
           $("#application").hide();
       }
    }
});

$('#partnerName').click(function(){
    if ($(":radio[value=partner]").is(':checked')) {
        partner(0);
    }
    if ($(":radio[value=tmc]").is(':checked')) {
        partner(1);
    }
});

function partner(t) {
    TR.select('partner',{type:t},function(data){
        $('#partnerName').val(data.name);
        $('#partnerId').val(data.id);
    });
}

//全时段
$("#isFullTime").click(function () {
	if ($(this).is(':checked')) {
	    $("#startTime").val('');
        $("#endTime").val('');
        $("#startTime").prop('disabled', true);
        $("#endTime").prop('disabled', true);
	} else {
        $("#startTime").prop('disabled', false);
        $("#endTime").prop('disabled', false);
	}
});

//设置来源选中
if ('${hubJSONString}') {
    var hubJSONString = '${hubJSONString}';
    var hubCheckbox = $("#isLimitOrigin").parents('.form-inline').find(':checkbox');
    for (var i = 1; i < hubCheckbox.length; i++) {
        if (hubJSONString.match(new RegExp(hubCheckbox[i].value, "gm"))) {
            $(hubCheckbox[i]).prop('checked', true);
            continue;
		}
	}
}

$("#isIncludeWeek").click(function () {
	if ($(this).is(':checked')) {
        $(this).val(true);
	} else {
        $(this).val(false);
	}
});

$("#isIncludeWork").click(function () {
	if ($(this).is(':checked')) {
        $(this).val(true);
	} else {
        $(this).val(false);
	}
});


$("#isLimitOrigin").click(function () {
	if ($(this).is(':checked')) {
        $(this).val(true);
	} else {
        $(this).val(false);
	}
});


// 订单提交校验
function validateResult() {
    if ($("#declaration").val().trim()=='') {
        errorMsg($("#declaration"), '规则名称不能为空');
        return false;
    }
    if($("#declaration").val().length>50){
    	errorMsg($("#declaration"), '规则名称长度不能超过50个字符');
        return false;
    }
    if (!$("#partnerName").is(":hidden") && $("#partnerName").val().trim()=='') {
        errorMsg($("#partnerName"), '请选择适用范围');
        return false;
    }
	var result = false;
    $("#isLimitOrigin").parents('.form-inline').find(":checkbox").each(function (i, item) {
		   if ($(this).is(":checked")) {
            result |= true;
		   }
    });
    if (!result) {
        errorMsg($("#isLimitOrigin"), '渠道来源必须选择');
        return false;
	  	 }
    
    if ($("#starTime").val().trim()=='') {
        errorMsg($("#starTime"), '请选择起始时间');
        return false;
    }
    if ($("#endTime").val().trim()=='') {
        errorMsg($("#endTime"), '请选择结束时间');
        return false;
    }
    if(!($("#isIncludeWork").is(":checked")||$("#isIncludeWeek").is(":checked"))){
    	errorMsg($("#isIncludeWork"), '工作日与非工作日二者必选一个');
    	errorMsg($("#isIncludeWeek"), '工作日与非工作日二者必选一个');
    	return false;
    }
    var starTime = $("#starTime").val().split(":");
    var endTime = $("#endTime").val().split(":");

    if (parseInt(starTime[0]) > parseInt(endTime[0])) {
        errorMsg($("#endTime"), '起始日期必须小于终止日期');
        return false;
	} else if (parseInt(starTime[0]) == parseInt(endTime[0]) && parseInt(starTime[1]) >= parseInt(endTime[1])) {
        errorMsg($("#endTime"), '起始日期必须小于终止日期');
        return false;
	}
   if(!dateCheck()){
    	return false;
    } 
    if(!ValidPrice()){
    	errorMsg($("#salePrice"), '请输入正数，可精确到2位小数！');
    	return false;
    }
    if ($("#distanceFromCurrent").val()==''||$("#distanceFromCurrent").val()<0) {
    	
        errorMsg($("#distanceFromCurrent"), '距离当前天数不能为空且大于等于0');
        return false;
    }
    
    if((/^(\+|-)?\d+$/.test($("#distanceFromCurrent").val()))){  
       
    }else{  
    	errorMsg($("#distanceFromCurrent"), '请输入正整数');
        return false;  
    } 
    return true;
}
function submitRule(){
   if(!validateResult()) {
    return;
   } 
    $.ajax({
        cache : true,
        type : "POST",
        url : "hotel/orderInterceptRule/saveBasic",
        data : $('#ruleForm').serialize(),
        async : false,
        error : function(request) {
            $.alert("请求失败，请刷新重试","提示");
        },
        success : function(data) {
            if (data.code == '0') {
            	$.alert(data.msg,"提示");
                interceptId=data.data;
                isClone=false;
                $("#isClone").val(isClone);
            } else {
                $.alert(data.txt,"提示");
            }
        }
    });
}



//点击全部,选中或者去掉子选项
$("input[name^='isLimit']").click(function () {
    if ($(this).is(":checked")) {
        $(this).parents('.form-inline').find(':checkbox').prop('checked', true);
        $(this).val(true);
    } else {
        $(this).parents('.form-inline').find(':checkbox').prop('checked', false);
        $(this).val(false);
    }
});

//是否去掉全部
$(":checkbox:not([name^='isLimit'])").click(function () {
    if ($(this).prop('name') == 'isIncludeWeek'||$(this).prop('name') == 'isIncludeWork') { //特殊
        return;
	}
    if (!$(this).is(":checked")) {
        $(this).parents('.form-inline').find(':checkbox:eq(0)').prop('checked', false);
        $(this).parents('.form-inline').find(':checkbox:eq(0)').val(false);
    }
});



//是否勾选上全部
$(":checkbox:not([name^='isLimit'])").click(function () {
	 if ($(this).prop('name') == 'isIncludeWeek'||$(this).prop('name') == 'isIncludeWork') { //特殊
         return;
		}
    if ($(this).is(":checked")) {
        var result = 0;
        var labels = $(this).parent().siblings();
        for (var i = 0 ; i < labels.length; i++) {
            if (!$(labels[i]).find(":checkbox").is(':checked')) {
                break;
            }
            result ++;
        }
        if (result == labels.length) {
            $(this).parents('.form-inline').find(':checkbox:eq(0)').prop('checked', true);
            $(this).parents('.form-inline').find(':checkbox:eq(0)').val(true);
        }
    }
});

/***************拦截规则******************/
$(function(){
	initControl();
	})
var interceptId;

function  initControl(){
	interceptId=$('#id').val();
	intercept.showHelpTip(".effective-date", $(".effectiveDate").html());
}
var intercept={
    showHelpTip: function (className, dom) {
        $(document).on('mouseover', className, function () {
            var that = this;
            var index = layer.tips(dom, that, {
                tips: [2, '#1AA094'],
                time: 0,
                area: '500px'
            });
            $(that).on('mouseleave', function () {
                layer.close(index);
            })
        });
    },
    tipData: [
        {className: ".effective-date", dom: $(".effectiveDate").html()},
    ],
}  

function dateCheck(){
		var pattern=/^((((1[6-9]|[2-9]\d)\d{2})(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0[13456789]|1[012])(0[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})02(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0229))-((((1[6-9]|[2-9]\d)\d{2})(0[13578]|1[02])(0[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})(0[13456789]|1[012])(0[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})02(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))0229))$/;
   		var pattern1=/(\d{4})(\d{2})(\d{2})/;
        var len= $("#checkIn").val().length;
        var startdate,enddate;
        if(len==0){
        	return true;
        }
        if(len==17){
            startdate=$("#checkIn").val().split('-')[0].replace(pattern1, '$1-$2-$3');
            enddate=$("#checkIn").val().split('-')[1].replace(pattern1, '$1-$2-$3');
            if(new Date(startdate).getTime()>new Date(enddate).getTime()){
            	errorMsg($("#checkIn"), '日期格式输入不正确，请参照格式要求填写！');
                return false;
            }
            if(!pattern.test($("#checkIn").val())){
            	errorMsg($("#checkIn"), '日期格式输入不正确，请参照格式要求填写！');
                return false;
            }
        }else if(len>=35&&(len-35)%18==0){
            var arr=$("#checkIn").val().split(',');
            arr.forEach(function(value,index,item){
                if(!pattern.test(item[index])){
                	errorMsg($("#checkIn"), '日期格式输入不正确，请参照格式要求填写！');
                    return false;
                }
                if(index==0){
                    startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                    enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                    if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                    	errorMsg($("#checkIn"), '日期格式输入不正确，请参照格式要求填写！');
                        return false;
                    }
                }else{
                    if(new Date(enddate).getTime()>new Date(item[index].split('-')[0].replace(pattern1, '$1-$2-$3')).getTime()){
                        return false;
                    }
                    startdate=item[index].split('-')[0].replace(pattern1, '$1-$2-$3');
                    enddate=item[index].split('-')[1].replace(pattern1, '$1-$2-$3');
                    if(new Date(startdate).getTime()>new Date(enddate).getTime()){
                    	errorMsg($("#checkIn"), '日期格式输入不正确，请参照格式要求填写！');
                        return false;
                    }
                }
            })
        }else{
        			errorMsg($("#checkIn"), '日期格式输入不正确，请参照格式要求填写！');
        			return false;
        }
        return true;
}
function ValidPrice()
{
	var s=$("#salePrice").val();
	var reg = /^[0-9]+([.]{1}[0-9]{1,2})?$/;
	if (!reg.test(s))
	{
		return false;
	}
	return true;
}
</script>