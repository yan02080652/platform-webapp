<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" href="resource/plugins/city/cityselect.css?version=${globalVersion}">
<div id="ruleMain">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span>酒店品牌维护 &nbsp;&nbsp;
            </div>
            <div id="rightDetail" class="col-md-12 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <!-- 查询条件 -->
                                            <div class="form-inline" >
                                                <nav class="text-left">
                                                    <div class="form-group">
                                                        <label class="control-label">品牌名称: </label>
                                                        <input class="form-control" placeholder="品牌名称" id="brand_name" autocomplete="off">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">是否热门: </label>
                                                        <select class="form-control" style="width:100px;" id="is_hot">
                                                            <option value="">全部</option>
                                                            <option value="1">是</option>
                                                            <option value="0">否</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-default btn-primary"
                                                                onclick="reloadList(1)">搜索
                                                        </button>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div id="rule_Table" style="margin-top: 15px">
                                    <jsp:include page="table.jsp" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="add_protocol"></div>

<script src="${baseDes}/webpage/hotel/brand/brand.js"></script>