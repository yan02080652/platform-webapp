$(function(){
    reloadList(1);
})


function reloadList(pageIndex){
    var name = $('#brand_name').val();
    var is_hot = $('#is_hot').val();

    $.ajax({
        url: '/hotel/brand/list',
        type: 'post',
        data: {
            name: name,
            isHot: is_hot,
            pageIndex: pageIndex
        },success: function(data){
            $('#rule_Table').html(data);
        }, error: function () {
            layer.msg("系统异常", {icon: 2});
        }
    })


}

//热门切换
function changeHot(id,isHot){
    $.ajax({
        url: '/hotel/brand/changeHot',
        type: 'post',
        data: {
            id: id,
            isHot: isHot
        },success: function(data){
            if(data.success){
                layer.msg("设置成功",{icon: 1});
                reloadList($('.cur').text())
            }else{
                layer.msg("设置失败: " + data.msg,{icon: 2})
            }
        },error: function(){
            layer.msg("系统异常", {icon: 2});
        }
    })
}