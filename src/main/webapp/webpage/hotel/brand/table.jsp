<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp" %>

<table class="table table-striped table-bordered table-hover" id="ruleTable">
    <thead>
    <tr>
        <th style="text-align: center;"><label>ID </label></th>
        <th><label>品牌名称 </label></th>
        <th><label>是否热门 </label></th>
        <th><label>操作</label></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${pageList.list }" var="bean">
        <tr class="odd gradeX">
            <td style="text-align: center;">${bean.id }</td>
            <td>${bean.shortName }</td>
            <td><span class="label ${bean.isHot ? 'label-info' : 'label-danger' }">${bean.isHot ? '是' : '否' }</span></td>
            <td class="text-center">
                <a class="detail" onclick="changeHot(${bean.id},!${bean.isHot})">${bean.isHot ? '取消' : '设为'}热门</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagin">
    <jsp:include page="../../common/pagination_ajax.jsp?callback=reloadList"></jsp:include>
</div>