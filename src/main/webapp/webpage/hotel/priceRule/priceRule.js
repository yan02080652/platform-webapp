$(function(){
	reloadPriceRule();
	
})

function reloadPriceRule(pageIndex){
	//加载列表
	var keyword = $("#keyword").val();
	var templateId = $('#templateId').val();
	var cId = $('#c_id').val();
	$.post('hotel/priceRule/list',{
		keyword:keyword,
		templateId:templateId,
		cId:cId,
		pageIndex:pageIndex
		},function(data){
		$("#rule_Table").html(data);
	});
}

//返回模板页面
function backTemplate(){
	 location.href = "/hotel/priceRuleTemplate";
}

//转到添加或修改页面
function getRule(id){
	$.post("/hotel/priceRule/getRule",{'id':id,'templateId':$('#templateId').val()},function(data){
		$("#ruleDetail").html(data);
		$("#ruleMain").hide();
		$("#ruleDetail").show();
	});
}

//查找用户
function search_rule(){
	reloadPriceRule();
}

//重置
function reset_rule(){
	$("#keyword").val("");
	$("#c_id").val("");
	$("#c_name").val("");
	reloadPriceRule();
}

//checkbox全选
function selectAllRule(){
	if ($("#selectAllRule").is(":checked")) {
		 $("#ruleTable input[type=checkbox]").prop("checked", true);//所有选择框都选中
    } else {
    	 $("#ruleTable input[type=checkbox]").prop("checked", false);
    }
}

//返回
function backRuleMain(){
	$("#ruleMain").show();
	$("#ruleDetail").hide().html("");
	reloadPriceRule();
}

//批量删除
function batchDeleteRule(){
    //判断至少选了一项
    var checkedNum = $("#ruleTable input[name='ids']:checked").length;
    if(checkedNum==0){
        $.alert("请选择你要删除的记录!","提示");
        return false;
    }
    
    var ids = new Array();
    $("#ruleTable input[name='ids']:checked").each(function(){
    	ids.push($(this).val());
    });
    
    confirmDeleteRule(ids.toString());
}

//关联企业
function relateCompany(id){
	$.post("hotel/priceRule/index",{'id':id},function(data){
		$("#ralateCompany").html(data);
		$("#ruleMain").hide();
		$("#ralateCompany").show();
	});
}

//单个删除
function deleteRule(id) {
	confirmDeleteRule(id);
};

//确认删除
function confirmDeleteRule(id){
	$.confirm({
		title : '提示',
		confirmButton:'确认',
		cancelButton:'取消',
		content : "确认删除所选记录?",
		confirm : function() {
			$.ajax({
				cache : true,
				type : "POST",
				url : "hotel/priceRule/delete",
				data : {
					ids : id
				},
				async : false,
				error : function(request) {
					showErrorMsg("请求失败，请刷新重试");
				},
				success : function(data) {
					if (data.type == 'success') {
						showSuccessMsg(data.txt);
						reloadPriceRule();
					} else {
						$.alert(data.txt,"提示");
					}
				}
			});
		},
		cancel : function() {
		}
	});
}