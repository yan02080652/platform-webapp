<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="glyphicon glyphicon-home" aria-hidden="true"></span>加价模板关联企业
				<c:choose>
					<c:when test="${priceRuleDto.id != '' && priceRuleDto.id != null}">修改</c:when>
					<c:otherwise>添加</c:otherwise>
				</c:choose>
				&nbsp;&nbsp;<a data-pjax onclick="backRuleMain()">返回</a>
			</div>
			<div class="panel-body">
				<form:form   cssClass="form-horizontal bv-form"
					method="post" action="hotel/priceRule/save"
					modelAttribute="priceRuleDto" >
					<form:hidden path="id" />
					<form:hidden path="templateName"/>
					<form:hidden path="brandId" />
					<form:hidden path="hotelId" />
					<div class="form-group">
						<label class="col-sm-2 control-label">选择模板</label>
						<div class="col-sm-6">
							<form:select path="templateId" cssClass="form-control" onchange="templateChange(this.value)">
								<form:option value="-1">自定义</form:option>
								<form:options items="${templates }" itemLabel="name" itemValue="id" />
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">选择企业</label>
						<div class="col-sm-3">
							<label class="radio-inline"><form:radiobutton path="ruleGrade" value="1"  checked="true"/>&nbsp;&nbsp;&nbsp;&nbsp;TMC级&nbsp;&nbsp;&nbsp;&nbsp;</label>
							<label class="radio-inline"><form:radiobutton path="ruleGrade" value="2" />&nbsp;&nbsp;&nbsp;&nbsp;企业级&nbsp;&nbsp;&nbsp;&nbsp;</label>
						</div>
						<div class="col-sm-3">
							<form:hidden path="cId"/>
							<form:input path="cName" cssClass="form-control"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">产品类型</label>
						<div class="col-sm-6">
							<c:forEach items="${paymentTypes }" var="bean" varStatus="status">
								<c:if test="${bean ne 'CardGuarantee' }">
									<label class="checkbox-inline">
									<form:checkbox path="productTypes" value="${bean }" />&nbsp;&nbsp;&nbsp;&nbsp;${bean.message }&nbsp;&nbsp;&nbsp;&nbsp;</label>
								</c:if>
							</c:forEach>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">过滤非立即确认的产品</label>
						<div class="col-sm-6">
							<label class="radio-inline"><form:radiobutton path="filterImmediatelyConfirm" value="true"  checked="true"/>&nbsp;&nbsp;&nbsp;&nbsp;是&nbsp;&nbsp;&nbsp;&nbsp;</label> 
							<label class="radio-inline"><form:radiobutton path="filterImmediatelyConfirm" value="false" />&nbsp;&nbsp;&nbsp;&nbsp;否&nbsp;&nbsp;&nbsp;&nbsp;</label>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">酒店品牌</label>
						<div class="col-sm-6">
							<input class="form-control" id="brandsDesc" value="${priceRuleDto.brandsDesc}"  readonly onclick="selHotelBrandData()" style="width: 100%;"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">适用酒店</label>
						<div class="col-sm-6">
							<label class="radio-inline"><form:radiobutton path="suitableHotelType" value="1" checked="true" />适用所有酒店</label>
							<label class="radio-inline"><form:radiobutton path="suitableHotelType" value="2"  />适用以下酒店</label>
						</div>
					</div>
					<div class="form-group hoteldiv">
						<div class="col-sm-offset-2 col-sm-2">
							<a class="btn btn-primary" id="selecthotel" onclick="selHotelData()">+酒店</a>
						</div>
					</div>
					<div class="form-group hoteldiv">
						<div class="col-sm-offset-2 col-sm-6">
							<table class="table table-striped table-bordered table-hover" id="hoteltable">
								<thead>
								<tr>
									<th><label>酒店名称</label></th>
									<th><label>操作</label></th>
								</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">加价配置(预付) ↓</label>
					</div>
					<div class="form-group form-inline">
						<label class="col-sm-2 control-label">无星级</label>
						<div class="col-sm-6 ">
							<form:select path="noStarType" cssClass="form-control" style="width:32%;">
								<form:options items="${priceAddTypes }" itemLabel="desc" />
							</form:select>
							<form:input path="noStarAmount" placeholder="此处填 X , 例如 0.35" style="width:50%;" cssClass="form-control"/>
						</div>
					</div>
					<div class="form-group form-inline">
						<label class="col-sm-2 control-label">二星级/经济</label>
						<div class="col-sm-6 ">
							<form:select path="economyType" cssClass="form-control" style="width:32%;">
								<form:options items="${priceAddTypes }" itemLabel="desc" />
							</form:select>
							<form:input path="economyAmount" placeholder="此处填 X , 例如 0.35" style="width:50%;" cssClass="form-control"/>
						</div>
					</div>
					<div class="form-group form-inline">
						<label class="col-sm-2 control-label">三星级/舒适</label>
						<div class="col-sm-6">
							<form:select path="comfortType" cssClass="form-control" style="width:32%;">
								<form:options items="${priceAddTypes }" itemLabel="desc" />
							</form:select>
							<form:input path="comfortAmount" placeholder="此处填 X , 例如 0.35" style="width:50%;" cssClass="form-control"/>
						</div>
					</div>
					<div class="form-group form-inline">
						<label class="col-sm-2 control-label">四星级/高档</label>
						<div class="col-sm-6">
							<form:select path="topGradeType" cssClass="form-control" style="width:32%;">
								<form:options items="${priceAddTypes }" itemLabel="desc" />
							</form:select>
							<form:input path="topGradeAmount" placeholder="此处填 X , 例如 0.35" style="width:50%;" cssClass="form-control"/>
						</div>
					</div>
					<div class="form-group form-inline">
						<label class="col-sm-2 control-label">五星级/豪华</label>
						<div class="col-sm-6">
							<form:select path="luxuriousType" cssClass="form-control" style="width:32%;" >
								<form:options items="${priceAddTypes }" itemLabel="desc"/>
							</form:select>
							<form:input path="luxuriousAmount" placeholder="此处填 X , 例如 0.35" style="width:50%;" cssClass="form-control"/>
						</div>
					</div>
					<%--<div class="form-group">
						<label class="col-sm-2 control-label">预订服务费</label>
						<div class="col-sm-6">
							<form:input path="bookingFee" placeholder="预订服务费 = X元/间夜 , 默认为0 , 此处填 X" cssClass="form-control"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">退房服务费</label>
						<div class="col-sm-6">
							<form:input path="refundFee" placeholder="退房服务费 = X元/间夜 , 默认为0 , 此处填 X" cssClass="form-control"/>
						</div>
					</div>--%>
				</form:form>
				<div class="form-group">
					<div class="col-sm-offset-4">
						<a class="btn btn-default" onclick="submitRule()">提交</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
 var getHotelBrandChoosedData = JSON.parse('${brandList}' || '[]');
 var getHotelChoosedData= JSON.parse('${hotelList}' || '[]');
$(function(){
	if($('#templateId option[selected="selected"]').val() > 0 ){
		dis(true)
	}else{
		dis(false)
	}
	$('#cName').click(function(){
        var ruleGrade=$("input[name='ruleGrade']:checked").val();
        TR.select('partner',{
            type: ruleGrade==2?0:1
        },function(data){
            $('#cName').val(data.name);
            $('#cId').val(data.id);
        });

	});
    setVisiable();
    addHotel();
})

 $("input[type=radio][name='ruleGrade']").change(function() {
     $('#cName').val("");
     $('#cId').val("");
 })

$("input[type=radio][name='suitableHotelType']").change(function() {
    if(this.value == 1){
        $('.hoteldiv').hide();
    }else{
        $('.hoteldiv').show();
    }
})

function setVisiable(){
    var suitableHotelType=$("input[name='suitableHotelType']:checked").val();
    if( suitableHotelType == 2){
        $('.hoteldiv').show();
    }else{
        $('.hoteldiv').hide();
    }
}
function selHotelBrandData() {
    getHotelChoosedData = [];
    $('#hoteltable tbody').find('tr').remove();
    new choose.hotelBrand({
        id:'selMultiBrand',//给id可以防止一直新建对象
        multi:false,
        empty:true,
        getChoosedData:function () {
            return getHotelBrandChoosedData;
        }
    },function (data) {
        console.log("sss", data);
        var brandsDesc = '';
        getHotelBrandChoosedData = [];
        if (data) {
            $(data).each(function (i, item) {
                getHotelBrandChoosedData.push({id: item.id, shortName: item.shortName});
                if (i <= 2) {
                    brandsDesc += item.shortName+",";
                }
                if (i == 3) {
                    brandsDesc = brandsDesc.substr(0, brandsDesc.length - 1) + '  ...';
                }
            });
            if (getHotelBrandChoosedData.length <= 3) {
                brandsDesc = brandsDesc.substr(0, brandsDesc.length - 1);
            }
        }
        $('#brandsDesc').val(brandsDesc);
        if(getHotelBrandChoosedData.length>0){
            $("input[name='brandId']").val(getHotelBrandChoosedData[0].id);
        }else{
            $("input[name='brandId']").val("");
        }
    });
};

function selHotelData() {
    var brand = $("input[name='brandId']").val();
    new choose.hotelByBrand(brand =='ALL'?'':brand,{
        //id:'selMultiHotel',//给id可以防止一直新建对象
        multi:true,
        empty:true,
        getChoosedData:function () {
            return getHotelChoosedData;
        }
    },function (data) {
        console.log("sss", data);
        var brandsDesc = '';
        getHotelChoosedData = [];
        if (data) {
            $(data).each(function (i, item) {
                if(getHotelChoosedData.indexOf(item)==-1)
                    getHotelChoosedData.push({id: item.id, name: item.name});

            });
            addHotel();
        }
    });
};

function addHotel(){
    $('#hoteltable tbody').find("td").remove();
    if (getHotelChoosedData.length != 0) {
        $(getHotelChoosedData).each(function (i, item) {
            $('#hoteltable tbody').append("<tr><td>"+item.name+"</td><td class=\"text-center\" ><a onclick=\"removeHotel("+item.id+",this)\" >删除</a> </td></tr>");
        });
    }
    setHotelIds();
}
//加载模板酒店列表
function loadHotelOfTemplate(data){
    $('#hoteltable tbody').find("td").remove();
    if(data!=null&&data.length!=0){
        $(data).each(function (i, item) {
            $('#hoteltable tbody').append("<tr><td>"+item.name+"</td><td class=\"text-center\" ><a onclick=\"removeHotel("+item.id+",this)\" >删除</a> </td></tr>");
        });
	}

}
function removeHotel(hotelId,obj){
    for(var i = 0; i < getHotelChoosedData.length ;i++){
        if(getHotelChoosedData[i].id=hotelId){
            getHotelChoosedData.splice(i,1);
            break;
        }
    }
    $(obj).parents('tr').remove();
    setHotelIds();
}
//设置酒店的值
function setHotelIds(){
    var hotelId='';
    if (getHotelChoosedData.length != 0) {
        $.each(getHotelChoosedData,function(index,item){
            hotelId+=item.id+",";
        });
        if(hotelId.length>0){
            hotelId.substr(0,hotelId.length-1);
        }
    }else{
        hotelId='ALL';
    }
    $('input[name="hotelId"]').val(hotelId);
}

//提交表单
function submitRule(){
	var $productTypes = $('input[name="productTypes"]');
	var flag = false;
	for(var i = 0;i<$productTypes.length;i++){
		flag |= $productTypes[i].checked;
	}
	if(!flag){
		layer.tips('请选择产品类型', $("input[name='productTypes']"), {
			  tips: [1, '#3595CC'],
			  time: 4000
			});
		return;
	}
	if($('#cName').val().trim() == ''){
		layer.tips('请选择企业', $("#cName"), {
			  tips: [1, '#3595CC'],
			  time: 4000
			});
		return;
	}
    if($("input[name='suitableHotelType']:checked").val()==1){
        $('input[name="hotelId"]').val("ALL");
    }
    if($("input[name='brandId']").val().trim().length==0){
        $("input[name='brandId']").val("ALL")
	}
	dis(false);
	$.ajax({
		cache : true,
		type : "POST",
		url : "hotel/priceRule/save",
		data : $('#priceRuleDto').serialize(),
		async : false,
		error : function(request) {
			showErrorMsg("请求失败，请刷新重试");
		},
		success : function(data) {
			if (data.type == 'success') {
				showSuccessMsg(data.txt);
				backRuleMain();
			} else {
				dis(true);
				$.alert(data.txt,"提示");
			}
		}
	});
}

function dis(check){
	$('#refundFee').attr('disabled',check);
	$('#bookingFee').attr('disabled',check);
	$('#noStarAmount').attr('disabled',check);
	$('#luxuriousAmount').attr('disabled',check);
	$('#topGradeAmount').attr('disabled',check);
	$('#comfortAmount').attr('disabled',check);
	$('#economyAmount').attr('disabled',check);
	$('#noStarType').attr('disabled',check);
	$('#luxuriousType').attr('disabled',check);
	$('#topGradeType').attr('disabled',check);
	$('#comfortType').attr('disabled',check);
	$('#economyType').attr('disabled',check);
	$('input[name="productTypes"]').attr('disabled',check);
	$('input[name="filterImmediatelyConfirm"]').attr('disabled',check);

	$('#brandsDesc').attr('disabled',check);
    $('input[name="suitableHotelType"]').attr('disabled',check);
    if(check){
        $('#hoteltable a').removeAttr('onclick');
        $('#selecthotel').addClass('disabled');
	}else{
        $('#selecthotel').removeClass('disabled');
	}


}

function templateChange(id){
	if(id == -1){
		dis(false);
		$('#templateName').val('自定义')
	}else{
		$.ajax({
			url: 'hotel/priceRule/getTemplate',
			type: 'post',
			async: true,
			data: {id:id},
			success: function(data){
				$('#templateName').val(data.name)
				$('#refundFee').val(data.refundFee);
				$('#bookingFee').val(data.bookingFee);
				$('#luxuriousAmount').val(data.luxuriousAmount);
				$('#topGradeAmount').val(data.topGradeAmount);
				$('#comfortAmount').val(data.comfortAmount);
				$('#economyAmount').val(data.economyAmount);
				$('#noStarAmount').val(data.noStarAmount);
				
				$('#noStarType').val(data.noStarType);
				$('#luxuriousType').val(data.luxuriousType);
				$('#topGradeType').val(data.topGradeType);
				$('#comfortType').val(data.comfortType);
				$('#economyType').val(data.economyType);
				$('#brandId').val(data.brandId);
				$('#hotelId').val(data.hotelId);
                $('#brandsDesc').val(data.brandsDesc);
                $("[name='suitableHotelType'][value='"+data.suitableHotelType+"']").prop("checked", "checked");
                loadHotelOfTemplate(data.hotelList);
                setVisiable();
				if(data.filterImmediatelyConfirm){
					$('input[name="filterImmediatelyConfirm"]').eq(0).click();
				}else{
					$('input[name="filterImmediatelyConfirm"]').eq(1).click();
				}
				
				$('input[name="productTypes"]').each(function(i){
					$('input[name="productTypes"]')[i].checked = false;
				})
				for(var i = 0; i < data.productTypes.length; i++){
					$('input[name="productTypes"][value="'+data.productTypes[i]+'"]')[0].checked = true;
				}
				
				dis(true);
			}
		})
		
	}
}
</script>