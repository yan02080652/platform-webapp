<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div id="ruleMain">
	<div class="panel-body">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 5px">
	           <span class="glyphicon glyphicon-home" aria-hidden="true"></span>企业加价规则   &nbsp;&nbsp;
			</div>
				<div id="rightDetail" class="col-md-12 animated fadeInRight"> 
				     <div class="row">
						<div class="col-lg-12 col-md-12">
					         <div class="panel panel-default"> 
					             <div class="panel-body">
									<div class="row">
										<div class="col-md-12">
										<div class="pull-left">
											<a class="btn btn-default" onclick="getRule('')" role="button">新增关联企业</a>
											<a class="btn btn-default" onclick="batchDeleteRule()" role="button">批量删除</a>
										</div>
										<!-- 查询条件 -->
											<form class="form-inline" method="post" >
												<nav class="text-right">
													<div class=" form-group">
													 <label class="control-label">查询条件：</label>
														<input name="keyword" id="keyword" type="text" value="${keyword }" class="form-control" placeholder="模板名称" style="width: 250px"/>
														<input name="cName" id="c_name" class="form-control" placeholder="企业">
														<input type="hidden" id="c_id">
													</div>
													<div class="form-group">
														<button type="button" class="btn btn-default" onclick="search_rule()">查询</button>
														<button type="button" class="btn btn-default" onclick="reset_rule()">重置</button>
													</div>
												</nav>
											</form>
										</div>
									</div>
									<div  id="rule_Table" style="margin-top: 15px">
					
					                 </div>
					             </div>
					         </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<input type="hidden" name="templateId" id="templateId" value="${templateId }">
<!-- 详细页面 -->
<div id="ruleDetail" style="display:none;"></div>
<script type="text/javascript" src="webpage/hotel/priceRule/priceRule.js?version=${globalVersion}"></script>
<script>
$('#c_name').click(function(){
	
	TR.select('partner',function(data){
		$('#c_name').val(data.name);
		$('#c_id').val(data.id);
	});
})
</script>