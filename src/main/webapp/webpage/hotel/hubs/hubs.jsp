<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>

<style type="text/css">
	.trSelected{
		background: #CCCCCC
	}
</style>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<span class="glyphicon glyphicon-home" aria-hidden="true"></span>连接器配置
				</div>
				<div style="margin-left: 10px;margin-top: 5px">
					<button class="btn btn-default" onclick="addHubs();" id="addHubs">添加</button>
					<button class="btn btn-default" onclick="editHubs();" id="editHubs">修改</button>
					<!-- <button class="btn btn-default" onclick="deleteHubs();" id="deleteHubs">删除</button> -->
				</div>
				<div style="margin-left: 10px;margin-right: 10px;margin-top: 5px">
					<table id="hubsListTable" class="table  table-bordered table-hover table-condensed dataTables-example dataTable" id="userTable">
						<tr>
					      <th style="text-align: center;"><label>连接器编码  </label></th>
				          <th style="text-align: center;"><label>连接器名称</label></th>
				          <th style="text-align: center;"><label>是否禁用</label></th>
				        </tr>
				       <tbody>
					        <c:forEach items="${hubsDtos }" var="hubsDto">
					         <tr id="hubsListTr${hubsDto.hubsCode }" onclick="selectHubs('${hubsDto.hubsCode }')">
					             <td>${hubsDto.hubsCode }</td>
					             <td>${hubsDto.hubsName}</td>
					             <td>
					             	<c:if test="${hubsDto.isDisable == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
					             	<c:if test="${hubsDto.isDisable == false}"> <input type="checkbox" disabled="disabled"/></c:if>
					             </td>
					         </tr>
					       </c:forEach>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- 右侧详细面板 -->
		<div id="rightDetail" class="col-md-8 animated fadeInRight">
			<jsp:include page="hubsRight.jsp"></jsp:include>
		</div>
	</div>
</div>
<div id="modelHubsDiv"></div>
<script type="text/javascript" src="webpage/hotel/hubs/hubs.js?version=${globalVersion}"></script>