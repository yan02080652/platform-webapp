<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="mode_hubs" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            <c:choose>
            	<c:when test="${ empty hubsDto.hubsCode}">
            		<h4 class="modal-title">新增连接器</h4>
            	</c:when>
            	<c:otherwise>
            		<h4 class="modal-title">修改连接器</h4>
            	</c:otherwise>
            </c:choose>
         </div>
         <div class="modal-body">
         <form modelAttribute="hubsDto" class="form-horizontal" id="detailForm" role="form">
               <input type="hidden" name="editMode" id="editMode" value="${editMode}"/>
			   <div class="form-group">
		     	  <label for="hubsCode" class="col-sm-3 control-label">连接器编码</label>
		     	  <div class="col-sm-9">
		         	 <input type="text" class="form-control" id="hubsCode" name="hubsCode" value="${hubsDto.hubsCode}" placeholder="编码"/>
		     	  </div>
		  	   </div>
			   <div class="form-group">
			      <label for="hubsName" class="col-sm-3 control-label">连接器名称</label>
			      <div class="col-sm-9">
			         <input type="text" class="form-control" name="hubsName" value="${hubsDto.hubsName}" placeholder="名称"/>
			      </div>
			   </div>
		  	   <div class="form-group">
			      <label for="isDisable" class="col-sm-3 control-label">查询接口</label>
				  <div class="col-sm-9">
				     <label class="radio-inline"><input  type="radio" value="1" name="isSupportSearch" />支持</label>
				     <lable class="radio-inline"><input  type="radio" value="0" checked="checked" name="isSupportSearch"/>不支持</lable>
				  </div>
			   </div>
			   <div class="form-group">
			      <label for="isDisable" class="col-sm-3 control-label">出票接口</label>
				  <div class="col-sm-9">
				     <label class="radio-inline"><input  type="radio" value="1" name="isSupportDraw" />支持</label>
				     <lable class="radio-inline"><input  type="radio" value="0" checked="checked" name="isSupportDraw"/>不支持</lable>
				  </div>
			   </div>
			    <div class="form-group">
			      <label for="isDisable" class="col-sm-3 control-label">取消接口</label>
				  <div class="col-sm-9">
				     <label class="radio-inline"><input  type="radio" value="1" name="isSupportCancel" />支持</label>
				     <lable class="radio-inline"><input  type="radio" value="0" checked="checked" name="isSupportCancel"/>不支持</lable>
				  </div>
			   </div>
			      <div class="form-group">
			      <label for="isDisable" class="col-sm-3 control-label">区域类型</label>
				  <div class="col-sm-9">
				     <label class="radio-inline"><input  type="radio" value="1" name="areaType" />国内</label>
				     <lable class="radio-inline"><input  type="radio" value="2" name="areaType"/>国际</lable>
				  </div>
			   </div>
			   <div class="form-group">
			      <label for="isDisable" class="col-sm-3 control-label">是否禁用</label>
				  <div class="col-sm-9">
				     <label class="radio-inline"><input  type="radio" value="1" name="isDisable" />是</label>
				     <lable class="radio-inline"><input  type="radio" value="0" checked="checked" name="isDisable"/>否</lable>
				  </div>
			   </div>
			  
				<div class="form-group">
			      <label for="description" class="col-sm-3 control-label">描述</label>
			      <div class="col-sm-9">
			         <textarea class="form-control" name="description" rows="3" placeholder="定义">${hubsDto.description} </textarea>
			      </div>
			   </div>
			</form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            <button type="button" class="btn btn-primary" onclick="submitHubs()">保存</button>
         </div>
      </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		var hubsCode = "${hubsDto.hubsCode}";
		if(hubsCode!=null && hubsCode!=''){//修改
			$('#hubsCode').attr('readonly',true);
			if('${hubsDto.isDisable}'=='false'){
				$("input:radio[value=0][name=isDisable]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isDisable]").attr('checked','checked');
			}
			if('${hubsDto.isSupportSearch}'=='false'){
				$("input:radio[value=0][name=isSupportSearch]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isSupportSearch]").attr('checked','checked');
			}
			if('${hubsDto.isSupportDraw}'=='false'){
				$("input:radio[value=0][name=isSupportDraw]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isSupportDraw]").attr('checked','checked');
			}
			if('${hubsDto.isSupportCancel}'=='false'){
				$("input:radio[value=0][name=isSupportCancel]").attr('checked','checked');
			}else{
				$("input:radio[value=1][name=isSupportCancel]").attr('checked','checked');
			}
			if('${hubsDto.areaType}'=='1'){
				$("input:radio[value=1][name=areaType]").attr('checked','checked');
			}else{
				$("input:radio[value=2][name=areaType]").attr('checked','checked');
			}
		}
	});
</script>
