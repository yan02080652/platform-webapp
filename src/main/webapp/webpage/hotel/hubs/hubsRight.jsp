<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
 <style>
 #trans-detail table{
 	margin-left: 15px;
 }
 #trans-detail table th{
 	font-size: 14px;
 	text-align: right;
 }
 
</style>
<c:if test="${not empty hubsDto}">
	<div class="panel panel-default">
		<div class="panel-heading">
			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>连接器详细信息&nbsp;&nbsp;
		</div>
		<div class="panel-body">
			<div id="trans-detail">
				<table>
					<tr>
						<th width="85px">连接器编码:</th>
						<td width="10%">${hubsDto.hubsCode }</td>
						<th width="85px">名称:</th>
						<td width="10%">${hubsDto.hubsName }</td>
					    <th width="85px">是否禁用:</th>
						<td width="10%"><c:if test="${hubsDto.isDisable == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
					        <c:if test="${hubsDto.isDisable == false}"> <input type="checkbox" disabled="disabled"/></c:if>
						</td>
					</tr>
					<tr>
						<th>详细说明:</th>
						<td colspan="7">
							${hubsDto.description}
						</td>
					</tr>
					<tr>
						<th width="85px">查询接口:</th>
						<td width="10%">
							<c:if test="${hubsDto.isSupportSearch == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
					        <c:if test="${hubsDto.isSupportSearch == false}"> <input type="checkbox" disabled="disabled"/></c:if>
						</td>
						<th width="85px">出票接口:</th>
						<td width="10%">
							<c:if test="${hubsDto.isSupportDraw == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
					        <c:if test="${hubsDto.isSupportDraw == false}"> <input type="checkbox" disabled="disabled"/></c:if>
						</td>
						<th width="85px">取消接口:</th>
						<td width="10%">
							<c:if test="${hubsDto.isSupportCancel == true}"> <input type="checkbox" disabled="disabled" checked="checked"/></c:if>
					        <c:if test="${hubsDto.isSupportCancel == false}"> <input type="checkbox" disabled="disabled"/></c:if>
						</td>
						<th width="85px">区域类型:</th>
						<td width="10%">
							<c:if test="${hubsDto.areaType == '1'}"> 国内</c:if>
					        <c:if test="${hubsDto.areaType == '2'}"> 国际</c:if>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="panel panel-default" id="authtypeDiv">
		<div class="panel-heading">
			<span class="glyphicon glyphicon-home" aria-hidden="true"></span>所支持的连接&nbsp;&nbsp;
			<button class="btn btn-default" onclick="addConnect('${hubsDto.hubsCode }')" role="button">添加</button>
		</div>
		<div class="panel-body">
			<div class="col-sm-12">
				<table class="table  table-bordered table-hover table-condensed dataTables-example dataTable">
					<thead>
					<tr>
						<th class="sort-column">连接名称</th>
						<th class="sort-column">商户名称</th>
						<th class="sort-column">所属tmc</th>
						<th class="sort-column">出票渠道</th>
						<th class="sort-column">联系人</th>
						<th class="sort-column">禁用</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>
					<c:forEach items="${hubsConnectDtos }" var="hubsConnectDto">
						<tr>
							<td>${hubsConnectDto.hubsConnectName }</td>
							<td>${hubsConnectDto.merchantName }</td>
							<td>${hubsConnectDto.tmcId }</td>
							<td>${hubsConnectDto.channelCode }</td>
							<td>${hubsConnectDto.contactName }</td>
							<td>
								<input type="checkbox" disabled="disabled" ${hubsConnectDto.isDisable ? 'checked="checked"' : ''}/>
							</td>
							<td>
								<c:if test="${hubsDto.isDisable == false}">
									<a onclick="editConnect('${hubsConnectDto.id }')">修改</a>&nbsp;
									<a onclick="deleteConnect('${hubsConnectDto.id }')">删除</a>&nbsp;
								</c:if>
							</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
</c:if>

<div id="modelHubsConnectDiv"></div>
