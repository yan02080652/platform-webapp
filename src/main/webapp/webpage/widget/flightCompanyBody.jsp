<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table class="table table-hover" style="margin-top: 5px;">
	<thead>
		<tr>
			<th>航司编码</th>
			<th>航司名称</th>
			<th>英文名称</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pageList.list }" var="company">
			<tr class="odd gradeX" data-name="${company.nameCn}" data-short="${company.nameShort}" data-code="${company.code }">
				<td>${company.code}</td>
				<td>${company.nameCn}</td>
				<td>${company.nameEn}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<%@ include file="/webpage/common/wgAjaxPage.jsp"%>
