<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table class="table table-hover" style="margin-top: 5px;">
	<thead>
		<tr>
			<th>名称</th>
			<th>域名</th>
			<c:if test="${showType}">
				<th>类型</th>
			</c:if>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pageList.list }" var="partner">
			<tr class="odd gradeX" data-id="${partner.id}" data-name="${partner.name}" data-code="${partner.code }">
				<td>${partner.name}</td>
				<td>${partner.mailDomain}</td>
				<c:if test="${showType}">
					<td>${partner.type eq 1?'合作伙伴':'客户'}</td>
				</c:if>
			</tr>
		</c:forEach>
	</tbody>
</table>
<%@ include file="/webpage/common/ajaxpagination.jsp"%>
