<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row" style="min-height: 300px;">
	<div class="col-md-8" style="padding-right: 5px;">
		<div class="col-lg-12">
			<div class="input-group">
				<input type="text" class="form-control dropdown-org orgInput"
					placeholder="用户名/职位">
				<span class="input-group-btn" style="padding-right:13px;">
					<button class="btn btn-default" type="button"
						id="trWg_btnSearch_users">
						<i class="fa fa-search" aria-hidden="true"></i>
					</button>
					<button class="btn btn-default" type="button"
						onclick="platform.users._selectDropdownOrg(this)" style="border-top-right-radius:3px;border-bottom-right-radius:3px;">
						<div class="overflow_text" style="max-width: 150px;float: left;">${empty orgName?'单位':orgName}</div>&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i>
					</button> <input type="hidden" id="trWg_input_orgs" value="${orgId }"/>
				</span>
			</div>
			<!-- /input-group -->
			<div id="trWg_table_users" style="margin-top: 5px;">
			</div>
		</div>
	</div>
	<div class="col-md-1"
		style="padding-top: 140px; padding-left: 0px; padding-right: 0px;">
		<button type="button" class="btn btn-default fa fa-arrow-right"
			onclick="platform.users.addSelectedTr()"></button>
	</div>
	<div class="col-md-3" style="padding-left: 0px;">
		<h5>
			<b>已选列表</b>
		</h5>
		<ul class="list-group selected">
			<!-- <li class="list-group-item">免费域名注册<span class="glyphicon glyphicon-remove"></span></li> -->
		</ul>
	</div>
</div>
