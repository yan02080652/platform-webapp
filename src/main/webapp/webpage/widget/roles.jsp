<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<div class="row" style="min-height: 300px;">
	<div class="col-md-5">
		<ul id="tr_treeRoles" class="ztree"></ul>
	</div>
	<div class="col-md-2" style="padding-top: 140px;">
		<button type="button" class="btn btn-default fa fa-arrow-right" onclick="platform.roles.addSelectedNodes()"></button>
	</div>
	<div class="col-md-5">
		<h5>
			<b>已选列表</b>
		</h5>
		<ul class="list-group selected">
			<!-- <li class="list-group-item">免费域名注册<span class="glyphicon glyphicon-remove"></span></li> -->
		</ul>
	</div>
</div>
