<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row" style="min-height: 300px;">
	<div class="col-lg-12">
		<div class="input-group">
			<input type="text" class="form-control dropdown-org orgInput"
				placeholder="用户名/职位">
			<span class="input-group-btn" style="padding-right:13px;">
				<button class="btn btn-default" type="button"
					id="trWg_btnSearch_user">
					<i class="fa fa-search" aria-hidden="true"></i>
				</button>
				<button class="btn btn-default" type="button"
					onclick="platform.user._selectDropdownOrg(this)" style="border-top-right-radius:3px;border-bottom-right-radius:3px;">
					<div class="overflow_text" style="max-width: 150px;float: left;">${empty orgName?'单位':orgName}</div>&nbsp;<i
						class="fa fa-caret-down" aria-hidden="true"></i>
				</button> <input type="hidden" id="trWg_input_org" value="${orgId }" />
			</span>
		</div>
		<!-- /input-group -->
		<div id="trWg_table_user" style="margin-top: 5px;"></div>
	</div>
</div>