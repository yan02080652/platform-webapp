<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="staff-select" id="sel-partnerAndUser">
    <div class="staff-header">
        <div class="staff-search">
            <input type="text" autocomplete="off" placeholder="搜索人员姓名" class="search-user">
            <span onclick="resetSearchName()">取消</span>
        </div>
    </div>
    <div class="staff-body">
        <ul class="body-title choose-ul">
            <li>
                <span>企业</span>
                <span class="check-all" onclick="chooseAllPartner()">全选</span>
                <span class="check-all" onclick="unchooseAllPartner()">全不选</span>
            </li>
            <li>
                <span>部门</span>
                <span class="check-all" onclick="chooseAllOrg()">全选</span>
                <span class="check-all" onclick="unchooseAllOrg()">全不选</span>
            </li>
            <li>
                <span>人员</span>
                <span class="check-all" onclick="chooseAllUser()">全选</span>
                <span class="check-all" onclick="unchooseAllUser()">全不选</span>
            </li>
        </ul>
        <ul class="body-info choose-ul">
            <li>
                <ul class="sel-partner">
                    <li>企业有限公司1</li>
                </ul>
            </li>
            <li class="sel-org">
                <ul id="tree-org" class="ztree"></ul>
            </li>
            <li class="sel-user">
                <ul>
                    <%--<li>企业有限公司3</li>--%>
                </ul>
            </li>
        </ul>
        <ul class="search-list" style="display: none;">
            <%--<li>
                <span>企业F有限公司</span>
                <span>技术部</span>
                <span>SB</span>
            </li>--%>
        </ul>
    </div>
    <div class="staff-selected">
        <h2>已选</h2>
        <div class="main-container">
            <ul>
                <%--<li>
                    <button type="button">企业有限公司</button>
                    <span>X</span>
                </li>--%>
            </ul>
        </div>
    </div>
    <div class="staff-footer">
        <button type="button" onclick="_surePartnerAndUser()">确认</button>
        <button type="button" onclick="_closePartnerAndUser()">取消</button>
    </div>
</div>
<style>
    .staff-select {
        width: 600px;
        background: #f5f8f9;
        padding: 15px;
        color: #666;
        display: none;
        height: 100%;
    }

    .staff-header h2 {
        margin: 0;
        font-size: 16px;
        margin-bottom: 15px;
    }

    .staff-header .staff-search {
        position: relative;
    }

    .staff-header .staff-search input {
        width: 100%;
        height: 34px;
        border: 1px #8fbecf solid;
        padding: 0 12px;
        box-sizing: border-box;
    }

    .staff-header .staff-search span {
        position: absolute;
        right: 15px;
        top: 50%;
        transform: translateY(-50%);
        font-size: 12px;
        color: #8fbecf;
    }

    .staff-body .body-title {
        width: 100%;
        font-size: 12px;
        display: flex;
        justify-content: space-between;
        align-items: center;
        margin-bottom: 8px;
    }

    .staff-body .body-title li {
        width: 30%;
        margin-right: 3%;
    }

    .staff-body .body-title > li:last-child {
        margin-right: 0;
    }

    .staff-body .body-title li span:first-child {
        font-weight: 600;
        padding-right: 10px;
    }

    .staff-body .body-title .check-all {
        color: #6db7df;
        cursor: pointer;
    }

    .staff-body{
        height: 280px;
        padding-top: 8px;
    }

    .staff-body .body-info {
        height: 256px;
        display: flex;
        font-size: 12px;
    }

    .staff-body .body-info > li {
        width: 32%;
        background: #fff;
        margin-right: 3%;
        overflow-y: auto;
        overflow-x: hidden;
    }

    .staff-body .body-info > li:last-child {
        margin-right: 0;
    }

    .staff-body .sel-partner li,.staff-body .sel-user li {
        padding: 5px 10px;
        height: 34px;
        line-height: 24px;
        cursor: pointer;
    }

    .staff-body .sel-partner > li.active{
        background-color: #EEF7F9;
    }

    .staff-body .cb-partner,.staff-body .cb-user {
        vertical-align: sub;
        margin-right: 3px;
    }

    .staff-body .search-list {
        height: 100%;
        background: #fff;
        margin-bottom: 0px;
        margin-top: 8px
    }

    .staff-body .search-list li {
        width: 100%;
        display: flex;
        font-size: 12px;
        padding: 5px 10px;
        line-height: 30px;
    }

    .staff-body .search-list li:hover {
        background: #eff7f9;
    }

    .staff-body .search-list li span {
         display: inline-block;
         width: 40%;
         margin-right: 3%;
     }

    .staff-body .search-list li span:last-child {
        width: 20%;
    }

    .staff-body .search-list li span:last-child {
        margin-right: 0;
    }

    .staff-selected h2 {
        font-size: 12px;
        margin-bottom: 7px;
        font-weight: bold;
    }

    .staff-selected .main-container {
        background: #fff;
        padding: 5px;
        font-size: 12px;
        min-height: 66px;
        max-height: 100px;
        overflow: auto;
    }

    .staff-selected .main-container ul {
        margin-bottom: 0 !important;
    }

    .staff-selected .main-container ul li {
        position: relative;
        display: inline-block;
        margin-right: 8px;
        padding: 3px;
    }

    .staff-selected .main-container ul li:last-child {
        margin-right: 0;
    }

    .staff-selected .main-container ul li button {
        padding: 0 30px 0 5px;
        color: #fff;
        line-height: 22px;
        background: #52a8da;
        outline: none;
        border: none;
    }

    .staff-selected .main-container ul li span {
        position: absolute;
        right: 10px;
        top: 50%;
        transform: translateY(-50%);
        color: #fff;
    }

    .staff-footer {
        bottom: 8px;
        position: absolute;
        right: 20px
    }

    .staff-footer button {
        margin-left: 10px;
        padding: 5px 20px;
        outline: none;
        border: none;
    }

    .staff-footer button:last-child {
        color: #78bbdf;
        border: 1px #78bbdf solid;
    }

    .staff-footer button:first-child {
        background: #5ca0b8;
        color: #fff;
    }

    .staff-select .hide {
        display: none;
    }
</style>
<script>
    /**
     * 只支持单例
     * config 配置说明
     *  partnerIds 可选企业列表,多个以逗号,分隔
     *  selected 已选择企业或员工，格式 {partner:[{id:1,name:'在途网络科技有限公司'},{id:2}],org:[{id:1436,partnerId:1,name:'技术部'}],user:[{id:739,name:'发如雪'},{id:733}]}
     *                           注意：格式只有id是必需的，其它信息如name、partnerId可以不提供
     *  _forceRefresh:是否强制刷新页面
     */
    $(function () {
        var pa = {
            partnerInfo: {},
            orgDataCache: {},
            userDataCache: {}
        };

        //初始化
        pa.init = function (config) {
            config = config || {};

            pa.obj = $('#sel-partnerAndUser');

            var partnerIds = config.partnerIds ? config.partnerIds.split(',') : [];
            var selected = config.selected || {};
            var partnerSelected = selected.partner;
            if (partnerSelected && partnerSelected.length > 0) {
                partnerSelected.forEach(function (p) {
                    if (p.id && !p.name) {
                        partnerIds.push(p.id);
                    }
                });
            }

            var orgIds = [];
            var orgSelected = selected.org;
            if (orgSelected && orgSelected.length > 0) {
                orgSelected.forEach(function (o) {
                    if (o.id) {
                        if (!o.name) {
                            orgIds.push(o.id);
                        }
                        if (o.partnerId && !o.partnerName) {
                            partnerIds.push(o.partnerId);
                        }
                    }
                });
            }

            var userIds = [];
            var userSeleceted = selected.user;
            if (userSeleceted && userSeleceted.length > 0) {
                userSeleceted.forEach(function (user) {
                    if (user.id && !user.name) {
                        userIds.push(user.id);
                    }
                });
            }
            var cur = this;
            $.get('widget/partner/getPartnerAndChooseInfo.json', {
                partnerIds: partnerIds.join(','),
                orgIds: orgIds.join(','),
                userIds: userIds.join(',')
            }, function (data) {
                cur.initPartners(config.partnerIds, data.partnerInfo);
                cur.initSelected(selected, data);
                cur.bindEvent();
                pa.showCurPartner();
            });

            var inputUser = $('.search-user', this.obj);
            inputUser.val(null);
            inputUser.unbind('keyup').keyup(function () {
                pa.searchUser($(this).val(),config.partnerIds);
            });
        };

        pa.resetSearchName = function () {
            $('.search-user', this.obj).val(null);
            pa._searchUser();
        };

        //搜索用户
        pa.searchUser = function(name,partnerIds){
            var flagMark = setTimeout(function () {
                if(pa.flagSearchMark == flagMark){
                    pa._searchUser(name,partnerIds);
                }
            },500);
            pa.flagSearchMark = flagMark;
        }

        pa._searchUser = function (name, partnerIds) {
            name = name ? name.trim() : null;
            if (name) {
                this.switchSearchMode();
                var loadIndex = layer.load();
                $.get('widget/user/searchUser.json', {
                    partnerIds: partnerIds || null,
                    name: name
                }, function (data) {
                    layer.close(loadIndex);
                    pa.initSeachUserDom(data);
                });
            } else {
                this.switchChooseMode();
            }
        };

        //切换至搜索用户模式
        pa.switchSearchMode = function(){
            $('.choose-ul', this.obj).hide();
            $('.search-list', this.obj).show();
        }

        //切换至选择模式
        pa.switchChooseMode = function(){
            $('.choose-ul', this.obj).show();
            $('.search-list', this.obj).hide();
        }

        pa.initSeachUserDom = function(data){
            var ul = $('.search-list',this.obj);
            ul.empty();
            if(data.users){
                var orgNames = data.orgNames || {};
                var partnerNames = data.partnerNames || {};
                data.users.forEach(function (user) {
                    var li = $("<li/>").data('id', user.id).data('name', user.fullname).data('orgid', user.orgId).data('partnerid', user.partnerId);
                    var partnerName = partnerNames[user.partnerId] || '-';
                    var orgName = orgNames[user.orgId] || '-';
                    li.append($('<span/>').text(partnerName));
                    li.append($('<span/>').text(orgName));
                    li.append($('<span/>').text(user.fullname));
                    ul.append(li);
                });

                $('li', ul).dblclick(function () {
                    var d = $(this);
                    pa.select({
                        id:d.data('id'),
                        name:d.data('name'),
                        orgId:d.data('orgid'),
                        partnerId:d.data('partnerid')
                    },'user');
                });
            }
        }

        //初始化左边可选择的企业
        pa.initPartners = function (partnerIds, partnerInfo) {
            if (partnerIds) {
                this.partnerInfo = partnerInfo || {};
                var pids = partnerIds.split(',');
                var selPartner = $('.sel-partner', this.obj);
                selPartner.empty();
                pids.forEach(function (id) {
                    if (partnerInfo[id]) {
                        var name = partnerInfo[id].name;
                        var dom = $('<li class="overflow_text"/>').data('id', id).data('name', name)
                            .append($("<input type='checkbox' class='cb-partner'>")).append(name);
                        selPartner.append(dom);
                    }
                });
            }
            pa.showCurPartner();
        };

        //初始化已选择的企业、组织、用户
        pa.initSelected = function (selected, data) {
            $('.staff-selected ul', this.obj).empty();
            var partnerInfo = data.partnerInfo;
            var orgInfo = data.orgInfo;
            var userInfo = data.userInfo;
            var partner = selected.partner;
            var org = selected.org;
            var user = selected.user;
            partner.forEach(function (p) {
                pa.select({
                    id: p.id,
                    name: p.name || partnerInfo[p.id].name
                }, 'partner', true);
            });
            org.forEach(function (o) {
                var lo = orgInfo[o.id] || {};
                var opartnerId = o.partnerId || lo.partnerId;
                var opartnerInfo = opartnerId ? partnerInfo[opartnerId] : null;
                pa.select({
                    id: o.id,
                    name: o.name || lo.name,
                    partnerId: opartnerId,
                    partnerName: opartnerInfo ? opartnerInfo.name : null
                }, 'org', true);
            })
            user.forEach(function (u) {
                var uinfo = userInfo[u.id] || {};
                pa.select({
                    id: u.id,
                    name: u.name || uinfo.fullname
                }, 'user');
            });
        };

        //绑定事件
        pa.bindEvent = function () {
            $('.sel-partner .active',this.obj).removeClass('active');

            $('.sel-partner li', this.obj).click(function () {
                $('.sel-partner .active', this.obj).removeClass('active');
                $(this).addClass('active');
                pa.showCurPartner();
            });

            $('.sel-partner .cb-partner', this.obj).change(function () {
                var li = $(this).parent();
                if ($(this).prop('checked')) {
                    pa.select({
                        id: li.data('id'),
                        name: li.data('name')
                    }, 'partner');

                    li.removeClass('active');
                } else {
                    pa.remove(li.data('id'), 'partner')
                }
                pa.showCurPartner();
            });
        };

        //选中部门并显示这个部门的组织架构
        pa.showCurPartner = function () {

            var curPartnerDom = $('.sel-partner .active');
            var curPartnerId = null;
            if (curPartnerDom.length > 0) {
                if (!$(".cb-partner", curPartnerDom).prop('checked')) {
                    curPartnerId = curPartnerDom.data('id');
                }
            }
            if (curPartnerId) {
                pa.loadOrgs(curPartnerId);
                $('.sel-org', this.obj).show();
            } else {
                $('.sel-org', this.obj).hide();
                $('.sel-user', this.obj).hide();
            }
        };

        //全选/全不选企业
        pa.chooseAllPartner = function (checked) {
            var partnerDoms = $('.sel-partner', this.obj).children();
            $.each(partnerDoms, function () {
                $('.cb-partner', this).prop('checked', checked);
                if (checked) {
                    pa.select({
                        id: $(this).data('id'),
                        name: $(this).data('name')
                    }, 'partner');
                } else {
                    pa.remove($(this).data('id'), 'partner');
                }
            });
            pa.showCurPartner();
        };

        //加载组织信息
        pa.loadOrgs = function (partnerId) {
            var treeOrg = $("#tree-org");
            if (treeOrg.data('partnerid') == partnerId) {
                return;
            }
            treeOrg.data('partnerid', partnerId);
            var orgData = this.orgDataCache[partnerId];
            if (orgData) {
                pa.initOrgTree(orgData);
            } else {
                var index = layer.load();
                $.get('widget/org/getAllOrgs.aj', {
                    partnerId: partnerId
                }, function (data) {
                    layer.close(index);
                    var treeData = TR.turnToZtreeData(data);
                    pa.orgDataCache[partnerId] = treeData;
                    pa.initOrgTree(treeData);
                });
            }
        };

        //初始化树
        pa.initOrgTree = function (data) {
            var setting = {
                view: {
                    selectedMulti: true,
                    showIcon: true,
                    showLine: true,
                    dblClickExpand: false
                },
                check: {
                    enable: true,
                    chkboxType: {"Y": "", "N": ""}
                },
                callback: {
                    onDblClick: function () {
                    },
                    onCheck: function (event, treeId, treeNode) {
                        pa.checkNode(treeNode);
                    },
                    onClick: function (event, treeId, treeNode) {
                        pa.showUsers(treeNode);
                    }
                }
            };
            pa.treeOrg = $.fn.zTree.init($("#tree-org"), setting, data);
            var seled = pa.getSelected();
            seled.org.forEach(function (org) {
                pa.syncTopSelected(org.id, 'org', true);
            });
        };

        //选择组织上一个节点
        pa.checkNode = function (node) {
            var data = node.data;
            if (node.checked) {
                var partner = pa.partnerInfo[data.partnerId];
                this.select({
                    id: data.id,
                    name: data.name,
                    partnerId: data.partnerId,
                    partnerName: partner ? partner.name : null
                }, 'org');
            } else {
                this.remove(data.id, 'org');
            }
        };

        //全选/全不选组织
        pa.chooseAllOrg = function (checked) {
            if (this.treeOrg) {
                this.treeOrg.checkAllNodes(checked);
                var nodes = this.treeOrg.getNodes();
                this.treeOrg.transformToArray(nodes).forEach(function (node) {
                    pa.checkNode(node);
                });

                if(checked){
                    $('.sel-user', this.obj).hide();
                }
            }
        }

        //显示用户
        pa.showUsers = function (node) {
            if (node.checked) {
                $('.sel-user', this.obj).hide();
            } else {
                pa.loadUsers(node.id);
                $('.sel-user', this.obj).show();
            }
        };

        //加载用户
        pa.loadUsers = function (orgId) {
            var cur = this;
            if (cur.userDataCache[orgId]) {
                cur.initUserDom(orgId);
            } else {
                $.get('widget/user/searchUser.json', {
                    orgId: orgId
                }, function (data) {
                    cur.userDataCache[orgId] = data.users || [];
                    cur.initUserDom(orgId);
                });
            }
        };

        //初始化右侧用户元素
        pa.initUserDom = function (orgId) {
            var parentDom = $('.sel-user', this.obj);
            /*if (parentDom.data('orgid') == orgId) {
                return;
            }*/
            parentDom.data('orgid', orgId);
            parentDom.empty();
            var users = this.userDataCache[orgId];
            if (users) {
                var seledData = this.getSelected();
                var checkedUser = {};
                seledData.user.forEach(function (u) {
                    checkedUser[u.id] = true;
                });
                users.forEach(function (user) {
                    var name = user.fullname;
                    var dom = $('<li class="overflow_text"/>').data('id', user.id).data('name', name).data('orgid', orgId).data('partnerid',user.partnerId);
                    var cb = $("<input type='checkbox' class='cb-user'>");
                    if(checkedUser[user.id]){
                        cb.prop('checked', true);
                    }
                    dom.append(cb).append(name);
                    parentDom.append(dom);
                });

                $('.cb-user', parentDom).change(function () {
                    var li = $(this).parent();
                    if ($(this).prop('checked')) {
                        pa.select({
                            id: li.data('id'),
                            name: li.data('name'),
                            orgId:li.data('orgid'),
                            partnerId:li.data('partnerid')
                        }, 'user');
                    } else {
                        pa.remove(li.data('id'), 'user')
                    }
                    pa.showCurPartner();
                });
            }
        };

        //全选/全不选用户
        pa.chooseAllUser = function (checked) {
            var partnerDoms = $('.sel-user', this.obj).children();
            $.each(partnerDoms, function () {
                $('.cb-user', this).prop('checked', checked);
                if (checked) {
                    pa.select({
                        id: $(this).data('id'),
                        name: $(this).data('name'),
                        orgId:$(this).data('orgid'),
                        partnerId:$(this).data('partnerid')
                    }, 'user');
                } else {
                    pa.remove($(this).data('id'), 'user');
                }
            });
        }

        //查找一个已选择的数据
        pa.findSelect = function (id, type) {
            var ul = $('.staff-selected ul', this.obj);
            var rs = null;
            $.each(ul.children(), function () {
                if ($(this).data('id') == id && $(this).data('type') == type) {
                    rs = this;
                    return false;
                }
            });
            return rs;
        };

        //选择一个数据
        pa.select = function (obj, type, syncTop) {
            if (obj && obj.id) {
                var sel = pa.findSelect(obj.id, type);
                if (!sel) {
                    var lastClassKey = 'last-seled-' + type;
                    var name = obj.name;
                    if (type == 'org' && obj.partnerName) {
                        name = obj.partnerName + '-' + name;
                    }
                    var li = $("<li/>").data('id', obj.id).data('type', type).data('name', obj.name);
                    if (type == 'org') {
                        li.data('partnerid', obj.partnerId || '');
                        li.data('partnername', obj.partnerName || '');
                    } else if (type == 'user') {
                        li.data('partnerid', obj.partnerId || '');
                        li.data('partnername', obj.partnerName || '');
                        li.data('orgid', obj.orgId || '');
                        li.data('orgname', obj.orgName || '');
                    }
                    var dom = li.append($("<button/>").text(name));
                    var delDom = $("<span class='seled-del'>X</span>");
                    delDom.click(function () {
                        var d_parent = $(this).parent();
                        pa.remove(d_parent.data('id'), d_parent.data('type'));
                    });
                    dom.append(delDom);

                    var ul = $('.staff-selected ul', this.obj);
                    var before = $('.' + lastClassKey, ul);//插在哪个元素的后面
                    if (before.length > 0) {
                        dom.insertAfter(before);
                    } else {
                        var after = null;
                        if (type == 'partner') {
                            after = $('last-seled-org', ul);
                            if (after.length == 0) {
                                after = $('last-seled-user', ul);
                            }
                        } else if (type == 'org') {
                            after = $('last-seled-user', ul);
                        }

                        if (after && after.length > 0) {
                            dom.insertBefore(after);
                        } else {
                            ul.append(dom);
                        }
                    }
                    $('.' + lastClassKey, ul).removeClass(lastClassKey);
                    dom.addClass(lastClassKey)

                    //同步上面数据
                    if (syncTop) {
                        this.syncTopSelected(obj.id, type, true);
                    }

                    var seledData = this.getSelected();
                    if (type == 'partner') {//清空下级已选组织和员工
                        seledData.org.forEach(function (o) {
                            if (o.partnerId == obj.id) {
                                pa.remove(o.id, 'org');
                            }
                        });
                        seledData.user.forEach(function (u) {
                            if (u.partnerId == obj.id) {
                                pa.remove(o.id, 'user');
                            }
                        });
                    } else if (type == 'org') {//清空下级已选员工
                        seledData.user.forEach(function (u) {
                            if (u.orgId == obj.id) {
                                pa.remove(u.id, 'user');
                            }
                        });
                    }
                }
            }
        };

        //同步上面选择的数据
        pa.syncTopSelected = function (id, type, checked) {
            if (type == 'partner') {
                var selPartner = $('.sel-partner', this.obj).children();
                $.each(selPartner, function () {
                    if ($(this).data('id') == id) {
                        $('.cb-partner', this).prop('checked', checked);
                    }
                });
            } else if (type == 'org') {
                if (this.treeOrg) {
                    var node = this.treeOrg.getNodeByParam('id', id);
                    if (node) {
                        this.treeOrg.checkNode(node, checked);
                    }
                }
            }else if(type == 'user'){
                var selPartner = $('.sel-user', this.obj).children();
                $.each(selPartner, function () {
                    if ($(this).data('id') == id) {
                        $('.cb-user', this).prop('checked', checked);
                    }
                });
            }
        };

        //根据id删除选中数据
        pa.remove = function (id, type) {
            var dom = $(pa.findSelect(id, type));
            var lastClassKey = 'last-seled-' + type;
            if (dom.hasClass(lastClassKey)) {
                var prev = $(dom.prev());
                if (prev.length > 0 && prev.data('type') == dom.data('type')) {
                    prev.addClass(lastClassKey);
                }
            }
            this.syncTopSelected(id, type, false);
            dom.remove();
        };

        //获取已选择的数据
        pa.getSelected = function () {
            var seled = $('.staff-selected ul', this.obj).children();
            var partner = [];
            var org = [];
            var user = [];
            $.each(seled, function () {
                var el = $(this);
                var type = el.data('type');
                var d = {
                    id: el.data('id'),
                    name: el.data('name')
                };
                if ('org' == type) {
                    d.partnerId = el.data('partnerid');
                    d.partnerName = el.data('partnername');
                }else if('user' == type){
                    d.orgId = el.data('orgid');
                    d.partnerId = el.data('partnerid');
                }
                if (type == 'partner') {
                    partner.push(d);
                } else if (type == 'org') {
                    org.push(d);
                } else if (type == 'user') {
                    user.push(d);
                }
            });

            return {
                partner: partner,
                org: org,
                user: user
            }
        };

        //显示选择对话框
        pa.show = function (config, callback) {
            this.init(config);
            pa.curDlgIndx = layer.open({
                type: 1,
                title: '导入企业人员',
                content: $('#sel-partnerAndUser'),
                area: ['600px', '580px'],
            });
            pa.curDlgCallback = callback;
        };

        //关闭对话框
        pa.close = function () {
            layer.close(pa.curDlgIndx);
        };

        if (TR) {
            TR.partnerAndUser = pa;
        } else {
            console.error('TR js need should be load first!');
        }
    });

    //确定
    function _surePartnerAndUser() {
        var pa = TR.partnerAndUser;
        if (pa.curDlgCallback) {
            pa.close();
            var seleted = pa.getSelected();
            pa.curDlgCallback(seleted);
        }
    }

    //取消
    function _closePartnerAndUser() {
        TR.partnerAndUser.close();
    }

    //全选企业
    function chooseAllPartner() {
        TR.partnerAndUser.chooseAllPartner(true);
    }

    //全不选企业
    function unchooseAllPartner() {
        TR.partnerAndUser.chooseAllPartner(false);
    }

    //全选单位
    function chooseAllOrg() {
        TR.partnerAndUser.chooseAllOrg(true);
    }
    //全不选单位
    function unchooseAllOrg() {
        TR.partnerAndUser.chooseAllOrg(false);
    }

    //全选用户
    function chooseAllUser(){
        TR.partnerAndUser.chooseAllUser(true);
    }

    //全不选用户
    function unchooseAllUser(){
        TR.partnerAndUser.chooseAllUser(false);
    }

    function resetSearchName() {
        TR.partnerAndUser.resetSearchName();
    }
</script>