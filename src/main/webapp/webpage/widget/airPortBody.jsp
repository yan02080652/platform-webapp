<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table class="table table-hover" style="margin-top: 5px;">
	<thead>
		<tr>
			<th width="80">机场编码</th>
			<th>机场名称</th>
			<th>英文名称</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pageList.list }" var="ca">
			<tr class="odd gradeX" data-name="${ca.nameCn}" data-code="${ca.code }">
				<td>${ca.code}</td>
				<td>${ca.nameCn}</td>
				<td>${ca.nameEn}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<%@ include file="/webpage/common/wgAjaxPage.jsp"%>