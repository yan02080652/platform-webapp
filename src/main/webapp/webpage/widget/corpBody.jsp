<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table class="table table-hover" style="margin-top: 5px;">
	<thead>
		<tr>
			<th>抬头</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pageList }" var="corp">
			<tr class="odd gradeX" data-id="${corp.id}" data-name="${corp.invoiceTitle}">
				<td>${corp.invoiceTitle}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<%-- <%@ include file="/webpage/common/ajaxpagination.jsp"%> --%>
