<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table class="table table-hover" style="margin-top: 5px;">
	<thead>
		<tr>
			<th>用户名</th>
			<th>职位</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${pageList.list }" var="user">
			<tr class="odd gradeX" data-id="${user.id}" data-fullname="${user.fullname}">
				<td>${user.fullname}</td>
				<td>${user.title}</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<%@ include file="/webpage/common/ajaxpagination.jsp"%>
