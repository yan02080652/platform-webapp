<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row" style="min-height: 300px;">
	<div class="col-lg-12">
		<div class="input-group">
			<input type="text" class="form-control dropdown-flightCompant codeInput"
				placeholder="编码/名称"> <span class="input-group-btn">
				<button class="btn btn-default" type="button"
					id="trWg_btnSearch_flightCompany">
					<i class="fa fa-search" aria-hidden="true"></i>
				</button>
			</span>
		</div>
		<!-- /input-group -->
		<div id="trWg_table_flightCompany" style="margin-top: 5px;"></div>
	</div>
</div>