function chooseOrgs() {
    TR.select('orgs', {
        partnerId: 1,
        selected: [{
            id: 1,
            name: '财务部财务部'
        }, {
            id: 5,
            name: '安保处'
        }]
    }, function (result) {
        console.log(result);
    });
}

function chooseOrg() {
    TR.select('org', {
        partnerId: 1
    }, function (result) {
        console.log(result);
    });
}

function chooseRoles() {
    TR.select('roles', {
        selected: [{
            id: 1,
            name: '单位负责人'
        }, {
            id: 5,
            name: '普通员工1'
        }],
//		partnerId:1,//可选项，不传使用当前用户所使用的partner
//		sysId:'PLATFORM',//可选项，不传使用当前用户所使用的partner
    }, function (result) {
        console.log(result);
    });
}

function chooseRole() {
    TR.select('role', {
//		partnerId:1,//可选项，不传使用当前用户所使用的partner
//		sysId:'PLATFORM',//可选项，不传使用当前用户所使用的partner
    }, function (result) {
        console.log(result);
    });
}

/**
 * 选择下拉列表单位
 * @param dom
 */
function selectDropdownOrg(dom) {
    TR.select('dropdownOrg', {
        dom: dom
    }, function (data) {
        console.log(data);
        $('#orgInput').val(data.name);
    });
}

/**
 * 选择下拉列表单位
 * @param dom
 */
function selectDropdownRole(dom) {
    TR.select('dropdownRole', {
        dom: dom,
        levels: 1,
//		partnerId:1,//可选项，不传使用当前用户所使用的partner
//		sysId:'PLATFORM',//可选项，不传使用当前用户所使用的partner
    }, function (data) {
        console.log(data);
        $('#roleInput').val(data.name);
    });
}

/**
 * 用户多选
 */
function chooseUsers() {
    TR.select('users', {
        type: 'me',
        selected: [{
            id: 1,
            fullname: '张三'
        }, {
            id: 3,
            fullname: '王五'
        }]
    }, function (data) {
        console.log(data);
    });
}

/**
 * 用户单选
 */
function chooseUser() {
    TR.select('user', function (data) {
        console.log(data);
    });
}

/**
 * 选择下拉列表组织层面
 * @param dom
 */
function selectDropdownOrgGrade(dom) {
    TR.select('dropdownOrgGrade', {
        dom: dom
    }, function (data) {
        console.log(data);
        $('#orgGradeInput').val(data.name);
    });
}

/**
 * 自定义下拉列表选择
 * @param dom
 */
function selectMyDropDown(dom) {
    var data = [{
        code: 1,
        name: '自定义选择1'
    }, {
        code: 2,
        name: '自定义选择2'
    }, {
        code: 3,
        name: '自定义选择3'
    }, {
        code: 4,
        name: '自定义选择4'
    }];

    TR.select(data, {
        key: 'custom1',//多个自定义下拉选择时key值不能重复
        dom: dom,
        name: 'name',//default:name
        allowBlank: true//是否允许空白选项
    }, function (result) {
        console.log(result);
        $('#myDropDown').val(result ? result.name : null);
    });
}

/**
 * 自定义下拉列表树形数据选择
 * @param dom
 */
function selectMyDropDownTree(dom) {
    var data = [{
        code: 1,
        name: '节点1'
    }, {
        code: 2,
        name: '节点2'
    }, {
        code: 3,
        name: '节点3',
        pcode: 2
    }, {
        code: 4,
        name: '节点3',
        pcode: 2
    }, {
        code: 5,
        name: '节点5',
        pcode: 3
    }];

    TR.select(data, {
        key: 'custom2',//多个自定义下拉选择时key值不能重复
        dom: dom,
        isTree: true,
        id: 'code',//default:name
        pid: 'pcode',
        namePro: 'name',
        allowBlank: true,//是否允许空白选项
        blankText: '清空'
    }, function (result) {
        console.log(result);
        $('#myDropDownTree').val(result ? result.name : null);
    });
}

function selectMyDicDropDownTree(dom, sysId, dicCode) {

    TR.select('dic', {
        key: 'custom3',//多个自定义下拉选择时key值不能重复
        dom: dom,
        sysId: sysId,//传过去的系统编码
        dicCode: dicCode,//传过去的字典编码
        allowBlank: true,//是否允许空白选项
        blankText: '清空'
    }, function (result) {
        console.log(result);
        $('#myDicDropDownTree').val(result ? result.itemTxt : null);
    });
}


//选择企业
function choosePartner() {
    TR.select('partner', {
        type: 0//固定参数
    }, function (data) {
        console.log(data)
    });
}

//选择合作伙伴
function chooseDep() {
    TR.select('partner', {
        type: 1//固定参数
    }, function (data) {
        console.log(data)
    });
}


function selCustom() {
    TR.select2({
        id: 'dlg_relPartner',
        name: '选择客服人员',
        width: 500,
        condition: [{
            code: 'searchName',
            placeholder: '客服名称',
            enterSearch: true
        }/*,{
         code:'name',
         placeholder:'AAAAAA',
         width:300
         }*/],
        columns: [{
            property: 'userName',
            caption: '客服',
            render: function (td, data) {
                console.log(data)
                var name = data.fullName;
                td.text(name);
            }
        }, {
            property: 'flight',
            caption: '机票',
            render: function (td, data) {
                var canServer = data.serverSkill ? 1 == data.serverSkill.doFlight : false;
                if (canServer) {
                    td.text('√')
                }
            }
        }, {
            property: 'hotel',
            caption: '酒店',
            render: function (td, data) {
                var canServer = data.serverSkill ? 1 == data.serverSkill.doHotel : false;
                if (canServer) {
                    td.text('√')
                }
            }
        }, {
            property: 'train',
            caption: '火车',
            render: function (td, data) {
                var canServer = data.serverSkill ? 1 == data.serverSkill.doTrain : false;
                if (canServer) {
                    td.text('√')
                }
            }
        }, {
            property: 'insurance',
            caption: '保险',
            render: function (td, data) {
                var canServer = data.serverSkill ? 1 == data.serverSkill.doInsurance : false;
                if (canServer) {
                    td.text('√')
                }
            }
        }, {
            property: 'general',
            caption: '通用',
            render: function (td, data) {
                var canServer = data.serverSkill ? 1 == data.serverSkill.doGeneral : false;
                if (canServer) {
                    td.text('√')
                }
            }
        }],
        service: 'pss/serverPartner/searchCs',
        hasPage: true,
        forceSeach: true,
        callback: function (rs) {
            var userId = rs.userRole.userId;
            var td = $(pageVal.curBtn).parent();
            var choosed_ids = td.data('users');
            var flag = false;
            if (choosed_ids) {
                var ids = choosed_ids.split(',');
                $.each(ids, function (i, id) {
                    if (userId == id) {
                        flag = true;
                        return false;
                    }
                });
            }
            if (flag) {
                layer.msg('已经存在');
            } else {
                var partnerId = td.parent().data('partnerid');
                ajaxAdd({
                    partnerId: partnerId,
                    userId: userId
                }, function () {
                    var fullname = rs.userRole.user.fullname;
                    $("<span data-userid=" + userId + " class='user'>&nbsp;&nbsp;" + fullname + "</span><i class=\"fa fa-close\" aria-hidden=\"true\" title='移出' onclick='removeCsUser(this," + userId + ")'>").insertBefore(pageVal.curBtn);
                    if (choosed_ids) {
                        choosed_ids += "," + userId;
                    } else {
                        choosed_ids = userId + '';
                    }
                    td.data('users', choosed_ids);
                    pageVal.userSkill[userId] = rs.serverSkill || {};
                    userNameMap[userId] = fullname;
                    showTip($(pageVal.curBtn).parent());
                });
            }
        }
    });
}

function selCustom2() {
    new choose.severPartner(function (data) {
        console.log(data)
    });
}

function selCustom2Multi() {
    new choose.severPartner({
        id: 'serverMulti',
        multi: true,//是否多选
        empty: true,//是否显示清空按钮
        getChoosedData: function () {
            return [{
                userId: 1000,
                fullname: '左冷禅'
            }, {
                userId: 2000,
                fullname: '迪丽热巴'
            }];
        }
    }, function (data) {
        console.log(data)
    });
}

function selHotel() {
    new choose.hotel({
        id: 'selHotel'//给id可以防止一直新建对象
    }, function (data) {
        console.log(data)
    });
}

var choosedHotels = [];
function selHotel2() {
    new choose.hotel({
        id: 'selMultiHotel',//给id可以防止一直新建对象
        multi: true,
        empty: true,
        getChoosedData: function () {
            return choosedHotels;
        }
    }, function (data) {
        choosedHotels = data;
    });
}

//使用前请先添加<jsp:include page="../widget/partnerAndUser.jsp"/>
function selPartnerAndUser() {
    TR.partnerAndUser.show({
        partnerIds: '1395,1404,1426',
        selected: {
            partner: [{id: 1, name: '在途网络科技有限公司'}, {id: 2},{id: "1426", name: "TEM彩排环境1"}],
            org: [{id: 1436, partnerId: 1, name: '技术部'}, {id: 1438}],
            user: [{id: 739, name: '发如雪'}, {id: 733}]
        }
    },function (data) {
        console.log(data)
    });
}