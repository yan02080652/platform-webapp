<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<link rel="stylesheet" href="resource/plugins/sco/css/scojs.css?version=${globalVersion}">

<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tab.js?version=${globalVersion}"></script>
<h3>tab页切换</h3>
<button onclick="changeTab(2)">点我切换tab3</button>
<ul id="myTab" class="nav nav-tabs" data-trigger="tab">
  <li><a href="#">Tab 1</a></li>
  <li><a href="#">Tab 2</a></li>
  <li><a href="#">Tab 3</a></li>
  <li><a href="#">Tab 4</a></li>
</ul>
<div class="pane-wrapper">
  <div>tab1 content</div>
  <div>tab2 content</div>
  <div>tab3 content</div>
  <div>tab4 content</div>
</div>

<script>
function changeTab(index){
	var $tab = $.scojs_tab('#myTab');
	$tab.select(index);
}  
</script>
---------------------------------------------------------------------------------------------------------------------
<h3>提示</h3>
<script src="resource/plugins/sco/js/sco.tooltip.js?version=${globalVersion}"></script>
<a href="#" data-trigger="tooltip" data-content="Lorem ipsum dolor">Hover me</a>


----------------------------------------------------
<h3>消息提示</h3>
<a id="message_trigger_ok" href="#">查看正确信息</a>
<a id="message_trigger_err" href="#">查看错误信息</a>
<script>
  $('#message_trigger_ok').on('click', function(e) {
    e.preventDefault();
    $.scojs_message('This is an info message', $.scojs_message.TYPE_OK);
  });
  $('#message_trigger_err').on('click', function(e) {
    e.preventDefault();
    $.scojs_message('This is an error message', $.scojs_message.TYPE_ERROR);
  });
</script>

-------------------------------------------------------------
<h3>面板切换</h3>
<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<div class="pane-wrapper" id="panes-example">
  <div><img src="resource/image/test/carousel1.jpg"></div>
  <div><img src="resource/image/test/carousel2.jpg"></div>
</div>
<button class="btn" id="select-pane1">Show 1</button>
<button class="btn" id="select-pane2">Show 2</button>
<script>
  $(function() {
    var $panes = $.scojs_panes('#panes-example', {easing: 'flip'});
    $('#select-pane1').on('click', function() {
      $panes.select(0);
    });
    $('#select-pane2').on('click', function() {
      $panes.select(1);
    });
  });
</script>

