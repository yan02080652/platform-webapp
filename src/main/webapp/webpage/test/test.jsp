<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<style>
	h3{
		font-size: 16px;
		margin-top: 8px;
		margin-bottom: 3px;
	}
</style>

<h2>公共选择器Demo</h2>
<p class="lead">js文件路径:webapp/webpage/test/test.js</p>

<h3>单位选择器</h3>
<button onclick="chooseOrgs()">单位多选</button>
<button onclick="chooseOrg()">单位单选</button>

<h3>单位下拉选择</h3>
<div class="input-group col-md-3">
	<input type="text" id = "orgInput" class="form-control dropdown-org orgInput"
		readonly="readonly"> <span class="input-group-btn">
		<button class="btn btn-default" type="button"
			onclick="selectDropdownOrg(this)">
			<i class="fa fa-caret-down" aria-hidden="true"></i>
		</button>
	</span>
</div>

<h3>角色选择器</h3>
<button onclick="chooseRoles()">角色多选</button>
<button onclick="chooseRole()">角色单选</button>

<h3>角色下拉选择</h3>
<div class="input-group col-md-3">
	<input type="text" id = "roleInput" class="form-control dropdown-role roleInput"
		readonly="readonly"> <span class="input-group-btn">
		<button class="btn btn-default" type="button"
			onclick="selectDropdownRole(this)">
			<i class="fa fa-caret-down" aria-hidden="true"></i>
		</button>
	</span>
</div>

<h3>员工选择器</h3>
<button onclick="chooseUsers()">员工多选</button>
<button onclick="chooseUser()">员工单选</button>

<h3>组织层面下拉选择</h3>
<div class="input-group col-md-3">
	<input type="text" id="orgGradeInput" class="form-control dropdown-role roleInput"
		readonly="readonly"> <span class="input-group-btn">
		<button class="btn btn-default" type="button"
			onclick="selectDropdownOrgGrade(this)">
			<i class="fa fa-caret-down" aria-hidden="true"></i>
		</button>
	</span>
</div>

<h3>自定义数据下拉选择(列表)</h3>
<div class="input-group col-md-3">
	<input type="text" id="myDropDown" class="form-control dropdown-common myDropDownInput"> <span class="input-group-btn">
		<button class="btn btn-default" type="button"
			onclick="selectMyDropDown(this)">
			<i class="fa fa-caret-down" aria-hidden="true"></i>
		</button>
	</span>
</div>

<h3>自定义数据下拉选择(树形数据)</h3>
<div class="input-group col-md-3">
	<input type="text" id="myDropDownTree" class="form-control dropdown-common"> <span class="input-group-btn">
		<button class="btn btn-default" type="button"
			onclick="selectMyDropDownTree(this)">
			<i class="fa fa-caret-down" aria-hidden="true"></i>
		</button>
	</span>
</div>

<h3>选择企业</h3>
<button onclick="choosePartner()">选择企业</button>

<h3>选择合作伙伴</h3>
<button onclick="chooseDep()">选择合作伙伴</button>

<h3>数据字典下拉</h3>
<div class="input-group col-md-3">
	<input type="text" id="myDicDropDownTree" class="form-control dropdown-common"> <span class="input-group-btn">
		<button class="btn btn-default" type="button"
			onclick="selectMyDicDropDownTree(this,'PLATFORM','SYS_LIST')">
			<i class="fa fa-caret-down" aria-hidden="true"></i>
		</button>
	</span>
</div>

<h3>自定义选择器1</h3>
<button onclick="selCustom()">自定义选择客服</button>

<h3>自定义选择器2</h3>
<button onclick="selCustom2()">单选客服</button>
<button onclick="selCustom2Multi()">多选客服</button>

<h3>酒店选择器</h3>
<button onclick="selHotel()">单选酒店</button>
<button onclick="selHotel2()">度选酒店</button>

<h3>---------------</h3>
<button onclick="selPartnerAndUser()">导入企业人员</button>

<jsp:include page="../widget/partnerAndUser.jsp"/>

<script type="text/javascript" src="webpage/test/test.js?version=${globalVersion}"></script>

