<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<nav class="text-center">
	<c:if test="${pageList.total <= 0 }">
            <span style="color: #bbb;"> 没有相关的数据</span>
      </c:if>
      <c:if test="${pageList.total >0 }">
      		<ul class="pagination">
                <li <c:if test="${1 eq pageList.currentPage }">class="disabled"</c:if>>
                    <a href="javascript:void(0);" aria-label="Previous" <c:if test="${1 ne pageList.currentPage }">onclick="${pageScript}(${pageList.currentPage-1},${pageList.size})"</c:if>>
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
			    <c:forEach begin="1" end="${pageList.totalPage }" var="i">
                     <c:choose>
                           <c:when test="${i eq pageList.currentPage }">
                                 <li class="active">
                                       <a data-pjax href="javascript:void(0);">
                                             ${i }
                                             <span class="sr-only">(current)</span>
                                       </a>
                                 </li>
                           </c:when>
                           <c:otherwise>
                                 <li>
                                       <a data-pjax href="javascript:void(0);" onclick="${pageScript}(${i},${pageList.size})">${i}</a>
                                 </li>
                           </c:otherwise>
                     </c:choose>
               </c:forEach>
                <li <c:if test="${pageList.totalPage eq pageList.currentPage }">class="disabled"</c:if>>
                    <a href="javascript:void(0);" aria-label="Previous" <c:if test="${pageList.totalPage ne pageList.currentPage }">onclick="${pageScript}(${pageList.currentPage+1},${pageList.size})"</c:if>>
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
			</ul>
      </c:if>
  
</nav>
