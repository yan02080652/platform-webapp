<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<nav class="text-center">
	<c:if test="${pageList.total <= 0 }">
            <span style="color: #bbb;"> 没有相关的数据</span>
      </c:if>
      <c:if test="${pageList.total >0 }">
      		<ul class="pagination">
      			<c:choose>
                      <c:when test="${1 eq pageList.currentPage }">
                            <li class="disabled">
						    	<a href="javascript:void(0);" aria-label="Previous">
						    		<span aria-hidden="true">&laquo;</span>
						    	</a>
						    </li>
                      </c:when>
                      <c:otherwise>
                            <li>
						      <a data-pjax href="javascript:void(0);" onclick="${pageList.pageScript}(${pageList.start-pageList.size},${pageList.size})" aria-label="Previous">
						        <span aria-hidden="true">&laquo;</span>
						      </a>
						    </li>
                      </c:otherwise>
                </c:choose>
			    <c:forEach begin="${pageList.pageStart }" end="${pageList.pageEnd }" var="i">
                     <c:choose>
                           <c:when test="${i eq pageList.currentPage }">
                                 <li class="active">
                                       <a data-pjax href="javascript:void(0);">
                                             ${i }
                                             <span class="sr-only">(current)</span>
                                       </a>
                                 </li>
                           </c:when>
                           <c:otherwise>
                                 <li>
                                       <a data-pjax href="javascript:void(0);" onclick="${pageList.pageScript}(${pageList.size*(i-1)},${pageList.size})">${i}</a>
                                 </li>
                           </c:otherwise>
                     </c:choose>
               </c:forEach>
			    <c:choose>
                     <c:when test="${pageList.pageEnd eq pageList.currentPage }">
                           <li class="disabled next">
						    	<a href="javascript:void(0);" aria-label="Next">
						    		<span aria-hidden="true">&raquo;</span>
						    	</a>
						    </li>
                     </c:when>
                     <c:otherwise>
                           <li>
						      <a data-pjax href="javascript:void(0);" onclick="${pageList.pageScript}(${pageList.start+pageList.size},${pageList.size})" aria-label="Next">
						        <span aria-hidden="true">&raquo;</span>
						      </a>
						   </li>
                     </c:otherwise>
               </c:choose>
			</ul>
      </c:if>
  
</nav>
