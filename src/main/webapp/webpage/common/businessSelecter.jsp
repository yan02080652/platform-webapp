<%--
  Created by IntelliJ IDEA.
  User: rickzhang
  Date: 2017/9/13
  Time: 上午10:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.tem.platform.security.authorize.PermissionUtil" %>
<style>
    .selectHeader label{
        font-weight: 400;
    }
    .selectHeader input{
        height: 34px;
        padding: 6px 12px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }

</style>
<input type="hidden" id="tmcId" value="${tmcId}" />
<c:if test='<%=PermissionUtil.checkTrans("IP_PARTNER_ADMIN")== 0%>'>
    <label style=" margin-left: 5px;width: 45px;">TMC：</label>
    <input type="hidden" id="queryBpTmcId" name="tmcId" />
    <input name="bpTmcName" type="text" style="width: 150px;padding-left: 5px;" id="queryBpTmcName" value="" onclick="selectTmc()" placeholder="选择TMC" />

</c:if>
<label style=" margin-left: 5px;width: 45px;">企业：</label>
<input type="hidden" id="queryBpId" name="bpId" />
<input type="hidden" id="queryPartnerId" name="partnerId">
<input name="bpName" type="text" style="width: 150px;padding-left: 5px;" id="queryBpName" value="" onclick="selectPartner()" placeholder="选择企业" />

<script type="text/javascript">
    function selectPartner(){
        var tmcId = $("#queryBpTmcId").val();
        //当前员工的tmcId
        if (tmcId != undefined) {
            if (tmcId == null || tmcId =="") {
                $.alert('请先选择TMC！','提示');
                return;
            }
        }

        TR.select('partner',{
            type:0,//固定参数
            tmcId:tmcId,
        },function(data){
            $("#queryBpId").val(data.id);
            $("#queryBpName").val(data.name);
            $("#queryPartnerId").val(data.id);
        });
    }
    function selectTmc(){
        //选择TMC时清空企业选择器

        $("#queryBpId").val('');
        $("#queryBpName").val('');
        $("#queryPartnerId").val('');

        TR.select('partner',{
            type:1//固定参数
        },function(data){
            $("#queryBpTmcId").val(data.id);
            $("#queryBpTmcName").val(data.name);
        });
    }


</script>