<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="partnerInfoHeaderCommon"> 

</div>

<script type="text/javascript">
$(document).ready(function(){
	$.ajax({
		type : 'GET',
		url : "platform/mainInfo/getPartnerInfo",
		success : function(data) {			
			//初始化当前企业信息
//			if(data.auth == 'All'){//说明是全部权限则用企业选择器
				if(data.partnerId){
					$("#partnerInfoHeaderCommon").html("当前企业：<input type='hidden' id='partnerIdHeader' value='"+data.partnerId+"'/><input type='text' id='partnerNameHeader' value='"+data.partnerName+"'/><button onclick='selectPartnerHeader()'>切换</button>");
				}else {
                    $("#partnerInfoHeaderCommon").html("<input type='hidden' id='partnerIdHeader' /><input type='text' id='partnerNameHeader' value='"+data.partnerName+"'/><button onclick='selectPartnerHeader()'>切换</button>");
                }
//			}else{//说明是部分选择器 则下拉选择
//				if(data.partnerId){
//					var html = "当前企业：";
//					html += "<select class='form-control'  id='partnerIdHeader' onchange='onSelectPartnerHeader()'>";
//
//					$.each(data.partnerDtoList, function(i, item){
//						html += "<option value="+item.id+">"+item.name+"</opton>";
//					});
//					html += "</select>";
//
//					$("#partnerInfoHeaderCommon").html(html);
//					$("#partnerIdHeader").val(data.partnerId);
//
//				}
//			}
			
			
		}
	});
});

function selectPartnerHeader(){
    var type = '${type}';
    var partnerType = 0;
    if(type == 'tmc'){
        partnerType = 1;
    }
	TR.select('partner',{
		type:partnerType//固定参数
	},function(data){
		$("#partnerIdHeader").val(data.id);
		$("#partnerNameHeader").val(data.name);
		
		changePartnerCookie(data.id);
	});
}

function onSelectPartnerHeader(){
	changePartnerCookie($("#partnerIdHeader").val());
}

function changePartnerCookie(partnerId){
	$.ajax({
		type : 'GET',
		data :{
			partnerId :partnerId
		},
		url : "platform/mainInfo/changePartner",
		error : function(request) {
			showErrorMsg("切换失败，请刷新重试!");
		},
		success : function(data) {
			if (data.type == 'success') {
				window.location.reload();
			} else {
				$.alert(data.txt,"提示");
			}
		}
	});
}


</script>