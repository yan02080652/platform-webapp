<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="container">
	<div class="row">
		<div class="span12">
		
		
<!-- 小图标 -->
<i class="glyphicon glyphicon-search"></i>
<span class="glyphicon glyphicon-search"></span>
<br/>
<br/>


<!-- 下拉菜单  -->
<div class="dropdown">
	<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
		<li><a tabindex="-1" href="#">Action</a></li>
		<li><a tabindex="-1" href="#">Another action</a></li>
		<li><a tabindex="-1" href="#">Something else here</a></li>
		<li class="divider"></li>
		<li><a tabindex="-1" href="#">Separated link</a></li>
	</ul>
</div>

<div class="dropdown">
	<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
		<li><a tabindex="-1" href="#">Action</a></li>
		<li><a tabindex="-1" href="#">Another action</a></li>
		<li><a tabindex="-1" href="#">Something else here</a></li>
		<li class="divider"></li>
		<li><a tabindex="-1" href="#">Separated link</a></li>
	</ul>
</div>

<!-- 多级嵌套 -->
<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
	<!-- 其他多个菜单项li元素 -->
	<li class="dropdown-submenu pull-left"><a tabindex="-1"
		href="#">More options</a>
		<ul class="dropdown-menu">
			<li><a tabindex="-1" href="#">二级菜单项</a></li>
			<li><a tabindex="-1" href="#">二级菜单项</a></li>
			<li><a tabindex="-1" href="#">二级菜单项</a></li>
			<li><a tabindex="-1" href="#">二级菜单项</a></li>
			<li><a tabindex="-1" href="#">二级菜单项</a></li>
		</ul></li>
	<!-- 其他多个菜单项li元素 -->
</ul>


<br/>
<br/>

<!-- 按钮组 -->
<div class="btn-group">
	<button type="button" class="btn btn-default">Left</button>
	<button type="button" class="btn btn-default">Middle</button>
	<button type="button" class="btn btn-default">Right</button>
</div>
			
<div class="btn-group">
	<button class="btn btn-default" type="button">首页</button>
	<button class="btn btn-default" type="button">个人简介</button>
	<button class="btn btn-default" type="button">作品</button>
	<div class="btn-group">
		<button data-toggle="dropdown"
			class="btn btn-default dropdown-toggle" type="button">
			图书<span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><a href="#">JavaScript编程精解</a></li>
			<li><a href="#">JavaScript设计模式</a></li>
			<li><a href="#">JavaScript启示录</a></li>
		</ul>
	</div>
</div>

<!-- 按钮下拉菜单 -->
<div class="btn-group">
	<button type="button" class="btn btn-success dropdown-toggle"
		data-toggle="dropdown">
		Success <span class="caret"></span>
	</button>
	<ul class="dropdown-menu">
		<li><a href="#">Action</a></li>
		<li><a href="#">Another action</a></li>
		<li><a href="#">Something else here</a></li>
		<li class="divider"></li>
		<li><a href="#">Separated link</a></li>
	</ul>
</div>

<br/>
<br/>

<!-- 输入框组 -->
<div class="input-group">
	<span class="input-group-addon">$</span> <input type="text"
		class="form-control">
	<!-- 这里的input，必须使用formcontrol样式才行 -->
	<span class="input-group-addon">.00</span>
</div>
<div class="row">
	<div class="col-lg-3">
		<div class="input-group">
			<span class="input-group-addon"><input type="checkbox"></span>
			<input type="text" class="form-control">
		</div>
	</div>
	<div class="col-lg-3">
		<div class="input-group">
			<span class="input-group-addon"><input type="radio"></span>
			<input type="text" class="form-control">
		</div>
	</div>
</div>

<br/>
<br/>

<div class="row">
	<div class="col-lg-2">
		<div class="input-group">
			<div class="input-group-btn ">
				<button type="button" class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					Action<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">...
				</ul>
			</div>
			<input type="text" class="form-control">
		</div>
	</div>
	<div class="col-lg-2">
		<div class="input-group">
			<input type="text" class="form-control">
			<div class="input-group-btn ">
				<button type="button" class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					Action<span class="caret"></span>
				</button>
				<ul class="dropdown-menu pull-right">
					<!-- 这里使用了pullright，以便可以右对齐 -->
				</ul>
			</div>
		</div>
	</div>
</div>

<br/>
<br/>

<!-- tab页 -->
<div class="tabbable">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab1" data-toggle="tab">标题 1</a></li>
		<li><a href="#tab2" data-toggle="tab">标题 2</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<p>我是内容1</p>
		</div>
		<div class="tab-pane" id="tab2">
			<p>我是内容2</p>
		</div>
	</div>
</div>

<br/>
<br/>
<!-- panel  面板 -->
<div class="panel panel-default">
	<div class="panel-heading">面板header</div>
	<div class="panel-body">这里是面板内容</div>
	<div class="panel-footer">面板footer</div>
</div>

<br/>
<br/>
<!-- 可链接的列表组 -->
<div class="list-group">
	<a href="#" class="list-group-item active">JavaScript编程精解<span
		class="badge">1</span></a> <a href="#" class="list-group-item">JavaScript设计模式
		<span class="badge">2</span>
	</a> <a href="#" class="list-group-item">JavaScript启示录 <span
		class="badge">3</span>
	</a> <a href="#" class="list-group-item">当前你正在读的书<span class="badge">4</span></a>
	<a href="#" class="list-group-item">正在构思的书<span class="badge">5</span></a>
</div>

<br/>
<br/>
<!-- 可关闭的警告框 -->
<div class="alert alert-warning alert-dismissable">
	<button type="button" class="close" data-dismiss="alert"
		ariahidden="true">
		×</ button> <strong>Warning!</strong>
		<p>Best check yo self, you're not looking too good.</p>
</div>

<br/>
<br/>
<!-- 页面标题 -->
<div class="page-header">
    <h1> 我是标题 <small>我是副标题</small></h1>
</div>

<br/>
<br/>
<!-- 分页 -->
<ul class="pagination">
	<li><a href="#">«</a></li>
	<!-- 上一页 -->
	<li><a href="#">1</a></li>
	<li><a href="#">2</a></li>
	<li><a href="#">3</a></li>
	<li><a href="#">4</a></li>
	<li><a href="#">5</a></li>
	<li><a href="#">»</a></li>
	<!-- 下一页 -->
</ul>

<br/>
<br/>
<!-- 翻页 -->
<ul class="pager">
	<li><a href="#">上一页</a></li>
	<li><a href="#">下一页</a></li>
</ul>

<br/>
<br/>
<!-- 面包屑导航 -->
<ul class="breadcrumb">
	<li><a href="#">Home</a></li>
	<li><a href="#">Library</a></li>
	<li class="active">Data</li>
</ul>

<br/>
<br/>
<!-- 基础导航条 -->
<nav class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Brand</a>
	</div>
	<ul class="nav navbar-nav">
		<li class="active"><a href="#">active</a></li>
		<li><a href="#">Link</a></li>
		<li class="disabled"><a href="#">disabled</a></li>
		<li><a href="#">Link</a></li>
	</ul>
</nav>

<br/>
<br/>
<!-- 选项卡导航 -->
<ul class="nav nav-tabs ">
	<li class="active"><a href="#">主页</a></li>
	<li><a href="#">个人信息</a></li>
	<li class="disabled"><a href="#">作品</a></li>
	<li><a href="#">图书</a></li>
</ul>

<br/>
<br/>
<!-- 胶囊式选项卡导航 -->
<ul class="nav nav-pills ">
	<li class="active"><a href="#">主页</a></li>
	<li><a href="#">个人信息</a></li>
	<li class="disabled"><a href="#">作品</a></li>
	<li><a href="#">图书</a></li>
</ul>

<br/>
<br/>
<!-- 堆叠式导航 -->
<ul class="nav nav-pills nav-stacked">
	<li class="active"><a href="#">主页</a></li>
	<li><a href="#">个人信息</a></li>
	<li><a href="#">作品</a></li>
	<li><a href="#">图书</a></li>
</ul>

<br/>
<br/>
<!-- 二级导航 -->
<ul class="nav nav-tabs">
	<li class="active"><a href="#">Home</a></li>
	<li><a href="#">Profile</a></li>
	<li><a href="#">Messages</a></li>
	<li class="dropdown"><a class="dropdown-toggle"
		data-toggle="dropdown" href="#">Dropdown <span class="caret"></span></a>
		<ul class="dropdown-menu">
			<li><a tabindex="-1" href="#">二级菜单1</a></li>
			<li><a tabindex="-1" href="#">二级菜单2</a></li>
		</ul></li>
</ul>

<!-- 警告框 -->
<div class="alert alert-warning fade in">
	<h4>警告标题</h4>
	<p>Change this and that and try again. ...</p>
	<a href="#" class="btn btn-danger" data-dismiss="alert">关闭</a>
</div>

		</div>
	</div>
</div>