<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${pageList.total <= 0 }">
      <span style="color: #bbb; font-size:14px;font-weight:bold;"> 没有相关的数据</span>
</c:if>
<c:if test="${pageList.total > pageList.size }">
	<a class="first" href="${pageList.pageUrl }start=${pageList.pageStart}&pageSize=${pageList.size}" >首页</a>
	<c:choose>
        <c:when test="${1 eq pageList.currentPage }">
	    	<a class="pre" href="javascript:void(0);">上一页</a>
        </c:when>
        <c:otherwise>
        	<a class="pre" href="${pageList.pageUrl }start=${pageList.start-pageList.size}&pageSize=${pageList.size}" >上一页</a>
        </c:otherwise>
    </c:choose>
    
    <c:forEach begin="${pageList.pageStart }" end="${pageList.pageEnd }" var="i">
          <c:choose>
                <c:when test="${i eq pageList.currentPage }">
                	<a class="cur" href="javascript:void(0);">${i }</a>
                </c:when>
                <c:otherwise>
                      <a href="${pageList.pageUrl }start=${pageList.size*(i-1)}&pageSize=${pageList.size}" >${i }</a>
                </c:otherwise>
          </c:choose>
    </c:forEach>
    
    <c:choose>
        <c:when test="${pageList.pageEnd eq pageList.currentPage }">
	    	<a class="pre" href="javascript:void(0);">下一页</a>
        </c:when>
        <c:otherwise>
        	<a class="pre" href="${pageList.pageUrl }start=${pageList.start+pageList.size}&pageSize=${pageList.size}"  >下一页</a>
        </c:otherwise>
    </c:choose>
    <a href="${pageList.pageUrl }start=${(pageList.pageEnd-1)*pageList.size}&pageSize=${pageList.size}" >尾页</a>
</c:if>
