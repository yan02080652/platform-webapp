<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.tem.platform.security.authorize.PermissionUtil,com.iplatform.common.exception.BizException" %>
<%
String orgIdStr = request.getParameter("orgId");
String transid = request.getParameter("TRANSID");
String activity = request.getParameter("ACTIVITY");
Long orgId = null;
if(transid == null || transid.trim().length() == 0) {
	throw new BizException("10403", "无访问权限。");
}
if(orgIdStr != null && orgIdStr.trim().length() != 0) {
	orgId = Long.parseLong(orgIdStr);
}
transid = transid.trim();

int auth = -1;
if(activity != null && activity.trim().length() != 0) {
	activity = activity.trim();
	
	auth = PermissionUtil.checkTrans(orgId, transid, activity);
} else {
	auth = PermissionUtil.checkTrans(orgId, transid);
}

if(auth != 0) {
	throw new BizException("10403", "无访问权限。");
}
%>