<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:if test="${pageList.total <= 0 }">
      <span style="color: #bbb; font-size:14px;font-weight:bold;"> 没有相关的数据</span>
</c:if>

<c:if test="${pageList.total >0 }">
    <p>共计${pageList.totalPage}页,${pageList.total }条数据</p> 
	<a class="first" href="javascript:void(0);" onclick="${param.callback}(1,<%=request.getParameter("p1")%>)" >首页</a>
	<c:choose>
        <c:when test="${1 eq pageList.currentPage }">
	    	<a class="pre" href="javascript:void(0);">上一页</a>
        </c:when>
        <c:otherwise>
        	<a class="pre" href="javascript:void(0);" onclick="${param.callback}(${pageList.currentPage - 1},<%=request.getParameter("p1")%>)" >上一页</a>
        </c:otherwise>
    </c:choose>
    
    <c:set var="pageStart" value="${pageList.currentPage > 5 ? pageList.currentPage : 1}" />
    <c:set var="pageEnd" value="${pageList.totalPage > pageStart + 9 ? pageStart + 9 : pageList.totalPage}" />
    
    <c:forEach begin="${pageStart }" end="${pageEnd }" var="i">
          <c:choose>
                <c:when test="${i eq pageList.currentPage }">
                	<a class="cur" href="javascript:void(0);">${i }</a>
                </c:when>
                <c:otherwise>
                      <a href="javascript:void(0);" onclick="${param.callback}(${i},<%=request.getParameter("p1")%>)">${i }</a>
                </c:otherwise>
          </c:choose>
    </c:forEach>
    
    <c:choose>
        <c:when test="${pageEnd eq pageList.currentPage }">
	    	<a class="pre" href="javascript:void(0);">下一页</a>
        </c:when>
        <c:otherwise>
        	<a class="pre" href="javascript:void(0);" onclick="${param.callback}(${pageList.currentPage + 1},<%=request.getParameter("p1")%>)" >下一页</a>
        </c:otherwise>
    </c:choose>
    
    <a href="javascript:void(0);" onclick="${param.callback}(${pageList.totalPage},<%=request.getParameter("p1")%>)" >尾页</a>
</c:if>
