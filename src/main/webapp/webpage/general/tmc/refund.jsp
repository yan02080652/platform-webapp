<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="refund_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog" style="width: 800px;">
      <div class="modal-content" style="width: 800px;">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            		<h4 class="modal-title">
            			填写退款信息
            		</h4>
         </div>
         <div class="modal-body">
	         <form  class="form-horizontal" id="refundForm">
	         	<input type="hidden" name="taskId" value="${taskId }"/>
	         	<input type="hidden" name="orderId" value="${orderId}">
	         	<input type="hidden" name="refundOrderId" value="${refundOrderId}">
	         	  <div id="refund_order_form">
		 			   <div class="form-group">
				     	 <label  class="col-sm-2 control-label">服务类型</label>
				     	 <div class="col-sm-4">
							<input type="text" class="form-control" value="${generalOrderDto.serviceName }"/>
				     	 </div>
				     	 
				     	 <label  class="col-sm-2 control-label">出行人数</label>
				     	  <div class="col-sm-4">
					         <input type="number" class="form-control" name="travellerCount" value="${generalOrderDto.travellerCount }"/>
					      </div>
				  	  </div>
		 
		  			   <div class="form-group">
					    <label  class="col-sm-2 control-label">行程时间</label>
				     	  <div class="col-sm-4">
					         <input type="text" class="form-control" name="travellerDate" value="${generalOrderDto.travellerDate }""/>
					      </div>
					       <label  class="col-sm-2 control-label">行程地点</label>
				     	  <div class="col-sm-4">
					         <input type="text" class="form-control" name="destination" value="${generalOrderDto.destination }" />
					      </div>
					   </div>
					   
					   <div class="form-group">
					      <label  class="col-sm-2 control-label">需求描述</label>
					      <div class="col-sm-10" >
					      	<textarea rows="2" class="form-control" name="descr">${generalOrderDto.descr }</textarea>
					      </div>
					   </div>
					   				   
		 			   <div class="form-group">
					    <label  class="col-sm-2 control-label">销售价(元)</label>
				     	  <div class="col-sm-4">
					         <input type="text" class="form-control" name="totalSalePrice" value="<fmt:formatNumber value="${generalOrderDto.totalSalePrice }" pattern="###0.##"/>"/>
					      </div>
					       <label  class="col-sm-2 control-label">服务费(元)</label>
				     	  <div class="col-sm-4">
					         <input type="text" class="form-control" name="servicePrice" value="<fmt:formatNumber value="${generalOrderDto.servicePrice }" pattern="###0.##"/>" />
					      </div>
					   </div>
					   
					   <div class="form-group">
				     	 <label  class="col-sm-2 control-label">供应商名称</label>
				     	 <div class="col-sm-4">
							<input type="text" class="form-control" name="providerName" value="${generalOrderDto.providerName }"/>
				     	 </div>
				     	 <label  class="col-sm-2 control-label">供应商订单号</label>
				     	  <div class="col-sm-4">
					         <input type="text" class="form-control" name="providerOrderNo" value="${generalOrderDto.providerOrderNo }"/>
					      </div>
				  	  </div>					   
					   
		 			   <div class="form-group">
					   	  <label  class="col-sm-2 control-label">供应商应付(元)</label>
				     	  <div class="col-sm-4">
					         <input type="text" class="form-control" name="totalSettlePrice" value="<fmt:formatNumber value="${generalOrderDto.totalSettlePrice }" pattern="###0.##"/>" />
					      </div>		 			   
					      <label  class="col-sm-2 control-label">客户可退(元)</label>
				     	  <div class="col-sm-4">
					         <input type="text" class="form-control" value="<fmt:formatNumber value="${refundOrder.refundableAmount }" pattern="###0.##"/>"/>
					      </div>
					   </div>					   
				   </div>	   

				   <br>
   	 			   <div class="form-group">
				      <label  class="col-sm-2 control-label">供应商应退(元)</label>
			     	  <div class="col-sm-4">
				         <input type="text" class="form-control" data-id="${generalOrderDto.totalSettlePrice}" name="providerFee" />
				      </div>  	 			   
   	 			   	  <label  class="col-sm-2 control-label">客户应退(元)</label>
			     	  <div class="col-sm-4">
				         <input type="text" class="form-control" data-id="${refundOrder.refundableAmount}"  name="customerFee" />
				      </div>

				   </div>
			 </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="submitRefundForm()">退款</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
         </div>
      </div>
</div>
</div>
<script type="text/javascript">
$('#refund_order_form').find('input,textarea,select').attr('disabled',true);
</script>