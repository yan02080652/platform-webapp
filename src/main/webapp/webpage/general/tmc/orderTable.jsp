<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>


<!-- 订单状态选项 -->
	<div class="tab" style="margin-bottom: 5px">
		<ul class="fix" id="orderStatus">
			<c:forEach items="${OrderStatuslist }" var="orderStatus">
				<c:choose>
					<c:when test="${orderStatus.orderShowStatus eq currentOrderStatus }">
						<li class="active"><span data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.message }(${orderStatus.count })</span></li>
					</c:when>
					<c:otherwise>
						<li><span data-value="${orderStatus.orderShowStatus }">${orderStatus.orderShowStatus.message }(${orderStatus.count })</span></li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</div>

<!-- 订单列表 -->

	<table class="table table-bordered" id="orderTable">
		<thead>
			<tr>
				<th style="width: 20%">服务信息</th>
				<th style="width: 20%">旅客信息</th>
				<th style="width: 20%">收支信息</th>
				<th style="width: 20%">处理信息</th>
				<th style="width: 20%">供应信息</th>
			</tr>
		</thead>
		<tbody>
		
		<c:set var="nowDate" value="<%=System.currentTimeMillis()%>"></c:set>
		
			<c:forEach items="${pageList.list }" var="generalOrder">
					<tr>
						<td colspan="5">
							<span>企业：${generalOrder.cName } </span>
							<span>预订人：${generalOrder.userName }</span>
							<span>订单号：<font color="blue"><a href="/order/generalDetail/${generalOrder.id }" target="_Blank">${generalOrder.id }</a></font></span>
							<span>来源:${originMap[generalOrder.orderOriginType]}</span>
							<span>标签：${generalOrder.orderShowStatus.message }</span>
							<span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${generalOrder.createDate }" /></span>
						    <%-- <span><a onclick="logList('${generalOrder.id}')">日志</a></span> --%>
					    </td>
					</tr>
					<tr>
						<td>
							<span>时间：${generalOrder.travellerDate }</span></br>
							<span>行程：${generalOrder.destination }</span></br>
							<span>服务品类：${generalOrder.serviceName }</span>							
						</td>
						<td>
							<span>出行人数 : ${generalOrder.travellerCount }(人)</span>
						</td>
						<td>
							<c:choose>
								<c:when test="${not empty generalOrder.totalOrderPrice }">
									<span>应收￥${generalOrder.totalOrderPrice }</span>
									<c:if test="${not empty generalOrder.paymentPlanNo }">
										<span>已收</span><br/>
										<span><a onclick="loadIncomeDetail('${generalOrder.id}','${generalOrder.paymentPlanNo}')">明细</a></span>
									</c:if>
									<c:if test="${empty generalOrder.paymentPlanNo }">
										<span>未收</span><br/>
										<span><a onclick="getToDesk('${generalOrder.userId }','${generalOrder.id}')">付款</a></span>
									</c:if>
								</c:when>
								<c:otherwise>
									待报价
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<span>
								${generalOrder.orderShowStatus.message }
							</span>
						 </td>
						<td>
						 <c:if test="${not empty generalOrder.providerName }">
							 ${generalOrder.providerName }&nbsp;&nbsp;<br/>
								<a data-trigger="tooltip" data-content="${generalOrder.providerOrderNo }">订单号</a><br/>
						 </c:if>
						</td>
					</tr>
			</c:forEach>
		</tbody>
	</table>
<div class="pagin" style="margin-bottom: 20px">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadOrderList"></jsp:include>
</div>

<form action="" method="get" id="login_Payment_Form">
	<input type="hidden" name="alp"/>
	<input type="hidden" name="service"/>
</form>

<script>
$("#orderStatus li").click(function() {
	submitForm(null,$(this).find("span").attr("data-value"));
});

function getToDesk(userId,orderNo){

    var myDate=new Date()
    var date2 = myDate.getTime();
    $("#login_Payment_Form").attr("target", date2);
    var win = window.open(null,date2);
    win.focus();

	$.ajax({
		type:"get",
		url:"general/tmc/getToDesk",
		data:{userId:userId,orderNo:orderNo},
		success:function(data){

            $("#login_Payment_Form").attr("action", data.loginUrl);
            $("input[name='alp']").val(data.alp);
            $("input[name='service']").val(data.service);

            logout(data.logoutUrl);

            $("#login_Payment_Form").submit();

		}
	});

    function logout(logoutUrl){
        for(var i = 0 ; i < logoutUrl.length ; i++){
            new Image().src = logoutUrl[i];
            console.log(logoutUrl[i]);
        }
    }

}
</script>