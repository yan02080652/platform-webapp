<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<link rel="stylesheet" href="resource/plugins/layer/skin/layui.css?version=${globalVersion}">
<link rel="stylesheet" href="resource/css/bootstrap/fileinput.min.css?version=${globalVersion}">
<style>
.otherExpense {
    height: 28px;
    padding-left: 8px;
    list-style: none;
    line-height: 28px;
    font-size: 14px;
    color: #46a1bb ;
    cursor: pointer;
}
/*树弹窗*/
#expenseDetail {
    margin-left: 30px;
}
.otherExpenseH {
    display: none;
    color: #ccc;
}
/*对树追加圆点的操作 小圆点*/
.activePoint {
    color: #3e3a39;
    cursor: pointer;
}
.activeTree {
    color: #46a1bb ;
}
.activePoint:hover {
    color: #46a1bb ;
}
.layui-tree li a {
    /*cursor: not-allowed;*/
}
cite {
    position: relative;
    /*cursor: not-allowed;*/
}
.costUnit {
    position: absolute;
    z-index: 99;
    width: 8px;
    height: 8px;
    background-color: #1176CC;
    border-radius: 50%;
    right: -4px;
    top: 4px;
}
cite:hover .costUnit:after {
    border: 1px solid #cccccc;
    font-size: 12px;
    content: '成本核算单元';
    position: absolute;
    background-color: #FFFFFF;
    z-index: 2147483584;
    margin-top: -6px;
    margin-left: 10px;
    padding: 2px;
    height: 14px;
    line-height: 14px;
}
.item_pull {
    float: left;
    height: 30px;
    line-height: 28px;
    text-align: left;
    background-color: #ffffff;
    position: relative;
}

.item_div {
	width:230px;
    height: 34px;
    padding-left: 8px;
    background: url('/resource/image/downGray.png') no-repeat 210px;
    border: 1px solid #aaaaaa;
    cursor: pointer;
    border-radius: 4px;
}

.ul_b {
    display: none;
    background: #ffffff;
    position: absolute;
    z-index: 999;
    border: 1px solid #bbbbbb;
    border-top: none;
    cursor: pointer;
}

.ul_b .otherExpense {
    width: 230px; 
    padding-left: 8px;
    color: #1176CC;
}

.item_ul > li {

    height: 28px;
    padding-left: 8px;
    list-style: none;
}

.item_ul > li:hover {
    background: #46a1bb;
    color: #FFFFFF;
    border-color: #46a1bb;
}
</style>

<div class="modal fade" id="ticket_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog" style="width: 800px;">
      <div class="modal-content" style="width: 800px;">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            		<h4 class="modal-title">
            			<c:if test="${taskType eq 'WAIT_OFFER' }">
            				填写报价信息
            			</c:if>            		
            			<c:if test="${taskType eq 'WAIT_DELIVER' }">
            				服务完成
            			</c:if>
            		</h4>
         </div>
         <div class="modal-body">
         <form  class="form-horizontal" id="ticketForm" method="post"  enctype="multipart/form-data">
         	<input type="hidden" name="taskId" value="${taskId }"/>
         	<input type="hidden" name="id" value="${generalOrderDto.id }">
 			<div id = "general_offer_from">
	 			 <div class="form-group">
			     	 <label  class="col-sm-2 control-label">服务类型</label>
			     	 <div class="col-sm-4">
						<div class="input-group">
							<input type="hidden" id = "serviceType" value="${generalOrderDto.serviceType }" name="serviceType"/>
							<input type="text" id="serviceName" name="serviceName" value="${generalOrderDto.serviceName }" class="form-control dropdown-common"> 
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"
									onclick="selectMyDropDown(this)">
									<i class="fa fa-caret-down" aria-hidden="true"></i>
								</button>
							</span>
						</div>					    
			     	 </div>
			     	 
			     	 <label  class="col-sm-2 control-label">出行人数</label>
			     	  <div class="col-sm-4">
				         <input type="number" class="form-control" name="travellerCount" value="${generalOrderDto.travellerCount }"/>
				      </div>
			  	  </div>
	 
	  			   <div class="form-group">
				    <label  class="col-sm-2 control-label">行程时间</label>
			     	  <div class="col-sm-4">
				         <input type="text" class="form-control" name="travellerDate" value="${generalOrderDto.travellerDate }"/>
				      </div>
				       <label  class="col-sm-2 control-label">行程地点</label>
			     	  <div class="col-sm-4">
				         <input type="text" class="form-control" name="destination" value="${generalOrderDto.destination }" />
				      </div>
				   </div>

				   <div class="form-group">
						<label  class="col-sm-2 control-label">实际行程时间</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" onclick="WdatePicker()" name="travelBeginDate" value="<fmt:formatDate value="${generalOrderDto.travelBeginDate }"   pattern="yyyy-MM-dd"/>"/>
						</div>
					   <label style="float: left;width: 2px;"  class="control-label">至</label>
					    <div class="col-sm-2">
						   <input type="text" class="form-control" onclick="WdatePicker()" name="travelEndDate" value="<fmt:formatDate value="${generalOrderDto.travelEndDate }"   pattern="yyyy-MM-dd"/>"/>
					    </div>
				   </div>

				   <div class="form-group">
				      <label  class="col-sm-2 control-label">需求描述</label>
				      <div class="col-sm-10" >
				      	<textarea rows="2" class="form-control" name="descr">${generalOrderDto.descr }</textarea>
				      </div>
				   </div>
	
				   <div class="form-group">
			     	 <label  class="col-sm-2 control-label">差旅类型</label>
			     	 <div class="col-sm-4">
			         	<select class="form-control" name="travelCode">
					          <c:forEach items="${travelTypes }" var="travelType">
					          	<option value="${travelType.itemCode }" <c:if test = "${generalOrderDto.travelCode eq travelType.itemCode}">selected="selected"</c:if> >${travelType.itemTxt }</option>
					          </c:forEach>
					    </select>
			     	 </div>
			     	 
			     	  <label  class="col-sm-2 control-label">费用归属</label>
			     	  <div class="col-sm-4" id = "chooseCost">
                            <div class="item_pull">
                                <div class="pullTitle">
                                    <label>
                                        <input type="text" class="item_div" value="${costCenterName }" placeholder="请选择费用归属" readonly/>
                                    </label>
                                </div>
                                <div class="ul_b">
                                	<input  type="hidden" id = "orgIdAndcostIdAndCorpId" name="orgIdAndcostIdAndCorpId"/>
                                    <ul class="item_ul">
                                       <c:forEach items="${costCenter}" var="cost">
                                           <li data-value="${cost.orgId},${cost.id },${cost.corpId}" >${cost.name}</li>
                                       </c:forEach>
                                    </ul>
                                    <p class="otherExpense">使用其他成本核算单元</p>
                                </div>
                            </div>					    
				      </div>
			  	  </div>			   
				   
	 			   <div class="form-group">
				    <label  class="col-sm-2 control-label">销售价(元)</label>
			     	  <div class="col-sm-4">
				         <input type="text" class="form-control" name="totalSalePrice" value="<fmt:formatNumber value="${generalOrderDto.totalSalePrice }" pattern="###0.##"/>"/>
				      </div>
				       <label  class="col-sm-2 control-label">服务费(元)</label>
			     	  <div class="col-sm-4">
				         <input type="text" class="form-control" name="servicePrice" value="<fmt:formatNumber value="${generalOrderDto.servicePrice }" pattern="###0.##"/>" />
				      </div>
				   </div>
			  </div> 
			   
			   <div class="form-group">
			      <label  class="col-sm-2 control-label">服务内容</label>
			      <div class="col-sm-10" >
			      	<textarea rows="4" class="form-control" name="serviceDescr">${generalOrderDto.serviceDescr }</textarea>
				 </div>
			   </div>
			   
			   <div class="form-group">
			      <label  class="col-sm-2 control-label">附件</label>
					<c:if test="${ not empty generalOrderDto.fileName}">
						<div class="col-sm-4" id = "file_url_1">
				         	<span><a href="http://${SYS_CONFIG['imgserver'] }/${generalOrderDto.fileUrls}">${generalOrderDto.fileName }</a>
				         	<button type="button" class="btn btn-default" onclick="deleteIcon('${generalOrderDto.id}')">删除 </button></span>
				        </div>
				    </c:if>
					<div class="col-sm-4" id ="file_url_2">
				         <input type="file" name="file" class="file-loading">
				    </div>
			   </div>
			   			   
				<div class="form-group">
		     	 <label  class="col-sm-2 control-label">供应商名称</label>
		     	 <div class="col-sm-4">
		         	<select class="form-control" name="providerCode">
				          <c:forEach items="${issueChannelList }" var="channel">
				          	<option value="${channel.code },${channel.name }" <c:if test = "${generalOrderDto.providerCode eq channel.code}">selected="selected"</c:if> >${channel.name }</option>
				          </c:forEach>
						  <%--<c:forEach items="${channelList }" var="channel">
							  <option value="${channel.code }" <c:if test = "${generalOrderDto.providerCode eq channel.code}">selected="selected"</c:if> >${channel.name }</option>
						  </c:forEach>--%>
				    </select>

		     	 </div>
		     	 <label  class="col-sm-2 control-label">供应商订单号</label>
		     	  <div class="col-sm-4">
			         <input type="text" class="form-control" name="providerOrderNo" value="${generalOrderDto.providerOrderNo }"/>
			      </div>
		  	  </div>			   
			   <div class="form-group">
			   	  <label  class="col-sm-2 control-label">应付供应商(元)</label>
		     	  <div class="col-sm-4">
			         <input type="text" class="form-control" name="totalSettlePrice" value="<fmt:formatNumber value="${generalOrderDto.totalSettlePrice }" pattern="###0.##"/>" />
			      </div>
			      <div id="refundFlag" style="display: none">		   	  
				      <label  class="col-sm-2 control-label">是否允许退款</label>
				      <div class="col-sm-4"  >
				          <label class="radio-inline">
	                        <input type="radio" name="refundFlag" value="0" checked="checked"/> 是
	                      </label>
	                      <label class="radio-inline">
	                        <input type="radio"	name="refundFlag" value="1" /> 否
	                      </label>
				      </div>
				  </div>    
			   </div>			   
			   
			</form>
         </div>
         <div class="modal-footer">
     		
            <button type="button" class="btn btn-default" onclick="submitGeneralForm()">保存</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
         </div>
      </div>
</div>
</div>
<div class="otherExpenseH">
    <ul id="expenseDetail"></ul>
</div>
<script src="resource/plugins/layer/layui.js?version=${globalVersion}"></script>
<script src="resource/plugins/layer/tree.js?version=${globalVersion}"></script>
<script src="resource/js/tr-widget/expenseAttribution.js?version=${globalVersion}"></script>
<script type="text/javascript">

loadCostTree('${generalOrderDto.cId}');

$(".file-loading").fileinput({
    language: 'zh', //设置语言
   // uploadUrl: uploadUrl, //上传的地址
    allowedFileExtensions : ['docx', 'doc','pdf','xlsx','xls'],//接收的文件后缀
    showUpload: false, //是否显示上传按钮
    showCaption: false,//是否显示标题
    browseClass: "btn btn-primary", //按钮样式 
    previewFileIcon: "<i class='glyphicon glyphicon-king'></i>", 
});

$(".fileinput-remove").remove();

function deleteIcon(id){
	 if(!id){
		return;
	 }
	 
	 $.ajax({
		cache : true,
		type : "POST",
		url : "general/tmc/deleteFile",
		data : {
			id:id
		},
		async : false,
		error : function(request) {
			showErrorMsg("请求失败，请刷新重试");
		},
		success : function(data) {
			$("#file_url_1").hide();
			showSuccessMsg("删除成功");
		}
	});
}


var taskType = '${taskType}';
if(taskType == 'WAIT_DELIVER'){
	$(".item_ul").hide();
	$(".otherExpense").hide();
	$("#refundFlag").show();
	$('#general_offer_from').find('input,textarea,select').attr('disabled',true);
}

function selectMyDropDown(dom){
	
	$('.dropdown-common-list').remove();
	$.get('general/tmc/serviceType',{
		
	},function(data){
		TR.select(data,{
			key:'id',//多个自定义下拉选择时key值不能重复
			dom:dom,
			name:'itemTxt',//default:name
			allowBlank:false//是否允许空白选项
		},function(result){

			$("#serviceType").val(result ? result.itemCode : null);
			$("#serviceName").val(result ? result.itemTxt : null);
		});
		
	});
}
</script>