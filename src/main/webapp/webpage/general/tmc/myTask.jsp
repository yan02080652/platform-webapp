<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<!-- 状态栏 -->
<div >
	<!-- 左边状态 -->
	<div class="tab pull-left">
            <ul class="fix" id="myTaskStatusTab">
                <li class="tab1 ${active eq 'PROCESSING' ? 'active' : '' }" data-value="PROCESSING" onclick="submitAjax(null,'PROCESSING',null)">处理中(${typeMap.PROCESSING==null?0:typeMap.PROCESSING})</li>
                <li style="width: 350px;" class="tab2 ${active eq 'ALREADY_PROCESSED' ? 'active' : '' }" data-value="ALREADY_PROCESSED" onclick="submitAjax(null,'ALREADY_PROCESSED',null)">近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})</li>
            </ul>
		<div id="g_my_content">
            <c:if test="${active eq 'PROCESSING' }">
            	<div class="content1" style="display:${active eq 'PROCESSING' ? 'block' : 'none' }">
	                <p>&nbsp;&nbsp;&nbsp;&nbsp;
	               		<a onclick="submitAjax(null,'PROCESSING','WAIT_OFFER')">待报价(${typeMap.WAIT_OFFER==null?0:typeMap.WAIT_OFFER })</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	               		<%-- <a onclick="submitAjax(null,'PROCESSING','WAIT_URGE_PAY')">待催款(${typeMap.WAIT_URGE_PAY==null?0:typeMap.WAIT_URGE_PAY })</a> --%>
	               		<a onclick="submitAjax(null,'PROCESSING','WAIT_REFUND_G')">待退款(${typeMap.WAIT_REFUND_G==null?0:typeMap.WAIT_REFUND_G })</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	               		<a onclick="submitAjax(null,'PROCESSING','WAIT_DELIVER')">待交付(${typeMap.WAIT_DELIVER==null?0:typeMap.WAIT_DELIVER })</a>
	                </p>
	            </div>
            </c:if>
            <div class="content2" style="display:${active eq 'ALREADY_PROCESSED' ? 'block' : 'none' }">
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_OFFER')">报价(${alreadyMap.WAIT_OFFER==null?0:alreadyMap.WAIT_OFFER })</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        	       	<%-- <a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_URGE_PAY')">催款(${alreadyMap.WAIT_URGE_PAY==null?0:alreadyMap.WAIT_URGE_PAY })</a> --%>
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_REFUND_G')">退款(${alreadyMap.WAIT_REFUND_G==null?0:alreadyMap.WAIT_REFUND_G })</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax(null,'ALREADY_PROCESSED','WAIT_DELIVER')">交付(${alreadyMap.WAIT_DELIVER==null?0:alreadyMap.WAIT_DELIVER })</a>
                </p>
            </div>
		</div>
     </div>

	<!-- 右边领取任务 -->
	<%--<div class="pull-right">--%>
		<%--自动领取<span id="switchBtn_div" style="padding-right: 3px;">已关闭</span><button type="button" class="btn" onclick="autoRefresh()" id="switchBtn">开启</button>--%>
		<%--<input type="checkbox" checked="checked" id="type1" status="WAIT_OFFER"/>待报价(${waitTaskTypeMap.WAIT_OFFER==null?0:waitTaskTypeMap.WAIT_OFFER})--%>
 		<%--<input type="checkbox" checked="checked" id="type2" status="WAIT_REFUND_G"/>待退款(${waitTaskTypeMap.WAIT_REFUND_G==null?0:waitTaskTypeMap.WAIT_REFUND_G}) --%>
		<%--<button type="button" class="btn btn-primary" onclick="receiveTask(false)">领取任务</button>--%>
	<%--</div>--%>
</div>


<!-- 任务列表 -->
<div>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th style="width: 20%">服务信息</th>
				<th style="width: 20%">旅客信息</th>
				<th style="width: 20%">收支信息</th>
				<th style="width: 20%">处理信息</th>
				<th style="width: 20%">供应信息</th>
			</tr>
		</thead>
		<tbody id="myTaskTbody">
			<c:forEach items="${pageList.list }" var="orderTask">
				<tr>
					<td colspan="5">
					    <span>企业：${orderTask.orderDto.cName } </span>
						<span>预订人：${orderTask.orderDto.userName }</span>
						<span>订单号：<font color="blue"><a href="/order/generalDetail/${orderTask.orderDto.id }" target="_Blank">${orderTask.orderDto.id }</a></font></span>
						<span>来源:${originMap[orderTask.orderDto.orderOriginType]}</span>
						<span>标签：${orderTask.orderDto.orderShowStatus.message }</span>
						<span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${orderTask.orderDto.createDate }" /></span>
					    <%-- <span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span> --%>
					    <span> 任务创建时间：<font color="blue"><fmt:formatDate value="${orderTask.createDate }" pattern="yyyy-MM-dd HH:mm:ss"/></font></span>
					    <span hidden><font color="red">任务超时</font></span>
					    <input class="proDate" type="hidden" value='<fmt:formatDate value="${orderTask.maxProcessDate }" pattern="yyyy-MM-dd HH:mm:ss"/>'>
					    <input type="hidden" value="${orderTask.taskStatus }"/>
					    <c:if test="${orderTask.taskStatus eq 'PROCESSING'}">
					    	<label>任务延时</label>
						    <select  onchange="delay('${orderTask.id }',$(this).val())">
						    	<option value="-1">--
						    	<option value="5">5分钟
						    	<option value="15">15分钟
						    	<option value="60">1小时
						    	<option value="86400">1天
						    </select>					    
					    </c:if>
				    </td>
				</tr>
				<tr>
					<td>
						<span>时间：${orderTask.orderDto.travellerDate }</span></br>
						<span>行程：${orderTask.orderDto.destination }</span></br>
						<span>服务品类：${orderTask.orderDto.serviceName }</span>
					</td>
					<td>
						<span>出行人数 : ${orderTask.orderDto.travellerCount }(人)</span></br>
					</td>
					<td>
						<c:choose>
							<c:when test="${not empty orderTask.orderDto.totalOrderPrice }">
								<span>应收￥${orderTask.orderDto.totalOrderPrice }</span>
								<c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
									<span>已收</span><br/>
									<span><a onclick="loadIncomeDetail('${orderTask.orderDto.id}','${orderTask.orderDto.paymentPlanNo}')">明细</a></span>
								</c:if>
								<c:if test="${empty orderTask.orderDto.paymentPlanNo}">
									<span>未收</span>
								</c:if>
							</c:when>
							<c:otherwise>
								待报价
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>
						<span>处理人：${orderTask.operatorName }</span><br/>
						<c:if test="${orderTask.taskStatus eq 'PROCESSING' }">
							<span>
							    <a onclick="transfer('${orderTask.id}','${orderTask.operatorId }')">交接</a> | 
							    
								<!-- 填写报价 -->
								<c:if test="${orderTask.taskType eq 'WAIT_OFFER' }">
									 <a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id }','WAIT_OFFER')">填写报价</a> | 
									 <a onclick="cancelOrder('${orderTask.orderDto.id}','${orderTask.id }')">取消订单</a>
								</c:if>
								 
								 <!-- 退款审核任务 -->
								 <c:if test="${orderTask.taskType eq 'WAIT_REFUND_G' }">
									 <a onclick="getRefundInfo('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.refundOrderId }')">通过退款</a>
									 <a onclick="getRefuseRefundInfo('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.refundOrderId }')">拒绝退款</a>
								 </c:if>
								 
								<!-- 交付任务 -->
								<c:if test="${orderTask.taskType eq 'WAIT_DELIVER' }">
									 <a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id }','WAIT_DELIVER')">服务完成</a> | 
									 <a onclick="createRefund('${orderTask.orderDto.id}','${orderTask.id }')">取消退款</a>
								</c:if>
								 
								 
							 </span>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' }">
							<span>处理结果：${orderTask.result}</span>
							<span>
								<c:if test="${orderTask.taskType eq 'WAIT_OFFER' and orderTask.orderDto.orderShowStatus eq 'WAIT_PAYMENT'}">
									<br> <a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id }','WAIT_OFFER')">修改报价</a>
								</c:if>
							</span>
						</c:if>
					 </td>
					 <td>
						 <c:if test="${not empty orderTask.orderDto.providerName }">
						 		${orderTask.orderDto.providerName }&nbsp;&nbsp;<br/>
								<a data-trigger="tooltip" data-content="${orderTask.orderDto.providerOrderNo }">订单号</a><br/>
						 </c:if>
					 </td>
				</tr>
			</c:forEach>
		</tbody>
		
	</table>
</div>

<div class="pagin" style="margin-bottom: 22px">
<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadMyTaskTable"></jsp:include>
</div>
<script src="webpage/general/tmc/js/myTask_timer.js?version=${globalVersion}"></script>
<script>
    var bizProcessingMapStr = '${bizProcessingMap}';
    var bizWaitProcessMapStr = '${bizWaitProcessMap}';
    if(bizProcessingMapStr != '' ){
        var bizProcessingMap = eval('(' + bizProcessingMapStr.split('=').join(':') + ')');
        $("#bizMenu").find("span").each(function () {
            var bizName = $(this).attr("name");
            var count = bizProcessingMap[bizName];
            if (count != undefined){
                $(this).text("("+count+")");
            }
        })
    }
    if(bizWaitProcessMapStr != ''){
        var bizWaitProcessMap = eval('(' + bizWaitProcessMapStr.split('=').join(':') + ')');
        $(".tab_h_item").each(function () {
            var inputDom = $(this).find("input:eq(0)");
            var id = inputDom.attr("id");
            var count  = bizWaitProcessMap["COUNT"][id];
            if(count != undefined){
                inputDom.parent().find("span").text("("+count+")");
            }

            var ulDom = inputDom.parent().next();
            ulDom.find("input").each(function () {
                var child_input = $(this);
                var taskType = child_input.attr("task_type");
                var bizGroup = bizWaitProcessMap[id];
                if(bizGroup != undefined){
                    var child_count = bizGroup[taskType];
                    if(child_count != undefined){
                        child_input.parent().find("span").text("("+child_count+")");
                    }
                }
            })
        })

    }
</script>
<script src="webpage/general/tmc/js/myTask.js?version=${globalVersion}"></script>
