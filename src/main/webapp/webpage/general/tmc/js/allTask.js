$(function(){
	clearInterval(timerFlag);
    clearInterval(checkTimer);
	clearInterval(allTask_timer);
	refreshAllTask();
})

//allTask:自动刷新
var allTask_count = 10;
function refreshAllTask() {
	$("#countDown").text(allTask_count);
	allTask_count--  ;
	if(allTask_count >= 0){
		allTask_timer = setTimeout(refreshAllTask, 1000);
	}else{
		var pageIndex = $("#allTask_pageIndex").val();
        pageIndex = pageIndex ? pageIndex : 1;
        var taskStatus =  $("#allTaskStatusTab .active").attr("data-value");
        var taskType = $("#currentTaskType").val();
        reloadAllTaskTable(pageIndex,taskStatus,taskType);
	}
}

function reloadAllTaskTable(pageIndex,taskStatus,taskType){

    if (!taskStatus) {
        taskStatus = 'WAIT_PROCESS';
    }
	submitAjax_all(pageIndex,taskStatus, taskType);
}


//取消订单
function cancelOrder(orderId,taskId){
	$.ajax({
		url: 'general/tmc/cancelOrder',
		type: 'post',
		async: false,
		data: {
			orderId: orderId,
			taskId: taskId
		},
		success: function(data){
			if(data){
				layer.msg("订单取消,任务取消",{icon: 1})
				setTimeout(function(){
					submitAjax_all(null,'PROCESSING',null);
				},200);
			}else{
				layer.msg("订单取消失败",{icon:2})
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
		}
	})
}

//交接
function transfer(taskId,operatorId){
	TR.select('user',{
		 type: 'me'
		},
		function(data){
		console.log(data);
		
		$.ajax({
			type:"post",
			url:"general/tmc/transfer",
			data:{
				taskId:taskId,
				operatorId:operatorId,
				userId:data.id,
				userName:data.fullname
			},
			success:function(rs){
				if(rs){
					showPromptBox("任务交给"+data.fullname);
					setTimeout(function(){
						submitAjax_all(null,'WAIT_PROCESS',null);
					},200);
				}else{
					layer.msg("系统错误，请刷新重试！",{icon:2})
				}
			}
		});
	});
}

//显示Model
function showModal(url, data, callback) {
	$('#model2').load(url, data, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

//确认提示
function showConfirm(msg,_callback){
	var index = layer.alert(msg, {
		  btn: ['确定','取消'],
		  skin: 'layui-layer-lan' //样式类名
		}, function(){
			layer.close(index);
			_callback();
		},function(){
			
		});
}

//查询订单
function submitAjax_all(pageIndex,taskStatus,taskType){
	console.log("请求全部任务");
	console.log(taskStatus);
    $("#currentTaskType").val(taskType);
	$("#allTask_pageIndex").val(pageIndex);
	$("#allTask_task_status").val(taskStatus);
	$("#allTask_task_type").val(taskType);
	
	//查询条件
	var field1 = $("#search_Field2").val().trim();
	
	if(!taskStatus){
		taskStatus = $("#allTaskStatusTab .active").attr("data-value");
	}
	
	$.ajax({
		type:"post",
		url:"general/tmc/allTask",
		data:{
			taskStatus:taskStatus,
			pageIndex:pageIndex,
			taskType:taskType,
			keyword:field1,
		},
		success:function(data){
			$("#allTask").html(data);
			$("#search_Field2").val(field1);
		},
		error:function(){
			layer.msg("系统繁忙，请刷新重试")
		}
	});
}

//重置查询条件
function resetForm(){
	$('#orderSearchForm')[0].reset();
	// 再次请求
	submitAjax_all();
}

//右下角提示
function showPromptBox(content){
	layer.open({
		  type: 0,//基本层类型
		  title: "系统提示",
		  closeBtn: 0,//不显示关闭x关闭
		  btn:[],//不显示按钮
		  shade: 0,
		  area: ['250px', '200px'],//大小
		  offset: 'rb', //右下角弹出
		  time: 3000, //3秒后自动关闭
		  shift: 2,//动画0-6
		  content: content, 
		});
}