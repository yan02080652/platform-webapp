$(function(){
	submitForm();
})

//分页函数
function reloadOrderList(pageIndex){
	var	orderStatus = $("#orderStatus .active span").attr("data-value");
	if(!orderStatus){
		orderStatus = null;
	}
	submitForm(pageIndex,orderStatus);
}

//取消订单
function cancelOrder(orderId){
	$.ajax({
		url: 'general/tmc/cancelOrder',
		type: 'post',
		async: false,
		data: {
			orderId: orderId,
			taskId: taskId
		},
		success: function(data){
			if(data.result){
				layer.msg(data.msg,{icon: 1})
				setTimeout(function(){
					submitAjax(null,'PROCESSING',null);
				},200);
			}else{
				layer.msg(data.msg,{icon:2})
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
		}
	})
}

function submitForm(pageIndex,orderStatus){
	var startDate = $("#startDate3").val();
	var endDate = $("#endDate3").val();
	var field1 = $("#searchField2").val().trim();
	var field3 = $("#searchField4").val().trim();
	
	$.ajax({
		url: "general/tmc/orderTable",
		type: "post",
		data: {	
			formDate: startDate,
			toDate: endDate,
			keyword: field1,
			partnerName: field3,
			orderStatus: orderStatus,
			pageIndex:pageIndex,
			pageSize: 10
		},
		success:function(data){
			$("#orderTable").html(data);
		}
	});
	
}

function resetSearchForm() {

    $('#searchForm')[0].reset();
    $("#orderStatus li").each(function(){
        $(this).removeClass("active");
    })
    // 再次请求
    submitForm();

}