

//定时任务
function taskTimer(){
	var pageIndex = $("#myTask_pageIndex").val();
	var taskStatus = $("#myTask_task_status").val();
	var taskType = $("#myTask_task_type").val()
	if(!taskStatus || taskStatus =='WAIT_PROCESS'){
		taskStatus = 'PROCESSING';
	}
	submitAjax(pageIndex,taskStatus,taskType);
}


//超时任务提醒
function checkTimeoutTask(){
	var flag = false;
	$(".proDate").each(function(){
		var maxproDate = $(this).val();
		var taskStatus = $(this).next().val();
		if(taskStatus != 'ALREADY_PROCESSED' && new Date().getTime() > new Date(maxproDate).getTime()){
				$(this).prev().show();
				flag = true;
		}
	})
	//警告
	if(flag){
		playMusic('music2',2);
	}
}

function delay(taskId,delay){
	if(delay != -1){
		$.ajax({
			url:"general/tmc/updateMaxProcessDate",
			post:"get",
			data:{taskId:taskId,delay:delay},
			success:function(){
				layer.msg("任务延时成功!",{icon:1,offset:'t',time:1000})
				submitAjax(null,'PROCESSING',null);
			}
		});
	}
}