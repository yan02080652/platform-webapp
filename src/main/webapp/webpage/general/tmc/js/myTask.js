$(function(){
	clearInterval(timerFlag);
    clearInterval(checkTimer);
    timerFlag = setTimeout(taskTimer,10*1000);
    checkTimer = setTimeout(checkTimeoutTask,60 * 1000);

})

//取消订单
function cancelOrder(orderId,taskId){
	$.ajax({
		url: 'general/tmc/cancelOrder',
		type: 'post',
		async: false,
		data: {
			orderId: orderId,
			taskId: taskId
		},
		success: function(data){
			if(data){
				layer.msg("任务取消成功",{icon: 1})
				setTimeout(function(){
					submitAjax(null,'PROCESSING',null);
				},200);
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon: 2})
		}
	})
}

//确认完毕
function confirmDone(orderId,taskId){
	$.ajax({
		url: 'general/tmc/confirmDone',
		type: 'post',
		async: false,
		data:{
			'orderId': orderId,
			'taskId': taskId,
		},
		success: function(data){
			if(data){
				layer.msg("任务处理成功",{icon:1})
				setTimeout(function(){
					submitAjax(null,'PROCESSING',null);
				},200);
			}
		},
		error: function(e){
			layer.msg("系统繁忙，请稍后刷新重试！",{icon:2})
		}
	})
}

//领取任务
// function receiveTask(isMusic){
// 	var type1 = $("#type1").is(":checked");
// 	var type2 = $("#type2").is(":checked");
// 	$.ajax({
// 		type:"post",
// 		url:"general/tmc/receiveTask",
// 		data:{
// 			type1:type1,
// 			type2:type2
// 		},
// 		success:function(data){
// 			if(data){
// 				if(isMusic){
//                     playMusic('music1',2);
// 				}
// 				layer.msg("任务领取成功",{icon: 6})
// 			}else{
// 				layer.msg("任务都被人抢走了",{icon: 5})
// 			}
// 			setTimeout(function(){
// 				submitAjax(null,'PROCESSING',null);
// 			},200);
//
// 		},
// 		error:function(){
// 			layer.msg("系统繁忙，请稍后刷新重试！",{icon:2})
// 		}
// 	});
// }

//交接
function transfer(taskId,operatorId){
	TR.select('user',{type: "me"},function(data){
		
		$.ajax({
			type:"post",
			url:"general/tmc/transfer",
			data:{
				taskId:taskId,
				operatorId:operatorId,
				userId:data.id,
				userName:data.fullname
			},
			success:function(rs){
				if(rs){
					showPromptBox("任务已转交给"+data.fullname);
					setTimeout(function(){
						submitAjax(null,'PROCESSING',null);
					},200);
				}else{
					layer.msg("系统错误，请刷新重试！",{icon:2})
				}
			}
		});
	});
}

//显示Model
function showModal(url, data, callback) {

	$('#model1').load(url, data, function(context, state) {
		if ('success' == state && callback) {
			callback();
		}
	});
}

//填写报价
function getTicketData(orderId,taskId,taskType){
	showModal("general/tmc/getOrderDetail",{orderId:orderId,taskId:taskId, taskType:taskType},function(data){
		$('#ticket_model').modal();
		initTicketFrom();
	});
}

//取消订单退款,生成退款单
function createRefund(orderId,taskId){
	$.post('general/tmc/createRefund',{
		orderId : orderId
	},function(refundOrderId){
		getRefundInfo(orderId,taskId,refundOrderId);
	})
}

//填写拒绝退款信息
function getRefuseRefundInfo(orderId,taskId,refundOrderId){
	showModal("general/tmc/getRefuseRefundInfo",{orderId:orderId,taskId:taskId,refundOrderId:refundOrderId},function(data){
		$('#refuse_refund_model').modal();
		initRefuseRefundFrom();
	});
}


//提交退款信息
function submitRefuseRefundForm(){
	$("#refuseRefundForm").submit();
}


//初始化退款form
function initRefuseRefundFrom(){
	$("#refuseRefundForm").validate({
	    rules : {
	    	refuseReason : {
	            required:true
	        }
	    },
	    submitHandler : function(form) {
		   	$.ajax({
				type:"post",
				url:"general/tmc/refuseRefund",
				data:$('#refuseRefundForm').serialize(),
				success:function(data){
					if(data){
						$('#refuse_refund_model').modal('hide');
						showPromptBox("任务处理成功,拒绝退款操作成功");
						setTimeout(function(){
							submitAjax(null,'PROCESSING',null);
						},200);
					}
				},
				error:function(){
					layer.msg("系统繁忙，请刷新重试",{icon:2})
				}
			});		
	    },
	    errorPlacement : setErrorPlacement,
	    success : validateSuccess,
	    highlight : setHighlight
	});
}


//填写退款信息
function getRefundInfo(orderId,taskId,refundOrderId){
	showModal("general/tmc/getRefundInfo",{orderId:orderId,taskId:taskId,refundOrderId:refundOrderId},function(data){
		$('#refund_model').modal();
		initRefundFrom();
	});
}

//提交退款信息
function submitRefundForm(){
	$("#refundForm").submit();
}

//初始化退款form
function initRefundFrom(){
	$("#refundForm").validate({
	    rules : {
	    	providerFee : {
	            required:true,
	            checkTenTwoNumber :true,
	            checkAmount: true
	        },
	        customerFee : {
	            required:true,
	            checkTenTwoNumber :true,
	            checkAmount: true
	        }
	    },
	    messages : {
	        customerFee : {
	            checkAmount: "应退金额应小于可退金额"
	        },
	        providerFee : {
	            checkAmount: "应退金额应小于应付金额"
	        }
	    
	    },
	    submitHandler : function(form) {
		   	$.ajax({
				type:"post",
				url:"general/tmc/doRefund",
				data:$('#refundForm').serialize(),
				success:function(data){
					debugger;
					if(data.code == '0' || data.code == '1'){
						$('#refund_model').modal('hide');
						showPromptBox(data.data);
						setTimeout(function(){
							submitAjax(null,'PROCESSING',null);
						},200);
					}
					else if(data.code == '-1'){
						layer.msg("系统异常，退款失败!",{icon:2})
					}
				},
				error:function(){
					layer.msg("系统繁忙，请刷新重试",{icon:2})
				}
			});		
	    },
	    errorPlacement : setErrorPlacement,
	    success : validateSuccess,
	    highlight : setHighlight
	});
}

//提交报价
function submitGeneralForm(){
	$("#ticketForm").submit();
}

//初始化订单form
function initTicketFrom(){
	$("#ticketForm").validate({
	    rules : {
	    	providerOrderNo : {
	            required:true,
	            minlength:1
	        },
            totalSalePrice : {
	            required:true,
	            checkTenTwoNumber :true
	        },
            servicePrice : {
	            required:true,
	            checkTenTwoNumber :true
	        },
	        serviceDescr :{
	        	required:true
	        },
            totalSettlePrice : {
	        	required:true,
	        	checkTenTwoNumber :true
	        }
	    },
	    submitHandler : function(form) {
	    	
	    	if(taskType == 'WAIT_OFFER'){
	    		$('#ticketForm').attr('action','general/tmc/offer');
	    	}else if(taskType == 'WAIT_DELIVER'){
	    		$('#ticketForm').attr('action','general/tmc/confirmDone');
	    	}
	    	
			$('#ticketForm').ajaxSubmit({success:function(data){
				if(data){
					$('#ticket_model').modal('hide');
					showPromptBox("操作成功!")
					setTimeout(function(){
						submitAjax(null,'PROCESSING',null);
					},200);
				}else{
					layer.msg("系统异常，保存失败!",{icon:2})
				}
	       	 }});
	    },
	    errorPlacement : setErrorPlacement,
	    success : validateSuccess,
	    highlight : setHighlight
	});
}

//确认提示
function showConfirm(msg,_callback){
	var index = layer.alert(msg, {
		  btn: ['确定','取消'],
		  skin: 'layui-layer-lan' //样式类名
		}, function(){
			layer.close(index);
			_callback();
		},function(){
			
		});
}

function submitAjax(pageIndex,taskStatus,taskType){

	if(taskStatus == 'WAIT_PROCESS'){
		return ;
	}

	if(!taskStatus){
        taskStatus = $("#myTask_task_status").val();
	}
	if(!taskType){
        taskType = $("#myTask_task_type").val();
	}
    $("#currentTaskType").val(taskType);
    $("#myTask_pageIndex").val(pageIndex);
    $("#myTask_task_status").val(taskStatus);
    $("#myTask_task_type").val(taskType);

	$.ajax({
		type:"post",
		url:"general/tmc/getMyTaskTable",
		data:{
			taskStatus:taskStatus,
			pageIndex:pageIndex,
			taskType:taskType
		},
		success:function(data){
			$("#myTask").html(data);
		},
		error:function(){
			layer.msg("系统繁忙，请刷新重试",{icon:2})
		}
	});
}

//右下角提示
function showPromptBox(content){
	layer.open({
		  type: 0,//基本层类型
		  title: "系统提示",
		  closeBtn: 0,//不显示关闭x关闭
		  btn:[],//不显示按钮
		  shade: 0,
		  area: ['250px', '200px'],//大小
		  offset: 'rb', //右下角弹出
		  time: 3000, //3秒后自动关闭
		  shift: 2,//动画0-6
		  content: content, 
		});
}