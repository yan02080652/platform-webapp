<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<%@ page import="com.tem.platform.security.authorize.PermissionUtil" %>

<link rel="stylesheet" href="${baseRes }/plugins/sco/css/scojs.css?version=${globalVersion}">
<link rel="stylesheet" href="webpage/flight/tmc/css/tmc.css?version=${globalVersion}">


<ul id="myTab" class="nav nav-tabs" data-trigger="tab">
	<li><a onclick="getMyTask()">我的任务</a></li>
	<c:if test='<%=PermissionUtil.checkTrans("COMMON_TMC_SERVICE","GENERAL_M") == 0%>'>
		<li><a onclick="getAllTask()">全部任务</a></li>
	</c:if>

	<li><a onclick="getOrderList()">订单查询</a></li>
</ul>
<div class="pane-wrapper">
	<!-- 我的任务 -->
	<div id="myTask">
	</div>
	<!-- 全部任务 -->
	<c:if test='<%=PermissionUtil.checkTrans("COMMON_TMC_SERVICE","GENERAL_M") == 0%>'>
		<div id="allTask">
		</div>
	</c:if>
	<!-- 订单查询 -->
	<div id="orderList">
	</div>
</div>
<!-- 模态框 -->
<div id="model1"></div>
<!-- 模态框 -->
<div id="model2"></div>
<%--用来判断上次刷新前是选择的那个tab页，很繁琐，无奈之举 --%>
<input type="hidden" id="myTask_pageIndex"/>
<input type="hidden" id="myTask_task_status"/>
<input type="hidden" id="myTask_task_type"/>
<input type="hidden" id="allTask_pageIndex"/>
<input type="hidden" id="allTask_task_status"/>
<input type="hidden" id="allTask_task_type"/>
<script src="${base }/resource/plugins/My97DatePicker/WdatePicker.js?version=${globalVersion}"></script> 
<script src="resource/plugins/sco/js/sco.panes.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tab.js?version=${globalVersion}"></script>
<script src="resource/plugins/sco/js/sco.tooltip.js?version=${globalVersion}"></script>
<script src="webpage/general/tmc/js/index.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(function(){
	getMyTask();
})
//我的任务Tab
function getMyTask(){
	
	$.ajax({
		type:"get",
		url:"general/tmc/getMyTaskTable",
		data:{
			taskStatus: 'PROCESSING'
		},
		success:function(data){
			$("#myTask").html(data);
		}
	});
}
//全部任务Tab
function getAllTask(){
	
	$.ajax({
		type:"get",
		url:"general/tmc/allTask",
		data:{
			taskStatus: 'WAIT_PROCESS'
		},
		success:function(data){
			$("#allTask").html(data);
		}
	});
}

//订单列表Tab
function getOrderList(){
	$.ajax({
		type:"get",
		url:"general/tmc/orderList",
		data:{
			
		},
		success:function(data){
			$("#orderList").html(data);
		}
	});
}

//加载收入明细
//加载收入明细
function loadIncomeDetail(orderId,PaymentPlanNo){
    layer.open({
        type: 2,
        area: ['600px', '400px'],
        title: "交易记录",
        closeBtn: 1,
        shadeClose: true,
        // shade :0,
        content: "flight/tmc/getIncomeDetail?orderId="+orderId+"&PaymentPlanNo="+PaymentPlanNo,
    });
}
</script>