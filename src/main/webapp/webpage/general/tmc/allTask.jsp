<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/webpage/common/taglib.jsp"%>
<div>
	<input type="hidden" id="currentTaskType" />
	<!-- 状态栏 -->
	<div class="tab pull-left">
		<ul class="fix" id="allTaskStatusTab">
			<li class="tab1 ${active eq 'WAIT_PROCESS' ? 'active' : '' }" data-value="WAIT_PROCESS" onclick="submitAjax_all(null ,'WAIT_PROCESS')">待处理(${waitTaskTypeMap.WAIT_PROCESS==null?0:waitTaskTypeMap.WAIT_PROCESS})</li>
			<li class="tab2 ${active eq 'PROCESSING' ? 'active' : '' }" data-value="PROCESSING" onclick="submitAjax_all(null,'PROCESSING')">处理中(${typeMap.PROCESSING==null?0:typeMap.PROCESSING})</li>
			<li class="tab3 ${active eq 'ALREADY_PROCESSED' ? 'active' : '' }" data-value="ALREADY_PROCESSED" onclick="submitAjax_all(null,'ALREADY_PROCESSED')">近期已处理(${alreadyMap.ALREADY_PROCESSED==null?0:alreadyMap.ALREADY_PROCESSED})</li>
		</ul>
		<div id="g_all_content">
		<%--<c:if test="${active eq 'WAIT_PROCESS' }">--%>
        	<div class="content1" style="display:${active eq 'WAIT_PROCESS' ? 'block' : 'none' }">
            	<p>&nbsp;&nbsp;&nbsp;
                 	<a onclick="submitAjax_all(null,'WAIT_PROCESS','WAIT_OFFER')">待报价(${waitTaskTypeMap.WAIT_OFFER==null?0:waitTaskTypeMap.WAIT_OFFER })</a>
               		<a onclick="submitAjax_all(null,'WAIT_PROCESS','WAIT_URGE_PAY')">待催款(${waitTaskTypeMap.WAIT_URGE_PAY==null?0:waitTaskTypeMap.WAIT_URGE_PAY })</a>
               		<a onclick="submitAjax_all(null,'WAIT_PROCESS','WAIT_REFUND_G')">待退款(${waitTaskTypeMap.WAIT_REFUND_G==null?0:waitTaskTypeMap.WAIT_REFUND_G })</a>
               		<a onclick="submitAjax_all(null,'WAIT_PROCESS','WAIT_DELIVER')">待交付(${waitTaskTypeMap.WAIT_DELIVER==null?0:waitTaskTypeMap.WAIT_DELIVER })</a>  		
             	</p>
         	</div>
        <%--</c:if>--%>
        <%--<c:if test="${active eq 'PROCESSING' }">--%>
        	<div class="content2" style="display:${active eq 'PROCESSING' ? 'block' : 'none' }">
            	<p>&nbsp;&nbsp;&nbsp;
               		<a onclick="submitAjax_all(null,'PROCESSING','WAIT_OFFER')">待报价(${typeMap.WAIT_OFFER==null?0:typeMap.WAIT_OFFER })</a>
               		<a onclick="submitAjax_all(null,'PROCESSING','WAIT_URGE_PAY')">待催款(${typeMap.WAIT_URGE_PAY==null?0:typeMap.WAIT_URGE_PAY })</a>
               		<a onclick="submitAjax_all(null,'PROCESSING','WAIT_REFUND_G')">待退款(${typeMap.WAIT_REFUND_G==null?0:typeMap.WAIT_REFUND_G })</a>
               		<a onclick="submitAjax_all(null,'PROCESSING','WAIT_DELIVER')">待交付(${typeMap.WAIT_DELIVER==null?0:typeMap.WAIT_DELIVER })</a>
             	</p>
         	</div>
        <%--</c:if>--%>
        <%--<c:if test="${active eq 'ALREADY_PROCESSED' }">--%>
        	<div class="content3" style="display:${active eq 'ALREADY_PROCESSED' ? 'block' : 'none' }">
            	<p>&nbsp;&nbsp;&nbsp;
             	    <a onclick="submitAjax_all(null,'ALREADY_PROCESSED','WAIT_OFFER')">报价(${alreadyMap.WAIT_OFFER==null?0:alreadyMap.WAIT_OFFER })</a>&nbsp;&nbsp;&nbsp;&nbsp;
        	       	<a onclick="submitAjax_all(null,'ALREADY_PROCESSED','WAIT_URGE_PAY')">催款(${alreadyMap.WAIT_URGE_PAY==null?0:alreadyMap.WAIT_URGE_PAY })</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax_all(null,'ALREADY_PROCESSED','WAIT_REFUND_G')">退款(${alreadyMap.WAIT_REFUND_G==null?0:alreadyMap.WAIT_REFUND_G })</a>&nbsp;&nbsp;&nbsp;&nbsp;
                	<a onclick="submitAjax_all(null,'ALREADY_PROCESSED','WAIT_DELIVER')">交付(${alreadyMap.WAIT_DELIVER==null?0:alreadyMap.WAIT_DELIVER })</a>
             	</p>
         	</div>
        <%--</c:if>--%>
		</div>
	</div>
	<!-- 任务类型 -->
	<input type="hidden" id="task_type" value="WAIT_PROCESS"/>
	<input type="hidden" id="task_status" />
	
	<!-- 搜索条件 -->
	<div>
		<form class="form-inline" method="post" id="orderSearchForm">
			<nav>
				<div class=" form-group">
					<input  id="search_Field2" type="text" class="form-control" placeholder="订单号/处理人" style="width: 300px"/>				
					<button type="button" class="btn btn-default" onclick="submitAjax_all()">搜索</button>
					<button type="button" class="btn btn-default" onclick="resetForm()">重置</button>
				</div>
			</nav>
		</form>
	</div>
	<div>
		<label><font color="red" size="4">刷新倒计时:<span id="countDown">10</span>秒</font></label>
	</div>	

</div>


<!-- 任务列表 -->
<div style="margin-top: 10px">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th style="width: 20%">服务信息</th>
				<th style="width: 20%">旅客信息</th>
				<th style="width: 20%">收支信息</th>
				<th style="width: 20%">处理信息</th>
				<th style="width: 20%">供应信息</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pageList.list }" var="orderTask">
				<tr>
					<td colspan="5">
						<span>企业：${orderTask.orderDto.cName } </span> 
						<span>预订人：${orderTask.orderDto.userName }</span>
						<span>订单号：<font color="blue"><a href="/order/generalDetail/${orderTask.orderDto.id }" target="_Blank">${orderTask.orderDto.id }</a></font></span>
						<span>来源:${originMap[orderTask.orderDto.orderOriginType]}</span>
						<span>标签：${orderTask.orderDto.orderShowStatus.message }</span>
						<span> 下单时间：<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${orderTask.orderDto.createDate }" /></span>
					   <%--  <span><a onclick="logList('${orderTask.orderDto.id}')">日志</a></span> --%>
				    </td>
				</tr>
				<tr>
					<td>
						<span>时间：${orderTask.orderDto.travellerDate }</span></br>
						<span>行程：${orderTask.orderDto.destination }</span></br>
						<span>服务品类：${orderTask.orderDto.serviceName }</span>
					</td>
					<td>
						<span>出行人数 : ${orderTask.orderDto.travellerCount }(人)</span></br>
					</td>
					<td>
						<c:choose>
							<c:when test="${not empty orderTask.orderDto.totalOrderPrice }">
								<span>应收￥${orderTask.orderDto.totalOrderPrice }</span>
								<c:if test="${not empty orderTask.orderDto.paymentPlanNo }">
									<span>已收</span><br/>
									<span><a onclick="loadIncomeDetail('${orderTask.orderDto.id}','${orderTask.orderDto.paymentPlanNo}')">明细</a></span>
								</c:if>
								<c:if test="${empty orderTask.orderDto.paymentPlanNo}">
                                    <span>未收</span>
								</c:if>
							</c:when>
							<c:otherwise>
								待报价
							</c:otherwise>
						</c:choose>
					</td>
					<td>
						<span>任务：${orderTask.taskType.message } ${orderTask.taskStatus.message }</span><br/>
						<span>${orderTask.operatorName }</span><br/>
						<c:if test="${orderTask.taskStatus eq 'WAIT_PROCESS' }">
							<a onclick="transfer('${orderTask.id}','')">指派</a>					
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'PROCESSING' }">
							<span>
								<!-- 填写报价 -->
								<c:if test="${orderTask.taskType eq 'WAIT_OFFER' }">
									 <a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id }','WAIT_OFFER')">填写报价</a> | 
									 <a onclick="cancelOrder('${orderTask.orderDto.id}','${orderTask.id }')">取消订单</a>
								</c:if>
								 
								 <!-- 退款审核任务 -->
								 <c:if test="${orderTask.taskType eq 'WAIT_REFUND_G' }">
									 <a onclick="getRefundInfo('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.refundOrderId }')">通过退款</a>
									 <a onclick="getRefuseRefundInfo('${orderTask.orderDto.id}','${orderTask.id }','${orderTask.refundOrderId }')">拒绝退款</a>
								 </c:if>
								 
								<!-- 交付任务 -->
								<c:if test="${orderTask.taskType eq 'WAIT_DELIVER' }">
									 <a onclick="getTicketData('${orderTask.orderDto.id}','${orderTask.id }','WAIT_DELIVER')">服务完成</a> | 
									 <a onclick="createRefund('${orderTask.orderDto.id}','${orderTask.id }')">取消退款</a>
								</c:if>							 
								 
								 
							 </span>
						</c:if>
						<c:if test="${orderTask.taskStatus eq 'ALREADY_PROCESSED' }">
							<span>处理结果：${orderTask.result}</span>
						</c:if>
					 </td>
				     <td>
						 <c:if test="${not empty orderTask.orderDto.providerName }">
							 ${orderTask.orderDto.providerName }&nbsp;&nbsp;<br/>
								<a data-trigger="tooltip" data-content="${orderTask.orderDto.providerOrderNo }">订单号</a><br/>
						 </c:if>
					 </td>					 
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
</div>

<div class="pagin" style="margin-bottom: 22px">
	<jsp:include page="../../common/pagination_ajax.jsp?callback=reloadAllTaskTable"></jsp:include>
</div>
<script src="webpage/general/tmc/js/allTask.js?version=${globalVersion}"></script>
<script type="text/javascript">
$(function(){
	var status = "${taskStatus}";
	console.log(status)
	$("#allTaskStatusTab li").each(function(){
		if($(this).attr("data-value")==status){
		
			$(this).addClass("active");
			$(this).siblings().removeClass("active");
			if(status == 'WAIT_PROCESS'){
				 $('#g_all_content .content1').show();
			     $('#g_all_content .content2').hide();
			     $('#g_all_content .content3').hide();
			}else if(status=='PROCESSING'){
				 $('#g_all_content .content1').hide();
				 $('#g_all_content .content2').show();
			     $('#g_all_content .content3').hide();
			}else if(status=='ALREADY_PROCESSED'){
				 $('#g_all_content .content1').hide();
			     $('#g_all_content .content2').hide();
			     $('#g_all_content .content3').show();
			}
			return false;
		}
	});
});
</script>