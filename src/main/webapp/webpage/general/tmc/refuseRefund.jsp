<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/webpage/common/taglib.jsp"%>
<div class="modal fade" id="refuse_refund_model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog" style="width: 800px;">
      <div class="modal-content" style="width: 800px;">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
            		<h4 class="modal-title">
            			填写拒绝退款信息
            		</h4>
         </div>
         <div class="modal-body">
	         <form  class="form-horizontal" id="refuseRefundForm">
	         	<input type="hidden" name="taskId" value="${taskId }"/>
	         	<input type="hidden" name="orderId" value="${orderId}">
	         	<input type="hidden" name="refundOrderId" value="${refundOrderId}">	   
				<div class="form-group">
				      <label  class="col-sm-2 control-label">拒绝退款原因</label>
				      <div class="col-sm-10" >
				      	<textarea rows="3" class="form-control" name="refuseReason"></textarea>
				      </div>
				</div>
			 </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="submitRefuseRefundForm()">拒绝退款</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
         </div>
      </div>
</div>
</div>