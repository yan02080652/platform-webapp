$(function () {
    $("#excelFile").fileinput({
        language: "zh",//设置语言
        showCaption: true,//是否显示标题
        showUpload: false, //是否显示上传按钮
        showPreview: false, //是否显示预览
        browseClass: "btn btn-primary", //按钮样式
        allowedFileExtensions: ["xls", "xlsx"], //接收的文件后缀
        previewFileIcon: '<i class="glyphicon glyphicon-file"></i>',
        allowedPreviewTypes: null,
        previewFileIconSettings: {
            'docx': '<i class="glyphicon glyphicon-file"></i>',
            'xlsx': '<i class="glyphicon glyphicon-file"></i>',
            'pptx': '<i class="glyphicon glyphicon-file"></i>',
            'jpg': '<i class="glyphicon glyphicon-picture"></i>',
            'pdf': '<i class="glyphicon glyphicon-file"></i>',
            'zip': '<i class="glyphicon glyphicon-file"></i>',
        },
    });

    $(".partner").click(function () {
        TR.select('partner', {
            //type:0//固定参数
        }, function (data) {
            $("#pid").val(data.id);
            $("#pname").val(data.name);
            $("#userId").val("");
            $("#userName").val("");
        });
    });

    $(".user").click(function () {
        var pid = $("#pid").val();
        if (!pid) {
            layer.msg("请先选择企业！")
            return;
        }
        TR.select('user', {
            partnerId: pid,
        }, function (data) {
            $("#userId").val(data.id);
            $("#userName").val(data.fullname);
        });
    });

    initForm();
})

function initForm() {
    $("#importForm").validate({
        ignore: '.ignore',
        rules: {
            pid: {
                required: true,
            },
            userId: {
                required: true,
            }
        },
        submitHandler: function (form) {
            if ($("input[type='file']").val() == "") {
                layer.tips("请选择", $("#excelFile"), {
                    tips: [2, '#cf8934'],
                    tipsMore: true,
                });
                return;
            }

            var index = layer.load();
            $('#importForm').ajaxSubmit({
                success: function (data) {
                    layer.close(index);
                    if (data.code == '0') {
                        layer.alert("数据导入成功", function () {
                             window.location.reload();
                        });
                    } else if (data.code == '1') {
                        layer.alert(data.msg);
                    }
                }
            });
        },
        errorPlacement: function (error, element) {
            layer.tips("请选择", $(element).next().next(), {
                tips: [2, '#cf8934'],
                tipsMore: true,
            });
        }
    });
}