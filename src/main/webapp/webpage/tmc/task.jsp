<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/common/taglib.jsp" %>
<link rel="stylesheet" href="webpage/tmc/common/css/base.css">
<audio id="music1" src="resource/mp3/1.mp3" loop="loop">你的浏览器不支持audio标签。</audio>
<audio id="music2" src="resource/mp3/2.mp3" loop="loop">你的浏览器不支持audio标签。</audio>
<style>
    .tabL .tabItem {
        /*margin: 0 10px;*/
        border-bottom: 1px solid #fff;
        cursor: pointer;
        width: 100px;
        line-height: 40px;
        text-align: center;
    }

    .tabL li:last-of-type {
        border: none;
    }

    .tabL {
        background-color: #2c2c2c;
        color: #fff;
        height: 100%;
        min-height: 600px;
        margin-right: 20px;
    }

    .activeL {
        color: #00A7F8;
        background-color: #fff;
    }

    /*右*/
    .main {
        height: 100%;
        min-height: 600px;
        width: 88%;
    }

    .tab_C {
        width: 400px;
        background-color: #a9a9a9;
        color: #fff;
        margin-top: 20px;
        border-top: 1px solid #c3c3c3;
    }

    .tabCItem {
        float: left;
        width: 80px;
        text-align: center;
        line-height: 40px;
        cursor: pointer;
    }

    .processingCount {
        color: red;
    }

    .tab_h_item {
        position: relative;
        text-align: center;
        float: left;
        line-height: 24px;
        cursor: pointer;

    }
    .tab_h_item p {
        padding-right: 8px;
    }

    .tab_h_item ul {
        position: absolute;
        width: 80px;
        text-align: center;
        color: #000;
        border: 1px solid #c0c0c0;
        z-index: 99;
        background-color: #fff;
    }

    .tab_h_item ul li:hover {
        background-color: #c0c0c0;
        color: #fff;
    }

    .hide {
        display: none;
    }

    .tab_h_item:hover ul {
        display: block;
    }

    a {
        color: #00a0e9;
    }
</style>
<div class="wrap fix">
    <div class="tabL l">
        <div id="bizMenu">
            <c:if test="${skill.doFlight eq 1}">
                <p class="tabItem activeL" url="/flight/tmc/index">国内机票<span class="processingCount"
                                                                             name="FLIGHT">(${bizProcessingMap.FLIGHT == null ? 0 : bizProcessingMap.FLIGHT })</span>
                </p>
            </c:if>
            <!--doInterFlight-->
            <!--doInterFlightdoInterFlight-->
            <c:if test="${skill.doFlightOut eq 1}">
                <p class="tabItem" url="/internationalflight/tmc/index">国际机票<span class="processingCount"
                                                                             name="INTERNATIONAL_FLIGHT">(${bizProcessingMap.INTERNATIONAL_FLIGHT == null ? 0 : bizProcessingMap.INTERNATIONAL_FLIGHT })</span>
                </p>
            </c:if>
            <c:if test="${skill.doHotel eq 1}">
                <p class="tabItem" url="/hotel/tmc/index">酒店任务<span class="processingCount"
                                                                    name="HOTEL">(${bizProcessingMap.HOTEL == null ? 0 : bizProcessingMap.HOTEL })</span>
                </p>
            </c:if>
            <c:if test="${skill.doTrain eq 1}">
                <p class="tabItem" url="/train/tmc/index">火车任务<span class="processingCount"
                                                                    name="TRAIN">(${bizProcessingMap.TRAIN == null ? 0 : bizProcessingMap.TRAIN })</span>
                </p>
            </c:if>
            <c:if test="${skill.doInsurance eq 1}">
                <p class="tabItem" url="insurance/tmc/index">保险任务<span class="processingCount"
                                                                       name="INSURANCE">(${bizProcessingMap.INSURANCE_MAIN == null ? 0 : bizProcessingMap.INSURANCE_MAIN })</span>
                </p>
            </c:if>
            <c:if test="${skill.doGeneral eq 1}">
                <p class="tabItem" url="/general/tmc/index">需求任务<span class="processingCount"
                                                                      name="GENERAL">(${bizProcessingMap.GENERAL == null ? 0 : bizProcessingMap.GENERAL })</span>
                </p>
            </c:if>
            <c:if test="${skill.doCar eq 1}">
                <p class="tabItem" url="/car/tmc/index">用车任务
                    <span class="processingCount" name="CAR">(${bizProcessingMap.CAR == null ? 0 : bizProcessingMap.CAR })</span>
                </p>
            </c:if>
            <p class="tabItem" url="/flight/tmc/searchUserPage">代客下单</p>
        </div>
    </div>
    <div class="main l">
        <div class="head_C" id="topCountDiv">
            <div class="pull-right">
                <div class="tab_head fix">
                    <div class="tab_h_item">
                        <span id="statusTip">自动领取已关闭</span> 【<a onclick="autoRefresh()" id="switchBtn">开启</a>】
                    </div>
                    <c:if test="${skill.doFlight eq 1}">
                        <div class="tab_h_item">
                            <p><input type="checkbox" checked="checked" id="FLIGHT"/>国内机票
                                <span>
                                     (${bizWaitProcessMap.COUNT.FLIGHT==null?0:bizWaitProcessMap.COUNT.FLIGHT})
                                </span>
                            </p>
                            <ul class="hide">
                                <li><input type="checkbox" checked="checked" task_type="WAIT_TICKET" class="taskType"/>待出票
                                    <span>
                                         (${bizWaitProcessMap.FLIGHT.WAIT_TICKET==null?0:bizWaitProcessMap.FLIGHT.WAIT_TICKET})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_URGE" class="taskType"/>待催单
                                    <span>
                                       (${bizWaitProcessMap.FLIGHT.WAIT_URGE==null?0:bizWaitProcessMap.FLIGHT.WAIT_URGE})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="REFUND_TICKET_AUDIT"
                                           class="taskType"/>退审核
                                    <span>
                                        (${bizWaitProcessMap.FLIGHT.REFUND_TICKET_AUDIT==null?0:bizWaitProcessMap.FLIGHT.REFUND_TICKET_AUDIT})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_CHANGE" class="taskType"/>待改签
                                    <span>
                                       (${bizWaitProcessMap.FLIGHT.WAIT_CHANGE==null?0:bizWaitProcessMap.FLIGHT.WAIT_CHANGE})
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </c:if>
                    <!--International-->
                    <c:if test="${skill.doFlightOut eq 1}">
                        <div class="tab_h_item">
                            <p><input type="checkbox" checked="checked" id="INTERNATIONAL_FLIGHT"/>国际机票
                                <span>
                                     (${bizWaitProcessMap.COUNT.INTERNATIONAL_FLIGHT==null?0:bizWaitProcessMap.COUNT.INTERNATIONAL_FLIGHT})
                                </span>
                            </p>
                            <ul class="hide">
                                <li><input type="checkbox" checked="checked" task_type="WAIT_OFFER" class="taskType"/>待核价
                                    <span>
                                         (${bizWaitProcessMap.INTERNATIONAL_FLIGHT.WAIT_OFFER==null?0:bizWaitProcessMap.INTERNATIONAL_FLIGHT.WAIT_OFFER})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_TICKET" class="taskType"/>待出票
                                    <span>
                                       (${bizWaitProcessMap.INTERNATIONAL_FLIGHT.WAIT_TICKET==null?0:bizWaitProcessMap.INTERNATIONAL_FLIGHT.WAIT_TICKET})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_REFUND"
                                           class="taskType"/>待退款
                                    <span>
                                        (${bizWaitProcessMap.INTERNATIONAL_FLIGHT.WAIT_REFUND==null?0:bizWaitProcessMap.INTERNATIONAL_FLIGHT.WAIT_REFUND})
                                    </span>
                                </li>

                            </ul>
                        </div>
                    </c:if>
                    <c:if test="${skill.doHotel eq 1}">
                        <div class="tab_h_item">
                            <p><input type="checkbox" checked="checked" id="HOTEL"/>酒店
                                <span>
                                    (${bizWaitProcessMap.COUNT.HOTEL==null?0:bizWaitProcessMap.COUNT.HOTEL})
                                </span>
                            </p>
                            <ul class="hide">
                                <li><input type="checkbox" checked="checked" task_type="WAIT_URGE" class="taskType"/>待催单
                                    <span>
                                        (${bizWaitProcessMap.HOTEL.WAIT_URGE==null?0:bizWaitProcessMap.HOTEL.WAIT_URGE})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_CONFIRM" class="taskType"/>待确认
                                    <span>
                                        (${bizWaitProcessMap.HOTEL.WAIT_CONFIRM==null?0:bizWaitProcessMap.HOTEL.WAIT_CONFIRM})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="CONFIRM_AGAIN" class="taskType"/>酒店确认
                                    <span>
                                        (${bizWaitProcessMap.HOTEL.CONFIRM_AGAIN==null?0:bizWaitProcessMap.HOTEL.CONFIRM_AGAIN})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_TICKET" class="taskType"/>待出票
                                    <span>
                                     (${bizWaitProcessMap.HOTEL.WAIT_TICKET==null?0:bizWaitProcessMap.HOTEL.WAIT_TICKET})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_REFUND" class="taskType"/>待退款
                                    <span>
                                       (${bizWaitProcessMap.HOTEL.WAIT_REFUND==null?0:bizWaitProcessMap.HOTEL.WAIT_REFUND})
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </c:if>
                    <c:if test="${skill.doTrain eq 1}">
                        <div class="tab_h_item">
                            <p><input type="checkbox" checked="checked" id="TRAIN"/>火车
                                <span>
                                    (${bizWaitProcessMap.COUNT.TRAIN==null?0:bizWaitProcessMap.COUNT.TRAIN})
                                </span>
                            </p>
                            <ul class="hide">
                                <li><input type="checkbox" checked="checked" task_type="WAIT_TICKET" class="taskType"/>待出票
                                    <span>
                                       (${bizWaitProcessMap.TRAIN.WAIT_TICKET==null?0:bizWaitProcessMap.TRAIN.WAIT_TICKET})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_URGE" class="taskType"/>待催单
                                    <span>
                                        (${bizWaitProcessMap.TRAIN.WAIT_URGE==null?0:bizWaitProcessMap.TRAIN.WAIT_URGE})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="REFUND_TICKET_AUDIT"
                                           class="taskType"/>退审核
                                    <span>
                                        (${bizWaitProcessMap.TRAIN.REFUND_TICKET_AUDIT==null?0:bizWaitProcessMap.TRAIN.REFUND_TICKET_AUDIT})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_REFUND" class="taskType"/>待退款
                                    <span>
                                         (${bizWaitProcessMap.TRAIN.WAIT_REFUND==null?0:bizWaitProcessMap.TRAIN.WAIT_REFUND})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_CHANGE" class="taskType"/>待改签
                                    <span>
                                         (${bizWaitProcessMap.TRAIN.WAIT_CHANGE==null?0:bizWaitProcessMap.TRAIN.WAIT_CHANGE})
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </c:if>
                    <c:if test="${skill.doInsurance eq 1}">
                        <div class="tab_h_item">
                            <p><input type="checkbox" checked="checked" id="INSURANCE_MAIN"/>保险
                                <span>
                                    (${bizWaitProcessMap.COUNT.INSURANCE_MAIN==null?0:bizWaitProcessMap.COUNT.INSURANCE_MAIN})
                                </span>
                            </p>
                            <ul class="hide">
                                <li><input type="checkbox" checked="checked" task_type="WAIT_INSURE" class="taskType"/>待投保
                                    <span>
                                        (${bizWaitProcessMap.INSURANCE_MAIN.WAIT_INSURE==null?0:bizWaitProcessMap.INSURANCE_MAIN.WAIT_INSURE})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_REFUND_INSURE"
                                           class="taskType"/>待撤保
                                    <span>
                                        (${bizWaitProcessMap.INSURANCE_MAIN.WAIT_REFUND_INSURE==null?0:bizWaitProcessMap.INSURANCE_MAIN.WAIT_REFUND_INSURE})
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </c:if>
                    <c:if test="${skill.doGeneral eq 1}">
                        <div class="tab_h_item">
                            <p><input type="checkbox" checked="checked" id="GENERAL"/>通用
                                <span>
                                     (${bizWaitProcessMap.COUNT.GENERAL==null?0:bizWaitProcessMap.COUNT.GENERAL})
                                </span>
                            </p>
                            <ul class="hide">
                                <li><input type="checkbox" checked="checked" task_type="WAIT_OFFER" class="taskType"/>待报价
                                    <span>
                                        (${bizWaitProcessMap.GENERAL.WAIT_OFFER==null?0:bizWaitProcessMap.GENERAL.WAIT_OFFER})
                                    </span>
                                </li>
                                <li><input type="checkbox" checked="checked" task_type="WAIT_REFUND_G"
                                           class="taskType"/>待退款
                                    <span>
                                         (${bizWaitProcessMap.GENERAL.WAIT_REFUND_G==null?0:bizWaitProcessMap.GENERAL.WAIT_REFUND_G})
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </c:if>
                    <div class="tab_h_item">
                        <button type="button" class="btn btn-primary" onclick="receiveTask()">领取任务</button>
                    </div>
                </div>

            </div>
        </div>
        <div class="content" id="contentDiv">
        </div>
    </div>
</div>


<script>

    //=========================================sse======================================================
    if (!!window.EventSource) {
        var source = new EventSource('/tmc/hasNewTask');
        var lastTaskId = -1;
        source.addEventListener('message', function (e) {
            var id = parseInt(e.data);
            if (lastTaskId < id) {
                showPromptBox("有新任务产生，请及时处理！");
            }
            lastTaskId = id;

        });
    } else {
        console.log("你的浏览器不支持SSE");
    }
    //===============================================================================================

    //自动领取标志
    var isOpen = true;
    (function () {
        window.onload = function () {
            if (isOpen) {
                $("#switchBtn").text("关闭");
                $("#statusTip").text("自动领取已开启");
            } else {
                $("#switchBtn").text("开启");
                $("#statusTip").text("自动领取已关闭");
            }
            setTimeout(autoReceive, 10 * 1000);

            //定时任务扫描全部任务是否有未领取的
            checkAllTaskInWaitProcess();

            //每隔一分钟扫描领取的任务是否有超时
            checkMyTaskInProcess();

            $('.tab_h_item').mousemove(function () {
                $(this).children('ul').removeClass('hide')
            }).mouseleave(function () {
                $(this).children('ul').addClass('hide')
            })

            $(".tab_h_item input[type='checkbox']").change(function () {
                var curDom = $(this);
                if (!curDom.hasClass("taskType")) {
                    curDom.parent().next().find("input[type='checkbox']").each(function () {
                        $(this).prop("checked", curDom.is(":checked"));
                    })
                } else {
                    var flag = false;
                    curDom.parents("ul").find("input[type='checkbox']").each(function () {
                        if ($(this).is(":checked")) {
                            flag = true;
                        }
                    })
                    curDom.parents(".tab_h_item").find("input:eq(0)").prop("checked", flag);
                }
            })

            var index = layer.load()
            //默认加载第一个标签页
            var url = $("#bizMenu").find("p:eq(0)").attr("url");

            $.ajax({
                type: "get",
                url: url,
                data: {},
                success: function (data) {
                    layer.close(index)
                    $("#contentDiv").html(data);
                },
                error: function () {
                    layer.close(index)
                    layer.msg("系统异常，请刷新重试", {icon: 2, offset: 't', time: 1000})
                }
            });

            //点击切换
            $('.tabItem').click(function () {
                $('.tabItem').removeClass('activeL');
                $(this).addClass('activeL')
                var url = $(this).attr("url");
                var l = layer.load();
                $.ajax({
                    type: "get",
                    url: url,
                    data: {},
                    success: function (data) {
                        layer.close(l)
                        $("#contentDiv").html(data);
                    },
                    error: function () {
                        layer.close(l)
                        layer.msg("系统异常，请刷新重试", {icon: 2, offset: 't', time: 1000})
                    }
                });
            });
        }
    }())

    //领取任务
    function receiveTask() {
        var taskType = [];

        $(".taskType").each(function (i, n) {
            if ($(this).is(":checked")) {
                var type = $(this).attr("task_type");
                var bizType = $(this).parents("ul").prev().children().first().attr("id");
                taskType.push(bizType + "." + type);
            }
        })


        if (taskType.length == 0) {
            return;
        }
        var layer_index = layer.load(0);
        $.ajax({
            type: "post",
            url: "tmc/receiveTask",
            data: {
                taskType: taskType,
            },
            success: function (data) {
                layer.close(layer_index);
                if (data) {
//                    playMusic('music1', 3);
                    layer.msg("任务领取成功，请及时处理！", {icon: 6, offset: 't', time: 1000})
                }

                var pageIndex = $("#myTask_pageIndex").val();
                var taskStatus = $("#myTask_task_status").val();
                var taskType = $("#myTask_task_type").val();

                if (taskStatus == '') {
                    taskStatus = 'PROCESSING';
                }
                setTimeout(function () {
                    if (typeof submitAjax === "function") {
                        submitAjax(pageIndex, taskStatus, taskType);
                    }
                }, 200);
            },
            error: function () {
                layer.close(layer_index);
                layer.msg("系统繁忙，请稍后刷新重试！", {icon: 2, offset: 't', time: 1000})
            }
        });
    }
    function autoRefresh() {
        if (isOpen) {
            isOpen = false;
            $("#switchBtn").text("开启");
            $("#statusTip").text("自动领取已关闭");
        } else {
            isOpen = true;
            $("#switchBtn").text("关闭");
            $("#statusTip").text("自动领取已开启");
        }
    }

    function autoReceive() {
        if (isOpen) {
            // 最多自动领取5个任务
            $.get("tmc/autoReceive", function (rs) {
                if (rs) {
                    receiveTask();
                }
            });
        }
        setTimeout(autoReceive, 10 * 1000);
    }
    function checkAllTaskInWaitProcess() {
        //全部任务列表页面警报：1.任务创建10s后无人领取。
        var rs;
        $.ajax({
            url: "tmc/checkAllTaskInWaitProcess",
            type: "get",
            async: false,
            success: function (data) {
                rs = data;
            }
        })
        if (rs) {//需要提醒客服
            playMusic('music1', 2);
        }

        setTimeout(checkAllTaskInWaitProcess, 10 * 1000);
    }
    function checkMyTaskInProcess() {
        var rs;
        $.ajax({
            url: "tmc/checkMyTaskInProcess",
            type: "get",
            async: false,
            success: function (data) {
                rs = data;
            }
        })
        if (rs) {//需要提醒客服
            playMusic('music2', 2);
        }

        setTimeout(checkMyTaskInProcess, 60 * 1000);
    }

    /**
     * 播放提示音
     * @param id 音乐id
     * @param second 持续时间：秒
     */
    function playMusic(id, second) {
        var music = document.getElementById(id);
        music.currentTime = 0;
        music.play();
        setTimeout(function () {
            music.pause();
        }, second * 1000)
    }

    //右下角提示
    function showPromptBox(content) {
        layer.open({
            type: 0,//基本层类型
            title: "系统提示",
            closeBtn: 0,//不显示关闭x关闭
            btn: [],//不显示按钮
            shade: 0,
            area: ['250px', '200px'],//大小
            offset: 'rb', //右下角弹出
            time: 3000, //3秒后自动关闭
            shift: 2,//动画0-6
            content: content,
        });
    }
</script>