var date = {
	/**
	 * 时间增加
	 * @param time 时间字符串 'yyyy-MM-dd'或者'yyyy-MM-dd HH:mm:ss'
	 * @param strInterval 增加的类型 s:秒，n:分钟，h:小时，d:天,w：周，m:月，q:季度(3个月)，y:年
	 * @param Number 数值
	 * @returns {Date} 
	 */
	add : function(time,strInterval, Number){
		var dtTmp = time;
		switch (strInterval) {   
			case 's' :return new Date(Date.parse(dtTmp) + (1000 * Number));  
			case 'n' :return new Date(Date.parse(dtTmp) + (60000 * Number));  
			case 'h' :return new Date(Date.parse(dtTmp) + (3600000 * Number));  
			case 'd' :return new Date(Date.parse(dtTmp) + (86400000 * Number));  
			case 'w' :return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number)); 
			case 'q' :return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number*3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
			case 'm' :return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
			case 'y' :return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
		}
	},
	
	/**
	 * 时间减少
	 * @param time
	 * @param strInterval
	 * @param Number
	 * @returns {Date}
	 */
	reduce : function(time,strInterval, Number){
		  var dtTmp = time;
		    switch (strInterval) {   
		        case 's' :return new Date(Date.parse(dtTmp) - (1000 * Number));  
		        case 'n' :return new Date(Date.parse(dtTmp) - (60000 * Number));  
		        case 'h' :return new Date(Date.parse(dtTmp) - (3600000 * Number));  
		        case 'd' :return new Date(Date.parse(dtTmp) - (86400000 * Number));  
		        case 'w' :return new Date(Date.parse(dtTmp) - ((86400000 * 7) * Number)); 
		        case 'q' :return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) - Number*3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
		        case 'm' :return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) - Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
		        case 'y' :return new Date((dtTmp.getFullYear() - Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds()); 
		    }
	},
	
	/**
	 * 时间格式化
	 * <pre>
	 * 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
	 * 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
	 * </pre>
	 * <pre>
	 * date.format(new Date(),"yyyy-MM-dd HH:mm:ss.S") ==> 2006-07-02 08:09:04.423 
	 * date.format(new Date(),"yyyy-M-d H:m:s.S") ==> ==> 2006-7-2 8:9:4.18
	 * </pre>
	 * @param fmt
	 * @returns
	 */
	format : function(date,fmt){
		var o = {
			"M+" : date.getMonth() + 1, //月份   
			"d+" : date.getDate(), //日   
			"H+" : date.getHours(), //小时   
			"m+" : date.getMinutes(), //分   
			"s+" : date.getSeconds(), //秒   
			"q+" : Math.floor((date.getMonth() + 3) / 3), //季度   
			"S" : date.getMilliseconds() //毫秒   
		};
		if (/(y+)/.test(fmt)){
			fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
		}
		for ( var k in o){
			if (new RegExp("(" + k + ")").test(fmt)){
				fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]): (("00" + o[k]).substr(("" + o[k]).length)));
			}
		}
		return fmt;
	},
	/**
	 * 计算天数差的函数，通用  sDate1和sDate2是yyyy-MM-dd格式  
	 */
	diffDay: function(sDate1, sDate2){
		var aDate, oDate1, oDate2, iDays
		aDate = sDate1.split("-")
		oDate1 = new Date(aDate[0] , aDate[1]-1 ,aDate[2]) //转换为MM-dd-yyyy格式  
		aDate = sDate2.split("-") 
		oDate2 = new Date(aDate[0] , aDate[1]-1 , aDate[2]) 
		iDays = parseInt(Math.abs(oDate1 - oDate2) / 1000 / 60 / 60 / 24) //把相差的毫秒数转换为天数  
		return iDays
	}
}
