$("#sel3 span.sp-date").click(function() {
	//更新时间，然后发送ajax请求
	initDate($(this).attr("data-value"));
	reloadRefundOrder();

	$(this).addClass("selected").siblings("span.sp-date").removeClass("selected");
});

$("#sel3 span.sp-traveltype").click(function(){
    $(this).addClass("selected").siblings("span.sp-traveltype").removeClass("selected");
    reloadRefundOrder();
});


$("#sel1 span").click(function() {
	//更新类型，然后发送ajax请求
	$(this).addClass("selected").siblings().removeClass("selected");
	var ss = $(this).attr("data-value");
	$("#sel1 .selected").attr("data-value", ss);
	reloadRefundOrder();
});

/*$("#sel1 span").click(function () {
	if($(this).hasClass("selected")){
		$(this).removeClass("selected");
	}else{
		 $(this).addClass("selected");
		 
		 if($(this).attr("data-value") != 0){//选择了非全部的 讲全部去掉
			 $("#allOrderBizType").removeClass("selected");
		 }else{//选择了全部
			 $(this).addClass("selected").siblings().removeClass("selected");
		 }
	}
});*/

$("#sel2 span").click(function() {
	if ($(this).hasClass("selected")) {
		$(this).removeClass("selected");
	} else {
		$(this).addClass("selected");

		if ($(this).attr("data-value") != 0) {//选择了非全部的 讲全部去掉
			$("#allRefundStatus").removeClass("selected");
		} else {//选择了全部
			$(this).addClass("selected").siblings().removeClass("selected");
		}
	}
});

$('#begin').blur(function() {
	$('#sel3 span.sp-date').removeClass("selected");
});
$('#end').blur(function() {
	$('#sel3 span.sp-date').removeClass("selected");
});

//初始化
$(function() {
	initDate(0);
	$("#oneMonth").addClass("selected");
	var state = $("#state").val();
	if (state) {
		$("span[data-value='" + state + "']").addClass("selected");
	} else {
		$("#allOrderBizType").addClass("selected");
		$("#allRefundStatus").addClass("selected");
	}
	reloadRefundOrder();
});
//分页调用的方法
function reloadRefundOrder(pageIndex) {
	//状态
	var type = $("#sel1 .selected").attr("data-value");
	var orderStatus = "";
	$("#sel2 .selected").each(function() {
		if ($(this).attr("data-value") == 0) {
			orderStatus = "0";
		} else if (orderStatus != '0') {
			orderStatus = orderStatus + "" + $(this).attr("data-value") + ',';
		}
	});

	if (orderStatus != '') {
		orderStatus = orderStatus.substring(0, orderStatus.length - 1);
	}
	//出行性质
	var travelType ="";
    $("#sel3 span.sp-traveltype").each(function(){
        if($(this).hasClass("selected")){
            travelType=$(this).attr("data-value");
        }
    })
	//关键字
	var keyword = $("#keyWords").val().trim();

	//开始日期
	var startDate = $("#begin").val();
	//结束日期
	var endDate = $("#end").val();
	var queryBpId = $("#queryBpId").val();
	var isCompPay = $('#compPay').is(':checked');

	var tmcId = '';
	if ($("#queryBpTmcId").val()) {
		tmcId = $("#queryBpTmcId").val();
	}

	var index = layer.load();
	$.ajax({
		url : "order/refundOrderList",
		data : {
			startDate : startDate,
			endDate : endDate,
			keyword : keyword,
			orderStatus : orderStatus,
			travelType : travelType,
			type : type,
			pageIndex : pageIndex,
			queryBpId : queryBpId,
			isCompPay : isCompPay,
			tmcId : tmcId,
		},
		type : "POST",
		success : function(data) {
			layer.close(index);
			$("#refundOrderTable").html(data);
			$('.drop_ul').hover(function() {
				$('.drop_ul').removeClass('drop_ul1');
				$(this).addClass('drop_ul1');
			});

		},
		error : function() {
			layer.close(index);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
}
function searchDetail(that) {
	window.open($(that).siblings(".lastTd").find("a").attr("attr"));
}

function openRefundDetail(url) {
	window.open(url);
}

function reloadExportOrder() {
	//状态
	var type = $("#sel1 .selected").attr("data-value");
	var orderStatus = "";
	$("#sel2 .selected").each(function() {
		if ($(this).attr("data-value") == 0) {
			orderStatus = "0";
		} else if (orderStatus != '0') {
			orderStatus = orderStatus + "" + $(this).attr("data-value") + ',';
		}
	});

	if (orderStatus != '') {
		orderStatus = orderStatus.substring(0, orderStatus.length - 1);
	}
	//出行性质
	var travelType ="";
    $("#sel3 span.sp-traveltype").each(function(){
        if($(this).hasClass("selected")){
            travelType=$(this).attr("data-value");
        }
    })
	//关键字
	var keyword = $("#keyWords").val().trim();

	//开始日期
	var startDate = $("#begin").val();
	//结束日期
	var endDate = $("#end").val();
	var queryBpId = $("#queryBpId").val();
	var isCompPay = $('#compPay').is(':checked');

	var tmcId = '';
	if ($("#queryBpTmcId").val() != undefined) {
		if (!$("#queryBpTmcId").val().isEmpty()) {
			tmcId = $("#queryBpTmcId").val();
		}
	}

	window.location.href = 'order/exportRefund?startDate=' + startDate
			+ '&endDate=' + endDate + '&keyword=' + keyword + '&orderStatus='
			+ orderStatus + '&travelType='+ travelType + '&type=' + type + '&queryBpId=' + queryBpId
			+ '&isCompPay=' + isCompPay + '&tmcId=' + tmcId;

}
//更新时间
function initDate(range) {
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var begin;
	if (range == '0') {//一月
		if ((month - 1) == 0) {//跨年时要判断
			year--;
			month = 12;
		} else {
			month--;
		}

		begin = getBeginDate(year,month,day);
	} else {//一年
		begin = getBeginDate(year,month-1,day);
	}

	$("#begin").val(begin);
	$("#end").val(new Date().format("yyyy-MM-dd"));
}

function getBeginDate(year, month, day){
	if(day > new Date(year,month,0).getDate()){
		var begin = year+"-"+(month<10?'0'+month:month)+"-"+ new Date(year,month,0).getDate();
	}else {
		var begin = year+"-"+(month<10?'0'+month:month)+"-"+ (day<10?'0'+day:day);
	}
	return begin;
}
