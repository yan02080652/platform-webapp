/**
 * Created by wumian on 16/9/20.
 */
$(function () {
	
	if($('#cancelTime').val() != ''){
		var cancelTime = new Date($('#cancelTime').val());
		var now = new Date();
		if($('#cancelTime').val() == '1970-01-01 00:00:00'){
			$('.cancel').text('取消订单');
			$('.cancel').attr('disabled', false);
		}else if(cancelTime < now){
			$('.cancel').text('不可取消');
			$('.cancel').attr('disabled',true);
		}
	}
	
    /*tip层*/
    $('.total_order2').on('mouseover', function () {
        var that = this;
        var index = layer.tips($('.hideHover').html(), that, {
                tips: [3, '#eee'],
                time: 0,
                //area: '386px',
                skin:'demo-class'
            }
        ); //在元素的事件回调体中，follow直接赋予this即可
        $(that).on('mouseleave', function () {
            layer.close(index);
        });
    });
    
    //去支付
    $('.pay').on('click',function(){
    	location.href = "/pay-channel.html" + "?orderIds=" + $(this).attr('orderId');
    });
    
    $('.cancel').on('click',function(){
    	location.href = "/hotel/order/cancel/" + $(this).attr('orderId') + '.html';
    });
    
     //去担保
    $('.guarantee').click(function(){
    	location.href = "/hotel/cardGuaranteePay/" +  + $(this).attr('orderId') + ".html";
    });
    
    //酒店订单
    $('.sub').click(function(){
    	location.href = "/order/hotelOrder.html";
    });
    
    //订单详情
    $('.three').click(function(){
    	location.reload(true);
    });

    // 支付倒计时
    var status = $("#orderShowStatus").val();
    if(status == 'WAIT_PAYMENT' || status == 'WAIT_GUARANTEE' ){
        timer();
    }

});


function timer() {
    //订单创建时间
    var startTime = $("#startTime").val();
    //订单创建后25分钟
    var endTime = parseInt(startTime) + 25*60*1000;
    //当前时间
    var nowTime = new Date().getTime();
    //时间间隔（秒）
    var intDiff = (endTime - nowTime)/1000;

    if(intDiff > 0){
        var timer = $("#timer");

        showTxt();

        var intervalID = setInterval(showTxt, 1000);

        setTimeout(function(){
            timer.text("");
            clearInterval(intervalID);
            layer.alert("订单超过规定支付时限，系统自动取消！",{closeBtn: 0},function(){
                history.go(0);
            })
        }, intDiff*1000);
    }

    function showTxt(){
        var  minute = 0, second = 0;
        minute = Math.floor(intDiff / 60);
        second = Math.floor(intDiff)-(minute*60);
        if (minute <= 9) minute = '0' + minute;
        if (second <= 9) second = '0' + second;
        timer.text(minute + '分'+second + '秒');
        intDiff--;
    }

}

