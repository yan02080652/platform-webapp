
//退改签及特殊票务说明
$('.backmeal_show').click(function () {
    if ($('.backmeal_table').is(':hidden')) {
        $('.backmeal_table').show();
        $('.backmeal_show').css('background', "url(../resource/common/images/upBlueLarge.png) no-repeat 360px");
    } else {
        $('.backmeal_table').hide();
        $('.backmeal_show').css('background', "url(../resource/common/images/downBlueLarge.png) no-repeat 360px");
    }
});

//明细
$('.detailed_show').click(function () {
    if ($('.detailed_ul').is(':hidden')) {
        $('.detailed_ul').show();
        $('.detailed_show').css('background', "url('../resource/common/images/upBlueLarge.png') no-repeat 360px");
        //$(this).attr('class', 'detailed_show backgroundDwon');

    } else {
        $('.detailed_ul').hide();
        $('.detailed_show').css('background', "url('../resource/common/images/downBlueLarge.png') no-repeat 360px");
        //$(this).attr('class', 'detailed_show backgroundUp');
    }
});


//虚线自适应
$(".xuxian").each(function () {
    var a = $(this).prev().width();
    var b = $(this).next().width();
    var c = $(this).parent().width();
    var w = c - a - b - 10 + 'px';
    $(this).width(w);
});



//查看更多   收起
$('.see_more').click(function(){
    if($(this).text() == '查看更多'){
        $('.handleRUl').show();
        $(this).text('收起');
    }else{
        $('.handleRUl').hide();
        $(this).text('查看更多');
    }
});

var status=$("#refundStatus").val();
//每隔5秒刷新一下页面
function refush(){
  var refundId=$("#refundId").text();
  $("#refundDetail").attr("href","order/refundTrainDetail/"+refundId+".html");
  $("#refundDetail").submit();
}
var time=setInterval(refush,5000);
function clear(){
 if(status=="REFUNDED"||status=="REFUSED_REFUND"){
	 if(status=="REFUSED_REFUND"){
		 $("#last").text("拒绝退款");
	 }
	  window.clearInterval(time); 
  }
}
clear();
