$("#oneMonth").click(function() {
	initDate($(this).attr("data-value"));
	reloadGeneralOrder();
	$(this).addClass("selected");
	$("#oneYear").removeClass("selected");
});
$("#oneYear").click(function() {
	initDate($(this).attr("data-value"));
	reloadGeneralOrder();
	$(this).addClass("selected");
	$("#oneMonth").removeClass("selected");
});


$("#sel1 span.sp-traveltype").click(function(){
    $(this).addClass("selected").siblings("span.sp-traveltype").removeClass("selected");
    reloadGeneralOrder();
});


$("#sel2 span").click(function() {
	if ($(this).hasClass("selected")) {
		$(this).removeClass("selected");
	} else {
		$(this).addClass("selected");

		if ($(this).attr("data-value") != 0) {//选择了非全部的 讲全部去掉
			$("#allStatus").removeClass("selected");
		} else {//选择了全部
			$(this).addClass("selected").siblings().removeClass("selected");
		}
	}
});

$('#begin').blur(function() {
	$('#sel1 span.sp-date').removeClass("selected");
});
$('#end').blur(function() {
	$('#sel1 span.sp-date').removeClass("selected");
});

//初始化
$(function() {
	$("#oneMonth").addClass("selected");
	$("#allStatus").addClass("selected");
	$("#oneMonth").addClass("selected");
	$("#all").addClass("selected");
	initDate(0);
	reloadGeneralOrder();
});
//分页调用的方法
function reloadGeneralOrder(pageIndex) {
	//收集参数
	//状态
	var orderStatus = "";
	$("#sel2 .selected").each(function() {
		if ($(this).attr("data-value") == 0) {
			orderStatus = "0";
		} else if (orderStatus != '0') {
			orderStatus = orderStatus + "" + $(this).attr("data-value") + ',';
		}
	});
	if (orderStatus != '') {
		orderStatus = orderStatus.substring(0, orderStatus.length - 1);
	}
	
	//出行性质
	var travelType ="";
    $("#sel1 span.sp-traveltype").each(function(){
        if($(this).hasClass("selected")){
            travelType=$(this).attr("data-value");
        }
    })
    
	//关键字
	var keyword = $("#keyWords").val();
	//开始日期
	var startDate = $("#begin").val();
	//结束日期
	var endDate = $("#end").val();
	var queryBpId = $("#queryBpId").val();

	var issueChannel = $("#issueChannel").val();

	var isCompPay = $('#compPay').is(':checked');

	var tmcId = '';
	if ($("#queryBpTmcId").val()) {
		tmcId = $("#queryBpTmcId").val();
	}
	var loading = layer.load();

	$.ajax({
		url : "order/generalOrderList.json",
		data : {
			startDate : startDate,
			endDate : endDate,
			keyword : keyword,
			orderStatus : orderStatus,
			pageIndex : pageIndex,
			issueChannel : issueChannel,
			queryBpId : queryBpId,
			isCompPay : isCompPay,
			tmcId : tmcId,
			travelType : travelType
		},
		type : "POST",
		success : function(data) {
			layer.close(loading);
			$("#generalOrderTable").html(data);
			$('.drop_ul').hover(function() {
				$('.drop_ul').removeClass('drop_ul1');
				$(this).addClass('drop_ul1');
			});
		},
		error : function() {
			layer.close(loading);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
}

function reloadExportOrder() {

	//收集参数
	//状态
	var orderStatus = "";
	$("#sel2 .selected").each(function() {
		if ($(this).attr("data-value") == 0) {
			orderStatus = "0";
		} else if (orderStatus != '0') {
			orderStatus = orderStatus + "" + $(this).attr("data-value") + ',';
		}
	});

	if (orderStatus != '') {
		orderStatus = orderStatus.substring(0, orderStatus.length - 1);
	}
	//出行性质
	var travelType ="";
    $("#sel1 span.sp-traveltype").each(function(){
        if($(this).hasClass("selected")){
            travelType=$(this).attr("data-value");
        }
    })
	//关键字
	var keyword = $("#keyWords").val();
	//开始日期
	var startDate = $("#begin").val();
	//结束日期
	var endDate = $("#end").val();
	var queryBpId = $("#queryBpId").val();

	var issueChannel = $("#issueChannel").val();

	var isCompPay = $('#compPay').is(':checked');

	var tmcId = '';
	if ($("#queryBpTmcId").val() != undefined) {
		if (!$("#queryBpTmcId").val().isEmpty()) {
			tmcId = $("#queryBpTmcId").val();
		}
	}

	window.location.href = 'order/exportGeneral?startDate=' + startDate
			+ '&endDate=' + endDate + '&keyword=' + keyword + '&orderStatus='
			+ orderStatus + '&travelType='+ travelType +'&issueChannel=' + issueChannel + '&queryBpId='
			+ queryBpId + '&isCompPay=' + isCompPay + '&tmcId=' + tmcId;

}

//更新时间
function initDate(range) {
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();
	var begin;
	if (range == '0') {//一月
		if ((month - 1) == 0) {//跨年时要判断
			year--;
			month = 12;
		} else {
			month--;
		}
		begin = getBeginDate(year,month,day);
	} else {//一年
		begin = getBeginDate(year-1,month,day);
	}

	$("#begin").val(begin);
	$("#end").val(new Date().format("yyyy-MM-dd"));
}

function getBeginDate(year, month, day){
	if(day > new Date(year,month,0).getDate()){
		var begin = year+"-"+(month<10?'0'+month:month)+"-"+ new Date(year,month,0).getDate();
	}else {
		var begin = year+"-"+(month<10?'0'+month:month)+"-"+ (day<10?'0'+day:day);
	}
	return begin;
}