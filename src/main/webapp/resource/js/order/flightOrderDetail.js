//退改签及特殊票务说明
$('.backmeal_show').click(function () {
    if ($('.backmeal_table').is(':hidden')) {
        $('.backmeal_table').show();
        $(this).removeClass('up');
    } else {
        $('.backmeal_table').hide();

        $(this).addClass('up');
    }
});

//明细
$('.detailed_show').click(function () {
    if ($('.detailed_ul').is(':hidden')) {
        $('.detailed_ul').show();
        $(this).removeClass('up');
    } else {
        $('.detailed_ul').hide();
        $(this).addClass('up');
    }
});

//虚线自适应
$(".xuxian").each(function () {
    var a = $(this).prev().width();
    var b = $(this).next().width();
    var c = $(this).parent().width();
    var w = c - a - b - 10 + 'px';
    $(this).width(w);
});


function orderPayment(){
	var insuranceAndOrderIds = $("#insuranceAndOrderIds").val();
	if(insuranceAndOrderIds != "") {
		$("input[name='orderIds']").val(insuranceAndOrderIds);
	}
	$("#payForm").submit();
}

function changeBack(orderId){
	window.location.href = "/flight/order/changeBack/"+orderId+".html";
}


function orderCancel(orderId){
	 layer.confirm('您确定要取消订单吗?', function(index) {
	  var loadIndex = layer.load();
			$.ajax({
				type:"post",
				url:"flight/order/cancelOrder.json",
				data:{
					orderId:orderId,
				},
				success:function(data){
					layer.close(loadIndex);
					if(data){//重新加载页面
						layer.msg("订单取消成功");
						setTimeout(function(){
							history.go(0);
						},1000);
					}else{
						layer.msg("取消订单失败");
					}
				},
				error:function(){
					layer.close(index);
					layer.msg("系统错误");
				}
			});

		});
}

function showInsuranceOrder(orderId){
	window.location.href = "/order/flight/showInsurances/"+orderId;
}


/*tip层*/
$('.dayHover').on('mouseover', function () {

    var that = this;
    var index = layer.tips($(this).next().html(), that, {
        tips: [3, '#fff'],
        time: 0,
        area: '280px'
    }); //在元素的事件回调体中，follow直接赋予this即可
    $(that).on('mouseleave', function () {
        layer.close(index);
    });
});


$(function() { 
	if($("#orderShowStatus").val() == 'WAIT_PAYMENT'){
		timer();
	}
});

function timer() {
	//订单创建时间
	var startTime = $("#startTime").val();
	//订单创建后30分钟
	var endTime = parseInt(startTime) + 30*60*1000;
	//当前时间
	var nowTime = new Date().getTime();
	//时间间隔（秒）
	var intDiff = (endTime - nowTime)/1000;
	
	if(intDiff > 0){
		var timer = $("#timer");
		
		showTxt();
		
		var intervalID = setInterval(showTxt, 1000);

        var maxTime = 12*60*60;//12小时
		var delayTime = intDiff > maxTime ? maxTime : intDiff;

		setTimeout(function(){
			timer.text("");
			clearInterval(intervalID);
			layer.alert("订单超过规定支付时限，系统自动取消！",{closeBtn: 0},function(){
				history.go(0);
			});
		}, delayTime*1000);
	}
	
	function showTxt(){
			var  minute = 0, second = 0;
			minute = Math.floor(intDiff / 60);
			second = Math.floor(intDiff)-(minute*60);
			if (minute <= 9) minute = '0' + minute;
			if (second <= 9) second = '0' + second;
			timer.text(minute + '分'+second + '秒');
			intDiff--;
	}

}
