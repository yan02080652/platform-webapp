
//退改签及特殊票务说明
$('.backmeal_show').click(function () {
    if ($('.backmeal_table').is(':hidden')) {
        $('.backmeal_table').show();
        $('.backmeal_show').css('background', "url(resource/common/images/upBlueLarge.png) no-repeat 360px");
    } else {
        $('.backmeal_table').hide();
        $('.backmeal_show').css('background', "url(resource/common/images/downBlueLarge.png) no-repeat 360px");
    }
});

//明细
$('.detailed_show').click(function () {
    if ($('#cost-detail').is(':hidden')) {
        $('.cost-detail').show();
        $(this).removeClass('up');
    } else {
        $('#cost-detail').hide();
        $(this).addClass('up');
    }
});
$(".save").click(function(){
	location.href="/pay-channel.html?orderIds="+$("#id").text();
});

//虚线自适应
$(".xuxian").each(function () {
    var a = $(this).prev().width();
    var b = $(this).next().width();
    var c = $(this).parent().width();
    var w = c - a - b - 10 + 'px';
    $(this).width(w);
});
//取消订单
$(".cancel").click(function(){
	if (paymentStatus == 'NOT_PAYMENT') {
		layer.confirm('确认要取消吗', {
			  btn: ['确认','取消'] //按钮
			}, function(){
				 //未支付取消
				 $("#hiddenForm").attr("action","train/order/noPayCancel.html?orderId="+id);
				 $("#hiddenForm").submit();
			}, function(){
			  
			});
	} else {
		 $("#hiddenForm").attr("action","train/order/toRefund.html?orderId="+id);
		 $("#hiddenForm").submit();
	}
	
	
});
var status=$("#orderShowStatus").val();//订单显示状态
var id=$("#id").text();//订单id
var paymentStatus=$("#paymentStatus").val();//订单支付状态
//每隔50秒刷新一下页面
function refush(){
if ($("#paymentStatus").val() == 'PAYMENT_SUCCESS' && status != 'ISSUED') {
	   var id=$("#id").text();
	  location.href="/order/trainDetail/"+id;
}
}
var time=setInterval(refush, 50000);
function clear(){
	//如果支付,隐藏“立即支付”按钮
	if(paymentStatus == 'PAYMENT_SUCCESS'){
		$(".oCount").hide();
		$(".save").hide();
		if(status != "ISSUED"){
			$(".cancel").hide();
		}
	}
	
	if(status == "ISSUED"){
		  window.clearInterval(time);  
	}
	
	if (paymentStatus == "NOT_PAYMENT" && status == "CANCELED") {
		$(".cancel").hide();
		$(".save").hide();
		$(".oCount").hide();
		$(".oProgressItemActive").removeClass("oProgressItemActive");
		window.clearInterval(time);
	} 
	
	if (paymentStatus == "WAIT_AUTHORIZATION") {
		$(".cancel").hide();
		$(".save").hide();
		window.clearInterval(time);
	}
	
	
}

$(function() {
	clear();
	if(status == 'WAIT_PAYMENT'){
		timer();
	}
});
function timer() {
	//订单创建时间
	var startTime = $("#startTime").val();
	//订单创建后25分钟
	var endTime = parseInt(startTime) + 25*60*1000;
	//当前时间
	var nowTime = new Date().getTime();
	//时间间隔（秒）
	var intDiff = (endTime - nowTime)/1000;
	
	if(intDiff > 0){
		var timer = $("#timer");
		
		showTxt();
		
		var intervalID = setInterval(showTxt, 1000);
		
		setTimeout(function(){
			timer.text("");
			clearInterval(intervalID);
			layer.alert("订单超过规定支付时限，系统自动取消！",{closeBtn: 0},function(){
				history.go(0);
			})
		}, intDiff*1000);
	}
	
	function showTxt(){
			var  minute = 0, second = 0;
			minute = Math.floor(intDiff / 60);
			second = Math.floor(intDiff)-(minute*60);
			if (minute <= 9) minute = '0' + minute;
			if (second <= 9) second = '0' + second;
			timer.text(minute + '分'+second + '秒');
			intDiff--;
	}

}