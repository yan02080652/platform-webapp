var pageVal = {
    titles: ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十'],
    userIndex: 0,
    getUserIndex:function() {
        return ++this.userIndex;
    }
};
var layUserIndex = null;

$(document).ready(function () {

    //绑定事件
    bindEvent();

    //以下默认增加逻辑，可以删除
    //addProcess();
    //addUser();

    $(".item_div").val($(".item_div").attr("value"));
    $(".item_div").attr("code",$("#costUnitCode").val());
    $(".item_div").attr("orgId",$("#orgId").val());
    $(".item_div").attr("corpId",$("#corpId").val());

    pageVal.userIndex = $('tbody tr').not('.defaultDiv').length - 1;

    var flightRules = {};

    for(var i = 0;i<= pageVal.titles.length;i++){

        flightRules['flightNo_'+i] = {required:true,interFlightNo:true};
        flightRules['realFlightNo_'+i] = {required : function (ele) {

            if($(ele).parents(".fnDiv").find(".share").is(":checked")) {
                return true;
            } else {
                return false;
            }
        },interFlightNo:true};
        flightRules['cabinCode_'+i] = {required : true,interCabinCode:true};
        flightRules['fromDate_'+i] = {required : true};
        flightRules['toDate_'+i] = {required : true,internationalToDate:true};
        flightRules['fromAirportCode_'+i] = {required : true,interAirportCode:true};
        flightRules['fromTerminal_'+i] = {interTerminal:true};
        flightRules['toAirportCode_'+i] = {required : true,interAirportCode:true};
        flightRules['toTerminal_'+i] = {interTerminal:true};
        flightRules['fromTime_'+i] = {required : true,interTime:true};
        flightRules['toTime_'+i] = {required : true,interTime:true};
        flightRules['aircraftType_'+i] = {interAircraftType:true};
        flightRules['baggageRule_'+i] = {required : true,maxlength:200};
        flightRules['refundRule_'+i] = {required : true,maxlength:200};
        flightRules['specialRule_'+i] = {maxlength:200};
        flightRules['stopCityName_'+i] = {required:function (ele) {
            if($(ele).parents(".stopFlight").find(".stopOver").is(":checked")) {
                return true;
            } else {
                return false;
            }
        },maxlength : 15,checkCHN:true};
        flightRules['stopTime_'+i] = {required : function (ele) {
            if($(ele).parents(".stopFlight").find(".stopOver").is(":checked")) {
                return true;
            } else {
                return false;
            }
        },stopTime:true};
        flightRules['baggageType_'+i] = {required : true};
    }

    for(var i = 0;i< 10;i++){
        flightRules['name_'+i] = {required:true,interFullName:true};
        flightRules['surName_'+i] = {required:true,maxlength:30,interName:true};
        flightRules['givenName_'+i] = {required:true,maxlength:30,interName:true};
        flightRules['nationality_'+i] = {required:true,maxlength:20,checkCHN:true};
        flightRules['gender_'+i] = {required:true};
        flightRules['birthday_'+i] = {required:true};
        flightRules['credentialType_'+i] = {required:true};
        flightRules['credentialNo_'+i] = {required:true};
        flightRules['dateOfExpiry_'+i] = {required:true};
        flightRules['mobile_'+i] = {mobile:true};
        flightRules['ticketNo_'+i] = {required:true,ticketNo:true};
    }

    flightRules['contactorName'] = {required : true,minlength : 1,maxlength:30,contactorName:true},
        flightRules['contactorEmail'] = {email : true},
        flightRules['contactorMobile'] = {required : true,mobile:true},
        flightRules['singleTicketPrice'] = {required:true},
        flightRules['singleTaxFee'] = {required:true},
        flightRules['singleExtraFee'] = {required:true},
        flightRules['singleSettelPrice'] = {required:true},
        flightRules['singleSettelTaxFee'] = {required:true},
        flightRules['pnrCode'] = {required:true,pnrCode:true},
        flightRules['supplierName'] = {required:true,maxlength:30},
        flightRules['issueChannel'] = {required:true},
        flightRules['supplierNo'] = {required:true},

        flightRules['userMobile'] = {mobile:true},
        flightRules['userEmail'] = {email : true},

        flightRules['refundRule'] = {required : true},
        flightRules['changeRule'] = {required : true},

        //联系人校验规则
        $("#interFlight").validate({
            rules : flightRules,
            messages : {
                contactorEmail : {
                    email : "请输入正确的邮箱格式"
                },
                maxlength : {
                    maxlength : "请输入正确的供应商名称"
                }

            },
            submitHandler : function(form) {

                if($("input[name='userEmail']").val() == '' && $("input[name='userMobile']").val() == '') {
                    layer.alert("预订人手机号和邮箱至少要填一项",{title:'',closeBtn: 0});
                    return;
                }

                //收集数据
                var details = [];
                var passengers = [];

                var processes = $('.ftTravel .process').not('.defaultDiv');
                $.each(processes, function (i, process) {
                    var detail = {
                        id : $(this).find("input[name='id']").val(),
                        digital : $(this).find("input[name='digital']").val(),
                        flightNo : $(this).find("input[name='flightNo_"+(i + 1)+"']").val(),
                        share : $(this).find("input[name='share_"+(i + 1)+"']").is(":checked"),
                        realFlightNo : $(this).find("input[name='realFlightNo_"+(i + 1)+"']").val(),
                        cabinCode : $(this).find("input[name='cabinCode_"+(i + 1)+"']").val(),
                        fromDate : $(this).find("input[name='fromDate_"+(i + 1)+"']").val(),
                        toDate : $(this).find("input[name='toDate_"+(i + 1)+"']").val(),
                        fromAirportCode : $(this).find("input[name='fromAirportCode_"+(i + 1)+"']").val(),
                        fromTerminal : $(this).find("input[name='fromTerminal_"+(i + 1)+"']").val(),
                        toAirportCode : $(this).find("input[name='toAirportCode_"+(i + 1)+"']").val(),
                        toTerminal : $(this).find("input[name='toTerminal_"+(i + 1)+"']").val(),
                        fromTime : $(this).find("input[name='fromTime_"+(i + 1)+"']").val(),
                        toTime : $(this).find("input[name='toTime_"+(i + 1)+"']").val(),
                        aircraftType : $(this).find("input[name='aircraftType_"+(i + 1)+"']").val(),
                        stopOver : $(this).find("input[name='stopOver_"+(i + 1)+"']").is(":checked"),
                        stopCityName : $(this).find("input[name='stopCityName_"+(i + 1)+"']").val(),
                        stopTime : $(this).find("input[name='stopTime_"+(i + 1)+"']").val(),
                        // baggageRule : $(this).find("input[name='baggageRule_"+(i + 1)+"']").val(),
                        // refundRule : $(this).find("input[name='refundRule_"+(i + 1)+"']").val(),
                        // specialRule : $(this).find("input[name='specialRule_"+(i + 1)+"']").val(),
                        baggageType : $(this).find("input[name='baggageType_"+(i + 1)+"']:checked").val(),
                        pieceWeight : $(this).find("select[name='pieceWeight_"+(i + 1)+"'] option:selected").val(),
                        piece : $(this).find("select[name='piece_"+(i + 1)+"'] option:selected").val(),
                        weight : $(this).find("select[name='weight_"+(i + 1)+"'] option:selected").val(),
                        refundRule : $("input[name='refundRule']").val(),
                        changeRule : $("input[name='changeRule']").val(),
                        specialChangeRule : $("input[name='specialChangeRule']").val(),
                        specialRefundRule :$("input[name='specialRefundRule']").val(),
                        turningPoint : $(this).find("select[name='turningPoint_"+(i + 1)+"'] option:selected").val() == 1 ? true : false,
                        baggageDirect : $(this).find("select[name='baggageDirect_"+(i + 1)+"'] option:selected").val() == 1 ? true : false,
                        transitVisa : $(this).find("select[name='transitVisa_"+(i + 1)+"'] option:selected").val() == 1 ? true : false,
                    }
                    details.push(detail);

                });

                var lastTodate = 0;
                var errorDate = false;
                var detailIndex = [];
                for(var i = 0;i<details.length;i++) {
                    if(i == 0) {
                        lastTodate = details[i].toDate;
                        continue;
                    }
                    if(parseInt(details[i].fromDate) < lastTodate) {
                        detailIndex.push(i + 1);
                        errorDate = true;
                    }
                    lastTodate = details[i].toDate;
                }
                if(errorDate) {
                    layer.alert("行程中第"+detailIndex.join(",")+"程的出发日期小于上一程的到达日期，请确认正确后在录入",{title:'',closeBtn: 0,btn: ['知道了']});
                    return;
                }

                var users = $('#users tr').not('.defaultDiv');
                $.each(users, function (i, user) {
                    if(i != 0) {
                        var p = {
                            id : $(this).find("input[name='id']").val(),
                            userId : $(this).find("input[name='userId_"+(i)+"']").val(),
                            name : $(this).find("input[name='name_"+(i)+"']").val(),
                            surName : $(this).find("input[name='surName_"+(i)+"']").val(),
                            givenName : $(this).find("input[name='givenName_"+(i)+"']").val(),
                            gender : $(this).find("input[name='gender_"+(i)+"']:checked").val(),
                            passengerType : $(this).find("input[name='passengerType_"+(i)+"']").val(),
                            nationality : $(this).find("input[name='nationality_"+(i)+"']").val(),
                            birthday : $(this).find("input[name='birthday_"+(i )+"']").val(),
                            credentialType : $(this).find("select[name='credentialType_"+(i )+"'] option:selected").val(),
                            credentialNo : $(this).find("input[name='credentialNo_"+(i )+"']").val(),
                            dateOfExpiry : $(this).find("input[name='dateOfExpiry_"+(i )+"']").val(),
                            travellerType : $(this).find("input[name='travellerType_"+(i )+"']").val(),
                            mobile : $(this).find("input[name='mobile_"+(i)+"']").val(),
                            ticketNo : $(this).find("input[name='ticketNo_"+(i )+"']").val(),
                            empLevelCode : $(this).find("select[name='empLevelCode_"+(i )+"'] option:selected").val(),
                            travelType : $("select[name='travelType'] option:selected").val(),
                            costUnitCode : $(".item_div").attr("code"),
                            orgId : $(".item_div").attr("orgid"),
                            corporationId : $(".item_div").attr("corpid"),
                        }
                        console.log(p);
                        passengers.push(p);
                    }

                });

                var orderForm = {
                    id : $("#orderId").val(),
                    cName : $("input[name='cName']").val(),
                    issueChannel : $("select[name='issueChannel']").val(),
                    supplierName:$("select[name='issueChannel'] option:selected").text(),
                    supplierNo : $("input[name='supplierNo']").val(),
                    pnrCode : $("input[name='pnrCode']").val(),
                    singleTicketPrice : $("input[name='singleTicketPrice']").val(),
                    singleTaxFee : $("input[name='singleTaxFee']").val(),
                    singleSettelPrice : $("input[name='singleSettelPrice']").val(),
                    singleSettelTaxFee : $("input[name='singleSettelTaxFee']").val(),
                    singleExtraFee : $("input[name='singleExtraFee']").val(),
                    contactorName : $("input[name='contactorName']").val(),
                    contactorMobile : $("input[name='contactorMobile']").val(),
                    contactorEmail : $("input[name='contactorEmail']").val(),
                    tripType : $("input[name='tripType']:checked").val(),
                    detailDtos : details,
                    passengerDtos : passengers,
                };


                $.ajax({
                    url: "order/updateInternationalOrder.json",
                    type: "post",
                    data: JSON.stringify(orderForm),
                    contentType: "application/json",
                    success: function (data) {
                        if (data) {
                            layer.msg("修改成功");
                        } else {
                            layer.msg("修改失败，请稍后重试");
                        }
                    }
                })
            },
            errorClass:"promptError"
        });

    $(".formBtnGray").click(function(){
        $("#interFlight").submit();
    });

    //金额数
    $("input[name='singleTicketPrice']").blur(function(){
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });
    $("input[name='singleTaxFee']").blur(function(){
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });
    $("input[name='singleExtraFee']").blur(function(){
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });
    $("input[name='singleSettelPrice']").blur(function(){
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });
    $("input[name='singleSettelTaxFee']").blur(function(){
        $(this).val(toDecimal2($(this).val()));
        calculationPrice();
    });

    $(".fromDate").jeDate({
        format:"YYYYMMDD",
    })

    $(".toDate").jeDate({
        format:"YYYYMMDD",
    })

    calculationPrice();

    $('.wd').change(function () {
        var v = $(this).prop('checked');
        var input = $('.wd_input', $(this).parent().parent());
        if (v) {
            input.removeAttr('disabled');
        } else {
            input.attr('disabled', 'disabled');
            input.val("");
        }
    });

    $('.wd').each(function () {
        var v = $(this).prop('checked');
        var input = $('.wd_input', $(this).parent().parent());
        if (v) {
            input.removeAttr('disabled');
        } else {
            input.attr('disabled', 'disabled');
        }

    });

    //清空
    $('.clearX').click(function () {
        $('input', $(this).parent().parent()).val(null);
        $("label.promptError",$(this).parent().parent()).remove();
    });

    //删除
    $('.icon-close').click(function () {
        if($('.ftTravel .process').not('.defaultDiv').length == 1) {
            $('input', $(this).parent().parent()).val(null);
            $("label.promptError",$(this).parent().parent()).remove();
            return;
        }
        $(this).parent().parent().parent().remove();
        $("#detailsCheck .checkSpan").not(".defaultDiv").eq(0).remove();
        reOrderProcess();
    });

    $('.delUser').click(function () {
        if($(".userInfo tr").not('.defaultDiv').length == 2) {
            $(this).parent().parent().remove();
            addUser();
            $("input[name='travellerType_1']").val("");
            return;
        }
        $(this).parent().parent().remove();
        pageVal.userIndex = $('tbody tr').not('.defaultDiv').length - 1;
        reOrderUser();

    });
    //时间
    $(".birthday").jeDate({
        format:"YYYY-MM-DD",
    })
    $(".dateOfExpiry").jeDate({
        format:"YYYY-MM-DD",
    })

    $(".baggageType").click(function () {
        var value = $(this).val();
        var pDiv = $(this).parents(".newBaggage");
        if(value == 1) {
            pDiv.find(".piece").attr("disabled","disabled");
            pDiv.find(".pieceWeight").attr("disabled","disabled");
            pDiv.find(".weight").attr("disabled","disabled");
        } else if(value == 2) {
            pDiv.find(".piece").removeAttr("disabled");
            pDiv.find(".pieceWeight").removeAttr("disabled");
            pDiv.find(".weight").attr("disabled","disabled");
        } else if(value == 3){
            pDiv.find(".piece").attr("disabled","disabled");
            pDiv.find(".pieceWeight").attr("disabled","disabled");
            pDiv.find(".weight").removeAttr("disabled");

        }
    });

    $(".turningPoint").change(function () {
        var turningPoint = $(this).val();
        var pDiv = $(this).parents(".stopFlight");
        if(turningPoint == 0) {
            pDiv.find(".baggageDirect").attr("disabled","disabled");
            pDiv.find(".transitVisa").attr("disabled","disabled");
        } else {
            pDiv.find(".baggageDirect").removeAttr("disabled");
            pDiv.find(".transitVisa").removeAttr("disabled");
        }
    });

});

function bindEvent() {
    //添加行程按钮事件
    $('.ftTravel .addTrvbtn').click(function () {
        addProcess();
    });

    //添加非员工乘机人
    $('.ftUser .addNoStaff_flight').click(function () {
        addUser({},true);
    });


    /*添加员工乘机人*/
    $('.addStaff_flight').click(function () {

        var topUserHtml = '';
        var orgHtml = '';
        var userHtml = '';

        $("#orgNames").find("a").eq(0).nextAll().remove();

        $.ajax({
            url : "order/inter/findOrgAndUserByOrgId.json",
            type : "get",
            dataType:"json",
            data : {"orgId":null,"cId":$("input[name='cId']").val()},
            async:false,
            success:function (data) {

                $(data.topUser).each(function(){
                    if(this.status != -99) {
                        topUserHtml += '<span>' + this.fullname + '</span>'
                        topUserHtml += '<input type="hidden" value="' + this.id + '" class="userInfo">'
                    }
                });

                $(data.orgs).each(function(){
                    if(this.status == 0) {
                        orgHtml += '<div><span class="overflow_text">' + this.name + '(' + data.countMap[this.id] + ')</span>'
                        orgHtml += '<input type="hidden" value="' + this.id + '" class="orgId"></div>'
                    }
                });

                $(data.users).each(function(){
                    if(this.status != -99) {
                        userHtml += '<span>' + this.fullname + '</span>'
                        userHtml += '<input type="hidden" value="' + this.id + '" class="userInfo">'
                    }
                });

            }

        });
        if(topUserHtml == '') {
            $(".topUser").css("display","none");
        } else {
            $(".topUser").html(topUserHtml);
        }
        if(orgHtml != '') {
            $(".depart").html(orgHtml);
            bindSearchClick();
        }

        $(".staff").html(userHtml);

        layUserIndex = layer.open({
            title: '',
            type: 1,
            skin: 'demo-class',
            offset: '130px',
            area: ['544px', ""],
            content: $('.addStaff')
        })
        bindAddClick();

    });



    $("#condition").keyup(function(){
        var html = '';
        var orgHtml = '';
        $.post("order/inter/findUserByCondition.json",{key:$(this).val(),cId:$("input[name='cId']").val()},function(data){

            $(data.users).each(function(){
                if(this.status != -99) {
                    html += '<span>' + this.fullname + '</span>'
                    html += '<input type="hidden" value="' + this.id + '" class="userInfo">'
                }
            })

            $(data.orgs).each(function(){
                if(this.status == 0) {
                    orgHtml += '<div><span class="overflow_text">' + this.name + '(' + data.countMap[this.id] + ')</span>'
                    orgHtml += '<input type="hidden" value="' + this.id + '" class="orgId"></div>'
                }
            });

            if(orgHtml != '') {
                $(".depart").html(orgHtml);

                bindSearchClick();
            }

            if(html != '') {
                $(".staff").html(html);

                $('.addStaff .staff span').click(function () {
                    //员工1
                    var user;
                    $.ajax({
                        url : "interFlight/manualOrder/findByUserId.json",
                        type : "get",
                        dataType:"json",
                        data : {"id":$(this).next().val(),"cId":$("input[name='cId']").val()},
                        async:false,
                        success:function (data) {
                            user = data;
                        }
                    });

                    var add = {
                        isEmp: true,
                        userId:user.id,
                        name: user.fullname,
                        familyName: user.lastName,
                        givenName: user.firstName,
                        nationality: user.country,
                        sex: user.gender == '1' ? 'F' : 'M',
                        birthday:formatTime(user.birthDay),
                        credentialNo: user.userCert[0] ? user.userCert[0].idNumber : '',
                        credentialType: user.userCert[0] ? user.userCert[0].idType : '',
                        dateOfExpiry: user.userCert[0] ? formatTime(user.userCert[0].validDate) : '',
                        mobile:user.mobile,
                        empLevelCode:user.rank,
                        travellerType:3,
                        passengerType : "ADU",
                        cards : user.userCert,
                    };

                    addUser(add);

                    layer.close(layUserIndex);
                });

            }

        })

    });

    bindSearchClick();


}

//行程事件绑定
function bindProcessEvent(process) {
    //共享航班、经停航班控制
    $('.wd', process).change(function () {
        var v = $(this).prop('checked');
        var input = $('.wd_input', $(this).parent().parent());
        if (v) {
            input.removeAttr('disabled');
        } else {
            input.attr('disabled', 'disabled');
            input.val("");
            input.siblings(".promptError").remove();
        }
    });

    //清空
    $('.clearX', process).click(function () {
        $('input', $(this).parent().parent()).val(null);
    });

    //删除
    $('.icon-close', process).click(function () {
        if($('.ftTravel .process').not('.defaultDiv').length == 1) {
            $('input', $(this).parent().parent()).val(null);
            return;
        }
        $(this).parent().parent().parent().remove();
        $("#detailsCheck .checkSpan").not(".defaultDiv").eq(0).remove();
        reOrderProcess();
    });

    $(".fromDate").jeDate({
        format:"YYYYMMDD",
    })

    $(".toDate").jeDate({
        format:"YYYYMMDD",
    })


}

//绑定乘机人事件
function bindUserEvent(userTr) {
    $('.delUser', userTr).click(function () {
        if($(".userInfo tr").not('.defaultDiv').length == 2) {
            $(this).parent().parent().remove();
            addUser();
            $("input[name='travellerType_1']").val("");
            return;
        }
        $(this).parent().parent().remove();
        pageVal.userIndex = $('tbody tr').not('.defaultDiv').length - 1;

        reOrderUser();
    });
    //时间
    $(".birthday").jeDate({
        format:"YYYY-MM-DD",
    })
    $(".dateOfExpiry").jeDate({
        format:"YYYY-MM-DD",
    })

    $(".credentialType").change(function(){
        var newType = $(this).find("option:selected").val();
        var idNo = "";
        var valiteDate = "";
        $(this).parent().parent().find(".cardData input").each(function(i){
            if(newType==$(this).val()){
                idNo = $(this).attr("data-value");
                valiteDate = $(this).attr("validDate-value");
            }
        });
        $(this).parent().parent().find(".credentialNo").val(idNo);
        $(this).parent().parent().find(".dateOfExpiry").val(valiteDate == "null" ? "" : valiteDate);
    });

}

//添加行程
function addProcess() {
    if ($('.ftTravel .process').not('.defaultDiv').length >= pageVal.titles.length) {
        layer.open({
            content: '最多添加' + pageVal.titles.length + '个行程'
        });
        return;
    }

    var newProcess = $('.ftTravel .defaultDiv').clone();
    newProcess.removeClass('defaultDiv');
    $('.ftTravel').append(newProcess);

    //退改行程增加一个
    var checkSpan = $("#detailsCheck").find(".defaultDiv").clone();
    checkSpan.removeClass("defaultDiv");
    $("#detailsCheck").append(checkSpan);

    bindProcessEvent(newProcess);
    reOrderProcess();

}

//重新排序行程
function reOrderProcess() {
    var processes = $('.ftTravel .process').not('.defaultDiv');
    var titles = pageVal.titles;
    $.each(processes, function (i, process) {
        var originIndex = $(this).find("input[name='digital']").val();
        $('.process_title', process).text('第' + titles[i] + '程');
        $(this).find("input[name='digital']").val(i + 1);
        $(this).find(".flightNo").attr("name","flightNo_"+(i + 1));
        $(this).find(".realFlightNo").attr("name","realFlightNo_"+(i + 1));
        $(this).find(".cabinCode").attr("name","cabinCode_"+(i + 1));
        $(this).find(".fromDate").attr("name","fromDate_"+(i + 1));
        $(this).find(".toDate").attr("name","toDate_"+(i + 1));
        $(this).find(".fromAirportCode").attr("name","fromAirportCode_"+(i + 1));
        $(this).find(".fromTerminal").attr("name","fromTerminal_"+(i + 1));
        $(this).find(".toAirportCode").attr("name","toAirportCode_"+(i + 1));
        $(this).find(".toTerminal").attr("name","toTerminal_"+(i + 1));
        $(this).find(".fromTime").attr("name","fromTime_"+(i + 1));
        $(this).find(".toTime").attr("name","toTime_"+(i + 1));
        $(this).find(".aircraftType").attr("name","aircraftType_"+(i + 1));
        $(this).find(".stopOver").attr("name","stopOver_"+(i + 1));
        $(this).find(".stopCityName").attr("name","stopCityName_"+(i + 1));
        $(this).find(".stopTime").attr("name","stopTime_"+(i + 1));
        $(this).find(".baggageRule").attr("name","baggageRule_"+(i + 1));
        $(this).find(".refundRule").attr("name","refundRule_"+(i + 1));
        $(this).find(".specialRule").attr("name","specialRule_"+(i + 1));
        $(this).find(".share").attr("name","share_"+(i + 1));

        $(this).find(".baggageType").attr("name","baggageType_"+(i + 1));
        $(this).find(".piece").attr("name","piece_"+(i + 1));
        $(this).find(".pieceWeight").attr("name","pieceWeight_"+(i + 1));
        $(this).find(".weight").attr("name","weight_"+(i + 1));

        $(this).find(".turningPoint").attr("name","turningPoint_"+(i + 1));
        $(this).find(".baggageDirect").attr("name","baggageDirect_"+(i + 1));
        $(this).find(".transitVisa").attr("name","transitVisa_"+(i + 1));
    });

    var detailChecks = $("#detailsCheck .checkSpan").not(".defaultDiv");
    $.each(detailChecks, function (i, process) {
        $(this).find(".detailNo").text('第' + titles[i] + '程');
    });

}

//添加乘机人
function addUser(data,boo) {
    if ($('#users tr').not('.defaultDiv').length >= 10) {
        layer.open({
            offset: '130px',
            content: '最多添加9个乘机人'
        });
        return;
    }
    var repeat = false;
    $(".userId").each(function(){
        if($(this).val() == (data ?  data.userId : "undefined")) {
            repeat = true;
        }
    });
    if(repeat) {
        layer.open({
            offset: '130px',
            content: '该员工已添加'
        });
        return;
    }

    var index = pageVal.getUserIndex();
    data = data || {};
    var userTr = $('.userTab .defaultDiv').clone();
    userTr.data('index', index);
    $('.sexIpt input', userTr).attr('name', 'gender_' + index );
    userTr.removeClass('defaultDiv');
    $('.userTab ').append(userTr);
    if($(".userInfo tr").not('.defaultDiv').length == 3 && $(".userInfo tr").not('.defaultDiv').eq(1).find(".travellerType").val() == '') {
        $(".userInfo tr").not('.defaultDiv').eq(1).remove();
    }

    if (data.isEmp) {//员工
        $('.name input', userTr).val(data.name || '');
        $('.name input', userTr).prop("readonly",data.name ? true : false);
        $('.familyName input', userTr).prop("readonly",data.familyName ? true : false);
        $('.givenName input', userTr).prop("readonly",data.givenName ? true : false);
        $('.nationality input', userTr).prop("readonly",data.nationality ? true : false);
        $('.birthday', userTr).prop("disabled",data.birthday ? true : false);
        $('.mobile', userTr).prop("readonly",data.mobile ? true : false);
        $('.credentialNo', userTr).prop("readonly",data.credentialNo ? true : false);
        $('.empLevelCode', userTr).prop("disabled",data.empLevelCode ? true : false);
        $('.sexIpt input', userTr).prop("disabled",data.sex ? true : false);
    } else {//非员工
        $('.name input', userTr).val(data.name || null);
    }

    if(boo) {
        $('.empLevelCode', userTr).prop("disabled",true);
        $('.empLevelCode', userTr).html('<option value="">非员工</option>');
    }

    $('.userId', userTr).val(data.userId || null);
    $('.familyName input', userTr).val(data.familyName || null);
    $('.givenName input', userTr).val(data.givenName || null);
    $('.nationality input', userTr).val(data.nationality || null);
    $('.birthday', userTr).val(data.birthday || null);

    $('.credentialType option[value='+(data.credentialType || 'PASSPORT')+']', userTr).attr("selected",true);

    $('.credentialNo', userTr).val(data.credentialNo || null);
    $('.dateOfExpiry', userTr).val(data.dateOfExpiry || null);
    $('.mobile', userTr).val(data.mobile || null);

    $('.empLevelCode option[value='+(data.empLevelCode)+']', userTr).attr("selected",true);
    $(".travellerType",userTr).val(data.travellerType || "1");

    $(".passengerType",userTr).val(data.passengerType || $("select[name='passengerType'] option:selected").val());

    $('.sexIpt input[value=' + (data.sex || 'M') + ']', userTr).attr("checked", true);

    if(data.cards) {
        console.log(data.cards);
        var cardInput = "";
        $.each(data.cards,function(){
            cardInput += '<div class="cardData"><input type="hidden" value="'+this.idType+'" data-value="'+this.idNumber+'" validDate-value="'+formatTime(this.validDate)+'" /></div>';
        });
        console.log(cardInput);
        $(userTr).append(cardInput);
    }

    bindUserEvent(userTr);

    reOrderUser();
}

function reOrderUser() {
    var users = $('.userTab tr').not('.defaultDiv');
    $.each(users, function (i, user) {
        $('.order', user).text(i);
        $(this).find(".name").attr("name","name_"+i);
        $(this).find(".userId").attr("name","userId_"+i);
        $(this).find(".travellerType").attr("name","travellerType_"+i);
        $(this).find(".passengerType").attr("name","passengerType_"+i);
        $(this).find(".surName").attr("name","surName_"+i);
        $(this).find(".givenName").attr("name","givenName_"+i);
        $(this).find(".nationality").attr("name","nationality_"+i);
        $(this).find(".gender").attr("name","gender_"+i );
        $(this).find(".birthday").attr("name","birthday_"+i);
        $(this).find(".credentialType").attr("name","credentialType_"+i );
        $(this).find(".credentialNo").attr("name","credentialNo_"+i);
        $(this).find(".dateOfExpiry").attr("name","dateOfExpiry_"+i );
        $(this).find(".mobile").attr("name","mobile_"+i);
        $(this).find(".ticketNo").attr("name","ticketNo_"+i);
        $(this).find(".empLevelCode").attr("name","empLevelCode_"+i);
    });

    //计算页面价格
    $("#passengerNum").text(users.length - 1);
    calculationPrice();

}




//取两位小数
function toDecimal2(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return false;
    }
    var f = Math.round(x*100)/100;
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
        rs = s.length;
        s += '.';
    }
    while (s.length <= rs + 2) {
        s += '0';
    }
    return s;
}

//计算页面价格 显示
function calculationPrice(){

    if($("input[name='singleTicketPrice']").val() == '') {
        return;
    }
    if($("input[name='singleTaxFee']").val() == '') {
        return;
    }
    if($("input[name='singleExtraFee']").val() == '') {
        return;
    }
    if($("input[name='singleSettelPrice']").val() == '') {
        return;
    }
    if($("input[name='singleSettelTaxFee']").val() == '') {
        return;
    }
    var singleTicketPrice = parseFloat($("input[name='singleTicketPrice']").val());
    var singleTaxFee = parseFloat($("input[name='singleTaxFee']").val());
    var singleExtraFee = parseFloat($("input[name='singleExtraFee']").val());
    var singleSettelPrice = parseFloat($("input[name='singleSettelPrice']").val());
    var singleSettelTaxFee = parseFloat($("input[name='singleSettelTaxFee']").val());
    var passengerNum = parseInt($("#passengerNum").text());


    var totalSalePrice = passengerNum * (singleTicketPrice + singleTaxFee + singleExtraFee);
    var totalSettelPrice = passengerNum * (singleSettelPrice + singleSettelTaxFee);
    var profit = totalSalePrice - totalSettelPrice;

    $("#totalSalePrice").text(toDecimal2(totalSalePrice));
    $("#totalSettelPrice").text(toDecimal2(totalSettelPrice));
    $("#profit").text(toDecimal2(profit));

}
//转大写
function toUpper(obj) {
    $(obj).val($(obj).val().toUpperCase());
}

function bindAddClick(){

    $('.addStaff .staff span').click(function () {
        //员工1
        var user;
        $.ajax({
            url : "order/inter/findByUserId.json",
            type : "get",
            dataType:"json",
            data : {"id":$(this).next().val(),"cId":$("input[name='cId']").val()},
            async:false,
            success:function (data) {
                user = data;
            }
        });

        var add = {
            isEmp: true,
            userId:user.id,
            name: user.fullname,
            familyName: user.lastName,
            givenName: user.firstName,
            nationality: user.country,
            sex: user.gender == '1' ? 'F' : 'M',
            birthday:formatTime(user.birthDay),
            credentialNo: user.userCert[0] ? user.userCert[0].idNumber : '',
            credentialType: user.userCert[0] ? user.userCert[0].idType : '',
            dateOfExpiry: user.userCert[0] ? formatTime(user.userCert[0].validDate) : '',
            mobile:user.mobile,
            empLevelCode:user.rank,
            travellerType:3,
            cards : user.userCert,
        };

        addUser(add);

        layer.close(layUserIndex);
    });

    $('.addStaff .topUser span').click(function () {
        //员工1
        var user;
        $.ajax({
            url : "order/inter/findByUserId.json",
            type : "get",
            dataType:"json",
            data : {"id":$(this).next().val()},
            async:false,
            success:function (data) {
                user = data;
            }
        });

        var add = {
            isEmp: true,
            userId:user.id,
            name: user.fullname,
            familyName: user.lastName,
            givenName: user.firstName,
            nationality: user.country,
            sex: user.gender == '1' ? 'F' : 'M',
            birthday:formatTime(user.birthDay),
            credentialNo: user.userCert[0] ? user.userCert[0].idNumber : '',
            credentialType: user.userCert[0] ? user.userCert[0].idType : '',
            dateOfExpiry: user.userCert[0] ? formatTime(user.userCert[0].validDate) : '',
            mobile:user.mobile,
            empLevelCode:user.rank,
            travellerType:3,
            cards : user.userCert,
        };
        addUser(add);
        layer.close(layUserIndex);
    });
}

function bindSearchClick(){
    $(".depart div span").click(function(){
        var a = $(this).parent().find(".orgId").val();

        var name = $(this).text().substring(0,$(this).text().indexOf("("));

        var topUserHtml = '';
        var orgHtml = '';
        var userHtml = '';
        $.ajax({
            url : "order/inter/findOrgAndUserByOrgId.json",
            type : "get",
            dataType:"json",
            data : {"orgId":a,"cId":$("input[name='cId']").val()},
            async:false,
            success:function (data) {

                $(data.topUser).each(function(){
                    if(this.status != -99) {
                        topUserHtml += '<span>' + this.fullname + '</span>'
                        topUserHtml += '<input type="hidden" value="' + this.id + '" class="userInfo">'
                    }
                });

                $(data.orgs).each(function(){
                    if(this.status == 0) {
                        orgHtml += '<div><span class="ell">' + this.name + '(' + data.countMap[this.id] + ')</span>'
                        orgHtml += '<input type="hidden" value="' + this.id + '" class="orgId"></div>'
                    }
                });

                $(data.users).each(function(){
                    if(this.status != -99) {
                        userHtml += '<span>' + this.fullname + '</span>'
                        userHtml += '<input type="hidden" value="' + this.id + '" class="userInfo">'
                    }
                });
            }
        });
        if(topUserHtml == '') {
            $(".topUser").css("display","none");
        } else {
            $(".topUser").html(topUserHtml);
        }

        if(orgHtml != '') {
            $(".depart").html(orgHtml);

            $("#orgNames").append("<a onclick='searchByOrgId("+a+")' id='"+a+"' > >"+name+"</a>");

            bindSearchClick();
        }
        $(".staff").html(userHtml);

        bindAddClick();

    });
}

function formatTime(time){
    if(time == null) {
        return null;
    }
    var date = new Date(time);
    var m = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    var d = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    return date.getFullYear() + "-" + m + "-" + d;
}

function searchByOrgId(orgId) {

    if(orgId == null) {
        $("#orgNames").find("a").eq(0).nextAll().remove();
        orgId = null;
    }

    $("#"+orgId).nextAll().remove();

    var topUserHtml = '';
    var orgHtml = '';
    var userHtml = '';
    $.ajax({
        url : "order/inter/findOrgAndUserByOrgId.json",
        type : "get",
        dataType:"json",
        data : {"orgId":orgId,"cId":$("input[name='cId']").val()},
        async:false,
        success:function (data) {

            $(data.topUser).each(function(){
                if(this.status != -99) {

                    topUserHtml += '<span>' + this.fullname + '</span>'
                    topUserHtml += '<input type="hidden" value="' + this.id + '" class="userInfo">'
                }
            });

            $(data.orgs).each(function(){
                if(this.status == 0) {
                    orgHtml += '<div><span class="overflow_text">' + this.name + '(' + data.countMap[this.id] + ')</span>'
                    orgHtml += '<input type="hidden" value="' + this.id + '" class="orgId"></div>'
                }
            });

            $(data.users).each(function(){
                if(this.status != -99) {
                    userHtml += '<span>' + this.fullname + '</span>'
                    userHtml += '<input type="hidden" value="' + this.id + '" class="userInfo">'
                }
            });
        }
    });
    if(topUserHtml == '') {
        $(".topUser").css("display","none");
    } else {
        $(".topUser").html(topUserHtml);
    }

    if(orgHtml != '') {
        $(".depart").html(orgHtml);

        bindSearchClick();
    }

    $(".staff").html(userHtml);

    bindAddClick();


}