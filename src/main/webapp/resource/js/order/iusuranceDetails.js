/**
 * Created by tuyican on 17/3/31.
 */
$(function(){
	 "use strict";
	    $(".listcontent").on("mousemove",".itmehadertitle",function(){
	        $(this).parents().siblings(".detailtext").show();
	    })
	    $(".listcontent").on("mouseout",".itmehadertitle",function(){
	        $(this).parents().siblings(".detailtext").hide();
	    })
	    $(".detailtext").mouseout(function(){
	        $(this).hide();
	    })
	    $(".detailtext").mousemove(function(){
	        $(this).show();
	    })
    //总价格
    var totalPrice = 0;
    $(".listcontent").each(function(){
    	var count = $(this).find(".ovh").length - 1;
    	if(count == 0) {
    		$(this).remove();
    	}
    	
    	$(this).find(".insurance_count").text("(" +count + "份)");
    	var  unitPrice = $(this).find("input[type='hidden']").val();
    	totalPrice = totalPrice + (count * parseFloat(unitPrice));
    });
    var m = Math.pow(10, 1);
    $(".big").text(parseInt(totalPrice * m, 10) / m);
    
    //保险详情中如果有一个没支付也传对应的保险订单id过去
//    var insuranceOrderIds = new Array();
//    $(".listcontent").each(function(){
//    	var hasNoPay = false;
//    	$(this).find(".ovh [name='orderStatus']").each(function(){
//    		var orderStatus = $(this).val();
//    		if(orderStatus == 'AUTHORIZE_RETURN' || orderStatus== 'WAIT_PAYMENT') {
//    			$(".success").removeAttr("style");
//    			$(".notpaid").removeAttr("style");
//    			$(".notpaid").css("cursor","pointer");
//    			
//    			$(".notpaid").click(function(){
//    				payForInsurance();
//    			});
//    			hasNoPay = true;
//    		}
//    	});
//    	if(hasNoPay){
//    		var insuranceOrderId = $(this).find("input[name='insuranceOrderDtoId']").val();
//    		insuranceOrderIds.push(insuranceOrderId);
//    	}
//    });
//    $("input[name='orderIds']").val(insuranceOrderIds.join(","));
    var insuranceOrderIds = new Array();
    $(".listcontent").each(function(){
    	var hasNoPay = false;
    	var insuranceOrderDtoStatus = $(this).find("input[name='insuranceOrderDtoStatus']").val();
    	if(insuranceOrderDtoStatus == 'AUTHORIZE_RETURN' || insuranceOrderDtoStatus == 'WAIT_PAYMENT'){
    		$(".success").removeAttr("style");
			$(".notpaid").removeAttr("style");
			$(".notpaid").css("cursor","pointer");
			
			$(".notpaid").click(function(){
				payForInsurance();
			});
			hasNoPay = true;
    	}
    	if(hasNoPay){
    		var insuranceOrderId = $(this).find("input[name='insuranceOrderDtoId']").val();
    		insuranceOrderIds.push(insuranceOrderId);
    	}
    })
    $("input[name='orderIds']").val(insuranceOrderIds.join(","));
    
})

	function payForInsurance(){
		document.forms['sbumitSuccessForm'].submit();
	}

