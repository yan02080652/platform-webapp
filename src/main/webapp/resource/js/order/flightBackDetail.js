/**
 * Created by wumian on 16/9/27.
 */
/**
 * Created by QiYuBu on 2016/9/19.
 * @ Description:[一句话描述功能]
 */

//退改签及特殊票务说明
$('.backmeal_show').click(function () {
    if ($('.backmeal_table').is(':hidden')) {
        $('.backmeal_table').show();
        $(this).removeClass('up');
    } else {
        $('.backmeal_table').hide();

        $(this).addClass('up');
    }
});

//明细
$('.detailed_show').click(function () {
    if ($('.detailed_ul').is(':hidden')) {
        $('.detailed_ul').show();
        $(this).removeClass('up');
    } else {
        $('.detailed_ul').hide();
        $(this).addClass('up');
    }
});


//虚线自适应
$(".xuxian").each(function () {
    var a = $(this).prev().width();
    //alert(a);
    var b = $(this).next().width();
    //alert(b);
    var c = $(this).parent().width();
    //alert(c);
    var w = c - a - b - 10 + 'px';
    $(this).width(w);
    //alert(w);
});


//查看更多   收起
$('.see_more').click(function () {
    if ($(this).text() == '查看更多') {
        $('.handleRUl').show();
        $(this).text('收起');
    } else {
        $('.handleRUl').hide();
        $(this).text('查看更多');
    }
});