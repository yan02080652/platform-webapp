/**
 * Created by heyuanjing on 18/1/4.
 */
(function () {
//    $("#begin").attr('value',addDate(new Date(),-30));
//    $("#end").attr('value',new Date().format("yyyy-MM-dd"));
//
//    function addDate(date,days){
//        var d=new Date(date);
//        d.setDate(d.getDate()+days);
//        var m=d.getMonth()+1;
//        if(m < 10){
//            m = "0" + m;
//        }
//        var da = d.getDate();
//        if(da < 10){
//            da = "0" + da;
//        }
//        return d.getFullYear()+'-'+m+'-'+da;
//    }
	$("#oneMonth").addClass("selected");
	initDate(0);
	
    $('.selAll')[0].setAttribute('class','selected');


    search();

    $("#sel1 span.sp-date").click(function () {
    	//更新时间，然后发送ajax请求
    	initDate($(this).attr("data-value"));
    	reloadOrderList()
    	
        $(this).addClass("selected").siblings("span.sp-date").removeClass("selected");

        var copyThis = $(this).clone();
        if ($("#selA").length > 0) {
            $("#selA").html($(this).text());
        } else {
            $("#selResult").append(copyThis.attr("id", "selA"));

        }
    });

    $("#sel1 span.sp-traveltype").click(function(){
        $(this).addClass("selected").siblings("span.sp-traveltype").removeClass("selected");
        reloadOrderList();
    });

    $("#sel2 span").click(function () {
        if($(this).hasClass("selected")){
            $(this).removeClass("selected");
        }else{
            $(this).addClass("selected");

            if($(this).attr("status") != 0){//选择了非全部的 讲全部去掉
                $("#allStatus").removeClass("selected");
            }else{//选择了全部
                $(this).addClass("selected").siblings().removeClass("selected");
            }
        }
        //$(this).addClass("selected").siblings().removeClass("selected");

        //发送请求
        //reloadFlightOrder();
    });

    $('#search').on('click',function(){
        search();
    });


    $('.dateRange').on('click',function(){
        var date = new Date();
        $('#end').attr('value',date.format('yyyy-MM-dd'));
        $("#end").val(date.format('yyyy-MM-dd'));
        date.setDate(date.getDate() - parseInt($(this).attr('attr')));
        $('#begin').attr('value',date.format('yyyy-MM-dd'));
        $("#begin").val(date.format('yyyy-MM-dd'));
        search();
    });

})();

$('#begin').blur(function(){
    $('#sel1 span.sp-date').removeClass("selected");
});
$('#end').blur(function(){
    $('#sel1 span.sp-date').removeClass("selected");
});

//搜索
function search(){
    reloadOrderList(1);
}

//订单列表
function reloadOrderList(pageIndex){
    //收集参数
    //状态
    var orderStatus = "";
    $("#sel2 .selected").each(function(){
        if($(this).attr("status") == 0){
            orderStatus = "0";
        }else if(orderStatus != '0'){
            orderStatus = orderStatus + "" +$(this).attr("status")+',';
        }
    });

    if(orderStatus != ''){
        orderStatus = orderStatus.substring(0, orderStatus.length-1);
    }
    var travelType ="";
    $("#sel1 span.sp-traveltype").each(function(){
        if($(this).hasClass("selected")){
            travelType=$(this).attr("data-value");
        }
    })

    //关键字
    var keyword = $("#keyWords").val();
    //开始日期
    var startDate = $("#begin").val();
    //结束日期
    var endDate = $("#end").val();
    var queryBpId = $("#queryBpId").val();

    var issueChannel = $("#issueChannel").val();

    var isCompPay = $('#compPay').is(':checked');

    var tmcId ='';
    if ($("#queryBpTmcId").val()) {
        tmcId = $("#queryBpTmcId").val();
    }

    var loading = layer.load();

    $.ajax({
        url:"car/carOrderList.json",
        data:{
            startDate:startDate,
            endDate:endDate,
            keyword:keyword,
            orderStatus:orderStatus,
            pageIndex:pageIndex,
            issueChannel:issueChannel,
            queryBpId:queryBpId,
            isCompPay:isCompPay,
            tmcId:tmcId,
            travelType:travelType
        },
        type:"POST",
        success:function(data){
            layer.close(loading);
            $("#hotelOrderTable").html(data);
            $('.drop_ul').hover(function(){
                $('.drop_ul').removeClass('drop_ul1');
                $(this).addClass('drop_ul1');
            });
        },
        error:function(){
            layer.close(loading);
            layer.msg("系统繁忙，请稍后再试");
        }
    });
}

function reloadExportOrder(){

    //收集参数
    //状态
    var orderStatus = "";
    $("#sel2 .selected").each(function(){
        if($(this).attr("status") == 0){
            orderStatus = "0";
        }else if(orderStatus != '0'){
            orderStatus = orderStatus + "" +$(this).attr("status")+',';
        }
    });

    if(orderStatus != ''){
        orderStatus = orderStatus.substring(0, orderStatus.length-1);
    }

    var travelType ="";
    $("#sel1 span.sp-traveltype").each(function(){
        if($(this).hasClass("selected")){
            travelType=$(this).attr("data-value");
        }
    })
    //关键字
    var keyword = $("#keyWords").val();
    //开始日期
    var startDate = $("#begin").val();
    //结束日期
    var endDate = $("#end").val();
    var queryBpId = $("#queryBpId").val();

    var issueChannel = $("#issueChannel").val();

    var isCompPay = $('#compPay').is(':checked');

    var tmcId ='';
    if ($("#queryBpTmcId").val() != undefined) {
        if (!$("#queryBpTmcId").val().isEmpty()){
            tmcId = $("#queryBpTmcId").val();
        }
    }

    window.location.href='car/exportCar?startDate='+startDate+'&endDate='+endDate+'&keyword='+keyword+'&orderStatus='+orderStatus+'&travelType='+travelType+'&issueChannel='+issueChannel+'&queryBpId='+queryBpId+'&isCompPay='+isCompPay+'&tmcId='+tmcId;

}

function initDate(range){
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	var begin ;
	if(range=='0'){//一月
		if((month-1)==0){//跨年时要判断
			year--;
			month=12;
		}else{
			month--;

		}
		begin = getBeginDate(year,month,day);
	}else if(range=='1'){//一年
		begin = getBeginDate(year-1,month,day);
	}else{//一周
		begin = getDay(-7);
	}
	
	$("#begin").val(begin);
	$("#end").val(new Date().format("yyyy-MM-dd"));
}

function getBeginDate(year, month, day){
	if(day > new Date(year,month,0).getDate()){
		var begin = year+"-"+(month<10?'0'+month:month)+"-"+ new Date(year,month,0).getDate();
	}else {
		var begin = year+"-"+(month<10?'0'+month:month)+"-"+ (day<10?'0'+day:day);
	}
	return begin;
}
function getDay(day){
    var today = new Date();
    var targetday_milliseconds=today.getTime() + 1000*60*60*24*day;
    today.setTime(targetday_milliseconds); //计算日期
    return today.format("yyyy-MM-dd");
}

function openHotelDetail(url){
    window.open(url);
}

