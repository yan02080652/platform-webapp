// 中文字两个字节
jQuery.validator.addMethod("byteRangeLength",
		function(value, element, param) {
			var length = value.length;
			for (var i = 0; i < value.length; i++) {
				if (value.charCodeAt(i) > 127) {
					length++;
				}
			}
			return this.optional(element)
					|| (length >= param[0] && length <= param[1]);
		}, $.validator.format("请确保输入的值在{0}-{1}个字节之间(一个中文字算2个字节)"));

// 邮政编码验证
jQuery.validator.addMethod("zipcode", function(value, element) {
	var tel = /^[0-9]{6}$/;
	return this.optional(element) || (tel.test(value));
}, "请正确填写您的邮政编码");

// 用户名等验证
jQuery.validator.addMethod("username", function(value, element) {
	var username = /^[0-9a-z_]+$/;
	return this.optional(element) || (username.test(value));
}, "请勿使用 [小写字母/数字/下划线] 以外的字符");

// 用户昵称等验证
jQuery.validator.addMethod("nickname", function(value, element) {
	var reg = /^[\u4e00-\u9fa5a-z0-9A-Z]+$/;
	return this.optional(element) || (reg.test(value));
}, "非法字符");

//手机验证
jQuery.validator.addMethod("mobile", function(value, element) {
	var reg = /^(1)\d{10}$/;
	return this.optional(element) || (reg.test(value));
}, "请输入正确的手机号码");

//年份名验证
jQuery.validator.addMethod("gradeName", function(value, element) {
	var certName = /^[0-9]+$/;
	return this.optional(element) || (certName.test(value));
}, "请勿使用 [数字] 以外的字符");

//数字和下划线
jQuery.validator.addMethod("numberAndUnderline", function(value, element) {
	var reg = /^[0-9]+([-][0-9]+[-][0-9]+)+$/;
	return this.optional(element) || (reg.test(value));
}, "请输入正确格式，支持纯数字加英文状态下的横线 '-'");

/**    
 * 自定义验证规则——增加对select的验证    
 */
jQuery.validator.addMethod("type", // name验证方法名     
	function(value, element) { // 验证规则     
		if (value == "-1") {
			return false;
		} else {
			return true;
		}
	}, "请选择");
/*
 * 自定义验证规则———结束日期必须小于起始日期
 */
jQuery.validator.addMethod("endDate", 
		function(value, element) {
		var startDate = $('#validForm1').val();
		return Date.parse(startDate.replace(/-/g, "/")) <= Date.parse(value.replace(/-/g, "/"));
		}, 
		"结束日期必须大于开始日期!");

//域名验证
jQuery.validator.addMethod("mailDomain", function(value, element) {
	var reg = /^(?=^.{3,255}$)[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+$/;
	return this.optional(element) || (reg.test(value));
}, "域名格式不正确");

//金额
jQuery.validator.addMethod("checkTenTwoNumber", function(value, element) {
	return this.optional(element) || /^\d{1,8}([\.]\d{1,2})?$/.test(value);
}, "请输入0~99999999.99內的金额");

//输入金额小于清账金额
jQuery.validator.addMethod("checkAmount", function(value,element) {
	var originalAmount =parseFloat($(element).attr("data-id"));
	var newAmount = parseFloat(value);
  return this.optional(element) || newAmount <= originalAmount;   
}, $.validator.format("清账单金额不大于未清账金额"));


//国际机票航班号格式校验
jQuery.validator.addMethod("interFlightNo", function(value, element, key) {
    var regex = /^[a-zA-Z\d]{1,8}$/;

    return this.optional(element) || regex.test(value);
}, "请录入正确的航班号格式");
//国际机票舱位校验
jQuery.validator.addMethod("interCabinCode", function(value, element, key) {
    var regex = /^[a-zA-Z\d]{0,2}$/;

    return this.optional(element) || regex.test(value);
}, "请录入正确的舱位格式");

//国际机票机场三字码校验
jQuery.validator.addMethod("interAirportCode", function(value, element, key) {
    var regex = /^[a-zA-Z]{0,20}$/;

    return this.optional(element) || regex.test(value);
}, "请录入正确的三字码格式");

//国际机票机场航站楼校验
jQuery.validator.addMethod("interTerminal", function(value, element, key) {
    var regex = /^[a-zA-Z\d]{0,3}$/;

    return this.optional(element) || regex.test(value);
}, "请录入正确的航站楼格式");

//国际机票时间校验  HHMM HHMM+1 HHMM-1
jQuery.validator.addMethod("interTime", function(value, element, key) {
    var regex = /^[\d]{4}$/;

    return this.optional(element) || regex.test(value);
}, "请录入正确的小时和分钟的HHMM格式");

//国际机票机型校验  HHMM HHMM+1 HHMM-1
jQuery.validator.addMethod("interAircraftType", function(value, element, key) {
    var regex = /^[a-zA-Z0-9\u4e00-\u9fa5]{1,15}$/;

    return this.optional(element) || regex.test(value);
}, "请录入正确的机型格式");

jQuery.validator.addMethod("contactorName", function(value, element, key) {
    var regex = /^[a-zA-Z\u4e00-\u9fa5]+$/;

    return this.optional(element) || regex.test(value);
}, "请录入正确的联系人名称格式");

//国际机票时间校验  HHMM HHMM+1 HHMM-1
jQuery.validator.addMethod("stopTime", function(value, element, key) {
    var regex = /^[\d]{4}$/;
    return this.optional(element) || regex.test(value);
}, "请录入正确的小时和分钟格式HHMM");

//国际机票姓/名
jQuery.validator.addMethod("interName", function(value, element, key) {
    var regex = /^[a-zA-Z\s]{1,30}$/;
    return this.optional(element) || regex.test(value);
}, "请录入正确的英文姓/名格式");

//国际机票票号
jQuery.validator.addMethod("ticketNo", function(value, element, key) {
    var regex = /^[a-zA-Z\d-]{1,16}$/;
    return this.optional(element) || regex.test(value);
}, "请录入正确的票号格式");

jQuery.validator.addMethod("pnrCode", function(value, element, key) {
    var regex = /^[a-zA-Z\d]{6}$/;
    return this.optional(element) || regex.test(value);
}, "请录入正确的PNR格式");
//验证中文
jQuery.validator.addMethod("checkCHN", function(value, element) {
    var reg =  /^[\u4e00-\u9fa5]+$/;
    return this.optional(element) || reg.test(value);
}, "请录入中文");

jQuery.validator.addMethod("internationalToDate",
    function(value, element) {
        var startDate = $(element).parent().siblings(".fnDiv").find(".fromDate").val();
        return parseInt(startDate) <= parseInt(value);
    },
    "到达日期必须大于出发日期!");

//国际机票姓/名
jQuery.validator.addMethod("interFullName", function(value, element, key) {
    var regex = /^[a-zA-Z\u4e00-\u9fa5\s]{1,30}$/;
    var reCh=/[\u4e00-\u9fa5]/;
    var strlen=0; //初始定义长度为0
    var txtval = value.trim();
    for(var i=0;i<txtval.length;i++){

        if(txtval.charAt(i) == " ") {
            strlen=strlen+1;
            continue;
        }
        if(reCh.test(txtval.charAt(i))==true){
            strlen=strlen+2;
        }else{
            strlen=strlen+1;
        }
    }
    return this.optional(element) || (regex.test(value) && strlen <=30);
}, "请录入正确的全名格式");


//长度校验
jQuery.validator.addMethod("cnAnEnLengh", function(value, element, key) {
    var reCh=/[\u4e00-\u9fa5]/;
    var strlen=0; //初始定义长度为0
    var txtval = value.trim();
    for(var i=0;i<txtval.length;i++){

        if(txtval.charAt(i) == " ") {
            strlen=strlen+1;
            continue;
        }
        if(reCh.test(txtval.charAt(i))==true){
            strlen=strlen+2;
        }else{
            strlen=strlen+1;
        }
    }
    return this.optional(element) || (strlen <=key);
}, $.validator.format("请确保输入的值在{0}个字节以内(一个中文字算2个字节)"));


jQuery.validator.addMethod("effectiveSaleDay", function(value, element, key) {
    var reg = /^(\d{8}-\d{8})(\,\d{8}-\d{8})*$/;
    return this.optional(element) || reg.test(value);;
}, "适用销售日期格式有误,请更正！");
