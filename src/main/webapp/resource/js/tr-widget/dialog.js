(function () {
    var __dlg = {
        create: function () {
            return function () {
                this.initialize.apply(this, arguments);
            }
        }
    }
    var dlg = __dlg.create();
    dlg.prototype = {
        initialize: function (config) {
            if (!layer || !$) {
                console.error('JQuery or layer is not define,pleas import layer first!');
                return;
            }
            if (!config.id) {
                console.error('id is an required parameter!');
                return;
            }
            var dc = {
                title: '标题',
                type: 0,
                content: '我是内容',
                width: 600,
                height: 565,//不包含已选清单高度
                tableHeight: 400,
                chooedHeight: 100,//已选清单高度
                sureCaption: '确定',
                closeCaption: '关闭',
                keyProperty: null,
                namePropery: null
            };
            $.extend(dc, config);
            this.config = dc;
            this.dlgId = 'dlg_' + dc.id;
            if (dc.multi) {
                dc.height += dc.chooedHeight;
                if (!dc.keyProperty || !dc.namePropery) {
                    console.error('keyProperty or namePropery is not define');
                    return;
                }
            }

            var cur = this;
            if (!config.content) {
                if (!config.url) {
                    console.error('url is not define!');
                    return;
                }
                layui.use('table', function () {
                    cur._createDom();
                });
            } else {
                console.error('not support custom content!');
            }
        },
        getId: function () {
            return this.config.id;
        },
        _createDom: function () {//自定义内容
            var cur = this;
            var conf = cur.config;
            var makeContent = $("<div style='padding: 8px 5px 0px;display: none;' id='" + cur.dlgId + "'/>");
            $('body').append(makeContent);
            //1、搜索条件
            if (conf.conditions) {
                makeContent.append(cur._createCondtions(conf.conditions));
            }
            conf.type = 1;
            conf.content = makeContent;
            conf.success = function () {
                //必须在对话框显示完毕构建元素，不然会乱掉
                //2、创建Table
                cur._insertTable(makeContent);

                //3.多选则创建已选清单
                if (conf.multi) {
                    cur._insertChoosedDom(makeContent);
                }
                //4.显示已选择的数据
                setTimeout(function () {
                    cur._showChoosed();
                }, 300);
                conf.success = function () {
                    cur._showChoosed();
                };
            };
            cur.show();
        },
        _createCondtions: function (conditions) {//构造搜索条件
            var cur = this;
            var conDiv = $("<div class='condition'/>");
            $.each(conditions, function (i, con) {
                if (!con.name) {
                    return;
                }
                if (con.label) {
                    conDiv.append("<span style='padding-left: 5px;padding-right: 5px;'>" + con.label + "</span>");
                }
                var input = $("<input class='layui-input' name='" + con.name + "' autocomplete='off'>");
                if (con.placeholder) {
                    input.attr('placeholder', con.placeholder);
                }
                if (con.value) {
                    input.val(conf.value);
                }
                if (con.width != undefined) {
                    input.css('width', con.width + 'px');
                }
                var conDom = $("<div class='layui-inline' style='margin-right: 4px;'/>").append(input)

                if (con.select && (con.selectUrl || con.selectData)) {
                    var chooseList = $("<div class='choose-list'/>");
                    chooseList.css({
                        width: (con.width || 200) + 'px',
                    });
                    conDom.append(chooseList);
                    conDom.mouseenter(function () {
                        $('.choose-list', $(this)).show()
                    })
                    conDom.mouseleave(function () {
                        $('.choose-list', $(this)).hide()
                    })

                    function keyupSearch() {
                        var my = $(this);
                        cur._loadCondtionData(con.selectUrl, con.name, my, function (res) {
                            var list = res.code == 0?(res.data || []):null;
                            cur._curListDom(con.name,con.keyName || con.name + '_key', my.next(), list);
                        })
                    }

                    input.keyup(keyupSearch)
                    input.focus(keyupSearch)
                }
                conDiv.append(conDom);
            });
            conDiv.append("<button class='layui-btn search' data-type='reload'>搜索</button>");
            return conDiv;
        },
        //加载数据
        _loadCondtionData(url, name, input, callback) {
            setTimeout(function () {
                var historyvalue = input.data('historyvalue');
                var value = input.val();
                if (historyvalue == value && historyvalue) {
                    return;
                }
                var data = {}
                data[name] = value
                $.ajax({
                    url:url,
                    data:data,
                    dataTpe:'json',
                    success:callback,
                    error:function () {
                        callback({code:-1})
                    }
                })
            }, 70)
        },
        //创建下拉列表
        _curListDom(conditionName,keyName, dom, list) {
            var cur = this;
            dom.empty();
            if (!list) {
                dom.append("<div class='info-tip'>服务异常</div>");
                return;
            }
            if (list.length == 0) {
                dom.append("<div class='info-tip'>无可选数据</div>");
                return;
            }
            if (list && list.length > 0) {
                //限制50个
                if (list.length > 50) {
                    list.length = 50;
                }
                list.forEach(function (li, index) {
                    var item = $("<div class='list-item'/>")
                    item.text(li[conditionName])
                    item.data(keyName, li[keyName]).data(conditionName, li[conditionName])
                    item.click(function () {
                        cur._selectConditionItem(conditionName,keyName, $(this));
                    })
                    dom.append(item)
                })
            }
        },
        //移至下一个焦点
        _selectConditionItem(conditionName,keyName, itemDom) {
            var input = $("#" + this.dlgId + " .condition [name='" + conditionName + "']");
            var key = itemDom.data(keyName) || null;
            var text = itemDom.data(conditionName) || null;
            input.val(text)
            input.data(keyName, key).data(conditionName, text);
            input.next().hide()

            var nextDom = input.parent().next();
            if (nextDom.hasClass('layui-inline')) {
                $('input', nextDom).focus();
            } else {
                $("#" + this.dlgId + " .condition .search").trigger('click');
            }
        },
        _insertTable: function (makeContent) {
            var cur = this;
            var conf = cur.config;
            var tableId = "table_" + conf.id;
            var contentTable = $("<table id='" + tableId + "' class='layui-hide' lay-filter='" + tableId + "'></table>");
            makeContent.append(contentTable);
            //方法级渲染
            var table = layui.table;
            table.render({
                elem: '#' + tableId,
                url: conf.url,
                where: conf.parameter,
                request: {
                    pageName: 'pageIndex',
                    limitName: 'pageSize'
                },
                limits: [15, 30, 50],
                limit: 15,
                response: {
                    statusName: 'statusCode',
                    statusCode: 0,
                    msgName: 'msg',
                    countName: 'count',
                    dataName: 'data'
                },
                cols: conf.cols,
                /*cols: [[
                 {checkbox: true, fixed: true}
                 ,{field:'id', title: 'ID', width:80, sort: true, fixed: true}
                 ,{field:'sex', title: '性别', width:80, sort: true}
                 ]]*/
                height: conf.tableHeight,
                id: tableId,
                page: true,
                //skin:'line',
                done: function (res, curr, count) {
                    cur._asyncCheckStatus(res.data || []);
                }
            });
            var active = {
                reload: function () {
                    var parameter = conf.parameter || {};
                    $.each(conf.conditions, function (i, condition) {
                        var input = $("#" + cur.dlgId + " .condition [name='" + condition.name + "']");
                        parameter[condition.name] = input.val();
                        if(condition.select && condition.selectUrl){
                            parameter[condition.keyName || (condition.name+'_key')] = input.val()?input.data(condition.keyName):null;
                        }
                    });
                    table.reload(tableId, {
                        where: parameter,
                        page:{
                            curr:1
                        }
                    });
                }
            };

            $('#' + cur.dlgId + ' .condition .layui-btn').on('click', function () {

                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
            });

            table.on("checkbox(" + tableId + ")", function (obj) {
                /*console.log(obj.checked); //当前是否选中状态
                console.log(obj.data); //选中行的相关数据
                console.log(obj.type); //如果触发的是全选，则为：all，如果触发的是单选，则为：one*/
                var data = null;
                if (obj.type == 'all') {//全选
                    if (obj.checked) {
                        data = table.checkStatus(tableId).data;
                    } else {
                        data = table.cache[tableId];
                    }
                } else {//单选
                    data = [obj.data];
                    if (!data || data.length == 0 || $.isEmptyObject(data[0])) {
                        data = table.cache[tableId];
                    }
                }
                $.each(data, function (i, d) {

                    if (obj.checked) {
                        cur._addOne(d);
                    } else {
                        cur._removeOne(d);
                    }
                });
            });

            /*table.on("radio("+tableId+")", function(obj){
                //console.log(obj.checked); //当前是否选中状态
                //console.log(obj.data); //选中行的相关数据
            });*/
        },
        _insertChoosedDom: function (makeContent) {

            var chooedHeight = this.config.chooedHeight;
            makeContent.append($("<div class='choosed' style='min-height: " + chooedHeight + "px;border: 1px solid #e2e2e2;padding-left: 4px;padding-top: 2px;'><span>已选列表：</span></div>"));
        },
        _showChoosed: function () {
            //清空
            this._empty();
            //显示已选择的列表
            var conf = this.config;
            if (conf.getChoosedData) {
                var choosedData = conf.getChoosedData();
                if (choosedData && choosedData.length > 0) {
                    //TODO choosedData 可以通过url加载成更完整的信息
                    var cur = this;
                    $.each(choosedData, function (i, data) {
                        cur._addOne(data, false);
                    });
                }
            }
            this._asyncCheckStatus();
        },
        _asyncCheckStatus: function (data) {
            var conf = this.config;
            //同步表格选中状态：用于数据重新加载、数据不重新加载但窗口被重新加载
            if (!data) {
                data = layui.table.cache['table_' + conf.id];
            }
            if (data && data.length > 0) {
                var tableEl = $('#' + this.dlgId + ' .layui-table-body');
                //去掉选中的
                $('.layui-form-checked', tableEl).trigger('click');
                var selData = this._getSelMapData();
                if (!selData) {
                    return;
                }
                $.each(data, function (i, d) {
                    if (selData[d[conf.keyProperty]]) {
                        tableEl.find("tbody tr:eq(" + i + ") .layui-form-checkbox").trigger('click');
                    }
                });
            }
        },
        _empty: function () {
            $('#' + this.dlgId + ' .layui-form-checked').trigger('click');
            $('#' + this.dlgId + ' .choosed .cdspan').remove();
        },
        _addOne: function (data, checkDuplicate) {

            //check=false 不用检查重复性，只用于第一次初始化
            var cur = this;
            var conf = cur.config;
            var key = data[conf.keyProperty], name = data[conf.namePropery];
            var choosedDiv = $('#' + cur.dlgId + ' .choosed');
            if (checkDuplicate != false) {
                var flagSpan = null;
                $.each($('.cdspan', choosedDiv), function (i, span) {
                    if ($(span).data('key') == key) {
                        flagSpan = span;
                        return false;
                    }
                });

                if (flagSpan) {//TODO 已存在的选项，可友好提示
                    return false;
                }
            }
            var newSpan = $("<span class='cdspan' style='font-weight: bold;padding: 0px 5px;'/>").text(name).data('key', key).data('userData', data);
            var removeBtn = $("<i class=\"fa fa-close\" aria-hidden=\"true\" title='移出' style='padding: 0 4px;cursor: pointer;font-size: 16px;'>");
            removeBtn.click(function () {
                var rmSpan = $(this).parent();
                cur._removeTableCheck(rmSpan);
            });
            newSpan.append(removeBtn);
            choosedDiv.append(newSpan);
        },
        _removeOne: function (data) {
            var conf = this.config;
            var key = data[conf.keyProperty];
            $.each($('#' + this.dlgId + ' .choosed .cdspan'), function (i, span) {
                if ($(span).data('key') == key) {
                    $(span).remove();
                    return false;
                }
            });
        },
        _removeTableCheck: function (rmSpan) {
            var conf = this.config;
            var key = rmSpan.data('key');
            var keyProperty = conf.keyProperty;
            rmSpan.remove();
            var tableId = 'table_' + conf.id;
            var datas = layui.table.cache[tableId];
            $.each($('#' + this.dlgId + ' .layui-table-body .layui-form-checked'), function (i, cbDiv) {
                var tr = $(cbDiv).parentsUntil('tbody', 'tr');
                var index = tr.data('index');
                if (index >= 0) {
                    if (datas[index][keyProperty] == key) {
                        $(cbDiv).trigger('click');
                        return false;
                    }
                }
            });
        },
        show: function () {
            var cur = this;
            var conf = cur.config;

            var btns = ['确定', '关闭'];
            if (conf.empty) {
                btns.push('清空');
            }
            layer.open({
                title: conf.title,
                type: conf.type,
                content: conf.content,
                success: conf.success,
                area: [conf.width + 'px', conf.height + 'px'],
                btn: btns,
                yes: function (index, layero) {
                    cur.close(index);
                    if (conf.yes) {
                        var data = cur._getSelData();
                        conf.yes(data);
                    }
                },
                btn2: function (index) {
                    cur.close(index);
                },
                btn3: function (index) {
                    cur._empty();
                    cur.close(index);
                    if (conf.empty && conf.yes) {
                        conf.yes();
                    }
                }
            });
        },
        close: function (index) {
            layer.close(index);
            //$('#dlg_'+this.config.id).hide();
        },
        _getSelData: function () {
            //获取已选择的数据(多选：Array格式，单选：Object)
            var conf = this.config;
            var tableId = 'table_' + conf.id;
            if (conf.multi) {
                /*var checkStatus = table.checkStatus(tableId);
                console.log(checkStatus.data)*/
                var datas = [];
                $.each($('#' + this.dlgId + ' .choosed .cdspan'), function (i, span) {
                    datas.push($(span).data('userData'));
                });
                return datas;
            } else {
                //console.log(table.cache[tableId]);
                //var radioStatus = layui.table.radioStatus(tableId);
                var radioStatus = layui.table.checkStatus(tableId);
                var data = radioStatus.data;
                return data.length > 0 ? data[0] : null;
            }
        },
        _getSelMapData: function () {
            //获取已选择的数据(Map格式)
            var spans = $('#' + this.dlgId + ' .choosed .cdspan');
            if (spans.length == 0) {
                return null;
            }
            var mapData = {};
            $.each(spans, function (i, span) {
                mapData[$(span).data('key')] = $(span).data('userData');
            });
            return mapData;
        }
    };
    window._dlg = dlg;
})();

var choose = {
    id: 6666,
    getId: function () {
        return this.id++;
    },
    tpls: {
        severPartner: 'template'
    },
    tplsLoaded: {},
    dlgs: {},
    loadTpl: function (name, callback) {
        var fileName = this.tpls[name];
        if (!fileName) {
            callback();
            //console.error(name + ' template is not define!');
            return;
        }
        if (this.tplsLoaded[fileName]) {
            callback();
            return;
        }
        $.get('resource/js/tr-widget/' + fileName + '.html', function (txt) {
            $('body').append(txt);
            choose.tplsLoaded[fileName] = true;
            callback();
        });
    },
    saveDlg: function (name, dlg) {//保存选择器，防止使用方一直new 对象操作造成不必要的浪费
        var obj = this.dlgs[name];
        if (!obj) {
            obj = {};
            this.dlgs[name] = obj;
        }
        var id = dlg.getId();
        obj[id] = dlg;
    },
    init: function (conf, callback, name) {
        //检查是否已经有对象存在
        var dlgs = this.dlgs[name];
        var _dlg = dlgs && conf.id ? dlgs[conf.id] : null;

        if (typeof conf == 'function') {
            conf = {
                yes: conf
            };
        } else {
            conf.yes = callback;
        }
        if (!conf.id) {
            conf.id = this.getId();
        }
        return {
            _dlg: _dlg,
            conf: conf
        }
    }
};

/**
 * 选择TMC客服
 * @param config
 */
choose.severPartner = function (config, callback) {
    var name = 'severPartner';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        choose.loadTpl(name, function () {
            var defaultConf = {
                title: '选择客服人员',
                conditions: [
                    {name: 'searchName', placeholder: '客服名称', width: 300}
                ],
                cols: [[ //标题栏
                    {rowspan: 2, width: 48},
                    {field: 'fullname', title: '客服人员', width: 100, rowspan: 2, templet: '<div>{{d.fullname}}</div>'},
                    {title: '工作条线', colspan: 5, align: 'center'},
                ], [
                    {field: 'flight', title: '机票', width: 82, align: 'center', templet: '#tpl_serverSkill_flight'},
                    {field: 'hotel', title: '酒店', width: 82, align: 'center', templet: '#tpl_serverSkill_hotel'},
                    {field: 'train', title: '火车', width: 82, align: 'center', templet: '#tpl_serverSkill_train'},
                    {
                        field: 'insurance',
                        title: '保险',
                        width: 82,
                        align: 'center',
                        templet: '#tpl_serverSkill_insurance'
                    },
                    {field: 'general', title: '通用', width: 82, align: 'center', templet: '#tpl_serverSkill_general'}
                ]],
                url: 'pss/serverPartner/spData',
                multi: false,
                page: true,
                empty: false,
                keyProperty: 'userId',//多选的必需字段，用于重复判断。
                namePropery: 'fullname',//显示名称
                //width:700,
            };
            $.extend(defaultConf, obj.conf);
            var cbBtn = defaultConf.cols[0][0];
            //cbBtn[defaultConf.multi ? 'checkbox' : 'radio'] = true;
            cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
            var dlg = new _dlg(defaultConf);
            choose.saveDlg(name, dlg);
        });
    }
};

choose.hotel = function (config, callback) {
    var name = 'hotel';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        //select:true 是否下拉框
        // selectUrl:'' 请求下拉框数据的路径, 参数名是city
        // 如果有需要，可支持selectData等非后台请求数据，比如男女人妖。
        // key:null  编码值  比如 key=beijin text=北京
        // text:null 文本值
        var defaultConf = {
            title: '选择酒店',
            conditions: [
                {
                    name: 'cityName',
                    keyName:'cityCode',
                    placeholder: '城市',
                    width: 150,
                    select: true,
                    selectUrl: 'hotel/findCityList.json'
                },
                {name: 'name', placeholder: '酒店名称', width: 250},
            ],
            cols: [[ //标题栏
                {rowspan: 2, width: 48},
                {field: 'name', title: '酒店', width: 500}
            ]],
            url: 'hotel/orderInterceptRule/hotelData',
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'hotelId',//多选的必需字段，用于重复判断。
            namePropery: 'name',//显示名称
            width: 730,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        //cbBtn[defaultConf.multi ? 'checkbox' : 'radio'] = true;
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};

choose.hotelByBrand = function (brandId, config, callback) {
    var name = 'hotelByBrand';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        var defaultConf = {
            title: '选择酒店',
            conditions: [
                {
                    name: 'cityName',
                    keyName:'cityCode',
                    placeholder: '城市',
                    width: 150,
                    select: true,
                    selectUrl: 'hotel/findCityList.json'
                },
                {name: 'name', placeholder: '酒店名称', width: 300},
            ],
            cols: [[ //标题栏
                {rowspan: 2, width: 48},
                {field: 'name', title: '酒店', width: 500}
            ]],
            url: 'hotel/priceRuleTemplate/hotelData?brandId=' + brandId,
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'id',//多选的必需字段，用于重复判断。
            namePropery: 'name',//显示名称
            width: 700,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};

choose.hotelRoomByBrandAndHotelIds = function (policyId, config, callback) {
    var name = 'hotelRoomByBrandAndHotelIds';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        var defaultConf = {
            title: '选择酒店房型',
            conditions: [
                {name: 'name', placeholder: '酒店房型名称', width: 300},
            ],
            cols: [[ //标题栏
                {rowspan: 2, width: 48},
                {field: 'name', title: '酒店房型', width: 400}
            ]],
            url: 'hotel/policy/hotelRoomData?policyId=' + policyId,
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'name',//多选的必需字段，用于重复判断。
            namePropery: 'name',//显示名称
            width: 554,
            height: 520,
            tableHeight: 355,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};

choose.hotelRooms = function (brandId, applyType, interceptId, config, callback) {
    var name = 'hotelRooms';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        var defaultConf = {
            title: '选择酒店房型',
            conditions: [
                {name: 'name', placeholder: '酒店房型名称', width: 300},
            ],
            cols: [[ //标题栏
                {rowspan: 2, width: 48},
                {field: 'name', title: '酒店房型', width: 400}
            ]],
            url: 'hotel/orderInterceptRule/roomContent?brandId=' + brandId + '&applyType=' + applyType + '&interceptId=' + interceptId,
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'name',//多选的必需字段，用于重复判断。
            namePropery: 'name',//显示名称
            width: 554,
            tableHeight: 355,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};

choose.hotelCity = function (config, callback) {
    var name = 'hotelCity';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        var defaultConf = {
            title: '选择城市',
            conditions: [
                {name: 'name', placeholder: '城市名称', width: 300},
            ],
            cols: [[ //标题栏
                {rowspan: 2, width: 48},
                {field: 'nameCn', title: '城市', width: 500}
            ]],
            url: 'hotel/orderInterceptRule/cityData',
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'code',//多选的必需字段，用于重复判断。
            namePropery: 'nameCn',//显示名称
            width: 730,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        //cbBtn[defaultConf.multi ? 'checkbox' : 'radio'] = true;
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};

choose.hotelBrand = function (config, callback) {
    var name = 'hotelBrand';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        var defaultConf = {
            title: '选择酒店品牌',
            conditions: [
                {name: 'name', placeholder: '品牌名称', width: 300},
            ],
            cols: [[ //标题栏
                {rowspan: 2, width: 48},
                {field: 'shortName', title: '名称', width: 500}
            ]],
            url: 'hotel/orderInterceptRule/brandData',
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'id',//多选的必需字段，用于重复判断。
            namePropery: 'shortName',//显示名称
            width: 730,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        //cbBtn[defaultConf.multi ? 'checkbox' : 'radio'] = true;
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};

choose.districtData = function (config, callback) {
    var name = 'districtData';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        var defaultConf = {
            title: '选择城市',
            conditions: [
                {name: 'name', placeholder: '城市名称', width: 300},
            ],
            cols: [[ //标题栏
                {rowspan: 2, width: 48, type: 'radio'},
                {field: 'namePath', title: '名称', width: 500}
            ]],
            url: 'train/station/districtData',
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'id',//多选的必需字段，用于重复判断。
            namePropery: 'nameCn',//显示名称
            width: 730,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        //cbBtn[defaultConf.multi ? 'checkbox' : 'radio'] = true;
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};

choose.flightDistrictData = function (config, callback) {
    var name = 'districtData';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        var defaultConf = {
            title: '选择城市',
            conditions: [
                {name: 'name', placeholder: '城市名称', width: 300},
            ],
            cols: [[ //标题栏
                {rowspan: 2, width: 48},
                {field: 'nameCn', title: '名称', width: 500}
            ]],
            url: 'flight/airport/districtData',
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'id',//多选的必需字段，用于重复判断。
            namePropery: 'nameCn',//显示名称
            width: 730,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        //cbBtn[defaultConf.multi ? 'checkbox' : 'radio'] = true;
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};

//航司选择器
choose.carrierData = function (config, callback) {
    var name = 'carrierData';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        var defaultConf = {
            title: '选择航司',
            conditions: [
                {name: 'keyword', placeholder: '编码/名称', width: 300},
            ],
            cols: [[ //标题栏
                {rowspan: 1, width: 48},
                {field: 'code', title: '编码', width: 150},
                {field: 'nameShort', title: '简称', width: 150},
                {field: 'nameCn', title: '中文名称', width: 198},
                {field: 'nameEn', title: '英文名称', width: 150},
            ]],
            url: 'flight/carrier/carrierData',
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'code',//多选的必需字段，用于重复判断。
            namePropery: 'code',//显示名称
            width: 730,
            height: 520,
            tableHeight: 355,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        //cbBtn[defaultConf.multi ? 'checkbox' : 'radio'] = true;
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};

//用车城市选择器
choose.carCity = function (config, callback) {
    var name = 'carCity';
    var obj = choose.init(config, callback, name);
    if (obj._dlg) {//使用原有对象
        obj._dlg.show();
        return;
    } else {//重新创建对象
        var defaultConf = {
            title: '选择城市',
            conditions: [
                {name: 'name', placeholder: '城市名称', width: 300},
            ],
            cols: [[ //标题栏
                {rowspan: 1, width: 48},
                {field: 'name', title: '城市', width: 500}
            ]],
            url: 'car/priceRule/detail/getCityData',
            multi: false,
            page: true,
            empty: false,
            keyProperty: 'cityId',//多选的必需字段，用于重复判断。
            namePropery: 'name',//显示名称
            width: 730,
        };
        $.extend(defaultConf, obj.conf);
        var cbBtn = defaultConf.cols[0][0];
        //cbBtn[defaultConf.multi ? 'checkbox' : 'radio'] = true;
        cbBtn.type = defaultConf.multi ? 'checkbox' : 'radio';
        var dlg = new _dlg(defaultConf);
        choose.saveDlg(name, dlg);
    }
};
