platform.selectFlightCompany = function(config){
	var obj = this.flightCompany;
	config = config || {};
	obj.curConfig = config;
	obj.appendToBody(function() {
		obj.showDlg();
	});
};

platform.flightCompany = {
		id:'dlg_selectFlightCompany',
		appendToBody : function(callback) {// 请求选择器文本内容
			if (this._append_Mark) {
				callback();
				return;
			}
			var objDiv = "<div id='" + this.id + "'/>";
			$('body').append(objDiv);
			$('#' + this.id).load('widget/flightCompany/show.html', function(context, state) {
				if (state == 'success') {
					callback();
				} else {
					showErrorMsg('选择器加载失败。');
				}
			});
			this._append_Mark = true;
		},
		showDlg : function() {
			$("#"+this.id+" .modal-title").text('选择航司');
			var dlg = $('#' + this.id + " .modal:first-child");
			dlg.modal({
				keyboard : true
			});
			this.initEvent();
			dlg.on('shown.bs.modal', function () {
				platform.flightCompany.search();
			});
		},
		initEvent:function(){
			var cur = this;
			$('#'+cur.id+' .codeInput').keydown(function(event){
				if(event.keyCode == 13){
					cur.search();
				}
			});
			$('#trWg_btnSearch_flightCompany').click(function(){
				cur.search();
			});
		},
		initTabelEvent:function(){
			var cur = this;
			var trs = $('#'+cur.id+' tbody tr');
			trs.click(function(){
				$('#'+cur.id+' tbody .warning').not(this).removeClass('warning');
				$(this).toggleClass('warning');
			});		
			$('#'+this.id+' tbody tr').dblclick(function(){
				platform.flightCompany.sure(this);
			});
		},
		search:function(pageIndex,pageSize){
			var codeInput = $('#'+this.id+' .codeInput');
			$('#trWg_table_flightCompany').load("widget/flightCompany/search",{
				key:codeInput.val(),
                pageIndex:pageIndex,
				pageSize:pageSize
			},function(){
				platform.flightCompany.initTabelEvent();
			});
		},
		turnTo:function(pageIndex,pageSize){
			this.search(pageIndex, pageSize);
		},
		sure:function(tr){
			if(!tr){
				var selected = $('#'+this.id+' tbody .warning');
				if(selected.length == 0){
					$.alert('请选择一条数据','提示');
					return;
				}
				tr = selected[0];
			}
			var trJq = $(tr);
			var data = {
					id:trJq.data('id'),
					name:trJq.data('name'),
					code:trJq.data('code'),
                	nameShort:trJq.data('short')
			};
			$('#' + this.id + " .modal:first-child").modal('hide');
			var conf = this.curConfig;
			if(conf.callback){
				conf.callback(data);
			}
		}
};