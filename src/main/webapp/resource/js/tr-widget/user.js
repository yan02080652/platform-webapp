platform.selectUsers = function(config){
	var orgObj = this.users;
	config = config || {};

    if(orgObj.curConfig && orgObj.curConfig.partnerId != config.partnerId){
        $('#'+orgObj.id).remove();
        orgObj._append_Mark = false;
        orgObj.forceFlush = true;
    }else{
        orgObj.forceFlush = false;
	}

	if(!config.selected){
		config.selected = [];
	}
	orgObj.curConfig = config;
	orgObj.appendToBody(function() {
		orgObj.showDlg();
	});
};

platform.users = {
		id : 'dlg_selectUsers',
		appendToBody : function(callback) {// 请求选择器文本内容
			if (this._append_Mark) {
				callback();
				return;
			}
			var orgDiv = "<div id='" + this.id + "'/>";
			$('body').append(orgDiv);
			$('#' + this.id).load('widget/user/show.html?selectType=m&partnerId='+(this.curConfig.partnerId || '')+'&type='+(this.curConfig.type || ''), function(context, state) {
				if (state == 'success') {
					callback();
				} else {
					showErrorMsg('选择器加载失败。');
				}
			});
			this._append_Mark = true;
		},
		showDlg : function() {
			var dlg = $('#' + this.id + " .modal:first-child");
			dlg.modal({
				keyboard : true
			});
			this.initEvent();
			this.initSelected();
			
			dlg.on('shown.bs.modal', function () {
				platform.users.search();
			});
		},
		initEvent:function(){
			var cur = this;
			$('#'+cur.id+' .orgInput').keydown(function(event){
				//$('#trWg_btnSearch_users').show();
				if(event.keyCode == 13){
					cur.search();
				}
			});
			$('#trWg_btnSearch_users').click(function(){
				cur.search();
			});
		},
		initTabelEvent:function(){
			var cur = this;
			var trDom = $('#'+cur.id+' tbody tr');
			trDom.click(function(){
				$(this).toggleClass('warning');
			});
			trDom.dblclick(function(){
				$(this).removeClass('warning');
				cur.addTrDom(this);
			});
		},
		_selectDropdownOrg:function(dom){//选择单位
			var cur = this;
			TR.select('dropdownOrg',{
                forceFlush:cur.forceFlush,
                partnerId:cur.curConfig.partnerId,
                dom:dom,
                allowBlank:cur.curConfig.allowBlank || false,
				type:cur.curConfig.type || null,
				blankText:'不限制单位'
			},function(data){
				$('#trWg_input_orgs').val(data?data.id:null);
				$(dom).find('div').text(data?data.name:'单位');
				//$('#trWg_btnSearch_users').hide();
				cur.search();
			});
		},
		search:function(start,pageSize){
			var orgInput = $('#'+this.id+' .orgInput');
			var key = orgInput.val();
			var orgId = $('#trWg_input_orgs').val();
			if(!key && !orgId){
				orgInput.attr('title','请输入查询条件').focus().tooltip('show');
				setTimeout(function(){
					orgInput.tooltip('destroy');
				}, 2000);
				return;
			}
			
			$('#trWg_table_users').load("widget/user/search",{
				key:key,
				orgId:orgId,
				start:start,
				pageSize:pageSize,
				selectType:'m'
			},function(){
				platform.users.initTabelEvent();
			});
		},
		turnTo:function(start,pageSize){
			this.search(start, pageSize);
		},
		initSelected : function() {// 初始化已选列表
			var selDom = $('#' + this.id + ' .selected');
			selDom.empty();
			if (this.curConfig.selected) {
				$.each(this.curConfig.selected,function(i, sel) {
					platform.users.addDom(sel);
				});
			}
		},
		add : function(user){
			var i = this.indexOf(user.id);
			if(i != -1){//已经存在，不可添加
				var dom = $('#' + this.id + ' .selected li:eq('+i+')').css('background-color','antiquewhite');
				setTimeout(function(){
					dom.css('background-color','transparent');
				}, 700);
				return;
			}
			this.curConfig.selected.push(user);
			this.addDom(user);
		},
		addDom : function(user){
			var selDom = $('#' + this.id + ' .selected');
			var newDom = $("<li class='list-group-item'>"+ user.fullname+ "<span class='glyphicon glyphicon-remove' onclick='platform.users.remove(this,"+user.id+")'></span></li>");
			selDom.append(newDom);
		},
		remove : function(dom,id) {
			$(dom).parent().remove();
			var index = this.indexOf(id);
			var selected = this.curConfig.selected;
			selected = selected.del(index);
		},
		indexOf:function(id){
			var tempIndex = -1;
			if(this.curConfig.selected){
				$.each(this.curConfig.selected,function(i,sel){
					if(sel.id == id){
						tempIndex = i;
						return false;
					}
				});
			}
			return tempIndex;
		},
		addSelectedTr:function(){
			var cur = this;
			var trs = $('#'+cur.id+' tbody .warning');
			$.each(trs,function(i,tr){
				cur.addTrDom(tr);
			});
			trs.removeClass('warning');
		},
		addTrDom:function(tr){
			var trJq = $(tr);
			this.add({
				id:trJq.data('id'),
				fullname:trJq.data('fullname')
			});
		},
		sure:function(){
			$('#' + this.id + " .modal:first-child").modal('hide');
			var conf = this.curConfig;
			if(conf.callback){
				conf.callback(conf.selected);
			}
		}
};

platform.selectUser = function(config){
	var orgObj = this.user;
    config = config || {};

	if(orgObj.curConfig && orgObj.curConfig.partnerId != config.partnerId){
        $('#'+orgObj.id).remove();
        orgObj._append_Mark = false;
        orgObj.forceFlush = true;
    }else{
        orgObj.forceFlush = false;
    }

	if(!config.selected){
		config.selected = [];
	}
	orgObj.curConfig = config;
	orgObj.appendToBody(function() {
		orgObj.showDlg();
	});
};

platform.user = {
		id:'dlg_selectUser',
		appendToBody : function(callback) {// 请求选择器文本内容
			if (this._append_Mark) {
				callback();
				return;
			}
			var orgDiv = "<div id='" + this.id + "'/>";
			$('body').append(orgDiv);
			$('#' + this.id).load('widget/user/show.html?selectType=s&partnerId='+(this.curConfig.partnerId || '')+'&type='+(this.curConfig.type || ''),function(context, state) {
				if (state == 'success') {
					callback();
				} else {
					showErrorMsg('选择器加载失败。');
				}
			});
			this._append_Mark = true;
		},
		showDlg : function() {
			var dlg = $('#' + this.id + " .modal:first-child");
			dlg.modal({
				keyboard : true
			});
			this.initEvent();
			dlg.on('shown.bs.modal', function () {
				platform.user.search();
			});
		},
		initEvent:function(){
			var cur = this;
			$('#'+cur.id+' .orgInput').keydown(function(event){
				if(event.keyCode == 13){
					cur.search();
				}
			});
			$('#trWg_btnSearch_user').click(function(){
				cur.search();
			});
		},
		initTabelEvent:function(){
			var cur = this;
			var trs = $('#'+cur.id+' tbody tr');
			trs.click(function(){
				$('#'+cur.id+' tbody .warning').not(this).removeClass('warning');
				$(this).toggleClass('warning');
			});		
			$('#'+this.id+' tbody tr').dblclick(function(){
				platform.user.sure(this);
			});
		},
		_selectDropdownOrg:function(dom){//选择单位
			var cur = this;
			TR.select('dropdownOrg',{
                forceFlush:cur.forceFlush,
				partnerId:cur.curConfig.partnerId || null,
                type:cur.curConfig.type || null,
				dom:dom,
				allowBlank:cur.curConfig.allowBlank || false,
				blankText:'不限制单位'
			},function(data){
				$('#trWg_input_org').val(data?data.id:null);
				$(dom).find('div').text(data?data.name:'单位');
				cur.search();
			});
		},
		search:function(start,pageSize){
			var orgInput = $('#'+this.id+' .orgInput');
			var key = orgInput.val();
			var orgId = $('#trWg_input_org').val();
			if(!key && !orgId){
				orgInput.attr('title','请输入查询条件').focus().tooltip('show');
				setTimeout(function(){
					orgInput.tooltip('destroy');
				}, 2000);
				return;
			}
			
			$('#trWg_table_user').load("widget/user/search",{
				key:key,
				orgId:orgId,
				start:start,
				pageSize:pageSize,
				selectType:'s'
			},function(){
				platform.user.initTabelEvent();
			});
		},
		turnTo:function(start,pageSize){
			this.search(start, pageSize);
		},
		sure:function(tr){
			if(!tr){
				var selected = $('#'+this.id+' tbody .warning');
				if(selected.length == 0){
					$.alert('提示','请选择一位员工');
					return;
				}
				tr = selected[0];
			}
			var trJq = $(tr);
			var data = {
					id:trJq.data('id'),
					fullname:trJq.data('fullname')
			};
			$('#' + this.id + " .modal:first-child").modal('hide');
			var conf = this.curConfig;
			if(conf.callback){
				conf.callback(data);
			}
		}
};