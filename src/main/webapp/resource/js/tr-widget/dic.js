platform.selectDic = function(config){
	if(!config.sysId || !config.dicCode){
		throw new error("【select】 param must be not null");
	}
	$.ajax({
		type : 'GET',
		url : "platform/syscfgCatalogCfg/getDic.do?sysId="+config.sysId+"&dicCode="+config.dicCode,
		success : function(data) {
			TR.select(data,{
				key:config.key,//多个自定义下拉选择时key值不能重复
				dom:config.dom,
				isTree:true,
				id:'id',//default:name
				pid:'pid',
				name:'itemTxt',
				allowBlank:config.allowBlank,//是否允许空白选项
				blankText:config.blankText
			},config.callback);
		}
	});
	
};