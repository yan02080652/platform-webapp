/**
 * 组织多选选择器 参数说明： selected:[{id:1,name:'A单位'},{id:2,name:'B单位'}] 已选列表
 */
platform.orgs = {
	id : 'dlg_selectOrgs',
	appendToBody : function(callback) {// 请求选择器文本内容
		if (this._append_Mark) {
			callback();
			return;
		}
		var orgDiv = "<div id='" + this.id + "'/>";
		$('body').append(orgDiv);
		$('#' + this.id).load('widget/org/show.html?selectType=m', function(context, state) {
			if (state == 'success') {
				callback();
			} else {
				showErrorMsg('选择器加载失败。');
			}
		});
		this._append_Mark = true;
	},
	showDlg : function() {
		$('#' + this.id + " .modal:first-child").modal({
			keyboard : true
		});
		this.initSelected();
		this.initTree();
	},
	initTree : function() {
		if(this._initTree_mark){
			return;
		}
		var setting = {
			view : {
				selectedMulti: true,
				showIcon : true,
				showLine : true,
				dblClickExpand : false
			},
			async : {
				enable : true,
				contentType : "application/json",
				url : "widget/org/getOrgs.aj",
				type : 'get',
				autoParam : [ "id" ],
				otherParam:{
					partnerId:this.curConfig.partnerId || null,
					type:this.curConfig.type || null
				},
				dataFilter : dataFilter
			},
			callback : {
				onDblClick : onDblClick
			}
		};
		this.zTreeObj = $.fn.zTree.init($("#tr_treeOrgs"), setting, null);
		this._initTree_mark = true;

		function onDblClick(event, treeId, treeNode) {
			if(treeNode && treeNode.id){
				platform.orgs.add({
					id:treeNode.id,
					name:treeNode.name,
					data:treeNode.data
				});
			}
		}

		function dataFilter(treeId, parentNode, responseData) {
			var treeData = [];
			$.each(responseData, function(i, org) {
				treeData.push({
					id : org.id,
					name : org.name,
					isParent : org.hasChild,
					pid : org.pid,
					data:org
				});
			});
			return treeData;
		}
	},
	getOrgData:function(callback,opt){
		opt = opt || {};
		if(this.orgData){
			callback(this.orgData);
		}
		$.getJSON('widget/org/getAllOrgs.aj',{
			partnerId:opt.partnerId || null,
			type:opt.type || null
		},function(data){
			this.orgData = data;
			callback(this.orgData);
		});
	},
	initSelected : function() {// 初始化已选列表
		var selDom = $('#' + this.id + ' .selected');
		selDom.empty();
		if (this.curConfig.selected) {
			$.each(this.curConfig.selected,function(i, sel) {
				platform.orgs.addDom(sel);
			});
		}
	},
	add : function(node){
		var i = this.indexOf(node.id);
		if(i != -1){//已经存在，不可添加
			var dom = $('#' + this.id + ' .selected li:eq('+i+')').css('background-color','antiquewhite');
			setTimeout(function(){
				dom.css('background-color','transparent');
			}, 700);
			return;
		}
		this.curConfig.selected.push(node);
		this.addDom(node.data);
	},
	addDom : function(org){
		var selDom = $('#' + this.id + ' .selected');
		var newDom = $("<li class='list-group-item'>"+ org.name+ "<span class='glyphicon glyphicon-remove' onclick='platform.orgs.remove(this,"+org.id+")'></span></li>");
		selDom.append(newDom);
	},
	remove : function(dom,id) {
		$(dom).parent().remove();
		var index = this.indexOf(id);
		var selected = this.curConfig.selected;
		selected = selected.del(index);
	},
	indexOf:function(id){
		var tempIndex = -1;
		if(this.curConfig.selected){
			$.each(this.curConfig.selected,function(i,sel){
				if(sel.id == id){
					tempIndex = i;
					return false;
				}
			});
		}
		return tempIndex;
	},
	sure:function(){
		$('#' + this.id + " .modal:first-child").modal('hide');
		var conf = this.curConfig;
		if(conf.callback){
			conf.callback(conf.selected);
		}
	},
	addSelectedNodes:function(){
		if(!this.zTreeObj){
			return;
		}
		var nodes = this.zTreeObj.getSelectedNodes();
		$.each(nodes,function(i,node){
			platform.orgs.add({
				id:node.id,
				name:node.name,
				data:node.data
			});
		});
	}
};

platform.getOrgData = platform.orgs.getOrgData;

platform.selectOrgs = function(config) {
	var orgObj = this.orgs;
	config = config || {};
	if(!config.selected){
		config.selected = [];
	}
	orgObj.curConfig = config;
	orgObj.appendToBody(function() {
		orgObj.showDlg();
	});
};

platform.selectOrg = function(config){
	var orgObj = this.org;
	config = config || {};
	if(!config.selected){
		config.selected = [];
	}
	orgObj.curConfig = config;
	orgObj.appendToBody(function() {
		orgObj.showDlg();
	});
};

//单位单选
platform.org = {
		id : 'dlg_selectOrg',
		appendToBody : function(callback) {// 请求选择器文本内容
			if (this._appendMark) {
				callback();
				return;
			}
			var orgDiv = "<div id='" + this.id + "'/>";
			$('body').append(orgDiv);
			$('#' + this.id).load('widget/org/show.html?selectType=s', function(context, state) {
				if (state == 'success') {
					callback();
				} else {
					showErrorMsg('选择器加载失败。');
				}
			});
			this._appendMark = true;
		},
		showDlg : function() {
			$('#' + this.id + " .modal:first-child").modal({
				keyboard : true
			});
			this.initTree();
		},
		initTree : function() {
			if(this._initTree_mark){
				return;
			}
			var setting = {
				view : {
					selectedMulti: false,
					showIcon : true,
					showLine : true,
					dblClickExpand : true
				},
				async : {
					enable : true,
					contentType : "application/json",
					url : "widget/org/getOrgs.aj",
					type : 'get',
					autoParam : [ "id" ],
					otherParam:{
						partnerId:this.curConfig.partnerId || null,
                        type:this.curConfig.type || null
					},
					dataFilter : dataFilter
				},
				callback : {
					onDblClick : onDblClick
				}
			};
			this.zTreeObj = $.fn.zTree.init($("#tr_treeOrg"), setting, null);
			this._initTree_mark = true;

			function onDblClick(event, treeId, treeNode) {
			}

			function dataFilter(treeId, parentNode, responseData) {
				var treeData = [];
				$.each(responseData, function(i, org) {
					treeData.push({
						id : org.id,
						name : org.name,
						isParent : org.hasChild,
						pid : org.pid,
						data:org
					});
				});
				return treeData;
			}
		},
		sure:function(node){
			if(!this.zTreeObj){
				$.alert('请选择一个单位','提示');
				return;
			}
			var rs = null;
			if(node){
				rs = {
					id:node.id,
					name:node.name,
					data:node.data
				};
			}else{
				var nodes = this.zTreeObj.getSelectedNodes();
				if(nodes.length >0){
					var selNode = nodes[0];
					rs = {
						id:selNode.id,
						name:selNode.name,
						data:selNode.data
					};
				}
			}
			if(!rs){
				$.alert('请选择一个单位','提示');
				return;
			}
			$('#' + this.id + " .modal:first-child").modal('hide');
			var conf = this.curConfig;
			if(conf.callback){
				conf.callback(rs);
			}
		}
};

/**
 * 下拉选择单位
 */
platform.dropdownOrg = {
		className:'dropdown-org-list',
		dom:"<div class='dropdown-org-list' tabindex='-1'></div>"
};

platform.selectDropdownOrg = function(opt){
	var dom = opt.dom,callback = opt.callback;
	var orgObj = platform.dropdownOrg;
	orgObj.callback = callback || function(){};
	var cur = $(dom);
	if(cur.is('disabled')){
		return;
	}
	var created = cur.next().hasClass(orgObj.className);
	
	if(created && opt.forceFlush){//强制刷新下拉数据列表
		cur.next().remove();
		created = false;
	}
	
	if(!created){
		var width = cur.closest('.input-group').width();
		cur.after($(platform.dropdownOrg.dom).css('min-width',width));
		platform.getOrgData(function(orgData){
			var data = TR.turnToTreeViewData(orgData, {
				namePro:"name",
				allowBlank:opt.allowBlank,
				blankText:opt.blankText
			});
			cur.next().treeview({
				data: data,
				levels:opt.levels,
				onNodeSelected:function(event,node){
					var nodeDom = $(this);
					setTimeout(function(){
						platform.dropdownOrg.callback(node.data);
						nodeDom.closest('.'+platform.dropdownOrg.className).hide();
					}, 150);
				}
			});
		},{
			partnerId:opt.partnerId || null,
			type:opt.type || null
		});
		cur.next().show().focus().blur(function(){
			var e = $(this);
			setTimeout(function(){
				if(!e.prev().is(':focus')){
					e.hide();
				}
			}, 50);
		});
	}else{
		if('block' == cur.next().css('display') && !opt.force){
			cur.next().hide();
		}else{
			cur.next().show().focus();
		}
	}
};