<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>
				<h4 class="modal-title">标题</h4>
			</div>
			<div class="modal-body">
                <div class="row" style="min-height: 300px;">
                    <div class="col-lg-12">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                        <table class="table table-hover" style="margin-top: 5px;">
                        	<thead>
                        		<tr>
                        			<th>名称</th>
                        			<th>域名</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        	</tbody>
                        </table>
                        <nav class="text-center"></nav>
                    </div>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
			</div>
		</div>
	</div>
</div>