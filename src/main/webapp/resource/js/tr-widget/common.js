platform.dropdownCommon = {
		className:'dropdown-common-list',
		dom:"<div class='dropdown-common-list' tabindex='-1'></div>",
		getListDom:function(key){
			key = key || 'common';
			var keyData = this[key];
			var opt = keyData.opt || {};
			var ulDom = $("<ul class='list-group'/>").addClass('dropdown-common-ul-'+key).data('key',key);
			var namePro = opt.name || 'name';
			if(opt.allowBlank){
				var blankText = opt.blankText || '';
				var emptyDom = $("<li class='list-group-item' style='color:grey'>&nbsp;"+blankText+"</li>");
				emptyDom.click(function(){
					platform.dropdownCommon.chooseSure(this);
				});
				ulDom.append(emptyDom);
			}
			$.each(keyData.data,function(i,d){
				var liDom = $("<li class='list-group-item'>"+ d[namePro]+ "</li>").data(d);
				liDom.click(function(){
					platform.dropdownCommon.chooseSure(this);
				});
				ulDom.append(liDom);
			});
			return ulDom;
		},
		chooseSure:function(dom){
			var d = $(dom).data();
			var key = $(dom).parent().data('key');
			var keyData = this[key];
			if(d){
				$('.dropdown-common-ul-'+key+' .curSel').removeClass('curSel');
				$(dom).addClass('curSel');
			}
			
			setTimeout(function(){
				$(dom).closest('.'+platform.dropdownCommon.className).hide();
				if(keyData.callback){
					keyData.callback(d);
				}
			}, 100);
		}
};

platform.select = function(data,opt,callback){
	var commonObj = platform.dropdownCommon;
	var key = opt.key || 'common';
	commonObj[key] = {
			data:data,
			opt:opt,
			callback:callback || function(){}
	};
	
	var dom = opt.dom;
	if(!dom){
		throw new error("dom is undefine in select");
	}
	var cur = $(dom);
	if(cur.is('disabled')){
		return;
	}
	var created = cur.next().hasClass(commonObj.className);
	if(created && opt.forceFlush){//强制刷新下拉数据列表
		cur.next().remove();
		created = false;
	}
	
	if(!created){
		var width = cur.closest('.input-group').width();
		cur.after($(platform.dropdownCommon.dom).css('min-width',width));
		
		if(opt.isTree){
			var treeData = TR.turnToTreeViewData(data, {
				id:opt.id,
				pid:opt.pid,
				namePro:opt.name,
				allowBlank:opt.allowBlank,
				blankText:opt.blankText
			});
			cur.next().data('key',key).treeview({
				data: treeData,
				levels:opt.levels,
				onNodeSelected:function(event,node){
					var nodeDom = $(this);
					var pKey = nodeDom.closest('.'+platform.dropdownCommon.className).data('key');
					var keyData = platform.dropdownCommon[pKey];
					setTimeout(function(){
						keyData.callback(node.data);
						nodeDom.closest('.'+platform.dropdownCommon.className).hide();
					}, 150);
				}
			});
		}else{
			var ulDom = commonObj.getListDom(key);
			cur.next().append(ulDom);
		}
		
		cur.next().show().focus().blur(function(){
			var e = $(this);
			setTimeout(function(){
				if(!e.prev().is(':focus')){
					e.hide();
				}
			}, 50);
		});
	}else{
		if('block' == cur.next().css('display') && !opt.force){
			cur.next().hide();
		}else{
			cur.next().show().focus();
		}
	}
};