platform.selectPartner = function(config){
	var obj = this.partner;
	config = config || {};
	obj.curConfig = config;
	obj.appendToBody(function() {
		obj.showDlg();
	});
};

platform.partner = {
		id:'dlg_selectPartner',
		appendToBody : function(callback) {// 请求选择器文本内容
			if (this._append_Mark) {
				callback();
				return;
			}
			var objDiv = "<div id='" + this.id + "'/>";
			$('body').append(objDiv);
			$('#' + this.id).load('widget/partner/show.html', function(context, state) {
				if (state == 'success') {
					callback();
				} else {
					showErrorMsg('选择器加载失败。');
				}
			});
			this._append_Mark = true;
		},
		showDlg : function() {
			var title = '选择企业';
			if(this.curConfig.type == 0){
                title = '选择客户';
			}else if(this.curConfig.type == 1){
                title = '选择合作伙伴';
			}
			$("#"+this.id+" .modal-title").text(title);
            var dlg = $('#' + this.id + " .modal:first-child");
            dlg.modal({
                keyboard : true
            });
            this.initEvent();
            dlg.on('shown.bs.modal', function () {
                platform.partner.search();
            });
		},
		initEvent:function(){
			var cur = this;
			$('#'+cur.id+' .partnerInput').keydown(function(event){
				if(event.keyCode == 13){
					cur.search();
				}
			});
			$('#trWg_btnSearch_partner').click(function(){
				cur.search();
			});
		},
		initTabelEvent:function(){
			var cur = this;
			var trs = $('#'+cur.id+' tbody tr');
			trs.click(function(){
				$('#'+cur.id+' tbody .warning').not(this).removeClass('warning');
				$(this).toggleClass('warning');
			});		
			$('#'+this.id+' tbody tr').dblclick(function(){
				platform.partner.sure(this);
			});
		},
		search:function(start,pageSize){
			var partnerInput = $('#'+this.id+' .partnerInput');
			$('#trWg_table_partner').load("widget/partner/search",{
				key:partnerInput.val(),
				partnerType:platform.partner.curConfig.type,
                tmcId:platform.partner.curConfig.tmcId,
				dissme:platform.partner.curConfig.dissme,
				start:start,
				pageSize:pageSize
			},function(){
				platform.partner.initTabelEvent();
			});
		},
		turnTo:function(start,pageSize){
			this.search(start, pageSize);
		},
		sure:function(tr){
			if(!tr){
				var selected = $('#'+this.id+' tbody .warning');
				if(selected.length == 0){
					$.alert('请选择一条数据','提示');
					return;
				}
				tr = selected[0];
			}
			var trJq = $(tr);
			var data = {
					id:trJq.data('id'),
					name:trJq.data('name'),
					code:trJq.data('code'),
			};
			$('#' + this.id + " .modal:first-child").modal('hide');
			var conf = this.curConfig;
			if(conf.callback){
				conf.callback(data);
			}
		}
};