platform.selectCorp = function(config){
	var obj = this.corp;
	config = config || {};
	config.type = config.type;
	obj.curConfig = config;
	obj.appendToBody(function() {
		obj.showDlg();
	});
};

platform.corp = {
		id:'dlg_selectCorp',
		appendToBody : function(callback) {// 请求选择器文本内容
			if (this._append_Mark) {
				callback();
				return;
			}
			var objDiv = "<div id='" + this.id + "'/>";
			$('body').append(objDiv);
			var corpType = this.curConfig.type;
			$('#' + this.id).load('widget/corp/show.html', function(context, state) {
				if (state == 'success') {
					callback();
				} else {
					showErrorMsg('选择器加载失败。');
				}
			});
			this._append_Mark = true;
		},
		showDlg : function() {
			$("#"+this.id+" .modal-title").text('选择付款单位!');
			var dlg = $('#' + this.id + " .modal:first-child");
			dlg.modal({
				keyboard : true
			});
			this.initEvent();
			dlg.on('shown.bs.modal', function () {
				platform.corp.search();
			});
		},
		initEvent:function(){
			var cur = this;
			$('#'+cur.id+' .corpInput').keydown(function(event){
				if(event.keyCode == 13){
					cur.search();
				}
			});
			$('#trWg_btnSearch_corp').click(function(){
				cur.search();
			});
		},
		initTabelEvent:function(){
			var cur = this;
			var trs = $('#'+cur.id+' tbody tr');
			trs.click(function(){
				$('#'+cur.id+' tbody .warning').not(this).removeClass('warning');
				$(this).toggleClass('warning');
			});		
			$('#'+this.id+' tbody tr').dblclick(function(){
				platform.corp.sure(this);
			});
		},
		search:function(start,pageSize){
			var corpInput = $('#'+this.id+' .corpInput');
			$('#trWg_table_corp').load("widget/corp/search",{
				key:corpInput.val(),
				partnerId:TR.partnerId,
				start:start,
				pageSize:pageSize
			},function(){
				platform.corp.initTabelEvent();
			});
		},
		turnTo:function(start,pageSize){
			this.search(start, pageSize);
		},
		sure:function(tr){
			if(!tr){
				var selected = $('#'+this.id+' tbody .warning');
				if(selected.length == 0){
					$.alert('请选择一条数据','提示');
					return;
				}
				tr = selected[0];
			}
			var trJq = $(tr);
			var data = {
					id:trJq.data('id'),
					name:trJq.data('name')
			};
			$('#' + this.id + " .modal:first-child").modal('hide');
			var conf = this.curConfig;
			if(conf.callback){
				conf.callback(data);
			}
		}
};