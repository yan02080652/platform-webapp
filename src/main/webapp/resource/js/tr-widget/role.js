/**
 * 组织多选选择器 参数说明： selected:[{id:1,name:'A单位'},{id:2,name:'B单位'}] 已选列表
 */
platform.roles = {
	id:'dlg_selectRoles',
	appendToBody : function(callback) {// 请求选择器文本内容
		if (this._append_Mark) {
			callback();
			return;
		}
		var roleDiv = "<div id='" + this.id + "'/>";
		$('body').append(roleDiv);
		$('#' + this.id).load('widget/role/show.html?selectType=m&type='+(this.curConfig.type || ''), function(context, state) {
			if (state == 'success') {
				callback();
			} else {
				showErrorMsg('选择器加载失败。');
			}
		});
		this._append_Mark = true;
	},
	showDlg : function() {
		$('#' + this.id + " .modal:first-child").modal({
			keyboard : true
		});
		this.initSelected();
		this.initTree();
	},
	initTree : function() {
		var e = this;
		if(e._initTree_mark){
			return;
		}
		var setting = {
			view : {
				selectedMulti: true,
				showIcon : true,
				showLine : true,
				dblClickExpand : false
			},
			callback : {
				onDblClick : onDblClick
			}
		};
		
//		$.ajax({
//			type : 'GET',
//			url : "widget/role/getRoles.aj",
//			success : function(data) {
//				e.data = data || [];
//				
//			}
//		});
		
		this.getRoleData(this.curConfig,function(data){
			e.zTreeObj = $.fn.zTree.init($("#tr_treeRoles"), setting,e.getNodeData());
			e._initTree_mark = true;
		});

		function onDblClick(event, treeId, treeNode) {
			if(treeNode && treeNode.id){
				platform.roles.add({
					id:treeNode.id,
					name:treeNode.name,
					data:treeNode.data
				});
			}
		}
	},
	initSelected : function() {// 初始化已选列表
		var selDom = $('#' + this.id + ' .selected');
		selDom.empty();
		if (this.curConfig.selected) {
			$.each(this.curConfig.selected,function(i, sel) {
				platform.roles.addDom(sel);
			});
		}
	},
	add : function(role){
		var i = this.indexOf(role.id);
		if(i != -1){//已经存在，不可添加
			var dom = $('#' + this.id + ' .selected li:eq('+i+')').css('background-color','antiquewhite');
			setTimeout(function(){
				dom.css('background-color','transparent');
			}, 700);
			return;
		}
		this.curConfig.selected.push(role);
		this.addDom(role);
	},
	addDom : function(role){
		var selDom = $('#' + this.id + ' .selected');
		var newDom = $("<li class='list-group-item'>"+ role.name+ "<span class='glyphicon glyphicon-remove' onclick='platform.roles.remove(this,"+role.id+")'></span></li>");
		selDom.append(newDom);
	},
	remove : function(dom,id) {
		$(dom).parent().remove();
		var index = this.indexOf(id);
		var selected = this.curConfig.selected;
		selected = selected.del(index);
	},
	indexOf:function(id){
		var tempIndex = -1;
		if(this.curConfig.selected){
			$.each(this.curConfig.selected,function(i,sel){
				if(sel.id == id){
					tempIndex = i;
					return false;
				}
			});
		}
		return tempIndex;
	},
	sure:function(){
		$('#' + this.id + " .modal:first-child").modal('hide');
		var conf = this.curConfig;
		if(conf.callback){
			conf.callback(conf.selected);
		}
	},
	addSelectedNodes:function(){
		if(!this.zTreeObj){
			return;
		}
		var nodes = this.zTreeObj.getSelectedNodes();
		$.each(nodes,function(i,node){
			platform.roles.add({
				id:node.id,
				name:node.name,
				data:node.data
			});
		});
	},
	getNodeData:function(){
		return TR.turnToZtreeData(this.roleData);
	},
	getRoleData:function(config,callback){
		var cur = this;
		if(cur.roleData){
			callback(cur.roleData);
		}
		$.getJSON('widget/role/getRoles.aj',{
			partnerId:config.partnerId,
			sysId:config.sysId,
			type:config.type
		},function(data){
			cur.roleData = data;
			callback(cur.roleData);
		});
	}
};

platform.selectRoles = function(config) {
	var roleObj = this.roles;
	config = config || {};
	if(!config.selected){
		config.selected = [];
	}
	roleObj.curConfig = config;
	roleObj.appendToBody(function() {
		roleObj.showDlg();
	});
};

platform.selectRole = function(config){
	var roleObj = this.role;
	config = config || {};
	if(!config.selected){
		config.selected = [];
	}
	roleObj.curConfig = config;
	roleObj.appendToBody(function() {
		roleObj.showDlg();
	});
};

//单位单选
platform.role = {
		id : 'dlg_selectRole',
		appendToBody : function(callback) {// 请求选择器文本内容
			if (this._appendMark) {
				callback();
				return;
			}
			var roleDiv = "<div id='" + this.id + "'/>";
			$('body').append(roleDiv);
			$('#' + this.id).load('widget/role/show.html?selectType=s&type='+(this.curConfig.type || ''), function(context, state) {
				if (state == 'success') {
					callback();
				} else {
					showErrorMsg('选择器加载失败。');
				}
			});
			this._appendMark = true;
		},
		showDlg : function() {
			$('#' + this.id + " .modal:first-child").modal({
				keyboard : true
			});
			this.initTree();
		},
		initTree : function() {
			var e = this;
			if(e._initTree_mark){
				return;
			}
			var setting = {
				view : {
					selectedMulti: false,
					showIcon : true,
					showLine : true,
					dblClickExpand : true
				},
				callback : {
					onDblClick : onDblClick
				}
			};
			var config = e.curConfig;
			$.ajax({
				type : 'GET',
				data:{
					sysId:config.sysId,
					partnerId:config.partnerId,
					type:config.type
				},
				url : "widget/role/getRoles.aj",
				success : function(data) {
					e.data = data || [];
					e.zTreeObj = $.fn.zTree.init($("#tr_treeRole"), setting,e.getNodeData());
					e._initTree_mark = true;
				}
			});

			function onDblClick(event, treeId, treeNode) {
			}
		},
		sure:function(node){
			if(!this.zTreeObj){
				$.alert('请选择一个角色','提示');
				return;
			}
			var rs = null;
			if(node){
				rs = {
					id:node.id,
					name:node.name,
					data:node.data
				};
			}else{
				var nodes = this.zTreeObj.getSelectedNodes();
				if(nodes.length >0){
					rs = nodes[0].data;
					var selNode = nodes[0];
					rs = {
						id:selNode.id,
						name:selNode.name,
						data:selNode.data
					};
				}
			}
			if(!rs){
				$.alert('请选择一个角色','提示');
				return;
			}
			$('#' + this.id + " .modal:first-child").modal('hide');
			var conf = this.curConfig;
			if(conf.callback){
				conf.callback(rs);
			}
		},
		getNodeData:function(){
			return TR.turnToZtreeData(this.data);
		}
};

platform.getRoleData = platform.roles.getRoleData;

/**
 * 下拉选择单位
 */
platform.dropdownRole = {
		className:'dropdown-role-list',
		dom:"<div class='dropdown-role-list' tabindex='-1'></div>"
};

platform.selectDropdownRole = function(opt){
	var dom = opt.dom,callback = opt.callback;
	var roleObj = platform.dropdownRole;
	roleObj.callback = callback || function(){};
	var cur = $(dom);
	if(cur.is('disabled')){
		return;
	}
	var created = cur.next().hasClass(roleObj.className);
	if(!created){
		var width = cur.closest('.input-group').width();
		cur.after($(platform.dropdownRole.dom).css('min-width',width));
		platform.getRoleData(opt,function(roleData){
			var data = TR.turnToTreeViewData(roleData, {
				namePro:"name",
				allowBlank:opt.allowBlank,
				blankText:opt.blankText
			});
			cur.next().treeview({
				data: data,
				levels:opt.levels,
				onNodeSelected:function(event,node){
					var nodeDom = $(this);
					setTimeout(function(){
						platform.dropdownRole.callback(node.data);
						nodeDom.closest('.'+platform.dropdownRole.className).hide();
					}, 150);
				}
			});
		});
		cur.next().show().focus().blur(function(){
			var e = $(this);
			setTimeout(function(){
				if(!e.prev().is(':focus')){
					e.hide();
				}
			}, 50);
		});
	}else{
		if('block' == cur.next().css('display') && !opt.force){
			cur.next().hide();
		}else{
			cur.next().show().focus();
		}
	}
};