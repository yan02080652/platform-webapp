/**
 * 选择器组件预订义
 */
platform._selectData = {
	orgs:{
		key:'selectOrgs',
		require:'org_wg'
	},
	org:{
		key:'selectOrg',
		require:'org_wg'
	},
	dropdownOrg:{
		key:'selectDropdownOrg',
		require:['treeview','org_wg']
	},
	roles:{
		key:'selectRoles',
		require:'role_wg'
	},
	role:{
		key:'selectRole',
		require:'role_wg'
	},
	dropdownRole:{
		key:'selectDropdownRole',
		require:['treeview','role_wg']
	},
	users:{
		key:'selectUsers',
		require:'user_wg'
	},
	user:{
		key:'selectUser',
		require:'user_wg'
	},
	dropdownOrgGrade:{
		key:'selectDropdownOrgGrade',
		require:['treeview','orgGrade_wg']
	},
	partner:{
		key:'selectPartner',
		require:'partner_wg'
	},
	dic:{
		key:'selectDic',
		require:'dic_wg'
	},
	corp:{
		key:'selectCorp',
		require: 'corp_wg'
	},
	flightCompany:{//选择航司
		key:'selectFlightCompany',
        require:'flightCompany_wg'
	},
	airPort:{//选择机场
		key:'selectAirPort',
		require:'airPort_wg'
	}
};

TR.select = function(name,option,callback){
	if(!name){
		return;
	}
	
	if(name instanceof Array){//自定义数据选择
		var requireCommon = ['common_wg'];
		if(option && option.isTree){
			requireCommon.push('treeview');
		}
		require(requireCommon,function(){
			platform.select(name,option,callback);
		});
		return;
	}

	var method = platform._selectData[name];
	if(!method){
		throw new error("【"+name+"】method are not define");
	}
	
	if(!option){
		throw new error("【select】 param must be not null");
	}
	var config = null;
	if(typeof option == 'function'){
		config = {callback:option};
	}else if(callback){
		config = option;
		config.callback = callback;
	}
	
	var required = method.require;
	if(typeof required == 'string'){
		required = [required];
	}
	
	require(required,function(){
		platform[method.key](config);
	});
};

/**
 * 通用对话框选择
 * @param config
 */
TR.select2 = function (config) {
	var sel = TR.select2;
	var defaultConfig = $.extend({
		id:'dlg_demo',
		name:'标题',
        width:600,
		condition:[{code:'code',placeholder:'编码/名称'}],//查询条件
        columns:[],//{property:'name',caption:'名称',lenth:300}
		service:'',
		parameter:{},
		callback:function () {},
        forceSeach:false//是否始终每次强制搜索
	},config);

	if(defaultConfig.columns.length < 1){
        console.error('缺少列定义');
        return;
	}

    var dlgId = defaultConfig.id;
	//1.初始化过就直接show
    if(sel.isInited(dlgId)){
    	sel.show(dlgId);
    	if(defaultConfig.hasPage && defaultConfig.forceSeach) {
    		var toPage = $("#"+dlgId+" .pagination .active a").data('toPage');
    		if(toPage != null) {
                turnToPage(toPage);
            }else{
                $("#"+dlgId+" .input-group-btn").triggerHandler('click');
			}
        }else if(defaultConfig.forceSeach){
            $("#"+dlgId+" .input-group-btn").triggerHandler('click');
		}
    	return;
	}
    //2.加载对话框内容
	var txt = sel.getDialogText();
	var dlg = $("<div id='"+dlgId+"'/>").append(txt);
	//3.根据配置初始化对话框内容
	$('.modal-dialog',dlg).css('width',defaultConfig.width);
    $('.modal-title',dlg).html(defaultConfig.name);

    //查询条件配置
	var searchBtn = $('.input-group-btn',dlg);
	if(defaultConfig.condition.length > 1){
        searchBtn.css('display', 'block');
	}
    $.each(defaultConfig.condition,function (i,con) {
    	if(con.code){
            var input = $("<input class='form-control'/>").attr('name',con.code);
            if(con.width){
                input.css('width', con.width);
			}
			if(con.placeholder){
                input.attr('placeholder', con.placeholder);
			}
            input.insertBefore(searchBtn);

			if(con.enterSearch) {
                input.keyup(function (event) {
					if(event.key == 'Enter') {
                        searchBtn.triggerHandler('click');
                    }
                });
            }
		}else{
    		console.error('查询参数未指定编码：code');
		}
    });

	//表格配置
	var table = $('table',dlg);
	var theadTr = $('thead tr',table).empty();
	$.each(defaultConfig.columns,function (i, column) {
		if(column.property && column.caption) {
            var th = $("<th/>").data('property', column.property).text(column.caption);
            if(column.width != null && column.width != undefined){
                th.css('width',column.width);
			}
            theadTr.append(th);
        }
    });

	//end:显示对话框
    $('body').append(dlg);
    sel.show(dlgId);

    //查询
    searchBtn.click(function () {
		search(function (data) {
			createTable(data);
        });
    });

    //搜索
    searchBtn.triggerHandler('click');
    function search(callback,pageIndex){
    	var parameter = $.extend({},defaultConfig.parameter);
    	if(defaultConfig.hasPage){
            parameter.pageIndex = pageIndex || 1;
		}
    	$.each(defaultConfig.condition,function (i, con) {
            var conValue = $("#" + dlgId + " [name=" + con.code + "]").val();
            if(conValue){
                parameter[con.code] = conValue;
			}
        });
        var loadingIndex = layer.load();
        $.ajax({
            type: 'POST',
            url: defaultConfig.service,
            data: parameter,
            dataType: 'JSON',
            success: function (data) {
            	if(defaultConfig.hasPage){
                    callback(data.pagelist);
				}else{
                    callback(data);
				}
                layer.close(loadingIndex);
            },
            error:function () {
                layer.close(loadingIndex);
                layer.msg('查询失败');
            }
        });
	}

    //跳转到某页
    function turnToPage(pageIndex) {
        search(function (data) {
            createTable(data);
        },pageIndex);
    }

	//确认
    $('.btn-primary', dlg).click(function () {
        var curTr = $('.info',dlg);
        if(curTr.length < 1){
            layer.msg('请选择一条数据');
        }else{
            var data = curTr.data('userData');
            defaultConfig.callback(data);
            $(".modal:first-child",dlg).modal('hide')
        }
    });

	//创建表格
	function createTable(pagelist) {
        pagelist = pagelist || {};
		var tbody = $("#"+dlgId+" tbody").empty();
        if(pagelist.list) {
            $.each(pagelist.list, function (i, d) {
                var tr = $("<tr/>").data('userData',d);
                $.each(defaultConfig.columns,function (i, column) {
                    if(column.property) {
                    	var td = $("<td/>");
                    	if(column.render && typeof column.render == 'function') {
                            column.render(td,d,column);
                        }else{
                            td.text(d[column.property] || null);
						}
                        tr.append(td);
                    }
                });
                tr.click(function () {
					$('.info',$(this).parent()).removeClass('info');
                    $(this).addClass('info');
                });
                tbody.append(tr);
            });
        }


        //构建分页条
        var nav = $("#"+dlgId+" nav").empty();
        if(!defaultConfig.hasPage) {
			return;
        }
        if(pagelist.total == 0) {
            nav.html("<span style='color: #bbb;'> 没有相关的数据</span>");
        }else{
            var ul = $("<ul class='pagination'/>");
            var preLi = $("<li/>");
            var preA = $("<a href='javascript:void(0);' aria-label='Previous'><span aria-hidden='true'>«</span></a>");
            if(pagelist.currentPage == 1){
                preLi.addClass('disabled');
			}else{
                var toPrePage = pagelist.currentPage -2;
                preA.click(function () {
                    turnToPage(toPrePage);
                });
			}
			ul.append(preLi.append(preA));

            for (var i = 1;i<=pagelist.totalPage;i++){
            	var liNum = $("<li/>");
            	var liA = $("<a href='javascript:void(0);'/>").data('toPage',i).text(i);
            	if(pagelist.currentPage == i){
                    liNum.addClass('active');
				}else{
                    liA.click(function () {
                        turnToPage($(this).data('toPage'));
                    });
				}
                ul.append(liNum.append(liA));
			}

            var nextLi = $("<li/>");
            var nextA = $("<a href='javascript:void(0);' aria-label='Next'><span aria-hidden='true'>»</span></a>");
            if(pagelist.currentPage == pagelist.totalPage){
                nextLi.addClass('disabled');
            }else{
                var toNextPage = pagelist.currentPage;
                nextA.click(function () {
                    turnToPage(toNextPage);
                });
            }
            ul.append(nextLi.append(nextA));
            nav.append(ul);
		}
    }

};

$.extend(TR.select2,{
	isInited:function (dlgId) {
		return $('#'+dlgId).length > 0;
    },
    getDialogText:function () {
		var cur = this;
    	if(cur._txtloaded){
    		return cur._txt;
		}
        $.ajax({
            type:'GET',
            url:'resource/js/tr-widget/baseDialog.txt',
            async:false,
            dataType:'text',
            success:function (data) {
            	cur._txtloaded = true;
            	cur._txt = data;
            }
        });
        return cur._txt;
    },
	show:function (dlgId) {
        $('#' + dlgId + " .modal:first-child").modal({
            keyboard : true
        });
    }
});