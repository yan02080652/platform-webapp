
var code=null;
var corpId=null;
var orgId=null;
(function(){

    //费用归属下拉
    var $item_pull = $('.item_pull');  //最外层div
    var $ul_b = $('.ul_b');
    $item_pull.click(function () {
        if ($ul_b.is(':hidden')) {
        	$ul_b.show()
        } else {
        	$ul_b.hide()
        }
    });
    //费用归属
    $('.item_ul li').click(function () {
    	$('.item_div').val($(this).html());
    	$(".item_pull").css("border","");
    	
    	$("#orgIdAndcostIdAndCorpId").val($(this).attr("data-value"));

         
    });
    $('.ul_b').mouseleave(function () {
        $('.ul_b').hide();
    });
    
})();

var costName;
$('.otherExpense').on('click', function () {
	//加载成本中心树
    layer.open({
        title: '选择成本单元',
        type: 1,
        area: ['350px', '350px'],
        skin: 'layui-layer-rim',
        content: $('.otherExpenseH'),
        btn: ['确定', '取消'],
        yes: function (index) {
        	 if ($("cite").hasClass("activeTree")) {
        		 $(".item_pull").css("border","");
        	 }
            layer.close(index);
            $(".item_div").val(costName);
        }
    })
});

function loadCostTree(partnerId){
	$.ajax({
		type : 'GET',
		url : "widget/corp/expenseTree.json",
		data :{partnerId:partnerId},
		async: false,
		success : function(data1) {
			//批量移动的树
		    layui.tree({
		    	skin : 'as' ,
				drag : true ,
		        elem: '#expenseDetail' //批量移动的树
		        , nodes: TR.turnToLayerTreeViewDataIsCost(data1),
		        click : function (data){
		        	var da=data.data;
		        	if(data.href=='1'){

		        		 costName = da.name;

		        		 $(".item_div").val(costName);
		        		 
		        		  code=da.costCenterId;
		        	      corpId=da.corpId;
		        	      orgId=da.orgIdForCostCenter;
		        	      
		        	      $("#orgIdAndcostIdAndCorpId").val(orgId + "," + code + "," + corpId);
		           }
		    	}
		    });
		    initCostCenter();
		}
	});
}
function initCostCenter(){

	//选中效果以及小圆点
    var newObj = '<div class="costUnit"></div>';
    //防止反复追加
    $('cite').addClass('citeOpen');

    $('#expenseDetail').hover(function () {
        $('#expenseDetail a').each(function () {
        	var cite=$(this).find("cite");
            if ($(this).attr("href") == '1' && cite.hasClass('citeOpen')) {
            	cite.append(newObj);
            	cite.removeClass('citeOpen');
            	cite.addClass('activePoint');
            }
        })
    });
    
    $('a').click(function(event){
    	if($(this).attr("href")=='1'){
    		$("cite").removeClass('activeTree');
    		$(this).find("cite").addClass('activeTree');
            event.preventDefault();
        }
    });

    $('#expenseDetail i').click(function() {
	    $(this).next().removeClass('activeTree');
	});
}

