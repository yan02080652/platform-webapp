/**
 * 组织层面
 */
platform.orgGrade = {
	getOrgGradeData:function(callback){
		var cur = this;
		if(cur.orgGradeData){
			callback(cur.orgGradeData);
		}
        var config =  cur.curConfig || {};
		$.getJSON('widget/orgGrade/show?type='+(config.type || ''),function(data){
			cur.orgGradeData = data;
			callback(cur.orgGradeData);
		});
	}
};
 
platform.getOrgGradeData = platform.orgGrade.getOrgGradeData;

/**
 * 下拉选择单位
 */
platform.dropdownOrgGrade = {
		className:'dropdown-role-list',
		dom:"<div class='dropdown-role-list' tabindex='-1'></div>"
};

platform.selectDropdownOrgGrade = function(opt){
    platform.orgGrade.curConfig = opt;
	var dom = opt.dom,callback = opt.callback;
	var orgGradeObj = platform.dropdownOrgGrade;
	orgGradeObj.callback = callback || function(){};
	var cur = $(dom);
	if(cur.is('disabled')){
		return;
	}
	var created = cur.next().hasClass(orgGradeObj.className);
	if(!created){ 
		var width = cur.closest('.input-group').width();
		cur.after($(platform.dropdownOrgGrade.dom).css('min-width',width));
		platform.getOrgGradeData(function(orgGradeData){
			var data = TR.turnToTreeViewData(orgGradeData, {
				id:"code",
				pid:"pcode",
				namePro:"name", 
				allowBlank:opt.allowBlank,
				blankText:opt.blankText
			});
			cur.next().treeview({
				data: data,
				onNodeSelected:function(event,node){
					var nodeDom = $(this);
					setTimeout(function(){
						platform.dropdownOrgGrade.callback(node.data);
						nodeDom.closest('.'+platform.dropdownOrgGrade.className).hide();
					}, 150);
				}
			});
		});
		cur.next().show().focus().blur(function(){
			var e = $(this);
			setTimeout(function(){
				if(!e.prev().is(':focus')){
					e.hide();
				}
			}, 50);
		});
	}else{
		if('block' == cur.next().css('display') && !opt.force){
			cur.next().hide();
		}else{
			cur.next().show().focus();
		}
	}
};