var TR = {};//对外对象
var platform = {};//对内对象

Array.prototype.del = function(dx) {
	if (dx < 0 || isNaN(dx) || dx > this.length) {
		return false;
	}
	this.splice(dx, 1);
};

/**
 * 转换平面结构数据至bootstrap-treeview树形数据结构格式
 */
TR.turnToTreeViewData = function(data, config){
	if (!data) {
		return [];
	}
	config = config || {};
	
	id = config.id || 'id';
	pid = config.pid || 'pid';
	text = config.namePro || 'name';
	var nodeDataTop = [];
	if(config.allowBlank){
		nodeDataTop.push({
			color:'grey',
			icon:config.blankIcon,
			text:config.blankText
		});
	}
	var childrenData = {};
	
	var ids = {};
	$.each(data,function(i,d){
		ids[d[id]] = true;
	});
	
	$.each(data, function(i, d) {
		var node = {
			id : d[id],
			pid : d[pid],
			text : d[text],
			data : d
		};
		if (!node.pid || !ids[node.pid]) {
			nodeDataTop.push(node);
		} else {
			if (!childrenData[node.pid]) {
				childrenData[node.pid] = [];
			}
			childrenData[node.pid].push(node);
		}
	});

	$.each(nodeDataTop, function(i, n) {
		addChild(n);
	});

	function addChild(n) {
		var children = childrenData[n.id];
		if (children) {
			$.each(children, function(i, c) {
				addChild(c);
			});
			n.nodes = children;
		}
	}
	;
	return nodeDataTop;
};

/**
 * 转换平面结构数据至ztree树形数据结构格式
 */
TR.turnToZtreeData = function(data, config) {
	if (!data) {
		return [];
	}
	config = config || {};
	id = config.id || 'id';
	pid = config.pid || 'pid';
	name = config.namePro || 'name';
	var nodeDataTop = [];
	if(config.allowBlank){
		nodeDataTop.push({
			color:'grey',
			name:config.blankText
		});
	}
	var childrenData = {};
	$.each(data, function(i, d) {
		var node = {
			id : d[id],
			pid : d[pid],
			name : d[name],
			data : d
		};
		if (!node.pid) {
			nodeDataTop.push(node);
		} else {
			if (!childrenData[node.pid]) {
				childrenData[node.pid] = [];
			}
			childrenData[node.pid].push(node);
		}
	});

	$.each(nodeDataTop, function(i, n) {
		addChild(n);
	});

	function addChild(n) {
		var children = childrenData[n.id];
		if (children) {
			$.each(children, function(i, c) {
				addChild(c);
			});
			n.children = children;
		}
	}
	return nodeDataTop;
};

/**
 * bootstrap-treeview 传入树的所有数据 以及当前要选中的id 得到当前的nodeId
 */
TR.getCurNodeId = function(data, id) {
	var rsData = {};
	var curNodeId = -1;
	var haschecked = false;
	if (!data) {
		return null;
	}
	if (!id) {
		return null;
	}
	checkChild(data);
	rsData.nodeId = curNodeId;
	
	function checkChild(data) {
		$.each(data, function(i, d) {
			if(!haschecked)
				curNodeId++ ;
			if(d.nodes){//存在子节点
				if(id == d.id){
					rsData = d;
					haschecked = true;
				}else{
					checkChild(d.nodes);
				}
			}else{
				if(id == d.id){
					rsData = d;
					haschecked = true;
				}
			}
			
		});
	}
	return rsData;
};


TR.turnToLayerTreeViewDataIsCost = function(data, config){
	if (!data) {
		return [];
	}
	config = config || {};
	
	id = config.id || 'id';
	pid = config.pid || 'pid';
	name = config.name || 'name';
	isCost = config.isCost || 'isCost';
	corpId =config.corpId || 'corpId';
	orgId = config.orgId || 'orgId';
	spread = 'true';
	var nodeDataTop = [];
	var childrenData = {};
	$.each(data, function(i, d) {
		var node = {
			id : d[id],
			pid : d[pid],
			name : d[name],
			href :d[isCost],
			spread : 'true ',
			data : d
		};
		if (!node.pid) {
			nodeDataTop.push(node);
		} else {
			if (!childrenData[node.pid]) {
				childrenData[node.pid] = [];
			}
			childrenData[node.pid].push(node);
		}
	});

	$.each(nodeDataTop, function(i, n) {
		addChild(n);
	});

	function addChild(n) {
		var children = childrenData[n.id];
		if (children) {
			$.each(children, function(i, c) {
				addChild(c);
			});
			n.children = children;
		}
	};
	return nodeDataTop;
};


TR.partnerId =null;