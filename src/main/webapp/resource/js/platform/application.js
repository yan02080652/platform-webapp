/*$(document).ready(function() {
	
	setInterval(function(){$("#alertMsg").alert('close')},3000);
	
	*//**
	 * 绑定页面链接的pjax, 对a标签进行事件绑定，如果该节点有data-pjax属性, 则点击时发送pjax请求， 同时显示正在加载图标
	 *//*
	$(document).pjax('a[data-pjax]', '#page-wrapper', {
		timeout : 5000
	});

	*//**
	 * 当pjax请求页面时触发，显示正在载入
	 *//*
	$(document).on('pjax:send', function() {
		$("#page-wrapper").css("opacity", "0.5");
	});

	*//**
	 * 当pjax请求页面时，载入完成后触发，取消正在加载的旋转图标
	 *//*
	$(document).on('pjax:success', function() {
		updateNavStatus();
		$("#page-wrapper").css("opacity", "1");
	});

	*//**
	 * 处理页面加载失败
	 *//*
	$(document).on('pjax:error', function() {
		$('#loading').hide();
		showErrorMsg("请求失败，请刷新重试");
	});

	*//**
	 * 处理页面加载超时
	 *//*
	$(document).on('pjax:timeout', function() {
		$('#loading').hide();
		showErrorMsg("请求超时，请刷新重试");
	});

	$(document).trigger('pjax:success');//触发pjax:success事件，保证监听唯一的事件，无需重复监听
});
*/

/**
* 更新左侧导航条的选中状态
*/
function updateNavStatus() {
	// 选中左侧父级导航
	var baseurl = getBaseUrl();
	var path = window.location.href;
	
	if (path.endWith("#") || path.endWith("/")) {
		path = path.substring(0, path.length - 1);
	}
	if (path.indexOf("?") != -1) {
		path = path.substring(0, path.indexOf("?"));
	}
	

	var url = path.replace(baseurl, "");
	
	if(url.split('/').length == 1)
		return;
	
	while (url.split('/').length - 1 > 1) {
		url = url.substring(0, url.lastIndexOf('/'));
	}

	$("#side-menu ul li a").each(function(e){
		$(this).removeClass("in");
	});
	
	var item = $("#side-menu ul li").find("a[href*='" + url + "']");
	
	if (item.length > 0) {
		item.parents("ul").addClass("in");
		item.addClass("active");
		return;
	}
}


/**
 * 在modal底部显示错误信息
 * @param dom {jQueryDom}
 * @param message
 */
function showErrorModalFooter(dom, message) {
	dom.find(".modal-footer>p.bg-danger>strong").html(message);
	dom.find(".modal-footer>p.bg-danger").removeClass("hidden");
}

/**
 * 在modal底部显示错误信息,dom为jQueryDom
 * @param dom {jQueryDom}
 */
function hideErrorModalFooter(dom) {
	dom.find(".modal-footer>p.bg-danger").addClass("hidden");
}

/**
 * 在页面显示错误消息
 * @param message {String}
 */
function showErrorMsg(message) {
	var msg = '<span class="icon icon-error">'+message+'</span>';
	$.scojs_message(msg, $.scojs_message.TYPE_ERROR);
}

/**
 * 在页面显示成功消息
 * @param message {String}
 */
function showSuccessMsg(message) {
	var msg = '<span class="icon icon-success">'+message+'</span>';
	$.scojs_message(msg, $.scojs_message.TYPE_OK);
}

/**
 * 在form验证时正确时去掉提示信息
 * @param label
 * @param element {HtmlDocument}
 */
function validateSuccess(label, element) {
	var dom = $(element);
	var popover = dom.nextAll("span.form-control-feedback");
	popover.popover("hide");
}

/**
 * 在form验证时显示错误信息的样式和位置
 * @param error {HTMLDocument}
 * @param element {HTMLDocument} 输入dom节点 input textarea等
 */
function setErrorPlacement(error, element) {
	setPopoverPosition(element, $(error).text());
}

/**
 * 在form验证时错误时提示信息
 * @param element {HTMLDocument} 输入dom节点 input textarea等
 */
function setHighlight(element) {
	setPopoverPosition(element);
}

/**
 * 设置验证失败时错误信息的显示位置
 * @param element {HTMLDocument} 输入dom节点 input textarea等
 * @param content {String}
 */
function setPopoverPosition(element, content) {
	var dom = $(element);

	var popover = dom.nextAll("span.form-control-feedback");
	var addonDom = dom.nextAll("span.input-group-addon");
	var modal = dom.parents("div.modal");

	if (content != null && popover.length > 0 && popover.attr("data-content") == content
			&& $("#" + popover.attr("aria-describedby")).css("display") == "block") {
		return;
	}

	if (content == null && popover.length > 0 && $("#" + popover.attr("aria-describedby")).css("display") == "block") {
		return;
	}

	if (content && content.notEmpty()) {
		popover.attr("data-content", content);
	}

	if (addonDom.length == 0) {
		addonDom = dom;
	}

	if (popover.length < 1) {
		var container;
		if (modal.length < 1) {
			container = "body";
		} else {
			container = "#" + modal.attr("id");
		}
		addonDom.after('<span class="form-control-feedback hidden" data-container="' + container
				+ '" data-toggle="popover" data-trigger="manual"></span>');
		popover = dom.nextAll("span.form-control-feedback");
	}

	popover.popover("show");

	var popdom = $("#" + popover.attr("aria-describedby"));

	if (modal.length > 0) {
		popdom.css("top", addonDom.offset().top - window.scrollY + modal[0].scrollTop - 3 + "px");
	} else {
		popdom.css("top", addonDom.offset().top - $("body").offset().top - 3 + "px");
	}

	popdom.css("left", addonDom.offset().left + addonDom.outerWidth() + "px");
}

/**
 * 异步重新载入本页面
 */
function reloadPage() {
	$.pjax.reload('#page-wrapper');
}

/**
 * 异步载入页面
 * @param url 载入的地址
 * @param containerSelector 返回的htm放置的父节点
 */
function loadPage(url, containerSelector, push) {
	$(".modal-backdrop.fade.in").remove();
	$.pjax({
		url : url,
		container : containerSelector,
		push : push != null ? push : true
	});
}

function closeWindow() {
	window.open('', '_parent', '');
	window.close();
}


/**
 * 返回项目的basePath
 * @returns {String}
 */
function getBaseUrl() {
	var baseUrl = $("base").attr("href");
	if(baseUrl.indexOf(":80/") > -1)
		baseUrl = baseUrl.replace(":80/","/");
	return baseUrl;
}


/*
 * jQuery placeholder, fix for IE6,7,8,9
 * @author JENA
 * @since 20131115.1504
 * @website ishere.cn
 */
var JPlaceHolder = {
    //检测
    _check : function(){
        return 'placeholder' in document.createElement('input');
    },
    //初始化
    init : function(){
        if(!this._check()){
            this.fix();
        }
    },
    //修复
    fix : function(){
        jQuery(':input[placeholder]').each(function(index, element) {
            var self = $(this), txt = self.attr('placeholder');
            self.wrap($('<div></div>').css({position:'relative', zoom:'1', border:'none', background:'none', padding:'none', margin:'none'}));
            var pos = self.position(), h = self.outerHeight(true), paddingleft = self.css('padding-left');
            var holder = $('<span></span>').text(txt).css({position:'absolute', left:pos.left, top:pos.top, height:h, lienHeight:h, paddingLeft:paddingleft, color:'#aaa'}).appendTo(self.parent());
            self.focusin(function(e) {
                holder.hide();
            }).focusout(function(e) {
                if(!self.val()){
                    holder.show();
                }
            });
            holder.click(function(e) {
                holder.hide();
                self.focus();
            });
        });
    }
};

function ajaxpag(url,div){
	$.post(url,{},function(data){
		$("#" + div).html();
		$("#" + div).html(data);
	})
}