require.config({
	baseUrl : "resource/js",
	paths : {
        /*jquery:'jquery/jquery-3.1.0.min',*/
		treeview: "bootstrap/bootstrap-treeview",
		ztree: "jquery/jquery.ztree.all-3.5.min",
		common_wg:'tr-widget/common',
		org_wg:"tr-widget/org",
		role_wg:"tr-widget/role",
		user_wg:'tr-widget/user',
		orgGrade_wg:'tr-widget/orgGrade',
		partner_wg:'tr-widget/partner',
		dic_wg:'tr-widget/dic',
		corp_wg:'tr-widget/corp',
		flightCompany_wg:'tr-widget/flightCompany',//选择航司
		airPort_wg:'tr-widget/airPort'//选择机场
	}
});

//防止与其他组件冲突
define.amd = null;