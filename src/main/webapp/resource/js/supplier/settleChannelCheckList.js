
//展开，隐藏
function hideClick(TagName){
	var classElements = document.getElementsByClassName(TagName);
	for(var i = 0; i < classElements.length; i++){
		if(classElements[i].style.display==""){
			classElements[i].style.display = "none";
		}else{
			classElements[i].style.display = "";
		}
	}
	
}

//收支渠道勾选，checkbox选择
function selectAll(id) {
	if ($("#"+id).is(":checked")) {
		$("input[name="+id+"]").prop("checked", true);//所有选择框都选中
	} else {
		$("input[name="+id+"]").prop("checked", false);
	}
	
	changeData();
}

function batchCheck() {
    //判断至少选了一项
    var checkedNum = $("#settleChannelCheckDataTable input[type='checkbox']:checked").length;
    if (checkedNum == 0) {
        return false;
    }

    var items = new Array();
    $("#settleChannelCheckDataTable input[type='checkbox']:checked").each(function () {
    	items.push($(this).val());
    });

    return items;
}

//checkBox勾选事件改变
function changeData(){
	//已选核对集合
	var checkItems = batchCheck().toString();
	if(checkItems=='false'){
		checkItems="";
	}
	$.ajax({
		url:"supplier/channelCheck/toCheckData.json",
		data:{
			checkItems:checkItems
		},
		type:"POST",
		success:function(data){
			$("#settleChannelCheckData").html(data);
		},
		error:function(){
            layer.close(loading);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
}


//统计已选的核对数据
function confirmSettleChannelCheck(){
	//已选核对集合
	var checkItems = batchCheck().toString();
	if(checkItems != 'false'){
		$.ajax({
			url:"supplier/channelCheck/toCheckData.json",
			data:{
				checkItems:checkItems
			},
			type:"POST",
			success:function(data){
				$("#settleChannelCheckData").html(data);
			},
			error:function(){
	            layer.close(loading);
				layer.msg("系统繁忙，请稍后再试");
			}
		});
	}
}


//取消已选的核对数据
/*function cancelChannelCheck(){
	//已选核对集合
	var checkItems = batchCheck().toString();
	if(checkItems != 'false'){
		$.confirm({
			title : '提示',
			confirmButton:'确认',
			cancelButton:'取消',
			content : "请确认是否要取消核对？"	,
			confirm : function() {
				
				$.ajax({
					url:"supplier/channelCheck/cancelCheckData.json",
					data:{
						checkItems:checkItems
					},
					type:"POST",
					success:function(data){
						if (data.code == '1') {
			                $.alert(data.data, "提示");
			                reloadSettleChannelCheck();
			            } else {
			                $.alert(data.data, "提示");
			            }
					},
					error:function(){
						layer.close(loading);
						layer.msg("系统繁忙，请稍后再试");
					}
				});
				
			}
		});
		
	}else{
		$.alert("请选择需要取消的记录!", "提示");
	}
	
}*/



//初始化
$(function(){
	initDate();
	reloadSettleChannelCheck();
});

function resetForm(){
	//收支通道类型
	$("#type").val("0");
	//收支通道
	$("#settleChannel").val("0");
	//审核状态
	$("#status").val("");
	 
}



//分页调用的方法
function reloadSettleChannelCheck(){
	//收集参数
	
	//入账起始日期
	var beginDate = $("#begin").val();
	//入账截止日期
	var endDate = $("#end").val();
	//收支通道
	var settleChannel = $("#settleChannel").val();
	//收支通道类型
	var type = $("#type").val();
	//审核状态
	var status = $("#status").val();
	
    var tmcId ='';
   
	var loading = layer.load();

	//分页数据
	$.ajax({
		url:"supplier/channelCheck/checklList.json",
		data:{
			beginDate:beginDate,
    		endDate:endDate,
    		settleChannel:settleChannel,
    		status:status,
	        type:type,
			tmcId:tmcId,
		},
		type:"POST",
		success:function(data){
			layer.close(loading);
			$("#settleChannelCheckTable").html(data);
			$('.drop_ul').hover(function(){
		        $('.drop_ul').removeClass('drop_ul1');
		        $(this).addClass('drop_ul1');
		    });
			
		},
		error:function(){
            layer.close(loading);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
	
	//加载下拉
	$.ajax({
		url:"supplier/channelCheck/toCheckData.json",
		data:{
			checkItems:""
		},
		type:"POST",
		success:function(data){
			$("#settleChannelCheckData").html(data);
		},
		error:function(){
            layer.close(loading);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
	
  }


//更新时间
function initDate(){
	$("#begin").val(getDay(-7));
	$("#end").val(getDay(-1));
}
function getDay(day){
    var today = new Date();
    var targetday_milliseconds=today.getTime() + 1000*60*60*24*day;
    today.setTime(targetday_milliseconds); //计算日期
    return today.format("yyyy-MM-dd");
}
