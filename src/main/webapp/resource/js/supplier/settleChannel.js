var settleChannel = {
    url: {
        list: function () {
            return 'supplier/channel/list';
        },
        remove: function (id) {
            return 'supplier/channel/delete/' + id;
        },
        save: function () {
            return 'supplier/channel/save';
        },
        detail: function (id) {
            if (id) {
                return "supplier/channel/detail?id=" + id;
            } else {
                return "supplier/channel/detail";
            }
        }
    },

//=====================================列表页===============================================
    list: {
        init: function () {
        	settleChannel.list.reloadList();

        },
        reloadList: function (pageIndex) {
            var channelName = $("#channelName").val();
            $.post(settleChannel.url.list(), {
                pageIndex: pageIndex,
                channelName: channelName,
            }, function (data) {
                $("#listData").html(data);
            });
        },
        reset: function () {
            $("#channelName").val("");
            settleChannel.list.reloadList();
        },
        remove: function (id) {
            $.confirm({
                title: '提示',
                confirmButton: '确认',
                cancelButton: '取消',
                content: "确认停用所选记录?",
                confirm: function () {
                    $.ajax({
                        cache: true,
                        type: "POST",
                        url: settleChannel.url.remove(id),
                        async: false,
                        error: function (request) {
                            $.alert("停用失败,请刷新重试！", "提示");
                        },
                        success: function (result) {
                            if (result.code == '0') {
                            	settleChannel.list.reloadList();
                            } else if(result.code == '1'){
                                $.alert("停用失败,请刷新重试！", "提示");
                            }else if(result.code == '2'){
                            	$.alert(result.data, "提示");
                            }                           	
                        }
                    });
                },
                cancel: function () {
                }
            });
        },
    },
    //=======================================详情页=================================================//

    detail: {
        showModal: function (url, callback) {
            $('#channelDiv').load(url, function (context, state) {
                if ('success' == state && callback) {
                    callback();
                }
            });
        },

        detailPage: function (id) {
        	settleChannel.detail.showModal(settleChannel.url.detail(id), function () {
                $('#settleChannelModel').modal();
                settleChannel.detail.initForm();
            });
        },


        initForm: function () {
            var rules = {
                type: {
                    required: true
                },
                name: {
                    required: true
                },
                descr: {
                    required: true
                },
            };


            $("#settleChannelForm").validate({
                rules: rules,
                errorPlacement: setErrorPlacement,
                success: validateSuccess,
                highlight: setHighlight,
                submitHandler: function (form) {
                	
                	 $.post(settleChannel.url.save(),$("#settleChannelForm").serialize(),
                             function(data){
                                 if (data.code == '0') {
                                	 $('#settleChannelModel').modal('hide');
                                     layer.msg("保存成功");
                                     settleChannel.list.reloadList();
                                 }else{
                                     layer.msg(data.msg);
                                 }
                             }
                         );
                }
            });
        },

        saveForm: function () {
            $("#settleChannelForm").submit();
        }
    }

};

