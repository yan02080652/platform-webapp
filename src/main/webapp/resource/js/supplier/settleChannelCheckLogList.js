//初始化
$(function(){
	reloadSettleChannelCheckLog();
});

//分页调用的方法
function reloadSettleChannelCheckLog(pageIndex){
	//收集参数
	var settleChannel = $("input[name='settleChannel']").val();
	var beginDate = $("input[name='beginDate']").val();
	var endDate = $("input[name='endDate']").val();
    pageIndex = pageIndex ? pageIndex : 1;
	
	var loading = layer.load();

	//分页数据
	$.ajax({
		url:"supplier/channelCheck/getChannelCheckLogPage.json",
		data:{
			settleChannel:settleChannel,
    		endDate:endDate,
    		beginDate:beginDate,
    		pageIndex:pageIndex,
		},
		type:"POST",
		success:function(data){
			layer.close(loading);
			$("#settleChannelCheckLogTable").html(data);
			$('.drop_ul').hover(function(){
		        $('.drop_ul').removeClass('drop_ul1');
		        $(this).addClass('drop_ul1');
		    });
			
		},
		error:function(){
            layer.close(loading);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
	
  }


