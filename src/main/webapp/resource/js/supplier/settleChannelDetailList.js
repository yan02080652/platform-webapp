
//初始化
$(function(){
	initDate(0);
	reloadSettleChannelDetail();
});

function resetForm(){
	$("#keyWords").val("");
	//收支通道
	$("#settleChannel").val("0");
	//出票渠道
	$("#issueChannel").val("0");
	//交易类型
	$("#tradeType").val("0");
	 
}


//分页调用的方法
function reloadSettleChannelDetail(pageIndex){
	//收集参数
	//关键字
	var keyword = $("#keyWords").val();
	//入账起始日期
	var beginDate = $("#begin").val();
	//入账截止日期
	var endDate = $("#end").val();
	//收支通道
	var settleChannel = $("#settleChannel").val();
	//出票渠道
	var issueChannel = $("#issueChannel").val();
	//交易类型
	var tradeType = $("#tradeType").val();
	
    var tmcId ='';
   
	var loading = layer.load();

	//分页数据
	$.ajax({
		url:"supplier/channelDetail/detailList.json",
		data:{
			beginDate:beginDate,
    		endDate:endDate,
    		keyword:keyword,
    		settleChannel:settleChannel,
	        issueChannel:issueChannel,
	        tradeType:tradeType,
    		pageIndex:pageIndex,
			tmcId:tmcId,
		},
		type:"POST",
		success:function(data){
			layer.close(loading);
			$("#settleChannelDetailTable").html(data);
			$('.drop_ul').hover(function(){
		        $('.drop_ul').removeClass('drop_ul1');
		        $(this).addClass('drop_ul1');
		    });
			
		},
		error:function(){
            layer.close(loading);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
	//汇总统计数据
	$.ajax({
		url:"supplier/channelDetail/detailData.json",
		data:{
			beginDate:beginDate,
    		endDate:endDate,
    		keyword:keyword,
    		settleChannel:settleChannel,
	        issueChannel:issueChannel,
	        tradeType:tradeType,
			tmcId:tmcId,
		},
		type:"POST",
		success:function(data){
			$("#settleChannelDetailData").html(data);
			$('.drop_ul').hover(function(){
		        $('.drop_ul').removeClass('drop_ul1');
		        $(this).addClass('drop_ul1');
		    });
			
		},
		error:function(){
            layer.close(loading);
			layer.msg("系统繁忙，请稍后再试");
		}
	});
  }

 function reloadExportSettleChannelDetail(){

	//收集参数
	//关键字
	var keyword = $("#keyWords").val();
	//入账起始日期
	var beginDate = $("#begin").val();
	//入账截止日期
	var endDate = $("#end").val();
	//收支通道
	var settleChannel = $("#settleChannel").val();
	//出票渠道
	var issueChannel = $("#issueChannel").val();
	//交易类型
	var tradeType = $("#tradeType").val();
	
    var tmcId ='';
    if ($("#queryBpTmcId").val() != undefined) {
        if (!$("#queryBpTmcId").val().isEmpty()){
            tmcId = $("#queryBpTmcId").val();
        }
    }
    var loading = layer.load();
    
	window.location.href='supplier/channelDetail/exportSettleChannelDetail?beginDate='+beginDate+'&endDate='+endDate+'&keyword='+keyword+'&settleChannel='+settleChannel+'&issueChannel='+issueChannel+'&tradeType='+tradeType+'&tmcId='+tmcId;
	
	setTimeout(function() { layer.close(loading); }, 3000);
  }

//更新时间
function initDate(range){
	/*var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	var begin ;
	if(range=='0'){//一月
		if((month-1)==0){//跨年时要判断
			year--;
			month=12;
		}else{
			month--;
		}
		begin = year+"-"+month+"-"+day;
	}else{//一年
		begin = (year-1)+"-"+month+"-"+day;
	}
	$("#begin").val(begin);
	$("#end").val(new Date().format("yyyy-MM-dd"));*/
	//昨天的时间
	var day1 = new Date();
	day1.setTime(day1.getTime()-24*60*60*1000);
	var date = day1.getFullYear()+"-" + (day1.getMonth()+1) + "-" + day1.getDate();
	$("#begin").val(date);
	$("#end").val(date);
}


function addSettleChannelDetail(){
    $.get('/supplier/channelDetail/toAddDetail', {
    },function (data) {
        $("#settleChannelDetail_div").html(data);
        $("#modelSettleChannelDetail").modal();
    })
}

