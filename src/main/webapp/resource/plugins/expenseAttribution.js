var cost={};
var code=null;
var corpId=null;
var orgId=null;
var travelCode = null;
(function(){
	
	
	//选择出差申请
    var $product_pull = $('.product_pull');
    var $product_ul = $('.product_ul');
    $product_pull.click(function () {
        if ($product_ul.is(':hidden')) {
            $product_ul.show()
        } else {
            $product_ul.hide()
        }
    });
    $('.product_ul li').click(function () {
        $('.product_div').val($(this).html());
    });
    $('.product_pull').mouseleave(function () {
        $('.product_ul').hide();
    });


   //差旅类型下拉
    var $cost_pull = $('.cost_pull');  //最外层div
    var $cost_ul = $('.cost_ul');
    $cost_pull.click(function () {
        if ($cost_ul.is(':hidden')) {
            $cost_ul.show()
        } else {
            $cost_ul.hide()
        }
    });
    //差旅类型
    $('.cost_ul li').click(function () {
    	$(".cost_pull").css("border","");
    	$('.cost_show').attr("code",$(this).attr("code"));
        $('.cost_show').attr("value",$(this).text()); 
        
        $('.cost_show').val($(this).html()); //显示内容的小div
        
    });
    $('.cost_pull').mouseleave(function () {
        $('.cost_ul').hide();
    });
    $('.item_pull').mouseleave(function () {
        $('.ul_b').hide();
    });

    $('.cost_show').val($('.cost_ul li:first').html());
    $('.cost_show').attr("code",$('.cost_ul li:first').attr("code"));
    //================================================================

    //费用归属下拉
    var $item_pull = $('.item_pull');  //最外层div
    var $ul_b = $('.ul_b');
    $item_pull.click(function () {
        if ($ul_b.is(':hidden')) {
        	$ul_b.show()
        } else {
        	$ul_b.hide()
        }
    });
    //费用归属
    $('.item_ul li').click(function () {
    	$('.item_div').val($(this).html());
    	$(".item_pull").css("border","");
        $('.item_div').attr("code",$(this).attr("code"));
     	$('.item_div').attr("corpId",$(this).attr("corpId"));
     	$('.item_div').attr("orgId",$(this).attr("orgId"));
         
    });
    $('.ul_b').mouseleave(function () {
        $('.ul_b').hide();
    });
	
    $('.item_div').val($('.item_ul li:first').html())//费用归属
    $('.item_div').attr("code",$('.item_ul li:first').attr("code"));
    
})();

var costName;
$('.otherExpense').on('click', function () {
    layer.open({
        title: '选择成本单元',
        type: 1,
        area: ['350px', '350px'],
        skin: 'layui-layer-rim', //加上边框
        content: $('.otherExpenseH'),
        btn: ['确定', '取消'],
        yes: function (index) {
        	 if ($("cite").hasClass("activeTree")) {
        		 $(".item_pull").css("border","");
        	 }
            layer.close(index);
            $(".item_div").val(costName);
            
     	   $('.item_div').attr("code", code);
  	       $('.item_div').attr("corpId", corpId);
  	       $('.item_div').attr("orgId", orgId);
  	       
  	       $(".orgIdAndcostIdAndCorpId").val(orgId + "," + code + "," + corpId);
            
            /*保存操作*/
        },
        btn2: function (index) {
            /*取消操作*/
            layer.close(index);
        },
        cancel: function (index) {
            /*右上角关闭回调*/
        }
    })
});

//成本中心树
loadCostTree();
function loadCostTree(){
	$.ajax({
		type : 'GET',
		url : "order/inter/expenseTree.json",
		async: false,
        data:{"cId":$("input[name='cId']").val()},
		success : function(data1) {
			//批量移动的树
		    layui.tree({
		    	skin : 'as' ,
				drag : true ,
		        elem: '#expenseDetail' //批量移动的树
		        , nodes: TR.turnToLayerTreeViewDataIsCost(data1),
		        click : function (data){
		        	var da=data.data;
		        	if(data.href=='1'){
		        			costName = da.name;
		        	       code=da.costCenterId;
		        	       corpId=da.corpId;
		        	       orgId=da.orgIdForCostCenter;
		        	       
		           }
		        	
		        
		    }
		    });
		    initCostCenter();
			
		}
	});
	
}
function initCostCenter(){
	//选中效果以及小圆点
    var newObj = '<div class="costUnit"></div>';
    //防止反复追加
    $('cite').addClass('citeOpen');
    $('#expenseDetail').hover(function () {
        $('#expenseDetail a').each(function () {
        	var cite=$(this).find("cite");
            if ($(this).attr("href") == '1' && cite.hasClass('citeOpen')) {
            	cite.append(newObj);
            	cite.removeClass('citeOpen');
            	cite.addClass('activePoint');
            }
        })
    });
    
    $('a').click(function(event){
    	if($(this).attr("href")=='1'){
    		$("cite").removeClass('activeTree');
    		$(this).find("cite").addClass('activeTree');
            event.preventDefault();
        }
    });
    

    $('#expenseDetail i').click(function() {
	    $(this).next().removeClass('activeTree');
	});

}

/**
 * 差旅信息提交前的校验
 * true校验通过
 */
function costValidate(){
	var resultC=true;
	var cost=$(".item_div").attr("code");
	if($.isEmptyObject(cost)){
		resultC=false;
		$(".item_pull").css("border","solid 1px red");
	}
	return resultC;
}
/**
 * orgId组织id, corpId法人id,  id成本核算单元id
 * @returns cost
 */
function getCostObject(){
	cost.code=$('.item_div').attr("code");
	cost.corpId=$('.item_div').attr("corpId");
	cost.orgId=$('.item_div').attr("orgId");
	return cost;
}

function setCostObject(v1,v2,v3){
	code = v1;
	corpId = v2;
	orgId = v3;
}

function getTravelCode(){
	return $(".cost_show").attr("code");
}


function setTravelCode(name,code){
	$(".cost_pull").css("border","");
	$('.cost_show').attr("code",code);
    $('.cost_show').val(name);
}

function setCost(name,code,orgId,corpId){
	$(".item_pull").css("border", "");
	$('.item_div').val(name);
	$('.item_div').attr("code", code);
	$('.item_div').attr("corpId", corpId);
 	$('.item_div').attr("orgId", orgId);
}
//默认选中
$('.item_div').val($('.item_ul li:first').html());
$('.item_div').attr("code", $(".item_ul li:first").attr("code"));
$('.item_div').attr("corpId", $(".item_ul li:first").attr("corpId"));
$('.item_div').attr("orgId", $(".item_ul li:first").attr("orgId"));
