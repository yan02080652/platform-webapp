String.prototype.trim = function() {
	return this.replace(/(^\s*)|(\s*$)/g, "");
};

String.prototype.ltrim = function() {
	return this.replace(/(^\s*)/g, "");
};

String.prototype.rtrim = function() {
	return this.replace(/(\s*$)/g, "");
};

String.prototype.notEmpty = function() {
	return this != null && this.trim() != '';
};

String.prototype.isEmpty = function() {
	return !this.notEmpty();
};

String.prototype.endWith = function(str) {
	if (str == null || str == "" || this.length == 0 || str.length > this.length)
		return false;
	if (this.substring(this.length - str.length) == str)
		return true;
	else
		return false;
	return true;
}
String.prototype.startWith = function(str) {
	if (str == null || str == "" || this.length == 0 || str.length > this.length)
		return false;
	if (this.substr(0, str.length) == str)
		return true;
	else
		return false;
	return true;
}

Object.clone = function(sObj) {

	if (typeof sObj !== "object") {
		return sObj;
	}

    var s = {}; 
    if(sObj.constructor == Array){ 
        s = []; 
    } 

    for(var i in sObj){ 
        s[i] = Object.clone(sObj[i]); 
    } 
    return s; 
};

Array.unique = function(array){
	array.sort();
	var re = [array[0]];
	for(var i =1; i<array.length;i++){
		if(array[i] !==re[re.length-1]){
			re.push(array[i]);
		}
	}
	return re;
};

Array.prototype.insert = function(index, item){
	this.splice(index, 0, item);
}

if(!Date.now){
	Date.now = function(){
		return new Date().getTime();
	};
}

Date.prototype.format = function(fmt){
	var o = { 
	 "M+" : this.getMonth()+1,                 //月份 
	 "d+" : this.getDate(),                    //日 
	 "h+" : this.getHours(),                   //小时 
	 "m+" : this.getMinutes(),                 //分 
	 "s+" : this.getSeconds(),                 //秒 
	 "q+" : Math.floor((this.getMonth()+3)/3), //季度 
	 "S"  : this.getMilliseconds()             //毫秒 
	}; 
	if(/(y+)/.test(fmt)){
		fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
	}
	for(var k in o){
		if(new RegExp("("+ k +")").test(fmt)){
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length))); 
		}
	} 
	return fmt; 
};

if(!Object.keys){
    Object.keys = (function(){
         var hasOwnPrototype = Object.prototype.hasOwnPrototype,
               hasDontEnumBug = !({toString:null}).propertyIsEnumerable('toString'),
          dontEnums = [
                 'toString',
                 'toLocalString',
                 'valueOf',
                 'hasOwnProperty',
                 'isPrototypeOf',
                 'propertyIsEnumerable',
                 'constructor'
          ],
          dontEnumsLength = dontEnums.length;
          return function(obj){
                if(typeof obj !== 'object' && typeof obj !== 'function' || obj === null){
        throw new TypeError('Object.keys called on non-object')
                }  
                var result = [];
                for(var prop in obj){
                     if(hasOwnProperty.call(obj,prop)){
                             result.push(prop);
                     }
               }
               if(hasDontEnumBug){
                     for(var i =0;i<dontEnumsLength;i++){
                          if (hasOwnProperty.call(obj, dontEnums[i])){           
                                 result.push(dontEnums[i])                      
                          }
                     }
               }
               return result; 
          }
    })
}